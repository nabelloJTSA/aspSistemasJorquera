﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCategorias.aspx.vb" Inherits="aspSistemasJorquera.aspCategorias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    <script type='text/javascript'>
        function openModal() {
            $('[id*=myModal]').modal('show');
        }

    </script>

    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text="CATEGORIAS"></asp:Label>
        </h1>
    </section>


    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <%--     <asp:UpdatePanel runat="server" ID="UpdatePanel">
                    <ContentTemplate>--%>

                <div class="row">
                    <div class="col-md-3 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Nombre</label>
                        <div>
                            <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre ..." CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Script</label>
                        <div>
                            <asp:TextBox ID="txtScript" runat="server" TextMode="MultiLine" Height="150px" placeholder="Script ..." CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Descripción</label>
                        <div>
                            <asp:TextBox ID="txtDes" runat="server" TextMode="MultiLine" Height="150px" placeholder="Descripción ..." CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 ">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:Button ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server" Text="Guardar" />
                        </div>
                    </div>

                    <div class="col-md-1">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:Button ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server" Text="Cancelar" UseSubmitBehavior="False" />
                        </div>
                    </div>
                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">

                        <asp:GridView ID="grdDatos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnSeleccionar" CssClass="small-box-footer text-orange" OnClick="btnSeleccionar_Click" runat="server"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Nombre Categoria">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Script">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScript" runat="server" Text='<%# Bind("obs_script")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Descripción">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDes" runat="server" Text='<%# Bind("des_categoria")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEscalamiento" runat="server" CssClass="text-orange" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerEscalamientosGrilla" Text="ESCALAMIENTO"> </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                        </asp:GridView>
                    </div>
                </div>


                <asp:HiddenField ID="hdnID" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />


                

                <div class="modal" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                    <div class="modal-dialog  modal-lg">
                        <div class="modal-content">
                       

                             <div class="modal-header bg-warning">
                                                    <h3 class="modal-title text-light">
                                                        <asp:Label ID="lblNomModal" CssClass="text-orange" Font-Bold="true" runat="server" Text="CATEGORIA: "></asp:Label><asp:Label ID="lblNomCategoria" runat="server" CssClass="text-orange" Text="-"></asp:Label></h3>
                                                    
                                                        
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;</button>
                                                </div>

                            <div class="modal-body">
                                <asp:UpdatePanel runat="server" ID="updClientes">
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="btnGuardarEscalamiento" EventName="Click" />--%>                                    
                                    </Triggers>

                                    <ContentTemplate>
                                        <div class="row justify-content-center">
                                            <div class="col-md-10 form-group justify-content-center">
                                                <label for="fname"
                                                    class="control-label col-form-label">
                                                    Escalamientos</label>
                                                <div>
                                                    <asp:DropDownList ID="cboEscalamiento" CssClass="form-control" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-2 form-group pull-right">
                                                <label for="fname"
                                                    class="control-label col-form-label">
                                                    </label>
                                                <div>
                                                  <asp:LinkButton ID="btnGuardarEscalamiento" runat="server" CssClass="btn bg-orange btn-block">Guardar</asp:LinkButton>
                                                </div>


                                               
                                            </div>

                                        </div>

                                       

                                        <div class="row justify-content-center">
                                            <div class="col-md-12 form-group justify-content-center">
                                                <asp:GridView ID="grdEscalamiento" runat="server" CssClass="table table-bordered with-border table-striped  bg-warning  " AutoGenerateColumns="false" GridLines="None">
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="Escalamiento">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEscalamiento" runat="server" Text='<%# Eval("nom_escalamiento") %>'></asp:Label>
                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Descripcion">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDes" runat="server" Text='<%# Eval("des_escalamiento") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnEliminar" CssClass="text-orange" OnClick="EliminarEscalamiento" runat="server"> <i class="fa fa-trash"></i></asp:LinkButton>    

                                                            </ItemTemplate>
                                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle HorizontalAlign="center" BackColor="#FFB738" />

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>







                <%--           </ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>
    </section>
    <!-- /.content -->


</asp:Content>





