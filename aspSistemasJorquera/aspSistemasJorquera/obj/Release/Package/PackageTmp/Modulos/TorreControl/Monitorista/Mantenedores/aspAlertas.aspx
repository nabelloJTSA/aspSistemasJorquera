﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAlertas.aspx.vb" Inherits="aspSistemasJorquera.aspAlertas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
       <link href="/Content/cssTabs.css" rel="stylesheet" />
    
    <section class="content-header">       
        <h2>
            <asp:Label ID="lbltitulo" runat="server" Text="CONFIGURACIÓN DE ALERTAS"></asp:Label>
        </h2>
    </section>
    <section class="content">       
        <div class="row">
            <div class="col-md-12">
                <asp:UpdatePanel runat="server" ID="UpdatePanel">
                    <ContentTemplate>
                        <div class="row" style="text-align:right;">
                            <div class="col-md-2">
                                <div>
                                    <asp:Button ID="btnNuevo" OnClick="btnNuevo_Click" CssClass="btn bg-orange btn-block" runat="server" Text="Nueva Alerta" />
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-12">
                                <asp:GridView ID="grdAlertas" runat="server" style="float:left" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnSeleccionar" OnClick="btnSeleccionar_Click" CssClass="small-box-footer text-orange" runat="server"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>                 
                                                             
                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("nom_alerta")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnIDAlerta" runat="server" Value='<%# Bind("id_alerta")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Categoría">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnIDCategoria" runat="server" Value='<%# Bind("id_categoria")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Criticidad">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomCriticidad" runat="server" Text='<%# Bind("cri_alerta")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cliente">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomCliente" runat="server" Text='<%# Bind("cli_alerta")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnRUTCliente" runat="server" Value='<%# Bind("rut_cliente")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("des_alerta")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Fecha de Activación">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecActivacion" runat="server" Text='<%# Bind("fec_activa")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Hora Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHoraInicio" runat="server" Text='<%# Bind("hor_inicio")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Hora Término">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHoraFin" runat="server" Text='<%# Bind("hor_fin")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cant. Ocurrencias">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCantOcurrencias" runat="server" Text='<%# Bind("ocu_cantidad")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tiem. Ocurrencias">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTiemOcurrencias" runat="server" Text='<%# Bind("ocu_tiempo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tiem. Separación">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTiemSeparacion" runat="server" Text='<%# Bind("sep_tiempo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dist. Separación">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDistSeparacion" runat="server" Text='<%# Bind("sep_distancia")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Alerta Base">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAlertaBase" runat="server" Text='<%# Bind("nom_alerta_base")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnIDAlertaBase" runat="server" Value='<%# Bind("id_alerta_base")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                    <asp:LinkButton ID="btnEstado" OnClick="btnEstado_Click" CssClass='<%# If(Eval("est_alerta").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>' runat="server"> <i class='<%# If(Eval("est_alerta").ToString() = "0", "fa fa-toggle-on fa-flip-horizontal", "fa fa-toggle-on")%>'></i></asp:LinkButton>                                                                                         
                                                    <asp:HiddenField ID="hdnEstado" runat="server" Value='<%# Bind("est_alerta")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                </asp:GridView>
                            </div>
                        </div>             
                        <asp:HiddenField ID="hdnEdit" runat="server" Value="0" />

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
       
        

            <!-- MODAL INGRESO DE ALERTA -->
        <asp:UpdatePanel ID="upModalAlerta" runat="server" UpdateMode="Conditional">
            <ContentTemplate> 
            <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" aria-hidden="true">
                <div style="width:90%" class="modal-dialog">
                    <asp:UpdatePanel ID="upModal" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text="Reglas de Alerta"></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="warpper">
                                        <input class="radio" id="one" name="group" type="radio" checked>
                                        <input class="radio" id="two" name="group" type="radio">
                                        <input class="radio" id="three" name="group" type="radio">
                                        <div class="tabs form-inline">
                                            <div class="form-inline" style="float:left">
                                                <label class="tab" id="one-tab" for="one">Información General</label>
                                            </div>
                                            <div class="form-inline" style="float:left">
                                                <label class="tab" id="two-tab" for="two" >Frecuencia</label>
                                            </div>
                                            <asp:HiddenField runat="server" ID="hdnVisible" Value="false"/>
                                            <div class="form-inline" id="divTres" runat="server">
                                                <label class="tab" ID="three-tab" for="three">Tareas y Protocolos</label>                                                                               
                                            </div>
                                        </div>
                                        <div class="panels-v2">
                                            <div class="panel" id="one-panel">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanelInfo">
                                                    <ContentTemplate>                       
                                                        <div class="panel-title">Información de la Alerta</div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <label for="fname" class="control-label col-form-label">Nombre de la Alerta</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAlertaNom" runat="server" placeholder="Nombre..." CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 form-group">
                                                                <label for="fname" class="control-label col-form-label">Categoría de Agrupación</label>
                                                                <div>
                                                                    <asp:DropDownList ID="cboCategoria" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 form-group">
                                                                <label for="fname" class="control-label col-form-label">Tipo de Alerta Base</label>
                                                                <div>
                                                                    <asp:DropDownList ID="cboAlertaBase" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 form-group">
                                                                <label for="fname" class="control-label col-form-label">Nivel de Criticidad</label>
                                                                <div>
                                                                    <asp:DropDownList ID="cboCriticidad" runat="server" CssClass="form-control">
                                                                        <asp:ListItem>Baja</asp:ListItem>
                                                                        <asp:ListItem>Media</asp:ListItem>
                                                                        <asp:ListItem>Alta</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Cliente</label>
                                                                <div>
                                                                    <asp:DropDownList ID="cboCliente" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>                                                          
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 form-group">
                                                                <label for="fname" class="control-label col-form-label">Descripción</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAlertaDesc" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel-title">Activación de la Alerta</div>
                                                        <div class="row">
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Fecha de Activación</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAlertaFecha" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Hora de Inicio</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAlertaInicio" TextMode="Time" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Hora de Término</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtAlertaFin" TextMode="Time" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            </div>

                                            <div class="panel" id="two-panel">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanelFrec">
                                                    <ContentTemplate>
                                                        <div class="panel-title">Frecuencia de Medición</div>
                                                        <div class="row">
                                                            
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Tipo de Control</label>
                                                                <div style="margin-left:7%">
                                                                    <asp:CheckBox type="checkbox" runat="server" OnCheckedChanged="chkFrecOcu_CheckedChanged" ID="chkFrecOcu"  CssClass="checkbox" AutoPostBack="true"/>
                                                                    <label class="form-check-label">Ocurrencia</label>
                                                                </div>
                                                                <div style="margin-left:7%">
                                                                    <asp:CheckBox type="checkbox" runat="server" OnCheckedChanged="chkFrecTiempo_CheckedChanged" ID="chkFrecTiempo" CssClass="checkbox" AutoPostBack="true"/>
                                                                    <label class="form-check-label">Tiempo</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Cantidad de Ocurrencias</label>
                                                                <div>
                                                                    <asp:TextBox runat="server" ID="txtFrecOcurrencia" Enabled="false" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Tiempo de Ocurrencias</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtFrecTiempo" TextMode="Time" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>  

                                                        <div class="panel-title">Separación entre Alertas</div>
                                                        <div class="row">
                                                             <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Tipo de Separación</label>
                                                                <div style="margin-left:7%">
                                                                    <asp:CheckBox type="checkbox" runat="server" OnCheckedChanged="chkSepDistancia_CheckedChanged" ID="chkSepDistancia" CssClass="checkbox" AutoPostBack="true"/>
                                                                    <label class="form-check-label">Distancia</label>
                                                                </div>
                                                                <div style="margin-left:7%">
                                                                    <asp:CheckBox type="checkbox" runat="server" OnCheckedChanged="chkSepTiempo_CheckedChanged" ID="chkSepTiempo" CssClass="checkbox" AutoPostBack="true"/>
                                                                    <label class="form-check-label">Tiempo</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Distancia entre Alertas</label>
                                                                <div>
                                                                    <asp:TextBox runat="server" Enabled="false" ID="txtAlertaSepDistancia" TextMode="Number" placeholder="" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Tiempo entre Alertas</label>
                                                                <div>
                                                                    <asp:TextBox ID="txtFrecSepTiempo" Enabled="false" TextMode="Time" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>                                                   
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="panel" id="three-panel">
                                                <asp:UpdatePanel ID="upPanelTres" runat="server" Visible="false">
                                                    <ContentTemplate>
                                                        <div class="panel-title">Niveles de Escalamiento</div>
                                                        <div class="row">
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label">Nombre de Nuevo Nivel</label>
                                                                <div>
                                                                    <asp:TextBox runat="server" ID="txtNomNivel" placeholder="Nombre ..." CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 form-group">
                                                                <label for="fname" class="control-label col-form-label"></label>
                                                                <div class="row">
                                                                    <div class ="col-md-3 form-group">
                                                                        <div>
                                                                            <asp:Button runat="server" ID="btnScript" OnClick="btnScript_Click" aria-hidden="true" Text="SCRIPT" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 form-group">
                                                                        <div>
                                                                            <asp:Button runat="server" ID="btnGuardarNivel" OnClick="btnGuardarNivel_Click" aria-hidden="true" Text="Guardar"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                           
                                                        
                                                        <div class="row">
                                                            <div class="col-md-12 form-group" style="border-radius:3px;margin-top:1%;">                                                             
                                                                <asp:ListView runat="server" ID="listaNiveles">                                                                                                               
                                                                    <ItemTemplate>
                                                                        <div class="row">                                                                           
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <div class="col-sm-4 bg-orange form-inline" style="padding-top:2.5px;padding-bottom:2.5px;">                                                                      
                                                                                        <label for="fname" style="margin-left:1%;margin-right:1%; margin-bottom:4px;float:left;" class="control-label col-form-label">Nivel <%# Eval("nro_nivel")  %>: <%# Eval("nom_nivel") %></label>
                                                                                        <asp:UpdatePanel runat="server">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton runat="server" ID="linEditarNivel" OnClick="linEditarNivel_Click" EventName="Click" CssClass="btn-sm bg-yellow">
                                                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                                                </asp:LinkButton>
                                                                                                <label style="margin-left:1%"></label>
                                                                                                <asp:LinkButton ID="linAgregarContacto" runat="server" OnClick="linAgregarContacto_Click" EventName="Click">
                                                                                                    <i class="fa fa-user-plus btn-sm bg-yellow"></i>
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="linEditarNivel"></asp:AsyncPostBackTrigger>
                                                                                                <asp:AsyncPostBackTrigger ControlID="linAgregarContacto"></asp:AsyncPostBackTrigger>
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                        <asp:Label runat="server" ID="btnEstNivel">             
                                                                                            <asp:HiddenField runat="server" ID="hdnNroNivel" Value='<%# Eval("nro_nivel") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnNomNivel" Value='<%# Eval("nom_nivel") %>'/>                                                                             
                                                                                            <asp:HiddenField runat="server" ID="hdnEstadoNivel" Value='<%# Eval("est_nivel") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnIDNivel" Value='<%# Eval("id_nivel") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnObligatorio" Value='<%# Eval("est_obligatorio") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnNivelLlamada" Value='<%# Eval("scr_llamada") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnNivelSMS" Value='<%# Eval("scr_sms") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnNivelCorreo" Value='<%# Eval("scr_correo") %>'/>
                                                                                            <asp:HiddenField runat="server" ID="hdnNivelWsp" Value='<%# Eval("scr_wsp") %>'/>
                                                                                        </asp:Label>
                                                                                    </div>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            
                                                                            <div class="col-sm-8 bg-orange form-inline" style="padding-top:2.5px;padding-bottom:2.5px;text-align:right;">       
                                                                                <asp:UpdatePanel runat="server">
                                                                                    <ContentTemplate>
                                                                                       
                                                                                        <asp:LinkButton ID="linEstadoNivel" runat="server" OnClick="linEstadoNivel_Click" EventName="Click"><i class='<%# If(Eval("est_nivel").ToString() = "1", "fa fa-toggle-on btn-sm bg-green", "fa fa-toggle-on fa-flip-horizontal btn-sm bg-red ") %>'></i></asp:LinkButton>
                                                                                        <asp:LinkButton ID="linObligatorio" runat="server" OnClick="linObligatorio_Click" EventName="Click"><i class='<%# If(Eval("est_obligatorio").ToString() = "1", "fa fa-ban btn-sm bg-red", "fa fa-ban btn-sm bg-green") %>'></i></asp:LinkButton>
                                                                                        
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="linEstadoNivel"></asp:AsyncPostBackTrigger>
                                                                                        <asp:AsyncPostBackTrigger ControlID="linObligatorio"></asp:AsyncPostBackTrigger>
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            
                                                                                <asp:GridView ID="grdContactos" runat="server" style="float:left" CssClass="table bg-gray-light" AutoGenerateColumns="false">
                                                                                    <Columns>           
                                                             
                                                                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:UpdatePanel runat="server">
                                                                                                    <ContentTemplate>
                                                                                                        <asp:LinkButton ID="linEditContacto" OnClick="linEditContacto_Click" runat="server"><i class="fa fa-pencil-square text-orange"></i></asp:LinkButton>                                                                            
                                                                                                    </ContentTemplate>
                                                                                                    <Triggers>
                                                                                                        <asp:AsyncPostBackTrigger ControlID="linEditContacto" />
                                                                                                    </Triggers>
                                                                                                </asp:UpdatePanel>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>  

                                                                                        <asp:TemplateField HeaderText="Nombre de Contacto" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblNomContacto" runat="server" Text='<%# Bind("nom_contacto")%>'></asp:Label> 
                                                                                                <asp:HiddenField runat="server" ID="hdnIDContacto" Value='<%# Bind("id_contacto")%>'/>    
                                                                                                <asp:HiddenField runat="server" ID="hdnTipoContacto" Value='<%# Bind("tip_contacto")%>'/>                                                                                         
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>                                                                                   

                                                                                        <asp:TemplateField HeaderText="Correo de Contacto" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblCorContacto" runat="server" Text='<%# Bind("cor_contacto")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>  

                                                                                        <asp:TemplateField HeaderText="Teléfono de Contacto" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblTelContacto" runat="server" Text='<%# Bind("tel_contacto")%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>  
                                                                                    
                                                                                        <asp:TemplateField HeaderText="Rol del Contacto" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblRolContacto" runat="server" Text='<%# Bind("nom_rol")%>'></asp:Label>    
                                                                                                <asp:HiddenField runat="server" ID="hdnIDRol" Value='<%# Bind("id_rol")%>'/>                                                                                        
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>
                                                                                    
                                                                                        <asp:TemplateField HeaderText="Llamada" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField runat="server" ID="hdnLlamada" Value='<%# Bind("act_llamada")%>'/>
                                                                                                <asp:LinkButton ID="linkLlamada" runat="server" CssClass='<%# If(Eval("act_llamada").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>'>
                                                                                                    <div style="text-align:center">
                                                                                                        <i class='<%# If(Eval("act_llamada").ToString() = "1", "fa fa-check-circle", "fa fa-times-circle")%>'></i>
                                                                                                    </div>
                                                                                                </asp:LinkButton>                                                                                            
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>
                                                                                    
                                                                                        <asp:TemplateField HeaderText="SMS" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField runat="server" ID="hdnSMS" Value='<%# Bind("act_sms")%>'/>
                                                                                                <asp:LinkButton ID="linkSMS" runat="server" CssClass='<%# If(Eval("act_sms").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>'>
                                                                                                    <div style="text-align:center">
                                                                                                        <i class='<%# If(Eval("act_sms").ToString() = "1", "fa fa-check-circle", "fa fa-times-circle")%>'></i>
                                                                                                    </div>
                                                                                                </asp:LinkButton>                                                                                            
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>
                                                                                    
                                                                                        <asp:TemplateField HeaderText="Correo" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField runat="server" ID="hdnCorreo" Value='<%# Bind("act_correo")%>'/>
                                                                                                <asp:LinkButton ID="linkCorreo" runat="server" CssClass='<%# If(Eval("act_correo").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>'>
                                                                                                    <div style="text-align:center">
                                                                                                        <i class='<%# If(Eval("act_correo").ToString() = "1", "fa fa-check-circle", "fa fa-times-circle")%>'></i>
                                                                                                    </div>
                                                                                                </asp:LinkButton>                                                                                            
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>  
                                                                                    
                                                                                        <asp:TemplateField HeaderText="WhatsApp" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField runat="server" ID="hdnWsp" Value='<%# Bind("act_wsp")%>'/>
                                                                                                <asp:LinkButton ID="linkWsp" runat="server" CssClass='<%# If(Eval("act_wsp").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>'>
                                                                                                    <div style="text-align:center">
                                                                                                        <i class='<%# If(Eval("act_wsp").ToString() = "1", "fa fa-check-circle", "fa fa-times-circle")%>'></i>
                                                                                                    </div>
                                                                                                </asp:LinkButton>                                                                                            
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" />
                                                                                        </asp:TemplateField>                                                                                                
                                                                                       
                                                                                        <%--<asp:TemplateField HeaderText="" HeaderStyle-CssClass="bg-gray">
                                                                                            <ItemTemplate>
                                                                                                <asp:Button ID="btnBorrarContacto" OnClick="btnBorrarContacto_Click" CssClass="bg-danger text-danger" runat="server" Text="x"></asp:Button>
                                                                                                <asp:HiddenField runat="server" ID="hdnIDContacto" Value='<%# Bind("id_contacto")%>'/>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                        </asp:TemplateField>  --%> 
                                                                                        
                                                                                    </Columns>
                                                                                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                                                                </asp:GridView>
                                                                           
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:ListView>  
                                                                      
                                                            </div>                                                        
                                                            <asp:HiddenField runat="server" ID="hdnEditarNivel" Value="0"/>     
                                                            <asp:HiddenField runat="server" ID="hdnInsNivel" Value="0"/>      
                                                            <asp:HiddenField runat="server" ID="hdnEditarContacto" Value="0"/>  
                                                        </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel> 
                                            </div>                   
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <asp:UpdatePanel runat="server" ID="upFooter">
                                        <ContentTemplate>
                                            <asp:Button runat="server" ID="btnCancelarAlerta" data-dismiss="modal" aria-hidden="true" Text="Cerrar"/>
                                            <asp:Button runat="server" ID="btnGuardarAlerta" OnClick="btnGuardarAlerta_Click" aria-hidden="true" Text="Guardar"/>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                                   
                                </div>
                            </div>    
                                          
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        

        <!-- MODAL SCRIPTS -->
        <div class="modal fade" id="myModalContactos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <asp:UpdatePanel ID="upModalContactos" runat="server">
                    <ContentTemplate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" onclick="$('#myModalContactos').modal('hide');" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><asp:Label ID="Label1" runat="server" Text="Definición de Scripts de Protocolo"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-8 form-inline">
                                        <asp:Button ID="btnScrPersona" OnClick="btnScrPersona_Click" runat="server" Text="Persona" CssClass="btn bg-gray-active"/>
                                        <asp:Button ID="btnScrUsuario" OnClick="btnScrUsuario_Click" runat="server" Text="Usuario" CssClass="btn bg-gray-active"/>
                                        <asp:Button ID="btnScrPatente" OnClick="btnScrPatente_Click" runat="server" Text="Patente" CssClass="btn bg-gray-active"/>
                                        <asp:Button ID="btnScrAlerta" OnClick="btnScrAlerta_Click" runat="server" Text="Alerta" CssClass="btn bg-gray-active"/>
                                    </div>
                                    <div class="col-md-1">
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:HiddenField runat="server" ID="hdnCampo" Value="0"/>
                                    <div class="col-md-12 form-group">
                                        <label for="fname" class="control-label col-form-label">Script de Llamada</label>
                                        <div>
                                            <asp:TextBox runat="server" OnTextChanged="txtScrLlamada_TextChanged" AutoPostBack="false" TextMode="MultiLine" ID="txtScrLlamada" ClientIDMode="Static" placeholder="Escribir..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="fname" class="control-label col-form-label">Script de SMS</label>
                                        <div>
                                            <asp:TextBox runat="server" OnTextChanged="txtScrSms_TextChanged" AutoPostBack="false" TextMode="MultiLine" ID="txtScrSms" placeholder="Escribir..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="fname" class="control-label col-form-label">Script de Correo</label>
                                        <div>
                                            <asp:TextBox runat="server" OnTextChanged="txtScrCorreo_TextChanged" AutoPostBack="false" TextMode="MultiLine" ID="txtScrCorreo" placeholder="Escribir..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="fname" class="control-label col-form-label">Script de WhatsApp</label>
                                        <div>
                                            <asp:TextBox runat="server" OnTextChanged="txtScrWsp_TextChanged" AutoPostBack="false" TextMode="MultiLine" ID="txtScrWsp" placeholder="Escribir..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn bg-orange" onclick="$('#myModalContactos').modal('hide');" aria-hidden="true">Ok</button>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>


        <!-- MODAL CONTACTOS -->
        <div class="modal fade" id="myModalInsertarCon" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:50%">
                <asp:UpdatePanel ID="upInsertarCon" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" onclick="$('#myModalInsertarCon').modal('hide');" aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><asp:Label ID="Label2" runat="server" Text="Agregar Nuevo Contacto"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Tipo de Contacto</label>
                                        <div>
                                            <asp:DropDownList runat="server" ID="cboContTipo" CssClass="form-control" OnSelectedIndexChanged="cboContTipo_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem>Contacto Fijo</asp:ListItem>
                                                     <asp:ListItem>Depende del Viaje</asp:ListItem>
                                                     <asp:ListItem>Usuario de Sistema</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Rol del Contacto</label>
                                        <div>
                                            <asp:DropDownList runat="server" ID="cboRoles" CssClass="form-control"  AutoPostBack="true">                                                                           
                                            </asp:DropDownList>
                                        </div>
                                    </div>  
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Usuario de Sistema</label>
                                        <div>
                                            <asp:DropDownList runat="server" ID="cboUsuarios" OnSelectedIndexChanged="cboUsuarios_SelectedIndexChanged" CssClass="form-control"  AutoPostBack="true">                                                                           
                                            </asp:DropDownList>
                                        </div>
                                    </div>                                 
                                </div>  

                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Nombre del Contacto</label>
                                        <div>
                                            <asp:TextBox runat="server" ID="txtConNombre" placeholder="Nombre..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Número de Teléfono</label>
                                        <div>
                                            <asp:TextBox runat="server" ID="txtConTelefono" TextMode="Number" placeholder="Teléfono..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="fname" class="control-label col-form-label">Correo de Contacto</label>
                                        <div>
                                            <asp:TextBox runat="server" ID="txtConCorreo" TextMode="Email" placeholder="Nombre..." CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="fname" class="control-label col-form-label">Canal de Contacto</label>
                                        <div class="form-inline">
                                            <asp:CheckBox runat="server" ID="cheLlamada" CssClass="checkbox"/>
                                            <label class="form-check-label" style="margin-right:3%" for="inlineRadio1">Llamada</label>

                                            <asp:CheckBox runat="server" ID="cheSMS" CssClass="checkbox"/>
                                            <label class="form-check-label" style="margin-right:3%" for="inlineRadio1">SMS</label>

                                            <asp:CheckBox runat="server" ID="cheCorreo" CssClass="checkbox"/>
                                            <label class="form-check-label" style="margin-right:3%" for="inlineRadio1">Correo</label>

                                            <asp:CheckBox runat="server" ID="cheWsp" CssClass="checkbox"/>
                                            <label class="form-check-label" style="margin-right:3%" for="inlineRadio1">WhatsApp</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                
                                <asp:Button runat="server" ID="btnGuardarContacto" CssClass="btn bg-orange" Text="Guardar" OnClick="btnGuardarContacto_Click"/>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentClientes" runat="server">
</asp:Content>
