﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspValidarViajeFact.aspx.vb" Inherits="aspSistemasJorquera.aspValidarViajeFact" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMonitorista" runat="server">
 

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <div class="row">
            <div class="col-md-5 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="Validar Viajes Facturación"></asp:Label>
                </h3>
            </div>
                        
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-md-12">
                

                <div class="row">


                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                        Desde</label>
                        <div>
                            <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                        Hasta</label>
                        <div>
                            <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-3 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Cliente</label>
                        <div>
                            <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Servicio</label>
                        <div>
                            <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Trasportista</label>
                        <div>
                            <asp:DropDownList ID="cboTrasportista" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-1 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            POD</label>
                        <div>
                            <asp:DropDownList ID="cboPOD" CssClass="form-control" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="3">TODOS</asp:ListItem>
                                <asp:ListItem Value="1">SI</asp:ListItem>
                                <asp:ListItem Value="0">NO</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                                     

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Expedición</label>
                        <div>

                            <asp:TextBox ID="txtExp" CssClass="form-control" runat="server" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Guia</label>
                        <div>

                            <asp:TextBox ID="txtGuia" CssClass="form-control" runat="server" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-1 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnLimpiarFiltroGral" CssClass="btn btn-primary" runat="server">Limpiar Filtros</asp:LinkButton>
                        </div>
                    </div>



                </div>

                <div class="row table-responsive">
                    <div class="col-md-12">
                        <div class="box">

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-5 form-group  ">
                                        <div>
                                            <h4>
                                                <asp:Label ID="Label11" CssClass="text-primary" runat="server" Text="TOTAL REGISTROS: "></asp:Label>
                                                <b>
                                                    <asp:Label ID="lblTotReg" CssClass="text-primary" runat="server" Text=""></asp:Label></b></h4>
                                        </div>
                                    </div>


                                    <div class="col-md-2 form-group  pull-right">
                                        <div>
                                            <asp:LinkButton ID="btnExportarViajes" CssClass="btn botonesTC-descargar btn-block " runat="server"> <i class="fa fa-download"></i> Descargar Tabla </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>


                                <div class="row table-responsive">


                                    <div class="col-sm-12 col-xs-6">

                                        <div id="global">
                                            <div id="mensajes">
                                                <div class="description-block border-right">


                                                    <asp:GridView ID="grdViajes" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="N° VIAJE / OS / GUIA">
                                                                <ItemTemplate>
                                                                    <b>
                                                                        <asp:LinkButton ID="btnTipoViaje" runat="server" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton></b><br />
                                                                    <b>
                                                                        <asp:LinkButton ID="btnNumViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></b>
                                                                    <br />
                                                                    <b>OS:&nbsp;</b>
                                                                    <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label>
                                                                    <br />
                                                                    <b>GUIA:&nbsp;</b><asp:Label ID="lblNumGuia" Font-Bold="true" runat="server" Text='<%# Bind("num_guia")%>'></asp:Label>



                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>




                                                            <asp:TemplateField HeaderText="CAMIÓN">
                                                                <ItemTemplate>


                                                                    <asp:LinkButton ID="btnCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>'> </asp:LinkButton></b>
                                                             
                                                                    
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="ARRASTRE">
                                                                <ItemTemplate>

                                                                    <b>
                                                                        <asp:LinkButton ID="btnArrastre" runat="server" Text='<%# Bind("nom_pat_arrastre")%>'> </asp:LinkButton></b>


                                                                </ItemTemplate>
                                                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                            </asp:TemplateField>




                                                            <asp:TemplateField HeaderText="ORIGEN/FECHA">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                    <br />
                                                                    <b>
                                                                        <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                        <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label></b>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="DESTINO/FECHA">
                                                                <ItemTemplate>
                                                                    <b>
                                                                        <asp:LinkButton ID="btnDestino" runat="server" Enabled="false" Text='<%# Bind("nom_destino")%>'> </asp:LinkButton></b><br />

                                                                    <b>
                                                                        <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                                        <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>'></asp:Label></b>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="ESTADO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_est_viaje")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="POD">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPOD" runat="server" Text='<%# Bind("est_pod")%>'></asp:Label>
                                                                    <h4>
                                                                        <asp:LinkButton ID="btnCargarPOD" runat="server" Visible="true" ToolTip="Cargar POD"><i class="fa fa-upload"></i></asp:LinkButton></h4>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:TemplateField>



                                                        </Columns>

                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                    </asp:GridView>
                                                </div>


                                            </div>
                                        </div>





                                        <asp:Timer ID="TimerViajes" runat="server" Interval="90000"></asp:Timer>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->



    <%--    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
</asp:Content>
