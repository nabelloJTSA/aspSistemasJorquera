﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREGestionViajes.aspx.vb" Inherits="aspSistemasJorquera.aspREGestionViajes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <%--  <Triggers>
           <asp:PostBackTrigger ControlID="linCargar" />
                                <asp:PostBackTrigger ControlID="linLimpiar" />
                                <asp:PostBackTrigger ControlID="linDescargar" />
          
        </Triggers>--%>
        <ContentTemplate>


            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>


            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->



                <div class="row btn-sm">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Desde</label>
                        <div>
                            <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Hasta</label>
                        <div>
                            <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Familia</label>
                        <div>
                            <asp:DropDownList ID="cboFamilia" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Servicio</label>
                        <div>
                            <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Cliente</label>
                        <div>
                            <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Transportista</label>
                        <div>
                            <asp:DropDownList ID="cboTrasportista" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                </div>


                <div class="row btn-sm">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">POD</label>
                        <div>
                            <asp:DropDownList ID="cboPOD" CssClass="form-control" runat="server">
                                <asp:ListItem Value="3">TODOS</asp:ListItem>
                                <asp:ListItem Value="1">SI</asp:ListItem>
                                <asp:ListItem Value="0">NO</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Patente</label>
                        <div>
                            <asp:TextBox ID="txtCamion" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Conductor</label>
                        <div>
                            <asp:TextBox ID="txtConductor" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Estado</label>
                        <div>
                            <asp:DropDownList ID="cboEstados" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Propietario</label>
                        <div>
                            <asp:DropDownList ID="cboPropietario" runat="server" CssClass="form-control" AutoPostBack="true">
                                <asp:ListItem>TODOS</asp:ListItem>
                                <asp:ListItem>Propio</asp:ListItem>
                                <asp:ListItem>Tercero</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="row btn-sm ">
                    <div class="col-sm-2 form-group  pull-right">
                        <asp:LinkButton ID="linDescargar" OnClick="linDescargar_Click" runat="server" Text="Descargar Reporte" CssClass="btn botonesTC-descargar btn-block"></asp:LinkButton>
                    </div>


                    <div class="col-md-2  pull-right">
                        <div>
                            <asp:LinkButton ID="linLimpiar" OnClick="linLimpiar_Click" runat="server" Text="Limpiar Campos" CssClass="btn btn-primary btn-block"></asp:LinkButton>
                        </div>
                    </div>


                    <div class="col-md-2 form-group  pull-right">
                        <div>
                            <asp:LinkButton ID="linCargar" runat="server" OnClick="linCargar_Click" Text="Cargar Reporte" CssClass="btn btn-primary btn-block"></asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row btn-sm ">
                    <div class="col-md-12">

                        <div class="row table-responsive">
                            <div class="col-sm-12 col-xs-6">

                                <div class="description-block border-right">
                                    <asp:GridView ID="grdViajes" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                        <Columns>

                                            <asp:TemplateField HeaderText="N° VIAJE">
                                                <ItemTemplate>
                                                    <%--<asp:Label ID="lblNumviaje" runat="server" Text='' />--%>
                                                    <asp:LinkButton ID="btnNumViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerFichaViaje" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></>
                                                     
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="OS">
                                                <ItemTemplate>
                                                    <h5>
                                                        <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label></h5>
                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                    <asp:HiddenField ID="hdnOS" runat="server" Value='<%# Bind("OS")%>' />
                                                    <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />
                                                    <asp:HiddenField ID="hdnCodServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                    <asp:HiddenField ID="hdnEstadoviaje" runat="server" Value='<%# Bind("id_est_viaje")%>' />
                                                    <asp:HiddenField ID="hdnCodcliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                    <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                    <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                                    <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                    <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("num_fono")%>' />
                                                    <asp:HiddenField ID="hdnNomTrasnportista" runat="server" Value='<%# Bind("nom_transportista")%>' />
                                                    <%--    <asp:HiddenField ID="hdnCodUnidad" runat="server" Value='<%# Bind("cod_unidad")%>' />--%>
                                                    <asp:HiddenField ID="hdnCodDestino" runat="server" Value='<%# Bind("cod_destino")%>' />
                                                    <asp:HiddenField ID="hdnCodOrigen" runat="server" Value='<%# Bind("cod_origen")%>' />
                                                    <asp:HiddenField ID="hdnGrupoOperativo" runat="server" Value='<%# Bind("grupo_operativo")%>' />
                                                    <asp:HiddenField ID="hdnNumeroViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                    <asp:HiddenField ID="hdnNomCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />
                                                    <asp:HiddenField ID="hdnNomProducto" runat="server" Value='<%# Bind("nom_producto")%>' />
                                                    <asp:HiddenField ID="hdnNomServicio" runat="server" Value='<%# Bind("nom_servicio")%>' />
                                                    <asp:HiddenField ID="hdnNomGrupoOperativo" runat="server" Value='<%# Bind("nom_grupo_operativo")%>' />
                                                    <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />

                                                    <asp:HiddenField ID="hdnLatViaje" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                    <asp:HiddenField ID="hdnLonViaje" runat="server" Value='<%# Bind("num_longitud")%>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="FAMILIA">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFamilia" runat="server" Text='<%# Bind("nom_familia")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="SERVICIO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("nom_servicio")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CAMIÓN">
                                                <ItemTemplate>
                                                    <asp:Label ID="btnCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>'> </asp:Label>
                                                    <asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" Visible="false" placeholder="Camión..." Text='<%# Bind("nom_pat_camion")%>' Enabled="true" OnTextChanged="CargarDatosCamion" CssClass="form-control"></asp:TextBox>
                                                                                                        <b>
                                                        <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                    <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />

                                                </ItemTemplate>
                                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ARRASTRE">
                                                <ItemTemplate>
                                                    <asp:Label ID="btnArrastre" runat="server" Text='<%# Bind("nom_pat_arrastre")%>'> </asp:Label>

                                                    <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" Visible="false" placeholder="Arrastre..." Text='<%# Bind("nom_pat_arrastre")%>' Enabled="true" OnTextChanged="GPSArrastre" CssClass="form-control"></asp:TextBox>


                                                    <b>
                                                        <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                    <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TRANSPORTISTA">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrasnportista" runat="server" Text='<%# Bind("nom_transportista")%>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CONDUCTOR">
                                                <ItemTemplate>
                                                    <asp:Label ID="btnRutConductor" runat="server" Text='<%# Bind("rut_conductor")%>'> </asp:Label>

                                                    <asp:TextBox ID="txtRutconductor" runat="server" AutoPostBack="true" placeholder="Rut..." Visible="false" OnTextChanged="DoctosConductor" Enabled="true" Text='<%# Bind("rut_conductor")%>' CssClass="form-control"></asp:TextBox>
                                                    /
                                                                    <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                    <asp:Label ID="lblSlash" runat="server" Text="/" />
                                                    <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' />
                                                    <asp:LinkButton ID="btnVerDocConductor" CssClass="small-box-footer text-orange" OnClick="btnVerDocConductor_ClickG" runat="server" Visible="true"><i class="fa fa-info-circle"></i></asp:LinkButton>

                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ORIGEN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="DESTINO">
                                                <ItemTemplate>
                                                    <%--<asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />--%>
                                                    <asp:Label ID="btnDestino" runat="server" Text='<%# Bind("nom_destino")%>'> </asp:Label>

                                                    <%--<asp:DropDownList ID="cboLugar" runat="server" CssClass="form-control" DataTextField="nom_lugar" DataValueField="cod_lugar" />--%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="FECHA ORIGEN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                    /  
                                                                    <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="FECHA DESTINO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                    /
                                                                    <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ESTADÍA ORIGEN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEstadiaOrigen" runat="server" Text='<%# Bind("estadia_origen")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ESTADÍA DESTINO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEstadiaDestino" runat="server" Text='<%# Bind("estadia_destino")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TIEMPO EN RUTA">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTiempoRuta" runat="server" Text='<%# Bind("tiempo_en_ruta")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TIPO VIAJE">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnTipoViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="btnTipoViajeClick" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TOTAL ALERTAS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAlertas" Font-Bold="true" runat="server" Text='<%# Bind("total_alertas")%>' />
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="TOTAL GESTIONADAS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGestionadas" Font-Bold="true" runat="server" Text='<%# Bind("total_gestionadas")%>' />
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ESTADO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_est_viaje")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="POD">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPOD" runat="server" Text='<%# Bind("estado_pod")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>

                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                    </asp:GridView>
                                </div>


                                <div>
                                    <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                                        <div class="modal-dialog modal-lg">
                                            <!-- Modal content-->
                                            <div class="modal-content">

                                                <asp:Panel ID="pnlDoctos" runat="server" Visible="false">
                                                    <div class="modal-header bg-success">
                                                        <h3 class="modal-title text-light">
                                                            <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label><asp:Label ID="lblIdentificador" runat="server" CssClass="text-orange" Text="-"></asp:Label></h3>
                                                        <h3>
                                                            <asp:Label ID="lblTrasnportista" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label></h3>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="DOCUMENTO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("nom_docto") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("fec_vencimiento", "{0:d}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>


                                                <asp:Panel ID="pnlDetalleViaje" runat="server" Visible="false">
                                                    <div class="modal-header bg-warning">
                                                        <h3 class="modal-title text-light">
                                                            <asp:Label ID="Label2" CssClass="text-orange" Font-Bold="true" runat="server" Text="Ordern De Servicio: "></asp:Label><asp:Label ID="lblNumOS" runat="server" CssClass="text-orange" Text="-"></asp:Label></h3>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:GridView ID="grdMultipaso" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="HORA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblhoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ETA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEtaOrigen" runat="server" Text='ETA O.'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="FECHA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>' />

                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="HORA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>' />

                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>



                                                                <asp:TemplateField HeaderText="ETA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEtaDestino" runat="server" Text='ETA D.' />

                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>


                                                <asp:Panel ID="pnlKPI" runat="server" Visible="false">
                                                    <div class="modal-header bg-warning">
                                                        <h3 class="modal-title text-light">
                                                            <asp:Label ID="lblTituloKPI" CssClass="text-orange" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:GridView ID="grdDemandasKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="N° DEMANDA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdDemanda" runat="server" Text='<%# Eval("id_demanda") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CLIENTE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("nom_cliente") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_origen") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA/HORA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFHOrigen" runat="server" Text='<%# Eval("FechaHoraOrigen") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("nom_destino") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA/HORA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFHDestino" runat="server" Text='<%# Eval("FechaHoraDestino") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProducto" runat="server" Text='<%# Eval("nom_producto") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="SERVICIO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>


                                                        <asp:GridView ID="grdDisponiblesKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="CAMIÓN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblcamion" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ARRASTRE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblArrastre" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CONDUCTOR">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("conductor") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="SERVICIO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>

                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>



                                    <!-- Modal CONFIRMACION -->
                                    <div class="modal" id="mdlActualizarGrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">

                                                <div class="modal-body">
                                                    <asp:UpdatePanel runat="server" ID="updClientes">
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnConfirmar" />
                                                        </Triggers>
                                                        <ContentTemplate>
                                                            <div class="modal-header bg-success">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title ">
                                                                    <asp:Label ID="lblTituloActualizar" runat="server" Text=""></asp:Label>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">


                                                                <div class="row justify-content-center">
                                                                    <div class="col-md-12 form-group justify-content-center">

                                                                        <asp:Panel ID="pnlDestinos" runat="server" Visible="false">
                                                                            <asp:DropDownList ID="cboDestinos" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                                        </asp:Panel>

                                                                        <asp:Panel ID="pnlCamion" runat="server" Visible="false">
                                                                            <div class="row">
                                                                                <div class="col-md-4 form-group">
                                                                                    <label for="fname"
                                                                                        class="control-label col-form-label">
                                                                                        Camión</label>
                                                                                    <%--<asp:LinkButton ID="btnVerDocCamion" CssClass="small-box-footer text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>

                                                                                    <div>
                                                                                        <%--<asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" placeholder="Camión..." CssClass="form-control"></asp:TextBox>--%>
                                                                                        <asp:DropDownList ID="cboCamion" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="true"></asp:DropDownList>
                                                                                        <label class="text-info">Estado GPS: </label>
                                                                                        <asp:Image ID="imgGPSCamion" runat="server" Width="15px" Height="15px" Visible="false" />

                                                                                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>


                                                                        <asp:Panel ID="pnlArrastre" runat="server" Visible="false">
                                                                            <div class="row">
                                                                                <div class="col-md-4 form-group">
                                                                                    <label for="fname"
                                                                                        class="control-label col-form-label">
                                                                                        Arrastre</label>
                                                                                    <%--<asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>
                                                                                    <div>
                                                                                        <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." CssClass="form-control" Enabled="true"></asp:TextBox>
                                                                                        <label class="text-info">Estado GPS: </label>
                                                                                        <asp:Image ID="imgGPSArrastre" runat="server" Width="15px" Height="15px" Visible="false" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>



                                                                        <asp:Panel ID="pnlConductor" runat="server" Visible="false">
                                                                            <div class="row">

                                                                                <div class="col-md-3 form-group">
                                                                                    <label for="fname"
                                                                                        class="control-label col-form-label">
                                                                                        RUT</label>
                                                                                    <div>
                                                                                        <asp:TextBox ID="txtRutconductor" runat="server" placeholder="RUT..." AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-6 form-group">
                                                                                    <label for="fname"
                                                                                        class="control-label col-form-label">
                                                                                        Nombre Conductor</label>
                                                                                    <asp:LinkButton ID="btnVerDocConductor" CssClass="text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                                                                    <div>
                                                                                        <asp:TextBox ID="txtNomconductor" runat="server" placeholder="Nombre..." Enabled="false" CssClass="form-control"></asp:TextBox>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 form-group">
                                                                                    <label for="fname"
                                                                                        class="control-label col-form-label">
                                                                                        Fono</label>
                                                                                    <div>
                                                                                        <asp:TextBox ID="txtFono" runat="server" placeholder="Fono..." Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </asp:Panel>

                                                                        <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>


                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12 form-group justify-content-center">
                                                                        <asp:TextBox ID="txtOBS" runat="server" placeHolder="Ingresar motivo..." TextMode="MultiLine" Height="100px" CssClass="form-control" AutoCompleteType="Disabled" required></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>

                                                <div class="modal-footer">

                                                    <asp:Button ID="btnConfirmar" CssClass="btn btn-warning " OnClientClick="$('#modalClientes').modal('hide');" runat="server" Visible="true" Text="ACTUALIZAR" />
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--fin modal--%>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>


            </section>
            <!-- /.content -->


            <asp:HiddenField ID="hdnIdViaje" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

            <asp:HiddenField ID="hdnLatAlerta" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLonAlerta" runat="server" Value="-72.329437" />


            <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnID" runat="server" Value="0" />


            <asp:HiddenField ID="hdnTipoAlerta" runat="server"></asp:HiddenField>



        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

