﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspFichaViaje.aspx.vb" Inherits="aspSistemasJorquera.aspFichaViaje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <script type='text/javascript'>
        function openModalGestiones() {
            $('[id*=mdlGestiones]').modal('show');
        }
    </script>

    <section class="content">
        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="FICHA VIAJE"></asp:Label>

                </h3>
            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnASignar" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx">Volver</asp:LinkButton>
                    <asp:LinkButton ID="btnVolverHR" CssClass="btn btn-primary btn-block" Visible="false  " runat="server" PostBackUrl="/Modulos/TorreControl/HojaDeRuta/Procesos/aspValidarViajeHR.aspx">Volver</asp:LinkButton>
                    <asp:LinkButton ID="btnVolverFac" CssClass="btn btn-primary btn-block" Visible="false" runat="server" PostBackUrl="/Modulos/TorreControl/Facturacion/Procesos/aspValidarViajeFact.aspx">Volver</asp:LinkButton>
                </div>

            </div>
        </div>


        <div class="row">

            <div class="col-md-7">

                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title">Información del Viaje</h3>
                    </div>
                    <div class="box-body">
                        <div class="row btn-sm">

                            <div class="col-xs-6">


                                <label class="col-sm-3 control-label">N° VIAJE:</label>

                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtNumViaje" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-3 control-label">TRANSPORTISTA:</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtTransportista" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ETA</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtEtaOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ORIGEN:</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">DESTINO</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtDestino" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>


                            </div>


                            <div class="col-xs-6">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CAMION:</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtCamion" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ARRASTRE:</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtArrastre" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CONDUCTOR:</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtConductor" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">SCORE</label>

                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtScore" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">RUT CONDUCTOR</label>

                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtRut" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>



                <div style="overflow: auto; height: 350px;">

                    <div class="box box-success ">
                        <div class="box-header with-border bg-success ">
                            <h3 class="box-title ">Alertas</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <asp:GridView ID="grdAlertas" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="ID / N° VIAJE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%# Bind("id_call_center")%>' />
                                                / 
                                                <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("id_viaje")%>' />
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id_call_center")%>' />
                                                <asp:HiddenField ID="hdnIdAlerta" runat="server" Value='<%# Bind("id_alerta")%>' />
                                                <asp:HiddenField ID="hdnIdAlertaBase" runat="server" Value='<%# Bind("id_alerta_base")%>' />
                                                <asp:HiddenField ID="hdnLatitud" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                <asp:HiddenField ID="hdnLongitud" runat="server" Value='<%# Bind("num_longitud")%>' />

                                                <asp:HiddenField ID="hdnNumViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                <asp:HiddenField ID="hdnNomConductor" runat="server" Value='<%# Bind("nom_conductor")%>' />
                                                <asp:HiddenField ID="hdnOrigen" runat="server" Value='<%# Bind("nom_origen")%>' />
                                                <asp:HiddenField ID="hdnDestino" runat="server" Value='<%# Bind("nom_destino")%>' />
                                                <asp:HiddenField ID="hdnCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />

                                                <asp:HiddenField ID="hdnEstAlerta" runat="server" Value='<%# Bind("est_alerta")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="TIPO ALERTA">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipoAlerta" runat="server" Text='<%# Bind("nom_alerta")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="FECHA">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFechaAlerta" runat="server" Text='<%# Bind("fec_alerta")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="PATENTE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("nom_patente")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ESTADO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnGestiones" runat="server" Visible="false" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="verGestionesClick" CssClass="btn btn-primary" ToolTip="Ver Gestiones">
                                    GESTIONES                                  
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>



                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>


                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title ">Incidencias</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <asp:GridView ID="grdIncidencias" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                <Columns>

                                    <%--                                            <asp:TemplateField HeaderText="CAMIÓN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTipoIncidencia" runat="server" Text='<%# Bind("cod_camion")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ARRASTRE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTipoIncidencia" runat="server" Text='<%# Bind("pat_arrastre")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="TIPO INCIDENCIA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoIncidencia" runat="server" Text='<%# Bind("tipo_incidencia")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OBSERVACIONES">
                                        <ItemTemplate>
                                            <asp:Label ID="lblObsIncidencia" runat="server" Text='<%# Bind("obs")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FECHA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecIncidencia" runat="server" Text='<%# Bind("fec_incidencia")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>


                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title">POD</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">

                            <asp:GridView ID="grdPOD" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="NOMBRE ARCHIVO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("url_pod")%>' />
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                            <asp:HiddenField ID="hdnIdTipodocto" runat="server" Value='<%# Bind("id_tipo_docto")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TIPO DOCUMENTO ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoDocto" runat="server" Text='<%# Bind("nom_tipo_docto")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="VALIDADO ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValidado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <h4>
                                                <asp:LinkButton ID="btnDescargar" runat="server" OnClick="DescargarPODClick" ToolTip="Ver POD"><i class="fa fa-download"></i></asp:LinkButton>
                                            </h4>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                                    </asp:TemplateField>



                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>


                </div>


            </div>



            <div class="col-md-5">
                <!-- Horizontal Form -->
                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title">Detalle del Trayecto</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <asp:GridView ID="grdEstados" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="ESTADO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FECHA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fec_add")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OBSERVACIONES">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOBS" runat="server" Text='<%# Bind("obs")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                            </asp:GridView>


                        </div>
                    </div>


                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->
                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title">Mapa</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">

                            <%--<img style="width: 100%; height: 600px;" src=<%=VerImagenMapa()%>>--%>

                            <div id="map" class="table-responsive" style="width: 100%; height: 600px;">
                            </div>

                            <script>                                               
                                
                                var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                                L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                    maxZoom: 18,
                                    attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                        '<a href="https://www.mapbox.com/">Mapbox</a>',
                                    id: 'mapbox/streets-v11',
                                    tileSize: 512,
                                    zoomOffset: -1
                                }).addTo(mymap);

                                L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>").openPopup();
                                                                                                                             

                            </script>
                        </div>
                    </div>


                </div>
                <!-- /.box -->
            </div>

        </div>


        <div>
            <div class="modal fade bd-example-modal-lg" id="mdlGestiones" role="dialog">
                <div class="modal-dialog modal-lg ">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h4 class="modal-title ">
                                <asp:Label ID="lblTituloIncidencias" runat="server" Text=""></asp:Label>
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">
                                &times;</button>
                        </div>
                        <div class="modal-body">


                            <asp:GridView ID="grdGestiones" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                <Columns>

                                    <asp:TemplateField HeaderText="N° NIVEL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNumNivel" runat="server" Text='<%# Bind("nro_nivel")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="NIVEL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNivel" runat="server" Text='<%# Bind("nom_nivel")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="CONTACTO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblContacto" runat="server" Text='<%# Bind("nom_contacto")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GETIONADA POR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserGestion" runat="server" Text='<%# Bind("usr_gestion")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="EXPLICACION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExplicacion" runat="server" Text='<%# Bind("nom_explicacion")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OBS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOBS" runat="server" Text='<%# Bind("obs_gestion")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TIEMPO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTiempo" runat="server" Text='<%# Bind("num_tiempo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FECHA REGISTRO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecRegistro" runat="server" Text='<%# Bind("fec_registro")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                            </asp:GridView>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- /.row -->
    </section>
    <!-- /.content -->

    <asp:HiddenField ID="hdnIdViaje" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnLatOrigen" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnLonOrigen" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
    <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />
    <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />




</asp:Content>

