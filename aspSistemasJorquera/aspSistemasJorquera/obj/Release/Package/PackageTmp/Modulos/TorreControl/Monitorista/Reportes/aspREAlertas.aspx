﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREAlertas.aspx.vb" Inherits="aspSistemasJorquera.aspREAlertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <Triggers>
            <asp:PostBackTrigger ControlID="linDescargar" />

        </Triggers>
        <ContentTemplate>



            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">

                        <div class="row btn-sm">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Categoría de Alerta</label>
                                <div>
                                    <asp:DropDownList runat="server" ID="cboCategorias" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Alerta</label>
                                <div>
                                    <asp:DropDownList ID="cboTipo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Criticidad</label>
                                <div>
                                    <asp:DropDownList ID="cboCriticidad" runat="server" CssClass="form-control" AutoPostBack="true">
                                        <asp:ListItem>0-TODAS</asp:ListItem>
                                        <asp:ListItem>Baja</asp:ListItem>
                                        <asp:ListItem>Media</asp:ListItem>
                                        <asp:ListItem>Alta</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Familia</label>
                                <div>
                                    <asp:DropDownList ID="cboFamilias" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row btn-sm">
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicios" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Transportista</label>
                                <div>
                                    <asp:DropDownList ID="cboTransportista" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Patente</label>
                                <div>
                                    <asp:TextBox ID="txtCamion" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Conductor</label>
                                <div>
                                    <asp:TextBox ID="txtConductor" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Estado</label>
                                <div>
                                    <asp:DropDownList ID="cboEstados" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Propietario</label>
                                <div>
                                    <asp:DropDownList ID="cboPropietario" runat="server" CssClass="form-control" AutoPostBack="true">
                                        <asp:ListItem>TODOS</asp:ListItem>
                                        <asp:ListItem>Propio</asp:ListItem>
                                        <asp:ListItem>Tercero</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row btn-sm">
                            <div class="col-md-12 form-group">

                                <div class="col-sm-2  pull-right">
                                    <asp:LinkButton ID="linDescargar" OnClick="linDescargar_Click" runat="server" Text="Descargar Reporte" CssClass="btn botonesTC-descargar  btn-block"></asp:LinkButton>
                                </div>

                                <div class="col-sm-2  pull-right">
                                    <asp:LinkButton ID="linLimpiar" OnClick="linLimpiar_Click" runat="server" Text="Limpiar Filtros" CssClass="btn btn-primary  btn-block"></asp:LinkButton>
                                </div>

                                <div class="col-sm-2  pull-right">
                                    <asp:LinkButton ID="linCargar" runat="server" OnClick="linCargar_Click" Text="Actualizar Reporte" CssClass="btn btn-primary btn-block"></asp:LinkButton>


                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">

                                    <!-- /.box-header -->



                                    <!-- ./box-body -->
                                    <div class="box-footer">
                                        <div class="row table-responsive">
                                            <div class="col-sm-12 col-xs-6">

                                                <div class="description-block border-right">
                                                    <asp:GridView ID="grdAlertas" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="N° VIAJE" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnNumViaje" runat="server" OnClick="btnNumViaje_Click" CommandArgument="<%# Container.DataItemIndex.ToString() %>" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></>                                                
                                                                            <asp:HiddenField ID="hdnOrigen" runat="server" Value='<%# Bind("nom_origen")%>' />
                                                                    <asp:HiddenField ID="hdnDestino" runat="server" Value='<%# Bind("nom_destino")%>' />
                                                                    <asp:HiddenField ID="hdnCamion" runat="server" Value='<%# Bind("nom_pat_camion")%>' />
                                                                    <asp:HiddenField ID="hdnArrastre" runat="server" Value='<%# Bind("nom_pat_arrastre")%>' />
                                                                    <asp:HiddenField ID="hdnLatitud" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                                    <asp:HiddenField ID="hdnLongitud" runat="server" Value='<%# Bind("num_longitud")%>' />
                                                                    <asp:HiddenField ID="hdnIDCall" runat="server" Value='<%# Bind("id_call_center")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="N° ALERTA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNroAlerta" runat="server" Text='<%# Bind("id_call_center")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="FAMILIA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFamilia" runat="server" Text='<%# Bind("nom_familia")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="SERVICIO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("nom_servicio")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="TRANSPORTISTA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTransportista" runat="server" Text='<%# Bind("transportista")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CONDUCTOR" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_conductor")%>'></asp:Label>
                                                                    /
                                                                            <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("nom_conductor")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CAMIÓN" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="PROPIETARIO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPropietario" runat="server" Text='<%# Bind("nom_propietario")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ALERTA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNombreAlerta" runat="server" Text='<%# Bind("nom_alerta")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <%--<asp:TemplateField HeaderText="ALERTA BASE" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNombreAlertaBase" runat="server" Text='<%# Bind("nom_alerta_base")%>'></asp:Label>                                              
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>--%>

                                                            <asp:TemplateField HeaderText="CATEGORÍA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CRITICIDAD" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCriticidad" runat="server" Text='<%# Bind("cri_alerta")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="FECHA ALERTA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFechaAlerta" runat="server" Text='<%# Bind("fec_alerta")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="FECHA REGISTRO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFechaRegistro" runat="server" Text='<%# Bind("fec_registro")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ESTADO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#FFB738" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </section>
            <!-- /.content -->

            <asp:HiddenField ID="hdnIdViaje" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

            <asp:HiddenField ID="hdnLatAlerta" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLonAlerta" runat="server" Value="-72.329437" />


            <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTipoAlerta" runat="server"></asp:HiddenField>



        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>



</asp:Content>

