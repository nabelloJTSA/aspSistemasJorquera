﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCatRol.aspx.vb" Inherits="aspSistemasJorquera.aspCatRol" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    
    <link href="../../../Content/cssTabs.css" rel="stylesheet" />

    <section class="content-header">
        <h2>
            <asp:Label ID="lbltitulo" runat="server" Text="CATEGORÍAS Y ROLES"></asp:Label>
        </h2>
    </section>

    <section class="content">
        <div class="warpper">
            <input class="radio" id="one" name="group" type="radio" checked>
            <input class="radio" id="two" name="group" type="radio">
            <div class="tabs">
              <label class="tab" id="one-tab" for="one">Categorías</label>
              <label class="tab" id="two-tab" for="two">Roles</label>
            </div>
            <div class="panels">
                <div class="panel" id="one-panel">
                    <div class="panel-title">Categorías de Agrupación</div>
                    <asp:UpdatePanel runat="server" ID="UpdatePanel">
                        <ContentTemplate>
                            <asp:GridView ID="grdDatos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnSeleccionar" OnClick="btnSeleccionarCategoria_Click" CssClass="small-box-footer text-orange" runat="server"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="ID Categoría">
                                            <ItemTemplate>
                                                <asp:Label ID="lnlId" runat="server" Text='<%# Bind("id_categoria")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id_categoria")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nombre Categoría">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminarCategoria" CssClass="small-box-footer text-orange" OnClick="btnEliminarCategoria_Click" runat="server"> <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                </asp:GridView>
                            
                            <div class="panel-title">Nueva Categoría de Agrupación</div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">Nombre de la Categoría</label>
                                    <div>
                                        <asp:TextBox ID="txtNombreCat" runat="server" placeholder="Nombre ..." CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-1 ">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                    </label>
                                    <div>
                                        <asp:Button ID="btnAddCategoria" OnClick="btnAddCategoria_Click" CssClass="btn bg-orange btn-block" runat="server" Text="Guardar"/>
                                    </div>
                                </div>
                            </div>

                            <asp:HiddenField ID="hdnID" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                </div>

                <div class="panel" id="two-panel">
                    <div class="panel-title">Categorías de Agrupación</div>
                    <asp:UpdatePanel runat="server" ID="UpdatePanelRoles">
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="grdDatosRol" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btnSelecRol" OnClick="btnSeleccionarRol_Click" CssClass="small-box-footer text-orange"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ID Rol">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblIdRol" Text='<%# Bind("id_rol")%>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdnIDRol" Value='<%# Bind("id_rol")%>'/>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nombre Rol">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNombreRol" Text='<%# Bind("nom_rol")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btnEliminarRol" OnClick="btnEliminarRol_Click" CssClass="small-box-footer text-orange"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                    </asp:TemplateField>
                                </Columns>     
                                <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />                             
                            </asp:GridView>
                            <div class="panel-title">Nuevo Rol de Escalamiento</div>
                            <div class="row">
                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">Nombre del Rol</label>
                                    <div>
                                        <asp:TextBox runat="server" ID="txtNomRol" placeholder="Nombre ..." CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 ">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                    </label>
                                    <div>
                                        <asp:Button runat="server" ID="btnNewRol" OnClick="btnAddRol_Click" CssClass="btn bg-orange btn-block" Text="Guardar"/>
                                    </div>
                                </div>
                            </div>  
                            <asp:HiddenField ID="hdnActivoRol" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnIDRol" runat="server" Value="0" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                   
                </div>
            </div>    
    </section>

</asp:Content>
