﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspRETurnos.aspx.vb" Inherits="aspSistemasJorquera.aspRETurnos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

              <%-- <Triggers>
            <asp:PostBackTrigger ControlID="btnExportar" />
          
        </Triggers>--%>
        <ContentTemplate>


            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="Turno Operador"></asp:Label>
                </h3>
                <style>
                    .bg-azul {
                        background-color: #367FA9;
                        color: white;
                    }
                </style>
            </section>

            <script lang="javascript" type="text/javascript">
                function CallPrint(strid) {
                    var prtContent = document.getElementById(strid);
                    var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
                    WinPrint.document.write(prtContent.innerHTML);
                    WinPrint.document.close();
                    WinPrint.focus();
                    WinPrint.print();
                }
            </script>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Familia</label>
                        <asp:DropDownList ID="cboFamilia" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Servicio</label>
                        <asp:DropDownList ID="cboServicios" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Transportista</label>
                        <asp:DropDownList ID="cboTransportista" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Nro. de Viaje</label>
                        <asp:DropDownList ID="cboViaje" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tracto</label>
                        <asp:DropDownList ID="cboTracto" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Conductor</label>
                        <asp:DropDownList ID="cboConductor" runat="server" CssClass="form-control" AutoPostBack="false"></asp:DropDownList>
                    </div>
                </div>
                <!-- SELECT2 EXAMPLE -->
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="form-inline" style="text-align: right">
                             <asp:LinkButton ID="linCargar" runat="server" Text="Actualizar Reporte" CssClass="btn btn-primary"></asp:LinkButton>
                            <asp:LinkButton ID="linVUT" runat="server" CssClass="btn btn-primary" OnClick="lnkComentario_Click">Comentarios de Turno</asp:LinkButton>
                            <asp:LinkButton ID="linVConductor" runat="server" CssClass="btn btn-primary" OnClick="lnkGestion_Click">Gestión de Alertas</asp:LinkButton>
                            <asp:LinkButton ID="linDescargar" runat="server" CssClass="btn botonesTC-descargar" OnClientClick="javascript:CallPrint('reporte1');">Descargar Reporte</asp:LinkButton>
                        </div>
                    </div>
                </div>




                <div id="reporte" style="width: 100%; height: 100%">
                    <div class="row">

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdPerformance" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>PERFORMANCE</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPERFFamilias" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdDisponible" runat="server" CssClass="table table-bordered table-hover " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered"><tr><td>DISPONIBILIDAD EQ</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDispTotal" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDispDisp" runat="server" Text='<%# Bind("DISPONIBLE")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDispPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                            
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdCumplimiento" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>CUMPLIMIENTO</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="ASIG" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCumpViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROG" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCumpDemanda" runat="server" Text='<%# Bind("DEMANDA")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCumpPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdOTIF" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>OTIF</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOtifViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ONTIME" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOtifDemanda" runat="server" Text='<%# Bind("ONTIME")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOtifPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                             
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdVueltas" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>VUELTAS POR EQ</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="UT" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVuelViajes" runat="server" Text='<%# Bind("UT")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVuelDemanda" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVuelPorcentaje" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdSobrestadia" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>SOBRESTADÍA</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="CERRADOS" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSobrViajes" runat="server" Text='<%# Bind("CERRADOS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SOBR" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSobrDemanda" runat="server" Text='<%# Bind("SOBRESTADIA")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSobrPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdPOD" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>CIERRE POD</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="CERRADOS" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPODViajes" runat="server" Text='<%# Bind("CERRADOS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPODDemanda" runat="server" Text='<%# Bind("POD")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPODPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>

                    </div>



                    <div class="row">
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdSafety" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>SAFETY & SECURITY</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSAFEFamilias" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdReportabilidad" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>REPORTABILIDAD EQ</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblREPOViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CONECT" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblREPODemanda" runat="server" Text='<%# Bind("CONECTADOS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblREPOPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdAlertas" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>ALERTAS VIAJE</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblALERViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblALERDemanda" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblALERPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdAtendidas" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>ALERTAS ATENDIDAS</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblATENViajes" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GEST" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblATENDemanda" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblATENPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdTiempo" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>TIEMPO ATENCIÓN</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTIEMViajes" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GEST" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTIEMDemanda" runat="server" Text='<%# Bind("TIEMPO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="T° RESP" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTIEMPorcentaje" runat="server" Text='<%# Bind("PROMEDIO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdVelocidad" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>EXCESO VELOCIDAD</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXCEViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXCEDemanda" runat="server" Text='<%# Bind("VELOCIDAD")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEXCEPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdDetenciones" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>DETENCIONES NO AUT</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDETEViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDETEDemanda" runat="server" Text='<%# Bind("DETENCIONES")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDETEPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdRecurso" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>RECURSO</td></tr></table>'
                                CaptionAlign="Top">
                                <Columns>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRECUFamilias" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdVencidos" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>DOCUMENTOS VENC</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOCUEquipos" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="VENC" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOCUVencidos" runat="server" Text='<%# Bind("VENC")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDOCUPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdSinGPS" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SIN GPS</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGPSViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGPSDemanda" runat="server" Text='<%# Bind("NOCONECTADO")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSGPSPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdSinConductor" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SIN COND</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSCONViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SIN COND" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSCONSincon" runat="server" Text='<%# Bind("SINCON")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSCONPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdSiniestrados" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SINIESTRADA</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSINIViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SINIESTRADO" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSINISincon" runat="server" Text='<%# Bind("SINIESTRADOS")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSINIPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                        <div class="col-md-2" style="width: 14%">
                            <asp:GridView ID="grdTaller" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                                Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA TALLER</td></tr></table>'
                                CaptionAlign="Top">

                                <Columns>
                                    <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTALLViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TALLER" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTALLSincon" runat="server" Text='<%# Bind("TALLER")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTALLPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                        </ItemTemplate>
                                        <ItemStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                                

            
                <asp:Panel ID="Panel1" runat="server" HorizontalAlign="justify" Style="display: none; vertical-align:top" CssClass="EstPanel" Visible="True" Height="2000px" Width="700px">
                    <div id="reporte1" class="btn-sm">
                        <table id="Table2" style="vertical-align:top; ">

                            <h2><p><b>REPORTE TURNO OPERADOR</b></p></h2>

                            <tr>
                                <td style="vertical-align:top;">
                                    <label style="font-size:13px">PERMORMANCE</label>
                                    <asp:GridView ID="grdPerformance1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPERFFamilias" Font-Size ="Smaller" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top;">
                                    <label style="font-size:13px" >DISPONIBLES</label>
                                    <asp:GridView ID="grdDisponible1" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDispTotal" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDispDisp" Font-Size ="Smaller" runat="server" Text='<%# Bind("DISPONIBLE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDispPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                            
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top;">
                                    <label style="font-size:13px">CUMPLIMIENTO</label>
                                    <asp:GridView ID="grdCumplimiento1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ASIG" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCumpViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PROG" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCumpDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("DEMANDA")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCumpPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7"  Font-Size="Smaller"/>
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top">
                                    <label style="font-size:13px">OTIF</label>
                                    <asp:GridView ID="grdOTIF1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOtifViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ONTIME" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOtifDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("ONTIME")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOtifPorcentaje"  Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                             
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top">
                                    <label style="font-size:13px">VUELTAS POR EQ</label>
                                    <asp:GridView ID="grdVueltas1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="UT" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVuelViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("UT")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVuelDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVuelPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top" >

                                    <label style="font-size:13px">SOBRESTADÍA</label>
                                    <asp:GridView ID="grdSobrestadia1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="CERRADOS" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSobrViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("CERRADOS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SOBR" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSobrDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("SOBRESTADIA")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSobrPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top" class="auto-style1">
                                    <label style="font-size:13px">CIERRE POD</label>
                                    <asp:GridView ID="grdPOD1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="CERRADOS" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPODViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("CERRADOS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPODDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("POD")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPODPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>
                            </tr>


                            <tr>

                                <td style="vertical-align:top">
                                    <label style="font-size:13px">SAFETY & SECURITY</label>
                                    <asp:GridView ID="grdSafety1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSAFEFamilias" Font-Size ="Smaller" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top" >
                                    <label style="font-size:13px">REPORTABILIDAD EQ</label>
                                    <asp:GridView ID="grdReportabilidad1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblREPOViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CONECT" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblREPODemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("CONECTADOS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblREPOPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top">
                                    <label style="font-size:13px">ALERTAS VIAJE</label>
                                    <asp:GridView ID="grdAlertas1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblALERViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblALERDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblALERPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top">
                                    <label style="font-size:13px">ALERTAS ATENDIDAS</label>
                                    <asp:GridView ID="grdAtendidas1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATENViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GEST" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATENDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="T° RESP" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATENPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top" >

                                    <label style="font-size:13px">TIEMPO ATENCIÓN</label>
                                    <asp:GridView ID="grdTiempo1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL"  ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTIEMViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GEST" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTIEMDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("TIEMPO")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="T° RESP" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTIEMPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PROMEDIO")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top">
                                    <label style="font-size:13px">EXCESO VELOCIDAD</label>
                                    <asp:GridView ID="grdVelocidad1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEXCEViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEXCEDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("VELOCIDAD")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEXCEPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>


                                <td style="vertical-align:top">

                                    <label style="font-size:13px">DETENCIONES NO AUT</label>
                                    <asp:GridView ID="grdDetenciones1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="VIAJES" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDETEViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ALERT" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDETEDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("DETENCIONES")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDETEPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>
                            </tr>


                            <tr>
                                <td style="vertical-align:top">
                                    <label style="font-size:13px">RECURSO</label>
                                    <asp:GridView ID="grdRecursos1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRECUFamilias" Font-Size ="Smaller" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top">
                                    <label style="font-size:13px">DOCUMENTOS VENC</label>
                                    <asp:GridView ID="grdVencidos1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">
                                        
                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDOCUEquipos" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VENC" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDOCUVencidos" Font-Size ="Smaller" runat="server" Text='<%# Bind("VENC")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDOCUPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                                <td style="vertical-align:top">
                                    <label style="font-size:13px">FLOTA SIN GPS</label>
                                    <asp:GridView ID="grdSinGPS1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">

                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSGPSViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSGPSDemanda" Font-Size ="Smaller" runat="server" Text='<%# Bind("NOCONECTADO")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSGPSPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>
                                <td style="vertical-align:top">
                                      <label style="font-size:13px">FLOTA SIN COND</label>
                                    <asp:GridView ID="grdSinConductor1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSCONViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VENC" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSCONSincon" Font-Size ="Smaller" runat="server" Text='<%# Bind("SINCON")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSCONPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>
                                <td style="vertical-align:top">

                                       <label style="font-size:13px">FLOTA SINIESTRADA</label>
                                    <asp:GridView ID="grdSiniestrados1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSINIViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VENC" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSINISincon" Font-Size ="Smaller" runat="server" Text='<%# Bind("SINIESTRADOS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSINIPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>
                                <td style="vertical-align:top">
                                     <label style="font-size:13px">FLOTA TALLER</label>
                                    <asp:GridView ID="grdTaller1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false">


                                        <Columns>
                                            <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTALLViajes" Font-Size ="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VENC" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTALLSincon" Font-Size ="Smaller" runat="server" Text='<%# Bind("TALLER")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTALLPorcentaje" Font-Size ="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </td>

                            </tr>


                        </table>
                    </div>
                </asp:Panel>



                <div class="modal fade" id="modalTurnos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 40%">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" onclick="$('#modalTurnos').modal('hide');" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            <asp:Label ID="Label2" runat="server" Text="Comentarios de Turno"></asp:Label></h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row" style="margin-bottom: 5%">
                                            <div class="col-md-12" style="background-color: white">
                                                <div class="row">
                                                    <div class="col-md-4 form-group">
                                                        <asp:DropDownList ID="cboCategoria" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-8 form-group">
                                                        <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Comentario"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:GridView ID="grdComentarios" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Turno" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTurno" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Categoría" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoria") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("usuario") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Comentario" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("comentario") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Hora" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <asp:Button ID="btnGuardar" runat="server" CssClass="btn bg-green" Text="Agregar comentario" OnClick="btnGuardar_Click" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>




