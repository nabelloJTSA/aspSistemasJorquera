﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPodHR.aspx.vb" Inherits="aspSistemasJorquera.aspPodHR" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMonitorista" runat="server">


    

            <section class="content">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                        </h3>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnASignar" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/HojaDeRuta/Procesos/aspValidarviajeHR.aspx">Volver</asp:LinkButton>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <!-- lft column -->

                    <div class="col-md-12">
                        <!-- Horizontal Form -->

                        <div class="row">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Tipo Documento</label>
                                <div>
                                    <asp:DropDownList ID="cbotipodocto" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Numero Documento</label>
                                <div>
                                    <asp:TextBox ID="txtNumDocto" CssClass="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>


                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Archivo
                                </label>
                                <div>
                                    <asp:FileUpload ID="uplCDocumento" CssClass="form-control botonesTC-descargar" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    <asp:Label ID="Label1" runat="server" ForeColor="Transparent" Text="_"></asp:Label></label>
                                <div>
                                    <asp:Button ID="brnRelacionar" CssClass="btn btn-primary btn-block " runat="server" Text="Guardar" />
                                </div>
                            </div>
                        </div>



                        <div class="row center-block">

                            <div class="col-md-8 form-group">
                                <asp:GridView ID="grdPOD" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="NOMBRE ARCHIVO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("url_pod")%>' />
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnIdTipodocto" runat="server" Value='<%# Bind("id_tipo_docto")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TIPO DOCUMENTO ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipoDocto" runat="server" Text='<%# Bind("nom_tipo_docto")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="N° DOCTO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNumDocto" runat="server" Text='<%# Bind("num_docto")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="VALIDADO ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblValidado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="VALIDAR">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnValidar" runat="server" OnClick="ValidarPODClick" ToolTip="Validar POD"><i class="fa fa-check"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" Width="50px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnDescargar" runat="server" OnClick="DescargarPODClick" ToolTip="Ver POD"><i class="fa fa-download"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnEliminarPOD" runat="server" OnClick="EliminarPODClick" ToolTip="eliminar POD"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>


                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>
                        </div>


                    </div>
                </div>





            </section>


            <asp:HiddenField ID="hdnIdTipoEquipo" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hdnActivoTipoEquipo" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdRelacion" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server" Value="0"></asp:HiddenField>





</asp:Content>

