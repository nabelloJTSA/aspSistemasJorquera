﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCorreos.aspx.vb" Inherits="aspSistemasJorquera.aspCorreos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

        <script type='text/javascript'>
        function openModal() {
            $('[id*=mdlActualizarGrilla]').modal('show');
        }
    </script>

    <section class="content">
        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="Correos "></asp:Label>
                </h3>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-success">
                    <div class="box-header with-border bg-success">
                        <h3 class="box-title">Mantenedor de Correos</h3>
                    </div>

                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Proceso</label>
                                <div>
                                    <asp:DropDownList ID="cboProceso" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>




                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Nombre
                                </label>
                                <div>
                                    <asp:TextBox ID="txtNombreMov" runat="server" AutoPostBack="True" CssClass="form-control" AutoCompleteType="Disabled"/>
                                    <asp:DropDownList ID="cbobuscarnombre" runat="server" AutoPostBack="true" Visible="false" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    RUT
                                </label>
                                <div>
                                    <asp:TextBox ID="txtrut" runat="server" Enabled="false" AutoPostBack="true" CssClass="form-control" AutoCompleteType="Disabled" />
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Correo
                                </label>
                                <div>
                                    <asp:TextBox ID="txtCorreo" runat="server"  CssClass="form-control" AutoCompleteType="Disabled" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:Button ID="btnGuardar" CssClass="btn btn-primary btn-block " runat="server" Text="Guardar" />
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12 form-group">
                                <asp:GridView ID="grdCorreos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_personal")%>' />
                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnIdTipoProceso" runat="server" Value='<%# Bind("id_tipo_proceso")%>' />
                                                <asp:HiddenField ID="hdnRutPersonal" runat="server" Value='<%# Bind("rut_personal")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Correo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCorreo" runat="server" Text='<%# Bind("correo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Proceso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProceso" runat="server" Text='<%# Bind("nom_proceso")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnEditarTipo" runat="server" OnClick="EditarClick"><i class="fa fa-edit"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnEliminar" runat="server" OnClick="EliminarClick"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="30px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


    <asp:HiddenField ID="hdnId" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0"></asp:HiddenField>



    <div class="modal" id="mdlActualizarGrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title ">
                            <asp:Label ID="lblTitModal" runat="server" Text=""></asp:Label>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-md-12 form-group justify-content-center">


                                <label for="fname"
                                    class="control-label col-form-label">
                                    ¿ESTA SEGURO QUE DESEA ELIMINAR ESTE REGISTRO?</label>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <asp:Button ID="btnConfirmar" CssClass="btn btn-primary " OnClientClick="$('#mdlActualizarGrilla').modal('hide');" runat="server" Visible="true" Text="CONFIRMAR" CausesValidation="False" />


                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>



</asp:Content>


