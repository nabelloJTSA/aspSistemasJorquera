﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site.Master" CodeBehind="aspGestionViajes.aspx.vb" Inherits="aspSistemasJorquera.aspGestionViajes" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.15/proj4.js" data-export="true"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd0xT9fMvo_M0PyAbr7EmLSGMoG7Emn_8&callback=initMap&libraries=drawing&v=weekly" defer></script>
    <script src="http://maps.google.com/maps/api/js?libraries=places&region=uk&language=sp&sensor=true"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd0xT9fMvo_M0PyAbr7EmLSGMoG7Emn_8&callback=initAutocomplete&libraries=places&v=weekly" defer></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd0xT9fMvo_M0PyAbr7EmLSGMoG7Emn_8&callback=initMap&libraries=visualization&v=weekly"
        defer></script>


    <script type='text/javascript'>
        function openModal() {
            $('[id*=ModalDocumentos]').modal('show');
        }
    </script>


    <script type='text/javascript'>
        function openModalD() {
            $('[id*=mdlActualizarGrilla]').modal('show');
        }
    </script>


    <script type='text/javascript'>
        function openModalncidencias() {
            $('[id*=mdlIncidencias]').modal('show');
        }
    </script>


    <style type="text/css">
        #global {
            height: 700px;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #global2 {
            height: 400px;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes {
            height: auto;
        }

        .texto {
            padding: 4px;
            background: #fff;
        }
    </style>



    <style>
        /* Style the counter cards */
        /*.card {
            box-shadow: 0 8px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 6px;
            text-align: center;
            background-color: #fff;
            height: 130px;
        }*/

        .notice {
            padding: 10px;
            height: 95px;
            background-color: #fff;
            border-left: 4px solid #7f7f84;
            -webkit-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            -moz-box-shadow: 0 5px 8px -6px rgba(0,0,0,.2);
            box-shadow: 0 12px 10px 0 rgba(0, 0, 0, 0.2);
        }

        .notice-sm {
            padding: 10px;
            font-size: 80%;
        }

        .notice-lg {
            padding: 35px;
            font-size: large;
        }

        .notice-success {
            border-color: #46ad06;
        }

            .notice-success > strong {
                color: #0b8006;
            }

        .notice-info {
            border-color: #45ABCD;
        }

            .notice-info > strong {
                color: #45ABCD;
            }

        .notice-warning {
            border-color: #FEAF20;
        }

            .notice-warning > strong {
                color: #808080;
            }

        .notice-danger {
            border-color: #d73814;
        }

            .notice-danger > strong {
                color: #d73814;
            }
    </style>


    <asp:DropDownList ID="cboEstado" Visible="false" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>


    <ul class="nav nav-tabs bg-success">
        <li>
            <asp:LinkButton ID="btnControlVW" CssClass="bg-green" runat="server"><h5>CONTROL DE ALERTAS</h5></asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnAlertasVW" CssClass="bg-success" runat="server"><h5>ALERTAS</h5></asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnCicloVW" CssClass="bg-success" runat="server"><h5>CICLO LOGISTICO</h5></asp:LinkButton></li>
    </ul>


    <!-- Content Header (Page header) -->

    <section class="content-header">

        <div class="row">
            <div class="col-md-2 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:TextBox ID="txtDesde" Visible="false" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:TextBox ID="txtHasta" Visible="false" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                </div>
            </div>


            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnASignar" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Procesos/aspAsignacion.aspx">Asignación</asp:LinkButton>
                </div>
            </div>



            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Reportes/aspRETurnos.aspx">T. Operador</asp:LinkButton>
                </div>

            </div>


        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-md-12">

                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="vwIngreso" runat="server">


                        <div class="row btn-sm">


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">

                                    <strong><b>DEMANDAS/<br />
                                        VIAJES </b></strong>
                                    <h4>
                                        <asp:Label ID="lblTotDemandas" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label>/<asp:Label ID="lblTotalViajes" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>

                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>ASIGNADOS </strong>
                                    <h3>
                                        <asp:Label ID="lblAsignados" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>
                                    </h3>
                                </div>
                            </div>

                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN ORIGEN</strong>
                                    <h3>
                                        <asp:Label ID="lblEnOrigen" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN RUTA</strong>
                                    <h3>
                                        <asp:Label ID="lblEnRuta" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>



                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN DESTINO</strong>
                                    <h3>
                                        <asp:Label ID="lblEnDestino" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>TERMINADOS</strong>
                                    <h3>
                                        <asp:Label ID="lblTerminados" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">

                                    <strong><b>CON POD/<br />
                                        POD VALIDO</b></strong>
                                    <h4>
                                        <asp:Label ID="lblTermiandosPOD" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label>/
                                        <asp:Label ID="lblTerminadosPODValido" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>

                            <div class="col-md-1 col-sm-6 col-xs-12 ">
                                <div class="notice notice-success ">
                                    <strong><b>
                                        <asp:Label ID="Label4" Font-Size="10px" runat="server" Font-Bold="true" Text="CUMPLIMIENTO VIAJES"></asp:Label>
                                    </b></strong>

                                    <h4>
                                        <asp:Label ID="lblCumplimiento" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>%
                                        <asp:Image ID="imgCumplViajes" ImageUrl="" Width="13px" Height="13px" runat="server" />
                                    </h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>TOTAL ALERTAS</strong>
                                    <h4>
                                        <asp:Label ID="lblTotalAlertas" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>
                                        <asp:Label ID="Label7" Font-Size="11px" runat="server" Font-Bold="true" Text="ALERTAS GESTIONADAS"></asp:Label>
                                    </strong>
                                    <h4>
                                        <asp:Label ID="lblAlertasGestionadas" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>
                                        <asp:Label ID="Label8" Font-Size="11px" runat="server" Font-Bold="true" Text="ALERTAS PENDIENTES"></asp:Label>
                                    </strong>

                                    <h4>
                                        <asp:Label ID="lblAlertasPendientes" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label><asp:Image ID="imgAlertasPendientes" ImageUrl="" Width="13px" Height="13px" runat="server" /></h4>
                                </div>
                            </div>

                            <div class="col-md-1 col-sm-6 col-xs-12 ">
                                <div class="notice notice-success ">
                                    <strong>
                                        <asp:Label ID="Label5" Font-Size="10px" runat="server" Font-Bold="true" Text="CUMPLIMIENTO ALERTAS"></asp:Label>
                                    </strong>
                                    <h4>
                                        <asp:Label ID="lblCumplimientoAlertas" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>%</h4>
                                </div>
                            </div>
                        </div>

                        <br />



                        <div class="row">


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Trasportista</label>
                                <div>
                                    <asp:DropDownList ID="cboTrasportista" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    POD</label>
                                <div>
                                    <asp:DropDownList ID="cboPOD" CssClass="form-control" runat="server" AutoPostBack="true">
                                        <asp:ListItem Value="3">TODOS</asp:ListItem>
                                        <asp:ListItem Value="1">SI</asp:ListItem>
                                        <asp:ListItem Value="0">NO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Origen</label>
                                <div>
                                    <asp:DropDownList ID="cboOrigen" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Destino</label>
                                <div>
                                    <asp:DropDownList ID="cboDestinoKPI" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Alertas</label>
                                <div>
                                    <asp:DropDownList ID="cboAlertas" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Expedición</label>
                                <div>

                                    <asp:TextBox ID="txtExp" CssClass="form-control" runat="server" AutoPostBack="True"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Guia</label>
                                <div>

                                    <asp:TextBox ID="txtGuia" CssClass="form-control" runat="server" AutoPostBack="True"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:LinkButton ID="btnLimpiarFiltroGral" CssClass="btn btn-primary" runat="server">Limpiar Filtros</asp:LinkButton>
                                </div>
                            </div>



                        </div>

                        <div class="row table-responsive">
                            <div class="col-md-12">
                                <div class="box">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-8">

                                                <div id="global2">
                                                    <div id="mensajes">
                                                        <div class="col-md-9 form-group">
                                                            <label for="fname"
                                                                class="control-label col-form-label text-primary">
                                                                ESTADO ALERTAS <asp:Label ID="lbltotRegistrosdAlertas" runat="server" CssClass="text-primary" Text=""></asp:Label> </label>
                                                            <div>
                                                                <asp:DropDownList ID="cboEstadoAlertas" CssClass="form-control" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2 form-group">
                                                            <label for="fname"
                                                                class="control-label col-form-label">
                                                            </label>
                                                            <div>
                                                                <asp:LinkButton ID="btnLimpiarAlertas" CssClass="btn btn-primary " runat="server">Limpiar Filtro</asp:LinkButton>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-1 form-group">
                                                            <label for="fname"
                                                                class="control-label col-form-label">
                                                            </label>
                                                            <div>
                                                                <asp:LinkButton ID="btnExportarAlertas" CssClass="btn botonesTC-descargar btn-block " runat="server"> <i class="fa fa-download"></i> </asp:LinkButton>
                                                            
                                                            </div>
                                                        </div>

                                                             

                                                        <asp:GridView ID="grdAlertas" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                       
                                                            <Columns>
                                                    
                                                                <asp:TemplateField HeaderText="N° VIAJE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("id_viaje")%>' />

                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id_call_center")%>' />
                                                                        <asp:HiddenField ID="hdnNomDescripcion" runat="server" Value='<%# Bind("nom_descripcion")%>' />


                                                                        <asp:HiddenField ID="hdnIdAlerta" runat="server" Value='<%# Bind("id_alerta")%>' />
                                                                        <asp:HiddenField ID="hdnIdAlertaBase" runat="server" Value='<%# Bind("id_alerta_base")%>' />
                                                                        <asp:HiddenField ID="hdnLatitud" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                                        <asp:HiddenField ID="hdnLongitud" runat="server" Value='<%# Bind("num_longitud")%>' />

                                                                        <asp:HiddenField ID="hdnNumViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                                        <asp:HiddenField ID="hdnNomConductor" runat="server" Value='<%# Bind("nom_conductor")%>' />
                                                                        <asp:HiddenField ID="hdnOrigen" runat="server" Value='<%# Bind("nom_origen")%>' />
                                                                        <asp:HiddenField ID="hdnDestino" runat="server" Value='<%# Bind("nom_destino")%>' />
                                                                        <asp:HiddenField ID="hdnCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />
                                                                        <asp:HiddenField ID="hdnScoreViaje" runat="server" Value='<%# Bind("score_viaje")%>' />

                                                                        <asp:HiddenField ID="hdnEstAlerta" runat="server" Value='<%# Bind("est_alerta")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>
                                                                                                                            

                                                                <asp:TemplateField HeaderText="ID ALERTA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIdAlerta" runat="server" Text='<%# Bind("id_call_center")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                   <asp:TemplateField HeaderText="FECHA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFechaAlerta" runat="server" Text='<%# Bind("fec_alerta")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="TIPO ALERTA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTipoAlerta" runat="server" Text='<%# Bind("nom_alerta")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                
                                                                <asp:TemplateField HeaderText="PATENTE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("nom_patente")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CRITICIDAD">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCriticidad" runat="server" Text='<%# Bind("cri_alerta")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="CATEGORIA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>



                                                                <asp:TemplateField HeaderText="ESTADO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEstado" CssClass="text-primary" runat="server" Text='<%# Bind("nom_estado")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnVerMapa" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerMapaAlerta" CssClass="btn btn-primary" ToolTip="Ver Mapa">
                                                      MAPA                                  
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>

                                                                        <asp:LinkButton ID="BTNgESTIONAR" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="GestionarAlerta" CssClass="btn btn-primary" ToolTip="Gestionar">
                                                      GESTIONAR                                  
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- /.col -->
                                            <div class="col-md-4">
                                                <div id="map" class="table-responsive" style="width: 100%; height: 400px;">
                                                </div>

                                                <script>                                               
                                
                                                    var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                                                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                                        maxZoom: 18,
                                                        attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                                            '<a href="https://www.mapbox.com/">Mapbox</a>',
                                                        id: 'mapbox/streets-v11',
                                                        tileSize: 512,
                                                        zoomOffset: -1
                                                    }).addTo(mymap);

                                                    L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>");
                                                </script>
                                            </div>

                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>

                                    <!-- ./box-body -->
                                    <div class="box-footer">

                                        <div class="row">
                                            <div class="col-md-5 form-group  ">
                                                <div>
                                                    <h4>
                                                        <asp:Label ID="Label11" CssClass="text-primary" runat="server" Text="TOTAL REGISTROS: "></asp:Label>
                                                        <b>
                                                            <asp:Label ID="lblTotReg" CssClass="text-primary" runat="server" Text=""></asp:Label></b></h4>
                                                </div>
                                            </div>


                                            <div class="col-md-2 form-group  pull-right">
                                                <div>
                                                    <asp:LinkButton ID="btnExportarViajes" CssClass="btn botonesTC-descargar btn-block " runat="server"> <i class="fa fa-download"></i> Descargar Tabla </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row table-responsive">


                                            <div class="col-sm-12 col-xs-6">

                                                <div id="global">
                                                    <div id="mensajes">
                                                        <div class="description-block border-right">


                                                            <asp:GridView ID="grdViajes" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="N° VIAJE / OS / GUIA">
                                                                        <ItemTemplate>
                                                                            <b>
                                                                                <asp:LinkButton ID="btnTipoViaje" runat="server" OnClick="btnTipoViajeClick" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton></b><br />
                                                                            <b>
                                                                                <asp:LinkButton ID="btnNumViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerFichaViaje" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></b>
                                                                            <br />
                                                                            <b>OS:&nbsp;</b>
                                                                            <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label>
                                                                            <br />
                                                                            <b>GUIA:&nbsp;</b><asp:Label ID="lblNumGuia" Font-Bold="true" runat="server" Text='<%# Bind("num_guia")%>'></asp:Label>
                                                                            <br />
                                                                            <b>SCORE: </b>
                                                                            <asp:Label ID="lblScore" Font-Bold="true" runat="server" Text='<%# Bind("score_viaje")%>'></asp:Label>
                                                                            <br />

                                                                            <asp:Image ID="imgIncidenciasActivas" Visible="false" ImageUrl="" runat="server" />
                                                                             

                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                                            <asp:HiddenField ID="hdnOS" runat="server" Value='<%# Bind("OS")%>' />
                                                                            <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />
                                                                            <asp:HiddenField ID="hdnCodServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                                            <asp:HiddenField ID="hdnEstadoviaje" runat="server" Value='<%# Bind("id_est_viaje")%>' />
                                                                            <asp:HiddenField ID="hdnCodcliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                                            <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                                            <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                                                            <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                                            <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("num_fono")%>' />
                                                                            <asp:HiddenField ID="hdnNomTrasnportista" runat="server" Value='<%# Bind("nom_transportista")%>' />
                                                                            <asp:HiddenField ID="hdnCodDestino" runat="server" Value='<%# Bind("cod_destino")%>' />
                                                                            <asp:HiddenField ID="hdnCodOrigen" runat="server" Value='<%# Bind("cod_origen")%>' />
                                                                            <asp:HiddenField ID="hdnGrupoOperativo" runat="server" Value='<%# Bind("grupo_operativo")%>' />
                                                                            <asp:HiddenField ID="hdnNumeroViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                                            <asp:HiddenField ID="hdnNomCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />
                                                                            <asp:HiddenField ID="hdnNomProducto" runat="server" Value='<%# Bind("nom_producto")%>' />
                                                                            <asp:HiddenField ID="hdnNomServicio" runat="server" Value='<%# Bind("nom_servicio")%>' />
                                                                            <asp:HiddenField ID="hdnNomGrupoOperativo" runat="server" Value='<%# Bind("nom_grupo_operativo")%>' />
                                                                            <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />
                                                                            <asp:HiddenField ID="hdnNumSecuencia" runat="server" Value='<%# Bind("num_secuencia")%>' />
                                                                            <asp:HiddenField ID="hdnLatViaje" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                                            <asp:HiddenField ID="hdnLonViaje" runat="server" Value='<%# Bind("num_longitud")%>' />
                                                                            <asp:HiddenField ID="hdnFecEstLlegadaDestino" runat="server" Value='<%# Bind("fec_destino_estimada")%>' />
                                                                            <asp:HiddenField ID="hdnAlertasPendientes" runat="server" Value='<%# Bind("est_alertas_pendientes")%>' />
                                                                            <asp:HiddenField ID="hdnIncidencias" runat="server" Value='<%# Bind("incidencias")%>' />
                                                                            <asp:HiddenField ID="hdnOnTime" runat="server" Value='<%# Bind("est_ontime")%>' />
                                                                            <asp:HiddenField ID="hdnHorOnTime" runat="server" Value='<%# Bind("hor_ontime")%>' />
                                                                            <asp:HiddenField ID="hdnHorRuta" runat="server" Value='<%# Bind("hor_ruta")%>' />
                                                                            <asp:HiddenField ID="hdnScoreConductor" runat="server" Value='<%# Bind("score_viaje")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ALERTAS">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAlertas" Font-Bold="true" runat="server" />
                                                                            <asp:LinkButton ID="btnVerAlerta" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerAlertas" CssClass="btn btn-primary" Text='<%# Bind("total_alertas")%>' ToolTip="Gestionar Alertas"><h4></h4></asp:LinkButton></>
                                                                       <br />
                                                                            <b>
                                                                                <asp:LinkButton ID="btnVerAlertas" runat="server" Text="VER" OnClick="VerAlertasClick" ToolTip="Ver alertas"></asp:LinkButton></b>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="CAMIÓN">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnVerDocCamion" CssClass="text-green" OnClick="btnVerDocCamion_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="btnCamion" runat="server" OnClick="btnActRecursosCamion" Text='<%# Bind("nom_pat_camion")%>'> </asp:LinkButton></b>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnCamion" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>

                                                                            <asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" Visible="false" placeholder="Camión..." Text='<%# Bind("nom_pat_camion")%>' Enabled="true" OnTextChanged="CargarDatosCamion" CssClass="form-control"></asp:TextBox>

                                                                            <b>
                                                                                <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                                            <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />


                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ARRASTRE">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnVerDocArrastre" CssClass="text-green" OnClick="btnVerDocArrastre_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="btnArrastre" runat="server" OnClick="btnActRecursosArrastre" Text='<%# Bind("nom_pat_arrastre")%>'> </asp:LinkButton></b>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnArrastre" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>

                                                                            <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" Visible="false" placeholder="Arrastre..." Text='<%# Bind("nom_pat_arrastre")%>' Enabled="true" OnTextChanged="GPSArrastre" CssClass="form-control"></asp:TextBox>

                                                                            <b>
                                                                                <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                                            <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />


                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="CONDUCTOR">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="btnVerDocConductor" CssClass="text-green" OnClick="btnVerDocConductor_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="btnRutConductor" runat="server" OnClick="btnActRecursosConductor" Text='<%# Bind("rut_conductor")%>'> </asp:LinkButton></b><br />
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnRutConductor" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                            
                                                                            <asp:TextBox ID="txtRutconductor" runat="server" AutoPostBack="true" placeholder="Rut..." Visible="false" OnTextChanged="DoctosConductor" Enabled="true" Text='<%# Bind("rut_conductor")%>' CssClass="form-control"></asp:TextBox>

                                                                            <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                                            <asp:Label ID="lblSlash" runat="server" Text="/" />
                                                                            <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' /><br />
                                                                            <asp:Label ID="lblTrasnportista" runat="server" Text='<%# Bind("nom_transportista")%>' Font-Bold="true" />

                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ORIGEN/FECHA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                            <br />
                                                                            <b>
                                                                                <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                                <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label></b>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="DESTINO/FECHA">
                                                                        <ItemTemplate>
                                                                    <%--        <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>--%>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="btnDestino" runat="server" Enabled="false" OnClick="btnActDestino" Text='<%# Bind("nom_destino")%>'> </asp:LinkButton></b><br />
                                                                           <%--     </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnDestino" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>--%>
                                                                            <b>
                                                                                <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                                                <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>'></asp:Label></b>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ESTADO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_est_viaje")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ON TIME">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="btnOnTime" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="HORA ON TIME">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblHorOnTime" runat="server" Text='<%# Bind("hor_ontime")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="HORA RUTA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblHorRuta" runat="server" Text='<%# Bind("hor_ruta")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ETA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblETA" runat="server" Text='<%# Bind("tiempo_eta")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="POD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPOD" runat="server" Text='<%# Bind("est_pod")%>'></asp:Label>
                                                                            <h4>
                                                                                <asp:LinkButton ID="btnCargarPOD" runat="server" Visible="true" OnClick="CargarPODClick" ToolTip="Cargar POD"><i class="fa fa-upload"></i></asp:LinkButton></h4>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CIERRE">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEstadoCierre" runat="server" Text='<%# Bind("cierre_manual")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <h4>
                                                                                <asp:LinkButton ID="btnCancelarAsignacion" runat="server" Visible="false" OnClick="CancelarUT" ToolTip="Cancelar Viaje"><i class="fa fa-minus-square"></i></asp:LinkButton>
                                                                            </h4>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <center><h4>
                                                                                <asp:LinkButton ID="btnIncidencias" runat="server" Visible="true" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="AddIndicencias" ToolTip="Ingresar Incidencia"><i class="fa  fa-exclamation-triangle"></i></asp:LinkButton>
                                                                                                                                                              <br />                                                                              
                                                                                
                                                                                 <asp:LinkButton ID="btnCantidadincidencias" runat="server" Text='<%# Bind("incidencias")%>' OnClick="IncidenciasClick" ToolTip="Ver Incidencias"></asp:LinkButton>                                                                                                                                                     
                                                                                
                                                                                  </h4></center>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                  <%--  <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <h4>
                                                                                <asp:LinkButton ID="btnMensaje"  runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" ToolTip=""><i class="fa  fa-envelope"></i></asp:LinkButton>
                                                                            </h4>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>--%>


                                                                    <%-- <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <h4>
                                                                                <asp:UpdatePanel runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="btnBaremos" OnClick="btnBaremos_Click" runat="server" ToolTip=""><i class="fa fa-truck"></i></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnBaremos" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </h4>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>--%>



                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <h4>
                                                                                <asp:LinkButton ID="btnCerrarViaje" CssClass="btn btn-primary" runat="server" ToolTip="Cerrar Viaje" Visible="true" OnClick="CerrarViajeClick">Cerrar Viaje</asp:LinkButton>
                                                                            </h4>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                </Columns>

                                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                            </asp:GridView>
                                                        </div>

                                                        <asp:UpdatePanel runat="server">
                                                            <ContentTemplate>
                                                                <asp:HiddenField runat="server" ID="hdnViajeBaremos" Value="0" />
                                                                <asp:HiddenField runat="server" ID="hdnSecuenciaBaremos" Value="0" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </div>
                                                </div>


                                                <div class="modal" id="mdlBar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <div class="modal-body">
                                                                <div class="modal-header bg-success">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title ">
                                                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                                    </h4>
                                                                </div>

                                                                <div class="modal-body">
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <div class="row justify-content-center">
                                                                                <div class="col-md-12 form-group justify-content-center">
                                                                                    <div class="row">
                                                                                        <div class="col-md-4 form-group">
                                                                                            <label for="fname" class="control-label col-form-label">Valor</label>
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtBValor" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4 form-group">
                                                                                            <label for="fname" class="control-label col-form-label">Tipo de Carga</label>
                                                                                            <div>
                                                                                                <asp:DropDownList ID="cboBCargas" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4 form-group">
                                                                                            <label for="fname" class="control-label col-form-label">Punto de Paso</label>
                                                                                            <div>
                                                                                                <asp:DropDownList ID="cboBPuntoPaso" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <asp:GridView ID="grdBar" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="VIAJE">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblBarViaje" runat="server" Text='<%# Bind("num_viaje")%>' />
                                                                                                        <asp:HiddenField runat="server" ID="hdnIDBaremos" Value='<%# Bind("id")%>' />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="PUNTO DE PASO">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblBarSecuencia" runat="server" Text='<%# Bind("paso")%>' />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="VALOR" ItemStyle-HorizontalAlign="Left">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblBarValor" runat="server" Text='<%# Bind("valor")%>' />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="TIPO DE CARGA">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblBarTipo" runat="server" Text='<%# Bind("tipo_carga")%>' />
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="">
                                                                                                    <ItemTemplate>
                                                                                                        <h4>
                                                                                                            <asp:UpdatePanel runat="server">
                                                                                                                <ContentTemplate>

                                                                                                                    <asp:LinkButton runat="server" ID="btnBBorrar" OnClick="btnBBorrar_Click"><i class="fa fa-trash"></i></asp:LinkButton>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:AsyncPostBackTrigger ControlID="btnBBorrar" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </h4>
                                                                                                    </ItemTemplate>
                                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton runat="server" ID="btnBInsertarBaremos" CssClass="btn btn-primary" Text="GUARDAR" OnClick="btnInsertarBaremos_Click"></asp:LinkButton>
                                                                        <button type="button" class="btn btn-default pull-left" onclick="$('#mdlBar').modal('hide');" data-dismiss="modal">CANCELAR</button>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="btnBInsertarBaremos" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div>
                                                    <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">


                                                                <asp:Panel ID="pnlDoctos" runat="server" Visible="false">
                                                                    <div class="modal-header bg-success">
                                                                        <h3 class="modal-title text-light">
                                                                            <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label><asp:Label ID="lblIdentificador" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                                                        <h3>
                                                                            <asp:Label ID="lblTrasnportista" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label></h3>
                                                                        <button type="button" class="close" data-dismiss="modal">
                                                                            &times;</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="DOCUMENTO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("nom_docto") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("fec_vencimiento", "{0:d}") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>


                                                                <asp:Panel ID="pnlDetalleViaje" runat="server" Visible="false">
                                                                    <div class="modal-header bg-success">
                                                                        <h3 class="modal-title text-light">
                                                                            <asp:Label ID="Label2" CssClass="text-green" Font-Bold="true" runat="server" Text="Ordern De Servicio: "></asp:Label><asp:Label ID="lblNumOS" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                                                        <button type="button" class="close" data-dismiss="modal">
                                                                            &times;</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <asp:GridView ID="grdMultipaso" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                                            <Columns>

                                                                                <asp:TemplateField HeaderText="ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="FECHA ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="HORA ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblhoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>

                                                                                <%--    <asp:TemplateField HeaderText="ETA ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblEtaOrigen" runat="server" Text='ETA O.'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>--%>


                                                                                <asp:TemplateField HeaderText="DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>


                                                                                <asp:TemplateField HeaderText="FECHA DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>' />

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>


                                                                                <asp:TemplateField HeaderText="HORA DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>' />

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>


                                                                                <%--                                                                                <asp:TemplateField HeaderText="ETA DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblEtaDestino" runat="server" Text='ETA D.' />

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>--%>


                                                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>
                                                                    </div>
                                                                </asp:Panel>


                                                                <asp:Panel ID="pnlKPI" runat="server" Visible="false">
                                                                    <div class="modal-header bg-success">
                                                                        <h3 class="modal-title text-light">
                                                                            <asp:Label ID="lblTituloKPI" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                                            <button type="button" class="close" data-dismiss="modal">
                                                                                &times;</button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <asp:GridView ID="grdDemandasKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="N° DEMANDA">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIdDemanda" runat="server" Text='<%# Eval("id_demanda") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="CLIENTE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("nom_cliente") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_origen") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="FECHA/HORA ORIGEN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFHOrigen" runat="server" Text='<%# Eval("FechaHoraOrigen") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("nom_destino") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="FECHA/HORA DESTINO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFHDestino" runat="server" Text='<%# Eval("FechaHoraDestino") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblProducto" runat="server" Text='<%# Eval("nom_producto") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="SERVICIO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>


                                                                        <asp:GridView ID="grdDisponiblesKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="CAMIÓN">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblcamion" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="ARRASTRE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblArrastre" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="CONDUCTOR">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("conductor") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="SERVICIO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>

                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div>
                                                        <div class="modal fade bd-example-modal-lg" id="mdlIncidencias" role="dialog">
                                                            <div class="modal-dialog modal-lg ">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header bg-success">
                                                                        <h4 class="modal-title ">
                                                                            <asp:Label ID="lblTituloIncidencias" runat="server" Text=""></asp:Label>
                                                                        </h4>
                                                                        <button type="button" class="close" data-dismiss="modal">
                                                                            &times;</button>
                                                                    </div>
                                                                    <div class="modal-body">


                                                                        <asp:GridView ID="grdIncidencias" runat="server" Visible="false" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                                            <Columns>

                                                                                <asp:TemplateField HeaderText="TIPO INCIDENCIA">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTipoIncidencia" runat="server" Text='<%# Bind("tipo_incidencia")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="OBSERVACIONES">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblObsIncidencia" runat="server" Text='<%# Bind("obs")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="FECHA">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFecIncidencia" runat="server" Text='<%# Bind("fec_incidencia")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>


                                                                        <asp:GridView ID="grdAlertasPopUp" runat="server" Visible="false" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                                            <Columns>

                                                                                <asp:TemplateField HeaderText="ID / N° VIAJE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("id_call_center")%>' />
                                                                                        / 
                                                                                        <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("id_viaje")%>' />

                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>


                                                                                <asp:TemplateField HeaderText="TIPO ALERTA">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTipoAlerta" runat="server" Text='<%# Bind("nom_alerta")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="FECHA">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFechaAlerta" runat="server" Text='<%# Bind("fec_alerta")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="PATENTE">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("nom_patente")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="ESTADO">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="CATEGORIA / CRITICIDAD">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>' />
                                                                                        / 
                                                                                        <asp:Label ID="lblCriticidad" runat="server" Text='<%# Bind("cri_alerta")%>' />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                                </asp:TemplateField>                                                                             

                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                                        </asp:GridView>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <!-- Modal CONFIRMACION -->
                                                    <div class="modal" id="mdlActualizarGrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">

                                                                <div class="modal-body">
                                                                    <asp:UpdatePanel runat="server" ID="updClientes">
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnConfirmar" />
                                                                        </Triggers>
                                                                        <ContentTemplate>
                                                                            <div class="modal-header bg-success">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span></button>
                                                                                <h4 class="modal-title ">
                                                                                    <asp:Label ID="lblTituloActualizar" runat="server" Text=""></asp:Label>
                                                                                </h4>
                                                                            </div>
                                                                            <div class="modal-body">


                                                                                <div class="row justify-content-center">
                                                                                    <div class="col-md-12 form-group justify-content-center">

                                                                                        <asp:Panel ID="pnlCerrarViaje" runat="server" Visible="false">
                                                                                            <label for="fname"
                                                                                                class="control-label col-form-label">
                                                                                                ¿ESTA SEGURO QUE DESEA CERRAR ESTE VIAJE?</label>
                                                                                        </asp:Panel>

                                                                                        <asp:Panel ID="pnlCancelarViaje" runat="server" Visible="false">
                                                                                            <label for="fname"
                                                                                                class="control-label col-form-label">
                                                                                                ¿ESTA SEGURO QUE DESEA CANCELAR ESTE VIAJE?</label>
                                                                                        </asp:Panel>

                                                                                        <asp:Panel ID="pnlDestinos" runat="server" Visible="false">
                                                                                            <asp:DropDownList ID="cboDestinos" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                                                        </asp:Panel>

                                                                                        <asp:Panel ID="pnlCamion" runat="server" Visible="false">
                                                                                            <div class="row">

                                                                                                <div class="col-md-4 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        Grupo Operativo</label>
                                                                                                    <div>
                                                                                                        <asp:DropDownList ID="cboGO" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="true"></asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-4 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        Camión</label>
                                                                                                    <%--<asp:LinkButton ID="btnVerDocCamion" CssClass="small-box-footer text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>

                                                                                                    <div>
                                                                                                        <%--<asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" placeholder="Camión..." CssClass="form-control"></asp:TextBox>--%>
                                                                                                        <asp:DropDownList ID="cboCamion" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="true"></asp:DropDownList>
                                                                                                        <label class="text-info">Estado GPS: </label>
                                                                                                        <asp:Image ID="imgGPSCamion" runat="server" Width="15px" Height="15px" Visible="false" />

                                                                                                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>

                                                                                            </div>
                                                                                        </asp:Panel>


                                                                                        <asp:Panel ID="pnlArrastre" runat="server" Visible="false">
                                                                                            <div class="row">
                                                                                                <div class="col-md-4 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        Arrastre</label>
                                                                                                    <%--<asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>
                                                                                                    <div>
                                                                                                        <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." CssClass="form-control" Enabled="true"></asp:TextBox>
                                                                                                        <label class="text-info">Estado GPS: </label>
                                                                                                        <asp:Image ID="imgGPSArrastre" runat="server" Width="15px" Height="15px" Visible="false" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>



                                                                                        <asp:Panel ID="pnlConductor" runat="server" Visible="false">
                                                                                            <div class="row">

                                                                                                <div class="col-md-3 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        RUT</label>
                                                                                                    <div>
                                                                                                        <asp:TextBox ID="txtRutconductor" runat="server" placeholder="RUT..." AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-6 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        Nombre Conductor</label>
                                                                                                    <asp:LinkButton ID="btnVerDocConductor" CssClass="text-green" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                                                                                    <div>
                                                                                                        <%--<asp:TextBox ID="txtNomconductor" runat="server" placeholder="Nombre..." Enabled="true" CssClass="form-control"></asp:TextBox>--%>
                                                                                                        <asp:DropDownList ID="cboConductor" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="true"></asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="col-md-3 form-group">
                                                                                                    <label for="fname"
                                                                                                        class="control-label col-form-label">
                                                                                                        Fono</label>
                                                                                                    <div>
                                                                                                        <asp:TextBox ID="txtFono" runat="server" placeholder="Fono..." Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </asp:Panel>

                                                                                        <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>


                                                                                    </div>
                                                                                </div>

                                                                                <div class="row">
                                                                                    <div class="col-md-12 form-group justify-content-center">
                                                                                        <asp:TextBox ID="txtOBS" runat="server" placeHolder="Ingresar motivo..." TextMode="MultiLine" Height="100px" CssClass="form-control" AutoCompleteType="Disabled" required></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>

                                                                <div class="modal-footer">

                                                                    <asp:Button ID="btnConfirmar" CssClass="btn btn-primary " runat="server" Visible="true" Text="CONFIRMAR" />
                                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--fin modal--%>
                                                </div>
                                              
                                                  <asp:Timer ID="TimerViajes" runat="server" Interval="90000"></asp:Timer>
                                                    


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>




                    <asp:View ID="vwAlertas" runat="server">

                        <div class="row btn-sm">
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa  fa-bell "></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">TOTALES</span>
                                        <h4>

                                            <asp:Label ID="lblTotalAlertasAlerta" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                        <%--<asp:LinkButton ID="LinkButton2" CssClass="small-box-footer text-orange" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-orange"></i></asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-check-square-o"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">GESTIONADAS</span>
                                        <h4>
                                            <asp:Label ID="lblGestionadasAlerta" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                        <%--<asp:LinkButton ID="LinkButton3" CssClass="small-box-footer text-orange" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-orange"></i></asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-exclamation-triangle"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">PENDIENTES</span>
                                        <h4>
                                            <asp:Label ID="lblPendientesAlerta" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                        <%--<asp:LinkButton ID="LinkButton1" CssClass="small-box-footer text-orange" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-orange"></i></asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />

                        <div class="row btn-sm">

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesdeAlertas" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHastaAlertas" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientealertas" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>



                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:Button ID="btnLimpiarFiltros" runat="server" CssClass="btn btn-primary" Text="Limpiar Filtros" />
                                </div>

                            </div>
                        </div>



                        <div class="row">

                            <div class="col-md-6">

                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">CRITICIDAD</h3>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div id="divCriticidad" style="width: 100%; height: 250px;"></div>
                                        </div>
                                    </div>
                                </div>


                                <script type="text/javascript">
                                    google.charts.load("current", { packages: ["corechart"] });
                                    google.charts.setOnLoadCallback(drawChart);
                                    function drawChart() {
                                        var data = new google.visualization.arrayToDataTable(<%=GraficoCriticidad()%>);
                                        var options = {
                                            title: 'CRITICIDAD ALERTAS',
                                            backgroundColor: {
                                                fill: '#FFFFFF',
                                                fillOpacity: 0.1
                                            },

                                            slices: {
                                                4: { offset: 0.2 },
                                                12: { offset: 0.3 },
                                                14: { offset: 0.4 },
                                                15: { offset: 4.5 },
                                            },
                                        };
                                        var chart = new google.visualization.PieChart(document.getElementById('divCriticidad'));
                                        chart.draw(data, options);
                                    }
                                </script>


                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">ESTADOS</h3>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div id="divGestionadas" style="width: 100%; height: 250px;"></div>
                                        </div>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    google.charts.load("current", { packages: ["corechart"] });
                                    google.charts.setOnLoadCallback(drawChart);
                                    function drawChart() {
                                        var data = new google.visualization.arrayToDataTable(<%=GraficoGestionadas()%>);
                                        var options = {
                                            title: 'ESTADO ALERTAS',
                                            backgroundColor: {
                                                fill: '#FFFFFF',
                                                fillOpacity: 0.1
                                            },
                                            slices: {
                                                4: { offset: 0.2 },
                                                12: { offset: 0.3 },
                                                14: { offset: 0.4 },
                                                15: { offset: 4.5 },
                                            },
                                        };
                                        var chart = new google.visualization.PieChart(document.getElementById('divGestionadas'));
                                        chart.draw(data, options);
                                    }
                                </script>
                            </div>


                            <div class="col-md-6">
                                <h3>
                                    <asp:Label ID="lblTitAlertasTipo" runat="server" CssClass="text-primary" Font-Bold="true" Text="TOTAL ALERTAS: "></asp:Label>
                                    <asp:Label ID="lblTotalAlertasMesActual" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label></h3>
                                <div class="box box-success">


                                    <div class="box-body">
                                        <div class="form-group">


                                            <br />

                                            <div id="global2">
                                                <div id="mensajes">
                                                    <asp:GridView ID="grdAlertasTipo" runat="server" CssClass="table text-primary" AutoGenerateColumns="false" GridLines="None" CellSpacing="-1">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="TIPO ALERTA">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnTipoAlerta" runat="server" OnClick="InfoTipoAlertaClick" Text='<%# Eval("nom_alerta") %>'></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnIdAlerta" runat="server" Value='<%# Bind("id_alerta")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="TOTAL">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblArrastre" runat="server" Text='<%# Eval("total") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                            </div>

                            <!-- /.col -->
                        </div>


                        <div class="row">
                            <div class="col-md-4">


                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">MAPA DE CALOR</h3>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div id="map2" class="table-responsive" style="width: 100%; height: 430px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <script>
                                    let map, heatmap;

                                    function initMap() {
                                        map = new google.maps.Map(document.getElementById('map2'), {
                                            zoom: 6,
                                            center: { lat: -37.448841, lng: -72.329437 },
                                            mapTypeId: google.maps.MapTypeId.HYBRID,
                                            styles: [{
                                                'featureType': 'all',
                                                'elementType': 'all',
                                                'stylers': [{
                                                    'visibility': 'on'
                                                }]
                                            }, {
                                                "elementType": "labels",
                                                "stylers": [{
                                                    "visibility": "on"
                                                }]
                                            }]
                                        });
                                            
                                        var heatmapData = []                                                                                    
                                        var array = <%=CargarGraficoMapaCalor()%>;

                                        var ContHeat = 0;

                                        for (var i = 0; i < array.length; i++) {
                                            heatmapData[ContHeat] = new google.maps.LatLng(array[i][0], array[i][1]);
                                            ContHeat += 1;                                        
                                        }                                                                              
                                                                                                                                             
                                        var heatmap = new google.maps.visualization.HeatmapLayer({
                                            data: heatmapData
                                        });
                                        heatmap.setMap(map);
                                            
                                        heatmap = new google.maps.visualization.HeatmapLayer({
                                            data: heatmapData(),
                                            map: map
                                        });
                                    }

                                    function changeGradient() {
                                        const gradient = [
                                          "rgba(0, 255, 255, 0)",
                                          "rgba(0, 255, 255, 1)",
                                          "rgba(0, 191, 255, 1)",
                                          "rgba(0, 127, 255, 1)",
                                          "rgba(0, 63, 255, 1)",
                                          "rgba(0, 0, 255, 1)",
                                          "rgba(0, 0, 223, 1)",
                                          "rgba(0, 0, 191, 1)",
                                          "rgba(0, 0, 159, 1)",
                                          "rgba(0, 0, 127, 1)",
                                          "rgba(63, 0, 91, 1)",
                                          "rgba(127, 0, 63, 1)",
                                          "rgba(191, 0, 31, 1)",
                                          "rgba(255, 0, 0, 1)"
                                        ];
                                        heatmap.set("gradient", heatmap.get("gradient") ? null : gradient);
                                    }                                        
                                </script>

                            </div>

                            <br />

                            <div class="col-md-8">

                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">EVOLUTIVO ALERTAS MES ANTETIOR / MES ACTUAL</h3>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <div id="chart_div5" style="width: 100%; height: 380px;"></div>
                                        </div>
                                    </div>
                                </div>




                                <script type="text/javascript">
                                    google.charts.load("current", { packages: ["line"] });
                                    google.charts.setOnLoadCallback(drawChart);
                                    function drawChart() {
                                        var data = new google.visualization.arrayToDataTable(<%=EvolutivoAlertas()%>);

                                        var options = {
                                            chart: {                                                
                                                backgroundColor: {
                                                    fill: '#FFFFFF',
                                                    fillOpacity: 0.7
                                                },
                                                // curveType: 'function',
                                                legend: { position: 'bottom' }                                             
                                               
                                            },
                                        };
                                        var chart = new google.charts.Line(document.getElementById('chart_div5'));
                                        chart.draw(data, google.charts.Line.convertOptions(options));
                                    }
                                </script>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">VIAJES RELACIONADOS: <asp:Label ID="lblTotRegistrosRelacioandos" runat="server" CssClass="text-green" Text=""></asp:Label> </h3>
                                     
                                            <asp:LinkButton ID="btnExportarRelacionados" CssClass="btn botonesTC-descargar pull-right" runat="server"> <i class="fa fa-download"></i> Tabla</asp:LinkButton>
                                        
                                    </div>
                                    </div>

                                    <div class="box-body">
                                        <div class="form-group">
                                            <asp:GridView ID="grdViajesAlertas" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="N° VIAJE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNumviaje" runat="server" Text='<%# Bind("id_viaje")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="CAMIÓN">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="ARRASTRE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("nom_pat_arrastre")%>' />

                                                        </ItemTemplate>
                                                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="CONDUCTOR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="ORIGEN">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="DESTINO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbldestino" runat="server" Text='<%# Bind("nom_destino")%>'></asp:Label>

                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="ESTADO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("estado")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ALERTA">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("nom_alerta")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    
                                                    <asp:TemplateField HeaderText="MAGNITUD">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNumMagnitud" runat="server" Text='<%# Bind("num_magnitud")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                </Columns>

                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>




                            </div>
                       
                    </asp:View>




                    <asp:View ID="vwCiclo" runat="server">


                        <div class="row btn-sm">
                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong><b>VIAJES/<br />
                                        DEMANDAS</b></strong>
                                    <h4>
                                        <asp:Label ID="lblTotalViajesCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label>/<asp:Label ID="lblTotDemandasCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>

                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>ASIGNADOS</strong>
                                    <h3>
                                        <asp:Label ID="lblAsignadosCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>
                                    </h3>
                                </div>
                            </div>

                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN ORIGEN</strong>
                                    <h3>
                                        <asp:Label ID="lblEnOrigenCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN RUTA</strong>
                                    <h3>
                                        <asp:Label ID="lblEnRutaCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>



                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>EN DESTINO</strong>
                                    <h3>
                                        <asp:Label ID="lblEnDestinoCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>TERMINADOS</strong>
                                    <h3>
                                        <asp:Label ID="lblTerminadosCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h3>
                                </div>
                            </div>



                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">

                                    <strong><b>CON POD/<br />
                                        POD VALIDO</b></strong>
                                    <h4>
                                        <asp:Label ID="lblTermiandosPODCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label>/ 
                                        <asp:Label ID="lblTerminadosPODValidoCiclo" ForeColor="#808080" runat="server" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>



                            <div class="col-md-1 col-sm-6 col-xs-12 ">
                                <div class="notice notice-success ">
                                    <strong><b>
                                        <asp:Label ID="Label13" Font-Size="10px" runat="server" Font-Bold="true" Text="CUMPLIMIENTO VIAJES"></asp:Label>
                                    </b></strong>
                                    <h4>
                                        <asp:Label ID="lblCumplimientoCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>%
                                        <asp:Image ID="imgCumplCiclo" ImageUrl="" Width="13px" Height="13px" runat="server" />
                                    </h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>TOTAL ALERTAS</strong>
                                    <h4>
                                        <asp:Label ID="lblTotalAlertasCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>
                                        <asp:Label ID="Label10" Font-Size="11px" runat="server" Font-Bold="true" Text="ALERTAS GESTIONADAS"></asp:Label>
                                    </strong>
                                    <h4>
                                        <asp:Label ID="lblAlertasGestionadasCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label></h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12">
                                <div class="notice notice-success">
                                    <strong>
                                        <asp:Label ID="Label9" Font-Size="11px" runat="server" Font-Bold="true" Text="ALERTAS PENDIENTES"></asp:Label>
                                    </strong>

                                    <h4>
                                        <asp:Label ID="lblAlertasPendientesCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label><asp:Image ID="imgAlertasPendientesCiclo" ImageUrl="" Width="13px" Height="13px" runat="server" /></h4>
                                </div>
                            </div>


                            <div class="col-md-1 col-sm-6 col-xs-12 ">
                                <div class="notice notice-success ">

                                    <strong>
                                        <asp:Label ID="Label14" Font-Size="10px" runat="server" Font-Bold="true" Text="CUMPLIMIENTO ALERTAS"></asp:Label>
                                    </strong>
                                    <h4>
                                        <asp:Label ID="lblCumplimientoAlertasCiclo" runat="server" ForeColor="#808080" Font-Bold="true" Text="0"></asp:Label>%</h4>


                                </div>
                            </div>
                        </div>
                        <br />


                        <div class="row btn-sm">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesdeCiclo" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHastaciclo" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientesCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServiciosCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Trasportista</label>
                                <div>
                                    <asp:DropDownList ID="cboTransportistaCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    POD</label>
                                <div>
                                    <asp:DropDownList ID="cboPODCiclo" CssClass="form-control" runat="server" AutoPostBack="true">
                                        <asp:ListItem Value="3">TODOS</asp:ListItem>
                                        <asp:ListItem Value="1">SI</asp:ListItem>
                                        <asp:ListItem Value="0">NO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Estado Viaje</label>
                                <div>
                                    <asp:DropDownList ID="cboEstadoCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Origen</label>
                                <div>
                                    <asp:DropDownList ID="cboOrigenCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Alertas</label>
                                <div>
                                    <asp:DropDownList ID="cboAlertasCiclo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:LinkButton ID="btnLimpiarFiltrosCiclo" CssClass="btn btn-primary" runat="server">Limpiar Filtros</asp:LinkButton>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue">
                                        <h1>TIEMPO EN ORIGEN</h1>
                                          <h4 class="widget-user-desc">Información General (Horas)</h4>
                                    </div>
                               
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                 <span class="description-text text-primary">PROMEDIO</span>
                                                <div class="description-block">
                                                    <h3> <asp:Label ID="lblPromedioOrigen" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label></h3>
                                                   
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                <span class="description-text text-primary"">MINIMO</span>
                                                <div class="description-block">
                                                     
                                                      <h3><asp:Label ID="lblMinimoOrigen" runat="server" Font-Bold="true" CssClass="text-primary" Text="0"></asp:Label></h3>
                                                   
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                   <span class="description-text text-primary"">MAXIMO</span>
                                                <div class="description-block">                                                    
                                                      <h3><asp:Label ID="lblMaximoOrigen" runat="server" Font-Bold="true" CssClass="text-primary" Text="0"></asp:Label></h3>
                                                 
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>


                             <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue">
                                        <h1>TIEMPO EN RUTA</h1>
                                          <h4 class="widget-user-desc">Información General (Horas)</h4>
                                    </div>
                               
                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                  <span class="description-text text-primary"">PROMEDIO</span>
                                                <div class="description-block">
                                                    <h3>   <asp:Label ID="lblPromedioRuta" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label></h3>
                                                  
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                <span class="description-text text-primary"">MINIMO</span>
                                                <div class="description-block">
                                                       <h3><asp:Label ID="lblMinimoruta" runat="server" Font-Bold="true" CssClass="text-primary" Text="0"></asp:Label></h3>
                                                    
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                  <span class="description-text text-primary"">MAXIMO</span>
                                                <div class="description-block">
                                                <h3><asp:Label ID="lblMaximoRuta" runat="server" Font-Bold="true" CssClass="text-primary" Text="0"></asp:Label></h3>
                                                  
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>



                             <div class="col-md-4">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue">
                                        <h1>TIEMPO EN DESTINO</h1>
                                          <h4 class="widget-user-desc">Información General (Horas)</h4>
                                    </div>
                               
                                    <div class="box-footer">
                                        <div class="row">                                            
                                            <div class="col-sm-4 border-right">
                                                <span class="description-text text-primary"">PROMEDIO</span>
                                                <div class="description-block">                                                    
                                                <h3>  <asp:Label ID="lblPromedioDestino"  runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label></h3>                                                    
                                                </div>
                                                                                                
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                 <span class="description-text text-primary"">MINIMO</span>
                                                <div class="description-block">
                                                      <h3><asp:Label ID="lblMinimoDestino" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label></h3>
                                                   
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                <span class="description-text text-primary"">MAXIMO</span>
                                                <div class="description-block">
                                                       <h3><asp:Label ID="lblMaximoDestino" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label></h3>                                                    
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>
                                </div>
                                <!-- /.widget-user -->
                            </div>




                        </div>




                        <div class="row">
                            <div class="col-md-4 form-group ">
                                <div id="div_origen" style="height: 500px; background: none;"></div>
                            </div>

                            <script type="text/javascript">
                                google.charts.load('current', { 'packages': ['corechart'] });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var data = new google.visualization.arrayToDataTable(<%=GraficoOrigen()%>);

                                    var options = {
                                        title: 'MINIMO / MAXIMO EN ORIGEN MES ACTUAL',
                                        backgroundColor: {
                                            fill: '#FFFFFF',
                                            fillOpacity: 0.1
                                        },
                                        // curveType: 'function',
                                        legend: { position: 'bottom' }
                                    };

                                    var chart = new google.visualization.LineChart(document.getElementById('div_origen'));

                                    chart.draw(data, options);
                                }
                            </script>



                            <div class="col-md-4  form-group ">
                                <div id="div_ruta" style="width: 100%; height: 500px; background: none;"></div>
                            </div>

                            <script type="text/javascript">
                                google.charts.load('current', { 'packages': ['corechart'] });
                                google.charts.setOnLoadCallback(drawChart);

                                function drawChart() {
                                    var data = new google.visualization.arrayToDataTable(<%=GraficoRUTA()%>);

                                    var options = {
                                        title: 'MINIMO / MAXIMO EN RUTA MES ACTUAL',
                                        backgroundColor: {
                                            fill: '#FFFFFF',
                                            fillOpacity: 0.1
                                        },
                                        // curveType: 'function',
                                        legend: { position: 'bottom' }
                                    };

                                    var chart = new google.visualization.LineChart(document.getElementById('div_ruta'));

                                    chart.draw(data, options);
                                }
                            </script>




                            <div class="col-md-4 form-group ">
                                <div id="div_destino" style="width: 100%; height: 500px; background: none;"></div>

                                <script type="text/javascript">
                                    google.charts.load('current', { 'packages': ['corechart'] });
                                    google.charts.setOnLoadCallback(drawChart);

                                    function drawChart() {
                                        var data = new google.visualization.arrayToDataTable(<%=GraficoDESTINO()%>);

                                        var options = {
                                            title: 'MINIMO / MAXIMO EN DESTINO MES ACTUAL',
                                            backgroundColor: {
                                                fill: '#FFFFFF',
                                                fillOpacity: 0.1
                                            },
                                            // curveType: 'function',
                                            legend: { position: 'bottom' }
                                        };

                                        var chart = new google.visualization.LineChart(document.getElementById('div_destino'));

                                        chart.draw(data, options);
                                    }
                                </script>

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-12 form-group table-responsive table-bordered">

                                <asp:GridView ID="grdTiempos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="N° VIAJE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNumviaje" runat="server" Text='<%# Bind("num_viaje")%>' />


                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnOS" runat="server" Value='<%# Bind("OS")%>' />
                                                <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />
                                                <asp:HiddenField ID="hdnCodServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                <asp:HiddenField ID="hdnEstadoviaje" runat="server" Value='<%# Bind("id_est_viaje")%>' />
                                                <asp:HiddenField ID="hdnCodcliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                                <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("num_fono")%>' />
                                                <asp:HiddenField ID="hdnNomTrasnportista" runat="server" Value='<%# Bind("nom_transportista")%>' />
                                                <asp:HiddenField ID="hdnCodDestino" runat="server" Value='<%# Bind("cod_destino")%>' />
                                                <asp:HiddenField ID="hdnCodOrigen" runat="server" Value='<%# Bind("cod_origen")%>' />
                                                <asp:HiddenField ID="hdnGrupoOperativo" runat="server" Value='<%# Bind("grupo_operativo")%>' />
                                                <asp:HiddenField ID="hdnNumeroViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                <asp:HiddenField ID="hdnNomCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />
                                                <asp:HiddenField ID="hdnNomProducto" runat="server" Value='<%# Bind("nom_producto")%>' />
                                                <asp:HiddenField ID="hdnNomServicio" runat="server" Value='<%# Bind("nom_servicio")%>' />
                                                <asp:HiddenField ID="hdnNomGrupoOperativo" runat="server" Value='<%# Bind("nom_grupo_operativo")%>' />
                                                <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />
                                                <asp:HiddenField ID="hdnNumSecuencia" runat="server" Value='<%# Bind("num_secuencia")%>' />
                                                <asp:HiddenField ID="hdnLatViaje" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                <asp:HiddenField ID="hdnLonViaje" runat="server" Value='<%# Bind("num_longitud")%>' />
                                                <asp:HiddenField ID="hdnFecEstLlegadaDestino" runat="server" Value='<%# Bind("fec_destino_estimada")%>' />
                                                <asp:HiddenField ID="hdnAlertasPendientes" runat="server" Value='<%# Bind("est_alertas_pendientes")%>' />

                                                <asp:HiddenField ID="hdnOnTime" runat="server" Value='<%# Bind("est_ontime")%>' />
                                                <asp:HiddenField ID="hdnHorOnTime" runat="server" Value='<%# Bind("hor_ontime")%>' />
                                                <asp:HiddenField ID="hdnHorRuta" runat="server" Value='<%# Bind("hor_ruta")%>' />
                                                <asp:HiddenField ID="hdnScoreConductor" runat="server" Value='<%# Bind("score_conductor")%>' />
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="OS">
                                            <ItemTemplate>
                                                <h5>
                                                    <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label></h5>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CAMIÓN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>' />

                                                <b>
                                                    <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ARRASTRE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("nom_pat_arrastre")%>' />

                                                <b>
                                                    <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />


                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutConductor" runat="server" Text='<%# Bind("rut_conductor")%>' />

                                                /
                                                <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                <asp:Label ID="lblSlash" runat="server" Text="/" />
                                                <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' />

                                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("nom_transportista")%>' Font-Bold="true" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldestino" runat="server" Text='<%# Bind("nom_destino")%>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="FECHA ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                /  
                                                                    <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="FECHA DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                /
                                                 <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="TIPO VIAJE">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnTipoViaje" runat="server" OnClick="btnTipoViajeClick" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ESTADO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_est_viaje")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ON TIME">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnOnTime" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA ON TIME">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHorOnTime" runat="server" Text='<%# Bind("hor_ontime")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA RUTA">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHorRuta" runat="server" Text='<%# Bind("hor_ruta")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TIEMPO ORIGEN">
                                            <ItemTemplate>

                                                <b>
                                                    <label class="text-primary">Minimo</label></b>
                                                <asp:Label ID="lblMinOrigen" runat="server" Text='<%# Bind("min_origen")%>'></asp:Label>
                                                <br />
                                                <b>
                                                    <label class="text-primary">Maximo</label></b>
                                                <asp:Label ID="Label12" runat="server" Text='<%# Bind("max_origen")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TIEMPO RUTA">
                                            <ItemTemplate>
                                                <b>
                                                    <label class="text-primary">Minimo</label></b>
                                                <asp:Label ID="lblMinRuta" runat="server" Text='<%# Bind("min_ruta")%>'></asp:Label>
                                                <br />
                                                <b>
                                                    <label class="text-primary">Maximo</label></b>
                                                <asp:Label ID="lblMaxRuta" runat="server" Text='<%# Bind("max_ruta")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TIEMPO DESTINO">
                                            <ItemTemplate>
                                                <b>
                                                    <label class="text-primary">Minimo</label></b>
                                                <asp:Label ID="lblMinDestino" runat="server" Text='<%# Bind("min_destino")%>'></asp:Label>
                                                <br />
                                                <b>
                                                    <label class="text-primary">Máximo</label></b>
                                                <asp:Label ID="lblMaxDestino" runat="server" Text='<%# Bind("max_destino")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                    </Columns>

                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>

                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </section>
    <!-- /.content -->


    <asp:HiddenField ID="hdnIdViaje" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnCodTrasnportista" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnCodCliente" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnIdDemanda" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
    <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

    <asp:HiddenField ID="hdnLonViaje" runat="server" Value="-37.448841" />
    <asp:HiddenField ID="hdnLatViaje" runat="server" Value="-72.329437" />

    <asp:HiddenField ID="hdnLatAlerta" runat="server" Value="-37.448841" />
    <asp:HiddenField ID="hdnLonAlerta" runat="server" Value="-72.329437" />


    <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
    <asp:HiddenField ID="hdnID" runat="server" Value="0" />


    <asp:HiddenField ID="hdnTipoAlerta" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnTipoAlertaGrillaInformacion" runat="server" Value="0"></asp:HiddenField>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>




</asp:Content>

