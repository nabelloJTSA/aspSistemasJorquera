﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspHomologacionAlertas.aspx.vb" Inherits="aspSistemasJorquera.aspHomologacionAlertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <script type='text/javascript'>
        function openModal() {
            $('[id*=ModalDocumentos]').modal('show');
        }

    </script>

    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text="HOLOMOGACION ALERTAS"></asp:Label>
        </h1>
    </section>


    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <asp:UpdatePanel runat="server" ID="UpdatePanel">
                    <ContentTemplate>

                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Nombre WebFleet</label>
                                <div>
                                    <asp:TextBox ID="txtNombreW" runat="server" placeholder="Nombre ..." CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Nombre TC</label>
                                <div>
                                    <asp:TextBox ID="txtNombreTC" runat="server"  placeholder="Descripcion ..." CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-1 ">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:Button ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server" Text="Guardar" />
                                </div>
                            </div>

                            <div class="col-md-1">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:Button ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server" Text="Cancelar" UseSubmitBehavior="False"/>
                                </div>
                            </div>
                        </div>


                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-12">

                                <asp:GridView ID="grdDatos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnSeleccionar" CssClass="small-box-footer text-orange" OnClick="btnSeleccionar_Click" runat="server"><i class="fa fa-arrow-right"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                        

                                        <asp:TemplateField HeaderText="Alerta WebFleet">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomAlertaW" runat="server" Text='<%# Bind("nom_alerta_webfleet")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Alerta TC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomAlertaTC" runat="server" Text='<%# Bind("nom_alerta_tc")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" CssClass="small-box-footer text-orange" OnClick="EliminarGrilla" runat="server"> <i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>


                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                </asp:GridView>
                            </div>
                        </div>


                        <div class="border-top">
                            <div class="card-body fa-pull-right">
                                <button type="button" id="btnConfirmModal" runat="server" class="btn bg-orange btn-block" visible="false" data-toggle="modal" data-target="#modalClientes">
                                    Confirmar Demanda
                                </button>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal" id="modalClientes" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-md">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <asp:UpdatePanel runat="server" ID="updClientes">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnConfirmar" EventName="Click" />
                                            </Triggers>
                                            <ContentTemplate>


                                                <div class="modal-header bg-orange">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title ">Confirmar Demanda</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>¿ESTA SEGURO QUE DESEA CONFIRMAR ESTA DEMANDA? &hellip;</p>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="modal-footer">

                                        <asp:Button ID="btnConfirmar" CssClass="btn btn-warning " OnClientClick="$('#modalClientes').modal('hide');" runat="server" Visible="false" Text="REGISTRAR DEMANDA" />
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <asp:HiddenField ID="hdnID" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />


                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </section>
    <!-- /.content -->


</asp:Content>



