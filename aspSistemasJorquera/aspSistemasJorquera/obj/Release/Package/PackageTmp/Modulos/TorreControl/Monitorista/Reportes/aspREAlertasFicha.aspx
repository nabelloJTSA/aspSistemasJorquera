﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREAlertasFicha.aspx.vb" Inherits="aspSistemasJorquera.aspREAlertasFicha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <section class="content">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text="DETALLE DE GESTIÓN DE ALERTA"></asp:Label>
                        </h3>
                    </div>
                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnASignar" CssClass="btn bg-orange btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Reportes/aspREAlertas.aspx">Volver</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title">Información del Viaje</h3>
                            </div>
                            <div class="box-body">
                                <div class="row btn-sm">
                                    <div class="col-xs-7">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">N° VIAJE:</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtNumViaje" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">TRANSPORTISTA:</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtTransportista" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="col-sm-3 control-label">ETA ORIGEN</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtEtaOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">ORIGEN:</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">DESTINO</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtDestino" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>                                           
                                    </div>

                                    <div class="col-xs-5">
                                        <div class="">
                                            <label class="col-sm-5 control-label">CAMION:</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtCamion" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">ARRASTRE:</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtArrastre" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">CONDUCTOR:</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtConductor" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">RUT CONDUCTOR</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtRut" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-5 control-label">SCORE</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtScore" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title ">Detalle de Gestión</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group" style="height:300px;overflow: scroll">
                                    <asp:GridView ID="grdGestion" runat="server" CssClass="table table-bordered with-border table-striped  bg-warning  " AutoGenerateColumns="false">
                                        <Columns>

                                            <asp:TemplateField HeaderText="NRO. ESCALAMIENTO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNro" runat="server" Text='<%# Bind("nro_nivel")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ESCALAMIENTO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEscalamiento" runat="server" Text='<%# Bind("nom_nivel")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="CONTACTO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContacto" runat="server" Text='<%# Bind("nom_contacto")%>' />
                                                    /
                                                    <asp:Label ID="lblTelefono" runat="server" Text='<%# Bind("tel_contacto")%>' />
                                                    /
                                                    <asp:Label ID="lblCorreo" runat="server" Text='<%# Bind("cor_contacto")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="EXPLICACIÓN ALERTA">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExplicacion" runat="server" Text='<%# Bind("explicacion")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="OBSERVACIÓN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblObs" runat="server" Text='<%# Bind("obs")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="FECHA GESTIÓN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fec_gestion")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ESTADO GESTIÓN">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("est_gestion")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" />
                                            </asp:TemplateField>
                                          
                                        </Columns>
                                        <HeaderStyle BackColor="#FFB738" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>

                    <!--/.col (left) -->
                    <!-- right column -->
                    <div class="col-md-6">
                        <!-- /.box -->
                        <!-- general form elements disabled -->
                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title">Mapa</h3>
                            </div>
                            <div class="box-body">
                                <div class="form-group">
                                    <div id="map" class="table-responsive" style="width: 100%; height: 564px;">
                                    </div>

                                    <script>                                               
                                
                                        var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                                        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                            maxZoom: 18,
                                            attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                                '<a href="https://www.mapbox.com/">Mapbox</a>',
                                            id: 'mapbox/streets-v11',
                                            tileSize: 512,
                                            zoomOffset: -1
                                        }).addTo(mymap);

                                        L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>").openPopup();
                                                                                                                               

                                    </script>


                                </div>
                            </div>


                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>

                <!-- /.row -->
            </section>
            <!-- /.content -->

            <asp:HiddenField ID="hdnIdViaje" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

             <asp:HiddenField ID="hdnLatOrigen" runat="server"></asp:HiddenField>
             <asp:HiddenField ID="hdnLonOrigen" runat="server"></asp:HiddenField>


            <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />
            <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>

            
            <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnArrastre" runat="server"/>
            <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnAlertaTipo" runat="server" Value=""/>
            <asp:HiddenField ID="hdnAlertaCrit" runat="server" Value="" />
            <asp:HiddenField ID="hdnAlertaEstado" runat="server" Value="" />

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

