﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREIncidencias.aspx.vb" Inherits="aspSistemasJorquera.aspREIncidencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                <Triggers>
            <asp:PostBackTrigger ControlID="linDescargar" />
          
        </Triggers>
        <ContentTemplate>


            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">

                        <div class="row btn-sm">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" OnTextChanged="txtDesde_TextChanged" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" OnTextChanged="txtHasta_TextChanged" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Familia</label>
                                <div>
                                    <asp:DropDownList ID="cboFamilia" OnSelectedIndexChanged="cboFamilia_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicio" OnSelectedIndexChanged="cboServicio_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboCliente" OnSelectedIndexChanged="cboCliente_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Transportista</label>
                                <div>
                                    <asp:DropDownList ID="cboTrasportista" CssClass="form-control" OnSelectedIndexChanged="cboTrasportista_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row btn-sm">

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Tipo de Incidencia</label>
                                <div>
                                    <asp:DropDownList ID="cboTipo" CssClass="form-control" OnSelectedIndexChanged="cboTipo_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label"></label>
                                <div>
                                    <asp:LinkButton ID="linDescargar" OnClick="linDescargar_Click" runat="server" Text="Descargar Tabla" CssClass="btn bg-green"></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">

                                    <!-- /.box-header -->



                                    <!-- ./box-body -->
                                    <div class="box-footer">
                                        <div class="row table-responsive">
                                            <div class="col-sm-12 col-xs-6">

                                                <div class="description-block border-right">
                                                    <asp:GridView ID="grdIncidencias" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="N° VIAJE" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnNumViaje" runat="server" OnClick="btnNumViaje_Click" CommandArgument="<%# Container.DataItemIndex.ToString() %>" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></>                                                
                                                                            <asp:HiddenField ID="hdnIDIncidencia" runat="server" Value='<%# Bind("id_incidencia")%>' />
                                                                    <asp:HiddenField ID="hdnOrigen" runat="server" Value='<%# Bind("nom_origen")%>' />
                                                                    <asp:HiddenField ID="hdnDestino" runat="server" Value='<%# Bind("nom_destino")%>' />
                                                                    <asp:HiddenField ID="hdnCamion" runat="server" Value='<%# Bind("nom_pat_camion")%>' />
                                                                    <asp:HiddenField ID="hdnArrastre" runat="server" Value='<%# Bind("nom_pat_arrastre")%>' />
                                                                    <asp:HiddenField ID="hdnLatitud" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                                    <asp:HiddenField ID="hdnLongitud" runat="server" Value='<%# Bind("num_longitud")%>' />

                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FAMILIA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFamilia" runat="server" Text='<%# Bind("nom_familia")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SERVICIO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("nom_servicios")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CLIENTE" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Bind("nom_cliente")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TRANSPORTISTA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTransportista" runat="server" Text='<%# Bind("transportista")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CAMIÓN" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("nom_pat_camion")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ARRASTRE" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("nom_pat_arrastre")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="CONDUCTOR" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_conductor")%>'></asp:Label>
                                                                    /
                                                                            <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("nom_conductor")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ORIGEN" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="DESTINO" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TIPO INCIDENCIA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tipo_incidencia")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FECHA" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fec_incidencia")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="OBSERVACIÓN" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblObservacion" runat="server" Text='<%# Bind("obs")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPOD" runat="server" Text='<%# Bind("POD")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#FFB738" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </section>
            <!-- /.content -->


            <asp:HiddenField ID="hdnIdViaje" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFechaServicio" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

            <asp:HiddenField ID="hdnLatAlerta" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLonAlerta" runat="server" Value="-72.329437" />


            <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTipoAlerta" runat="server"></asp:HiddenField>



        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>



