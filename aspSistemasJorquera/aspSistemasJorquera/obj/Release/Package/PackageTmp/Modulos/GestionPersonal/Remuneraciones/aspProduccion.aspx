﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspProduccion.aspx.vb" Inherits="aspSistemasJorquera.aspProduccion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Área"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">

        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Sucursal</label>
                <div>
                    <asp:DropDownList ID="cboSucursal" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Tipo</label>
                <div>
                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                        <asp:ListItem Value="0">SELECCIONAR</asp:ListItem>
                        <asp:ListItem Value="M3">M3</asp:ListItem>
                        <asp:ListItem Value="VL">VUELTA</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Monto</label>
                <div>
                    <asp:TextBox ID="txtMonto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-4 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-5">

                <asp:GridView ID="grdProduccion" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="Id.">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("id_produccion")%>' />
                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_produccion")%>' />
                                <asp:HiddenField ID="hdnempresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                <asp:HiddenField ID="hdnsucursal" runat="server" Value='<%# Bind("cod_sucursal")%>' />
                                <asp:HiddenField ID="hdntipo" runat="server" Value='<%# Bind("t1")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Empresa">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sucursal">
                            <ItemTemplate>
                                <asp:Label ID="lblsucursal" runat="server" Text='<%# Bind("Sucursal")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Periodo">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("periodo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Producción">
                            <ItemTemplate>
                                <asp:Label ID="lblProduccion" runat="server" Text='<%# Bind("num_produccion", "{0:0,0}")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tip_produccion")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="20px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>

            </div>

        </div>
    </section>
    <asp:HiddenField ID="hdnProcesar" runat="server" />
    <asp:HiddenField ID="hdnactivo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnID" runat="server" Value="0" />
</asp:Content>

