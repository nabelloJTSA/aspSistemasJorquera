﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspItems.aspx.vb" Inherits="aspSistemasJorquera.aspItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Ingresar Ítems"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:MultiView ID="mtvItems" runat="server">


                    <asp:View ID="View1" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Nombre Item</label>
                                <div>
                                    <asp:TextBox ID="txtNombreItem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Des Item</label>
                                <div>
                                    <asp:TextBox ID="txtDesItem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Imponible</label>
                                <div>
                                    <asp:DropDownList ID="cboImponible" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled">
                                        <asp:ListItem>IMPONIBLE</asp:ListItem>
                                        <asp:ListItem>NO IMPONIBLE</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Tipo</label>
                                <div>
                                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled">
                                        <asp:ListItem>FIJO</asp:ListItem>
                                        <asp:ListItem>VARIABLE</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Ref. Instrumento</label>
                                <div>
                                    <asp:TextBox ID="txtRefInstrumento" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Orden liquidación</label>
                                <div>
                                    <asp:TextBox ID="txtOrden" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">Tributable</label>
                                <div>
                                    <asp:CheckBox ID="chkTributable" runat="server" Text="" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">Reajuste</label>
                                <div>
                                    <asp:CheckBox ID="chkReajuste" runat="server" Text="" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">Horas Extra</label>
                                <div>
                                    <asp:CheckBox ID="chkHHEE" runat="server" Text="" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">Asig. Vacaciones</label>
                                <div>
                                    <asp:CheckBox ID="chkAsigVac" runat="server" Text="" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">IAS</label>
                                <div>
                                    <asp:CheckBox ID="chkIas" runat="server" Text="" />
                                </div>

                            </div>

                            <div class="col-md-1 form-group ">
                                <label for="fname" class="control-label col-form-label">Vac. Proporcionales</label>
                                <div>
                                    <asp:CheckBox ID="chkVacPro" runat="server" Text="" />
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-6 form-group ">
                                <div>
                                </div>
                            </div>
                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>
                        </div>                       

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-10">
                                <asp:Panel ID="pnlDetAnt" runat="server" Enabled="true">

                                    <asp:GridView ID="grdItems" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                                <ItemStyle HorizontalAlign="Center" Width="20px" />
                                            </asp:CommandField>

                                            <asp:TemplateField HeaderText="ID Item">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItem" runat="server" Text='<%# Bind("id_item")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="20px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Empresa">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("empresa")%>' />
                                                    <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_item")%>' />
                                                    <asp:HiddenField ID="hdnCodEmp" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                                    <asp:HiddenField ID="hdnCargo" runat="server" Value='<%# Bind("cod_cargo")%>' />
                                                    <asp:HiddenField ID="hdnImponible" runat="server" Value='<%# Bind("item_imponible")%>' />
                                                    <asp:HiddenField ID="hdnTributable" runat="server" Value='<%# Bind("est_tributable")%>' />
                                                    <asp:HiddenField ID="hdnTipo" runat="server" Value='<%# Bind("item_tipo")%>' />
                                                    <asp:HiddenField ID="hdnReajuste" runat="server" Value='<%# Bind("est_reajuste")%>' />
                                                    <asp:HiddenField ID="hdnHHEE" runat="server" Value='<%# Bind("est_hhee")%>' />
                                                    <asp:HiddenField ID="hdnAsigVac" runat="server" Value='<%# Bind("est_asig_vac")%>' />
                                                    <asp:HiddenField ID="hdnIas" runat="server" Value='<%# Bind("est_ias")%>' />
                                                    <asp:HiddenField ID="hdnVacPro" runat="server" Value='<%# Bind("est_vac_pro")%>' />
                                                    <asp:HiddenField ID="hdnRefIns" runat="server" Value='<%# Bind("ref_instrumento")%>' />
                                                    <asp:HiddenField ID="hdnTramo" runat="server" Value='<%# Bind("tramo")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nom_item")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Descripción Item" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDesItem" runat="server" Text='<%# Bind("des_item")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Orden Liquidación">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrden" runat="server" Text='<%# Bind("num_orden")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="40px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tipo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("item_tipo")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                    </asp:GridView>
                                </asp:Panel>
                            </div>

                        </div>
                    </asp:View>
                </asp:MultiView>

                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdItem" runat="server" Value="0" />
                <asp:HiddenField ID="hdnExisteDetalle" runat="server" Value="0" />

            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                        <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>

</asp:Content>
