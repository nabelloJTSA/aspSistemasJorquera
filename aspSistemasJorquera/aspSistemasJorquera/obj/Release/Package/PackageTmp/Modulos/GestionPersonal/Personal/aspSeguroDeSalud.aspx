﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspSeguroDeSalud.aspx.vb" Inherits="aspSistemasJorquera.aspSeguroDeSalud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnIPersonas" runat="server">Ingresar Personas</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnProcesarSeguro" runat="server">Nomina Seguros</asp:LinkButton>
        </li>
        <%-- <li>
            <asp:LinkButton ID="btnExportarIPersonal" runat="server">Exportar Personal</asp:LinkButton>
        </li>--%>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <asp:MultiView ID="MtvPrincipal" runat="server">
        <asp:View ID="VwIngreso" runat="server">
            <!-- Main content -->
            <section class="content">

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                        <div>
                            <asp:TextBox ID="txtBNombreAgregar" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:Panel ID="PnlRecurso" runat="server" Style="overflow: auto;" Height="270px" Width="100%">
                            <asp:GridView ID="grdAgregar" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>

                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%#Bind("rut_personal")%>' />
                                            <asp:HiddenField ID="hdnCodEmp" runat="server" Value='<%#Bind("cod_empresa")%>' />
                                            <asp:HiddenField ID="hdnIdArea" runat="server" Value='<%#Bind("id_area")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%#Bind("nom_personal")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="220px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Empresa">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpresa" runat="server" Text='<%#Bind("nom_empresa")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seleccionar">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSeleccionar" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo Activo</label>
                        <div>
                            <asp:TextBox ID="lblperiodoactivo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnAgregar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnlimpiar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

              <%--      <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportarIngreso" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>--%>

                </div>
            </section>
        </asp:View>

        <asp:View ID="VwNomina" runat="server">
            <section class="content">

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboempresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Poliza</label>
                        <div>
                            <asp:TextBox ID="txtPoliza" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <label for="fname" class="control-label col-form-label">Valor UF</label>
                        <div>
                            <asp:TextBox ID="txtValUF" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                        <div>
                            <asp:TextBox ID="txtBNombreValores" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnvalidar" CssClass="btn bg-red btn-block" runat="server">Validar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCalcular" CssClass="btn bg-orange btn-block" runat="server">Calcular</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportarP" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-12">

                        <asp:Panel ID="pnlMP" runat="server" Style="overflow: auto;" Height="300px" Width="100%">
                            <asp:GridView ID="grdValoresPersonal" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRutVal" runat="server" Text='<%#Bind("rut_personal")%>' />
                                            <asp:HiddenField ID="hdn_id" runat="server" Value='<%#Bind("id_personal_vc")%>' />
                                            <asp:HiddenField ID="hdnCodEmp" runat="server" Value='<%#Bind("cod_empresa")%>' />
                                            <asp:HiddenField ID="hdnIdArea" runat="server" Value='<%#Bind("id_area")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="110px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombreVal" runat="server" Text='<%#Bind("nom_personal")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="350px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Periodo Seguro">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodoVal" runat="server" Text='<%#Bind("periodo_seguro")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Empresa">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomempresaVal" runat="server" Text='<%#Bind("nom_empresa")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="180px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Valor UF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValUFActualVal" runat="server" Text='<%#Bind("val_UF_actual")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nº Cargas">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNumCargas" runat="server" Width="50px" Text='<%#Bind("cant_cargas")%>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Carga UF">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCostoCargaUF" runat="server" Width="60px" Text='<%#Bind("costo_carga_uf")%>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Carga ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostoCargaPesos" runat="server" Width="80px" Text='<%#Bind("costo_carga_pesos")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nº Cargas Especiales">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNumCargaEspeciales" runat="server" Width="50px" Text='<%#Bind("cant_cargas_especiales")%>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Carga Especial UF">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCostoCargaEspecialUF" runat="server" Width="60px" Text='<%#Bind("costo_carga_especial_uf")%>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Carga Especial ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostoCargaEspecialPesos" runat="server" Width="90px" Text='<%#Bind("costo_carga_especial_pesos")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total Pago Cargas UF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalPagoCargaUFVal" runat="server" Width="90px" Text='<%#Bind("total_pago_cargas_UF")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total Pago Cargas ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotPagoCargaPesosVal" runat="server" Width="90px" Text='<%#Bind("total_pago_cargas_pesos")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Titular UF">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCostoTitularUF" runat="server" Width="60px" Text='<%#Bind("costo_titular_uf")%>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Costo Titular ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostoTitularPesos" runat="server" Width="90px" Text='<%#Bind("costo_titular_pesos")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Total A Pagar ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMontoAPagar" runat="server" Width="90px" Text='<%#Bind("monto_total_a_pagar")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Monto Pagado Titular ($) IVA Incl.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMontoPagadoTitularPesosVal" runat="server" Width="90px" Text='<%#Bind("monto_pagado_titular")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Descuento Trabajador UF">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescuentouftrabajdor" runat="server" Width="90px" Text='<%#Bind("DescuentoTrabajador")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Costo Empresa UF">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCostoEmpresaUF" runat="server" Width="90px" Text='<%#Bind("costo_empresa_uf")%>'> </asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"  Width="90px" />
                                    </asp:TemplateField>--%>

                                    <%--<asp:TemplateField HeaderText="Costo Empresa ($)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCostoempPesosVal" runat="server" Width="90px" Text='<%#Bind("costo_empresa_pesos")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"  Width="90px" />
                                    </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="Monto Pagado Empresa ($)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMontoPagadoEmpresaVal" runat="server" Width="90px" Text='<%#Bind("monto_pagado_empresa")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Retroactivo">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkRetroactivo" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Width="30px" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </asp:Panel>

                    </div>

                </div>

            </section>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnRut" runat="server" />
    <asp:HiddenField ID="hdnCodEmpresaAgregar" runat="server" />
    <asp:HiddenField ID="hdnCodEmpresa" runat="server" />
    <asp:HiddenField ID="hdnIdArea" runat="server" />
    <asp:HiddenField ID="hdnIncluido" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRutIncluido" runat="server" />
    <asp:HiddenField ID="hdnPorEmpresa" runat="server" />
    <asp:HiddenField ID="hdnPorTrabajador" runat="server" />
    <asp:HiddenField ID="hdnValTitular" runat="server" />
    <asp:HiddenField ID="hdnValCarga" runat="server" />
    <asp:HiddenField ID="hdnValCargaEspecial" runat="server" />
    <asp:HiddenField ID="hdnIdValores" runat="server" />
    <asp:HiddenField ID="hdnExiste" runat="server" Value="0" />

    <asp:HiddenField ID="hdnIdVC" runat="server" />
    <asp:HiddenField ID="hdnCostoCargaPesos" runat="server" />
    <asp:HiddenField ID="hdnCostoCargaEspecialPesos" runat="server" />
    <asp:HiddenField ID="hdnTotalPagaCargasUF" runat="server" />
    <asp:HiddenField ID="hdnTotalPagoCargaPesos" runat="server" />
    <asp:HiddenField ID="hdnCostoTitularPesos" runat="server" />
    <asp:HiddenField ID="hdnMontoPagadoTitular" runat="server" />
    <asp:HiddenField ID="hdnMontoPagadoEmpresa" runat="server" />
    <asp:HiddenField ID="hdnTotalAPagar" runat="server" />
    <asp:HiddenField ID="hdnPorEmpresaValores" runat="server" />
    <asp:HiddenField ID="hdnPorTrabajadorValores" runat="server" />
    <asp:HiddenField ID="hdnCodEmpresaValores" runat="server" />
    <asp:HiddenField ID="hdnIdAreaValores" runat="server" />
    <asp:HiddenField ID="hdnCodPeriodoAgregar" runat="server" />
    <asp:HiddenField ID="hdnUFActual" runat="server" />
    <asp:HiddenField ID="hdnCantidadCargas" runat="server" />
    <asp:HiddenField ID="hdnCantidadCargasEsp" runat="server" />

    <asp:HiddenField ID="hdnCArgaUFModificar" runat="server" />
    <asp:HiddenField ID="hdnCArgaEspecialUFModificar" runat="server" />
    <asp:HiddenField ID="hdnCostoTutilarUFModificar" runat="server" />
    <asp:HiddenField ID="hdnChkActivo" runat="server" />
    <asp:HiddenField ID="hdnRetroactivo" runat="server" />

</asp:Content>
