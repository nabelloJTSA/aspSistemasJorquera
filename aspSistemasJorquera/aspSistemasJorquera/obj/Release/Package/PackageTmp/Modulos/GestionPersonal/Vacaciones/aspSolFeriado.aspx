﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspSolFeriado.aspx.vb" Inherits="aspSistemasJorquera.aspSolFeriado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script lang="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();           
        }
    </script>

    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnIngresoFeriado" runat="server">Solicitud Feriado o Permiso</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnHistorial" runat="server">Historial Permisos</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnSaldos" runat="server">Ver Saldos</asp:LinkButton>
        </li>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">

        <asp:MultiView ID="MtvSolicitudFeriado" runat="server">
            <asp:View ID="ViewMovimiento" runat="server">

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">RUT</label>
                        <div>
                            <asp:TextBox ID="txtrut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre</label>
                        <div>
                            <asp:TextBox ID="txtNombreMov" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            <asp:DropDownList ID="cbobuscarnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Ingreso</label>
                        <div>
                            <asp:TextBox ID="txtFecIng" Enabled="False" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Cargo</label>
                        <div>
                            <asp:DropDownList ID="cboCargo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Area</label>
                        <div>
                            <asp:DropDownList ID="cboArea" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:DropDownList>
                            <asp:Label ID="lblFaenaAct" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="10px" Text="Faena Actual: " Visible="False"></asp:Label>
                            <asp:Label ID="lblNomFaenaAct" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="10px" Text="-" Visible="False"></asp:Label>
                        </div>
                    </div>

                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Tipo</label>
                        <div>
                            <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                             <asp:Label ID="lblPermisos" runat="server" Text="Permisos"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Permisos</label>
                        <div>
                            <asp:DropDownList ID="cboPermisos" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:Label ID="lbltextosaldo" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12px" Text="Saldo Periodo: " Visible="False"></asp:Label>
                            <asp:Label ID="lblSaldoTotal" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12px" Text="0" Visible="False"></asp:Label>
                            &nbsp;<asp:Label ID="lblDias" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="12px" Text="(Dias)" Visible="False"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Mail</label>
                        <div>
                            <asp:TextBox ID="txtMail" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                       <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                           <asp:CheckBox ID="CHKPAGADO" runat="server" Visible="False" Text="PAGADO" />
                        </div>
                    </div>
                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Inicio</label>
                        <div>
                            <asp:TextBox ID="dtFechaIniMov" TextMode="Date" AutoPostBack="True" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Termino</label>
                        <div>
                            <asp:TextBox ID="dtFecTerminoMov" TextMode="Date" AutoPostBack="True" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:CheckBox ID="chkMedioDia" runat="server" AutoPostBack="True" Visible="False" />
                            <asp:Label ID="lblMedioDia" runat="server" Text="1/2 dia" Font-Size="Medium" Visible="False"></asp:Label>

                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:CheckBox ID="chkPrestamo" runat="server" Visible="False" />
                            <asp:Label ID="lblSolPre" runat="server" Text="Solicita Prestamo" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:Label ID="lblDiasHabiles1" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="12px" Text="Dias habiles a solicitar: " Visible="False"></asp:Label>
                            <asp:Label ID="lblDH" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="12px" Text="0" Visible="False"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:Label ID="lblnomFeriados" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="12px" Text="Feriados entre fechas: " Visible="False"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:TextBox ID="txtFeriados" runat="server" Enabled="False" Height="34px" TextMode="MultiLine" Visible="False" Width="398px" Font-Bold="True" Font-Italic="True" CssClass="Border border rounded border-info text-info"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-6 form-group">
                        <label for="fname" class="control-label col-form-label">Observacion</label>
                        <div>
                            <asp:TextBox ID="txtOBSMov" runat="server" Height="30px" TextMode="MultiLine" Width="99%" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGrabarMov" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnLimpiarMov" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>
            </asp:View>

            <asp:View ID="ViewHistorial" runat="server">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-6 form-group">
                        <label for="fname" class="control-label col-form-label"></label>
                        <div>
                            
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:TextBox ID="lbltitulo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false" Visible ="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Nº Comprobante:</label>
                        <div>
                            <asp:TextBox ID="lblNumFormulario" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnImp" CssClass="btn btn-warning btn-block" runat="server" OnClientClick="javascript:CallPrint('Salida');"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>


                <div class="row justify-content-center table-responsive">

                    <div class="col-md-12">

                        <asp:GridView ID="grdHistorial" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Tipo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("permiso")%>' />
                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                        <asp:HiddenField ID="hdnIdDiaEsp" runat="server" Value='<%# Bind("id_dia_esp")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Permisos">
                                    <ItemTemplate>
                                        <asp:Label ID="lnlDiaEsp" runat="server" Text='<%# Bind("nom_dia_especial")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Termino">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Estado">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGenerado" runat="server" Text='<%# Bind("est_generado")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="110px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Periodo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("periodo")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nº Comprobante">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNumComprobante" runat="server" Text='<%# Bind("num_comprobante")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Solicitante">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSolPor" runat="server" Text='<%# Bind("usr_add")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnImprimir" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="3" CssClass="btn btn-warning btn-sm" ToolTip="Imprimir"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="1" CssClass="btn btn-warning btn-sm" ToolTip="Imprimir" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>

            </asp:View>
            <asp:View ID="View1" runat="server">
                <asp:GridView ID="grdSaldos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="TIPO">
                            <ItemTemplate>
                                <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("nom_tipo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="250px" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="PERIODO">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("periodo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="110px" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SALDO PERIODO">
                            <ItemTemplate>
                                <asp:Label ID="lblSaldo" runat="server" Text='<%# Bind("num_saldo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </asp:View>
        </asp:MultiView>
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnRut" runat="server" />
        <asp:HiddenField ID="hdnNombre" runat="server" />
        <asp:HiddenField ID="hdnEmpresa" runat="server" />
        <asp:HiddenField ID="hdnFecSol" runat="server" />
        <asp:HiddenField ID="hdnNumComprobante" runat="server" />
        <asp:HiddenField ID="hdnUserAdd" runat="server" />
        <asp:HiddenField ID="hdnMail" runat="server" />
        <asp:HiddenField ID="hdnAccion" runat="server" />
        <asp:HiddenField ID="hdnRutComprobante" runat="server" Value="0" />
        <asp:HiddenField ID="hdnInicio" runat="server" />
        <asp:HiddenField ID="hdnTermino" runat="server" />

    </section>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="hdndiasHabiles" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMailSupervisor" runat="server" />
    <asp:HiddenField ID="hdnTipoPermiso" runat="server" />
    <asp:HiddenField ID="hdnCodFaena" runat="server" />
    <asp:HiddenField ID="hdnPersonal" runat="server" />
    <asp:HiddenField ID="HiddenField2" runat="server" />
    <asp:HiddenField ID="hdnFechaIniAnterior" runat="server" />
    <asp:HiddenField ID="hdnTotalDias" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUltimoPerAsig" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDDAnterior" runat="server" />
    <asp:HiddenField ID="hdnDPFAnterior" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDTAnterior" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSaldoTotalAnterior" runat="server" Value="0" />
    <asp:HiddenField ID="hdnProporcionales" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdPeriodoAño" runat="server" />
    <asp:HiddenField ID="hdnSaldoAntiguo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDT" runat="server" Value="0" />
    <asp:HiddenField ID="hdnVerificar10Dias" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDiasFeriados" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDiferenciaDias" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSaldoReal" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenField3" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenField4" runat="server" />
    <asp:HiddenField ID="hdnAdelantado" runat="server" Value="0" />



    <asp:HiddenField ID="hdnExiste4Rol" runat="server" Value="0" />



    <asp:Panel ID="pnlComprobante" runat="server" Style="display: none" CssClass="EstPanel" Width="750px" Height="600px" Visible="True">
        <div id="Salida">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td rowspan="2" width="33%">
                                    <asp:ImageButton ID="imglogo" runat="server" ImageUrl="" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="33%" style="text-align: center;"><b><font style="font-size: 16px; font-family: Arial;"><asp:label runat="server" ID="lblTituloComprobante"></asp:label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 20px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 20px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Fecha</font></b></td>
                                <td style="padding: 20px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfecha" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 5px 0px 5px 4px"><b><font style="font-size: 11px; font-family: Arial;">N° Comprobante</font></b></td>
                                <td style="padding: 5px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblncomprobante" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial;">En cumplimiento de las disposiciones legales vigentes se deja constancia que don (ña):</font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px; width: 180px; height: 10px;"><b><font style="font-size: 11px; font-family: Arial;">Nombre del trabajador:</font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 400px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnombre" Text=""></asp:Label></font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial;">RUT:</font></b></td>
                                <td style="padding: 5px 20px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblrut" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Hará uso de su feriado desde el:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechai" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Hasta el:</font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechat" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Días Hábiles:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiashabiles" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbldiasprogresivosn" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiasprogresivos" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbltituloperiodo" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodoi" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lblal" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodot" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 40px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnota" Visible="true"></asp:Label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 40px 00px 0px 50px; text-align: center;">
                                    <hr />
                                </td>
                                <td width="70%"></td>
                                <td style="padding: 40px 50px 0px 0px; text-align: center;">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 00px 30px 50px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">EMPLEADOR</font></b></td>
                                <td width="70%" style="text-align: center;"></td>
                                <td style="padding: 0px 50px 30px 0px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">TRABAJADOR</font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
