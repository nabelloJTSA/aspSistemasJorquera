﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspDetalleProduccion.aspx.vb" Inherits="aspSistemasJorquera.aspDetalleProduccion" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
      <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Detalle Producción"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Rut</label>
                <div>
                    <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Nombre</label>
                <div>
                    <asp:Label ID="lblNombre" runat="server"></asp:Label>
                </div>
            </div>
            <div class="col-md-1 form-group">               
                    <label for="fname" class="control-label col-form-label">&nbsp</label>
                    <div>
                        <asp:LinkButton ID="btnGenerar" CssClass="btn bg-red btn-block" runat="server">Generar Reporte</asp:LinkButton>

                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-10">

                        <asp:Panel ID="pnlReporte" runat="server" Style="overflow: auto;" Height="650px" Width="100%" Visible="true">
                            <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana" Visible="true"
                                waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                            </rsreport:ReportViewer>
                        </asp:Panel>
                    </div>
                </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGenerar" />
        </Triggers>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        </section>
</asp:Content>
