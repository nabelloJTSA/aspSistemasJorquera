﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPrevired.aspx.vb" Inherits="aspSistemasJorquera.aspPrevired" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnPrevired" runat="server">Previred</asp:LinkButton>

        </li>
        <li>
            <asp:LinkButton ID="btnActualizar" runat="server">Actualizar Rut</asp:LinkButton>

        </li>
        <li>
            <asp:LinkButton ID="btnMPrevired" runat="server">Cargar Archivo</asp:LinkButton>

        </li>
    </ul>
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Previred"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">

        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Reprocesar</label>
                            <div>
                                <asp:CheckBox ID="chkRepro" runat="server" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnProcesar" CssClass="btn bg-red btn-block" runat="server">Generar</asp:LinkButton>

                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Año</label>
                        <div>
                            <asp:DropDownList ID="cboAnioRut2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Rut</label>
                        <div>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group ">
                        <asp:Label ID="lblNombre" runat="server" Font-Bold="True" CssClass="text-uppercase">&nbsp</asp:Label>
                        <div style="height: 5px; width: 5px" class="container"></div>
                        <div>
                            <asp:LinkButton ID="btnProcesarRut" CssClass="btn bg-red btn-block" runat="server">Actualizar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaCor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Año</label>
                        <div>
                            <asp:DropDownList ID="cboAnioCor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoCor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Reprocesar</label>
                        <div>
                            <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp</label>
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Generar</asp:LinkButton>

                        </div>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
        <asp:HiddenField ID="hdnProcesar" runat="server" />
    </section>
</asp:Content>

