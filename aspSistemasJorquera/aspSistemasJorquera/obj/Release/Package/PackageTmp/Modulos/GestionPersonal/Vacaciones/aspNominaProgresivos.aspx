﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspNominaProgresivos.aspx.vb" Inherits="aspSistemasJorquera.aspNominaProgresivos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Nomina Días Progresivos"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Archivo</label>
                <div>
                    <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" Width="350px" />
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnCargar" CssClass="btn bg-info btn-block" runat="server">Cargar</asp:LinkButton>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>
        </div>

        <asp:GridView ID="grdRuts" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="999" AllowPaging="True">
            <Columns>
                <asp:TemplateField HeaderText="Rut">
                    <ItemTemplate>
                        <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' /></ItemTemplate>
                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Dias">
                    <ItemTemplate>
                        <asp:Label ID="lblDias" runat="server" Text='<%# Bind("dias")%>' /></ItemTemplate>
                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                </asp:TemplateField>
            </Columns>
            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
        </asp:GridView>
    </section>
</asp:Content>
