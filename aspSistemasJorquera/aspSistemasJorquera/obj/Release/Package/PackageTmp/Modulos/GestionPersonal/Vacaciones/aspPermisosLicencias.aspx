﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPermisosLicencias.aspx.vb" Inherits="aspSistemasJorquera.aspPermisosLicencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnGral" runat="server">General</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnHistorico" runat="server">Historico</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btAnulados" runat="server">Anulados</asp:LinkButton></li>

    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Permisos"></asp:Label>
                </h2>
            </div>
        </div>
    </section>


    <section class="content">
        <asp:MultiView ID="mtvReporte" runat="server">
            <asp:View ID="vwReporte" runat="server">

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Inicio</label>
                        <div>
                            <asp:TextBox ID="dtFechaIni" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tipo</label>
                        <div>
                            <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                <asp:ListItem Value="1">Vacaciones</asp:ListItem>
                                <asp:ListItem Value="2">Permisos</asp:ListItem>
                                <asp:ListItem Value="3">Licencias</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnBuscar" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>

                <asp:Panel ID="pnlPermisos" runat="server" Visible="false">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Personal</label>
                            <div>
                                <asp:TextBox ID="txtbNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                            </div>
                        </div>

                    </div>

                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-12">
                            <div>
                                <asp:GridView ID="grdPermisos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Comprobante">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComprobante" runat="server" Text=' <%# Bind("num_comprobante")%>' />
                                                <asp:HiddenField ID="hdn_vigencia" runat="server" Value='<%#Bind("vigencia")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rut Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutPersonal" runat="server" Text='<%# Bind("rut_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomPersonal" runat="server" Text='<%# Bind("nombre_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Ingreso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIngreso" runat="server" Text='<%# Bind("fec_ingreso", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Termino">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFtermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="D.H">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDhabiles" runat="server" Text='<%# Bind("dias_habiles")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="50px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="D.C">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDcorridos" runat="server" Text='<%# Bind("dias_corridos")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="50px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nota">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNota" runat="server" Text='<%# Bind("nom_nota")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per. Ini.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodoIni" runat="server" Text='<%# Bind("fec_inicio_periodo", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per. Ter.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodoTer" runat="server" Text='<%# Bind("fec_termino_periodo", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>              
            </asp:View>

            <asp:View ID="vwHistorico" runat="server">
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Rut</label>
                        <div>
                            <asp:TextBox ID="txtRutHis" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tipo</label>
                        <div>
                            <asp:DropDownList ID="cboTipoHis" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                <asp:ListItem Value="1">Vacaciones</asp:ListItem>
                                <asp:ListItem Value="2">Permisos</asp:ListItem>
                                <asp:ListItem Value="3">Licencias</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnBuscarHistorico" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnExportarHistorico" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlHis" runat="server" Visible="false">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-12">
                            <div>
                                <asp:GridView ID="grdHistorico" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Comprobante">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComprobante" runat="server" Text=' <%# Bind("num_comprobante")%>' />
                                                <asp:HiddenField ID="hdn_vigencia" runat="server" Value='<%#Bind("vigencia")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rut Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutPersonal" runat="server" Text='<%# Bind("rut_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomPersonal" runat="server" Text='<%# Bind("nombre_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Ingreso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIngreso" runat="server" Text='<%# Bind("fec_ingreso", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Termino">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFtermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="D.H">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDhabiles" runat="server" Text='<%# Bind("dias_habiles")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="50px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="D.C">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDcorridos" runat="server" Text='<%# Bind("dias_corridos")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="50px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nota">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNota" runat="server" Text='<%# Bind("nom_nota")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Glosa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGlosa" runat="server" Text='<%# Bind("dia_esp")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per. Ini.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodoIni" runat="server" Text='<%# Bind("fec_inicio_periodo", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Per. Ter.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodoTer" runat="server" Text='<%# Bind("fec_termino_periodo", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="VwAnulados" runat="server">

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaAnulados" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Inicio</label>
                        <div>
                            <asp:TextBox ID="TxtFecAnulados" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Rut</label>
                        <div>
                            <asp:TextBox ID="txtRutAnulado" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnBuscarAnulado" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnExportarAnulado" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>

             
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-12">
                            <div>
                                <asp:GridView ID="grdAnulados" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                       
                                        <asp:TemplateField HeaderText="Rut Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutPersonal" runat="server" Text='<%# Bind("rut_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre Personal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomPersonal" runat="server" Text='<%# Bind("nombre_personal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Tipo Permiso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComprobante" runat="server" Text=' <%# Bind("TipoPermiso")%>' />                                              
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Ingreso">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIngreso" runat="server" Text='<%# Bind("fec_ingreso", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Inicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Termino">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFtermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                       

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                

            </asp:View>
        </asp:MultiView>
    </section>
    <asp:HiddenField ID="hdnId" runat="server" />
</asp:Content>

