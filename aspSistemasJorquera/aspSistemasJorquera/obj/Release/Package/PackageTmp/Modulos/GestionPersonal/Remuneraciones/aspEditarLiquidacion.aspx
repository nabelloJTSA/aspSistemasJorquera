﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspEditarLiquidacion.aspx.vb" Inherits="aspSistemasJorquera.aspEditarLiquidacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Editar Liquidación"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" ></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                            <div>
                                <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <asp:Label ID="lblNom" runat="server" Text="Nombre:" Visible="False" Font-Bold="true">&nbsp;</asp:Label>
                               <label for="fname" class="control-label col-form-label">&nbsp;</label>
                            <div>
                                <asp:Label ID="lblNombre" runat="server" Font-Bold="True" CssClass="text-uppercase"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <asp:Label ID="lblEmp" runat="server" Text="Empresa:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Empresa:</label>--%>
                            <div>
                                <asp:Label ID="lblEmpresa" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblSuc" runat="server" Text="Sucursal:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Sucursal:</label>--%>
                            <div>
                                <asp:Label ID="lblSucursal" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblCar" runat="server" Text="Cargo:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Cargo:</label>--%>
                            <div>
                                <asp:Label ID="lblCargo" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblIC" runat="server" Text="I.C" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Cargo:</label>--%>
                            <div>
                                <asp:Label ID="lblInstrumento" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblSB" runat="server" Text="Sueldo Base ($):" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Empresa:</label>--%>
                            <div>
                                <asp:Label ID="lblSueldoBase" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblMA" runat="server" Text="Monto Anticipo ($):" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Sucursal:</label>--%>
                            <div>
                                <asp:Label ID="lblAncipo" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlDet" runat="server" Visible="false">
                        <ul class="nav nav-tabs bg-info ">
                            <li>
                                <asp:LinkButton ID="btnRef" runat="server">Haberes</asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="btnEERRGeneral" runat="server">Otros Descuentos</asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="LinkButton1" runat="server">Descuentos Legales</asp:LinkButton>
                            </li>
                        </ul>
                        <asp:MultiView ID="MultiView1" runat="server">
                            <asp:View ID="View1" runat="server">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-5">

                                        <asp:GridView ID="grdHaberes" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nom_item")%>' /><asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_det_liquidacion")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMonto" runat="server" CssClass="Border border rounded border-info text-info text-uppercase" Text='<%# Bind("val_total", "{0:####,###,###,###,##.##}")%>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row justify-content-center table-responsive">
                                    <div class="col-md-4 form-group ">
                                        <div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-5">

                                        <asp:GridView ID="grdDescuentos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("des_descuento")%>' />
                                                        <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_descuento")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMonto" runat="server" CssClass="Border border rounded border-info text-info text-uppercase" Text='<%# Bind("num_valor", "{0:####,###,###,###,##.##}")%>'>
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row justify-content-center table-responsive">
                                    <div class="col-md-4 form-group ">
                                        <div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="btnGuardarOtro" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="btnCancelarOtro" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-5">

                                        <asp:GridView ID="grdLegales" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="lblNomItem" Width="300px" runat="server" Text='<%# Bind("des_descuento")%>' />
                                                        <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_descuento")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="% Descuento">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="lblPorDesc" Width="100px" runat="server" Text='<%# Bind("por_descuento")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMonto" runat="server" Text='<%# Bind("num_descuento", "{0:####,###,###,###,##.##}")%>'>
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row justify-content-center table-responsive">
                                    <div class="col-md-4 form-group ">
                                        <div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="Button1" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-1 form-group ">
                                        <div>
                                            <asp:LinkButton ID="Button2" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                    </asp:Panel>
                </asp:Panel>
                <asp:HiddenField ID="hdnDatosFijos" runat="server" Value="0" />
                <asp:HiddenField ID="hdnExisteDet" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdPlantilla" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodEmp" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodCargo" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodSucursal" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodInstrumento" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstValor" runat="server" Value="0" />
                <asp:HiddenField ID="hdnNumValor" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstFormula" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstSistema" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstIncluir" runat="server" Value="0" />
                <asp:HiddenField ID="hdnSueldoBase" runat="server" Value="0" />
                <asp:HiddenField ID="hdnTipoPlantilla" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                        <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>
</asp:Content>
