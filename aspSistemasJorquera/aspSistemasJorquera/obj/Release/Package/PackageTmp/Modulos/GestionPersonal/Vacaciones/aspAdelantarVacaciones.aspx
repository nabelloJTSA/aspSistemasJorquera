﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAdelantarVacaciones.aspx.vb" Inherits="aspSistemasJorquera.aspAdelantarVacaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnAdelantar" runat="server">Adelantar Vacaciones</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnSAldos" runat="server">Saldos Vacaciones</asp:LinkButton></li>

    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <asp:MultiView ID="mtvVac" runat="server">
        <asp:View ID="vwAdelantar" runat="server">
            <!-- Main content -->
            <section class="content">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Personal</label>
                        <div>
                            <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            <asp:DropDownList ID="cbobuscarnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="bntBuscarNombreHorasExtra" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">RUT</label>
                        <div>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Ingreso</label>
                        <div>
                            <asp:TextBox ID="txtFecIngreso" runat="server" Enabled="False" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                    </div>


                </div>

                <div class="row justify-content-center table-responsive">


                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tipo</label>
                        <div>
                            <asp:DropDownList ID="cboTipoDias" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoOtros" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>

                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:TextBox ID="lblSaldoAct" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Dias Adelantar</label>
                        <div>
                            <asp:TextBox ID="txtDiasAdd" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Motivo</label>
                        <div>
                            <asp:TextBox ID="txtMotivo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Width="100%" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardarEsp" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <%--<div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>--%>


                </div>
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:Panel ID="pnlDatos" runat="server" Visible="false">
                            <asp:Panel ID="pnlBusqueda" runat="server" Visible="false">
                                <asp:GridView ID="grdDias" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Periodo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("nom_periodo")%>' />
                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnIdPeriodo" runat="server" Value='<%# Bind("id_periodo_vac")%>' />
                                                <asp:HiddenField ID="hdnEstado" runat="server" Value='<%# Bind("est_descontado")%>' />
                                                <asp:HiddenField ID="hdnIdSolicitud" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dias Adelantados">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDiasAd" runat="server" Text='<%# Bind("num_dias")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Motivo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMotivo" runat="server" Text='<%# Bind("motivo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="450px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("estado")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" Width="18px" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" runat="server" ToolTip="Eliminar" OnClientClick="return confirm('Esta seguro (a) que desea Eliminar este Registro');"> <i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </asp:Panel>
                        </asp:Panel>
                    </div>
                </div>
            </section>
        </asp:View>

        <asp:View ID="vwSaldos" runat="server">
            <section class="content">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Personal</label>
                        <div>
                            <asp:TextBox ID="txtnombreS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            <asp:DropDownList ID="cboBuscarNombreS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="bntBuscarNombreS" runat="server" CssClass="btn "><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">RUT</label>
                        <div>
                            <asp:TextBox ID="txtRutS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Ingreso</label>
                        <div>
                            <asp:TextBox ID="txtFecIngresoS" runat="server" Enabled="False" CssClass="form-control text-uppercase"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tipo</label>
                        <div>
                            <asp:DropDownList ID="cboTipoDiasS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoOtrosS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>

                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:TextBox ID="lblSaldoActS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Nuevo Saldo</label>
                        <div>
                            <asp:TextBox ID="txtSaldoNuevo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Motivo</label>
                        <div>
                            <asp:TextBox ID="txtMotivoS" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Width="100%" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardarS" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnLimpiarS" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                <%--    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportarS" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>--%>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-12">
                        <asp:Panel ID="pnlDatosS" runat="server" Visible="false">
                            <asp:Panel ID="pnlBusquedaS" runat="server" Visible="false">
                                <asp:GridView ID="grdSaldos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="true">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>                                  

                                        <asp:TemplateField HeaderText="Periodo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("nom_periodo")%>' />
                                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnIdPeriodo" runat="server" Value='<%# Bind("id_periodo_vac")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Saldo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSaldo" runat="server" Text='<%# Bind("num_saldo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </asp:Panel>
                        </asp:Panel>
                    </div>

                </div>
            </section>
        </asp:View>

    </asp:MultiView>
    <asp:HiddenField ID="hdnExisteDias" runat="server" />
</asp:Content>
