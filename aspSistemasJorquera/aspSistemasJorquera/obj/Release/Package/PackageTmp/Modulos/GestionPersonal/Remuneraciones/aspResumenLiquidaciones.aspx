﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspResumenLiquidaciones.aspx.vb" Inherits="aspSistemasJorquera.aspResumenLiquidaciones" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnFiltroGeneral" runat="server">Filtro General</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnFiltroNomina" runat="server">Filtro Nomina</asp:LinkButton>
        </li>

    </ul>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Resumen Liquidaciones"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlCabSue" runat="server" Enabled="true">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnExportar" CssClass="btn bg-red btn-block" runat="server">Generar Reporte</asp:LinkButton>

                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <asp:Label ID="lblNom" runat="server" Text="Nombre:" Visible="False" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:Label ID="lblNombre" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblEmp" runat="server" Text="Empresa:" Visible="False" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:Label ID="lblEmpresa" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlDetSue" runat="server" Enabled="true">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdResumen" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("Periodo")%>' />
                                            <asp:HiddenField ID="hdnPeriodo" runat="server" Value='<%# Bind("num_periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departamento">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepto" runat="server" Text='<%# Bind("Depto")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="90px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Días Trab.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiasT" runat="server" Text='<%# Bind("num_dias_trabajados")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Días Lic.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiasL" runat="server" Text='<%# Bind("num_dias_licencias")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Imponible">
                                        <ItemTemplate>
                                            <asp:Label ID="lblImponible" runat="server" Text='<%# Bind("Imponible", "{0:###,###.###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="No Imponible">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoImp" runat="server" Text='<%# Bind("NoImponible", "{0:###,###.###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Haberes">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHaberes" runat="server" Text='<%# Bind("haberes", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Impuesto U.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblImpuestoU" runat="server" Text='<%# Bind("IU", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Des. Legales">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDLegales" runat="server" Text='<%# Bind("DesLegales", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Otros Des.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOtros" runat="server" Text='<%# Bind("OtrosDesc", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Desc.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTDesc" runat="server" Text='<%# Bind("TotalDesc", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Liquido">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLiquido" runat="server" Text='<%# Bind("liquido", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="BtnDescargar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Descargar" Width="18px"><i class="fa fa-fw fa-download"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>

                        </div>

                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:Panel ID="Panel1" runat="server" Enabled="true">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRutRango" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnio1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnio2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnExportarRango" CssClass="btn bg-red btn-block" runat="server">Generar Reporte</asp:LinkButton>

                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <asp:Label ID="Label1" runat="server" Text="Nombre:" Visible="False" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:Label ID="lblNombreRango" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="Label3" runat="server" Text="Empresa:" Visible="False" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:Label ID="lblEmpresaRango" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" Enabled="true">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdRango" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("Periodo")%>' />
                                            <asp:HiddenField ID="hdnPeriodo" runat="server" Value='<%# Bind("num_periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departamento">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepto" runat="server" Text='<%# Bind("Depto")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="90px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Días Trab.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiasT" runat="server" Text='<%# Bind("num_dias_trabajados")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Días Lic.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiasL" runat="server" Text='<%# Bind("num_dias_licencias")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Imponible">
                                        <ItemTemplate>
                                            <asp:Label ID="lblImponible" runat="server" Text='<%# Bind("Imponible", "{0:###,###.###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="No Imponible">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoImp" runat="server" Text='<%# Bind("NoImponible", "{0:###,###.###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Haberes">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHaberes" runat="server" Text='<%# Bind("haberes", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Impuesto U.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblImpuestoU" runat="server" Text='<%# Bind("IU", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Des. Legales">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDLegales" runat="server" Text='<%# Bind("DesLegales", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Otros Des.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOtros" runat="server" Text='<%# Bind("OtrosDesc", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Desc.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTDesc" runat="server" Text='<%# Bind("TotalDesc", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Liquido">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLiquido" runat="server" Text='<%# Bind("liquido", "{0:###,###,###}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="BtnDescargar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Descargar" Width="18px"><i class="fa fa-fw fa-download"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>

                        </div>

                    </div>
                </asp:Panel>
            </asp:View>
        </asp:MultiView>
        <asp:HiddenField ID="hdnCodEmp" runat="server" Value="0" />
        <asp:HiddenField ID="hdnCodCargo" runat="server" Value="0" />
        <asp:HiddenField ID="hdnCodSucursal" runat="server" Value="0" />
        <asp:HiddenField ID="hdnCodInstrumento" runat="server" Value="0" />
        <asp:HiddenField ID="hdnDptos" runat="server" Value="&quot;&quot;" />
        <div class="row justify-content-center table-responsive">

            <div class="col-md-10">

                <asp:Panel ID="pnlReporte" runat="server" Style="overflow: auto;" Height="650px" Width="100%" Visible="false">
                    <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana" Visible="false"
                        waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                    </rsreport:ReportViewer>
                </asp:Panel>
            </div>
        </div>


    </section>
</asp:Content>
