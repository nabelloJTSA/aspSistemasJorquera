﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspVistaPrevia.aspx.vb" Inherits="aspSistemasJorquera.aspVistaPrevia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Vista Previa"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
             

        <div class="row justify-content-center table-responsive">
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Rut</label>
                <div>
                    <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-1 form-group ">
                <label for="fname" class="control-label col-form-label">&nbsp</label>
                <div>
                    <asp:LinkButton ID="btnVistaPrevia" CssClass="btn bg-red btn-block" runat="server">Generar</asp:LinkButton>
                </div>
            </div>
        </div>
        <asp:Panel ID="pnlDet" runat="server" Visible="false">
            <div class="row justify-content-center table-responsive">
                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Nombre</label>
                    <div>
                        <asp:Label ID="lblnombre" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Días a Pago</label>
                    <div>
                        <asp:Label ID="lblDias" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center table-responsive">
                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Empresa</label>
                    <div>
                        <asp:Label ID="lblEmpresa" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Sucursal</label>
                    <div>
                        <asp:Label ID="lblSucursal" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center table-responsive">
                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Cargo</label>
                    <div>
                        <asp:Label ID="lblCargo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Plantilla</label>
                    <div>
                        <asp:Label ID="lblPlantilla" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center table-responsive">
                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Beneficio</label>
                    <div>
                        <asp:Label ID="lblBeneficio" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Depto</label>
                    <div>
                        <asp:Label ID="lblDepto" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-5">

                <asp:GridView ID="grdVistaPrevia" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Id. Item">
                            <ItemTemplate>
                                <asp:Label ID="lblIdItem" runat="server" Text='<%# Bind("id_item")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%# Bind("nom_item")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Id. Formula">
                            <ItemTemplate>
                                <asp:Label ID="lblIdFormula" runat="server" Text='<%# Bind("id_formula")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Haberes">
                            <ItemTemplate>
                                <asp:Label ID="lblHaberes" runat="server" Text='<%# Bind("haberes", "{0:0,0}")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descuentos">
                            <ItemTemplate>
                                <asp:Label ID="lblDescuentos" runat="server" Text='<%# Bind("Descuentos", "{0:0,0}")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </div>
        </div>
        
    </section>
    <asp:HiddenField ID="hdnSucursal" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEmpresa" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDpto" runat="server" Value="0" />
</asp:Content>
