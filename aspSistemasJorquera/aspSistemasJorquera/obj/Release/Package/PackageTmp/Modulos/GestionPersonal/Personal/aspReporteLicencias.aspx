﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspReporteLicencias.aspx.vb" Inherits="aspSistemasJorquera.aspReporteLicencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>


    <div class="row btn-sm">

        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp1" ForeColor="Black" runat="server"><i class="fa fa-fw fa-medkit"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Licencias Prolongadas</span>
                    <h3>
                        <asp:Label ID="lblK1" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>


        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp2" ForeColor="Black" runat="server"><i class="fa fa-fw fa-medkit"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Médicas</span>
                    <h3>
                        <asp:Label ID="lblK2" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>


        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp3" ForeColor="Black" runat="server"><i class="fa fa-fw fa-medkit"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Laborales</span>
                    <h3>
                        <asp:Label ID="lblK3" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon"><i class="fa fa-fw fa-medkit"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">% de personal </span>
                    <h3>
                        <asp:Label ID="lblK4" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>
    </div>

    <section class="content">
        <div class="row justify-content-center table-responsive">

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>

                    <asp:DropDownList ID="cboFiltroEmpComun" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>

                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Rut</label>
                <div>

                    <asp:TextBox ID="txtrut" runat="server" placeholder="Rut Trabajador..." MaxLength="12" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>

                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Nombre</label>
                <div>

                    <asp:TextBox ID="txtBuscarComun" runat="server" placeholder="Nombre..." MaxLength="12" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>

                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Tipo</label>
                <div>

                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                        <asp:ListItem Value="1">Licencia Médica</asp:ListItem>
                        <asp:ListItem Value="2">Licencia Laboral</asp:ListItem>
                    </asp:DropDownList>

                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Desde</label>
                <div>
                    <asp:TextBox ID="dtDesdeComun" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Hasta</label>
                <div>
                    <asp:TextBox ID="dtHastaComun" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-10 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnExportarComun" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
