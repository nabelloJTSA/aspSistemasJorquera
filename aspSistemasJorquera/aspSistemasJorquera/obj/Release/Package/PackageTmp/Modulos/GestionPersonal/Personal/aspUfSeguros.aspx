﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspUfSeguros.aspx.vb" Inherits="aspSistemasJorquera.aspUfSeguros" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Valores Mensuales"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
             
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Tipo</label>
                <div>
                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboAnio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Valor Mes ($)</label>
                <div>
                    <asp:TextBox ID="txtValor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

        </div>


        <div class="row justify-content-center table-responsive">

            <div class="col-md-5 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">
           
            <div class="col-md-5">

                <asp:GridView ID="grdValores" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("nom_valor")%>' />
                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_uf")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdnIdTipo" runat="server" Value='<%# Bind("tip_seguro")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdnCodPeriodo" runat="server" Value='<%# Bind("cod_periodo")%>'></asp:HiddenField>

                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Periodo">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("cod_periodo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID="lblValor" runat="server" Text='<%# Bind("num_valor")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                        </asp:TemplateField>


                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeleteComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>

            </div>

        </div>
    </section>


    <asp:HiddenField ID="hdnId" runat="server" />
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />


</asp:Content>

