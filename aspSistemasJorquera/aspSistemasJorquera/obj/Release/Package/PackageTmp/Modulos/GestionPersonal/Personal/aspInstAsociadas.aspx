﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspInstAsociadas.aspx.vb" Inherits="aspSistemasJorquera.aspInstAsociadas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text="Isntituciones Asociadas"></asp:Label>
        </h1>       
          <div class="row">
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                    <asp:LinkButton ID="btnInstAsciadas" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspFichaContrato.aspx" CssClass="btn bg-orange btn-block">Contrato <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                    <asp:LinkButton ID="btnDatosContrato" CssClass="btn bg-orange btn-block" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspFichaPersonal.aspx">Ficha Personal <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>

        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>


                <div class="box box-danger" data-select2-id="14">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos Personales</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>

                    <div class="box-body" style="" data-select2-id="13">
                        <div class="box box-widget widget-user-2">
                            <div class="widget-user-header bg-orange">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <div class="widget-user-image">
                                                <asp:Image ID="imgavatar" ImageUrl="/Imagen/Sfoto.png" runat="server" CssClass="img-circle" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label col-form-label" for="fname">RUT</label>
                                            <div>
                                                <asp:Label ID="lblRut" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label col-form-label" for="fname">NOMBRE</label>
                                            <div>
                                                <asp:Label ID="lblNombre" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">AFP</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Afp</label>
                                <div>
                                    <asp:DropDownList ID="cboAfp" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">Fec. Incorporacion</label>
                                    <div>
                                        <asp:TextBox ID="txtFecInc" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Pensionado</label>
                                <div>
                                    <asp:DropDownList ID="cboPensionado" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">APV</label>
                                <div>
                                    <asp:DropDownList ID="cboApv" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                        <asp:ListItem Value="1">Si</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Regimen APV</label>
                                <div>
                                    <asp:DropDownList ID="cboTipoApv" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase">
                                        <asp:ListItem Value="A">A</asp:ListItem>
                                        <asp:ListItem Value="B">B</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Monto APV ($)</label>
                                <div>
                                    <asp:TextBox ID="txtMontoApv" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Institucion APV</label>
                                <div>
                                    <asp:DropDownList ID="cboInstApv" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Ahorro AFP</label>
                                <div>
                                    <asp:DropDownList ID="cboAhorroAfp" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                        <asp:ListItem Value="1">Si</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Monto Ahorro ($)</label>
                                <div>
                                    <asp:TextBox ID="txtMontoAhorro" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Institucion Ahorro</label>
                                <div>
                                    <asp:DropDownList ID="cboInsAhorro" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Trabajo Pesado (%)</label>
                                <div>
                                    <asp:TextBox ID="txtTrabajoPesado" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-danger" data-select2-id="15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Salud</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="" data-select2-id="16">

                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Salud</label>
                                    <div>
                                        <asp:DropDownList ID="cboSalud" runat="server" CssClass="form-control text-uppercase"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Nº FUN</label>
                                    <div>
                                        <asp:TextBox ID="txtNumFun" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Monto</label>
                                    <div>
                                        <asp:TextBox ID="txtMonSalud" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Periodo Dscto.</label>
                                    <div>
                                        <asp:TextBox ID="txtPerDcto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Cotizacion Pactada</label>
                                    <div>
                                        <asp:CheckBoxList ID="chkCotizaciones" runat="server" CssClass="form-check-inline" RepeatDirection="Horizontal" CellPadding="10" CellSpacing="0">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-danger" data-select2-id="15">
                    <div class="box-header with-border">
                        <h3 class="box-title">Seguros y Otros</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="" data-select2-id="16">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Seg. Cesantia</label>
                                            <div>
                                                <asp:DropDownList ID="cboSegCesantia" runat="server" CssClass="form-control text-uppercase">
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label col-form-label" for="fname">Fec. Incorporacion</label>
                                            <div>
                                                <asp:TextBox ID="txtFecIncSegCesantia" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date" AutoPostBack="True"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Nº Cargas</label>
                                            <div>
                                                <asp:TextBox ID="txtNumCargasAsigFamiliar" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Tramo</label>
                                            <div>
                                                <asp:TextBox ID="txtAsigFamiliar" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-3">

                                        <asp:GridView ID="grdAsigFamiliar" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                                </asp:CommandField>
                                                <asp:TemplateField HeaderText="Tramo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesTramo" runat="server" Text='<%# Bind("des_tramo")%>' />
                                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id_tramo")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="40px" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Monto">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNumMonto" runat="server" Text='<%# Bind("num_monto")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="70px" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requisito">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRequisito" runat="server" Text='<%# Bind("des_requisito")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="270px" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Año">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblParentesco" runat="server" Text='<%# Bind("per_anio")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                  
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Seg. Voluntario</label>
                                        <div>
                                            <asp:DropDownList ID="cboSegVoluntario" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Monto</label>
                                        <div>
                                            <asp:TextBox ID="txtMontoSegVoluntario" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Costo Plan ($), (UF)</label>
                                        <div>
                                            <asp:TextBox ID="txtCostoPlanSegVoluntario" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Beneficiarios</label>
                                        <div>
                                            <asp:TextBox ID="txtBeneficiarios" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Nº Fono</label>
                                        <div>
                                            <asp:TextBox ID="txtFono" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Seg. Externos</label>
                                        <div>
                                            <asp:DropDownList ID="cboSegExternos" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Monto</label>
                                        <div>
                                            <asp:TextBox ID="txtMontoSegExternos" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Institucion</label>
                                        <div>
                                            <asp:DropDownList ID="cboInstitucion" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                               
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Seg. Comple.</label>
                                        <div>
                                            <asp:DropDownList ID="cboVidaCamara" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Nº Cargas</label>
                                        <div>
                                            <asp:TextBox ID="txtNumCargasVidaCamara" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Costo Plan (UF)</label>
                                        <div>
                                            <asp:TextBox ID="txtCostoPlanVidaCamara" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Seg. Oncologico</label>
                                        <div>
                                            <asp:DropDownList ID="cboSegOncologico" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Nº Cargas</label>
                                        <div>
                                            <asp:TextBox ID="txtNumCargasOnco" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Costo Plan </label>
                                        <div>
                                            <asp:TextBox ID="txtCostoPlanOnco" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdTramo" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>

                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="loader">
                                        <div class="loader-inner">
                                            <div class="loading one"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading two"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading three"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading four"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>
</asp:Content>
