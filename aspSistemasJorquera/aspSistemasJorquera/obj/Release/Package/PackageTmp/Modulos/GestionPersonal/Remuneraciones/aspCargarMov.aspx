﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCargarMov.aspx.vb" Inherits="aspSistemasJorquera.aspCargarMov" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Movilizacion"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">

        <div class="row justify-content-center table-responsive">          
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Archivo</label>
                <div>
                    <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" />
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp</label>
                <div>
                    <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                </div>
            </div>
            <div class="col-md-1 form-group ">
                <label for="fname" class="control-label col-form-label">&nbsp</label>
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-5">

                <asp:GridView ID="grdDescuentos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Rut">
                            <ItemTemplate>
                                <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Periodo">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("num_periodo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Valor">
                            <ItemTemplate>
                                <asp:Label ID="lblValor" runat="server" Text='<%# Bind("num_valor")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </div>
        </div>
        </asp:Panel>
          
        </asp:MultiView>
    </section>

</asp:Content>
