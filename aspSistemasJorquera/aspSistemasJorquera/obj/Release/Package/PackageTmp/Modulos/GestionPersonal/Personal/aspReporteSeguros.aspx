﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspReporteSeguros.aspx.vb" Inherits="aspSistemasJorquera.aspReporteSeguros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>


<%--    <div class="row btn-sm">

        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp1" ForeColor="Black" runat="server"><i class="fa fa-fw fa-briefcase"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Personal asegurado</span>
                    <h3>
                        <asp:Label ID="lblK1" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>


        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp2" ForeColor="Black" runat="server" ><i class="fa fa-fw fa-briefcase"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Personal no asegurado</span>
                    <h3>
                        <asp:Label ID="lblK2" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>


        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon">
                    <asp:LinkButton ID="btnKp3" ForeColor="Black" runat="server"><i class="fa fa-fw fa-briefcase"></i></asp:LinkButton></span>

                <div class="info-box-content">
                    <span class="info-box-text">Personal Total</span>
                    <h3>
                        <asp:Label ID="lblK3" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 col-sm-6 col-xs-12">
            <div class="info-box BG-INFO">
                <span class="info-box-icon"><i class="fa fa-fw fa-briefcase"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">% de personal asegurado</span>
                    <h3>
                        <asp:Label ID="lblK4" runat="server" Font-Bold="true" Text=""></asp:Label></h3>
                </div>
            </div>
        </div>
    </div>--%>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>

                    <asp:DropDownList ID="cboEmpresaC" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>

                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Tipo Seguro</label>
                <div>

                    <asp:DropDownList ID="cboTipoSeguro" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>

                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboAnioC" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodoC" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Mostrar Todo</label>
                <div>
                    <asp:CheckBox ID="chkMostrarTodoC" runat="server" Text="" AutoPostBack="True" />
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                <div>
                    <asp:TextBox ID="txtBNombreC" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-8 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnExportarC" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </section>

</asp:Content>

