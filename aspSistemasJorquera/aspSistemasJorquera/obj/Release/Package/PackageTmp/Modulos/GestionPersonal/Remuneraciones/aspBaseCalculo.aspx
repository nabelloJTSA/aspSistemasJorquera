﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspBaseCalculo.aspx.vb" Inherits="aspSistemasJorquera.aspBaseCalculo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Base Cálculo"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboAnio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">UTM ($)</label>
                <div>
                    <asp:TextBox ID="lblUTM" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="False" BorderStyle="None"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnCalcular" CssClass="btn btn-warning btn-block" runat="server">Calcular</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-8">

                <asp:GridView ID="grdImpuesto" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Tramo">
                            <ItemTemplate>
                                <asp:Label ID="lbltramo" runat="server" Text='<%# Bind("cod_tramo")%>'></asp:Label>
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id_impuesto_unico")%>' />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="80px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Desde">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDesd" Enabled="false" Text='<%# Bind("val_desde")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="100px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Hasta">
                            <ItemTemplate>
                                <asp:TextBox ID="txtHast" Enabled="false" Text='<%# Bind("val_hasta") %>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px"  MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="100px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Factor">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFac" Text='<%# Bind("num_factor")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="70px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="70px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Descuento">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescu" Enabled="false" Text='<%# Bind("num_descuento")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="90px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="90px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Base Tope">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBaseTope" Text='<%# Bind("base_tope")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="80px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="80px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Base Tasa">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBaseTasa" Text='<%# Bind("base_tasa")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="80px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="center" Width="80px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Base Dcto.">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBaseDesto" Text='<%# Bind("base_descuento")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="left" Width="110px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tope AFP/Salud">
                            <ItemTemplate>
                                <asp:TextBox ID="txtTopeAFPSALUD" Text='<%# Bind("tope_afp_salud")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="left" Width="100px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tope AFP Antiguo">
                            <ItemTemplate>
                                <asp:TextBox ID="txtTopeAFPAnt" Text='<%# Bind("tope_apf_antiguo")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="left" Width="100px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tope AFC">
                            <ItemTemplate>
                                <asp:TextBox ID="txtTopeAFC" Text='<%# Bind("tope_afc")%>'
                                    CssClass="EstEditCtrGrilla" runat="server" Width="100px" MaxLength="30" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="left" Width="110px" CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnActualizar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="18px" CommandName="1" ToolTip="Actualizar"><i class="fa fa-fw fa-save"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>

            </div>

        </div>
    </section>
    <asp:HiddenField ID="hdn_id_imp" runat="server" />
    <asp:HiddenField ID="hdn_dcto" runat="server" />
    <asp:HiddenField ID="hdn_desde" runat="server" />
    <asp:HiddenField ID="hdn_hasta" runat="server" />
    <asp:HiddenField ID="hdn_factor" runat="server" />
    <asp:HiddenField ID="hdnBaseTope" runat="server" />
    <asp:HiddenField ID="hdnBaseTasa" runat="server" />
    <asp:HiddenField ID="hdnBaseDescuento" runat="server" />
    <asp:HiddenField ID="hdnTopeAfpSalud" runat="server" />
    <asp:HiddenField ID="hdnTopeAfpAnt" runat="server" />
    <asp:HiddenField ID="hdnTopeAFC" runat="server" />
</asp:Content>
