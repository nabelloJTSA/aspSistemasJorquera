﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspConsolidadoProduccion.aspx.vb" Inherits="aspSistemasJorquera.aspConsolidadoProduccion" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Consolidado Producción"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row justify-content-center table-responsive">

            <div class="col-md-10">

                <asp:Panel ID="pnlReporte" runat="server" Style="overflow: auto;" Height="650px" Width="100%" Visible="true">
                    <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana" Visible="true"
                        waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                    </rsreport:ReportViewer>
                </asp:Panel>
            </div>
        </div>
    </section>
</asp:Content>

