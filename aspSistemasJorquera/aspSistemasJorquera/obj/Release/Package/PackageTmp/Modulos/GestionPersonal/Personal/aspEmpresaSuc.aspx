﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspEmpresaSuc.aspx.vb" Inherits="aspSistemasJorquera.aspEmpresaSuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <ul class="nav nav-tabs bg-info ">
                <li>
                    <asp:LinkButton ID="btnEmpresa" runat="server">Empresa</asp:LinkButton>

                </li>
                <li>
                    <asp:LinkButton ID="btnSucursal" runat="server">Sucursal</asp:LinkButton>

                </li>
              
            </ul>
            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>

            <section class="content">

                <asp:MultiView ID="MtvAreas" runat="server">
                    <asp:View ID="VwAreas" runat="server">
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Nombre</label>
                                <div>
                                    <asp:TextBox ID="txtNomEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Rut</label>
                                <div>
                                    <asp:TextBox ID="txtRutEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Razón Social</label>
                                <div>
                                    <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>                           
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Direccion</label>
                                <div>
                                    <asp:TextBox ID="txtDireccion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Direccion Abreviada</label>
                                <div>
                                    <asp:TextBox ID="txtDireccionAbrev" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Ciudad</label>
                                <div>
                                    <asp:TextBox ID="txtCiudad" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Cod. Empresa</label>
                                <div>
                                    <asp:TextBox ID="txtCodEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Cod. F700</label>
                                <div>
                                    <asp:TextBox ID="txtCodEmpresaF700" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Rep. Legal</label>
                                <div>
                                    <asp:TextBox ID="txtRepLegal" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">RUT Rep. Legal</label>
                                <div>
                                    <asp:TextBox ID="txtRutRepLegal" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Giro</label>
                                <div>
                                    <asp:TextBox ID="txtGiro" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Mutual</label>
                                <div>
                                    <asp:DropDownList ID="cboMutual" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Tasa</label>
                                <div>
                                    <asp:TextBox ID="txtTasaMutualidad" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Cod. Act. Economica</label>
                                <div>
                                    <asp:TextBox ID="txtCodActEc" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">CCAF</label>
                                <div>
                                    <asp:DropDownList ID="cboCCFA" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Tasa</label>
                                <div>
                                    <asp:TextBox ID="txtTasa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-10">

                                <asp:GridView ID="grdEmpresas" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Empresa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />

                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                                <asp:HiddenField ID="hdnDireccion" runat="server" Value='<%# Bind("nom_direccion")%>' />
                                                <asp:HiddenField ID="hdnGiro" runat="server" Value='<%# Bind("nom_giro")%>' />
                                                <asp:HiddenField ID="hdnRazonSocial" runat="server" Value='<%# Bind("nom_razon_social")%>' />
                                                <asp:HiddenField ID="hdnDireccionAbrev" runat="server" Value='<%# Bind("direccion_abrev")%>' />
                                                <asp:HiddenField ID="hdnCodEmpF700" runat="server" Value='<%# Bind("cod_empresa_fin700")%>' />
                                                <asp:HiddenField ID="hdnCodAct" runat="server" Value='<%# Bind("cod_actividad")%>' />
                                                <asp:HiddenField ID="hdnCCAF" runat="server" Value='<%# Bind("id_ccaf")%>' />
                                                <asp:HiddenField ID="hdnMutualidad" runat="server" Value='<%# Bind("id_mutualidad")%>' />
                                                <asp:HiddenField ID="hdnCiudad" runat="server" Value='<%# Bind("ciudad")%>' />

                                                <asp:HiddenField ID="hdnNumTasa" runat="server" Value='<%# Bind("num_tasa")%>' />
                                                <asp:HiddenField ID="hdnNumTasaMutualidad" runat="server" Value='<%# Bind("num_tasa_mutualidad")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="350px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="RUT">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutEmp" runat="server" Text='<%# Bind("rut_empresa")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Rep. Legal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRepLegal" runat="server" Text='<%# Bind("nom_replegal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="350px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="RUT Rep. Legal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutRepLegal" runat="server" Text='<%# Bind("rut_replegal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>
                    <asp:View ID="VwSubarea" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Nombre Sucursal</label>
                                <div>
                                    <asp:TextBox ID="txtNom" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardarSuc" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelarSuc" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdSucursal" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Empresa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_sucursal")%>' />
                                                <asp:HiddenField ID="hdn_Empresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sucursal">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombreSucursal" runat="server" Text='<%# Bind("nom_sucursal")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>                 
                </asp:MultiView>


                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdSuc" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivoSuc" runat="server" Value="0" />

            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
