﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspFichaContrato.aspx.vb" Inherits="aspSistemasJorquera.aspFichaContrato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text="Datos Contrato"></asp:Label>
        </h1>

        <div class="row">
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                    <asp:LinkButton ID="btnDatosContrato" CssClass="btn bg-orange btn-block" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspFichaPersonal.aspx">Ficha Personal <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                   <asp:LinkButton ID="LinkButton1" CssClass="btn bg-orange btn-block" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspInstAsociadas.aspx">Instituciones <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>


                <div class="box box-danger" data-select2-id="14">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos Personales</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>

                    <div class="box-body" style="" data-select2-id="13">
                        <div class="box box-widget widget-user-2">
                            <div class="widget-user-header bg-orange">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <div class="widget-user-image">
                                                <asp:Image ID="imgavatar" ImageUrl="/Imagen/Sfoto.png" runat="server" CssClass="img-circle" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label class="control-label col-form-label" for="fname">RUT</label>
                                            <div>
                                                <asp:Label ID="lblRut" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label col-form-label" for="fname">NOMBRE</label>
                                            <div>
                                                <asp:Label ID="lblNombre" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Datos Contrato</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">Fecha Ingreso</label>
                                    <div>
                                        <asp:TextBox ID="txtFecIngreso" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Activo</label>
                                <div>
                                    <asp:DropDownList ID="cboActivo" runat="server" CssClass="form-control text-uppercase">
                                        <asp:ListItem Value="1">Si</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Antigüedad</label>
                                <div>
                                    <asp:TextBox ID="txtAntiguedad" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Tipo Contrato</label>
                                <div>
                                    <asp:DropDownList ID="cboTipoContrato" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Tipo Trabajador</label>
                                <div>
                                    <asp:DropDownList ID="cboTipoTrabajador" AutoPostBack="True" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresa" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Sucursal</label>
                                <div>
                                    <asp:DropDownList ID="cboSucursal" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Area</label>
                                <div>
                                    <asp:DropDownList ID="cboArea" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Sub-Area</label>
                                <div>
                                    <asp:DropDownList ID="cboSubArea" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Departamento</label>
                                <div>
                                    <asp:DropDownList ID="cboDepto" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Cargo</label>
                                <div>
                                    <asp:DropDownList ID="cboCargo" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Cla. Pago</label>
                                <div>
                                    <asp:DropDownList ID="cboClaPago" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <asp:Label ID="lblFamiliaServicio" runat="server" Text="Familia/ Servicio" Visible="False" Font-Bold="True"></asp:Label>
                                <div style="height: 5px; width: 5px" class="container"></div>
                                <div>
                                    <asp:DropDownList ID="cboFamiliaServicio" runat="server" Visible="False" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Ubic. Geográfica</label>
                                <div>
                                    <asp:DropDownList ID="cboCiudad" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-danger" data-select2-id="17">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jornada Laboral</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="" data-select2-id="18">

                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Jornada Laboral</label>
                                    <div>
                                        <asp:DropDownList ID="cboJornada" runat="server" AutoPostBack="True" CssClass="form-control text-uppercase"></asp:DropDownList>
                                        <asp:Label ID="lblDesJornada" runat="server" Font-Size="Small"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <asp:Label ID="lblRol" runat="server" Text="Fec. Ingreso 4 Rol" Visible="False" Font-Bold="True"></asp:Label>
                                <div style="height: 5px; width: 5px" class="container"></div>
                                <div>
                                    <asp:TextBox ID="txtFecIngresoRol" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date" Visible="false"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Hr Mensual</label>
                                <div>
                                    <asp:TextBox ID="txtHrsMensuales" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Hr Semanal</label>
                                <div>
                                    <asp:TextBox ID="txtHrsSemanales" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Hrs Diaria</label>
                                <div>
                                    <asp:TextBox ID="txtHrsDiarias" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Pacto HR</label>
                                <div>
                                    <asp:DropDownList ID="cboPacto" AutoPostBack="True" runat="server" CssClass="form-control text-uppercase">
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                        <asp:ListItem Value="1">Si</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                    <div>
                                        <asp:LinkButton ID="btnAgregarGF" runat="server" CssClass="btn bg-orange btn-block">Agregar</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <div>
                                        <asp:DropDownList ID="cboContratoAsociado" runat="server" Visible="False" CssClass="form-control text-uppercase"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">

                                <asp:GridView ID="grdGrupoFamiliar" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tipo Licencia">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLicencia" runat="server" Text='<%# Bind("nom_tipo")%>' />
                                                <asp:HiddenField ID="hdnIdLicencia" runat="server" Value='<%# Bind("id_licencia")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Vencimiento">
                                            <ItemTemplate>
                                                <asp:TextBox ID="dtFechavencimiento" runat="server" CssClass="EstEditCtrGrilla" Height="20px" Text='<%# Bind("fec_vencimiento")%>' Width="80px" Enabled="false" />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="9" CssClass="btn btn-sm btn-secondary fa fa-trash" ToolTip="Eliminar"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">

                    <div class="col-md-5">
                        <div class="box box-danger" data-select2-id="15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Instrumento Colectivo</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="" data-select2-id="16">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">I.Colectivo</label>
                                            <div>
                                                <asp:DropDownList ID="cboInsColectivo" runat="server" CssClass="form-control text-uppercase" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Beneficios</label>
                                            <div>
                                                <asp:DropDownList ID="cboBeneficios" runat="server" CssClass="form-control text-uppercase" Width="100%">
                                                    <asp:ListItem Value="9">No Aplica</asp:ListItem>
                                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                            <div>
                                                <asp:LinkButton ID="btnAgregarIC" runat="server" CssClass="btn bg-orange btn-block">Agregar</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-3">

                                        <asp:GridView ID="grdIC" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="I. Colectivo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIc" runat="server" Text='<%# Bind("nom_ic")%>' />
                                                        <asp:HiddenField ID="hdn_cod_ic" runat="server" Value='<%# Bind("cod_ic")%>' />
                                                        <asp:HiddenField ID="hdn_est_b" runat="server" Value='<%# Bind("est_beneficio")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="250px" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Beneficios">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBeneficio" runat="server" Text='<%# Bind("beneficios")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box box-danger" data-select2-id="15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Datos Pago</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="" data-select2-id="16">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Medio de Pago</label>
                                            <div>
                                                <asp:DropDownList ID="cboMedioPago" runat="server" CssClass="form-control text-uppercase" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Banco</label>
                                            <div>
                                                <asp:DropDownList ID="cboBanco" runat="server" CssClass="form-control text-uppercase" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Nº Cuenta</label>
                                            <div>
                                                <asp:TextBox ID="txtNumCuenta" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Anticipo</label>
                                            <div>
                                                <asp:TextBox ID="txtAntFijo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Sueldo Base</label>
                                            <div>
                                                <asp:TextBox ID="txtSueldoBase" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-danger" data-select2-id="17">
                    <div class="box-header with-border">
                        <h3 class="box-title">Termino Contrato </h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="" data-select2-id="18">

                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="fname" class="control-label col-form-label">Fecha Termino contrato</label>
                                    <div>
                                        <asp:TextBox ID="txtFecTerminoContrato" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <asp:Label ID="lblFecFiniquito" runat="server" Text="Fecha Finiquito" Visible="False" Font-Bold="True"></asp:Label>
                                <div style="height: 5px; width: 5px" class="container"></div>
                                <div>
                                    <asp:TextBox ID="txtFecFiniquito" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <asp:Label ID="lblCausal" runat="server" Text="Causal" Visible="False" Font-Bold="True"></asp:Label>
                                <div style="height: 5px; width: 5px" class="container"></div>
                                <div>
                                    <asp:TextBox ID="txtCausal" runat="server" AutoCompleteType="Disabled" Enabled="False" CssClass="form-control text-uppercase" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdPerVac" runat="server" Value="0" />
                <asp:HiddenField ID="hdn4Rol" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>

                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="loader">
                                        <div class="loader-inner">
                                            <div class="loading one"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading two"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading three"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading four"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>
</asp:Content>
