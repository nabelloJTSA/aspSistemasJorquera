﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspItemInformados.aspx.vb" Inherits="aspSistemasJorquera.aspItemInformados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnRef" runat="server">Ingreso Items</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnEERRGeneral" runat="server">Ingreso Fallas</asp:LinkButton></li>

    </ul>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="ítems"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Item</label>
                            <div>
                                <asp:DropDownList ID="cboItem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdItems" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("num_periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Valor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValor" runat="server" Text='<%# Bind("num_valor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <div class="row justify-content-center table-responsive">                   
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Rut</label>
                        <div>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Inicio</label>
                        <div>
                            <asp:TextBox ID="txtfechai" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Termino</label>
                        <div>
                            <asp:TextBox ID="txtFechaT" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Días</label>
                        <div>
                            <asp:TextBox ID="txtDias" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group ">
                        <asp:Label ID="lblNombre" runat="server" Font-Bold="True">&nbsp</asp:Label>
                        <div style="height: 5px; width: 5px" class="container"></div>
                        <div>
                            <asp:LinkButton ID="btnGuardarF" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grdFallas" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Rut">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' />
                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_movimiento")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Nombre">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInicio" runat="server" Text='<%# Bind("fec_inicio")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Termino">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTerminmo" runat="server" Text='<%# Bind("fec_termino")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Días">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValor" runat="server" Text='<%# Bind("dias_corridos")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDeleteComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </section>

</asp:Content>
