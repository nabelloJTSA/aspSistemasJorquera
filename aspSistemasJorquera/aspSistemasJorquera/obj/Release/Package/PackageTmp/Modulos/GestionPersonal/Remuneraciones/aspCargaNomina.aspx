﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCargaNomina.aspx.vb" Inherits="aspSistemasJorquera.aspCargaNomina" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnAnt" runat="server">Anticipos</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnSue" runat="server">Sueldos</asp:LinkButton>
        </li>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Cargar Nomina"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

        </div>
        <asp:Panel ID="pnlDatos" runat="server" Visible="false">
            <asp:MultiView ID="mtvNomina" runat="server">
                <asp:View ID="vwANT" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuplCargarAnt" runat="server" CssClass="form-control text-uppercase" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargarAnticipos" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGuardarAnticipo" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnExcel" CssClass="btn bg-orange btn-block" runat="server">Exp. F700</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnTxt" CssClass="btn bg-red btn-block" runat="server">Exp. Pagos Web</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlCabAnt" runat="server" Enabled="true">
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">RUT</label>
                                <div>
                                    <asp:TextBox ID="txtRutEmpAnt" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:TextBox ID="txtNomEmpAnt" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Convenio</label>
                                <div>
                                    <asp:TextBox ID="txtConvenioAnt" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Nómina</label>
                                <div>
                                    <asp:TextBox ID="txtNominaAnt" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Fecha Pago</label>
                                <div>
                                    <asp:TextBox ID="txtFecPagoAnt" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Total</label>
                                <div>
                                    <asp:TextBox ID="txtTotalAnt" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Pesos</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Remuneraciones</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Generico</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetAnt" runat="server" Enabled="true">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdDetAnt" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Rut">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutAnt" runat="server" Text='<%# Bind("rut_persona")%>' />
                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_det")%>' />

                                                <asp:HiddenField ID="hdnCodBanco" runat="server" Value='<%# Bind("id_banco")%>' />
                                                <asp:HiddenField ID="hdnCodMedioPago" runat="server" Value='<%# Bind("id_medio_pago")%>' />
                                                <asp:HiddenField ID="hdnEstT" runat="server" Value='<%# Bind("est_traspaso")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombreAnt" runat="server" Text='<%# Bind("nom_persona")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Monto Pago">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMontoPagoAnt" runat="server" Text='<%#Bind("val_total")%>' Width="120px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="130px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Banco">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboBancoAnt" runat="server" CssClass="btn-sm" AutoPostBack="false" DataTextField="nom_banco" DataValueField="num_codigo" Width="170px" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" VerticalAlign="Middle" Width="180px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Medio de Pago">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMedioPagoAnt" runat="server" AutoPostBack="false" DataTextField="nom_mediopago" DataValueField="num_codigo" Width="200px" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" VerticalAlign="Middle" Width="200px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nª Cuenta">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNumCuentaAnt" runat="server" Text='<%#Bind("num_cuenta")%>' Width="100px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="110px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Descripcion">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDesAnt" runat="server" Text='<%#Bind("glosa")%>' Width="150px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Traspasar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSeleccionar" runat="server" Checked="true" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkactualizarA" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="vwSUE" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuplSueldo" runat="server" CssClass="form-control text-uppercase" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargaSueldos" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGuardarSueldos" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnSueldoFin700" CssClass="btn bg-orange btn-block" runat="server">Exp. F700</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnPagosWebSueldos" CssClass="btn bg-red btn-block" runat="server">Exp. Pagos Web</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlCabSue" runat="server" Enabled="true">
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">RUT</label>
                                <div>
                                    <asp:TextBox ID="txtRutSueldos" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:TextBox ID="txtEmpresaSueldo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Convenio</label>
                                <div>
                                    <asp:TextBox ID="txtConvenioSueldo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Nómina</label>
                                <div>
                                    <asp:TextBox ID="txtNominaSueldo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Fecha Pago</label>
                                <div>
                                    <asp:TextBox ID="txtfechaSueldo" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Periodo</label>
                                <div>
                                    <asp:TextBox ID="txtFecPagoPer" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False"></asp:TextBox>
                                    <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Total</label>
                                <div>
                                    <asp:TextBox ID="txtTotalSueldo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="cbo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Pesos</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Remuneraciones</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp</label>
                                <div>
                                    <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True">
                                        <asp:ListItem Value="1">Generico</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetSue" runat="server" Enabled="true">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdSueldos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="Rut">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRutAnt" runat="server" Text='<%# Bind("rut_persona")%>' />
                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_det")%>' />

                                                <asp:HiddenField ID="hdnCodBanco" runat="server" Value='<%# Bind("id_banco")%>' />
                                                <asp:HiddenField ID="hdnCodMedioPago" runat="server" Value='<%# Bind("id_medio_pago")%>' />
                                                <asp:HiddenField ID="hdnEstT" runat="server" Value='<%# Bind("est_traspaso")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nombre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNombreAnt" runat="server" Text='<%# Bind("nom_persona")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Monto Pago">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMontoPagoAnt" runat="server" Text='<%#Bind("val_total")%>' Width="120px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="130px" HorizontalAlign="Right" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Banco">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboBancoAnt" runat="server" AutoPostBack="false" DataTextField="nom_banco" DataValueField="num_codigo" Width="170" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" Width="170" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Medio de Pago">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboMedioPagoAnt" runat="server" AutoPostBack="false" DataTextField="nom_mediopago" DataValueField="num_codigo" Width="200px" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" Width="200px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nª Cuenta">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNumCuentaAnt" runat="server" Text='<%#Bind("num_cuenta")%>' Width="110px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Descripcion">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtDesAnt" runat="server" Text='<%#Bind("glosa")%>' Width="120px"> </asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Traspasar">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSeleccionar" runat="server" Checked="true" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkactualizar" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="60px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:View>
            </asp:MultiView>
        </asp:Panel>
    </section>
        <asp:HiddenField ID="hdnIdCab" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdDetAnt" runat="server" />
            <asp:HiddenField ID="hdnNumNominaAnt" runat="server" />
            <asp:HiddenField ID="hdnMedioPagoAnt" runat="server" />
            <asp:HiddenField ID="hdnCodBancoAnt" runat="server" />
            <asp:HiddenField ID="hdnNumCuentaAnt" runat="server" />
            <asp:HiddenField ID="hdnValTotalAnt" runat="server" />
            <asp:HiddenField ID="hdnGlosaAnt" runat="server" />
            <asp:HiddenField ID="hdnRutGrilla" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFiniquitado" runat="server" Value="0" />
            <asp:HiddenField ID="hdnEstTraspaso" runat="server" Value="0" />
            <asp:HiddenField ID="hdnNumConvenio" runat="server" Value="0" />
</asp:Content>

 