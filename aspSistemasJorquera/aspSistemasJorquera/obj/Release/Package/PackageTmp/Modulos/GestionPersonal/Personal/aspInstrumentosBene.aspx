﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspInstrumentosBene.aspx.vb" Inherits="aspSistemasJorquera.aspInstrumentosBene" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <ul class="nav nav-tabs bg-info ">
                <li>
                    <asp:LinkButton ID="btnInstrumento" runat="server">Ingresar Instrumento</asp:LinkButton>

                </li>
                <li>
                    <asp:LinkButton ID="btnRelacion" runat="server">Instrumento Cargo</asp:LinkButton>

                </li>
                <li>
                    <asp:LinkButton ID="btnBeneficios" runat="server">Beneficios</asp:LinkButton>

                </li>
            </ul>
            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>

            <section class="content">
                <asp:Label ID="lblIdBeneficio" runat="server" Enabled="False" Visible="false"></asp:Label>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Tipo Instrumento</label>
                                <div>
                                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Nombre Intrumento</label>
                                <div>
                                    <asp:TextBox ID="txtNomInstrumento" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdIntrumento" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Empresa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmp" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("cod_instrumento")%>' />
                                                <asp:HiddenField ID="hdn_emp" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                                <asp:HiddenField ID="hdn_tipo" runat="server" Value='<%# Bind("id_tipo_instrumento")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("Tipo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Instrumento">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstrumento" runat="server" Text='<%# Bind("nom_instrumento")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresaRelacion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Instrumento</label>
                                <div>
                                    <asp:DropDownList ID="cboInstrumento" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-5 form-group">
                                <asp:Label ID="lblDepto" runat="server" Text="Personal" Visible="false" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                <div>
                                    <asp:CheckBoxList ID="chkCargos" runat="server" AutoPostBack="True" RepeatColumns="5">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardarRelacion" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                        </div>
                        </fieldset>
                    <asp:HiddenField ID="hdnProcesar" runat="server" />

                        <asp:HiddenField ID="hdnTodos" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnDptos" runat="server" Value="&quot;&quot;" />

                    </asp:View>
                    <asp:View ID="VwDeptos" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresaBN" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Instrumento</label>
                                <div>
                                    <asp:DropDownList ID="cboInstrumentoBN" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Cargo</label>
                                <div>
                                    <asp:DropDownList ID="cboCargo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <asp:Label ID="lblDeptoBN" runat="server" Text="Depto" Visible="false" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                <div style="height: 5px; width: 5px"  class="container"></div>
                                <div>
                                    <asp:DropDownList ID="cboDepto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">ITEM</label>
                                <div>
                                    <asp:DropDownList ID="cboItem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Descripción</label>
                                <div>
                                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Valor</label>
                                <div>
                                    <asp:TextBox ID="txtValor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Antiguedad</label>
                                <div>
                                    <asp:CheckBox ID="chkAntiguedad" runat="server" AutoPostBack="true" />
                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="pnlAntiguedad" runat="server" Visible="false">

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblAntiguedad" runat="server" Text="Mayor a" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                           <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtAntiguedad" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblAntiguedadF" runat="server" Text="Menor A"  Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                           <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtAntiguedaF" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Pago desfase</label>
                                    <div>
                                        <asp:TextBox ID="txtDesfase" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>

                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Falla/Accidente</label>
                                <div>
                                    <asp:CheckBox ID="chkFallaAcc" runat="server" AutoPostBack="true"/>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnlAccidenteAusencia" runat="server" Visible="false">

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblFalla1" runat="server" Text="Falla/Accidente"  Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtControlAcc1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblValorAccidente1" runat="server" Text="% Descuento" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtValorAcc1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblFalla2" runat="server" Text="Falla/Accidente" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtControlAcc2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblValorAccidente2" runat="server" Text="% Descuento" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtValorAcc2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblFalla3" runat="server" Text="Falla/Accidente"  Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtControlAcc3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="lblValorAccidente3" runat="server" Text="% Descuento" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtValorAcc3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                        </asp:Panel>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Produccion</label>
                                <div>
                                    <asp:CheckBox ID="chkProduccion" runat="server" AutoPostBack="true"/>
                                </div>
                            </div>
                        </div>


                        <asp:Panel ID="pnlProduccion" runat="server" Visible="false">

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="Label1" runat="server" Text="Primeros" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtTopeProduccion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="Label3" runat="server" Text="$" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtPrecioTope" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Menor a" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtMinimoProduccion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <asp:Label ID="Label4" runat="server" Text="$" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                                      <div style="height: 5px" class="container"></div>
                                    <div>
                                        <asp:TextBox ID="txtPrecioMenor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardarBN" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelarBN" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdBeneficios" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>
                                        <asp:TemplateField HeaderText="Beneficio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBeneficio" runat="server" Text='<%# Bind("Item")%>' />
                                                <asp:HiddenField ID="hdnIdBeneficios" runat="server" Value='<%# Bind("id_beneficios")%>' />
                                                <asp:HiddenField ID="hdnCodInstrumento" runat="server" Value='<%# Bind("cod_instrumento")%>' />
                                                <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_item")%>' />
                                                <asp:HiddenField ID="hdnCodCargo" runat="server" Value='<%# Bind("cod_cargo")%>' />
                                                <asp:HiddenField ID="hdnAntiguedadNecesaria" runat="server" Value='<%# Bind("num_antiguedad_necesaria")%>' />
                                                <asp:HiddenField ID="hdnPagoDesfase" runat="server" Value='<%# Bind("num_pago_desfase")%>' />
                                                <asp:HiddenField ID="hdnFallaControl1" runat="server" Value='<%# Bind("falla_accidente_control1")%>' />
                                                <asp:HiddenField ID="hdnFallaValor1" runat="server" Value='<%# Bind("falla_accidente_valor1")%>' />
                                                <asp:HiddenField ID="hdnFallaControl2" runat="server" Value='<%# Bind("falla_accidente_control2")%>' />
                                                <asp:HiddenField ID="hdnFallaValor2" runat="server" Value='<%# Bind("falla_accidente_valor2")%>' />
                                                <asp:HiddenField ID="hdnFallaControl3" runat="server" Value='<%# Bind("falla_accidente_control3")%>' />
                                                <asp:HiddenField ID="hdnFallaValor3" runat="server" Value='<%# Bind("falla_accidente_valor3")%>' />
                                                <asp:HiddenField ID="hdnNumLimiteP" runat="server" Value='<%# Bind("num_lim_produccion")%>' />
                                                <asp:HiddenField ID="hdnValorLimiteP" runat="server" Value='<%# Bind("val_lim_produccion")%>' />
                                                <asp:HiddenField ID="hdnNumDiferenciaP" runat="server" Value='<%# Bind("num_dif_produccion")%>' />
                                                <asp:HiddenField ID="hdnValorDiferenciaP" runat="server" Value='<%# Bind("val_dif_produccion")%>' />
                                                <asp:HiddenField ID="hdnDescripcion" runat="server" Value='<%# Bind("des_beneficio")%>' />
                                                <asp:HiddenField ID="hdnAntiguedadNecesariaFin" runat="server" Value='<%# Bind("num_antiguedad_necesariafin")%>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="250px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Valor">
                                            <ItemTemplate>
                                                <asp:Label ID="lblValor" runat="server" Text='<%# Bind("num_valor")%>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Antiguedad">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAntiguedad" runat="server" Checked='<%# Bind("est_antiguedad")%>' Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Falla">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkFalla" runat="server" Checked='<%# Bind("est_falla_accidente")%>' Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Producción">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkProduccion" runat="server" Checked='<%# Bind("est_produccion")%>' Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>
                </asp:MultiView>


                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdSub" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivoSub" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdDpt" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivoDpt" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdResolucion" runat="server" />
                <asp:HiddenField ID="hdnIdBeneficio" runat="server" Value="0" />

            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
