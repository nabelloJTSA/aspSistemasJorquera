﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCentralizar.aspx.vb" Inherits="aspSistemasJorquera.aspCentralizar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Previred"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <asp:Panel ID="pnlDatos" runat="server">
            <div class="row justify-content-center table-responsive">

                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Empresa</label>
                    <div>
                        <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Año</label>
                    <div>
                        <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Periodo</label>
                    <div>
                        <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Reprocesar</label>
                    <div>
                        <asp:CheckBox ID="chkRepro" runat="server" />
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">&nbsp</label>
                    <div>
                        <asp:LinkButton ID="btnProcesar" CssClass="btn bg-red btn-block" runat="server">Generar</asp:LinkButton>

                    </div>
                </div>
            </div>

        </asp:Panel>
    </section>
</asp:Content>
