﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAdmPeriodoSeguros.aspx.vb" Inherits="aspSistemasJorquera.aspAdmPeriodoSeguros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">       
                <h1>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h1>
      
    </section>

    <section class="content">

        <div class="row justify-content-center table-responsive">

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">PERIODO</label>
                <div>
                    <asp:Label ID="lblPeriodo" runat="server" Text="" CssClass="form-control text-uppercase" Enabled="false"></asp:Label>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Año</label>
                <div>
                    <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-6">

                <asp:GridView ID="grdPeriodo" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="num_anio" HeaderText="Año" ReadOnly="True" SortExpression="num_anio">
                            <ItemStyle Width="460px" HorizontalAlign="left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="num_periodo" HeaderText="Periodo" ReadOnly="True" SortExpression="num_periodo">
                            <ItemStyle Width="460px" HorizontalAlign="left" />
                        </asp:BoundField>
                        <asp:TemplateField>

                            <ItemTemplate>                              
                                <asp:LinkButton ID="btnCostear"  runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Descargar" Width="18px"><i class="fa fa-fw fa-download"></i></asp:LinkButton>
                                <asp:HiddenField ID="hdnActivo" runat="server" Value='<%# Bind("bit_activo")%>' />
                                <asp:HiddenField ID="hdnCodigo" runat="server" Value='<%# Bind("cod_periodo")%>' />
                                <asp:HiddenField ID="hdnPeriodo" runat="server" Value='<%# Bind("num_periodo")%>' />
                            </ItemTemplate>


                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="20px" Wrap="False"  />
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </div>

        </div>
        <asp:LinkButton ID="btnCostear"  runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Descargar" Width="18px"><i class="fa fa-fw fa-download"></i></asp:LinkButton>
    </section>
    <asp:HiddenField ID="hdnActivo" runat="server" />
    <asp:HiddenField ID="hdnPeriodoCierre" runat="server" />
    <asp:HiddenField ID="hdnPeriodoAbir" runat="server" />
    <asp:HiddenField ID="hdnPeriodoActual" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIngresado" runat="server" Value="0" />
</asp:Content>
