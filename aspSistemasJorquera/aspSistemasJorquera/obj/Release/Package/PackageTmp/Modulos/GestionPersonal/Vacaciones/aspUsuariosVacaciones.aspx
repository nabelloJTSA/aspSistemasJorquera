﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspUsuariosVacaciones.aspx.vb" Inherits="aspSistemasJorquera.aspUsuariosVacaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Usuario Validador</label>
                <div>
                    <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                    <asp:DropDownList ID="cbobuscarnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>                    
                </div>
            </div>
              <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>                   
                    <asp:LinkButton ID="bntBuscarNombre" runat="server" CssClass="btn "><i class="fa fa-fw fa-search"></i></asp:LinkButton>       
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Mail</label>
                <div>
                    <asp:TextBox ID="txtMail" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:RadioButtonList ID="rblTipoPersonal" runat="server" AutoPostBack="True" CellPadding="3" CellSpacing="3" RepeatDirection="Horizontal" TextAlign="Left">
                        <asp:ListItem Value="1">Personal Conductor</asp:ListItem>
                        <asp:ListItem Value="2">Personal NO Conductor</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label"></label>
                <div class="custom-control custom-checkbox">                    
                </div>
            </div>

            <div class="col-md-8 form-group">
                <label for="fname" class="control-label col-form-label">Area/Faena</label>
                <div class="custom-control custom-checkbox">
                    <asp:CheckBoxList ID="chkAreas" runat="server" RepeatColumns="8" CellPadding="15" CellSpacing="15" RepeatDirection="Horizontal"></asp:CheckBoxList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label"></label>
                <div class="custom-control custom-checkbox">
                </div>
            </div>
        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                <div>
                    <asp:TextBox ID="txtbNombre" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-8 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGrabarMov" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnLimpiarMov" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

            <%--     <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>--%>
        </div>
        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">

                <asp:GridView ID="grdEncargados" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ButtonType="Image" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True"
                            ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="EstFilasGrilla">
                            <ItemStyle HorizontalAlign="Center" CssClass="EstFilasGrilla" Width="20px"></ItemStyle>
                        </asp:CommandField>
                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("usuario")%>' />
                                <asp:HiddenField ID="hdn_codusuario" runat="server" Value='<%# Bind("cod_usuario")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdn_tipo" runat="server" Value='<%# Bind("cod_tipo")%>'></asp:HiddenField>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Empresa">
                            <ItemTemplate>
                                <asp:Label ID="lblempresa" runat="server" Text='<%# Bind("empresa")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="210px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo Personal">
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("tipo_personal")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="210px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnCodUsu" runat="server" />
    <asp:HiddenField ID="hdnTipo" runat="server" />
</asp:Content>
