﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPlantillaEspecial.aspx.vb" Inherits="aspSistemasJorquera.aspPlantillaEspecial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Plantilla Especial"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                            <div>
                                <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <asp:Label ID="lblNom" runat="server" Text="Nombre:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Nombre:</label>--%>
                            <div>
                                <asp:Label ID="lblNombre" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <asp:Label ID="lblAct" runat="server" Text="Activa" Visible="False" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:CheckBox ID="chkActiva" runat="server" Text="" Visible="False" AutoPostBack="True" />
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <asp:Label ID="lblEmp" runat="server" Text="Empresa:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Empresa:</label>--%>
                            <div>
                                <asp:Label ID="lblEmpresa" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblSuc" runat="server" Text="Sucursal:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Sucursal:</label>--%>
                            <div>
                                <asp:Label ID="lblSucursal" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblCar" runat="server" Text="Cargo:" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Cargo:</label>--%>
                            <div>
                                <asp:Label ID="lblCargo" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblIC" runat="server" Text="I.C" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Cargo:</label>--%>
                            <div>
                                <asp:Label ID="lblInstrumento" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblSB" runat="server" Text="Sueldo Base ($):" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Empresa:</label>--%>
                            <div>
                                <asp:Label ID="lblSueldoBase" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <asp:Label ID="lblMA" runat="server" Text="Monto Anticipo ($):" Visible="False" Font-Bold="True"></asp:Label>
                            <%--<label for="fname" class="control-label col-form-label">Sucursal:</label>--%>
                            <div>
                                <asp:Label ID="lblAncipo" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlDet" runat="server" Visible="false">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4">

                                <asp:GridView ID="grdFijos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nom_item")%>' />
                                                <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_item")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Incluir">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIncluir" runat="server" AutoPostBack="true" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMonto" runat="server" CssClass="Border border rounded border-info text-info text-uppercase" Text='<%# Bind("monto", "{0:####,###,###,###,##.##}")%>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                            <div class="col-md-1">
                            </div>

                            <div class="col-md-4">

                                <asp:GridView ID="grdVariables" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nom_item")%>' />
                                                <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_item")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Incluir">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIncluir" runat="server" AutoPostBack="true" />

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Monto">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMonto" runat="server" CssClass="Border border rounded border-info text-info text-uppercase" Text='<%# Bind("monto", "{0:####,###,###,###,##.##}")%>'></asp:TextBox>

                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-6 form-group ">
                                <div>
                                </div>
                            </div>
                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:HiddenField ID="hdnDatosFijos" runat="server" Value="0" />
                <asp:HiddenField ID="hdnExisteDet" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdPlantilla" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodEmp" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodCargo" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodSucursal" runat="server" Value="0" />
                <asp:HiddenField ID="hdnCodInstrumento" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstValor" runat="server" Value="0" />
                <asp:HiddenField ID="hdnNumValor" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstFormula" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstSistema" runat="server" Value="0" />
                <asp:HiddenField ID="hdnEstIncluir" runat="server" Value="0" />
                <asp:HiddenField ID="hdnSueldoBase" runat="server" Value="0" />
                <asp:HiddenField ID="hdnTipoPlantilla" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                        <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>
</asp:Content>
