﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspSaldos.aspx.vb" Inherits="aspSistemasJorquera.aspSaldos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Saldos"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Area / Faena</label>
                <div>
                    <asp:DropDownList ID="cboAreaFaena" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnBuscar" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>

        </div>
        <asp:Panel ID="pnlSaldos" runat="server" Visible="false">
            <div class="row justify-content-center table-responsive">

                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Personal</label>
                    <div>
                        <asp:TextBox ID="txtbNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-2 form-group">
                    <label for="fname" class="control-label col-form-label">Rut</label>
                    <div>
                        <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                    </div>
                </div>

            </div>


            <div class="row justify-content-center table-responsive">
                <div class="col-md-12">
                    <div>
                        <asp:GridView ID="grdSaldos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="25" AllowPaging="True">
                            <Columns>

                                <asp:TemplateField HeaderText="Rut Personal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRutPersonal" runat="server" Text='<%# Bind("rut_personal")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre Personal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomPersonal" runat="server" Text='<%# Bind("nom_personal")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="400px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tipo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFInicio" runat="server" Text='<%# Bind("nom_tipo")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="200px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Saldo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFtermino" runat="server" Text='<%# Bind("num_saldo")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Periodo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDhabiles" runat="server" Text='<%# Bind("periodo")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="120px" HorizontalAlign="Center" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>
</asp:Content>
