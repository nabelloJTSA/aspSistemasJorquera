﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspInAreas.aspx.vb" Inherits="aspSistemasJorquera.aspInAreas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Área"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">

        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Nombre Area</label>
                <div>
                    <asp:TextBox ID="txtArea" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-4 form-group ">
                <div>
                </div>
            </div>


            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-1 form-group ">
                <div>
                    <asp:TextBox ID="txtbNombre" runat="server" placeHolder="Buscar área..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-5">

                <asp:GridView ID="grdArea" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="Nombre Area">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_area")%>' />
                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_area")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdnEmpresa" runat="server" Value='<%# Bind("cod_empresa")%>'></asp:HiddenField>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Empresa">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nomEmpresa")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeleteComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>

            </div>

        </div>
    </section>
    <asp:HiddenField ID="hdnIdItem" runat="server" />
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
</asp:Content>
