﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspListadoCharlas.aspx.vb" Inherits="aspSistemasJorquera.aspListadoCharlas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h1>
                    <asp:Label ID="lbltitulo" runat="server" Text="Listado de Charlas"></asp:Label>
                </h1>
            </div>
            <div class="col-md-2 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnListado" CssClass="btn bg-orange btn-block" PostBackUrl="~/Modulos/FormacionOnline/aspCharla.aspx" runat="server">Nueva Charla</asp:LinkButton>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Caegoría</label>
                <div>
                    <asp:DropDownList ID="cboCategoria" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Subcategoría</label>
                <div>
                    <asp:DropDownList ID="cboSubCategoria" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-pencil-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Por Asignar</span>
                        <asp:Label ID="lblPorASignar" runat="server" class="info-box-number"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-check"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Asignadas</span>
                        <asp:Label ID="lblAsignadas" runat="server" class="info-box-number"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-dot-circle-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total</span>
                        <asp:Label ID="lblTotal" runat="server" class="info-box-number"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

        </div>
        <div class="row">


            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Desde</label>
                <div>
                    <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Hasta</label>
                <div>
                    <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-4 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Charla</label>
                <div>
                    <asp:TextBox ID="txtNombreCharla" runat="server" placeholder="Nombre Charla..." MaxLength="1000" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    ESTADO</label>
                <div>
                    <asp:DropDownList ID="cboEstado" CssClass="form-control" runat="server">
                        <asp:ListItem Value="">Todas</asp:ListItem>
                        <asp:ListItem Value="S/A"> SIN ASIGNAR</asp:ListItem>
                        <asp:ListItem Value="A"> ASIGNADA</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <%--<div class="col-md-2 form-group">
                <label for="fname"
                    class="">
                </label>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="chkSinAsignar" runat="server" clientidmode="Static">
                    <label class="custom-control-label" for="chkSinAsignar">Sin Asignar</label>
                </div>
            </div>--%>
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnBuscar" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">
                <asp:GridView ID="grdListado" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="CHARLA">
                            <ItemTemplate>
                                <asp:Label ID="lblCharla" runat="server" Text='<%# Bind("nom_charla")%>'></asp:Label>
                                <asp:HiddenField ID="hdnidCharla" runat="server" Value='<%# Bind("id_charla")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CATEGORÍA">
                            <ItemTemplate>
                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SUBCATEGORÍA">
                            <ItemTemplate>
                                <asp:Label ID="lblSubcategoria" runat="server" Text='<%# Bind("nom_subcategoria")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Asignación">
                            <ItemTemplate>
                                <asp:Label ID="lblAsignacion" runat="server" Text='<%# Bind("nom_asignacion")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="VIDEO">
                            <ItemTemplate>
                                <asp:Label ID="lblVideo" runat="server" Text='<%# Bind("url_video")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ESTADO">
                            <ItemTemplate>
                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ACTIVA DESDE">
                            <ItemTemplate>
                                <asp:Label ID="lblActivaDesde" runat="server" Text='<%# Bind("fec_desde")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ACTIVA HASTA">
                            <ItemTemplate>
                                <asp:Label ID="lblActivaHasta" runat="server" Text='<%# Bind("fec_hasta")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CREADOR">
                            <ItemTemplate>
                                <asp:Label ID="lblCreador" runat="server" Text='<%# Bind("usr_add")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FECHA CREACIÓN">
                            <ItemTemplate>
                                <asp:Label ID="lblFecgaCreacion" runat="server" Text='<%# Bind("fec_add")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ASIGNAR">
                            <ItemTemplate>
                                <h4>
                                    <asp:LinkButton ID="btnAsignar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Asignar"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                </h4>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="EDITAR">
                            <ItemTemplate>
                                <h4>
                                    <asp:LinkButton ID="btnEditar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Editar"><i class="fa fa-pencil"></i></asp:LinkButton>
                                </h4>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ELIMINAR">
                            <ItemTemplate>
                                <h4>
                                    <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Eliminar" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-trash"></i></asp:LinkButton>
                                </h4>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                </asp:GridView>
            </div>
        </div>
    </section>
</asp:Content>

