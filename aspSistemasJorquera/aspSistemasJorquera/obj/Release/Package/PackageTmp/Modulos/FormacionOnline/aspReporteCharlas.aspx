﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspReporteCharlas.aspx.vb" Inherits="aspSistemasJorquera.aspReporteCharlas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-md-4 form-group">
                <h1>
                    <asp:Label ID="lbltitulo" runat="server" Text="Reporte de Charlas"></asp:Label>
                </h1>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-pencil-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Promedio</span>
                        <asp:Label ID="lblPromedio" runat="server" class="info-box-number" Text="0,0"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-pencil-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Respuestas %</span>
                        <asp:Label ID="lblPorResp" runat="server" class="info-box-number" Text="0,0 %"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-pencil-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Contestadas</span>
                        <asp:Label ID="lblContestadas" runat="server" class="info-box-number" Text="0"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>

            <div class="col-md-2 form-group pull-right">
                <div class="info-box">
                    <span class="info-box-icon"><i class="fa fa-pencil-square-o"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Impactados</span>
                        <asp:Label ID="lblImpactados" runat="server" class="info-box-number" Text="0"></asp:Label>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <%--inicio Filtros--%>
        <div class="row">
            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Área</label>
                <div>
                    <asp:DropDownList ID="cboArea" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Cargo</label>
                <div>
                    <asp:DropDownList ID="cboCargo" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Charla</label>
                <div>
                    <asp:DropDownList ID="cboCharla" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Categoría</label>
                <div>
                    <asp:DropDownList ID="cboCategoria" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Subcategoría</label>
                <div>
                    <asp:DropDownList ID="cboSubcategoria" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Rut</label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updRut" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase text-center" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-4 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Nombre</label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updNombre" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" style="height: 15px"
                    class="control-label col-form-label">
                </label>
                <div id="dvEncuestado" runat="server">
                    <button type="button" class="btn bg-blue btn-block" data-toggle="modal" data-target="#modalBuscarPersona" style="width: 50%">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Estado</label>
                <div>
                    <asp:DropDownList ID="cboEstado" CssClass="form-control" runat="server">
                        <asp:ListItem Value="T">Todas</asp:ListItem>
                        <asp:ListItem Value="1">FINALIZADA</asp:ListItem>
                        <asp:ListItem Value="0">NO FINALIZADA</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname" style="height: 15px"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 pull-right">
                <label for="fname" style="height: 15px"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server" ToolTip="Exportar"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>
            <div class="col-md-1 pull-right">
                <label for="fname" style="height: 15px"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnConsultar" CssClass="btn bg-orange btn-block" runat="server">Buscar</asp:LinkButton>
                </div>
            </div>
        </div>
        <%--Fin Filtros--%>

        <%--Grilla--%>
        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">
                <asp:GridView ID="grdListado" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="CHARLA">
                            <ItemTemplate>
                                <asp:Label ID="lblCharla" runat="server" Text='<%# Bind("nom_charla")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Categoria">
                            <ItemTemplate>
                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="250px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Subcategoria">
                            <ItemTemplate>
                                <asp:Label ID="lblSubategoria" runat="server" Text='<%# Bind("nom_subcategoria")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="250px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Fecha cierre">
                            <ItemTemplate>
                                <asp:Label ID="lblfec_cierre" runat="server" Text='<%# Bind("fec_cierre")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Hora cierre">
                            <ItemTemplate>
                                <asp:Label ID="lblhr_cierre" runat="server" Text='<%# Bind("hr_cierre")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Rut">
                            <ItemTemplate>
                                <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_encuestado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_encuestado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Empresa">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Área">
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nom_area")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Cargo">
                            <ItemTemplate>
                                <asp:Label ID="lblCargo" runat="server" Text='<%# Bind("nom_cargo")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("est_finalizado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Nota">
                            <ItemTemplate>
                                <asp:Label ID="lblNota" runat="server" Text='<%# Bind("nota")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                </asp:GridView>
            </div>
        </div>
        <%--Fin GRilla--%>
        <!-- Modal -->
        <div class="modal" id="modalBuscarPersona" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalBuscarPersonaLabel" aria-hidden="true">
            <div class="modal-dialog  modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-md-3">
                                <h5 class="modal-title" id="modalClientesLabel">Buscar Persona</h5>
                            </div>
                            <div class="col-md-1 pull-right">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel runat="server" ID="updClientes">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarPersona" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="row justify-content-center">
                                    <div class="col-md-10 form-group justify-content-center">
                                        <asp:TextBox ID="txtBuscar" runat="server" placeHolder="Rut o Nombre..." CssClass="form-control text-uppercase border-danger shadow" MaxLength="1000" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2 form-group justify-content-end">
                                        <asp:LinkButton ID="btnBuscarPersona" runat="server" CssClass="btn bg-blue btn-block"><i class="fa fa-search"></i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-12 form-group justify-content-center">
                                        <asp:GridView ID="grdPersona" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnSeleccion" runat="server" OnClientClick="$('#modalBuscarPersona').modal('hide');" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="btnSeleccion"><i class="fa fa-arrow-left"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Rut">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRutPersona" runat="server" Text='<%# Eval("rut_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNombrePersona" runat="server" Text='<%# Eval("nom_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <%--fin modal--%>
    </section>
</asp:Content>
