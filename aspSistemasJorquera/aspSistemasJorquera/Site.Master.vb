﻿Imports Microsoft.AspNet.Identity
Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Net.Mail

Public Class SiteMaster
    Inherits MasterPage
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Private strCnxQA As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String

    Protected Sub Page_Init(sender As Object, e As EventArgs)
        ' El código siguiente ayuda a proteger frente a ataques XSRF
        Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
        Dim requestCookieGuidValue As Guid
        If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then
            ' Utilizar el token Anti-XSRF de la cookie
            _antiXsrfTokenValue = requestCookie.Value
            Page.ViewStateUserKey = _antiXsrfTokenValue
        Else
            ' Generar un nuevo token Anti-XSRF y guardarlo en la cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
            Page.ViewStateUserKey = _antiXsrfTokenValue

            Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With {
                 .HttpOnly = True,
                 .Value = _antiXsrfTokenValue
            }
            If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then
                responseCookie.Secure = True
            End If
            Response.Cookies.[Set](responseCookie)
        End If

        AddHandler Page.PreLoad, AddressOf master_Page_PreLoad
    End Sub

    Protected Sub master_Page_PreLoad(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            ' Establecer token Anti-XSRF
            ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
            ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)
        Else
            ' Validar el token Anti-XSRF
            If DirectCast(ViewState(AntiXsrfTokenKey), String) <> _antiXsrfTokenValue OrElse DirectCast(ViewState(AntiXsrfUserNameKey), String) <> (If(Context.User.Identity.Name, [String].Empty)) Then
                Throw New InvalidOperationException("Error de validación del token Anti-XSRF.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Unnamed_LoggingOut(sender As Object, e As LoginCancelEventArgs)
        Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie)
    End Sub
    Private Sub FechaActual()
        Dim conx As New SqlConnection(strCnxQA)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_ingreso_demanda 'HAC'"
            Using cnxAcceso As New SqlConnection(strCnxQA)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Session("fec_actual") = rdoReader(0).ToString
                    Session("primer_dia") = rdoReader(1).ToString
                    Session("ultimo_dia") = rdoReader(2).ToString
                    Session("primer_dia_semana_actual") = rdoReader(3).ToString
                    Session("ultimo_dia_semana_actual") = rdoReader(4).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("FechaActual", Err, Page, Master)
        End Try
    End Sub


    Private Sub PermisosTorreControl(ByVal usuario As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec sel_tdi_usuario 'RDM', '" & usuario & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Session("PermisosTorreControl") = 1
                Else
                    Session("PermisosTorreControl") = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("PermisosTorreControl", Err, Page, Master)
        End Try
    End Sub

    Private Sub ModicarRutLogin()
        txtUsuario.Value = Replace(txtUsuario.Value, ".", "")
        txtUsuario.Value = Replace(txtUsuario.Value, "-", "")
        txtUsuario.Value = Trim(txtUsuario.Value)

        If Len(txtUsuario.Value) = 8 Then
            txtUsuario.Value = Left(txtUsuario.Value, 7) + "-" + Right(txtUsuario.Value, 1)
        ElseIf Len(txtUsuario.Value) = 9 Then
            txtUsuario.Value = Left(txtUsuario.Value, 8) + "-" + Right(txtUsuario.Value, 1)
        End If

        If Len(txtUsuario.Value) = 10 Then
            txtUsuario.Value = Trim(txtUsuario.Value)
        ElseIf Len(txtUsuario.Value) = 9 Then
            txtUsuario.Value = "0" + Trim(txtUsuario.Value)
        End If
    End Sub

    Private Sub ModicarRutRecuperar()
        txtRutRecuperar.Text = Replace(txtRutRecuperar.Text, ".", "")
        txtRutRecuperar.Text = Replace(txtRutRecuperar.Text, "-", "")
        txtRutRecuperar.Text = Trim(txtRutRecuperar.Text)

        If Len(txtRutRecuperar.Text) = 8 Then
            txtRutRecuperar.Text = Left(txtRutRecuperar.Text, 7) + "-" + Right(txtRutRecuperar.Text, 1)
        ElseIf Len(txtRutRecuperar.Text) = 9 Then
            txtRutRecuperar.Text = Left(txtRutRecuperar.Text, 8) + "-" + Right(txtRutRecuperar.Text, 1)
        End If

        If Len(txtRutRecuperar.Text) = 10 Then
            txtRutRecuperar.Text = Trim(txtRutRecuperar.Text)
        ElseIf Len(txtRutRecuperar.Text) = 9 Then
            txtRutRecuperar.Text = "0" + Trim(txtRutRecuperar.Text)
        End If
    End Sub


    Public Sub ValidarUsuario()
        Call FechaActual()

        ' Session("cod_modulo_tc") = 29

        Dim cmdTemporal As SqlCommand, blnError As Boolean = False
        Dim rdoReader As SqlDataReader
        ' Dim strSQL As String = "Exec sel_tdi_usuario 'BU','" & Trim(txtUsuario.Value) & "', '" & EncryptTripleDES(Trim(txtContraseña.Value)) & "', '" & Session("cod_modulo_tc") & "'"
        Dim strSQL As String = "Exec sel_tdi_usuario 'BUN','" & Trim(txtUsuario.Value) & "', '" & EncryptTripleDES(Trim(txtContraseña.Value)) & "'"

        Dim strCodUsuario As String = ""
        Dim strRutUsuario As String = ""
        Dim strIdUsuario As String = ""
        Dim strMailUsuario As String = ""
        Dim blnBloqueo As Boolean = False
        Dim strNomUsuario As String = ""
        Dim strIDPerfil As String = ""

        Dim strCodViatrack As String = ""
        Dim strNomViatrack As String = ""

        If Trim(txtUsuario.Value) <> "" And Trim(txtContraseña.Value) <> "" Then
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        blnError = True
                        strCodUsuario = rdoReader(0).ToString
                        strRutUsuario = rdoReader(1).ToString
                        strIdUsuario = rdoReader(2).ToString
                        strMailUsuario = rdoReader(3).ToString
                        blnBloqueo = rdoReader(4).ToString
                        strNomUsuario = rdoReader(5).ToString
                        'strIDPerfil = rdoReader(6).ToString
                        strCodViatrack = rdoReader(8).ToString
                        strNomViatrack = rdoReader(9).ToString

                    Else
                        '  Call ValidarCliente()
                        MessageBox("Inicio Sesión", "La identificación del usuario es incorrecta.", Page, Me, "E")
                        Exit Sub
                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                    rdoReader = Nothing
                    cmdTemporal = Nothing

                    If blnError Then
                        Session("cod_usu_tc") = strCodUsuario
                        Session("rut_usu_tc") = strRutUsuario
                        Session("id_usu_tc") = If(Trim(UCase(strIdUsuario)) = UCase(Trim(txtUsuario.Value)), UCase(Trim(txtUsuario.Value)), Trim(UCase(strIdUsuario)))
                        Session("mail_usu_tc") = strMailUsuario
                        Session("nom_usu_tc") = UCase(Trim(strNomUsuario))
                        Session("est_bloque_tc") = IIf(blnBloqueo = True, "S", "N")
                        'Session("id_perfil_tc") = strIDPerfil
                        Session("id_perfil_tc") = 1
                        If Not blnBloqueo Then
                            Call PermisosTorreControl(strCodUsuario)
                            Response.Redirect("~/aspHome.aspx")
                        Else
                            txtUsuario.Value = ""
                            txtContraseña.Value = ""
                            MessageBox("Inicio Sesión", "Cuenta Bloqueada, verifíquelo con el Administrador.", Page, Me, "W")
                        End If
                    Else
                        MessageBox("Inicio Sesión", "La identificación del usuario es incorrecta.", Page, Me, "E")
                    End If
                Catch ex As Exception
                    MessageBoxError("ValidarUsuario", Err, Page, Me)
                    Exit Sub
                Finally
                    cnxAcceso.Close()
                End Try
            End Using
        End If
    End Sub

    Public Sub ValidarCliente()
        Call ModicarRutLogin()

        Dim cmdTemporal As SqlCommand, blnError As Boolean = False
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec sel_tdi_usuario 'VUC', '" + Trim(txtUsuario.Value) + "','" + EncryptTripleDES(Trim(txtContraseña.Value)) + "'"

        Dim strCodUsuario As String = ""
        Dim strNomCliente As String = ""
        Dim srtRutcliente As String = ""
        Dim strDirCliente As String = ""
        Dim strMailcliente As String = ""
        Dim strFonocliente As String = ""
        Dim strIntentosClientes As String = ""
        Dim strBloqueoCliente As String = ""
        Dim strPrimerIngresoCliente As Boolean

        If Trim(txtUsuario.Value) <> "" And Trim(txtContraseña.Value) <> "" Then
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                'Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    blnError = True
                    strCodUsuario = rdoReader(1).ToString
                    srtRutcliente = rdoReader(2).ToString
                    strNomCliente = rdoReader(3).ToString
                    strDirCliente = rdoReader(4).ToString
                    strFonocliente = rdoReader(5).ToString
                    strMailcliente = rdoReader(6).ToString
                    strIntentosClientes = rdoReader(8).ToString
                    strBloqueoCliente = rdoReader(10).ToString
                    strPrimerIngresoCliente = rdoReader(12).ToString
                End If

                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing

                If blnError Then
                    Session("cod_usu_tc") = strCodUsuario
                    Session("rut_usu_tc") = srtRutcliente
                    Session("nom_usu_tc") = UCase(Trim(strNomCliente))
                    Session("nom_direccion_cliente_tc") = strDirCliente
                    Session("num_fono_cliente_tc") = strFonocliente
                    Session("num_intentos_cliente_tc") = strIntentosClientes
                    Session("est_bloque_tc") = strBloqueoCliente
                    Session("est_primer_ingreso_cliente_tc") = strPrimerIngresoCliente
                    Session("nom_mail_cliente_tc") = strMailcliente
                    Session("id_perfil_tc") = 4

                    If strBloqueoCliente = False Then
                        Call FechaActual()

                        If strPrimerIngresoCliente = True Then
                            pnlIniciSesion.Visible = False
                            pnlPrimerIngreso.Visible = True
                        Else
                            Response.Redirect("~/Default.aspx")
                        End If
                    Else
                        txtUsuario.Value = ""
                        txtContraseña.Value = ""
                        MessageBox("Inicio Sesión", "Cuenta Bloqueada, verifíquelo con el Administrador.", Page, Me, "W")
                    End If
                Else
                    MessageBox("Inicio Sesión", "Datos de Acceso incorrectos", Page, Me, "W")
                End If
                'Catch ex As Exception
                '    MessageBoxError("ValidarUsuario", Err, Page, Me)
                '    Exit Sub
                'Finally
                '    cnxAcceso.Close()
                'End Try
            End Using
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        Call ValidarUsuario()
    End Sub

    Private Sub btnSalircliente_Click(sender As Object, e As EventArgs) Handles btnSalirCliente.Click
        pnlIniciSesion.Visible = True
        pnlCliente.Visible = False
        Session.Clear()
        Session.Abandon()
        FormsAuthentication.SignOut()
        Response.Redirect("~/Default.aspx")
    End Sub


    Private Sub btnSalirMonitorista_Click(sender As Object, e As EventArgs) Handles btnSalirMonitorista.Click
        pnlIniciSesion.Visible = True
        PnlMenu.Visible = False
        Session.Clear()
        Session.Abandon()
        FormsAuthentication.SignOut()
        Response.Redirect("~/Default.aspx")
    End Sub




    ' ------------ PRIMER INGRESO
    Private Sub ActualizarPass()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 9000000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "sel_tdi_usuario"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UPI"
            comando.Parameters.Add("@nom_contraseña", SqlDbType.NVarChar)
            comando.Parameters("@nom_contraseña").Value = Trim(EncryptTripleDES(txtPass2.Text))
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarPass", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardarPass.Click
        If Trim(txtPass1.Text) = "" Or Trim(txtPass2.Text) = "" Then
            MessageBox("Cambio Contraseña", "Complete los campos requeridos", Page, Me, "W")
        Else
            If Trim(txtPass1.Text) = Trim(txtPass2.Text) Then
                Call ActualizarPass()
                Response.Redirect("~/Default.aspx")
            Else
                MessageBox("Cambio Contraseña", "Las contraseñas no coinciden.", Page, Me, "W")
            End If
        End If
    End Sub



    '-------------RECUPERAR CONTRASEÑA


    Protected Sub btnRecuPass_Click(sender As Object, e As EventArgs) Handles btnRecuPass.Click
        pnlIniciSesion.Visible = False
        pnlRecuperarContraseña.Visible = True
    End Sub

    Protected Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        pnlIniciSesion.Visible = True
        pnlRecuperarContraseña.Visible = False
        txtRutRecuperar.Text = ""
    End Sub

    Function GenerarPass(ByVal longitud As Integer)
        Dim caracteres As String = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789"
        Dim res As StringBuilder = New StringBuilder()
        Dim rnd As Random = New Random()
        While 0 < Math.Max(System.Threading.Interlocked.Decrement(longitud), longitud + 1)
            res.Append(caracteres(rnd.[Next](caracteres.Length)))
        End While
        Return res.ToString
    End Function

    Private Sub ActualizarPassRecuperar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 9000000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "sel_tdi_usuario"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "APA"
            comando.Parameters.Add("@nom_contraseña", SqlDbType.NVarChar)
            comando.Parameters("@nom_contraseña").Value = EncryptTripleDES(Trim(hdnPass.Value))
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(txtRutRecuperar.Text)

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Call EnviarCorreo()
            Session("reset_pass") = 1
            Session("mail_reset_pass") = hdnMail.Value
            Response.Redirect("~/Default.aspx")
        Catch ex As Exception
            MessageBoxError("ActualizarPass", Err, Page, Master)
        End Try
    End Sub

    Private Sub ExisteJTSA()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        ' Try
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec sel_tdi_usuario 'VJT', '" & Trim(txtRutRecuperar.Text) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnMail.Value = rdoReader(0).ToString
                hdnNombre.Value = rdoReader(1).ToString
                hdnExiste.Value = 1
            Else
                hdnExiste.Value = 0
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            rdoReader = Nothing
            cmdTemporal = Nothing
        End Using
        'Catch ex As Exception
        '    MessageBoxError("Existe", Err, Page, Master)
        'End Try
    End Sub

    Private Sub ExisteCliente()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        ' Try
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec sel_tdi_usuario 'VME', '" & Trim(txtRutRecuperar.Text) & "'" ' actualizar clave y agregar campos a la tabla usuarios jtsa
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnMail.Value = rdoReader(0).ToString
                hdnNombre.Value = rdoReader(1).ToString
                hdnExiste.Value = 1
            Else
                hdnExiste.Value = 0
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            rdoReader = Nothing
            cmdTemporal = Nothing
        End Using
        'Catch ex As Exception
        '    MessageBoxError("Existe", Err, Page, Master)
        'End Try
    End Sub

    Protected Sub btnRecuperar_Click(sender As Object, e As EventArgs) Handles btnRecuperar.Click
        Call ModicarRutRecuperar()
        Call ExisteCliente()

        If hdnExiste.Value = 1 Then
            If Trim(hdnMail.Value) = "" Then
                MessageBox("Recuperar", "Usuario NO posee mail registrado en la plataforma", Page, Me, "W")
            Else
                hdnPass.Value = GenerarPass(8)
                Call ActualizarPassRecuperar()
            End If
        Else
            MessageBox("Recuperar", "Usuario no Existe en Plataforma.", Page, Me, "W")
        End If
    End Sub

    Private Sub EnviarCorreo()
        Try
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add(Trim(hdnMail.Value))

            correo.Subject = "TORRE DE CONTROL"
            correo.Body = " 
            <table border ='0' width='100%' height='100%' cellpadding='0' cellspacing='0' bgcolor='#CFF0F1'>
            <tr><td align='center' valign='top' bgcolor='#CFF0F1' style='background-color: #CFF0F1;'>
                <br><table border='0' width='600' cellpadding='0' cellspacing='0' class='container' style='width:600px;max-width:600px'>
                    <tr><td align='left' bgcolor='#12A7AB' Class='container-padding header' style='font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#FFFFFF;padding-left:24px;padding-right:24px'>
                        <p align='center'>   </p><p align='center'>Creación Usuario</p></td></tr><tr>
                        <td Class='container-padding content' align='left' style='padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff'>
                            <br><div class='body-text' style='font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333'>
                                <p>Estimado/a.<br/>" & Trim(hdnNombre.Value.ToUpper) & ".</p></div>
                                <div class='body-text' style='font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333'>
                                    <p>Ha sido creado en la Plataforma con el perfil de <strong>" & Trim(hdnPerfil.Value.ToUpper) & "</strong>.</p>
                                    <p>Para ingresar a la plataforma, haz click en el siguiente enlace desde cualquier dispositivo móvil <em><strong> http://www..cl/ </strong></em> con los siguientes datos de acceso,</p>
                                   
                                    <p> Tus datos de Acceso son:</p><table width='' border='0'><tr>
                                        <td align='right' style='font-size:16px;padding-right: 10px;'><strong>Usuario:</strong></td>
                                        <td align='left' style='font-size:16px;'><strong>" & Trim(txtRutRecuperar.Text) & "</strong></td>
                                        </tr>
                                        <tr><td align='right' style='font-size:16px;padding-right: 10px;'><strong>Clave:</strong></td>
                                        <td align='left' style='font-size:16px;'><strong>" & hdnPass.Value & "</strong></td></tr>
                                       
                                        <br><br>
                                    </table><p><i>Este correo es enviado de forma automatica por lo que no sera respondido</i></p></div></td></tr><tr>
                        <td class='container-padding footer-text' align='left' style='font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px'>
                            
            </td></tr></table></td></tr></table></body></html>"

            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Port = 25
            smtp.Host = "mail.viatrack.cl"
            smtp.EnableSsl = False
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()

        Catch ex As Exception
            MessageBoxError("EnviarCorreo", Err, Page, Master)
        End Try
    End Sub


End Class