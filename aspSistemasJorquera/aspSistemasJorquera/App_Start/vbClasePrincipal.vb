﻿Imports System.Security.Cryptography
Imports System.Data.SqlClient
Imports System.Net.Mail


Public Class vbClasePrincipal
    Const sKey As String = "MyKey"
    Shared strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())


    Shared Sub RescatarPermisosEspeciales(ByVal master As Object, ByVal modulo As String, ByVal usuario As String)
        Dim NombreMenu As String
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec sel_tdi_usuario 'RP', '" & modulo & "', '" & usuario & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
            rdoReader = cmdTemporal.ExecuteReader()
            Do While (rdoReader.Read())
                NombreMenu = "" & rdoReader(0).ToString() & ""
                CType(master.FindControl(NombreMenu), LinkButton).Visible = False
            Loop
        End Using
    End Sub

    Shared Sub PanelesInicio(ByVal master As Object, ByVal Inicio As String, ByVal nombre As String, ByVal usuario As String)
        If usuario = "" Then
            HttpContext.Current.Response.Redirect("~/Default.aspx")
            CType(master.FindControl("pnlIniciSesion"), Panel).Visible = True
        Else
            If Inicio = 1 Then
                CType(master.FindControl("pnlIniciSesion"), Panel).Visible = False
                CType(master.FindControl("pnlCliente"), Panel).Visible = False
                CType(master.FindControl("PnlMenu"), Panel).Visible = True
                Call GenerarMenu(master, usuario)
                CType(master.FindControl("lblLoginMonitorista"), Label).Text = nombre
                CType(master.FindControl("lblLogin"), Label).Text = nombre
            End If
        End If
    End Sub

    Shared Sub GenerarMenu(ByVal master As Object, ByVal cod_usuario As Integer)
        Try
            Dim cnxBD As New SqlConnection
            cnxBD.ConnectionString = Trim(strCnx)
            Dim ConsultaSql As String = "exec mant_TDI_MenuDinamico 'CMO', " & cod_usuario & ""
            Dim da As SqlDataAdapter = New SqlDataAdapter(ConsultaSql, cnxBD)
            Dim dtModulos As DataTable = New DataTable()
            da.Fill(dtModulos)
            Dim modulos As HtmlGenericControl = UList("Menuid", "sidebar-menu tree")
            For Each row As DataRow In dtModulos.Rows
                ConsultaSql = "exec mant_TDI_MenuDinamico 'CME', " & cod_usuario & " , " & row("id_modulo").ToString() & ""
                da = New SqlDataAdapter(ConsultaSql, cnxBD)
                Dim dtMenu As DataTable = New DataTable()
                da.Fill(dtMenu)
                Dim sub_menu As HtmlGenericControl = LIListModulos(row("nom_modulo").ToString(), row("id_modulo").ToString(), "#", row("abr_modulo").ToString())
                Dim ul As HtmlGenericControl = New HtmlGenericControl("ul")

                ul.Attributes.Add("class", "treeview-menu")
                For Each r As DataRow In dtMenu.Rows
                    Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                    li.Attributes.Add("rel", r("id_menu").ToString())
                    Dim URL As String = ""
                    Dim html As String = ""
                    Dim conx As New SqlConnection(strCnx)
                    Dim rdoReader As SqlDataReader
                    Dim cmdTemporal As SqlCommand, comando As New SqlCommand
                    Dim strSQL As String = "exec mant_TDI_MenuDinamico 'URL', " & cod_usuario & " , " & row("id_modulo").ToString() & " , " & r("id_menu").ToString() & " "
                    Using cnxBaseDatos As New SqlConnection(strCnx)
                        cnxBaseDatos.Open()
                        comando = New SqlCommand(strSQL, cnxBaseDatos)
                        rdoReader = comando.ExecuteReader()
                        If rdoReader.Read Then
                            URL = rdoReader(4)
                            html = "<a href=""\Modulos\" & URL & """><i class=""fa fa-circle-o""></i>" & r("nom_menu").ToString() & "</a>"
                        Else
                            li.Attributes.Add("class", "treeview")
                            URL = "#"
                            html = "<a href=" & URL & "><i class=""fa fa-circle-o""></i>" & r("nom_menu").ToString() & "<span Class=""pull-right-container""> <i Class=""fa fa-angle-left pull-right""></i></span></a>"
                        End If
                        rdoReader.Close()
                        rdoReader = Nothing
                        comando.Dispose()
                        cmdTemporal = Nothing
                    End Using
                    li.InnerHtml = html
                    ConsultaSql = "exec mant_TDI_MenuDinamico 'CSM', " & cod_usuario & " , " & row("id_modulo").ToString() & " , " & r("id_menu").ToString() & ""
                    da = New SqlDataAdapter(ConsultaSql, cnxBD)
                    Dim dtSubMenu As DataTable = New DataTable()
                    da.Fill(dtSubMenu)
                    Dim ulSubMenu As HtmlGenericControl = New HtmlGenericControl("ul")
                    ulSubMenu.Attributes.Add("class", "treeview-menu")
                    For Each rSubMenu As DataRow In dtSubMenu.Rows
                        ulSubMenu.Controls.Add(LIListMenu(rSubMenu("nom_submenu").ToString(), rSubMenu("id_submenu").ToString(), row("id_modulo").ToString(), rSubMenu("id_submenu").ToString(), r("id_menu").ToString(), cod_usuario))
                    Next
                    li.Controls.Add(ulSubMenu)
                    ul.Controls.Add(li)
                Next
                sub_menu.Controls.Add(ul)
                modulos.Controls.Add(sub_menu)
            Next
            CType(master.FindControl("Panel1"), Panel).Controls.Add(modulos)
        Catch ex As Exception
        End Try
    End Sub



    Shared Function UList(ByVal id As String, ByVal cssClass As String) As HtmlGenericControl
        Dim ul As HtmlGenericControl = New HtmlGenericControl("ul")
        ul.ID = id
        ul.Attributes.Add("class", cssClass)
        ul.Attributes.Add("data-widget", "tree")
        Return ul
    End Function

    Shared Function LIListModulos(ByVal innerHtml As String, ByVal rel As String, ByVal url As String, ByVal icono As String) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        li.Attributes.Add("rel", rel)
        li.Attributes.Add("class", "treeview")
        Dim html As String = "<a href=""#"" class=""ah_check_tooltip""><i class=""" & icono & """></i><span>" & innerHtml & "</span><span Class=""pull-right-container""> <i Class=""fa fa-angle-left pull-right""></i></span></a>"
        li.InnerHtml = html
        Return li
    End Function


    Shared Function LIListMenu(ByVal innerHtml As String, ByVal rel As String, ByVal id_modulo As Integer, ByVal id_submenu As Integer, ByVal id_menu As Integer, ByVal cod_usuario As Integer) As HtmlGenericControl
        Dim li As HtmlGenericControl = New HtmlGenericControl("li")
        Dim html As String = ""
        li.Attributes.Add("rel", rel)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "exec mant_TDI_MenuDinamico 'URL2', " & cod_usuario & " , " & id_modulo & " , " & id_submenu & " , " & id_menu & " "
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            comando = New SqlCommand(strSQL, cnxBaseDatos)
            rdoReader = comando.ExecuteReader()
            If rdoReader.Read Then
                html = "<a href=""\Modulos\" & rdoReader(4) & """><i class=""fa fa-circle-o""></i>" & innerHtml & "</a>"
            Else
                html = "<a href=""#""><i class=""fa fa-file-text-o""></i>" & innerHtml & "</a>"
            End If
            rdoReader.Close()
            rdoReader = Nothing
            comando.Dispose()
            cmdTemporal = Nothing
        End Using
        li.InnerHtml = html
        Return li
    End Function

    Shared Sub CargarMenu(ByVal master As Object, ByVal perfil As String, ByVal nombre As String, ByVal modulo As String, ByVal usuario As String)
        If usuario = "" Then
            HttpContext.Current.Response.Redirect("~/Default.aspx")
            CType(master.FindControl("pnlIniciSesion"), Panel).Visible = True
        Else
            If perfil = 1 Then
                CType(master.FindControl("pnlIniciSesion"), Panel).Visible = False
                CType(master.FindControl("pnlCliente"), Panel).Visible = True
                'CType(master.FindControl("pnlUsuario"), Panel).Visible = False
                'CType(master.FindControl("pnlUserEspecial"), Panel).Visible = False
                'CType(master.FindControl("pnlIniciSesion"), Panel).Visible = False
                CType(master.FindControl("lblLoginCliente"), Label).Text = nombre
                CType(master.FindControl("lblLoginCliente2"), Label).Text = nombre

            ElseIf perfil = 2 Then
                CType(master.FindControl("pnlIniciSesion"), Panel).Visible = False
                CType(master.FindControl("pnlCliente"), Panel).Visible = False
                CType(master.FindControl("pnlMonitorista"), Panel).Visible = True

                CType(master.FindControl("lblLoginMonitorista"), Label).Text = nombre
                CType(master.FindControl("lblLoginMonitorista2"), Label).Text = nombre

            ElseIf perfil = 4 Then
                CType(master.FindControl("pnlIniciSesion"), Panel).Visible = False
                CType(master.FindControl("pnlCliente"), Panel).Visible = True
                CType(master.FindControl("pnlMonitorista"), Panel).Visible = False

                CType(master.FindControl("lblLoginCliente"), Label).Text = nombre
                CType(master.FindControl("lblLoginCliente2"), Label).Text = nombre
            End If
        End If
    End Sub

    Shared Function ValidarRut(ByRef objRut As Object) As Boolean
        Dim suma As Integer, intValor As Byte = 2, intResto As Byte, strRut As String
        Dim intLargoRut As Integer = 0, intCol As Integer, intResultado As Integer
        Dim strDigVer As String = "", strDigRes As String = ""
        Dim blnCorrecto As Boolean = True
        Try
            If Not String.IsNullOrEmpty(objRut.text) Then
                strRut = DelCaracter(objRut.text)
                intLargoRut = Len(strRut)
                For intCol = intLargoRut To 1 Step -1
                    If Not (intCol = intLargoRut) Then
                        suma = suma + Val(Mid(strRut, intCol, 1)) * intValor
                        intValor = intValor + 1
                        If intValor > 7 Then intValor = 2
                    ElseIf (intCol = intLargoRut) Then
                        strDigVer = UCase(Mid(strRut, intCol, 1))
                    End If
                Next
                intResto = suma Mod 11
                intResultado = 11 - intResto
                strDigRes = Trim(CStr(intResultado))
                strDigRes = IIf(strDigRes = "11", "0",
                                   IIf(strDigRes = "10", "K", strDigRes))
                If (Trim(strDigRes) <> Trim(strDigVer)) Then blnCorrecto = False
                strRut = Right("0", (9 - Len(strRut))) & Trim(strRut)
                objRut.text = Mid(strRut, 1, 8) & "-" &
                                  Mid(strRut, 9, 1)
            End If
        Catch ex As Exception
            blnCorrecto = False
        End Try
        Return blnCorrecto
    End Function

    '***************************************************************************************
    'Propósito  : Permite eliminar caracteres especiales al valor ingresado.
    '***************************************************************************************

    Shared Function DelCaracter(ByVal strValor As String) As String
        Dim strValorCorregido As String = ""
        strValor = Replace(strValor, ".", "")
        strValor = Replace(strValor, ",", "")
        strValor = Replace(strValor, "-", "")
        strValor = Replace(strValor, "/", "")
        strValor = Replace(strValor, "\", "")
        strValor = Replace(strValor, "+", "")
        strValor = Replace(strValor, "*", "")
        strValor = Replace(strValor, "%", "")
        strValor = Replace(strValor, "$", "")
        strValor = Replace(strValor, "#", "")
        strValor = Replace(strValor, "(", "")
        strValor = Replace(strValor, ")", "")
        strValor = Replace(strValor, "{", "")
        strValor = Replace(strValor, "}", "")
        strValor = Replace(strValor, "[", "")
        strValor = Replace(strValor, "]", "")
        strValor = Replace(strValor, "?", "")
        strValor = Replace(strValor, "¿", "")
        strValor = Replace(strValor, "&", "")
        strValor = Replace(strValor, "'", "")
        strValor = Replace(strValor, """", "")
        Return strValor
    End Function



    '***************************************************************************************
    'Propósito  : Permite eliminar caracteres especiales al valor ingresado.
    '***************************************************************************************

    Shared Function RepCaracter(ByVal strValor As String) As String
        Dim strValorCorregido As String = ""
        strValor = Replace(strValor, "Á", "A")
        strValor = Replace(strValor, "É", "E")
        strValor = Replace(strValor, "Í", "I")
        strValor = Replace(strValor, "Ó", "O")
        strValor = Replace(strValor, "Ú", "U")
        strValor = Replace(strValor, "á", "a")
        strValor = Replace(strValor, "é", "e")
        strValor = Replace(strValor, "í", "i")
        strValor = Replace(strValor, "ó", "o")
        strValor = Replace(strValor, "ú", "u")
        Return strValor
    End Function

    Shared Sub SentMail(ByVal MailTo As String, ByVal MailCC As String, ByVal MailSubject As String, ByVal MailBody As String)
        Dim correo As New MailMessage
        correo.From = New MailAddress("sistemas@viatrack.cl")
        correo.To.Add(Trim(MailTo))
        If MailCC <> "" Then
            correo.CC.Add(Trim(MailCC))
        End If
        correo.Subject = MailSubject
        correo.Body = MailBody
        correo.IsBodyHtml = True
        correo.Priority = MailPriority.Normal
        Dim smtp As New SmtpClient
        smtp.Host = "mail.viatrack.cl"
        smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
        smtp.Send(correo)
        correo.Attachments.Clear()
        correo.Dispose()
    End Sub

    '***************************************************************************************
    'Propósito  : Permite encryptar la información.
    '***************************************************************************************

    Shared Function EncryptTripleDES(ByVal sIn As String) As String
        Dim DES As New TripleDESCryptoServiceProvider()
        Dim hashMD5 As New MD5CryptoServiceProvider()
        DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey))
        DES.Mode = CipherMode.ECB
        Dim DESEncrypt As ICryptoTransform = DES.CreateEncryptor()
        Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(sIn)
        Return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    End Function

    '***************************************************************************************
    'Propósito  : Permite desencryptar la información.
    '***************************************************************************************

    Shared Function DecryptTripleDES(ByVal sOut As String) As String
        Dim DES As New TripleDESCryptoServiceProvider()
        Dim hashMD5 As New MD5CryptoServiceProvider()
        DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(sKey))
        DES.Mode = CipherMode.ECB
        Dim DESDecrypt As ICryptoTransform = DES.CreateDecryptor()
        Dim Buffer As Byte() = Convert.FromBase64String(sOut)
        Return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    End Function

    '***************************************************************************************
    'Propósito  : Permite cargar control DataSet con los registros asociados a la busqueda,
    '             y que será el que llenará el combo asociado.
    '***************************************************************************************

    Shared Function dtsTablas(ByVal strSQL As String,
                                  ByVal strTabla As String, Optional ByVal strNomCnxBD As String = "cnxBaseDatos",
                                  Optional ByRef intRegistros As Integer = -1) As DataSet
        Dim strCnx As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings(strNomCnxBD).ConnectionString()
        Dim rdoReader As SqlDataAdapter
        Dim objDataSet As New DataSet
        Dim cnxAcceso As SqlConnection = New SqlConnection(DecryptTripleDES(strCnx))
        cnxAcceso.Open()
        rdoReader = New SqlDataAdapter(strSQL, cnxAcceso)
        rdoReader.SelectCommand.CommandTimeout = 2000
        rdoReader.Fill(objDataSet, strTabla)
        intRegistros = objDataSet.Tables(strTabla).Rows.Count
        cnxAcceso.Close()
        cnxAcceso = Nothing
        Return objDataSet
    End Function


    Shared Sub MessageBox(ByVal strTitulo As String, ByVal strMensaje As String, ByVal objPage As Object, ByVal master As Object, ByVal strTipo As String)
        If strTipo = "I" Then
            strTipo = "info"
        End If
        If strTipo = "E" Then
            strTipo = "error"
        End If
        If strTipo = "S" Then
            strTipo = "success"
        End If
        If strTipo = "W" Then
            strTipo = "warning"
        End If
        ScriptManager.RegisterStartupScript(objPage, objPage.GetType(), "key", "Mensaje('" & strTitulo & "','" & strMensaje & "','" & strTipo & "');", True)
    End Sub


    Shared Sub MensajeSiNo(ByVal strMensaje As String, ByVal objPage As Object, ByVal master As Object, ByVal strSi As String, ByVal strNo As String)
        ScriptManager.RegisterStartupScript(objPage, objPage.GetType(), "key", "MensajeSiNo('" & strMensaje & "','" & strSi & "','" & strNo & "');", True)
    End Sub

    '***************************************************************************************
    'Propósito  : Formato para desplegar mensajes de error en pantalla.
    '***************************************************************************************



    Shared Sub MessageBoxError(ByVal strModulo As String, ByVal objErr As ErrObject, ByVal objPage As Object, ByVal master As Object, Optional ByVal objParametros As Object = Nothing)
        Dim strMensaje As String = "", strNumVentana As String = "", intPosIni As Integer = 1
        Dim intPosCrLf As Integer = 0, strParametros As String = "", strURL As String = "", strUsuario As String = ""
        Dim strNomUsuario As String = ""
        If objErr.Number > 0 Then
            InStrRev(objErr.GetException.Message, ".")
            intPosCrLf = If(InStr(objErr.GetException.Message, vbCrLf) = 0,
                                Len(Trim(objErr.GetException.Message)),
                                InStr(objErr.GetException.Message, vbCrLf))
            strMensaje = Replace(Mid(Trim(objErr.GetException.Message), intPosIni, (intPosCrLf - intPosIni)), "'", """")
            If Not objParametros Is Nothing Then

                strNumVentana = If(objParametros Is Nothing, "", objParametros.ventana)
                strUsuario = If(objParametros Is Nothing, "", objParametros.usuario)

                If Not String.IsNullOrEmpty(strUsuario) Then
                    Dim strCnx As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString()
                    Dim cmdTemporal As SqlCommand, rdoReader As SqlDataReader
                    Using cnxAcceso As New SqlConnection(DecryptTripleDES(strCnx))
                        cnxAcceso.Open()
                        cmdTemporal = New SqlCommand("SELECT nom_personal FROM REMMae_ficha_personal WHERE rut_personal = " & Trim(strUsuario), cnxAcceso)
                        rdoReader = cmdTemporal.ExecuteReader()
                        Try
                            If rdoReader.Read Then strNomUsuario = Trim(rdoReader(0).ToString)
                        Catch ex As Exception
                            strNomUsuario = ""
                        Finally
                            rdoReader.Close()
                            cmdTemporal.Dispose()
                            cnxAcceso.Close()
                            rdoReader = Nothing
                            cmdTemporal = Nothing
                        End Try
                    End Using
                End If
                strParametros = "x1=" & objErr.Number & "&x2=" & Trim(strModulo) & "&x3=" & Trim(strMensaje) & "&x4=" & Trim(strNumVentana) &
                        "&x5=" & Trim(strUsuario) & " - " & Trim(strNomUsuario)
                strURL = "javascript:window.open('/base/aspError.aspx?" & Trim(strParametros) & "', 'NewWindow', 'top=0,left=0,width=510," &
                        "height=490,status=no,resizable=no,scrollbars=no');"
                'objPage.ClientScript.RegisterStartupScript(objPage.GetType(), "key", "Mensaje('" & strModulo & "','" & strMensaje & "','error');", True)
                ScriptManager.RegisterStartupScript(objPage, objPage.GetType(), "key", "Mensaje('" & strModulo & "','" & strMensaje & "','error');", True)
            Else
                'objPage.ClientScript.RegisterStartupScript(objPage.GetType(), "key", "Mensaje('" & strModulo & "','" & strMensaje & "',);", True)
                ScriptManager.RegisterStartupScript(objPage, objPage.GetType(), "key", "Mensaje('" & strModulo & "','" & strMensaje & "','error');", True)
            End If
        End If
    End Sub



End Class

