﻿Imports System.Data.SqlClient
Imports System.IO
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspReporteCharlas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarCombo("SEMP", "", cboEmpresa)
                Call CargarCombo("SAR", "", cboArea)
                Call CargarCombo("SCAR", "", cboCargo)
                Call CargarCombo("SC", "", cboCategoria)
                Call CargarCombo("SSC", "", cboSubcategoria)
                Call CargarCombo("SCCHA", "", cboCharla)
                txtRut.Text = ""
                txtNombre.Text = ""
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        CargarGrilla()
        KPI()
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTablaR As String = "FOMov_charlas"
            Dim strSQLR As String = "Exec pro_FO_reporteCharlas 'SMCH'," &
                                                                   "@cod_empresa='" & cboEmpresa.SelectedValue & "'," &
                                                                   "@id_area='" & cboArea.SelectedValue & "'," &
                                                                   "@id_cargo='" & cboCargo.SelectedValue & "'," &
                                                                   "@id_charla='" & cboCharla.SelectedValue & "'," &
                                                                   "@id_categoria='" & cboCategoria.SelectedValue & "'," &
                                                                   "@id_subcategoria='" & cboSubcategoria.SelectedValue & "'," &
                                                                   "@rut_encuestado='" & txtRut.Text & "'," &
                                                                   "@estado='" & cboEstado.SelectedValue & "'"

            grdListado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdListado.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCombo(ByVal tipo As String, ByVal busqueda As String, ByVal combo As DropDownList)
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_reporteCharlas '" & tipo & "','" & busqueda & "'"
            combo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            combo.DataTextField = "nombre"
            combo.DataValueField = "id"
            combo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombo", Err, Page, Master)
        End Try
    End Sub

    Private Sub cboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCategoria.SelectedIndexChanged
        Call CargarCombo("SSC", cboCategoria.SelectedValue, cboSubcategoria)
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarCombo("SAR", cboEmpresa.SelectedValue, cboArea)
    End Sub


    Function KPI() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_reporteCharlas 'CKPI'," &
                                                                   "@cod_empresa='" & cboEmpresa.SelectedValue & "'," &
                                                                   "@id_area='" & cboArea.SelectedValue & "'," &
                                                                   "@id_cargo='" & cboCargo.SelectedValue & "'," &
                                                                   "@id_charla='" & cboCharla.SelectedValue & "'," &
                                                                   "@id_categoria='" & cboCategoria.SelectedValue & "'," &
                                                                   "@id_subcategoria='" & cboSubcategoria.SelectedValue & "'," &
                                                                   "@rut_encuestado='" & txtRut.Text & "'," &
                                                                   "@estado='" & cboEstado.SelectedValue & "'"

            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblImpactados.Text = rdoReader(0)
                    lblContestadas.Text = rdoReader(1)
                    lblPorResp.Text = rdoReader(2) & " %"
                    lblPromedio.Text = rdoReader(3)
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("KPI", Err, Page, Master)
            Return False
        End Try
    End Function

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call CargarCombo("SEMP", "", cboEmpresa)
        Call CargarCombo("SAR", "", cboArea)
        Call CargarCombo("SCAR", "", cboCargo)
        Call CargarCombo("SC", "", cboCategoria)
        Call CargarCombo("SSC", "", cboSubcategoria)
        Call CargarCombo("SCCHA", "", cboCharla)
        grdListado.DataBind()

        lblImpactados.Text = "0"
        lblContestadas.Text = "0"
        lblPorResp.Text = "0,0 %"
        lblPromedio.Text = "0,0"

        txtRut.Text = ""
        txtNombre.Text = ""
        txtBuscar.Text = ""
    End Sub

    Private Sub CargarGrillaPersona(ByVal buscar As String)
        Try
            Dim strNomTablaR As String = "RRHHMae_ficha_personal"
            Dim strSQLR As String = "Exec pro_FO_reporteCharlas 'BP',@busqueda='" & buscar & "'"
            grdPersona.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdPersona.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPersona", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Click(sender As Object, e As EventArgs) Handles btnBuscarPersona.Click
        CargarGrillaPersona(txtBuscar.Text)
    End Sub

    Private Sub grdPersona_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPersona.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)
        Dim str_rutPersona As String = CType(row.FindControl("lblRutPersona"), Label).Text
        Dim str_nomPersona As String = CType(row.FindControl("lblNombrePersona"), Label).Text
        Try
            If e.CommandName = "btnSeleccion" Then
                txtRut.Text = str_rutPersona
                txtNombre.Text = str_nomPersona
                updRut.Update()
                updNombre.Update()
                txtBuscar.Text = ""
                grdPersona.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdPersona_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        Me.grdListado.EnableViewState = False
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(Me.grdListado)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=rptCharlas.xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
    End Sub
End Class