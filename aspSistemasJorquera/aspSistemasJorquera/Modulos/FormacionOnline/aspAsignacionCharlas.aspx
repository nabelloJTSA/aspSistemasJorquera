﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAsignacionCharlas.aspx.vb" Inherits="aspSistemasJorquera.aspAsignacionCharlas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="Label1" runat="server" Text="ASIGNACIÓN DE CHARLAS"></asp:Label>
                    <small></small>
                </h3>
            </div>

            <div class="col-md-2 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnListado" CssClass="btn botonesTC-descargar btn-block" PostBackUrl="~/Modulos/FormacionOnline/aspListadoCharlas.aspx" runat="server">Listado Charlas</asp:LinkButton>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 form-group">
                <div>
                    <asp:Label ID="lblAsignacion" runat="server" Text="Asinación para: CHARLA COVID 19"></asp:Label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Área</label>
                <div>
                    <asp:DropDownList ID="cboArea" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Cargo</label>
                <div>
                    <asp:DropDownList ID="cboCargo" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 form-group" style="width: 20px">
                <label for="fname" style="height: 25px"
                    class="control-label col-form-label">
                    Rut</label>
                <div class="custom-control custom-checkbox">
                    <asp:CheckBox ID="chkEncuestado" runat="server" AutoPostBack="true" Width="20%" />
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" style="height: 15px"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updRut" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase text-center" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-4 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Nombre</label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updNombre" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" style="height: 15px"
                    class="control-label col-form-label">
                </label>
                <div id="dvEncuestado" runat="server">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalBuscarPersona" style="width: 50%">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
         
            <div class="col-md-1 form-group pull-right">
                <label for="fname" style="height: 15px"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn btn-primary btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>
            <div class="col-md-1 pull-right">
                <label for="fname" style="height: 15px"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnAsignar" CssClass="btn btn-primary btn-block" runat="server" OnClientClick="return confirm('Esta seguro que desea Asignar esta Charla');">Asignar</asp:LinkButton>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal" id="modalBuscarPersona" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalBuscarPersonaLabel" aria-hidden="true">
            <div class="modal-dialog  modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-md-3">
                                <h5 class="modal-title" id="modalClientesLabel">Buscar Persona</h5>
                            </div>
                            <div class="col-md-1 pull-right">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel runat="server" ID="updClientes">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarPersona" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="row justify-content-center">
                                    <div class="col-md-10 form-group justify-content-center">
                                        <asp:TextBox ID="txtBuscar" runat="server" placeHolder="Rut o Nombre..." CssClass="form-control text-uppercase border-danger shadow" MaxLength="1000" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2 form-group justify-content-end">
                                        <asp:LinkButton ID="btnBuscarPersona" runat="server" CssClass="btn btn-primary btn-block"><i class="fa fa-search"></i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-12 form-group justify-content-center">
                                        <asp:GridView ID="grdPersona" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnSeleccion" runat="server" OnClientClick="$('#modalBuscarPersona').modal('hide');" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="btnSeleccion"><i class="fa fa-arrow-left"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Rut">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRutPersona" runat="server" Text='<%# Eval("rut_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNombrePersona" runat="server" Text='<%# Eval("nom_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <%--fin modal--%>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">
                <asp:GridView ID="grdAsignacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                    <Columns>

                        <asp:TemplateField HeaderText="EMPRESA">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>'></asp:Label>
                                <asp:HiddenField ID="hdnidAsignacion" runat="server" Value='<%# Bind("id_asignacion")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ÁREA">
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nom_area")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CARGO">
                            <ItemTemplate>
                                <asp:Label ID="lblCargp" runat="server" Text='<%# Bind("nom_cargo")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="RUT ENCUESTADO">
                            <ItemTemplate>
                                <asp:Label ID="lblRutEncuestado" runat="server" Text='<%# Bind("rut_encuestado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="NOMBRE ENCUESTADO">
                            <ItemTemplate>
                                <asp:Label ID="lblNomEncuestado" runat="server" Text='<%# Bind("nom_encuestado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TIPO ASIGNACIÓN">
                            <ItemTemplate>
                                <asp:Label ID="lblTipAsignacion" runat="server" Text='<%# Bind("tip_asignacion")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="USUARIO ASIGNA">
                            <ItemTemplate>
                                <asp:Label ID="lblUsrAsigna" runat="server" Text='<%# Bind("usr_add")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FECHA ASIGNACIÓN">
                            <ItemTemplate>
                                <asp:Label ID="lblFecAsignacion" runat="server" Text='<%# Bind("fec_add")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                        <%--       <asp:TemplateField HeaderText="DESASIGNAR">
                            <ItemTemplate>
                                <h4>
                                    <asp:LinkButton ID="btnDesasignar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Desasignar" OnClientClick="return confirm('Esta seguro que desea Desasignar este Registro');"><i class="fa fa-minus-circle"></i></asp:LinkButton>
                                </h4>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                        </asp:TemplateField>--%>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                </asp:GridView>
            </div>
        </div>
    </section>
    <asp:HiddenField ID="hdnIDCharla" runat="server" />
</asp:Content>

