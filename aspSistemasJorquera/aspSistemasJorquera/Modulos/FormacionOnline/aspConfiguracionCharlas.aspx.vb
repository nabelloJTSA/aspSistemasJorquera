﻿Imports System.Data.SqlClient
Imports System.IO
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspSubCategoria
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                CargarGrillaCategoria("")
                MultiView1.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try

    End Sub


    Private Sub CargarModalidad()
        Try
            Dim strNomTablaR As String = "FOMae_modalidad"
            Dim strSQLR As String = "Exec pro_FO_subCategorias 'SM'"
            cboModalidad.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboModalidad.DataTextField = "nom_modalidad"
            cboModalidad.DataValueField = "id_modalidad"
            cboModalidad.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarModalidad", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarCategoria()
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_subCategorias 'SCA'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "nom_categoria"
            cboCategoria.DataValueField = "id_categoria"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategoria", Err, Page, Master)
        End Try
    End Sub

    Function validar() As Boolean
        If txtNombreSubCategoria.Text <> "" Then
            If cboCategoria.SelectedValue <> 0 Then
                If cboModalidad.SelectedValue <> 0 Then
                    Return True
                Else
                    MessageBox("Confirmar", "Seleccione Modalidad", Page, Master, "W")
                    Return False
                End If
            Else
                MessageBox("Confirmar", "Seleccione Categoría", Page, Master, "W")
                Return False
            End If
        Else
            MessageBox("Confirmar", "Ingrese Nombre de Subcategoría", Page, Master, "W")
            Return False
        End If
    End Function

    Private Sub btnGuardarSubcategoria_Click(sender As Object, e As EventArgs) Handles btnGuardarSubcategoria.Click
        If validar() = True Then
            If hdnId.Value = "" Then 'guardar
                If ExisteNombreSubCategoria(txtNombreSubCategoria.Text) = False Then
                    If InsertarSubcategoria() = True Then
                        MessageBox("Confirmar", "Registro Guardado Exitosamente", Page, Master, "S")
                        limpiarSubcategoria()
                    End If
                End If
            Else 'actualizar
                If ExisteNombreSubCategoriaModifcar(hdnId.Value, txtNombreSubCategoria.Text) = False Then
                    If ActualizarSubcategoria() = True Then
                        MessageBox("Confirmar", "Registro Actualizado Exitosamente", Page, Master, "S")
                        limpiarSubcategoria()
                    End If
                End If
            End If
        End If
    End Sub

    Function InsertarSubcategoria() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_subCategorias"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IE"

            comando.Parameters.Add("@nom_subCategoria", SqlDbType.VarChar)
            comando.Parameters("@nom_subCategoria").Value = Trim(txtNombreSubCategoria.Text)

            comando.Parameters.Add("@id_modalidad", SqlDbType.Int)
            comando.Parameters("@id_modalidad").Value = cboModalidad.SelectedValue

            comando.Parameters.Add("@id_Categoria", SqlDbType.Int)
            comando.Parameters("@id_Categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@est_activa", SqlDbType.Bit)
            comando.Parameters("@est_activa").Value = chkActivo.Checked

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarSubcategoria", Err, Page, Master)
            Return False
        End Try
    End Function

    Function ExisteNombreSubCategoria(ByVal nombre As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_subCategorias 'VE',@nom_subCategoria='" & nombre & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    MessageBox("Confirmar", "El nombre de Subcategoría ya existe", Page, Master, "W")
                    Return True
                Else
                    Return False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarNombre", Err, Page, Master)
            Return False
        End Try
    End Function

    Function ExisteNombreSubCategoriaModifcar(ByVal id As String, ByVal nombre As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_subCategorias 'VEM',@id_subCategoria='" & id & "',@nom_subCategoria='" & nombre & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    MessageBox("Confirmar", "El nombre de Subcategoría ya existe", Page, Master, "W")
                    Return True
                Else
                    Return False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarNombre", Err, Page, Master)
            Return False
        End Try
    End Function

    Private Sub CargarGrillaSubcategoria(ByVal nombre As String)
        Try
            Dim strNomTablaR As String = "FOMae_modalidad"
            Dim strSQLR As String = "Exec pro_FO_subCategorias 'SD',@nom_subCategoria='" & nombre & "'"
            grdSubcategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdSubcategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaSubcategoria", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdSubcategoria_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdSubcategoria.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)

        Dim str_idSubcategoria As String = CType(row.FindControl("hdnIdSubcategoria"), HiddenField).Value

        Try
            If e.CommandName = "Eliminar" Then
                If EliminarSubcategoria(str_idSubcategoria) = True Then
                    MessageBox("Confirmar", "Registro Eliminado Exitosamente", Page, Master, "S")
                    limpiarSubcategoria()
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdSubcategoria_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdSubcategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSubcategoria.SelectedIndexChanged
        Dim row As GridViewRow = grdSubcategoria.SelectedRow
        Dim str_idSubcategoria As String = CType(row.FindControl("hdnIdSubcategoria"), HiddenField).Value
        Dim str_nombreSubcategoria As String = CType(row.FindControl("lblNombre"), Label).Text
        Dim str_idModalidad As String = CType(row.FindControl("hdnIdModalidad"), HiddenField).Value
        Dim str_idCategoria As String = CType(row.FindControl("hdnIdCategoria"), HiddenField).Value
        Dim str_estado As String = CType(row.FindControl("hdnEstado"), HiddenField).Value

        txtNombreSubCategoria.Text = str_nombreSubcategoria
        cboModalidad.SelectedValue = str_idModalidad
        cboCategoria.SelectedValue = str_idCategoria

        chkActivo.Checked = str_estado
        hdnId.Value = str_idSubcategoria
    End Sub

    Public Sub limpiarSubcategoria()
        txtNombreSubCategoria.Text = ""
        CargarModalidad()
        CargarCategoria()
        hdnId.Value = ""
        CargarGrillaSubcategoria("")
        chkActivo.Checked = True
        txtBuscar.Text = ""
    End Sub

    Private Sub btnCancelarSubcategoria_Click(sender As Object, e As EventArgs) Handles btnCancelarSubcategoria.Click
        limpiarSubcategoria()
    End Sub
    Function ActualizarSubcategoria() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_subCategorias"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"

            comando.Parameters.Add("@id_subCategoria", SqlDbType.Int)
            comando.Parameters("@id_subCategoria").Value = hdnId.Value

            comando.Parameters.Add("@nom_subCategoria", SqlDbType.VarChar)
            comando.Parameters("@nom_subCategoria").Value = Trim(txtNombreSubCategoria.Text)

            comando.Parameters.Add("@id_modalidad", SqlDbType.Int)
            comando.Parameters("@id_modalidad").Value = cboModalidad.SelectedValue

            comando.Parameters.Add("@id_Categoria", SqlDbType.Int)
            comando.Parameters("@id_Categoria").Value = cboCategoria.SelectedValue


            comando.Parameters.Add("@est_activa", SqlDbType.Bit)
            comando.Parameters("@est_activa").Value = chkActivo.Checked

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("ActualizarSubcategoria", Err, Page, Master)
            Return False
        End Try
    End Function


    Function EliminarSubcategoria(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_subCategorias"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"

            comando.Parameters.Add("@id_subCategoria", SqlDbType.Int)
            comando.Parameters("@id_subCategoria").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarSubcategoria", Err, Page, Master)
            Return False
        End Try
    End Function



    Private Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        CargarGrillaSubcategoria(txtBuscar.Text)
    End Sub

    Private Sub btnCategoria_Click(sender As Object, e As EventArgs) Handles btnCategoria.Click
        MultiView1.ActiveViewIndex = 0
        btnCategoria.CssClass = btnCategoria.CssClass.Replace("bg-success", "bg-green")
        btnSubcategoria.CssClass = btnSubcategoria.CssClass.Replace("bg-green", "bg-success")
        CargarGrillaCategoria("")
    End Sub

    Private Sub btnSubcategoria_Click(sender As Object, e As EventArgs) Handles btnSubcategoria.Click
        MultiView1.ActiveViewIndex = 1
        btnSubcategoria.CssClass = btnSubcategoria.CssClass.Replace("bg-success", "bg-green")
        btnCategoria.CssClass = btnCategoria.CssClass.Replace("bg-green", "bg-success")
        CargarModalidad()
        CargarGrillaSubcategoria("")
        CargarCategoria()
    End Sub



    Private Sub CargarGrillaCategoria(ByVal nombre As String)
        Try
            Dim strNomTablaR As String = "pro_FO_categoria"
            Dim strSQLR As String = "Exec pro_FO_categoria 'SC',@nom_categoria='" & nombre & "'"
            grdCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaCategoria", Err, Page, Master)
        End Try
    End Sub


    Function InsertarCategoria() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_categoria"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IC"

            comando.Parameters.Add("@nom_categoria", SqlDbType.VarChar)
            comando.Parameters("@nom_categoria").Value = Trim(txtNomCategoria.Text)

            comando.Parameters.Add("@est_activa", SqlDbType.Bit)
            comando.Parameters("@est_activa").Value = chkActivoCat.Checked

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarCategoria", Err, Page, Master)
            Return False
        End Try
    End Function


    Function ModificarCategoria() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_categoria"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "MC"

            comando.Parameters.Add("@id_categoria", SqlDbType.Int)
            comando.Parameters("@id_categoria").Value = hdnIdCategoria.Value

            comando.Parameters.Add("@nom_categoria", SqlDbType.VarChar)
            comando.Parameters("@nom_categoria").Value = Trim(txtNomCategoria.Text)

            comando.Parameters.Add("@est_activa", SqlDbType.Bit)
            comando.Parameters("@est_activa").Value = chkActivoCat.Checked

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("modificarCategoria", Err, Page, Master)
            Return False
        End Try
    End Function

    Function EliminarCategoria(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_categoria"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EC"

            comando.Parameters.Add("@id_categoria", SqlDbType.Int)
            comando.Parameters("@id_categoria").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarCategoria", Err, Page, Master)
            Return False
        End Try
    End Function


    Function ExisteNombreCategoria(ByVal nombre As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_categoria 'VC',@nom_categoria='" & nombre & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    MessageBox("Confirmar", "El nombre de Categoría ya existe", Page, Master, "W")
                    Return True
                Else
                    Return False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ExisteNombreCategoria", Err, Page, Master)
            Return False
        End Try
    End Function



    Public Sub limpiarCategoria()
        txtNomCategoria.Text = ""
        chkActivoCat.Checked = True
        CargarGrillaCategoria("")
        txtBuscarCat.Text = ""
        hdnIdCategoria.Value = ""
    End Sub

    Private Sub btnGuardarCat_Click(sender As Object, e As EventArgs) Handles btnGuardarCat.Click
        If hdnIdCategoria.Value = "" Then 'guardar
            If txtNomCategoria.Text <> "" Then
                If ExisteNombreCategoria(Trim(txtNomCategoria.Text)) = False Then
                    If InsertarCategoria() = True Then
                        MessageBox("Confirmar", "Registro Guardado Exitosamente", Page, Master, "S")
                        limpiarCategoria()
                    End If
                End If
            Else
                MessageBox("Confirmar", "Favor Ingresar nombre de categoria", Page, Master, "W")
            End If
        Else 'modificar
            If txtNomCategoria.Text <> "" Then
                If ExisteNombreCategoriaModificar(hdnIdCategoria.Value, txtNomCategoria.Text) = False Then
                    If ModificarCategoria() = True Then
                        MessageBox("Confirmar", "Registro Actualizado Exitosamente", Page, Master, "S")
                        limpiarCategoria()
                    End If
                End If
            Else
                MessageBox("Confirmar", "Favor Ingresar nombre de categoria", Page, Master, "W")
            End If
        End If
    End Sub

    Function ExisteNombreCategoriaModificar(ByVal id As String, ByVal nombre As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_categoria 'VCM',@id_categoria='" & id & "',@nom_categoria='" & nombre & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    MessageBox("Confirmar", "El nombre de Categoría ya existe", Page, Master, "W")
                    Return True
                Else
                    Return False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ExisteNombreCategoria", Err, Page, Master)
            Return False
        End Try
    End Function

    Private Sub grdCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCategoria.SelectedIndexChanged
        Dim row As GridViewRow = grdCategoria.SelectedRow

        Dim str_idCategoria As String = CType(row.FindControl("hdnIdCategoria"), HiddenField).Value
        Dim str_nomCategoria As String = CType(row.FindControl("lblNombreCategoria"), Label).Text
        Dim str_estado As String = CType(row.FindControl("hdnEstado"), HiddenField).Value

        txtNomCategoria.Text = str_nomCategoria
        chkActivo.Checked = str_estado
        hdnIdCategoria.Value = str_idCategoria
    End Sub

    Private Sub grdCategoria_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCategoria.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)

        Dim str_idCategoria As String = CType(row.FindControl("hdnIdCategoria"), HiddenField).Value

        Try
            If e.CommandName = "Eliminar" Then
                If EliminarCategoria(str_idCategoria) = True Then
                    MessageBox("Confirmar", "Registro Eliminado Exitosamente", Page, Master, "S")
                    limpiarCategoria()
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdCategoria_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub txtBuscarCat_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarCat.TextChanged
        CargarGrillaCategoria(txtBuscarCat.Text)
    End Sub

    Private Sub btnCancelarCat_Click(sender As Object, e As EventArgs) Handles btnCancelarCat.Click
        limpiarCategoria()
    End Sub

    Private Sub btnExportarCat_Click(sender As Object, e As EventArgs) Handles btnExportarCat.Click
        Try
            Dim grdExportarCat As New DataGrid

            grdExportarCat.CellPadding = 0
            grdExportarCat.CellSpacing = 0
            grdExportarCat.BorderStyle = BorderStyle.None

            grdExportarCat.Font.Size = 10

            Dim strNomTabla As String = "FOMae_categoria"
            Dim strSQL As String = "Exec pro_FO_categoria 'SC',@nom_categoria='" & Trim(txtBuscarCat.Text) & "'"
            grdExportarCat.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdExportarCat.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdExportarCat.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=FOCategorías.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()

        Catch ex As Exception
            MessageBox("Exportar", "btnExportarCat_Click", Page, Master, "E")
        End Try
    End Sub

    Private Sub btnExportarSub_Click(sender As Object, e As EventArgs) Handles btnExportarSub.Click
        Try
            Dim grdExportarCat As New DataGrid

            grdExportarCat.CellPadding = 0
            grdExportarCat.CellSpacing = 0
            grdExportarCat.BorderStyle = BorderStyle.None

            grdExportarCat.Font.Size = 10

            Dim strNomTabla As String = "FOMae_subcategorias"
            Dim strSQL As String = "Exec pro_FO_subCategorias 'SD',@nom_subCategoria='" & Trim(txtBuscar.Text) & "'"
            grdExportarCat.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdExportarCat.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdExportarCat.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=FOSubcategorías.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()

        Catch ex As Exception
            MessageBox("Exportar", "btnExportarSub_Click", Page, Master, "E")
        End Try
    End Sub
End Class