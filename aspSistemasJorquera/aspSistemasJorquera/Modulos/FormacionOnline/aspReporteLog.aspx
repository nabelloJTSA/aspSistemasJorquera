﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspReporteLog.aspx.vb" Inherits="aspSistemasJorquera.aspReporteLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="LOG DE EVENTOS"></asp:Label>
                    <small></small>
                </h3>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Área</label>
                <div>
                    <asp:DropDownList ID="cboArea" CssClass="form-control" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Cargo</label>
                <div>
                    <asp:DropDownList ID="cboCargo" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Evento</label>
                <div>
                    <asp:DropDownList ID="cboEvento" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 form-group">
                <label for="fname" style="height: 15px"
                    class="control-label col-form-label">
                    Rut
                </label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updRut" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase text-center" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-4 form-group">
                <label for="fname" style="height: 15px"
                    class="control-label col-form-label">
                    Nombre</label>
                <div>
                    <asp:UpdatePanel runat="server" ID="updNombre" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div id="dvEncuestado" runat="server">
                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalBuscarPersona" style="width: 50%">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>

            <div class="col-md-1 pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn botonesTC-descargar btn-block" runat="server" ToolTip="Exportar">Reporte</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn btn-primary btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>
            <div class="col-md-1 pull-right">
                <label for="fname" 
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnConsultar" CssClass="btn btn-primary btn-block" runat="server">Consultar</asp:LinkButton>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal" id="modalBuscarPersona" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalBuscarPersonaLabel" aria-hidden="true">
            <div class="modal-dialog  modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-md-3">
                                <h5 class="modal-title" id="modalClientesLabel">Buscar Persona</h5>
                            </div>
                            <div class="col-md-1 pull-right">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel runat="server" ID="updClientes">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnBuscarPersona" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="row justify-content-center">
                                    <div class="col-md-10 form-group justify-content-center">
                                        <asp:TextBox ID="txtBuscar" runat="server" placeHolder="Rut o Nombre..." CssClass="form-control text-uppercase border-danger shadow" MaxLength="1000" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2 form-group justify-content-end">
                                        <asp:LinkButton ID="btnBuscarPersona" runat="server" CssClass="btn btn-primary btn-block"><i class="fa fa-search"></i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-12 form-group justify-content-center">
                                        <asp:GridView ID="grdPersona" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnSeleccion" runat="server" OnClientClick="$('#modalBuscarPersona').modal('hide');" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="btnSeleccion"><i class="fa fa-arrow-left"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Rut">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRutPersona" runat="server" Text='<%# Eval("rut_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Nombre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNombrePersona" runat="server" Text='<%# Eval("nom_personal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <%--fin modal--%>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">
                <asp:GridView ID="grdEventos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="EVENTO">
                            <ItemTemplate>
                                <asp:Label ID="lblEvento" runat="server" Text='<%# Bind("nom_evento")%>'></asp:Label>                               
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FECHA">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Hora">
                            <ItemTemplate>
                                <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="RUT ENCUESTADO">
                            <ItemTemplate>
                                <asp:Label ID="lblRutEncuestado" runat="server" Text='<%# Bind("rut_encuestado")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="NOMBRE">
                            <ItemTemplate>
                                <asp:Label ID="lblNomPersona" runat="server" Text='<%# Bind("nom_persona")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>                       

                        <asp:TemplateField HeaderText="EMPRESA">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SUCURSAL">
                            <ItemTemplate>
                                <asp:Label ID="lblSucursal" runat="server" Text='<%# Bind("nom_sucursal")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="ÁREA">
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nom_area")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="CARGO">
                            <ItemTemplate>
                                <asp:Label ID="lblCargo" runat="server" Text='<%# Bind("nom_cargo")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>
               
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                </asp:GridView>
            </div>
        </div>
    </section>
</asp:Content>
