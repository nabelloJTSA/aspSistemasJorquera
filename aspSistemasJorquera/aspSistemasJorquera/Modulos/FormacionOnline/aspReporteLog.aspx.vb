﻿Imports System.Data.SqlClient
Imports System.IO
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspReporteLog
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarCombo("SEMP", "", cboEmpresa)
                Call CargarCombo("SAR", "", cboArea)
                Call CargarCombo("SCAR", "", cboCargo)
                Call CargarCombo("SEVEN", "", cboEvento)
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Click(sender As Object, e As EventArgs) Handles btnBuscarPersona.Click
        CargarGrillaPersona(txtBuscar.Text)
    End Sub

    Private Sub CargarGrillaPersona(ByVal buscar As String)
        Try
            Dim strNomTablaR As String = "RRHHMae_ficha_personal"
            Dim strSQLR As String = "Exec pro_FO_asigCharlas 'BP',@busqueda='" & buscar & "'"
            grdPersona.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdPersona.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPersona", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCombo(ByVal tipo As String, ByVal busqueda As String, ByVal combo As DropDownList)
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_reporteLog '" & tipo & "','" & busqueda & "'"
            combo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            combo.DataTextField = "nombre"
            combo.DataValueField = "id"
            combo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombo", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdPersona_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPersona.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)
        Dim str_rutPersona As String = CType(row.FindControl("lblRutPersona"), Label).Text
        Dim str_nomPersona As String = CType(row.FindControl("lblNombrePersona"), Label).Text
        Try
            If e.CommandName = "btnSeleccion" Then
                txtRut.Text = str_rutPersona
                txtNombre.Text = str_nomPersona
                updRut.Update()
                updNombre.Update()
                txtBuscar.Text = ""
                grdPersona.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdPersona_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaEvento()
        Try
            Dim strNomTablaR As String = "FOMov_Log"
            Dim strSQLR As String = "Exec pro_FO_reporteLog 'SLOG',@cod_empresa='" & cboEmpresa.SelectedValue & "'
                                                                    ,@id_area='" & cboArea.SelectedValue & "'
                                                                    ,@id_cargo='" & cboCargo.SelectedValue & "'
                                                                    ,@id_evento='" & cboEvento.SelectedValue & "'
                                                                    ,@rut_encuestado='" & txtRut.Text & "'"
            grdEventos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdEventos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEvento", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        CargarGrillaEvento()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call CargarCombo("SEMP", "", cboEmpresa)
        Call CargarCombo("SAR", "", cboArea)
        Call CargarCombo("SCAR", "", cboCargo)
        Call CargarCombo("SEVEN", "", cboEvento)
        txtRut.Text = ""
        txtNombre.Text = ""
        grdEventos.DataBind()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdExportar As New DataGrid

            grdExportar.CellPadding = 0
            grdExportar.CellSpacing = 0
            grdExportar.BorderStyle = BorderStyle.None

            grdExportar.Font.Size = 10

            Dim strNomTabla As String = "FOMov_Log"
            Dim strSQL As String = "Exec pro_FO_reporteLog 'SLOG',@cod_empresa='" & cboEmpresa.SelectedValue & "'
                                                                    ,@id_area='" & cboArea.SelectedValue & "'
                                                                    ,@id_cargo='" & cboCargo.SelectedValue & "'
                                                                    ,@id_evento='" & cboEvento.SelectedValue & "'
                                                                    ,@rut_encuestado='" & txtRut.Text & "'"

            grdExportar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdExportar.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdExportar.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=LogEv.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()

        Catch ex As Exception
            MessageBox("Exportar", "btnExportar_Click", Page, Master, "E")
        End Try
    End Sub
End Class