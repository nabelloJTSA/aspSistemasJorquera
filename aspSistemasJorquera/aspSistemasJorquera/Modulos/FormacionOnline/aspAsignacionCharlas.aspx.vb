﻿Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspAsignacionCharlas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                hdnIDCharla.Value = Session("idCharla")
                Session("idCharla") = ""
                bloqueoEncuestado()
                MostrarDatos(hdnIDCharla.Value)
                Call CargarCombo("SEMP", "", cboEmpresa)
                Call CargarCombo("SAR", "", cboArea)
                Call CargarCombo("SCAR", "", cboCargo)
                cboArea.Enabled = False
                cboCargo.Enabled = False
                CargarGrillaAsignacion()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Public Sub bloqueoEncuestado()
        txtRut.Enabled = False
        txtNombre.Enabled = False
        dvEncuestado.Visible = False

        cboEmpresa.Enabled = True
        cboCargo.Enabled = True
        cboArea.Enabled = True
        chkEncuestado.Checked = False
    End Sub

    Public Sub bloqueoEmpCrgAr()
        txtRut.Enabled = False
        txtNombre.Enabled = False
        dvEncuestado.Visible = True

        cboEmpresa.Enabled = False
        cboCargo.Enabled = False
        cboArea.Enabled = False
    End Sub

    Private Sub chkEncuestado_CheckedChanged(sender As Object, e As EventArgs) Handles chkEncuestado.CheckedChanged
        If chkEncuestado.Checked = True Then
            bloqueoEmpCrgAr()
            Call CargarCombo("SEMP", "", cboEmpresa)
            Call CargarCombo("SAR", "", cboArea)
            Call CargarCombo("SCAR", "", cboCargo)
        Else
            bloqueoEncuestado()
            txtRut.Text = ""
            txtNombre.Text = ""
        End If
    End Sub

    Private Sub CargarCombo(ByVal tipo As String, ByVal busqueda As String, ByVal combo As DropDownList)
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_asigCharlas '" & tipo & "','" & busqueda & "'"
            combo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            combo.DataTextField = "nombre"
            combo.DataValueField = "id"
            combo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombo", Err, Page, Master)
        End Try
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarCombo("SAR", cboEmpresa.SelectedValue, cboArea)

        If cboEmpresa.SelectedValue <> 0 Then
            cboArea.Enabled = True
        Else
            cboArea.Enabled = True
        End If
    End Sub


    Public Sub MostrarDatos(ByVal id As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_asigCharlas 'SDCH',@id_charla='" & id & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblAsignacion.Text = "Charla: " & rdoReader(0)
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("MostrarDatos", Err, Page, Master)

        End Try
    End Sub

    Private Sub CargarGrillaPersona(ByVal buscar As String)
        Try
            Dim strNomTablaR As String = "RRHHMae_ficha_personal"
            Dim strSQLR As String = "Exec pro_FO_asigCharlas 'BP',@busqueda='" & buscar & "'"
            grdPersona.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdPersona.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPersona", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnBuscarPersona_Click(sender As Object, e As EventArgs) Handles btnBuscarPersona.Click
        CargarGrillaPersona(txtBuscar.Text)
    End Sub

    Private Sub grdPersona_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPersona.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)
        Dim str_rutPersona As String = CType(row.FindControl("lblRutPersona"), Label).Text
        Dim str_nomPersona As String = CType(row.FindControl("lblNombrePersona"), Label).Text
        Try
            If e.CommandName = "btnSeleccion" Then
                txtRut.Text = str_rutPersona
                txtNombre.Text = str_nomPersona
                updRut.Update()
                updNombre.Update()
                txtBuscar.Text = ""
                grdPersona.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdPersona_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnAsignar_Click(sender As Object, e As EventArgs) Handles btnAsignar.Click
        If validar() = True Then
            Dim Existe As String = ExisteAsignacion(hdnIDCharla.Value)
            If Existe = "S/A" Then
                If InsertarASignacion() = True Then
                    MessageBox("Confirmar", "Registro Guardado Exitosamente", Page, Master, "S")
                    limpiar()
                End If
            Else
                MessageBox("Confirmar", Existe, Page, Master, "W")
            End If
        End If
    End Sub

    Function validar() As Boolean
        If chkEncuestado.Checked = False Then
            If cboEmpresa.SelectedValue <> 0 Then
                Return True
            Else
                MessageBox("Confirmar", "Favor Seleccione Empresa", Page, Master, "W")
                Return False
            End If
        Else
            If txtRut.Text <> "" Then
                Return True
            Else
                MessageBox("Confirmar", "Favor Seleccione Persona", Page, Master, "W")
                Return False
            End If
        End If
    End Function

    Private Sub cboArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboArea.SelectedIndexChanged
        If cboArea.SelectedValue <> 0 Then
            cboCargo.Enabled = True
        Else
            cboCargo.Enabled = True
        End If
    End Sub

    Function ExisteAsignacion(ByVal id As String) As String
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_asigCharlas 'VAC',@cod_empresa='" & cboEmpresa.SelectedValue & "',@id_area='" & cboArea.SelectedValue & "',@id_cargo='" & cboCargo.SelectedValue & "',@rut_encuestado='" & txtRut.Text & "',@id_charla='" & hdnIDCharla.Value & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Return rdoReader(0).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ExisteAsignacion", Err, Page, Master)
        End Try
    End Function

    Function InsertarASignacion() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_asigCharlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IA"

            comando.Parameters.Add("@id_charla", SqlDbType.VarChar)
            comando.Parameters("@id_charla").Value = hdnIDCharla.Value

            comando.Parameters.Add("@cod_empresa", SqlDbType.VarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue

            comando.Parameters.Add("@id_area", SqlDbType.VarChar)
            comando.Parameters("@id_area").Value = cboArea.SelectedValue

            comando.Parameters.Add("@id_cargo", SqlDbType.VarChar)
            comando.Parameters("@id_cargo").Value = cboCargo.SelectedValue

            comando.Parameters.Add("@rut_encuestado", SqlDbType.VarChar)
            comando.Parameters("@rut_encuestado").Value = txtRut.Text

            comando.Parameters.Add("@nom_encuestado", SqlDbType.VarChar)
            comando.Parameters("@nom_encuestado").Value = txtNombre.Text

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarASignacion", Err, Page, Master)
            Return False
        End Try
    End Function


    Private Sub CargarGrillaAsignacion()
        Try
            Dim strNomTablaR As String = "FOMae_charlar"
            Dim strSQLR As String = "Exec pro_FO_asigCharlas 'SDA',@id_charla='" & hdnIDCharla.Value & "' "
            grdAsignacion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdAsignacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAsignacion", Err, Page, Master)
        End Try
    End Sub

    Public Sub limpiar()
        bloqueoEncuestado()
        Call CargarCombo("SEMP", "", cboEmpresa)
        Call CargarCombo("SAR", "", cboArea)
        Call CargarCombo("SCAR", "", cboCargo)
        txtBuscar.Text = ""
        grdPersona.DataBind()
        txtRut.Text = ""
        txtNombre.Text = ""
        CargarGrillaAsignacion()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        limpiar()
    End Sub

    Private Sub grdAsignacion_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAsignacion.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)

        Dim str_idAsignacion As String = CType(row.FindControl("hdnidAsignacion"), HiddenField).Value

        Try
            If e.CommandName = "Desasignar" Then
                EliminarAsignacion(str_idAsignacion)
                Call limpiar()
            End If
        Catch ex As Exception
            MessageBoxError("grdAsignacion_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Function EliminarAsignacion(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_asigCharlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EAS"

            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarAsignacion", Err, Page, Master)
            Return False
        End Try
    End Function


End Class