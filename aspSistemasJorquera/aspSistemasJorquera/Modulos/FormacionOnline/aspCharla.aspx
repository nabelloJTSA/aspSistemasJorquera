﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCharla.aspx.vb" Inherits="aspSistemasJorquera.aspCharla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">           
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="Label2" runat="server" Text="CHARLAS"></asp:Label>
                    <small></small>
                </h3>
            </div>

            <div class="col-md-2 form-group  pull-right"  id="dvAsignar" runat="server" visible="false">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnASignar" CssClass="btn botonesTC-descargar btn-block" Visible="false" runat="server">Asignar</asp:LinkButton>
                </div>
            </div>
            <div class="col-md-2 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:LinkButton ID="btnListado" CssClass="btn botonesTC-descargar btn-block" PostBackUrl="~/Modulos/FormacionOnline/aspListadoCharlas.aspx" runat="server">Listado Charlas</asp:LinkButton>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Nombre Charla</label>
                        <div>
                            <asp:TextBox ID="txtNombreCharla" runat="server" placeholder="Nombre Charla..." MaxLength="1000" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Categoría</label>
                        <div>
                            <asp:DropDownList ID="cboCategoria" CssClass="form-control" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Subcategoría</label>
                        <div>
                            <asp:DropDownList ID="cboSubCategoria" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Descripción</label>
                        <div>
                            <asp:TextBox ID="txtDescripcion" runat="server" placeholder="Descripción..." MaxLength="300" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Video</label>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:FileUpload ID="flpVideo" runat="server" CssClass="form-control text-uppercase"></asp:FileUpload>
                            <asp:Label ID="lblNombreVideo" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <label for="fname"
                            class="control-label col-form-label">
                            Desde</label>
                        <div>
                            <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="col-md-2 form-group">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <label for="fname"
                            class="control-label col-form-label">
                            Hasta</label>
                        <div>
                            <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-md-1 pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn botonesTC-descargar btn-block" runat="server">Reporte</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn btn-primary btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="">
                </label>
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn btn-primary btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="Label1" runat="server" Text="Preguntas"></asp:Label>
                </h2>
            </div>
        </div>

        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>

                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Pregunta</label>
                        <div>
                            <asp:TextBox ID="txtPregunta" runat="server" placeholder="Ingrese Pregunta..." MaxLength="1000" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Respuesta Correcta</label>
                        <div>
                            <asp:DropDownList ID="cboRespuesta" CssClass="form-control" runat="server">
                                <asp:ListItem Value="SI">SI</asp:ListItem>
                                <asp:ListItem Value="NO">NO</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnAgregarPregunta" CssClass="btn btn-primary btn-block" runat="server">Agregar</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:GridView ID="grdPreguntas" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="PREGUNTA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPregunta" runat="server" Text='<%# Bind("Pregunta")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="RESPUESTA CORRECTA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRspCorrecta" runat="server" Text='<%# Bind("RespuestaCorrecta")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ELIMINAR">
                                    <ItemTemplate>
                                        <h4>
                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Eliminar" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </h4>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                </asp:TemplateField>
                            </Columns>
                             <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </div>
                <asp:HiddenField ID="hdnidCharla" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </section>
</asp:Content>
