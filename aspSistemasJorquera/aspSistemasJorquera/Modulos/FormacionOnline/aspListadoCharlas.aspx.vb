﻿Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspListadoCharlas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarCombo("SC", "", cboCategoria)
                Call CargarCombo("SSC", "", cboSubCategoria)
                'Call CargarCombo("SEMP", "", cboEmpresa)
                'Call CargarCombo("SAR", "", cboArea)
                'Call CargarCombo("SCAR", "", cboCargo)
                Call CargarGrillaListado("0", "0", "", "", "", "")
                Call contadorCharla("0", "0", "", "", "", "")
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCombo(ByVal tipo As String, ByVal busqueda As String, ByVal combo As DropDownList)
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_listadoCharlas '" & tipo & "','" & busqueda & "'"
            combo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            combo.DataTextField = "nombre"
            combo.DataValueField = "id"
            combo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombo", Err, Page, Master)
        End Try
    End Sub

    Private Sub cboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCategoria.SelectedIndexChanged
        Call CargarCombo("SSC", cboCategoria.SelectedValue, cboSubCategoria)
    End Sub

    'Private Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
    '    Call CargarCombo("SAR", cboEmpresa.SelectedValue, cboArea)
    'End Sub
    Private Sub CargarGrillaListado(ByVal categoria As String, ByVal subcategoria As String, ByVal desde As String, ByVal hasta As String, ByVal nom_charla As String, ByVal Asignacion As String)
        Try
            Dim strNomTablaR As String = "FOMae_charlar"
            Dim strSQLR As String = "Exec pro_FO_listadoCharlas 'SCHA',@id_categoria='" & categoria & "',@id_subcategoria='" & subcategoria & "',@nom_charla='" & nom_charla & "',@fec_desde='" & desde & "',@fec_hasta='" & hasta & "',@nom_asignacion='" & Asignacion & "'"
            grdListado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdListado.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaListado", Err, Page, Master)
        End Try
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        limpiar()
    End Sub
    Public Sub limpiar()
        Call CargarCombo("SC", "", cboCategoria)
        Call CargarCombo("SSC", "", cboSubCategoria)
        'Call CargarCombo("SEMP", "", cboEmpresa)
        'Call CargarCombo("SAR", "", cboArea)
        'Call CargarCombo("SCAR", "", cboCargo)
        Call CargarGrillaListado("0", "0", "", "", "", "")
        Call contadorCharla("0", "0", "", "", "", "")
        cboEstado.SelectedIndex = 0
        txtNombreCharla.Text = ""
        txtDesde.Text = ""
        txtHasta.Text = ""
    End Sub


    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Call CargarGrillaListado(cboCategoria.SelectedValue, cboSubCategoria.SelectedValue, txtDesde.Text, txtHasta.Text, txtNombreCharla.Text, cboEstado.SelectedValue)
        If cboCategoria.SelectedValue <> 0 Then
            Call contadorCharla(cboCategoria.SelectedValue, cboSubCategoria.SelectedValue, txtDesde.Text, txtHasta.Text, txtNombreCharla.Text, cboEstado.SelectedValue)
        End If
    End Sub
    Private Sub grdListado_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdListado.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)

        Dim str_idCharla As String = CType(row.FindControl("hdnidCharla"), HiddenField).Value
        Dim str_estado As String = CType(row.FindControl("lblEstado"), Label).Text

        Try
            If e.CommandName = "Asignar" Then
                If str_estado = "ACTIVA" Then
                    Session("idCharla") = str_idCharla
                    Response.Redirect("~/Modulos/FormacionOnline/aspAsignacionCharlas.aspx")
                Else
                    MessageBox("Confirmar", "Charla Inactiva", Page, Master, "W")
                End If
            End If

            If e.CommandName = "Editar" Then
                Session("idCharla") = str_idCharla
                Response.Redirect("~/Modulos/FormacionOnline/aspCharla.aspx")
            End If

            If e.CommandName = "Eliminar" Then
                EliminarCharla(str_idCharla)
                Dim tipoAsig As String = ""
                Call CargarGrillaListado(cboCategoria.SelectedValue, cboSubCategoria.SelectedValue, txtDesde.Text, txtHasta.Text, txtNombreCharla.Text, cboEstado.SelectedValue)
                Call contadorCharla(cboCategoria.SelectedValue, cboSubCategoria.SelectedValue, txtDesde.Text, txtHasta.Text, txtNombreCharla.Text, cboEstado.SelectedValue)
            End If
        Catch ex As Exception
            MessageBoxError("grdListado_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdListado_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdListado.RowDataBound
        Dim row As GridViewRow = e.Row

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim objEliminar As LinkButton = CType(row.FindControl("btnEliminar"), LinkButton)
            Dim strAsignacion As String = CType(Row.FindControl("lblAsignacion"), Label).Text

            If strAsignacion <> "S/A" Then
                objEliminar.Visible = False
            End If
        End If
    End Sub

    Function EliminarCharla(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"

            comando.Parameters.Add("@id_charla", SqlDbType.VarChar)
            comando.Parameters("@id_charla").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarCharla", Err, Page, Master)
            Return False
        End Try
    End Function


    Function contadorCharla(ByVal categoria As String, ByVal subcategoria As String, ByVal desde As String, ByVal hasta As String, ByVal nom_charla As String, ByVal Asignacion As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_listadoCharlas 'CCH',@id_categoria='" & categoria & "',@id_subcategoria='" & subcategoria & "',@nom_charla='" & nom_charla & "',@fec_desde='" & desde & "',@fec_hasta='" & hasta & "',@nom_asignacion='" & Asignacion & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblTotal.Text = rdoReader(0)
                    lblAsignadas.Text = rdoReader(1)
                    lblPorASignar.Text = rdoReader(2)
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("contadorCharla", Err, Page, Master)
            Return False
        End Try
    End Function

End Class