﻿
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports aspSistemasJorquera.vbClasePrincipal
Imports aspSistemasJorquera.cargaDrive



Public Class aspCharla
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                hdnidCharla.Value = Session("idCharla")
                Session("idCharla") = ""
                CargarCategoria()
                Session("preguntas") = tablaPreguntas()
                CargarSubcategoria(0)

                If hdnidCharla.Value <> "" Then
                    CargarDatosActualizar()
                Else

                End If
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Public Sub CargarDatosActualizar()
        MostrarDatos(hdnidCharla.Value)
        MostrarPreguntas(hdnidCharla.Value)
        txtNombreCharla.Enabled = False
        cboCategoria.Enabled = False
        cboSubCategoria.Enabled = False
        txtDescripcion.Enabled = False
    End Sub

    Public Sub MostrarDatos(ByVal id As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_Charlas 'SCHA',@id_charla='" & id & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtNombreCharla.Text = rdoReader(1)
                    cboCategoria.SelectedValue = rdoReader(2)
                    CargarSubcategoria(cboCategoria.SelectedValue)
                    cboSubCategoria.SelectedValue = rdoReader(3)
                    txtDescripcion.Text = rdoReader(4)
                    lblNombreVideo.Text = rdoReader(5).ToString
                    txtDesde.Text = rdoReader(7)
                    txtHasta.Text = rdoReader(8)
                    'txtDesde.Text = "2021-03-01"
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarNombre", Err, Page, Master)

        End Try
    End Sub

    Public Sub MostrarPreguntas(ByVal id As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_Charlas 'SPREGC',@id_charla='" & id & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                While rdoReader.Read
                    grdPreguntas.Visible = True
                    Dim dtTmpFila As DataRow
                    Dim ocar As New DataTable
                    ocar = Session("preguntas")
                    dtTmpFila = ocar.NewRow
                    dtTmpFila("Pregunta") = rdoReader(0)
                    dtTmpFila("RespuestaCorrecta") = rdoReader(1)
                    ocar.Rows.Add(dtTmpFila)
                    grdPreguntas.DataSource = Session("preguntas")
                    grdPreguntas.DataBind()
                End While
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarNombre", Err, Page, Master)

        End Try
    End Sub

    Private Function tablaPreguntas() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("Pregunta", Type.GetType("System.String"))
            dtTemporal.Columns.Add("RespuestaCorrecta", Type.GetType("System.String"))
        Catch ex As Exception
            MessageBox("Charla", "tablaPreguntas", Page, Master, "E")
        End Try
        Return dtTemporal
    End Function

    Private Sub CargarCategoria()
        Try
            Dim strNomTablaR As String = "FOMae_categoria"
            Dim strSQLR As String = "Exec pro_FO_Charlas 'SCAT'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "nom_categoria"
            cboCategoria.DataValueField = "id_categoria"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("Charla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSubcategoria(ByVal id As String)
        Try
            Dim strNomTablaR As String = "FOMae_subcategorias"
            Dim strSQLR As String = "Exec pro_FO_Charlas 'SSCAT',@id_categoria=" & id & ""
            cboSubCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSubCategoria.DataTextField = "nom_subcategoria"
            cboSubCategoria.DataValueField = "id_subcategoria"
            cboSubCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("Charla", Err, Page, Master)
        End Try
    End Sub

    Private Sub cboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCategoria.SelectedIndexChanged
        CargarSubcategoria(cboCategoria.SelectedValue)
    End Sub

    Private Sub btnAgregarPregunta_Click(sender As Object, e As EventArgs) Handles btnAgregarPregunta.Click
        If txtPregunta.Text <> "" Then
            Try
                grdPreguntas.Visible = True
                Dim dtTmpFila As DataRow
                Dim ocar As New DataTable
                ocar = Session("preguntas")
                dtTmpFila = ocar.NewRow
                dtTmpFila("Pregunta") = txtPregunta.Text
                dtTmpFila("RespuestaCorrecta") = cboRespuesta.SelectedValue
                ocar.Rows.Add(dtTmpFila)
                grdPreguntas.DataSource = Session("preguntas")
                grdPreguntas.DataBind()
            Catch ex As Exception
                MessageBoxError("btnAgregarPregunta_Click", Err, Page, Master)
            End Try
        Else
            MessageBox("Confirmar", "Favor Ingresar Pregunta", Page, Master, "W")
        End If
    End Sub

    Private Sub grdPreguntas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPreguntas.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = sender.Rows(index)

        Dim str_pregunta As String = CType(row.FindControl("lblPregunta"), Label).Text

        Try
            If e.CommandName = "Eliminar" Then
                Dim tabla As New DataTable
                tabla = Session("preguntas")
                For i = 0 To tabla.Rows.Count - 1
                    If tabla.Rows.Item(i).Item("Pregunta").ToString = str_pregunta Then
                        tabla.Rows.Item(i).Delete()
                        Exit For
                    End If
                Next
                grdPreguntas.DataSource = tabla
                grdPreguntas.DataBind()

                If grdPreguntas.Rows.Count() = 0 Then
                    grdPreguntas.Visible = False
                Else
                    grdPreguntas.Visible = True
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdPreguntas_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Function validar()
        If txtNombreCharla.Text <> "" Then
            If cboCategoria.SelectedValue <> 0 Then
                If cboSubCategoria.SelectedValue <> 0 Then
                    If flpVideo.HasFile = True Or lblNombreVideo.Text <> "" Then
                        If txtDesde.Text <> "" Then
                            If txtDesde.Text <> "" Then
                                Dim hoy As String = Year(Date.Now) & "-" & Month(Date.Now) & "-" & Day(Date.Now)
                                Dim desde As String = Year(txtDesde.Text) & "-" & Month(txtDesde.Text) & "-" & Day(txtDesde.Text)
                                Dim hasta As String = Year(txtHasta.Text) & "-" & Month(txtHasta.Text) & "-" & Day(txtHasta.Text)
                                If desde < hoy Then
                                    MessageBox("Confirmar", "fecha DESDE menor a la actual", Page, Master, "W")
                                Else
                                    If desde > hasta Then
                                        MessageBox("Confirmar", "fecha desde mayor a la de finalización(Hasta)", Page, Master, "W")
                                    Else
                                        If grdPreguntas.Rows.Count >= 1 Then
                                            If Path.GetExtension(flpVideo.FileName) = ".mp4" Or Right(lblNombreVideo.Text, 4) = ".mp4" Then
                                                Return True
                                            Else
                                                MessageBox("Confirmar", "Formato de Video No valido", Page, Master, "W")
                                                Return False
                                            End If
                                        Else
                                            MessageBox("Confirmar", "Ingrese Preguntas", Page, Master, "W")
                                            Return False
                                        End If
                                    End If
                                End If
                            Else
                                MessageBox("Confirmar", "Ingresar Fecha de Fin", Page, Master, "W")
                                Return False
                            End If
                        Else
                            MessageBox("Confirmar", "Ingresar Fecha de Inicio", Page, Master, "W")
                            Return False
                        End If

                    Else
                        MessageBox("Confirmar", "Seleccione Video", Page, Master, "W")
                        Return False
                    End If
                Else
                    MessageBox("Confirmar", "Seleccione Subcategoría", Page, Master, "W")
                    Return False
                End If
            Else
                MessageBox("Confirmar", "Seleccione Categoría", Page, Master, "W")
                Return False
            End If
        Else
            MessageBox("Confirmar", "Ingrese nombre de charla", Page, Master, "W")
            Return False
        End If
    End Function


    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If validar() Then
            If hdnidCharla.Value = "" Then 'insertar
                If InsertarCharla() = True Then
                    'insertar preguntas
                    For Each row As GridViewRow In grdPreguntas.Rows
                        Dim str_pregunta As String = CType(row.FindControl("lblPregunta"), Label).Text
                        Dim str_respuesta As String = CType(row.FindControl("lblRspCorrecta"), Label).Text
                        Call InsertarPreguntas(str_pregunta, str_respuesta)
                    Next
                    'insertar video
                    CargarArchivo(flpVideo, Me.Request)
                    MessageBox("Confirmar", "Registro Guardado Exitosamente", Page, Master, "S")
                    'limpiar()
                    bloqueo()
                End If
            Else 'Modificar
                If ActualizarCharla(hdnidCharla.Value) = True Then
                    EliminarPreguntas(hdnidCharla.Value)
                    'insertar preguntas
                    For Each row As GridViewRow In grdPreguntas.Rows
                        Dim str_pregunta As String = CType(row.FindControl("lblPregunta"), Label).Text
                        Dim str_respuesta As String = CType(row.FindControl("lblRspCorrecta"), Label).Text
                        Call InsertarPreguntasActualizacion(hdnidCharla.Value, str_pregunta, str_respuesta)
                    Next
                    'insertar video
                    If flpVideo.HasFile = True Then
                        CargarArchivo(flpVideo, Me.Request)
                    End If
                    MessageBox("Confirmar", "Registro Actualizado Exitosamente", Page, Master, "S")
                    'limpiar()
                    bloqueo()
                End If
            End If
        End If
    End Sub

    Public Sub bloqueo()
        dvAsignar.Visible = True
        txtNombreCharla.Enabled = False
        cboCategoria.Enabled = False
        cboSubCategoria.Enabled = False
        txtDescripcion.Enabled = False
        flpVideo.Enabled = False
        txtDesde.Enabled = False
        txtHasta.Enabled = False
        btnGuardar.Enabled = False
        txtPregunta.Enabled = False
        cboRespuesta.Enabled = False
        btnAgregarPregunta.Enabled = False
    End Sub

    Public Sub desbloqueo()
        dvAsignar.Visible = False
        txtNombreCharla.Enabled = True
        cboCategoria.Enabled = True
        cboSubCategoria.Enabled = True
        txtDescripcion.Enabled = True
        flpVideo.Enabled = True
        txtDesde.Enabled = True
        txtHasta.Enabled = True
        btnGuardar.Enabled = True
        txtPregunta.Enabled = True
        cboRespuesta.Enabled = True
        btnAgregarPregunta.Enabled = True
    End Sub

    Private Function CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest) As Boolean
        Dim _carpeta As String = "C:\FormacionOnline\" & txtNombreCharla.Text.Replace(" ", "_") & "\"

        If Not Directory.Exists(_carpeta) Then
            Directory.CreateDirectory(_carpeta)
        End If

        Dim _directorioGral As String = instancia.PhysicalApplicationPath
        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName.Replace(" ", "_")

                    '----------------------Eliminar Archivos Existentes------------------------------
                    Call EliminarArchivos(txtNombreCharla.Text.Replace(" ", "_") & "\")
                    '-----------------------Guardar nuevo archivo------------------------------
                    archivo.SaveAs(_directorioParaGuardar)
                    Else
                    MessageBox("Cargar Archivo", "Debe seleccionar un archivo válido (.mp4)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
            Return False
        End Try
    End Function

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".mp4"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos(ruta As String)
        Dim _carpeta As String = "C:\FormacionOnline\" & ruta
        Dim _directorioGral As String = _carpeta
        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub


    Function ExisteCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String) As Boolean
        Dim Request As FtpWebRequest = CType(WebRequest.Create(New Uri("ftp://192.168.10.28/FormacionOnline/" & ruta)), FtpWebRequest)
        Request.Credentials = New NetworkCredential(usuario, pass)
        Request.Method = WebRequestMethods.Ftp.GetDateTimestamp
        Request.UsePassive = False
        Try
            Dim respuesta As FtpWebResponse
            respuesta = CType(Request.GetResponse(), FtpWebResponse)
            Return True
        Catch ex As WebException
            Return False
        End Try
    End Function

    Public Sub CrearCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Dim crearDirectorio As FtpWebRequest = DirectCast(System.Net.FtpWebRequest.Create("ftp://192.168.10.28/FormacionOnline/" & ruta), System.Net.FtpWebRequest)
        crearDirectorio.Credentials = New NetworkCredential(usuario, pass)
        crearDirectorio.Method = WebRequestMethods.Ftp.MakeDirectory
        Dim respuesta As FtpWebResponse = crearDirectorio.GetResponse()
    End Sub

    Function InsertarCharla() As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "INSC"

            comando.Parameters.Add("@nom_charla", SqlDbType.VarChar)
            comando.Parameters("@nom_charla").Value = Trim(txtNombreCharla.Text)

            comando.Parameters.Add("@des_charla", SqlDbType.VarChar)
            comando.Parameters("@des_charla").Value = Trim(txtDescripcion.Text)

            comando.Parameters.Add("@id_Categoria", SqlDbType.Int)
            comando.Parameters("@id_Categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@id_subcategoria", SqlDbType.Int)
            comando.Parameters("@id_subcategoria").Value = cboSubCategoria.SelectedValue

            comando.Parameters.Add("@fec_desde", SqlDbType.Date)
            comando.Parameters("@fec_desde").Value = txtDesde.Text

            comando.Parameters.Add("@fec_hasta", SqlDbType.Date)
            comando.Parameters("@fec_hasta").Value = txtHasta.Text

            comando.Parameters.Add("@nom_video", SqlDbType.VarChar)
            comando.Parameters("@nom_video").Value = flpVideo.FileName

            comando.Parameters.Add("@url_video", SqlDbType.VarChar)
            comando.Parameters("@url_video").Value = Trim("http://archivos.viatrack.cl/" & txtNombreCharla.Text.Replace(" ", "_") & "/" & flpVideo.FileName.Replace(" ", "_"))

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarCharla", Err, Page, Master)
            Return False
        End Try
    End Function

    Function ActualizarCharla(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@Tipo", SqlDbType.NVarChar)
            comando.Parameters("@Tipo").Value = "UP"

            comando.Parameters.Add("@id_charla", SqlDbType.Int)
            comando.Parameters("@id_charla").Value = id

            comando.Parameters.Add("@fec_desde", SqlDbType.Date)
            comando.Parameters("@fec_desde").Value = txtDesde.Text

            comando.Parameters.Add("@fec_hasta", SqlDbType.Date)
            comando.Parameters("@fec_hasta").Value = txtHasta.Text

            If flpVideo.HasFile = True Then
                comando.Parameters.Add("@url_video", SqlDbType.VarChar)
                comando.Parameters("@url_video").Value = Trim("http://archivos.viatrack.cl/" & txtNombreCharla.Text.Replace(" ", "_") & "/" & flpVideo.FileName.Replace(" ", "_"))
            End If

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("ActualizarCharla", Err, Page, Master)
            Return False
        End Try
    End Function

    Function EliminarPreguntas(ByVal id As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@Tipo", SqlDbType.NVarChar)
            comando.Parameters("@Tipo").Value = "EP"

            comando.Parameters.Add("@id_charla", SqlDbType.Int)
            comando.Parameters("@id_charla").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarPreguntas", Err, Page, Master)
            Return False
        End Try
    End Function


    Function InsertarPreguntas(ByVal pregunta As String, ByVal respuesta As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IPR"

            comando.Parameters.Add("@nom_pregunta", SqlDbType.VarChar)
            comando.Parameters("@nom_pregunta").Value = Trim(pregunta)

            comando.Parameters.Add("@rsp_correcta", SqlDbType.VarChar)
            comando.Parameters("@rsp_correcta").Value = Trim(respuesta)

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarPreguntas", Err, Page, Master)
            Return False
        End Try
    End Function

    Function InsertarPreguntasActualizacion(ByVal id As String, ByVal pregunta As String, ByVal respuesta As String) As Boolean
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_FO_Charlas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UPPRE"

            comando.Parameters.Add("@id_charla", SqlDbType.VarChar)
            comando.Parameters("@id_charla").Value = id


            comando.Parameters.Add("@nom_pregunta", SqlDbType.VarChar)
            comando.Parameters("@nom_pregunta").Value = Trim(pregunta)

            comando.Parameters.Add("@rsp_correcta", SqlDbType.VarChar)
            comando.Parameters("@rsp_correcta").Value = Trim(respuesta)

            comando.Parameters.Add("@usr", SqlDbType.VarChar)
            comando.Parameters("@usr").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("InsertarPreguntasActualizacion", Err, Page, Master)
            Return False
        End Try
    End Function

    Public Sub limpiar()
        txtNombreCharla.Text = ""
        CargarCategoria()
        CargarSubcategoria(0)
        flpVideo.Dispose()
        txtPregunta.Text = ""
        cboRespuesta.SelectedValue = "SI"
        hdnidCharla.Value = ""
        lblNombreVideo.Text = ""
        Dim dtPreguntas As New DataTable
        dtPreguntas = Session("preguntas")
        dtPreguntas.Clear()
        grdPreguntas.DataSource = dtPreguntas
        grdPreguntas.DataBind()
        grdPreguntas.Visible = False
        txtNombreCharla.Enabled = True
        cboCategoria.Enabled = True
        cboSubCategoria.Enabled = True
        txtDescripcion.Enabled = True
        txtDesde.Text = ""
        txtHasta.Text = ""
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        desbloqueo()
        limpiar()
    End Sub

    Private Sub btnASignar_Click(sender As Object, e As EventArgs) Handles btnASignar.Click

        Session("idCharla") = RescatarID()
        Response.Redirect("~/Modulos/FormacionOnline/aspAsignacionCharlas.aspx")
    End Sub


    Function RescatarID() As String
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_FO_Charlas 'SUIC'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Return rdoReader(0)
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarID", Err, Page, Master)
        End Try
    End Function

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdExportarCat As New DataGrid

            grdExportarCat.CellPadding = 0
            grdExportarCat.CellSpacing = 0
            grdExportarCat.BorderStyle = BorderStyle.None

            grdExportarCat.Font.Size = 10

            Dim strNomTabla As String = "FOMae_categoria"
            Dim strSQL As String = "Exec pro_FO_Charlas 'RPT'"
            grdExportarCat.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdExportarCat.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdExportarCat.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=FOCharlas.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBox("Exportar", "btnExportarCat_Click", Page, Master, "E")
        End Try
    End Sub
End Class