﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspConfiguracionCharlas.aspx.vb" Inherits="aspSistemasJorquera.aspSubCategoria" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-success">
        <li>
            <asp:LinkButton ID="btnCategoria" CssClass="bg-green" runat="server">Categoría</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnSubcategoria" CssClass="bg-success" runat="server">Subcategoría</asp:LinkButton></li>
    </ul>
    <asp:MultiView ID="MultiView1" runat="server">
        <!-- ***************************** Categoría  ***************************** -->
        <asp:View ID="vwCategoria" runat="server">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row btn-sm">

                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="Label2" runat="server" Text="INGRESO DE CATEGORÍAS"></asp:Label>
                            <small></small>
                        </h3>
                    </div>
                </div>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Nombre Categoría</label>
                        <div>
                            <asp:TextBox ID="txtNomCategoria" runat="server" placeholder="Nombre categoría..." MaxLength="300" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname"
                            class="">
                        </label>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="chkActivoCat" runat="server" clientidmode="Static" checked="checked">
                            <label class="custom-control-label" for="chkActivoCat">Activa</label>
                        </div>
                    </div>

                    <div class="col-md-1 pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnExportarCat" CssClass="btn botonesTC-descargar btn-block" runat="server">Reporte</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnCancelarCat" CssClass="btn btn-primary btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnGuardarCat" CssClass="btn btn-primary btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:TextBox ID="txtBuscarCat" runat="server" AutoPostBack="true" placeholder="Buscar por Nombre.." MaxLength="300" Width="30%" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        <asp:GridView ID="grdCategoria" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                            <Columns>
                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True"
                                    ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="NOMBRE CATEGORÍA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombreCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                        <asp:HiddenField ID="hdnIdCategoria" runat="server" Value='<%# Bind("id_categoria")%>' />
                                        <asp:HiddenField ID="hdnEstado" runat="server" Value='<%# Bind("est_activa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ESTADO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomEstadoCat" runat="server" Text='<%# Bind("nom_estado")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CREADOR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomCreadorCat" runat="server" Text='<%# Bind("usr_add")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="FECHA CREACIÓN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaCreacionCat" runat="server" Text='<%# Bind("fec_add")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ELIMINAR">
                                    <ItemTemplate>
                                        <h4>
                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Eliminar" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </h4>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </div>
                <asp:HiddenField ID="hdnIdCategoria" runat="server" />
            </section>
        </asp:View>

        <!-- ***************************** Subcategoría  ***************************** -->
        <asp:View ID="vwSubcategoria" runat="server">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="Label1" runat="server" Text="INGRESO DE SUBCATEGORÍAS"></asp:Label>
                            <small></small>
                        </h3>
                    </div>
                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-3 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Nombre Subcategoria</label>
                        <div>
                            <asp:TextBox ID="txtNombreSubCategoria" runat="server" placeholder="Nombre Subcategoria..." MaxLength="300" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Categoría</label>
                        <div>
                            <asp:DropDownList ID="cboCategoria" CssClass="form-control" runat="server" AutoPostBack="true">
                                <asp:ListItem>Seleccione</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname"
                            class="control-label col-form-label">
                            Modalidad</label>
                        <div>
                            <asp:DropDownList ID="cboModalidad" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname"
                            class="">
                        </label>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="chkActivo" runat="server" clientidmode="Static" checked="checked">
                            <label class="custom-control-label" for="chkActivo">Activa</label>
                        </div>
                    </div>
                    <div class="col-md-1 pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnExportarSub" CssClass="btn botonesTC-descargar btn-block" runat="server">Reporte</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnCancelarSubcategoria" CssClass="btn btn-primary btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnGuardarSubcategoria" CssClass="btn btn-primary btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:TextBox ID="txtBuscar" runat="server" AutoPostBack="true" placeholder="Buscar por Nombre.." Width="30%" MaxLength="300" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        <asp:GridView ID="grdSubcategoria" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                            <Columns>
                                <asp:CommandField ButtonType="Image" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True"
                                    ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="NOMBRE SUBCATEGORIA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_subcategoria")%>'></asp:Label>
                                        <asp:HiddenField ID="hdnIdSubcategoria" runat="server" Value='<%# Bind("id_subcategoria")%>' />
                                        <asp:HiddenField ID="hdnIdModalidad" runat="server" Value='<%# Bind("id_modalidad")%>' />
                                        <asp:HiddenField ID="hdnIdCategoria" runat="server" Value='<%# Bind("id_categoria")%>' />
                                        <asp:HiddenField ID="hdnEstado" runat="server" Value='<%# Bind("est_activa")%>' />

                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CATEGORÍA">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomCategoria" runat="server" Text='<%# Bind("nom_categoria")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MODALIDAD">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomModalidad" runat="server" Text='<%# Bind("nom_modalidad")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ESTADO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomEstado" runat="server" Text='<%# Bind("nom_estado")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CREADOR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomCreador" runat="server" Text='<%# Bind("usr_add")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="FECHA CREACIÓN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaCreacion" runat="server" Text='<%# Bind("fec_add")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ELIMINAR">
                                    <ItemTemplate>
                                        <h4>
                                            <asp:LinkButton ID="btnEliminarCat" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="Eliminar" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </h4>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                </asp:TemplateField>

                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                        </asp:GridView>
                    </div>
                </div>
            </section>

            <asp:HiddenField ID="hdnId" runat="server" />
        </asp:View>
    </asp:MultiView>


</asp:Content>
