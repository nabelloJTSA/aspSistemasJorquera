﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal


Public Class aspValidarViajeHR
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Session("est_asignado_tc") = 0

                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")
                Call CargarClientes()
                Call CargarServicios()
                Call CargarTransportistas()
                Call CargarCarga()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_HR 'CCC', '" & Session("cod_usu_tc") & "'"
            cboClientes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientes.DataTextField = "nom_cliente1"
            cboClientes.DataValueField = "cod_msoft"
            cboClientes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_HR 'CCS', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_HR 'CCT'"
            cboTrasportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasportista.DataTextField = "nom_transportista"
            cboTrasportista.DataValueField = "cod_msoft"
            cboTrasportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaViajes()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_HR 'CGV2', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & Trim(txtExp.Text) & "', '" & Trim(txtGuia.Text) & "'"
            grdViajes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdViajes.DataBind()
            lblTotReg.Text = grdViajes.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaViajes", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnExportarViajes_Click(sender As Object, e As EventArgs) Handles btnExportarViajes.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_HR 'CGV2', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & Trim(txtExp.Text) & "', '" & Trim(txtGuia.Text) & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=ViajesTerminados.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarAlertas_Click", Err, Page, Master)
        End Try
    End Sub


    Protected Sub VerFichaViaje(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("transportista_tc") = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value
        Session("origen_tc") = CType(row.FindControl("btnOrigen"), LinkButton).Text
        Session("destino_tc") = CType(row.FindControl("btnDestino"), LinkButton).Text
        Session("camion_tc") = CType(row.FindControl("btnCamion"), LinkButton).Text
        Session("arrastre_tc") = CType(row.FindControl("btnArrastre"), LinkButton).Text
        Session("conductor_tc") = CType(row.FindControl("lblNomConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("btnRutConductor"), LinkButton).Text

        Session("nun_fono_tc") = CType(row.FindControl("hdnFono"), HiddenField).Value

        Session("score") = CType(row.FindControl("hdnScoreConductor"), HiddenField).Value
        Session("eta_TC") = "0"


        Session("lat_ficha_tc") = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        Session("lon_ficha_tc") = CType(row.FindControl("hdnLonViaje"), HiddenField).Value
        Session("origen_ficha_tc") = CType(row.FindControl("hdnCodOrigen"), HiddenField).Value

        Session("ficha_HR") = 1

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspFichaViaje.aspx")

    End Sub

    Protected Sub CargarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text

        Response.Redirect("~/Modulos/TorreControl/HojaDeRuta/Procesos/aspPodHR.aspx")

    End Sub

    Protected Sub CargarExtraCostoClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text

        Response.Redirect("~/Modulos/TorreControl/HojaDeRuta/Procesos/aspExtraCostos.aspx")

    End Sub


    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text

                If CDate(objFechaVenc) <= CDate(Date.Now.Date) Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnVerDocCamion_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("btnCamion"), LinkButton).Text, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("btnCamion"), LinkButton).Text, "-", ""))

        lblTrasnportista.Text = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocArrastre_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("btnArrastre"), LinkButton).Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("btnArrastre"), LinkButton).Text, "-", ""))
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Protected Sub btnVerDocConductor_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False

        Call CargarGrillaDocumentos(2, Trim(CType(row.FindControl("btnRutConductor"), LinkButton).Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("lblNomConductor"), Label).Text, "-", "")) & " / " & Trim(CType(row.FindControl("hdnFono"), HiddenField).Value)
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnTipoViajeClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Call CargarGrillaMultipaso(CType(row.FindControl("hdnOS"), HiddenField).Value, CType(row.FindControl("hdnCodcliente"), HiddenField).Value)

        pnlDoctos.Visible = False
        pnlDetalleViaje.Visible = True

        lblNumOS.Text = Trim(Replace(CType(row.FindControl("btnNumViaje"), LinkButton).Text, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Private Sub CargarGrillaMultipaso(ByVal os As String, ByVal cod_cliente As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_HR 'CDM' , '" & os & "', '" & cod_cliente & "'"
            grdMultipaso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdMultipaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaMultipaso", Err, Page, Master)
        End Try
    End Sub





    Private Sub grdViajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdViajes.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("lblValidacionHR"), Label).Text = "VALIDADO" Then
                    CType(row.FindControl("btnValidarViaje"), LinkButton).Text = "VER VALIDACION"

                    CType(row.FindControl("btnCargarPOD"), LinkButton).Enabled = False
                    CType(row.FindControl("btnExtraCostos"), LinkButton).Enabled = False
                    CType(row.FindControl("btnBaremos"), LinkButton).Enabled = False
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdViajes_RowDataBound", Err, Page, Master)
        End Try
    End Sub



    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs) Handles txtDesde.TextChanged
        If CDate(txtDesde.Text) > CDate(txtHasta.Text) Then
            txtHasta.Text = txtDesde.Text
        End If
    End Sub

    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs) Handles txtHasta.TextChanged
        If CDate(txtHasta.Text) < CDate(txtDesde.Text) Then
            txtDesde.Text = txtHasta.Text
        End If
    End Sub



    Private Sub CargarCarga()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_HR 'CCTC'"
            cboBCargas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboBCargas.DataTextField = "nom_carga"
            cboBCargas.DataValueField = "cod_carga"
            cboBCargas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoCarga", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CargarGrillaBaremos(ByVal id As Integer)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_HR 'CGBAR', " & id & ""
            grdBar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdBar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaBaremos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CargarGrillaBaremosValidar()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_HR 'CGBAR', " & Session("num_viaje_tc") & ""
            grdBaremoValidar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdBaremoValidar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaBaremosValidar", Err, Page, Master)
        End Try
    End Sub


    Protected Sub InsertarBaremos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_HR"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "INB"
            comando.Parameters.Add("@num_viaje", SqlDbType.Int)
            comando.Parameters("@num_viaje").Value = Session("id_viaje_baremo")
            comando.Parameters.Add("@num_secuencia", SqlDbType.Int)
            comando.Parameters("@num_secuencia").Value = cboBPuntoPaso.SelectedValue
            comando.Parameters.Add("@valor_baremos", SqlDbType.Int)
            comando.Parameters("@valor_baremos").Value = txtBValor.Text
            comando.Parameters.Add("@tipo_carga", SqlDbType.Int)
            comando.Parameters("@tipo_carga").Value = cboBCargas.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("InsertarBaremos", Err, Page, Master)
        End Try
    End Sub



    Protected Sub EliminarBaremos(ByVal id As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_HR"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "DELB"
            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("BorrarBaremos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CargarPuntos(ByVal id As Integer)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_HR 'CCSEC'," & id & ""
            cboBPuntoPaso.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboBPuntoPaso.DataTextField = "nom_paso"
            cboBPuntoPaso.DataValueField = "num_secuencia"
            cboBPuntoPaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPuntosdePaso", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnBBorrar_Click(sender As Object, e As EventArgs)

        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdBar.Rows(rowIndex)
        Dim id As Integer = CType(row.FindControl("hdnIDBaremos"), HiddenField).Value
        Call EliminarBaremos(id)
        Call CargarGrillaBaremos(hdnViajeBaremos.Value)
        MessageBox("Eliminar", "Registro eliminado", Page, Master, "S")

    End Sub


    Protected Sub btnLimpiarFiltroGral_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltroGral.Click
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")

        cboClientes.ClearSelection()
        cboServicio.ClearSelection()
        cboTrasportista.ClearSelection()
        hdnIdViaje.Value = 0
        txtExp.Text = ""
        txtGuia.Text = ""

        Call CargarGrillaViajes()

    End Sub

    Protected Sub bntActualizarReporte_Click(sender As Object, e As EventArgs) Handles bntActualizarReporte.Click
        Call CargarGrillaViajes()
    End Sub


    Protected Sub btnBaremos_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        hdnViajeBaremos.Value = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        hdnSecuenciaBaremos.Value = CType(row.FindControl("hdnNumSecuencia"), HiddenField).Value
        lblTitBaremo.Text = "Baremo Viaje: " & CType(row.FindControl("btnNumViaje"), LinkButton).Text

        Session("id_viaje_baremo") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("num_secuencia_baremo") = CType(row.FindControl("hdnNumSecuencia"), HiddenField).Value

        Call CargarGrillaBaremos(hdnViajeBaremos.Value)
        Call CargarPuntos(hdnViajeBaremos.Value)
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mdlBar", "$('#mdlBar').modal();", True)
    End Sub





    Protected Sub btnValidarViajes_Click(sender As Object, e As EventArgs) Handles btnValidarViajes.Click
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_HR"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "VVI"
            comando.Parameters.Add("@num_viaje", SqlDbType.Int)
            comando.Parameters("@num_viaje").Value = Session("num_viaje_tc")
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call CargarGrillaViajes()
            MessageBox("Validar Viaje", "Viaje: " & Session("num_viaje_tc") & " VALIDADO", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("btnValidarViajes_Click", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnValidarClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        If CType(row.FindControl("lblValidacionHR"), Label).Text = "VALIDADO" Then
            btnValidarViajes.Visible = False
        End If


        lbltitValidar.Text = "VALIDAR VIAJE: " & CType(row.FindControl("btnNumViaje"), LinkButton).Text

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text

        txtTransportista.Text = CType(row.FindControl("lblTrasnportista"), Label).Text
        txtOrigen.Text = CType(row.FindControl("btnOrigen"), LinkButton).Text
        txtDestino.Text = CType(row.FindControl("btnDestino"), LinkButton).Text
        txtCamion.Text = CType(row.FindControl("btnCamion"), LinkButton).Text
        txtArrastre.Text = CType(row.FindControl("btnArrastre"), LinkButton).Text
        txtservicio.Text = CType(row.FindControl("hdnNomServicio"), HiddenField).Value

        txtConductorB.Text = CType(row.FindControl("lblNomConductor"), Label).Text
        txtConductorEX.Text = CType(row.FindControl("lblNomConductor"), Label).Text
        txtConductorPOD.Text = CType(row.FindControl("lblNomConductor"), Label).Text


        Call CargarGrillaPOD()
        Call CargarGrillaExtraCosto()
        Call CargarGrillaBaremosValidar()

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalValidar();", True)
    End Sub


    Private Sub CargarGrillaPOD()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_pod 'CGD', '" & Session("num_viaje_tc") & "'"
            grdPOD.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPOD.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPOD", Err, Page, Master)
        End Try
    End Sub


    Protected Sub DescargarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPOD.Rows(rowIndex)

        Dim archivo As String = "~/Temp/" & CType(row.FindControl("hdnNomTipo"), HiddenField).Value

        Call DescargarFTP(archivo, CType(row.FindControl("hdnNomTipo"), HiddenField).Value)

        Response.ContentType = "application/ms-word"
        Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(row.FindControl("hdnNomTipo"), HiddenField).Value)
        Response.TransmitFile(Server.MapPath(archivo))
        Response.End()
    End Sub


    Private Sub DescargarFTP(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If
            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/POD_VIAJES/Viaje_" & Session("num_viaje_tc") & "/" & Trim(nombre), Server.MapPath(archivo), "usuarioftp", "Ftp2018.")

        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaExtraCosto()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_extra_costos 'CGD', '" & Session("num_viaje_tc") & "'"
            grdExtraCosto.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdExtraCosto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaExtraCosto", Err, Page, Master)
        End Try
    End Sub

    Protected Sub DescargarEXClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdExtraCosto.Rows(rowIndex)

        Dim archivo As String = "~/Temp/" & CType(row.FindControl("hdnNomTipo"), HiddenField).Value

        Call DescargarFTPEX(archivo, CType(row.FindControl("hdnNomTipo"), HiddenField).Value)

        Response.ContentType = "application/ms-word"
        Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(row.FindControl("hdnNomTipo"), HiddenField).Value)
        Response.TransmitFile(Server.MapPath(archivo))
        Response.End()
    End Sub

    Private Sub DescargarFTPEX(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/EXTRA_COSTOS_VIAJES/Viaje_" & Session("num_viaje_tc") & "/" & Trim(nombre), Server.MapPath(archivo), "usuarioftp", "Ftp2018.")

        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub




End Class