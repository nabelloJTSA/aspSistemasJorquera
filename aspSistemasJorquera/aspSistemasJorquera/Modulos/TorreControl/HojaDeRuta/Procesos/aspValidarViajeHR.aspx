﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspValidarViajeHR.aspx.vb" Inherits="aspSistemasJorquera.aspValidarViajeHR" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <asp:UpdatePanel runat="server">
        <ContentTemplate>


            <script type='text/javascript'>
                function openModalValidar() {
                    $('[id*=mdlValidar]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>


            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>


            <script type='text/javascript'>
                function openModalncidencias() {
                    $('[id*=mdlIncidencias]').modal('show');
                }
            </script>

            <!-- Content Header (Page header) -->

            <section class="content-header">

                <div class="row">
                    <div class="col-md-5 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text="Validar Viajes / Hoja de Ruta"></asp:Label>
                        </h3>
                    </div>

                </div>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- SELECT2 EXAMPLE -->
                <div class="row">
                    <div class="col-md-12">


                        <div class="row">


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-3 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Trasportista</label>
                                <div>
                                    <asp:DropDownList ID="cboTrasportista" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    POD</label>
                                <div>
                                    <asp:DropDownList ID="cboPOD" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="3">TODOS</asp:ListItem>
                                        <asp:ListItem Value="1">SI</asp:ListItem>
                                        <asp:ListItem Value="0">NO</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>



                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Expedición</label>
                                <div>

                                    <asp:TextBox ID="txtExp" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Guia</label>
                                <div>

                                    <asp:TextBox ID="txtGuia" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:LinkButton ID="bntActualizarReporte" CssClass="btn btn-primary btn-block" runat="server">Actualizar Reporte</asp:LinkButton>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:LinkButton ID="btnLimpiarFiltroGral" CssClass="btn btn-primary btn-block" runat="server">Limpiar Filtros</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="row table-responsive">
                            <div class="col-md-12">
                                <div class="box">

                                    <div class="box-body">

                                        <div class="row">
                                            <div class="col-md-5 form-group  ">
                                                <div>
                                                    <h4>
                                                        <asp:Label ID="Label11" CssClass="text-primary" runat="server" Text="TOTAL REGISTROS: "></asp:Label>
                                                        <b>
                                                            <asp:Label ID="lblTotReg" CssClass="text-primary" runat="server" Text=""></asp:Label></b></h4>
                                                </div>
                                            </div>


                                            <div class="col-md-1 form-group  pull-right">
                                                <div>
                                                    <asp:LinkButton ID="btnExportarViajes" CssClass="btn botonesTC-descargar btn-block " runat="server"> <i class="fa fa-download"></i> </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row table-responsive">
                                            <div class="col-sm-12 col-xs-6">
                                                <asp:GridView ID="grdViajes" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="N° VIAJE / OS / GUIA">
                                                            <ItemTemplate>
                                                                <b>
                                                                    <asp:LinkButton ID="btnTipoViaje" runat="server" OnClick="btnTipoViajeClick" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton></b><br />
                                                                <b>
                                                                    <asp:LinkButton ID="btnNumViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerFichaViaje" Text='<%# Bind("num_viaje")%>' ToolTip="Ver Ficha Viaje"></asp:LinkButton></b>
                                                                <br />
                                                                <b>OS:&nbsp;</b>
                                                                <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label>
                                                                <br />
                                                                <b>GUIA:&nbsp;</b><asp:Label ID="lblNumGuia" Font-Bold="true" runat="server" Text='<%# Bind("num_guia")%>'></asp:Label>
                                                                <br />
                                                                <b>SCORE: </b>
                                                                <asp:Label ID="lblScore" Font-Bold="true" runat="server" Text='<%# Bind("score_viaje")%>'></asp:Label>
                                                                <br />
                                                                <asp:Image ID="imgIncidenciasActivas" Visible="false" ImageUrl="" runat="server" />

                                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                                <asp:HiddenField ID="hdnOS" runat="server" Value='<%# Bind("OS")%>' />
                                                                <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />
                                                                <asp:HiddenField ID="hdnCodServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                                <asp:HiddenField ID="hdnEstadoviaje" runat="server" Value='<%# Bind("id_est_viaje")%>' />
                                                                <asp:HiddenField ID="hdnCodcliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                                <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                                <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                                                <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                                <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("num_fono")%>' />
                                                                <asp:HiddenField ID="hdnNomTrasnportista" runat="server" Value='<%# Bind("nom_transportista")%>' />
                                                                <asp:HiddenField ID="hdnCodDestino" runat="server" Value='<%# Bind("cod_destino")%>' />
                                                                <asp:HiddenField ID="hdnCodOrigen" runat="server" Value='<%# Bind("cod_origen")%>' />
                                                                <asp:HiddenField ID="hdnGrupoOperativo" runat="server" Value='<%# Bind("grupo_operativo")%>' />
                                                                <asp:HiddenField ID="hdnNumeroViaje" runat="server" Value='<%# Bind("num_viaje")%>' />
                                                                <asp:HiddenField ID="hdnNomCliente" runat="server" Value='<%# Bind("nom_cliente")%>' />
                                                                <asp:HiddenField ID="hdnNomProducto" runat="server" Value='<%# Bind("nom_producto")%>' />
                                                                <asp:HiddenField ID="hdnNomServicio" runat="server" Value='<%# Bind("nom_servicio")%>' />
                                                                <asp:HiddenField ID="hdnNomGrupoOperativo" runat="server" Value='<%# Bind("nom_grupo_operativo")%>' />
                                                                <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />
                                                                <asp:HiddenField ID="hdnNumSecuencia" runat="server" Value='<%# Bind("num_secuencia")%>' />
                                                                <asp:HiddenField ID="hdnLatViaje" runat="server" Value='<%# Bind("num_latitud")%>' />
                                                                <asp:HiddenField ID="hdnLonViaje" runat="server" Value='<%# Bind("num_longitud")%>' />
                                                                <asp:HiddenField ID="hdnFecEstLlegadaDestino" runat="server" Value='<%# Bind("fec_destino_estimada")%>' />
                                                                <asp:HiddenField ID="hdnScoreConductor" runat="server" Value='<%# Bind("score_viaje")%>' />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="CAMIÓN">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnVerDocCamion" CssClass="text-green" OnClick="btnVerDocCamion_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />
                                                                <b>
                                                                    <asp:LinkButton ID="btnCamion" runat="server" Enabled="false" Text='<%# Bind("nom_pat_camion")%>'> </asp:LinkButton></b>                                                                                                                                <b>
                                                                        <br />
                                                                        <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                                <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />

                                                            </ItemTemplate>
                                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="ARRASTRE">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnVerDocArrastre" CssClass="text-green" OnClick="btnVerDocArrastre_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />

                                                                <b>
                                                                    <asp:LinkButton ID="btnArrastre" runat="server" Enabled="false" Text='<%# Bind("nom_pat_arrastre")%>'> </asp:LinkButton></b>
                                                                <br />
                                                                <b>
                                                                    <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                                <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />

                                                            </ItemTemplate>
                                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnVerDocConductor" CssClass="text-green" OnClick="btnVerDocConductor_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton><br />

                                                                <b>
                                                                    <asp:LinkButton ID="btnRutConductor" Enabled="false" runat="server" Text='<%# Bind("rut_conductor")%>'> </asp:LinkButton></b><br />

                                                                <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                                <asp:Label ID="lblSlash" runat="server" Text="/" />
                                                                <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' /><br />
                                                                <asp:Label ID="lblTrasnportista" runat="server" Text='<%# Bind("nom_transportista")%>' Font-Bold="true" />

                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="ORIGEN/FECHA">
                                                            <ItemTemplate>

                                                                <b>
                                                                    <asp:LinkButton ID="btnOrigen" runat="server" Enabled="false" Text='<%# Bind("nom_origen")%>'> </asp:LinkButton></b>
                                                                <br />
                                                                <b>
                                                                    <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                    <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label></b>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="DESTINO/FECHA">
                                                            <ItemTemplate>
                                                                <b>
                                                                    <asp:LinkButton ID="btnDestino" runat="server" Enabled="false" Text='<%# Bind("nom_destino")%>'> </asp:LinkButton></b><br />
                                                                <b>
                                                                    <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                                    <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>'></asp:Label></b>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="ESTADO">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_est_viaje")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="VALIDACION HR">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblValidacionHR" runat="server" Text='<%# Bind("estado_valido_hr")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="POD">
                                                            <ItemTemplate>
                                                                <h4>
                                                                    <asp:Label ID="lblPOD" runat="server" Text='<%# Bind("est_pod")%>'></asp:Label>
                                                                    <asp:LinkButton ID="btnCargarPOD" runat="server" Visible="true" OnClick="CargarPODClick" ToolTip="Cargar POD"><i class="fa fa-upload"></i></asp:LinkButton></h4>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Extra Costo">
                                                            <ItemTemplate>
                                                                <h4>
                                                                    <asp:LinkButton ID="btnExtraCostos" runat="server" OnClick="CargarExtraCostoClick" ToolTip="Ingresar Extra Costos"><i class="fa fa-money"></i></asp:LinkButton></h4>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="Baremos">
                                                            <ItemTemplate>
                                                                <h4>
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton ID="btnBaremos" OnClick="btnBaremos_Click" runat="server" ToolTip=""><i class="fa fa-truck"></i></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBaremos" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </h4>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <h4>
                                                                    <asp:LinkButton ID="btnValidarViaje" runat="server" OnClick="btnValidarClick" CssClass="btn btn-primary" ToolTip="Validar Viaje">Validar Viaje</asp:LinkButton></h4>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="center" />
                                                        </asp:TemplateField>
                                                    </Columns>

                                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>


            <div class="modal" id="mdlValidar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                <div class="modal-dialog  modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title ">
                                <asp:Label ID="lbltitValidar" runat="server" CssClass="text-green" Text=""></asp:Label></h4>
                        </div>

                        <div class="modal-body">


                            <div class="row btn-sm">

                                <div class="col-md-6">

                                    <div class="box box-success">
                                        <div class="box-header with-border bg-success">
                                            <h3 class="box-title">Información del Viaje</h3>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">TRANSPORTISTA:</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtTransportista" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">ORIGEN:</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>

                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">DESTINO</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtDestino" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">CAMION:</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtCamion" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">ARRASTRE:</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtArrastre" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">SERVICIO:</label>

                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtservicio" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="box box-success ">
                                        <div class="box-header with-border bg-success ">
                                            <h3 class="box-title ">POD</h3>
                                        </div>
                                        <div class="box-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">CONDUCTOR:</label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtConductorPOD" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="row btn-sm">
                                                <div class="col-md-12">

                                                    <asp:GridView ID="grdPOD" Visible="true" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="TIPO DOCUMENTO ">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTipoDocto" runat="server" Text='<%# Bind("nom_tipo_docto")%>' />
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                                    <asp:HiddenField ID="hdnIdTipodocto" runat="server" Value='<%# Bind("id_tipo_docto")%>' />
                                                                    <asp:HiddenField ID="hdnNomTipo" runat="server" Value='<%# Bind("url_pod")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="VALIDADO ">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblValidado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="N° DOCTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNumDocto" runat="server" Text='<%# Bind("num_docto")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <h4>
                                                                        <asp:LinkButton ID="btnDescargar" runat="server" OnClick="DescargarPODClick" ToolTip="Ver POD"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </h4>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <!-- Horizontal Form -->
                                    <div class="box box-success">
                                        <div class="box-header with-border bg-success">
                                            <h3 class="box-title">EXTRA COSTO</h3>
                                        </div>

                                        <div class="box-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">CONDUCTOR:</label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtConductorEX" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="row btn-sm">
                                                <div class="col-md-12">
                                                    <asp:GridView ID="grdExtraCosto" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="TIPO DOCUMENTO ">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTipoDocto" runat="server" Text='<%# Bind("nom_tipo_extra_costo")%>' />
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                                    <asp:HiddenField ID="hdnIdTipodocto" runat="server" Value='<%# Bind("id_tipo_extra_costo")%>' />
                                                                    <asp:HiddenField ID="hdnNomTipo" runat="server" Value='<%# Bind("url_docto")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="N° DOCTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNumDocto" runat="server" Text='<%# Bind("num_docto")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CANTIDAD">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNumCantidad" runat="server" Text='<%# Bind("num_cantidad")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <h4>
                                                                        <asp:LinkButton ID="btnDescargarEX" OnClick="DescargarEXClick" runat="server" ToolTip="Ver POD"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </h4>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                            </asp:TemplateField>




                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </div>


                                        </div>


                                    </div>


                                    <div class="box box-success">
                                        <div class="box-header with-border bg-success">
                                            <h3 class="box-title">BAREMOS</h3>
                                        </div>

                                        <div class="box-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">CONDUCTOR:</label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtConductorB" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="row btn-sm">
                                                <div class="col-md-12">
                                                    <asp:GridView ID="grdBaremoValidar" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="VIAJE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarViaje" runat="server" Text='<%# Bind("num_viaje")%>' />
                                                                    <asp:HiddenField runat="server" ID="hdnIDBaremos" Value='<%# Bind("id")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PUNTO DE PASO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarSecuencia" runat="server" Text='<%# Bind("paso")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="VALOR" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarValor" runat="server" Text='<%# Bind("valor")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TIPO DE CARGA">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarTipo" runat="server" Text='<%# Bind("tipo_carga")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>


                                    </div>



                                    <%-- <div class="box box-success">
                                            <div class="box-header with-border bg-success">
                                                <h3 class="box-title">Mapa</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="form-group">
                                                                                                      

                                                    <div id="map" class="table-responsive" style="width: 100%; height: 600px;">
                                                    </div>

                                                    <script>                                               
                                
                                                        var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                                                        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                                            maxZoom: 18,
                                                            attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                                                '<a href="https://www.mapbox.com/">Mapbox</a>',
                                                            id: 'mapbox/streets-v11',
                                                            tileSize: 512,
                                                            zoomOffset: -1
                                                        }).addTo(mymap);

                                                        L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>").openPopup();
                                                                                                                             

                                                    </script>
                                                </div>
                                            </div>


                                        </div>--%>
                                </div>

                            </div>

                        </div>


                        <div class="modal-footer">

                            <asp:Button ID="btnValidarViajes" CssClass="btn btn-primary " OnClientClick="$('#mdlValidar').modal('hide');" runat="server" Text="VALIDAR VIAJE" />
                            <button type="button" class="btn btn-default pull-left" onclick="$('#mdlBar').modal('hide');" data-dismiss="modal">CANCELAR</button>

                        </div>
                    </div>
                </div>
            </div>




            <div class="modal" id="mdlBar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <div class="modal-body">
                            <div class="modal-header bg-success">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title ">
                                    <asp:Label ID="lblTitBaremo" runat="server" CssClass="text-green" Text=""></asp:Label>
                                </h4>
                            </div>

                            <div class="modal-body">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="row justify-content-center">
                                            <div class="col-md-12 form-group justify-content-center">


                                                <div class="row">
                                                    <div class="col-md-2 form-group">
                                                        <label for="fname" class="control-label col-form-label">Valor</label>
                                                        <div>
                                                            <asp:TextBox ID="txtBValor" runat="server" CssClass="form-control" TextMode="Number"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 form-group">
                                                        <label for="fname" class="control-label col-form-label">Tipo de Carga</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboBCargas" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 form-group">
                                                        <label for="fname" class="control-label col-form-label">Punto de Paso</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboBPuntoPaso" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <asp:GridView ID="grdBar" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="VIAJE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarViaje" runat="server" Text='<%# Bind("num_viaje")%>' />
                                                                    <asp:HiddenField runat="server" ID="hdnIDBaremos" Value='<%# Bind("id")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PUNTO DE PASO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarSecuencia" runat="server" Text='<%# Bind("paso")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="VALOR" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarValor" runat="server" Text='<%# Bind("valor")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TIPO DE CARGA">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBarTipo" runat="server" Text='<%# Bind("tipo_carga")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <h4>
                                                                        <asp:LinkButton runat="server" ID="btnBBorrar" OnClick="btnBBorrar_Click"><i class="fa fa-trash"></i></asp:LinkButton>
                                                                    </h4>
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="modal-footer">

                            <asp:Button ID="btnGuardarBaremo" CssClass="btn btn-primary " OnClientClick="$('#mdlBar').modal('hide');" runat="server" Text="GUARDAR" />

                            <button type="button" class="btn btn-default pull-left" onclick="$('#mdlBar').modal('hide');" data-dismiss="modal">CANCELAR</button>

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">


                        <asp:Panel ID="pnlDoctos" runat="server" Visible="false">
                            <div class="modal-header bg-success">
                                <h3 class="modal-title text-light">
                                    <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label><asp:Label ID="lblIdentificador" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                <h3>
                                    <asp:Label ID="lblTrasnportista" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label></h3>
                                <button type="button" class="close" data-dismiss="modal">
                                    &times;</button>
                            </div>
                            <div class="modal-body">
                                <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="DOCUMENTO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("nom_docto") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VENCIMIENTO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("fec_vencimiento", "{0:d}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>


                        <asp:Panel ID="pnlDetalleViaje" runat="server" Visible="false">
                            <div class="modal-header bg-success">
                                <h3 class="modal-title text-light">
                                    <asp:Label ID="Label2" CssClass="text-green" Font-Bold="true" runat="server" Text="Numero Viaje: "></asp:Label><asp:Label ID="lblNumOS" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                <button type="button" class="close" data-dismiss="modal">
                                    &times;</button>
                            </div>
                            <div class="modal-body">
                                <asp:GridView ID="grdMultipaso" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="FECHA ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblhoraOrigen" runat="server" Text='<%# Bind("hor_origen_estimada")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="FECHA DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="HORA DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("hor_destino_estimada")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="PRODUCTO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>

                    </div>
                </div>
            </div>



            <asp:HiddenField ID="hdnIdViaje" runat="server" Value="0"></asp:HiddenField>

            <asp:HiddenField ID="hdnhoraservicio" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodTrasnportista" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodCliente" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdDemanda" runat="server" Value="0"></asp:HiddenField>

            <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

            <asp:HiddenField ID="hdnLonViaje" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLatViaje" runat="server" Value="-72.329437" />

            <asp:HiddenField ID="hdnLatAlerta" runat="server" Value="-37.448841" />
            <asp:HiddenField ID="hdnLonAlerta" runat="server" Value="-72.329437" />
            <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnRutconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnFecAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnID" runat="server" Value="0" />
            <asp:HiddenField ID="hdnTipoAlerta" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnTipoAlertaGrillaInformacion" runat="server" Value="0"></asp:HiddenField>

            <asp:HiddenField runat="server" ID="hdnViajeBaremos" Value="0" />
            <asp:HiddenField runat="server" ID="hdnSecuenciaBaremos" Value="0" />

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportarViajes" />
        </Triggers>
    </asp:UpdatePanel>



    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>




</asp:Content>
