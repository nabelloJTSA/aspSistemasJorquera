﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb


Public Class aspIngDemandaC
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                Call Inicializarcontroles()

                lbltitulo.Text = "INGRESO DEMANDA"
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnIngreso_Click(sender As Object, e As EventArgs) Handles btnIngreso.Click
        btnIngreso.CssClass = btnIngreso.CssClass.Replace("btn-outline-info", "btn-info active")
        btnExcel.CssClass = btnExcel.CssClass.Replace("btn-info active", "btn-outline-info")
        lbltitulo.Text = "INGRESO DEMANDA"
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        btnExcel.CssClass = btnExcel.CssClass.Replace("btn-outline-info", "btn-info active")
        btnIngreso.CssClass = btnIngreso.CssClass.Replace("btn-info active", "btn-outline-info")
        lbltitulo.Text = "CARGAR DEMANDA"
        MultiView1.ActiveViewIndex = 1
    End Sub


    Private Sub Inicializarcontroles()
        txtFechaServicio.Text = Session("fec_actual")
        txtHoraservicio.Text = "12:00"

        Call CargarLugares()
        Call CargarServicio()
        Call CargarProductos()

        Session("dtDetLugares") = CrearTablaDetLugares()
    End Sub


    Private Sub CargarLugares()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_demanda 'COD', '" & cboTipoLugar.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            cboLugar.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboLugar.DataTextField = "nom_lugar"
            cboLugar.DataValueField = "cod_lugar"
            cboLugar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarLugares", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarServicio()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_demanda 'CCS', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicio"
            cboServicio.DataValueField = "id_servicio"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarProductos()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_demanda 'CCP', '" & Session("cod_usu_tc") & "'"
            cboProducto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboProducto.DataTextField = "nom_producto"
            cboProducto.DataValueField = "cod_producto"
            cboProducto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarProductos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnAddOrigen_Click(sender As Object, e As EventArgs) Handles btnAddOrigen.Click
        Call GuardarDetTempOrigen()

        If grdLugares.Rows.Count > 1 Then
            btnConfirmar.Visible = True
            btnConfirmModal.Visible = True
        Else
            btnConfirmar.Visible = False
            btnConfirmModal.Visible = False
        End If
    End Sub

    Private Function CrearTablaDetLugares() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("fec_servicio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("hor_servicio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("rut_cliente", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_tipo", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_tipo", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_servicio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_servicio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("num_orden_servicio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_producto", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_producto", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_lugar", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_lugar", Type.GetType("System.String"))
            dtTemporal.Columns.Add("FH_lugar", Type.GetType("System.String"))

        Catch ex As Exception
            MessageBoxError("CrearTablaDetOrigenes", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Sub GuardarDetTempOrigen()
        Dim ocar As New DataTable
        ocar = Session("dtDetLugares")
        With ocar.Rows.Add
            .Item("fec_servicio") = txtFechaServicio.Text
            .Item("hor_servicio") = txtHoraservicio.Text
            .Item("rut_cliente") = Session("rut_usu_tc")
            .Item("cod_lugar") = cboLugar.SelectedValue
            .Item("nom_lugar") = cboLugar.SelectedItem.ToString
            .Item("cod_tipo") = cboTipoLugar.SelectedValue
            .Item("id_servicio") = cboServicio.SelectedValue
            .Item("nom_servicio") = cboServicio.SelectedItem.ToString
            .Item("nom_tipo") = cboTipoLugar.SelectedItem.ToString
            .Item("num_orden_servicio") = Trim(txtOS.Text)
            .Item("cod_producto") = cboProducto.SelectedValue
            .Item("nom_producto") = cboProducto.SelectedItem.ToString
            .Item("FH_lugar") = txtFechaServicio.Text & " " & txtHoraservicio.Text
        End With
        ocar.DefaultView.Sort = "num_orden_servicio, fec_servicio, hor_servicio, cod_tipo"
        grdLugares.DataSource = ocar
        grdLugares.DataBind()
    End Sub


    Private Sub grdLugares_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLugares.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdLugares.Rows(intRow)

                Dim ocar As New DataTable
                ocar = Session("dtDetLugares")
                ocar.Rows(intRow).Delete()

                grdLugares.DataSource = ocar
                grdLugares.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdDestinos_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarUnitario()
        txtOS.Text = ""
        btnConfirmar.Visible = False
        hdnExisteOrigen.Value = 0
        hdnExisteDestino.Value = 0
        cboTipoLugar.ClearSelection()
        btnConfirmModal.Visible = False

        Call CargarLugares()
        Call CargarServicio()
        Call CargarProductos()

        Dim ocar As New DataTable
        ocar = Session("dtDetLugares")
        ocar.Rows.Clear()
        grdLugares.DataSource = ocar
        grdLugares.DataBind()
    End Sub


    Protected Sub InsertarDemanda()
        Try
            For iRow = 0 To grdLugares.Rows.Count - 1
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_demanda"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "ITP"
                comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
                comando.Parameters("@rut_cliente").Value = CType(grdLugares.Rows(iRow).FindControl("hdnRutCliente"), HiddenField).Value
                comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
                comando.Parameters("@id_servicio").Value = CType(grdLugares.Rows(iRow).FindControl("hdnIdServicio"), HiddenField).Value
                comando.Parameters.Add("@num_orden_servicio", SqlDbType.NVarChar)
                comando.Parameters("@num_orden_servicio").Value = CType(grdLugares.Rows(iRow).FindControl("hdnNumOS"), HiddenField).Value
                comando.Parameters.Add("@tipo_viaje", SqlDbType.NVarChar)
                comando.Parameters("@tipo_viaje").Value = CType(grdLugares.Rows(iRow).FindControl("hdnCodTipo"), HiddenField).Value
                comando.Parameters.Add("@cod_lugar", SqlDbType.NVarChar)
                comando.Parameters("@cod_lugar").Value = CType(grdLugares.Rows(iRow).FindControl("hdnCodLugar"), HiddenField).Value
                comando.Parameters.Add("@fec_servicio", SqlDbType.DateTime)
                comando.Parameters("@fec_servicio").Value = CType(grdLugares.Rows(iRow).FindControl("hdnFecServicio"), HiddenField).Value
                comando.Parameters.Add("@hor_servicio", SqlDbType.Time)
                comando.Parameters("@hor_servicio").Value = CType(grdLugares.Rows(iRow).FindControl("hdnHoraServicio"), HiddenField).Value
                comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
                comando.Parameters("@cod_producto").Value = CType(grdLugares.Rows(iRow).FindControl("hdnCodProducto"), HiddenField).Value

                comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
                comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Next
        Catch ex As Exception
            MessageBoxError("InsertarOrigen", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CrearDemanda()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IDE"
            comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
            comando.Parameters("@rut_cliente").Value = Session("rut_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CrearDemanda", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click

        For iRow = 0 To grdLugares.Rows.Count - 1
            If CType(grdLugares.Rows(iRow).FindControl("hdnCodTipo"), HiddenField).Value = "O" Then
                hdnExisteOrigen.Value = 1
            ElseIf CType(grdLugares.Rows(iRow).FindControl("hdnCodTipo"), HiddenField).Value = "D" Then
                hdnExisteDestino.Value = 1
            End If
        Next

        If hdnExisteOrigen.Value = 1 And hdnExisteDestino.Value = 1 Then
            Call InsertarDemanda()
            Call CrearDemanda()
            Call LimpiarUnitario()
            MessageBox("Confirmar", "Solicitud Enviada", Page, Master, "S")
        Else
            MessageBox("Confirmar", "Ingrese Origen y Destino", Page, Master, "W")
            Exit Sub
        End If



    End Sub




    ' *****************************  CARGA A TRAVES DE PLANILLA ********************************************


    Protected Sub btnCargarDocto_Click(sender As Object, e As EventArgs) Handles btnCargarDocto.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdItems)
            btnConfirmarCarga.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub


    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\DemandasExcel\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("OS", GetType(String)),
                                    New DataColumn("RUT_CLIENTE", GetType(String)),
                                    New DataColumn("SERVICIO", GetType(String)),
                                    New DataColumn("SECUENCIA", GetType(String)),
                                    New DataColumn("ORIGEN", GetType(String)),
                                    New DataColumn("FECHA_ORIGEN", GetType(String)),
                                    New DataColumn("HORA_ORIGEN", GetType(String)),
                                     New DataColumn("DESTINO", GetType(String)),
                                    New DataColumn("FECHA_DESTINO", GetType(String)),
                                    New DataColumn("HORA_DESTINO", GetType(String)),
                                    New DataColumn("PRODUCTO", GetType(String))
                                         })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT OS, RUT, SERVICIO, SECUENCIA, ORIGEN, FECHA_ORIGEN, HORA_ORIGEN, DESTINO, FECHA_DESTINO, HORA_DESTINO, PRODUCTO FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFilaN.Item(2) = dtTemporal(i).Item(2)
                dtFilaN.Item(3) = dtTemporal(i).Item(3)
                dtFilaN.Item(4) = dtTemporal(i).Item(4)
                dtFilaN.Item(5) = dtTemporal(i).Item(5)
                dtFilaN.Item(6) = dtTemporal(i).Item(6)
                dtFilaN.Item(7) = dtTemporal(i).Item(7)
                dtFilaN.Item(8) = dtTemporal(i).Item(8)
                dtFilaN.Item(9) = dtTemporal(i).Item(9)
                dtFilaN.Item(10) = dtTemporal(i).Item(10)
                dtFinal.Rows.Add(dtFilaN)
            Next

            btnConfirmarCarga.Visible = True

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargaExcelDemandas\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdItems.Rows.Count - 1
                Dim objOS As Label = CType(grdItems.Rows(intRow).FindControl("lblOS"), Label)
                Dim objRut As Label = CType(grdItems.Rows(intRow).FindControl("lblRut"), Label)
                Dim objServicio As Label = CType(grdItems.Rows(intRow).FindControl("lblServicio"), Label)
                Dim objSecuencia As Label = CType(grdItems.Rows(intRow).FindControl("lblSecuencia"), Label)
                Dim objOrigen As Label = CType(grdItems.Rows(intRow).FindControl("lblOrigen"), Label)
                Dim objFecOrigen As Label = CType(grdItems.Rows(intRow).FindControl("lblFecOrigen"), Label)
                Dim objHoraOrigen As Label = CType(grdItems.Rows(intRow).FindControl("lblHoraOrigen"), Label)
                Dim objDestino As Label = CType(grdItems.Rows(intRow).FindControl("lblDestino"), Label)
                Dim objFecDestino As Label = CType(grdItems.Rows(intRow).FindControl("lblFecDestino"), Label)
                Dim objHoraDestino As Label = CType(grdItems.Rows(intRow).FindControl("lblHoraDestino"), Label)
                Dim objProducto As Label = CType(grdItems.Rows(intRow).FindControl("lblProducto"), Label)

                Call InsertarCargaDemanda(objOS.Text, objRut.Text, objServicio.Text, objSecuencia.Text, objOrigen.Text, objFecOrigen.Text, objHoraOrigen.Text, objDestino.Text, objFecDestino.Text, objHoraDestino.Text, objProducto.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function


    Protected Sub InsertarCargaDemanda(ByVal OS As String, ByVal rut As String, ByVal servicio As String, ByVal secuencia As String, ByVal des_origen As String, ByVal fec_servicio As String, ByVal hor_servicio As String, ByVal des_destino As String, ByVal fec_destino As String, ByVal hor_destino As String, ByVal des_producto As String)
        Try
            For iRow = 0 To grdItems.Rows.Count - 1
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_demanda"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "ICD"
                comando.Parameters.Add("@num_orden_servicio", SqlDbType.NVarChar)
                comando.Parameters("@num_orden_servicio").Value = Trim(OS)
                comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
                comando.Parameters("@rut_cliente").Value = Trim(rut)
                comando.Parameters.Add("@des_servicio", SqlDbType.NVarChar)
                comando.Parameters("@des_servicio").Value = Trim(servicio)
                comando.Parameters.Add("@secuencia", SqlDbType.NVarChar)
                comando.Parameters("@secuencia").Value = Trim(secuencia)
                comando.Parameters.Add("@des_origen", SqlDbType.NVarChar)
                comando.Parameters("@des_origen").Value = Trim(des_origen)
                comando.Parameters.Add("@fec_servicio", SqlDbType.NVarChar)
                comando.Parameters("@fec_servicio").Value = Trim(fec_servicio)
                comando.Parameters.Add("@hor_servicio", SqlDbType.NVarChar)
                comando.Parameters("@hor_servicio").Value = Trim(hor_servicio)
                comando.Parameters.Add("@des_destino", SqlDbType.NVarChar)
                comando.Parameters("@des_destino").Value = Trim(des_destino)
                comando.Parameters.Add("@fec_servicio_destino", SqlDbType.NVarChar)
                comando.Parameters("@fec_servicio_destino").Value = Trim(fec_destino)
                comando.Parameters.Add("@hor_servicio_destino", SqlDbType.NVarChar)
                comando.Parameters("@hor_servicio_destino").Value = Trim(hor_destino)
                comando.Parameters.Add("@des_producto", SqlDbType.NVarChar)
                comando.Parameters("@des_producto").Value = Trim(des_producto)
                comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
                comando.Parameters("@usr_add").Value = Session("nom_usu_tc")

                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Next

            'Call GenerarCSV()
            Call LimpiarUnitario()
            MessageBox("Confirmar", "Solicitud Enviada", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("InsertarDemanda", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnConfirmarCarga_Click(sender As Object, e As EventArgs) Handles btnConfirmarCarga.Click
        Dim _directorioParaGuardar As String = "C:\CargaExcelDemandas\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdItems.DataSource = MostrarExcel(_directorioParaGuardar)
            grdItems.DataBind()

            btnConfirmar.Visible = False
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub

    Protected Sub cboTipoLugar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoLugar.SelectedIndexChanged
        Call CargarLugares()
    End Sub
End Class