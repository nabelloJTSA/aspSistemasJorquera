﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspIngDemandaC.aspx.vb" Inherits="aspSistemasJorquera.aspIngDemandaC" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentClientes" runat="server">

    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnIngreso" runat="server">Ingresar Demanda</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnExcel" runat="server">Cargar Demanda</asp:LinkButton></li>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
            <small>Cliente</small>
        </h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/aspHome.aspx">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ingreso Demanda</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-default">
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">

                        <asp:MultiView ID="MultiView1" runat="server">
                            <asp:View ID="vwIngreso" runat="server">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel">
                                    <ContentTemplate>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="row btn-sm">

                                                    <div class="col-md-2 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Servicio</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Producto</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboProducto" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-1 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            OS</label>
                                                        <div class="">
                                                            <asp:TextBox ID="txtOS" runat="server" TextMode="Number" placeholder="N°" CssClass="form-control" required></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-2 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Tipo</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboTipoLugar" CssClass="form-control" runat="server" AutoPostBack="true">
                                                                <asp:ListItem Value="O">Origen (Carga)</asp:ListItem>
                                                                <asp:ListItem Value="D">Destino (Descarga)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-2 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Ubicación</label>
                                                        <div>
                                                            <asp:DropDownList ID="cboLugar" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-2 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Fecha Servicio</label>
                                                        <div>
                                                            <asp:TextBox ID="txtFechaServicio" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control" required></asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-1 form-group">
                                                        <label for="fname"
                                                            class="control-label col-form-label">
                                                            Hora</label>
                                                        <div>
                                                            <asp:TextBox ID="txtHoraservicio" runat="server" TextMode="Time" CssClass="form-control" required></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-9 form-group">
                                                    </div>
                                                    <div class="col-md-3 form-group">
                                                        <asp:Button ID="btnAddOrigen" CssClass="btn btn-primary btn-block btn-sm" runat="server" Text="Agregar Registro" />
                                                    </div>
                                                </div>


                                                <div class="row justify-content-center bg-warning">
                                                    <div class="col-md-12">
                                                        <asp:GridView ID="grdLugares" runat="server" CssClass="table table-bordered table-striped table-responsive with-border small" AutoGenerateColumns="false">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="Orden Servicio">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOS" runat="server" Text='<%# Bind("num_orden_servicio")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Tipo">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("nom_tipo")%>'></asp:Label>

                                                                        <asp:HiddenField ID="hdnCodLugar" runat="server" Value='<%# Bind("cod_lugar")%>' />
                                                                        <asp:HiddenField ID="hdnFecServicio" runat="server" Value='<%# Bind("fec_servicio")%>' />
                                                                        <asp:HiddenField ID="hdnHoraServicio" runat="server" Value='<%# Bind("hor_servicio")%>' />
                                                                        <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />
                                                                        <asp:HiddenField ID="hdnIdServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                                        <asp:HiddenField ID="hdnCodTipo" runat="server" Value='<%# Bind("cod_tipo")%>' />
                                                                        <asp:HiddenField ID="hdnRutCliente" runat="server" Value='<%# Bind("rut_cliente")%>' />
                                                                        <asp:HiddenField ID="hdnNumOS" runat="server" Value='<%# Bind("num_orden_servicio")%>' />

                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>



                                                                <asp:TemplateField HeaderText="Lugar">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblLugar" runat="server" Text='<%# Bind("nom_lugar")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA/HORA SERVICIO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecServicio" runat="server" Text='<%# Bind("FH_lugar")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Servicio">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("nom_servicio")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Producto">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblProducto" runat="server" Text='<%# Bind("nom_producto")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnEliminarDet" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="1" CssClass="btn btn-danger btn-sm"><i class="fa fa-trash"></i></asp:LinkButton>

                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                </asp:TemplateField>

                                                            </Columns>

                                                            <HeaderStyle HorizontalAlign="center" BackColor="#FFB738" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />

                                        <div class="border-top">
                                            <div class="card-body fa-pull-right">
                                                <button type="button" id="btnConfirmModal" runat="server" class="btn btn-warning btn-block btn-sm" visible="false" data-toggle="modal" data-target="#modalClientes">
                                                    Confirmar Demanda
                                                </button>
                                            </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal" id="modalClientes" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                                            <div class="modal-dialog  modal-md">
                                                <div class="modal-content">

                                                    <div class="modal-body">
                                                        <asp:UpdatePanel runat="server" ID="updClientes">
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="btnConfirmar" EventName="Click" />
                                                            </Triggers>
                                                            <ContentTemplate>


                                                                <div class="modal-header bg-orange">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title ">Confirmar Demanda</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>¿ESTA SEGURO QUE DESEA CONFIRMAR ESTA DEMANDA? &hellip;</p>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>

                                                    <div class="modal-footer">

                                                        <asp:Button ID="btnConfirmar" CssClass="btn btn-warning " OnClientClick="$('#modalClientes').modal('hide');" runat="server" Visible="false" Text="REGISTRAR DEMANDA" />
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <asp:HiddenField ID="hdnExisteOrigen" runat="server" Value="0" />
                                        <asp:HiddenField ID="hdnExisteDestino" runat="server" Value="0" />

                                        <%--fin modal--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:View>



                            <asp:View ID="vwExcel" runat="server">
                                <div class="p-20">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="row">
                                                <div class="col-md-4 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        Descarga Planilla</label>
                                                    <div>
                                                        <button type="button" class="btn btn-success">Descargar</button>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        Cargar Documento</label>
                                                    <div>
                                                        <asp:FileUpload ID="fuplCargar" CssClass="form-control bg-transparent" runat="server" />
                                                    </div>


                                                </div>

                                                <div class="col-md-1 form-group">
                                                    <label for="fname"
                                                        class="">
                                                        </label>
                                                    <div>

                                                        <asp:LinkButton ID="btnCargarDocto" CssClass="btn btn-primary btn-block " runat="server">Cargar</asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12 bg-warning">

                                        <asp:GridView ID="grdItems" runat="server" CssClass="table table-bordered table-striped table-responsive with-border" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="OS">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOS" runat="server" Text='<%# Bind("OS")%>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="RUT">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRut" runat="server" Text='<%# Bind("RUT_CLIENTE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SERVICIO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("SERVICIO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SECUENCIA">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSecuencia" runat="server" Text='<%# Bind("SECUENCIA")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ORIGEN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("ORIGEN")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FECHA ORIGEN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("FECHA_ORIGEN")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HORA ORIGEN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHoraOrigen" runat="server" Text='<%# Bind("HORA_ORIGEN")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="DESTINO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("DESTINO")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FECHA DESTINO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("FECHA_DESTINO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="HORA DESTINO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHoraDestino" runat="server" Text='<%# Bind("HORA_DESTINO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProducto" runat="server" Text='<%# Bind("PRODUCTO")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                </asp:TemplateField>

                                               

                                            </Columns>

                                            <HeaderStyle HorizontalAlign="center" BackColor="#FFB738" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="card-body fa-pull-right">
                                    <asp:Button ID="btnConfirmarCarga" CssClass="btn bg-orange" runat="server" Visible="false" Text="Confirmar Demanda" />
                                </div>
                            </asp:View>




                        </asp:MultiView>

                    </div>
                </div>

            </div>

        </div>

    </section>
    <!-- /.content -->


</asp:Content>

