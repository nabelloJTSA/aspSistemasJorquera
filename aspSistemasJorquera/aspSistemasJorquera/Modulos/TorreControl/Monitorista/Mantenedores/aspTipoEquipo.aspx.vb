﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspTipoEquipo
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                Call CargarGrillaTipoEquipo()

                Call CargarTipoEquipos()
                Call CargarProductos()
                Call CargarGrillaProductos()
                Call CargarArrastre()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnControlVW_Click(sender As Object, e As EventArgs) Handles btn1.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "Tipos De Equipo"

        btn1.CssClass = btn1.CssClass.Replace("bg-warning", "bg-orange")
        btn2.CssClass = btn2.CssClass.Replace("bg-orange", "bg-warning")

    End Sub

    Protected Sub btnAlertasVW_Click(sender As Object, e As EventArgs) Handles btn2.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "Tipo de Equipo / Arrastre"

        btn1.CssClass = btn1.CssClass.Replace("bg-orange", "bg-warning")
        btn2.CssClass = btn2.CssClass.Replace("bg-warning", "bg-orange")
    End Sub


    Private Sub CargarGrillaTipoEquipo()
        Try
            Dim strNomTabla As String = "TCMae_tipo_equipo"
            Dim strSQL As String = "Exec mant_tipo_equipo 'GTE'"
            grdTipoEquipo.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTipoEquipo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTipoEquipo", Err, Page, Master)
        End Try
    End Sub


    Protected Sub InsertarTipoEquipo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ITE"
            comando.Parameters.Add("@nom_tipo_equipo", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo_equipo").Value = Trim(txtNomTipo.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarTipoEquipo", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ActualizarTipoEquipo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UTE"
            comando.Parameters.Add("@nom_tipo_equipo", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo_equipo").Value = Trim(txtNomTipo.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdTipoEquipo.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarTipoEquipo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarTipoEquipo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ETE"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdTipoEquipo.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarTipoEquipo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EditarTipoClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdTipoEquipo.Rows(rowIndex)

        txtNomTipo.Text = Trim(CType(row.FindControl("lblNomTipo"), Label).Text)
        hdnActivoTipoEquipo.Value = 1
        hdnIdTipoEquipo.Value = CType(row.FindControl("hdnID"), HiddenField).Value

    End Sub


    Protected Sub EliminarTipoClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdTipoEquipo.Rows(rowIndex)

        lblTitModal.Text = "Tipo Equipo: " & Trim(CType(row.FindControl("lblNomTipo"), Label).Text)
        hdnIdTipoEquipo.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        btnConfirmar.Visible = True
        btnConfirmarProductos.Visible = False
        btnConfirmarArrastre.Visible = False

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If Trim(txtNomTipo.Text) = "" Then
            MessageBox("Tipo de Equipo", "Ingrese nombre valido", Page, Master, "W")
        Else
            If hdnActivoTipoEquipo.Value = 1 Then
                Call ActualizarTipoEquipo()
                MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
            Else
                Call InsertarTipoEquipo()
                MessageBox("Insertar", "Información Almacenada", Page, Master, "S")
            End If
            Call Limpiartipo()

        End If
    End Sub

    Private Sub Limpiartipo()
        txtNomTipo.Text = ""
        hdnActivoTipoEquipo.Value = 0
        Call CargarGrillaTipoEquipo()
        Call CargarTipoEquipos()
        Call CargarGrillaProductos()
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Call EliminarTipoEquipo()
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
        Call Limpiartipo()
    End Sub


    Private Sub CargarTipoEquipos()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec mant_tipo_equipo 'CTE'"
            cbotipoEquipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cbotipoEquipo.DataTextField = "nom_tipo_equipo"
            cbotipoEquipo.DataValueField = "id_tipo_equipo"
            cbotipoEquipo.DataBind()

            cboTipoEquipoA.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoEquipoA.DataTextField = "nom_tipo_equipo"
            cboTipoEquipoA.DataValueField = "id_tipo_equipo"
            cboTipoEquipoA.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarTipoEquipos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarProductos()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec mant_tipo_equipo 'CPR'"
            cboProductos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboProductos.DataTextField = "nom_producto"
            cboProductos.DataValueField = "cod_producto_msoft"
            cboProductos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarProductos", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaProductos()
        Try
            Dim strNomTabla As String = "TCMae_tipo_equipo"
            Dim strSQL As String = "Exec mant_tipo_equipo 'GTP', '" & cbotipoEquipo.SelectedValue & "'"
            grdRelacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdRelacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaProductos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub InsertarRelacion()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ITP"
            comando.Parameters.Add("@id_tipo_equipo", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_equipo").Value = cbotipoEquipo.SelectedValue
            comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
            comando.Parameters("@cod_producto").Value = cboProductos.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            MessageBox("Productos", "Información Relacionada", Page, Master, "S")
            Call CargarGrillaProductos()

        Catch ex As Exception
            MessageBoxError("InsertarRelacion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarRelacion()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ETP"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdRelacion.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarRelacion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarProductoClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRelacion.Rows(rowIndex)

        lblTitModal.Text = "Producto: " & Trim(CType(row.FindControl("lblNomProducto"), Label).Text)
        hdnIdRelacion.Value = CType(row.FindControl("hdnIDRelacion"), HiddenField).Value
        btnConfirmar.Visible = False
        btnConfirmarProductos.Visible = True
        btnConfirmarArrastre.Visible = False

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub brnRelacionar_Click(sender As Object, e As EventArgs) Handles brnRelacionar.Click
        Call InsertarRelacion()
    End Sub

    Protected Sub btnConfirmarProductos_Click(sender As Object, e As EventArgs) Handles btnConfirmarProductos.Click
        Call EliminarRelacion()
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
        Call CargarGrillaProductos()
    End Sub

    Protected Sub cbotipoEquipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbotipoEquipo.SelectedIndexChanged
        Call CargarGrillaProductos()
    End Sub




    Private Sub CargarArrastre()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec mant_tipo_equipo 'CAR'"
            cboArrastre.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboArrastre.DataTextField = "nom_patente"
            cboArrastre.DataValueField = "nom_patente"
            cboArrastre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarArrastre", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ActualizarTipoEquipoArrastre(ByVal tipo_equipo As String, ByVal patente As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_tipo_equipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "TAR"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = tipo_equipo
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = patente
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarTipoEquipoArrastre", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarA_Click(sender As Object, e As EventArgs) Handles btnGuardarA.Click
        Call ActualizarTipoEquipoArrastre(cboTipoEquipoA.SelectedValue, cboArrastre.SelectedValue)
        MessageBox("Relacionar Arrastre", "Registro Relacionado", Page, Master, "S")
        Call CargarGrillaArrastres()
        Call CargarArrastre()
    End Sub


    Protected Sub EliminarArrastreClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdArrastre.Rows(rowIndex)

        lblTitModal.Text = "Arrastre: " & Trim(CType(row.FindControl("lblArrastre"), Label).Text)

        btnConfirmar.Visible = False
        btnConfirmarProductos.Visible = False
        btnConfirmarArrastre.Visible = True

        hdnPatente.Value = Trim(CType(row.FindControl("lblArrastre"), Label).Text)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Private Sub CargarGrillaArrastres()
        Try
            Dim strNomTabla As String = "TCMae_tipo_equipo"
            Dim strSQL As String = "Exec mant_tipo_equipo 'GAR', '" & cboTipoEquipoA.SelectedValue & "'"
            grdArrastre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdArrastre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaProductos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboTipoEquipoA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoEquipoA.SelectedIndexChanged
        Call CargarGrillaArrastres()
    End Sub

    Protected Sub btnConfirmarArrastre_Click(sender As Object, e As EventArgs) Handles btnConfirmarArrastre.Click
        Call ActualizarTipoEquipoArrastre(0, hdnPatente.Value)
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
        Call CargarArrastre()
        Call CargarGrillaArrastres()
    End Sub




End Class