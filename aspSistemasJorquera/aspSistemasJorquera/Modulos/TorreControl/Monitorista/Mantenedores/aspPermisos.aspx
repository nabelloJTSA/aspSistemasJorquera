﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPermisos.aspx.vb" Inherits="aspSistemasJorquera.aspPermisos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>


            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">                                
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <!-- ./box-body -->
                                            <div class="box-footer">
                                                <div class="row table-responsive">
                                                    <div class="col-sm-12 col-xs-6">
                                                        <div class="description-block border-right">
                                                            <asp:GridView ID="grdUsuarios" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="RUT USUARIO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("rut_usuario")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hdnCodUsuario" runat="server" Value='<%# Bind("cod_usuario")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>    

                                                                    <asp:TemplateField HeaderText="NOMBRE USUARIO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCodigo" runat="server" Text='<%# Bind("nom_usuario")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                    <asp:TemplateField HeaderText="CORREO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltransportista" runat="server" Text='<%# Bind("mail_usuario")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="btnPermiso" runat="server" OnClick="btnPermiso_Click"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnPermiso" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                </Columns>
                                                                <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                                            </asp:GridView>
                                                            <asp:UpdatePanel runat="server">
                                                                <ContentTemplate>
                                                                    <asp:HiddenField ID="hdnCodigo" runat="server" Value="0" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <!-- MODAL CONTACTOS -->
                                                            <div class="modal fade" id="modalPermisos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog" style="width:20%">
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" onclick="$('#modalPermisos').modal('hide');" aria-hidden="true">&times;</button>
                                                                                    <h4 class="modal-title"><asp:Label ID="Label2" runat="server" Text="Asignar Permisos de Usuario"></asp:Label></h4>
                                                                                </div>
                                                                                <div class="modal-body"> 
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <div class="form-check" style="text-align:left;margin-left:25%">
                                                                                                <asp:CheckBoxList ID="checksPermisos" runat="server"></asp:CheckBoxList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <asp:Button ID="btnGuardar" OnClick="btnGuardar_Click" runat="server" CssClass="btn bg-orange" Text="Guardar" />
                                                                                </div>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>

            </section>
            <!-- /.content -->
</asp:Content>
