﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspCatRol
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                CargarTablaCategorias()
                CargarTablaRoles()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTablaCategorias()
        Try
            Dim strNomTabla As String = "TCMae_categoria"
            Dim strSQL As String = "Exec mant_categoria 'G'"
            grdDatos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDatos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTablaRoles()
        Try
            Dim strNomTabla As String = "TCMae_roles"
            Dim strSQL As String = "Exec mant_roles 'G'"
            grdDatosRol.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDatosRol.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Insertar_Categoria()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categoria"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_categoria", SqlDbType.NVarChar)
            comando.Parameters("@nom_categoria").Value = Trim(txtNombreCat.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Insertar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Insertar_Rol()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_roles"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_rol", SqlDbType.NVarChar)
            comando.Parameters("@nom_rol").Value = Trim(txtNomRol.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Insertar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Actualizar_Categoria()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categoria"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_categoria", SqlDbType.NVarChar)
            comando.Parameters("@nom_categoria").Value = Trim(txtNombreCat.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnID.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Actualizar_Rol()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_roles"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_rol", SqlDbType.NVarChar)
            comando.Parameters("@nom_rol").Value = Trim(txtNomRol.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIDRol.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Eliminar_Categoria(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categoria"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Eliminar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Eliminar_Rol(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_roles"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Eliminar", Err, Page, Master)
        End Try
    End Sub
    Private Sub Limpiar()
        txtNombreCat.Text = ""
        hdnID.Value = 0
        hdnActivo.Value = 0
        Call CargarTablaCategorias()
    End Sub
    Private Sub LimpiarRol()
        txtNomRol.Text = ""
        hdnIDRol.Value = 0
        hdnActivoRol.Value = 0
        Call CargarTablaRoles()
    End Sub
    Protected Sub btnEliminarCategoria_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)

        Call Eliminar_Categoria(CType(row.FindControl("hdnID"), HiddenField).Value)
        Call Limpiar()
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
    End Sub
    Protected Sub btnEliminarRol_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatosRol.Rows(rowIndex)

        Call Eliminar_Rol(CType(row.FindControl("hdnIDRol"), HiddenField).Value)
        Call LimpiarRol()
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
    End Sub
    Protected Sub btnSeleccionarCategoria_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)
        txtNombreCat.Text = CType(row.FindControl("lblNombre"), Label).Text
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        hdnActivo.Value = 1
    End Sub
    Protected Sub btnSeleccionarRol_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatosRol.Rows(rowIndex)
        txtNomRol.Text = CType(row.FindControl("lblNombreRol"), Label).Text
        hdnIDRol.Value = CType(row.FindControl("hdnIDRol"), HiddenField).Value
        hdnActivoRol.Value = 1
    End Sub
    Protected Sub btnAddRol_Click(sender As Object, e As EventArgs)
        If hdnActivoRol.Value = 1 Then
            Call Actualizar_Rol()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
        Else
            Call Insertar_Rol()
            MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
        End If

        Call LimpiarRol()
    End Sub
    Protected Sub btnAddCategoria_Click(sender As Object, e As EventArgs)
        If hdnActivo.Value = 1 Then
            Call Actualizar_Categoria()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
        Else
            Call Insertar_Categoria()
            MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
        End If

        Call Limpiar()
    End Sub
End Class