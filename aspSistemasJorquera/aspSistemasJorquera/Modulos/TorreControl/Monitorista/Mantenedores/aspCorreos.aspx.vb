﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspCorreos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then

                Call CargarProcesos()

                Call CargarGrilla()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarProcesos()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec mant_correos 'CPR'"
            cboProceso.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboProceso.DataTextField = "nom_proceso"
            cboProceso.DataValueField = "id"
            cboProceso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarProcesos", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "TCMae_tipo_equipo"
            Dim strSQL As String = "Exec mant_correos 'GRI'"
            grdCorreos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCorreos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub


    Protected Sub InsertarCorreo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_correos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ICO"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@nom_personal", SqlDbType.NVarChar)
            comando.Parameters("@nom_personal").Value = Trim(txtNombreMov.Text)
            comando.Parameters.Add("@correo", SqlDbType.NVarChar)
            comando.Parameters("@correo").Value = Trim(txtCorreo.Text)
            comando.Parameters.Add("@id_tipo_proceso", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_proceso").Value = cboProceso.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarCorreo", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ActualizarCorreo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_correos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UPC"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@nom_personal", SqlDbType.NVarChar)
            comando.Parameters("@nom_personal").Value = Trim(txtNombreMov.Text)
            comando.Parameters.Add("@correo", SqlDbType.NVarChar)
            comando.Parameters("@correo").Value = Trim(txtCorreo.Text)
            comando.Parameters.Add("@id_tipo_proceso", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_proceso").Value = cboProceso.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarCorreo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarCorreo()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_correos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ELC"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarCorreo", Err, Page, Master)
        End Try
    End Sub


    Protected Sub EditarClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdCorreos.Rows(rowIndex)

        txtrut.Text = Trim(CType(row.FindControl("hdnRutPersonal"), HiddenField).Value)
        txtNombreMov.Text = Trim(CType(row.FindControl("lblNombre"), Label).Text)
        txtCorreo.Text = Trim(CType(row.FindControl("lblCorreo"), Label).Text)
        cboProceso.SelectedValue = Trim(CType(row.FindControl("hdnIdTipoProceso"), HiddenField).Value)

        hdnActivo.Value = 1
        hdnId.Value = CType(row.FindControl("hdnId"), HiddenField).Value
    End Sub


    Protected Sub EliminarClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdCorreos.Rows(rowIndex)

        lblTitModal.Text = "CORREO : " & Trim(CType(row.FindControl("lblCorreo"), Label).Text)
        hdnId.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If Trim(txtrut.Text) = "" Then
            MessageBox("Guardar", "Ingrese RUT Valido", Page, Master, "W")
        Else
            If hdnActivo.Value = 1 Then
                Call ActualizarCorreo()
                MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
            Else
                Call InsertarCorreo()
                MessageBox("Insertar", "Información Almacenada", Page, Master, "S")
            End If
            Call Limpiar()
        End If
    End Sub

    Private Sub Limpiar()
        txtrut.Text = ""
        txtNombreMov.Text = ""
        txtCorreo.Text = ""
        cboProceso.ClearSelection()
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Call EliminarCorreo()
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
        Call Limpiar()
    End Sub



    Protected Sub txtrut_TextChanged(sender As Object, e As EventArgs) Handles txtrut.TextChanged
        txtrut.Text = Replace(txtrut.Text, ".", "")
        txtrut.Text = Replace(txtrut.Text, "-", "")
        If Len(txtrut.Text) = 8 Then
            txtrut.Text = Left(txtrut.Text, 7) + "-" + Right(txtrut.Text, 1)
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = Left(txtrut.Text, 8) + "-" + Right(txtrut.Text, 1)
        End If

        If Len(txtrut.Text) = 10 Then
            txtrut.Text = txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call Limpiar()
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = "0" + txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call Limpiar()
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        End If
    End Sub

    Private Sub BuscarPersonal()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec mant_correos 'BPE', '" & Trim(txtrut.Text) & "'"
        Try
            Dim cnxBDTC_QA As SqlConnection = New SqlConnection(strCnx)
            cnxBDTC_QA.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBDTC_QA)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                txtNombreMov.Text = rdoReader(0).ToString
                txtCorreo.Text = rdoReader(1).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBDTC_QA.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBDTC_QA = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtNombreMov_TextChanged(sender As Object, e As EventArgs) Handles txtNombreMov.TextChanged
        Dim blnVisible As Boolean = cbobuscarnombre.Visible, intRegistros As Integer = 0
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtNombreMov.Visible = blnVisible
            Call CargarComboNombre(Trim(txtNombreMov.Text), intRegistros)
            If intRegistros = 2 And cbobuscarnombre.Visible = True Then
                cbobuscarnombre.SelectedIndex = 1
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
            End If
            cbobuscarnombre.ClearSelection()
        Catch ex As Exception
            MessageBoxError("txtNombreMov_TextChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboNombre(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Exec mant_correos 'CCN', '" & Trim(strBuscar) & "'"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cbobuscarnombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cbobuscarnombre.DataTextField = "nom_personal"
            cbobuscarnombre.DataValueField = "rut_personal"
            cbobuscarnombre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombre", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cbobuscarnombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbobuscarnombre.SelectedIndexChanged
        Dim blnVisible As Boolean = cbobuscarnombre.Visible
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtNombreMov.Visible = blnVisible
            If cbobuscarnombre.SelectedValue <> "0" Then
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
                txtrut.Text = varValores(0).ToString
                Call BuscarPersonal()
            End If

        Catch ex As Exception
            MessageBoxError("cboBuscar_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

End Class