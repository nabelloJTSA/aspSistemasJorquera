﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspAlertas
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Call CargarAlertas()
                Call CargarCategorias()
                Call CargarClientes()
                Call CargarAlertasBase()
                Call CargarRoles()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAlertas()
        Try
            Dim strNomTabla As String = "TCMae_alertas"
            Dim strSQL As String = "Exec mant_alertas 'G'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarContactos(ByVal grd As GridView, ByVal id As Integer)
        Dim strNomTabla As String = "TCMae_contactos"
        Dim strSQL As String = "Exec mant_alertas 'GC', '" + id + "'"
        grd.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
        grd.DataBind()
    End Sub
    Private Sub CargarRoles()
        Try
            Dim strNomTablaR As String = "TCMae_roles"
            Dim strSQLR As String = "Exec mant_alertas 'CCR'"
            cboRoles.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboRoles.DataTextField = "nom_rol"
            cboRoles.DataValueField = "id_rol"
            cboRoles.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Public Sub CargarUsuarios()
        Try
            Dim strNomTablaR As String = "TCMae_usuarios"
            Dim strSQLR As String = "Exec mant_alertas 'CCU'"
            cboUsuarios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboUsuarios.DataTextField = "nom_usuario"
            cboUsuarios.DataValueField = "rut_usuario"
            cboUsuarios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec mant_alertas 'CCC'"
            cboCliente.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCliente.DataTextField = "nom_cliente"
            cboCliente.DataValueField = "rut_cliente"
            cboCliente.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCategorias()
        Try
            Dim strNomTablaR As String = "TCMae_categoria"
            Dim strSQLR As String = "Exec mant_categoria 'G'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "nom_categoria"
            cboCategoria.DataValueField = "id_categoria"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategorias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAlertasBase()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec mant_alertas 'CCA'"
            cboAlertaBase.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboAlertaBase.DataTextField = "nom_alerta"
            cboAlertaBase.DataValueField = "id_alerta_base"
            cboAlertaBase.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategorias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarNiveles()
        Try
            Dim strNomTablaR As String = "TCMae_niveles"
            Dim strSQLR As String = "Exec mant_alertas 'CLN', '" + hdnEdit.Value + "'"
            listaNiveles.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            listaNiveles.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Niveles", Err, Page, Master)
        End Try
    End Sub
    Private Sub Limpiar()
        Try
            txtAlertaNom.Text = ""
            cboCategoria.ClearSelection()
            cboCriticidad.ClearSelection()
            cboCliente.ClearSelection()
            txtAlertaDesc.Text = ""
            txtAlertaFecha.Text = ""
            txtAlertaInicio.Text = ""
            txtAlertaFin.Text = ""
            txtFrecOcurrencia.Text = ""
            txtFrecTiempo.Text = ""
            txtAlertaSepDistancia.Text = ""
            txtFrecSepTiempo.Text = ""
            cboAlertaBase.ClearSelection()
            chkFrecOcu.Checked = False
            txtFrecOcurrencia.Enabled = False
            chkFrecTiempo.Checked = False
            txtFrecTiempo.Enabled = False
            chkSepTiempo.Checked = False
            txtFrecSepTiempo.Enabled = False
            chkSepDistancia.Checked = False
            txtAlertaSepDistancia.Enabled = False
        Catch ex As Exception
            MessageBoxError("LimpiarCampos", Err, Page, Master)
        End Try
    End Sub
    Public Function Validar()
        Dim valid As Boolean = True
        Try
            If txtAlertaNom.Text = "" Or txtAlertaDesc.Text = "" Or txtAlertaFecha.Text = "" Or txtAlertaInicio.Text = "" Or txtAlertaFin.Text = "" Then
                valid = False
            End If
            If txtFrecOcurrencia.Text = "" And txtFrecTiempo.Text = "" Then
                valid = False
            End If
            If txtAlertaSepDistancia.Text = "" And txtFrecSepTiempo.Text = "" Then
                valid = False
            End If
            Return valid
        Catch ex As Exception
            Return valid
            MessageBoxError("ValidarCampos", Err, Page, Master)
        End Try
    End Function
    Protected Sub Insertar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"

            comando.Parameters.Add("@nom_alerta", SqlDbType.NVarChar)
            comando.Parameters("@nom_alerta").Value = txtAlertaNom.Text

            comando.Parameters.Add("@id_categoria", SqlDbType.Int)
            comando.Parameters("@id_categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@cri_alerta", SqlDbType.NVarChar)
            comando.Parameters("@cri_alerta").Value = cboCriticidad.SelectedValue

            comando.Parameters.Add("@cli_alerta", SqlDbType.NVarChar)
            comando.Parameters("@cli_alerta").Value = cboCliente.SelectedValue

            comando.Parameters.Add("@des_alerta", SqlDbType.NVarChar)
            comando.Parameters("@des_alerta").Value = txtAlertaDesc.Text

            comando.Parameters.Add("@fec_activa", SqlDbType.Date)
            comando.Parameters("@fec_activa").Value = txtAlertaFecha.Text

            comando.Parameters.Add("@hor_inicio", SqlDbType.NVarChar)
            comando.Parameters("@hor_inicio").Value = txtAlertaInicio.Text

            comando.Parameters.Add("@hor_fin", SqlDbType.NVarChar)
            comando.Parameters("@hor_fin").Value = txtAlertaFin.Text

            Dim frecocu As Integer = If(txtFrecOcurrencia.Text = "", 0, Integer.Parse(txtFrecOcurrencia.Text))
            comando.Parameters.Add("@ocu_cantidad", SqlDbType.Int)
            comando.Parameters("@ocu_cantidad").Value = frecocu

            Dim frectiem As String = If(txtFrecTiempo.Text = "", "", txtFrecTiempo.Text)
            comando.Parameters.Add("@ocu_tiempo", SqlDbType.NVarChar)
            comando.Parameters("@ocu_tiempo").Value = frectiem

            Dim septiem As String = If(txtFrecSepTiempo.Text = "", "", txtFrecSepTiempo.Text)
            comando.Parameters.Add("@sep_tiempo", SqlDbType.NVarChar)
            comando.Parameters("@sep_tiempo").Value = septiem

            Dim sepdist As Integer = If(txtAlertaSepDistancia.Text = "", 0, txtAlertaSepDistancia.Text)
            comando.Parameters.Add("@sep_distancia", SqlDbType.NVarChar)
            comando.Parameters("@sep_distancia").Value = sepdist

            comando.Parameters.Add("@est_alerta", SqlDbType.Int)
            comando.Parameters("@est_alerta").Value = 1

            comando.Parameters.Add("@id_alerta_base", SqlDbType.Int)
            comando.Parameters("@id_alerta_base").Value = cboAlertaBase.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarAlerta", Err, Page, Master)
        End Try
    End Sub
    Protected Sub Actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"

            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = hdnEdit.Value

            comando.Parameters.Add("@nom_alerta", SqlDbType.NVarChar)
            comando.Parameters("@nom_alerta").Value = txtAlertaNom.Text

            comando.Parameters.Add("@id_categoria", SqlDbType.Int)
            comando.Parameters("@id_categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@cri_alerta", SqlDbType.NVarChar)
            comando.Parameters("@cri_alerta").Value = cboCriticidad.SelectedValue

            comando.Parameters.Add("@cli_alerta", SqlDbType.NVarChar)
            comando.Parameters("@cli_alerta").Value = cboCliente.SelectedValue

            comando.Parameters.Add("@des_alerta", SqlDbType.NVarChar)
            comando.Parameters("@des_alerta").Value = txtAlertaDesc.Text

            comando.Parameters.Add("@fec_activa", SqlDbType.Date)
            comando.Parameters("@fec_activa").Value = txtAlertaFecha.Text

            comando.Parameters.Add("@hor_inicio", SqlDbType.Time)
            comando.Parameters("@hor_inicio").Value = txtAlertaInicio.Text

            comando.Parameters.Add("@hor_fin", SqlDbType.Time)
            comando.Parameters("@hor_fin").Value = txtAlertaFin.Text

            Dim frecocu As Integer = If(txtFrecOcurrencia.Text = "", 0, Integer.Parse(txtFrecOcurrencia.Text))
            comando.Parameters.Add("@ocu_cantidad", SqlDbType.Int)
            comando.Parameters("@ocu_cantidad").Value = frecocu

            Dim frectiem As String = If(txtFrecTiempo.Text = "", "", txtFrecTiempo.Text)
            comando.Parameters.Add("@ocu_tiempo", SqlDbType.NVarChar)
            comando.Parameters("@ocu_tiempo").Value = frectiem

            Dim septiem As String = If(txtFrecSepTiempo.Text = "", "", txtFrecSepTiempo.Text)
            comando.Parameters.Add("@sep_tiempo", SqlDbType.NVarChar)
            comando.Parameters("@sep_tiempo").Value = septiem

            Dim sepdist As Integer = If(txtAlertaSepDistancia.Text = "", 0, txtAlertaSepDistancia.Text)
            comando.Parameters.Add("@sep_distancia", SqlDbType.NVarChar)
            comando.Parameters("@sep_distancia").Value = sepdist

            comando.Parameters.Add("@est_alerta", SqlDbType.Int)
            comando.Parameters("@est_alerta").Value = 1

            comando.Parameters.Add("@id_alerta_base", SqlDbType.Int)
            comando.Parameters("@id_alerta_base").Value = cboAlertaBase.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarAlerta", Err, Page, Master)
        End Try
    End Sub
    Private Sub CambiarEstado(id, estado)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "CE"

            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id

            comando.Parameters.Add("@est_alerta", SqlDbType.Int)
            comando.Parameters("@est_alerta").Value = estado

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CambiarEstado", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnNuevo_Click(sender As Object, e As EventArgs)
        Call Limpiar()
        divTres.Visible = False
        listaNiveles.DataSource = ""
        listaNiveles.Items.Clear()
        listaNiveles.DataBind()
        btnScript.Enabled = False
        btnScript.CssClass = "btn bg-gray"
        btnGuardarNivel.Enabled = False
        btnGuardarNivel.CssClass = "btn bg-gray"
        btnGuardarAlerta.Enabled = True
        btnGuardarAlerta.CssClass = "btn bg-orange"
        btnCancelarAlerta.Enabled = True
        btnCancelarAlerta.CssClass = "btn bg-gray"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
        upModal.Update()
    End Sub
    Protected Sub btnGuardarAlerta_Click(sender As Object, e As EventArgs)
        Try
            If Validar() Then
                If hdnEdit.Value <> 0 Then
                    Call Actualizar()
                    Call CargarAlertas()
                    MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
                Else
                    Call Insertar()
                    Call CargarAlertas()

                    btnScript.Enabled = True
                    btnScript.CssClass = "btn bg-orange"
                    btnGuardarNivel.Enabled = True
                    btnGuardarNivel.CssClass = "btn bg-orange"
                    btnGuardarAlerta.Enabled = False
                    btnGuardarAlerta.CssClass = "btn bg-gray"

                    hdnEdit.Value = 0

                    MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
                End If
            Else
                MessageBox("Guardar", "Faltan campos por completar", Page, Master, "E")
            End If
        Catch ex As Exception
            MessageBoxError("InsertarAlerta", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkFrecOcu_CheckedChanged(sender As Object, e As EventArgs)
        If chkFrecOcu.Checked Then
            txtFrecOcurrencia.Enabled = True

            chkFrecTiempo.Checked = False
            txtFrecTiempo.Text = ""
            txtFrecTiempo.Enabled = False
            chkSepTiempo.Enabled = True
        Else
            txtFrecOcurrencia.Enabled = False
            txtFrecOcurrencia.Text = ""
        End If
    End Sub
    Protected Sub chkFrecTiempo_CheckedChanged(sender As Object, e As EventArgs)
        If chkFrecTiempo.Checked Then
            txtFrecTiempo.Enabled = True
            chkSepTiempo.Enabled = False

            chkSepTiempo.Checked = False
            chkSepDistancia.Enabled = True
            txtFrecSepTiempo.Text = ""
            txtFrecSepTiempo.Enabled = False

            chkFrecOcu.Checked = False
            txtFrecOcurrencia.Text = ""
            txtFrecOcurrencia.Enabled = False
        Else
            txtFrecTiempo.Text = ""
            txtFrecTiempo.Enabled = False

            chkSepTiempo.Enabled = True
        End If
    End Sub
    Protected Sub chkSepDistancia_CheckedChanged(sender As Object, e As EventArgs)
        If chkSepDistancia.Checked Then
            txtAlertaSepDistancia.Enabled = True

            chkSepTiempo.Checked = False
            txtFrecSepTiempo.Text = ""
            txtFrecSepTiempo.Enabled = False
        Else
            txtAlertaSepDistancia.Text = ""
            txtAlertaSepDistancia.Enabled = False
        End If
    End Sub
    Protected Sub chkSepTiempo_CheckedChanged(sender As Object, e As EventArgs)
        If chkSepTiempo.Checked Then
            txtFrecSepTiempo.Enabled = True

            chkSepDistancia.Checked = False
            txtAlertaSepDistancia.Text = ""
            txtAlertaSepDistancia.Enabled = False
        Else
            txtFrecSepTiempo.Text = ""
            txtFrecSepTiempo.Enabled = False
        End If
    End Sub
    Protected Sub btnEstado_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)

        Dim id As Integer = CType(row.FindControl("hdnIDAlerta"), HiddenField).Value
        Dim estado As Integer = CType(row.FindControl("hdnEstado"), HiddenField).Value

        If estado = 1 Then
            estado = 0
        Else
            estado = 1
        End If
        Call CambiarEstado(id, estado)
        Call CargarAlertas()
    End Sub
    Protected Sub btnSeleccionar_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)
        Dim id As Integer = CType(row.FindControl("hdnIDAlerta"), HiddenField).Value

        hdnEdit.Value = id
        btnGuardarAlerta.Enabled = True
        btnGuardarAlerta.CssClass = "btn bg-orange"
        btnCancelarAlerta.Enabled = True
        btnCancelarAlerta.CssClass = "btn bg-gray"

        btnScript.Enabled = True
        btnScript.CssClass = "btn bg-orange"
        btnGuardarNivel.Enabled = True
        btnGuardarNivel.CssClass = "btn bg-orange"

        txtAlertaNom.Text = CType(row.FindControl("lblNomAlerta"), Label).Text
        cboCategoria.SelectedValue = CType(row.FindControl("hdnIDCategoria"), HiddenField).Value
        cboCriticidad.SelectedValue = CType(row.FindControl("lblNomCriticidad"), Label).Text
        cboCliente.SelectedValue = CType(row.FindControl("hdnRUTCliente"), HiddenField).Value
        cboAlertaBase.SelectedValue = CType(row.FindControl("hdnIDAlertaBase"), HiddenField).Value
        txtAlertaDesc.Text = CType(row.FindControl("lblDescripcion"), Label).Text
        txtAlertaFecha.Text = DateTime.Parse(CType(row.FindControl("lblFecActivacion"), Label).Text).ToString("yyyy-MM-dd")
        txtAlertaInicio.Text = CType(row.FindControl("lblHoraInicio"), Label).Text
        txtAlertaFin.Text = CType(row.FindControl("lblHoraFin"), Label).Text
        txtFrecOcurrencia.Text = CType(row.FindControl("lblCantOcurrencias"), Label).Text
        txtFrecTiempo.Text = CType(row.FindControl("lblTiemOcurrencias"), Label).Text
        txtFrecSepTiempo.Text = CType(row.FindControl("lblTiemSeparacion"), Label).Text
        txtAlertaSepDistancia.Text = CType(row.FindControl("lblDistSeparacion"), Label).Text
        If txtFrecOcurrencia.Text <> "" Then
            chkFrecOcu.Checked = True
            txtFrecOcurrencia.Enabled = True
        End If
        If txtFrecTiempo.Text <> "00:00" Then
            chkFrecTiempo.Checked = True
            txtFrecTiempo.Enabled = True
            chkSepTiempo.Enabled = False
        End If
        If txtAlertaSepDistancia.Text <> "" Then
            chkSepDistancia.Checked = True
            txtAlertaSepDistancia.Enabled = True
        End If
        If txtFrecSepTiempo.Text <> "00:00" Then
            chkSepTiempo.Checked = True
            txtFrecSepTiempo.Enabled = True
        End If

        'VISIVILIDAD
        divTres.Visible = True
        upPanelTres.Visible = True

        'NIVELES
        txtNomNivel.Text = ""
        txtScrLlamada.Text = ""
        txtScrCorreo.Text = ""
        txtScrSms.Text = ""
        txtScrWsp.Text = ""
        hdnEditarNivel.Value = 0

        Call LimpiarNivel()
        Call CargarNiveles()

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", True)
        upModal.Update()
    End Sub
    Protected Sub btnScript_Click(sender As Object, e As EventArgs)
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalContactos", "$('#myModalContactos').modal();", True)
        'upModalContactos.Update()
    End Sub

    'MANEJO DE NIVELES
    Private Sub listaNiveles_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles listaNiveles.ItemDataBound
        Dim row As ListViewItem = e.Item
        Dim id As Integer = CType(row.FindControl("hdnIDNivel"), HiddenField).Value
        Dim grd As GridView = CType(row.FindControl("grdContactos"), GridView)
        Dim strNomTabla As String = "TCMae_contactos"
        Dim strSQL As String = "Exec mant_alertas 'GC', '" & id & "'"
        grd.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
        grd.DataBind()
    End Sub
    Protected Sub btnGuardarNivel_Click(sender As Object, e As EventArgs)
        Try
            Dim id As Integer = hdnEditarNivel.Value
            If hdnEditarNivel.Value <> 0 Then
                Call ActualizarNivel(id)
                hdnEditarNivel.Value = 0
            Else
                Call InsertarNivel()
            End If
            Call CargarNiveles()
            Call LimpiarNivel()
            MessageBox("InsertarNivel", "Nivel Guardado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("InsertarNivel", Err, Page, Master)
        End Try
    End Sub
    Protected Sub ActualizarNivel(ByVal id As Integer)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_alertas"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "UN"
        comando.Parameters.Add("@id_nivel", SqlDbType.Int)
        comando.Parameters("@id_nivel").Value = hdnEditarNivel.Value
        comando.Parameters.Add("@nom_nivel", SqlDbType.NVarChar)
        comando.Parameters("@nom_nivel").Value = txtNomNivel.Text
        comando.Parameters.Add("@scr_llamada", SqlDbType.NVarChar)
        comando.Parameters("@scr_llamada").Value = txtScrLlamada.Text
        comando.Parameters.Add("@scr_sms", SqlDbType.NVarChar)
        comando.Parameters("@scr_sms").Value = txtScrSms.Text
        comando.Parameters.Add("@scr_correo", SqlDbType.NVarChar)
        comando.Parameters("@scr_correo").Value = txtScrCorreo.Text
        comando.Parameters.Add("@scr_wsp", SqlDbType.NVarChar)
        comando.Parameters("@scr_wsp").Value = txtScrWsp.Text
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub
    Protected Sub InsertarNivel()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_alertas"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "IE"
        comando.Parameters.Add("@nom_nivel", SqlDbType.NVarChar)
        comando.Parameters("@nom_nivel").Value = txtNomNivel.Text
        comando.Parameters.Add("@scr_llamada", SqlDbType.NVarChar)
        comando.Parameters("@scr_llamada").Value = txtScrLlamada.Text
        comando.Parameters.Add("@scr_sms", SqlDbType.NVarChar)
        comando.Parameters("@scr_sms").Value = txtScrSms.Text
        comando.Parameters.Add("@scr_correo", SqlDbType.NVarChar)
        comando.Parameters("@scr_correo").Value = txtScrCorreo.Text
        comando.Parameters.Add("@scr_wsp", SqlDbType.NVarChar)
        comando.Parameters("@scr_wsp").Value = txtScrWsp.Text
        comando.Parameters.Add("@id_alerta", SqlDbType.Int)
        comando.Parameters("@id_alerta").Value = hdnEdit.Value
        comando.Parameters.Add("@est_nivel", SqlDbType.NVarChar)
        comando.Parameters("@est_nivel").Value = 1
        comando.Parameters.Add("@est_obligatorio", SqlDbType.NVarChar)
        comando.Parameters("@est_obligatorio").Value = 1
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub
    Public Sub CambiarEstadoNivel(id, estado)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "CEN"

            comando.Parameters.Add("@id_nivel", SqlDbType.Int)
            comando.Parameters("@id_nivel").Value = id

            comando.Parameters.Add("@est_nivel", SqlDbType.Int)
            comando.Parameters("@est_nivel").Value = estado

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Cambiar Estado Nivel", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnEstadoObligatorio_Click(sender As Object, e As EventArgs)
        Dim btn As Button = CType(sender, Button)
        Dim row As ListViewItem = CType(btn.NamingContainer, ListViewItem)

        Dim id As Integer = CType(row.FindControl("hdnIDNivel"), HiddenField).Value
        Dim estado As Integer = CType(row.FindControl("hdnObligatorio"), HiddenField).Value

        If estado = 1 Then
            estado = 0
        Else
            estado = 1
        End If
        Call CambiarEstadoObligatorio(id, estado)
        Call CargarNiveles()
    End Sub
    Public Sub CambiarEstadoObligatorio(id, estado)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"

            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "CEON"

            comando.Parameters.Add("@id_nivel", SqlDbType.Int)
            comando.Parameters("@id_nivel").Value = id

            comando.Parameters.Add("@est_obligatorio", SqlDbType.Int)
            comando.Parameters("@est_obligatorio").Value = estado

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Cambiar Estado Obligatorio", Err, Page, Master)
        End Try
    End Sub
    Public Sub LimpiarNivel()
        txtNomNivel.Text = ""
        txtScrLlamada.Text = ""
        txtScrCorreo.Text = ""
        txtScrSms.Text = ""
        txtScrWsp.Text = ""
    End Sub
    Protected Sub linEstadoNivel_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim row As ListViewItem = CType(btn.NamingContainer, ListViewItem)

        Dim id As Integer = CType(row.FindControl("hdnIDNivel"), HiddenField).Value
        Dim estado As Integer = CType(row.FindControl("hdnEstadoNivel"), HiddenField).Value

        If estado = 1 Then
            estado = 0
        Else
            estado = 1
        End If
        Call CambiarEstadoNivel(id, estado)
        Call CargarNiveles()
    End Sub
    Protected Sub linObligatorio_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim row As ListViewItem = CType(btn.NamingContainer, ListViewItem)

        Dim id As Integer = CType(row.FindControl("hdnIDNivel"), HiddenField).Value
        Dim estado As Integer = CType(row.FindControl("hdnObligatorio"), HiddenField).Value

        If estado = 1 Then
            estado = 0
        Else
            estado = 1
        End If
        Call CambiarEstadoObligatorio(id, estado)
        Call CargarNiveles()
    End Sub

    'CONTROLES DE CAMPOS DE TEXTO - SCRIPTS
    Protected Sub txtScrLlamada_TextChanged(sender As Object, e As EventArgs)
        hdnCampo.Value = "Llamada"
    End Sub
    Protected Sub txtScrSms_TextChanged(sender As Object, e As EventArgs)
        hdnCampo.Value = "SMS"
    End Sub
    Protected Sub txtScrCorreo_TextChanged(sender As Object, e As EventArgs)
        hdnCampo.Value = "Correo"
    End Sub
    Protected Sub txtScrWsp_TextChanged(sender As Object, e As EventArgs)
        hdnCampo.Value = "Wsp"
    End Sub
    Protected Sub btnScrPersona_Click(sender As Object, e As EventArgs)
        If hdnCampo.Value = "Llamada" Then
            txtScrLlamada.Text = txtScrLlamada.Text & " " & "@NomPersona"
        ElseIf hdnCampo.Value = "SMS" Then
            txtScrSms.Text = txtScrSms.Text & " " & "@NomPersona"
        ElseIf hdnCampo.Value = "Correo" Then
            txtScrCorreo.Text = txtScrCorreo.Text & " " & "@NomPersona"
        ElseIf hdnCampo.Value = "Wsp" Then
            txtScrWsp.Text = txtScrWsp.Text & " " & "@NomPersona"
        End If
    End Sub
    Protected Sub btnScrUsuario_Click(sender As Object, e As EventArgs)
        If hdnCampo.Value = "Llamada" Then
            txtScrLlamada.Text = txtScrLlamada.Text & " " & "@NomUsuario"
        ElseIf hdnCampo.Value = "SMS" Then
            txtScrSms.Text = txtScrSms.Text & " " & "@NomUsuario"
        ElseIf hdnCampo.Value = "Correo" Then
            txtScrCorreo.Text = txtScrCorreo.Text & " " & "@NomUsuario"
        ElseIf hdnCampo.Value = "Wsp" Then
            txtScrWsp.Text = txtScrWsp.Text & " " & "@NomUsuario"
        End If
    End Sub
    Protected Sub btnScrPatente_Click(sender As Object, e As EventArgs)
        If hdnCampo.Value = "Llamada" Then
            txtScrLlamada.Text = txtScrLlamada.Text & " " & "@NroPatente"
        ElseIf hdnCampo.Value = "SMS" Then
            txtScrSms.Text = txtScrSms.Text & " " & "@NroPatente"
        ElseIf hdnCampo.Value = "Correo" Then
            txtScrCorreo.Text = txtScrCorreo.Text & " " & "@NroPatente"
        ElseIf hdnCampo.Value = "Wsp" Then
            txtScrWsp.Text = txtScrWsp.Text & " " & "@NroPatente"
        End If
    End Sub
    Protected Sub btnScrAlerta_Click(sender As Object, e As EventArgs)
        If hdnCampo.Value = "Llamada" Then
            txtScrLlamada.Text = txtScrLlamada.Text & " " & "@TipoAlerta"
        ElseIf hdnCampo.Value = "SMS" Then
            txtScrSms.Text = txtScrSms.Text & " " & "@TipoAlerta"
        ElseIf hdnCampo.Value = "Correo" Then
            txtScrCorreo.Text = txtScrCorreo.Text & " " & "@TipoAlerta"
        ElseIf hdnCampo.Value = "Wsp" Then
            txtScrWsp.Text = txtScrWsp.Text & " " & "@TipoAlerta"
        End If
    End Sub
    Protected Sub linEditarNivel_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim row As ListViewItem = CType(btn.NamingContainer, ListViewItem)

        hdnEditarNivel.Value = CType(row.FindControl("hdnIDNivel"), HiddenField).Value
        txtNomNivel.Text = CType(row.FindControl("hdnNomNivel"), HiddenField).Value
        txtScrLlamada.Text = CType(row.FindControl("hdnNivelLlamada"), HiddenField).Value
        txtScrCorreo.Text = CType(row.FindControl("hdnNivelCorreo"), HiddenField).Value
        txtScrSms.Text = CType(row.FindControl("hdnNivelSMS"), HiddenField).Value
        txtScrWsp.Text = CType(row.FindControl("hdnNivelWsp"), HiddenField).Value

    End Sub

    'MANEJO DE CONTACTOS
    Protected Sub cboContTipo_SelectedIndexChanged(sender As Object, e As EventArgs)
        If cboContTipo.SelectedValue <> "Contacto Fijo" Then
            txtConNombre.Text = ""
            txtConNombre.Enabled = False
            txtConTelefono.Text = ""
            txtConTelefono.Enabled = False
            txtConCorreo.Text = ""
            txtConCorreo.Enabled = False
            cboUsuarios.Enabled = False
        Else
            txtConNombre.Enabled = True
            txtConTelefono.Enabled = True
            txtConCorreo.Enabled = True
        End If

        If cboContTipo.SelectedValue = "Usuario de Sistema" Then
            cboUsuarios.Enabled = True
            Call CargarUsuarios()
        Else
            cboUsuarios.Enabled = False
            cboUsuarios.DataSource = ""
            cboUsuarios.DataBind()
        End If
    End Sub
    Protected Sub linAgregarContacto_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim row As ListViewItem = CType(btn.NamingContainer, ListViewItem)

        hdnInsNivel.Value = CType(row.FindControl("hdnIDNivel"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalInsertarCon", "$('#myModalInsertarCon').modal();", True)
    End Sub
    Protected Sub btnGuardarContacto_Click(sender As Object, e As EventArgs)
        Dim id As Integer = hdnInsNivel.Value
        If hdnEditarContacto.Value <> 0 Then
            Call ActualizarContacto(hdnEditarContacto.Value)
            hdnEditarContacto.Value = 0
        Else
            Call InsertarContacto(id)
        End If
        Call CargarNiveles()
        Call LimpiarContacto()
        txtConNombre.Enabled = True
        txtConTelefono.Enabled = True
        txtConCorreo.Enabled = True
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalInsertarCon", "$('#myModalInsertarCon').modal('hide');", True)
    End Sub
    Public Sub InsertarContacto(ByVal idNivel As Integer)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_alertas"

        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "IC"

        comando.Parameters.Add("@tip_contacto", SqlDbType.NVarChar)
        comando.Parameters("@tip_contacto").Value = cboContTipo.SelectedValue

        comando.Parameters.Add("@nom_contacto", SqlDbType.NVarChar)
        comando.Parameters("@nom_contacto").Value = txtConNombre.Text

        comando.Parameters.Add("@cor_contacto", SqlDbType.NVarChar)
        comando.Parameters("@cor_contacto").Value = txtConCorreo.Text

        comando.Parameters.Add("@tel_contacto", SqlDbType.NVarChar)
        comando.Parameters("@tel_contacto").Value = txtConTelefono.Text

        Dim act_llamada As Integer = If(cheLlamada.Checked, 1, 0)
        comando.Parameters.Add("@act_llamada", SqlDbType.Int)
        comando.Parameters("@act_llamada").Value = act_llamada

        Dim act_sms As Integer = If(cheSMS.Checked, 1, 0)
        comando.Parameters.Add("@act_sms", SqlDbType.Int)
        comando.Parameters("@act_sms").Value = act_sms

        Dim act_correo As Integer = If(cheCorreo.Checked, 1, 0)
        comando.Parameters.Add("@act_correo", SqlDbType.Int)
        comando.Parameters("@act_correo").Value = act_correo

        Dim act_wsp As Integer = If(cheWsp.Checked, 1, 0)
        comando.Parameters.Add("@act_wsp", SqlDbType.Int)
        comando.Parameters("@act_wsp").Value = act_wsp

        comando.Parameters.Add("@id_nivel", SqlDbType.Int)
        comando.Parameters("@id_nivel").Value = idNivel

        comando.Parameters.Add("@id_rol", SqlDbType.Int)
        comando.Parameters("@id_rol").Value = cboRoles.SelectedValue

        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub
    Public Sub ActualizarContacto(ByVal id As Integer)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_alertas"

        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "MC"

        comando.Parameters.Add("@id", SqlDbType.NVarChar)
        comando.Parameters("@id").Value = id

        comando.Parameters.Add("@tip_contacto", SqlDbType.NVarChar)
        comando.Parameters("@tip_contacto").Value = cboContTipo.SelectedValue

        comando.Parameters.Add("@nom_contacto", SqlDbType.NVarChar)
        comando.Parameters("@nom_contacto").Value = txtConNombre.Text

        comando.Parameters.Add("@cor_contacto", SqlDbType.NVarChar)
        comando.Parameters("@cor_contacto").Value = txtConCorreo.Text

        comando.Parameters.Add("@tel_contacto", SqlDbType.NVarChar)
        comando.Parameters("@tel_contacto").Value = txtConTelefono.Text

        Dim act_llamada As Integer = If(cheLlamada.Checked, 1, 0)
        comando.Parameters.Add("@act_llamada", SqlDbType.Int)
        comando.Parameters("@act_llamada").Value = act_llamada

        Dim act_sms As Integer = If(cheSMS.Checked, 1, 0)
        comando.Parameters.Add("@act_sms", SqlDbType.Int)
        comando.Parameters("@act_sms").Value = act_sms

        Dim act_correo As Integer = If(cheCorreo.Checked, 1, 0)
        comando.Parameters.Add("@act_correo", SqlDbType.Int)
        comando.Parameters("@act_correo").Value = act_correo

        Dim act_wsp As Integer = If(cheWsp.Checked, 1, 0)
        comando.Parameters.Add("@act_wsp", SqlDbType.Int)
        comando.Parameters("@act_wsp").Value = act_wsp

        comando.Parameters.Add("@id_rol", SqlDbType.Int)
        comando.Parameters("@id_rol").Value = cboRoles.SelectedValue

        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub
    Protected Sub btnBorrarContacto_Click(sender As Object, e As EventArgs)
        Dim btn As Button = CType(sender, Button)
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim id As Integer = CType(row.FindControl("hdnIDContacto"), HiddenField).Value
        Call EliminarContacto(id)
        Call CargarNiveles()
    End Sub
    Public Sub EliminarContacto(ByVal id As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_alertas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EC"
            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("BorrarContacto", Err, Page, Master)
        End Try
    End Sub
    Public Sub LimpiarContacto()
        cboContTipo.ClearSelection()
        cboRoles.ClearSelection()
        cboUsuarios.ClearSelection()
        txtConNombre.Text = ""
        txtConTelefono.Text = ""
        txtConCorreo.Text = ""
        cheLlamada.Checked = False
        cheSMS.Checked = False
        cheCorreo.Checked = False
        cheWsp.Checked = False
    End Sub

    Protected Sub cboUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarDatosUsuario()
    End Sub
    Protected Sub CargarDatosUsuario()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim rdoReader As SqlDataReader
            Dim cmdTemporal As SqlCommand, comando As New SqlCommand
            Dim strSQL As String = "Exec mant_alertas 'CDU', '" & cboUsuarios.SelectedValue & "'"
            Using cnxBDTC_QA As New SqlConnection(strCnx)
                cnxBDTC_QA.Open()
                Try
                    comando = New SqlCommand(strSQL, cnxBDTC_QA)
                    rdoReader = comando.ExecuteReader()
                    If rdoReader.Read Then
                        txtConNombre.Text = rdoReader(0).ToString
                        txtConTelefono.Text = rdoReader(1).ToString
                        txtConCorreo.Text = rdoReader(2).ToString
                    End If
                    rdoReader.Close()
                    rdoReader = Nothing
                    comando.Dispose()
                    cmdTemporal = Nothing
                Catch ex As Exception
                    MessageBoxError("Cargar Datos Usuario", Err, Page, Master)
                End Try
            End Using
        Catch ex As Exception
            MessageBoxError("Cargar Datos Usuario - Conexión", Err, Page, Master)
        End Try
    End Sub
    Protected Sub linEditContacto_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = CType(sender, LinkButton)
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        hdnEditarContacto.Value = CType(row.FindControl("hdnIDContacto"), HiddenField).Value

        'Cargar datos
        txtConNombre.Text = CType(row.FindControl("lblNomContacto"), Label).Text
        txtConCorreo.Text = CType(row.FindControl("lblCorContacto"), Label).Text
        txtConTelefono.Text = CType(row.FindControl("lblTelContacto"), Label).Text
        cboContTipo.SelectedValue = CType(row.FindControl("hdnTipoContacto"), HiddenField).Value
        cboRoles.SelectedValue = CType(row.FindControl("hdnIDRol"), HiddenField).Value
        If CType(row.FindControl("hdnLlamada"), HiddenField).Value = 1 Then
            cheLlamada.Checked = True
        End If
        If CType(row.FindControl("hdnSMS"), HiddenField).Value = 1 Then
            cheSMS.Checked = True
        End If
        If CType(row.FindControl("hdnCorreo"), HiddenField).Value = 1 Then
            cheCorreo.Checked = True
        End If
        If CType(row.FindControl("hdnWsp"), HiddenField).Value = 1 Then
            cheWsp.Checked = True
        End If
        upInsertarCon.Update()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalInsertarCon", "$('#myModalInsertarCon').modal();", True)
    End Sub
End Class
