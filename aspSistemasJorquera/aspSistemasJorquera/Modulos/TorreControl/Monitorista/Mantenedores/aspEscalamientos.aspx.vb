﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb


Public Class aspEscalamientos
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "TCMov_homologacion_alertas"
            Dim strSQL As String = "Exec mant_escalamiento 'G'"
            grdDatos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDatos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub


    Protected Sub Insertar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_escalamiento"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_escalamiento", SqlDbType.NVarChar)
            comando.Parameters("@nom_escalamiento").Value = Trim(txtNombre.Text)
            comando.Parameters.Add("@des_escalamiento", SqlDbType.NVarChar)
            comando.Parameters("@des_escalamiento").Value = Trim(txtDes.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Insertar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub Actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_escalamiento"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_escalamiento", SqlDbType.NVarChar)
            comando.Parameters("@nom_escalamiento").Value = Trim(txtNombre.Text)
            comando.Parameters.Add("@des_escalamiento", SqlDbType.NVarChar)
            comando.Parameters("@des_escalamiento").Value = Trim(txtDes.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnID.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub Eliminar(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_escalamiento"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Eliminar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtNombre.Text = ""
        txtDes.Text = ""
        hdnActivo.Value = 0
        hdnID.Value = 0
        Call CargarGrilla()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 1 Then
            Call Actualizar()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
        Else
            Call Insertar()
            MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
        End If

        Call Limpiar()
    End Sub

    Protected Sub btnSeleccionar_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)

        txtNombre.Text = CType(row.FindControl("lblNombre"), Label).Text
        txtDes.Text = CType(row.FindControl("lblDes"), Label).Text.Replace("<br/>", Chr(13) + Chr(10))

        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        hdnActivo.Value = 1
    End Sub


    Protected Sub EliminarGrilla(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)

        Call Eliminar(CType(row.FindControl("hdnID"), HiddenField).Value)
        MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
        Call Limpiar()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

End Class

