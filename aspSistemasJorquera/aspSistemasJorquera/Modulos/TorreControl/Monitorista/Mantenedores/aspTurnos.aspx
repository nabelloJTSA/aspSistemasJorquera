﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspTurnos.aspx.vb" Inherits="aspSistemasJorquera.aspTurnos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>


            <!-- Content Header (Page header) -->
            <section class="content-header">       
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="CONFIGURACIÓN DE TURNOS"></asp:Label>
                </h2>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">                                
                            <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <!-- ./box-body -->
                                            <div class="box-footer">
                                                <div class="row table-responsive">
                                                    <div class="col-sm-12 col-xs-6">
                                                        <div class="description-block border-right">
                                                            <asp:GridView ID="grdUsuarios" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="RUT USUARIO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("rut_usuario")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hdnCodUsuario" runat="server" Value='<%# Bind("cod_usuario")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>    

                                                                    <asp:TemplateField HeaderText="NOMBRE USUARIO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCodigo" runat="server" Text='<%# Bind("nom_usuario")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                    <asp:TemplateField HeaderText="CORREO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltransportista" runat="server" Text='<%# Bind("mail_usuario")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton ID="btnTurno" OnClick="btnTurno_Click" runat="server"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="btnTurno" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>  

                                                                </Columns>
                                                                <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                                            </asp:GridView>
                                                            <asp:UpdatePanel runat="server">
                                                                <ContentTemplate>
                                                                    <asp:HiddenField ID="hdnCodigo" runat="server" Value="0" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>

                                                            <!-- MODAL TURNOS -->
                                                            <div class="modal fade" id="modalTurnos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog" style="width:40%">
                                                                    <asp:UpdatePanel runat="server">
                                                                        <ContentTemplate>
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" onclick="$('#modalTurnos').modal('hide');" aria-hidden="true">&times;</button>
                                                                                    <h4 class="modal-title"><asp:Label ID="Label2" runat="server" Text="Creación de Turnos de Trabajo"></asp:Label></h4>
                                                                                </div>
                                                                                <div class="modal-body"> 
                                                                                    <div class="row" style="margin-bottom:5%">
                                                                                        <div class="col-md-2">
                                                                                            <label for="fname" class="control-label col-form-label">Turno</label>
                                                                                            <div>
                                                                                                <asp:DropDownList ID="cboTurnos" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3">
                                                                                            <label for="fname" class="control-label col-form-label">Fecha Inicio</label>
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtInicio" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3">
                                                                                            <label for="fname" class="control-label col-form-label">Fecha Cierre</label>
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtCierre" TextMode="Date" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <label for="fname" class="control-label col-form-label">Hora Inicio</label>
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtHoraInicio" TextMode="Time" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <label for="fname" class="control-label col-form-label">Hora Cierre</label>
                                                                                            <div>
                                                                                                <asp:TextBox ID="txtHoraCierre" TextMode="Time" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                            <asp:GridView ID="grdTurnos" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                                                                <Columns>

                                                                                                    <asp:TemplateField HeaderText="TURNO">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblTurno" runat="server" Text="Turno "><%# Eval("num_turno")%></asp:Label>
                                                                                                            <asp:HiddenField runat="server" ID="hdnIDTurno" Value='<%# Bind("id_turno")%>'/>
                                                                                                            <asp:HiddenField runat="server" ID="hdnEstado" Value='<%# Bind("est_turno")%>'/>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="FECHA INICIO">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblInicio" runat="server" Text='<%# Bind("fec_inicio")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="FECHA CIERRE">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblCierre" runat="server" Text='<%# Bind("fec_cierre")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="HORA INICIO">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblHoraI" runat="server" Text='<%# Bind("hor_inicio")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="HORA CIERRE">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblHoraC" runat="server" Text='<%# Bind("hor_cierre")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:LinkButton ID="btnEstado" OnClick="btnEstado_Click" CssClass='<%# If(Eval("est_turno").ToString() = "1", "small-box-footer text-green", "small-box-footer text-danger")%>' runat="server"> 
                                                                                                                        <i class='<%# If(Eval("est_turno").ToString() = "1","fa fa-toggle-on", "fa fa-toggle-on fa-flip-horizontal")%>'></i>
                                                                                                                    </asp:LinkButton> 
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:AsyncPostBackTrigger ControlID="btnEstado" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server">
                                                                                                                <ContentTemplate> 
                                                                                                                    <asp:LinkButton ID="btnBorrar" OnClick="btnBorrar_Click" runat="server"> 
                                                                                                                        <i class="fa fa-trash text-orange"></i>
                                                                                                                    </asp:LinkButton> 
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:AsyncPostBackTrigger ControlID="btnBorrar"/>
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                                                    </asp:TemplateField>

                                                                                                </Columns>
                                                                                                <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <asp:Button ID="btnGuardar" OnClick="btnGuardar_Click" runat="server" CssClass="btn bg-orange" Text="Guardar" />
                                                                                </div>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>

            </section>
            <!-- /.content -->
</asp:Content>


