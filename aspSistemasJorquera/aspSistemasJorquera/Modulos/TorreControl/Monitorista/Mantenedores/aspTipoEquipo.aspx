﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspTipoEquipo.aspx.vb" Inherits="aspSistemasJorquera.aspTipoEquipo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <script type='text/javascript'>
        function openModal() {
            $('[id*=mdlActualizarGrilla]').modal('show');
        }
    </script>

    <ul class="nav nav-tabs bg-warning">
        <li>
            <asp:LinkButton ID="btn1" CssClass="bg-orange" runat="server"><h5>TIPOS DE EQUIPO</h5></asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btn2" CssClass="bg-warning" runat="server"><h5>TIPO DE EQUIPO / ARRASTRE</h5></asp:LinkButton></li>
    </ul>

    <section class="content">
        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="Tipos De Equipo"></asp:Label>
                </h3>
            </div>
        </div>



        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="vw1" runat="server">
                <div class="row">
                    <!-- lft column -->
                    <div class="col-md-5">

                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title">Mantenedor Tipos de Equipo</h3>
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-9 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Tipo de Equipo</label>
                                        <div>
                                            <asp:TextBox ID="txtNomTipo" CssClass="form-control" Text="" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:Button ID="btnGuardar" CssClass="btn btn-warning btn-block " runat="server" Text="Guardar" />
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <asp:GridView ID="grdTipoEquipo" runat="server" CssClass="table table-bordered with-border table-striped  bg-warning table-hover  " AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Tipo Equipo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("nom_tipo_equipo")%>' />
                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id_tipo_equipo")%>' />

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <h4>
                                                            <asp:LinkButton ID="btnEditarTipo" runat="server" OnClick="EditarTipoClick"><i class="fa fa-edit"></i></asp:LinkButton>
                                                        </h4>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <h4>
                                                            <asp:LinkButton ID="btnEliminarTipo" runat="server" OnClick="EliminarTipoClick"><i class="fa fa-trash"></i></asp:LinkButton>
                                                        </h4>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle BackColor="#FFB738" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-7">
                        <!-- Horizontal Form -->
                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title">Productos por tipo de Equipo</h3>
                            </div>

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Tipo de Equipo</label>
                                        <div>
                                            <asp:DropDownList ID="cbotipoEquipo" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-5 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Productos
                                        </label>
                                        <div>
                                            <asp:DropDownList ID="cboProductos" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:Button ID="brnRelacionar" CssClass="btn btn-warning btn-block " runat="server" Text="Relacionar" />
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <asp:GridView ID="grdRelacion" runat="server" CssClass="table table-bordered with-border table-striped  bg-warning table-hover  " AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Tipo Equipo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("nom_tipo_equipo")%>' />
                                                        <asp:HiddenField ID="hdnIDRelacion" runat="server" Value='<%# Bind("id")%>' />

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Producto">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <h4>
                                                            <asp:LinkButton ID="btnEliminarProducto" runat="server" OnClick="EliminarProductoClick"><i class="fa fa-trash"></i></asp:LinkButton>
                                                        </h4>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle BackColor="#FFB738" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            

            <asp:View ID="vw2" runat="server">
                <div class="row">                                  

                    <div class="col-md-12">
                        <!-- Horizontal Form -->
                        <div class="box box-warning">
                            <div class="box-header with-border bg-warning">
                                <h3 class="box-title"></h3>
                            </div>
                            
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-5 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Tipo de Equipo</label>
                                        <div>
                                            <asp:DropDownList ID="cboTipoEquipoA" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-5 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Arrastre
                                        </label>
                                        <div>
                                            <asp:DropDownList ID="cboArrastre" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:Button ID="btnGuardarA" CssClass="btn btn-warning btn-block " runat="server" Text="Relacionar" />
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <asp:GridView ID="grdArrastre" runat="server" CssClass="table table-bordered with-border table-striped  bg-warning table-hover" AutoGenerateColumns="false">
                                            <Columns>

                                                <asp:TemplateField HeaderText="Tipo Equipo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomTipo" runat="server" Text='<%# Bind("nom_tipo_equipo")%>' />                                                    

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Arrastre">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("nom_patente")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <h4>
                                                            <asp:LinkButton ID="btnEliminarProducto" runat="server" OnClick="EliminarArrastreClick"><i class="fa fa-trash"></i></asp:LinkButton>
                                                        </h4>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" Width="30px" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle BackColor="#FFB738" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </asp:View>

        </asp:MultiView>



    </section>


    <asp:HiddenField ID="hdnIdTipoEquipo" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnActivoTipoEquipo" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnIdRelacion" runat="server" Value="0"></asp:HiddenField>
     <asp:HiddenField ID="hdnPatente" runat="server" Value="0"></asp:HiddenField>


    <div class="modal" id="mdlActualizarGrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">

                <div class="modal-body">

                    <div class="modal-header bg-orange">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title ">
                            <asp:Label ID="lblTitModal" runat="server" Text=""></asp:Label>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <div class="col-md-12 form-group justify-content-center">


                                <label for="fname"
                                    class="control-label col-form-label">
                                    ¿ESTA SEGURO QUE DESEA ELIMINAR ESTE REGISTRO?</label>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">

                    <asp:Button ID="btnConfirmar" CssClass="btn btn-warning " OnClientClick="$('#mdlActualizarGrilla').modal('hide');" runat="server" Visible="false" Text="CONFIRMAR" CausesValidation="False" />

                    <asp:Button ID="btnConfirmarProductos" CssClass="btn btn-warning " OnClientClick="$('#mdlActualizarGrilla').modal('hide');" runat="server" Visible="false" Text="CONFIRMAR" CausesValidation="False" />
                      <asp:Button ID="btnConfirmarArrastre" CssClass="btn btn-warning " OnClientClick="$('#mdlActualizarGrilla').modal('hide');" runat="server" Visible="false" Text="CONFIRMAR" CausesValidation="False" />

                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>



</asp:Content>

