﻿Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspPermisos
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Call CargarUsuarios()
                Call CargarGO()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarUsuarios()
        Try
            Dim strNomTabla As String = "TDIMae_usuarios"
            Dim strSQL As String = "Exec pro_permisos 'CGU'"
            grdUsuarios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdUsuarios.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Permisos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGO()
        Try
            Dim strNomTablaR As String = "TCMae_grupo_operativo"
            Dim strSQLR As String = "Exec pro_permisos 'CGO'"
            checksPermisos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            checksPermisos.DataTextField = "nom_grupo"
            checksPermisos.DataValueField = "cod_grupo"
            checksPermisos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGruposOperativos", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnPermiso_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdUsuarios.Rows(rowIndex)
        hdnCodigo.Value = CType(row.FindControl("hdnCodUsuario"), HiddenField).Value
        Call CargarCheck()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalPermisos", "$('#modalPermisos').modal();", True)
    End Sub

    'GUARDAR PERMISOS
    Protected Sub Guardar()
        Try
            For Each p As ListItem In checksPermisos.Items
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.Connection = conx
                conx.Open()
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_permisos"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "IP"

                comando.Parameters.Add("@cod_usuario", SqlDbType.Int)
                comando.Parameters("@cod_usuario").Value = hdnCodigo.Value

                comando.Parameters.Add("@cod_grupo", SqlDbType.Int)
                comando.Parameters("@cod_grupo").Value = p.Value

                If p.Selected Then
                    comando.Parameters.Add("@est_permiso", SqlDbType.NVarChar)
                    comando.Parameters("@est_permiso").Value = 1
                Else
                    comando.Parameters.Add("@est_permiso", SqlDbType.NVarChar)
                    comando.Parameters("@est_permiso").Value = 0
                End If
                comando.ExecuteNonQuery()
                conx.Close()
            Next
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs)
        Call Guardar()
        MessageBox("Guardar", "Datos guardados correctamente", Page, Master, "S")
        hdnCodigo.Value = 0
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalPermisos", "$('#modalPermisos').modal('hide');", True)
    End Sub
    Private Sub CargarCheck()
        Dim strNomTablaR As String = "TCDet_permisos"
        For Each p As ListItem In checksPermisos.Items
            If CargarEstados(hdnCodigo.Value, p.Value) = 1 Then
                p.Selected = True
            Else
                p.Selected = False
            End If
        Next
    End Sub

    Function CargarEstados(ByVal usuario As Integer, ByVal permiso As Integer) As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strDatos As Integer = 0

        comando.CommandText = "Exec pro_permisos 'CC','" & usuario & "','" & permiso & "'"
        comando.Connection = conx
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            strDatos = rdoReader(0).ToString
        End If

        rdoReader.Close()
        conx.Close()

        Return strDatos
    End Function
End Class