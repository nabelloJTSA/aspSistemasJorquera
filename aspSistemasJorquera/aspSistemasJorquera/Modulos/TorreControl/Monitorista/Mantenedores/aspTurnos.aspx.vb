﻿Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspTurnos
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Call CargarUsuarios()
                Call CargarCBOTurnos()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarUsuarios()
        Try
            Dim strNomTabla As String = "TDIMae_usuarios"
            Dim strSQL As String = "Exec pro_permisos 'CGU'"
            grdUsuarios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdUsuarios.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Permisos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCBOTurnos()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec pro_turnos 'CGO'"
            cboTurnos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTurnos.DataTextField = "nom_turno"
            cboTurnos.DataValueField = "num_turno"
            cboTurnos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTurnos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnTurno_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdUsuarios.Rows(rowIndex)
        hdnCodigo.Value = CType(row.FindControl("hdnCodUsuario"), HiddenField).Value
        Call CargarTurnos()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalTurnos", "$('#modalTurnos').modal();", True)
    End Sub

    'Guardar Turnos
    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs)
        Call InsertarTurno()
        Call CargarTurnos()
        Call Limpiar()
        MessageBox("Guardar", "Registro guardado", Page, Master, "S")
    End Sub
    Private Sub InsertarTurno()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_turnos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IT"

            comando.Parameters.Add("@num_turno", SqlDbType.Int)
            comando.Parameters("@num_turno").Value = cboTurnos.SelectedValue

            comando.Parameters.Add("@cod_usuario", SqlDbType.Int)
            comando.Parameters("@cod_usuario").Value = hdnCodigo.Value

            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = txtInicio.Text

            comando.Parameters.Add("@fec_cierre", SqlDbType.NVarChar)
            comando.Parameters("@fec_cierre").Value = txtCierre.Text

            comando.Parameters.Add("@est_turno", SqlDbType.NVarChar)
            comando.Parameters("@est_turno").Value = 1

            comando.Parameters.Add("@hor_inicio", SqlDbType.NVarChar)
            comando.Parameters("@hor_inicio").Value = txtHoraInicio.Text

            comando.Parameters.Add("@hor_cierre", SqlDbType.NVarChar)
            comando.Parameters("@hor_cierre").Value = txtHoraCierre.Text

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarTurno", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTurnos()
        Try
            Dim strNomTabla As String = "TCDet_turnos"
            Dim strSQL As String = "Exec pro_turnos 'CT', '" & hdnCodigo.Value & "'"
            grdTurnos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTurnos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTurnos", Err, Page, Master)
        End Try
    End Sub
    Private Sub Limpiar()
        cboTurnos.ClearSelection()
        txtInicio.Text = ""
        txtCierre.Text = ""
        txtHoraInicio.Text = ""
        txtHoraCierre.Text = ""
    End Sub

    'Actualizar turnos
    Protected Sub btnBorrar_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdTurnos.Rows(rowIndex)
        Dim id As Integer = CType(row.FindControl("hdnIDTurno"), HiddenField).Value
        Call EliminarTurno(id)
        Call CargarTurnos()
    End Sub
    Private Sub EliminarTurno(ByVal id As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_turnos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "BT"

            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarTurno", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnEstado_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdTurnos.Rows(rowIndex)
        Dim id As Integer = CType(row.FindControl("hdnIDTurno"), HiddenField).Value
        Dim estado As Integer = CType(row.FindControl("hdnEstado"), HiddenField).Value
        If estado = 1 Then
            estado = 0
        Else
            estado = 1
        End If
        Call CambiarEstado(id, estado)
        Call CargarTurnos()
    End Sub
    Private Sub CambiarEstado(ByVal id As Integer, ByVal estado As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_turnos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "CE"

            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id

            comando.Parameters.Add("@est_turno", SqlDbType.Int)
            comando.Parameters("@est_turno").Value = estado

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CambiarEstado", Err, Page, Master)
        End Try
    End Sub
End Class