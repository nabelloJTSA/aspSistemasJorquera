﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb


Public Class aspCategorias
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())
    Private cboEscalamientos As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla()
                Call CargarEscalamientos()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "TCMov_homologacion_alertas"
            Dim strSQL As String = "Exec mant_categorias 'GCA'"
            grdDatos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDatos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEscalamientos()
        Try
            Dim strNomTablaR As String = "TCMae_escalamientos"
            Dim strSQLR As String = "Exec mant_categorias 'CES'"
            cboEscalamiento.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEscalamiento.DataTextField = "nombres"
            cboEscalamiento.DataValueField = "id"
            cboEscalamiento.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEscalamientos", Err, Page, Master)
        End Try
    End Sub



    Protected Sub Insertar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categorias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ICA"
            comando.Parameters.Add("@nom_categoria", SqlDbType.NVarChar)
            comando.Parameters("@nom_categoria").Value = Trim(txtNombre.Text)
            comando.Parameters.Add("@obs_script", SqlDbType.NVarChar)
            comando.Parameters("@obs_script").Value = Trim(txtScript.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@des_categoria", SqlDbType.NVarChar)
            comando.Parameters("@des_categoria").Value = Trim(txtDes.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Insertar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub Actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categorias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UCA"
            comando.Parameters.Add("@obs_script", SqlDbType.NVarChar)
            comando.Parameters("@obs_script").Value = Trim(txtScript.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@des_categoria", SqlDbType.NVarChar)
            comando.Parameters("@des_categoria").Value = Trim(txtDes.Text.Replace(Chr(13) + Chr(10), "<br/>"))
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnID.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtNombre.Text = ""
        txtScript.Text = ""
        txtDes.Text = ""
        hdnActivo.Value = 0
        hdnID.Value = 0
        Call CargarGrilla()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 1 Then
            Call Actualizar()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
        Else
            Call Insertar()
            MessageBox("Insertar", "Registro almacenado", Page, Master, "S")
        End If

        Call Limpiar()
    End Sub

    Protected Sub btnSeleccionar_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)

        lblNomCategoria.Text = CType(row.FindControl("lblNombre"), Label).Text
        txtScript.Text = CType(row.FindControl("lblScript"), Label).Text.Replace("<br/>", Chr(13) + Chr(10))
        txtDes.Text = CType(row.FindControl("lblDes"), Label).Text.Replace("<br/>", Chr(13) + Chr(10))

        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        hdnActivo.Value = 1
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub


    Private Sub CargarGrillaEscalamientos(ByVal id_categoria As String)
        Try
            Dim strNomTabla As String = "TCMov_homologacion_alertas"
            Dim strSQL As String = "Exec mant_categorias 'GCE', '" & id_categoria & "'"
            grdEscalamiento.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdEscalamiento.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEscalamientos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub VerEscalamientosGrilla(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdDatos.Rows(rowIndex)

        lblNomCategoria.Text = CType(row.FindControl("lblNombre"), Label).Text
        Call CargarGrillaEscalamientos(CType(row.FindControl("hdnID"), HiddenField).Value)

        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Private Sub btnGuardarEscalamiento_Click(sender As Object, e As EventArgs) Handles btnGuardarEscalamiento.Click
        Call InsertarEscalamiento()
        Call CargarGrillaEscalamientos(hdnID.Value)
    End Sub

    Protected Sub InsertarEscalamiento()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categorias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ICE"
            comando.Parameters.Add("@id_categoria", SqlDbType.NVarChar)
            comando.Parameters("@id_categoria").Value = hdnID.Value
            comando.Parameters.Add("@id_escalamiento", SqlDbType.NVarChar)
            comando.Parameters("@id_escalamiento").Value = cboEscalamiento.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Insertar", Err, Page, Master)
        End Try
    End Sub


    Protected Sub EliminarEscalamiento(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdEscalamiento.Rows(rowIndex)

        Call EliminarEscalamiento(CType(row.FindControl("hdnID"), HiddenField).Value)
        Call CargarGrillaEscalamientos(hdnID.Value)

    End Sub


    Protected Sub EliminarEscalamiento(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_categorias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ECE"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarEscalamiento", Err, Page, Master)
        End Try
    End Sub


End Class
