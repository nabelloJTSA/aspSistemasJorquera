﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Imports System.Net.Mail

Public Class aspGestionAlerta
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Session("est_asignado_tc") = 0
                lblNomAlerta.Text = Session("tipo_alerta_g_tc")
                lblIdCallCentar.Text = Session("idcallcenter_g_tc")
                btnN1.CssClass = btnN1.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
                hdnNumNivel.Value = 1
                lblDescripcion.Text = Session("descripcion_g_tc")

                lblPatCamion.Text = Session("camion_g_tc")
                lblNumviaje.Text = Session("num_viaje_g_tc")
                lblConductor.Text = Session("conductor_g_tc")
                lblOrigen.Text = Session("origen_g_tc")
                lblDestino.Text = Session("destino_g_tc")
                lblCliente.Text = Session("cliente_g_tc")
                lblScoreViaje.Text = Session("score_viaje_g_tc")
                Session("gestion_fin") = 0

                Call CargarExplicaciones()

                Call CantidadNiveles()
                Call RescatarTotalGestiones()
                Call CargarBitacoraEscalamiento()
                Call CargarGrillaAlertas()
                Call CargarGrillaEstados()
                Call RescatarTiempoGestionado()

                timerSLA.Enabled = False

                Call RescatarNombresNiveles()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaContactos(ByVal tipo As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_alertas 'GCO', '" & tipo & "', '" & lblNumviaje.Text & "'"
            grdContactos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdContactos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaContactos", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'CGA2', '" & Session("num_viaje_g_tc") & "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaEstados()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CGE', '" & Session("num_viaje_g_tc") & "'"
            grdEstados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdEstados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEstados", Err, Page, Master)
        End Try
    End Sub

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(Session("latitud_alerta_tc"), ",", ".") & "" & "," & Replace(Session("longitud_alerta_tc"), ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>N° Viaje: </b> " & Session("num_viaje_g_tc") & "<br /><b>Camión: </b> " & Session("camion_g_tc") & " <br /><b>Conductor: </b> " & Session("conductor_g_tc") & "<br /><b>Tipo Alerta: </b> " & (Session("tipo_alerta_g_tc").ToString.ToUpper) & "<b>"
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    Protected Sub btnN1_Click(sender As Object, e As EventArgs) Handles btnN1.Click
        hdnNumNivel.Value = 1
        Call RescatarIdNivel(hdnNumNivel.Value)

        lblTituloNivel.Text = "NIVEL 1"
        btnN1.CssClass = btnN1.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
    End Sub

    Protected Sub btnN2_Click(sender As Object, e As EventArgs) Handles btnN2.Click
        hdnNumNivel.Value = 2
        Call RescatarIdNivel(hdnNumNivel.Value)

        lblTituloNivel.Text = "NIVEL 2"
        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN2.CssClass = btnN2.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
    End Sub

    Protected Sub btnN3_Click(sender As Object, e As EventArgs) Handles btnN3.Click
        hdnNumNivel.Value = 3
        Call RescatarIdNivel(hdnNumNivel.Value)


        lblTituloNivel.Text = "NIVEL 3"
        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN3.CssClass = btnN3.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")

    End Sub

    Protected Sub btnN4_Click(sender As Object, e As EventArgs) Handles btnN4.Click
        hdnNumNivel.Value = 4
        Call RescatarIdNivel(hdnNumNivel.Value)

        lblTituloNivel.Text = "NIVEL 4"
        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN4.CssClass = btnN4.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
    End Sub

    Protected Sub btnN5_Click(sender As Object, e As EventArgs) Handles btnN5.Click
        hdnNumNivel.Value = 5
        Call RescatarIdNivel(hdnNumNivel.Value)

        lblTituloNivel.Text = "NIVEL 5"
        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
        btnN5.CssClass = btnN5.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
    End Sub


    Private Sub RescatarScripts(ByVal contacto As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'SCR', '" & hdnNumNivel.Value & "', '" & Session("id_alerta_g_tc") & "', '" & contacto & "', '" & Session("nom_usu_tc") & "', '" & lblPatCamion.Text & "', '" & Session("tipo_alerta_g_tc") & "', '" & Session("idcallcenter_g_tc") & "', '" & hdnIDNivel.Value & "', '" & lblNumviaje.Text & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblScript.Text = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("RescatarScripts", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub RescatarIdNivel(ByVal num_nivel As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'RIN', '" & Session("id_alerta_g_tc") & "', '" & num_nivel & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnNomRol.Value = rdoReader(1).ToString
                    hdnIDNivel.Value = rdoReader(0).ToString

                    ' Call CargarContactos(rdoReader(0).ToString)
                    Call CargarGrillaContactos(rdoReader(0).ToString)
                    Call VerificarEnvioCorreo(rdoReader(0).ToString)
                End If
            Catch ex As Exception
                MessageBoxError("RescatarIdNivel", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarEnvioCorreo(ByVal num_nivel As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'ENC', '" & num_nivel & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnEnvioCorreo.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("VerificarEnvioCorreo", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarContactoContactado(ByVal contacto As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'VCC',  '" & hdnIDNivel.Value & "', '" & Session("num_viaje_g_tc") & "', '" & contacto & "', '" & lblIdCallCentar.Text & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnContactado.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("VeriricarContactoContactado", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarNivelOpcional()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'VNO',  '" & hdnIDNivel.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnNivelObligatorio.Value = rdoReader(0).ToString ' verrrrrrrrrrr
                End If
            Catch ex As Exception
                MessageBoxError("VerificarNivelOpcional", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CantidadNiveles()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'CNI', '" & Session("id_alerta_g_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    hdnCantidadNiveles.Value = rdoReader(0).ToString
                    If rdoReader(0).ToString = 1 Then
                        btnN1.Visible = True
                        btnN2.Visible = False
                        btnN3.Visible = False
                        btnN4.Visible = False
                        btnN5.Visible = False
                    ElseIf rdoReader(0).ToString = 2 Then
                        btnN1.Visible = True
                        btnN2.Visible = True
                        btnN3.Visible = False
                        btnN4.Visible = False
                        btnN5.Visible = False
                    ElseIf rdoReader(0).ToString = 3 Then
                        btnN1.Visible = True
                        btnN2.Visible = True
                        btnN3.Visible = True
                        btnN4.Visible = False
                        btnN5.Visible = False
                    ElseIf rdoReader(0).ToString = 4 Then
                        btnN1.Visible = True
                        btnN2.Visible = True
                        btnN3.Visible = True
                        btnN4.Visible = True
                        btnN5.Visible = False
                    ElseIf rdoReader(0).ToString = 5 Then
                        btnN1.Visible = True
                        btnN2.Visible = True
                        btnN3.Visible = True
                        btnN4.Visible = True
                        btnN5.Visible = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("CantidadNiveles", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub CargarExplicaciones()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_alertas 'EXP'"
            cboExplicacion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboExplicacion.DataTextField = "nom_explicacion"
            cboExplicacion.DataValueField = "id_explicacion"
            cboExplicacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarExplicaciones", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnContactarClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdContactos.Rows(rowIndex)

        hdnContacto.Value = CType(row.FindControl("hdnIdContacto"), HiddenField).Value
        hdnNomContacto.Value = CType(row.FindControl("lblNomContacto"), Label).Text

        ' Call RescatarInfoContactos(hdnContacto.Value)

        lblCorreo.Text = CType(row.FindControl("hdnCorreo"), HiddenField).Value
        lblFono.Text = CType(row.FindControl("hdnFono"), HiddenField).Value

        Call RescatarScripts(CType(row.FindControl("lblNomContacto"), Label).Text)
        lblTitExplicacion.Visible = True
        cboExplicacion.Visible = True
        txtObs.Visible = True

        chkContactado.Visible = True

        If hdnCantidadNiveles.Value = hdnNumNivel.Value Then
            btnGestion.Visible = False
            btnFinGestion.Visible = True
        Else
            If hdnNivelObligatorio.Value = 1 Then
                btnGestion.Visible = True
                btnFinGestion.Visible = False
            Else
                btnGestion.Visible = True
                btnFinGestion.Visible = True
            End If
            btnGestion.Enabled = True
        End If

        timerSLA.Enabled = True

    End Sub



    Protected Sub FinalizarGestion()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_alertas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UFI"
            comando.Parameters.Add("@id_call_center", SqlDbType.NVarChar)
            comando.Parameters("@id_call_center").Value = Session("idcallcenter_g_tc")
            comando.Parameters.Add("@usr_gestion", SqlDbType.NVarChar)
            comando.Parameters("@usr_gestion").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@num_tiempo_gestion", SqlDbType.NVarChar)
            comando.Parameters("@num_tiempo_gestion").Value = lbl3.Text & ":" & lbl2.Text & ":" & lbl1.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            btnFinGestion.Visible = False
            timerSLA.Enabled = False
            cboExplicacion.Visible = False
            grdContactos.Enabled = False
            txtObs.Visible = False
            lblScript.Visible = False
            lblCorreo.Visible = False
            lblFono.Visible = False
            lblTitExplicacion.Visible = False


            Session("gestion_fin") = 1

        Catch ex As Exception
            MessageBoxError("FinalizarGestion", Err, Page, Master)
        End Try
    End Sub



    Protected Sub InsertarGestion(ByVal contacto As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_alertas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IGE"
            comando.Parameters.Add("@id_call_center", SqlDbType.NVarChar)
            comando.Parameters("@id_call_center").Value = Session("idcallcenter_g_tc")
            comando.Parameters.Add("@id_nivel", SqlDbType.NVarChar)
            comando.Parameters("@id_nivel").Value = hdnIDNivel.Value
            comando.Parameters.Add("@nro_nivel", SqlDbType.NVarChar)
            comando.Parameters("@nro_nivel").Value = hdnNumNivel.Value
            comando.Parameters.Add("@id_contacto", SqlDbType.NVarChar)
            comando.Parameters("@id_contacto").Value = contacto
            comando.Parameters.Add("@id_explicacion_alerta", SqlDbType.NVarChar)
            comando.Parameters("@id_explicacion_alerta").Value = cboExplicacion.SelectedValue
            comando.Parameters.Add("@num_tiempo", SqlDbType.NVarChar)
            comando.Parameters("@num_tiempo").Value = lbl3.Text & ":" & lbl2.Text & ":" & lbl1.Text
            comando.Parameters.Add("@est_gestion", SqlDbType.NVarChar)
            comando.Parameters("@est_gestion").Value = 1
            comando.Parameters.Add("@usr_gestion", SqlDbType.NVarChar)
            comando.Parameters("@usr_gestion").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@obs_gestion", SqlDbType.NVarChar)
            comando.Parameters("@obs_gestion").Value = Trim(txtObs.Text)
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = lblNumviaje.Text
            comando.Parameters.Add("@nom_contacto", SqlDbType.NVarChar)
            comando.Parameters("@nom_contacto").Value = hdnNomContacto.Value
            comando.Parameters.Add("@nom_correo", SqlDbType.NVarChar)
            comando.Parameters("@nom_correo").Value = Trim(lblCorreo.Text)
            comando.Parameters.Add("@est_contactado", SqlDbType.Bit)
            comando.Parameters("@est_contactado").Value = chkContactado.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            If Trim(lblCorreo.Text) <> "" Then
                If hdnEnvioCorreo.Value = 1 Then
                    Call EnviarCorreoScript(Trim(lblCorreo.Text), Trim(lblScript.Text), lblNomAlerta.Text)
                End If
            End If

            Call CargarBitacoraEscalamiento()
        Catch ex As Exception
            MessageBoxError("InsertarGestion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnConfirmarFinalizar_Click(sender As Object, e As EventArgs) Handles btnConfirmarFinalizar.Click
        If hdnContacto.Value <> 0 Then
            Call InsertarGestion(hdnContacto.Value)
        End If

        txtObs.Visible = False
        chkContactado.Visible = False
        btnGestion.Visible = False
        btnFinGestion.Visible = False
        Call CorreoTerminoTI()
        Call EnviarCorreoTermino()
        Call FinalizarGestion()
        MessageBox("Gestion ALertas", "Alerta Gestionada", Page, Master, "S")
    End Sub

    Protected Sub btnConfirnarVolver_Click(sender As Object, e As EventArgs) Handles btnConfirnarVolver.Click
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx")
    End Sub

    Private Sub EnviarCorreoScript(ByVal mail As String, ByVal comentarios As String, ByVal asunto As String)
        Try
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add(mail)
            correo.Subject = "[TC] ALERTA DE: " & asunto
            correo.Body = comentarios
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("EnviarCorreoDoctos", Err, Page, Master)
        End Try
    End Sub

    Public Function GetGridviewData(gv As GridView) As String
        Dim strBuilder As New StringBuilder()
        Dim strWriter As New StringWriter(strBuilder)
        Dim htw As New HtmlTextWriter(strWriter)
        gv.RenderControl(htw)
        Return strBuilder.ToString()
    End Function

    Private Sub CorreoTerminoTI()
        Dim grdResumen As New GridView
        grdResumen.CellPadding = 5
        grdResumen.CellSpacing = 5
        grdResumen.BorderStyle = BorderStyle.None
        grdResumen.Font.Size = 10

        Dim strSQL As String = "Exec pro_gestion_alertas 'CBE', '" & Session("num_viaje_g_tc") & "', '" & Session("idcallcenter_g_tc") & "'"
        Dim strNomTabla As String = "VJSMov_planificacion"
        grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
        grdResumen.DataBind()

        Dim correo As New MailMessage
        correo.From = New MailAddress("sistemas@viatrack.cl")
        correo.To.Add("paola.escobar@jtsa.cl, hector.salcedo@jtsa.cl, miguel.beltran@jtsa.cl")
        ' correo.To.Add("nestor.abello@jtsa.cl")
        correo.Subject = "[TC] GESTION DE:  " & lblNomAlerta.Text & ", VIAJE: " & lblNumviaje.Text & ""
        correo.Body = "<h3><B>GESTION ALERTA:</B> " & lblNomAlerta.Text & " / <b>ID: </b> " & lblIdCallCentar.Text & "  <BR /><BR /></h3>
                                      <p>" & lblDescripcion.Text & "</p><br />
                                       <h4><b>VIAJE:</b> " & lblNumviaje.Text & " /  <b>PATENTE:</b> " & lblPatCamion.Text & " /  <b>CONDUCTOR:</b> " & lblConductor.Text & " / <b>ORIGEN:</b> " & lblOrigen.Text & " / <b>DESTINO:</b> " & lblDestino.Text & " / <b>CLIENTE:</b> " & lblCliente.Text & " </h4> 
                                      <BR /> 
                            Estimado (a), se adjunta bitacora de escalamientos: <br/><br/>" & GetGridviewData(grdResumen) & ""
        correo.IsBodyHtml = True
        correo.Priority = MailPriority.Normal
        Dim smtp As New SmtpClient
        smtp.Host = "mail.viatrack.cl"
        smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
        smtp.Send(correo)
        correo.Attachments.Clear()
        correo.Dispose()
    End Sub

    Private Sub EnviarCorreoTermino()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_gestion_alertas 'CTG', '" & lblIdCallCentar.Text & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    If rdoReader(0).ToString <> "" Then
                        Dim grdResumen As New GridView
                        grdResumen.CellPadding = 5
                        grdResumen.CellSpacing = 5
                        grdResumen.BorderStyle = BorderStyle.None
                        grdResumen.Font.Size = 10

                        strSQL = "Exec pro_gestion_alertas 'CBE', '" & Session("num_viaje_g_tc") & "', '" & Session("idcallcenter_g_tc") & "'"
                        Dim strNomTabla As String = "VJSMov_planificacion"
                        grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                        grdResumen.DataBind()


                        Dim correo As New MailMessage
                        correo.From = New MailAddress("sistemas@viatrack.cl")
                        correo.To.Add(rdoReader(0).ToString)
                        correo.Subject = "[TC] GESTION DE:  " & lblNomAlerta.Text & ", VIAJE: " & lblNumviaje.Text & ""
                        correo.Body = "<h3><B>GESTION ALERTA:</B> " & lblNomAlerta.Text & " / <b>ID: </b> " & lblIdCallCentar.Text & " 
                                      " & lblDescripcion.Text & "</h4><br/></h3>
                                       <h4><b>VIAJE:</b> " & lblNumviaje.Text & " /  <b>PATENTE:</b> " & lblPatCamion.Text & " /  <b>CONDUCTOR:</b> " & lblConductor.Text & " / <b>ORIGEN:</b> " & lblOrigen.Text & " / <b>DESTINO:</b> " & lblDestino.Text & " / <b>CLIENTE:</b> " & lblCliente.Text & " </h4> 
                                      
                            Estimado (a), se adjunta bitacora de escalamientos: <br/><br/>" & GetGridviewData(grdResumen) & ""
                        correo.IsBodyHtml = True
                        correo.Priority = MailPriority.Normal
                        Dim smtp As New SmtpClient
                        smtp.Host = "mail.viatrack.cl"
                        smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
                        smtp.Send(correo)
                        correo.Attachments.Clear()
                        correo.Dispose()
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("EnviarCorreoTermino", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub



    Protected Sub btnGestion_Click(sender As Object, e As EventArgs) Handles btnGestion.Click


        If hdnContacto.Value = "0" Then
            MessageBox("Gestionar Alerta", "Seleccione Contacto", Page, Master, "W")
        Else
            Call VerificarContactoContactado(hdnContacto.Value)

            If hdnContactado.Value = 0 Then
                Call InsertarGestion(hdnContacto.Value)
                txtObs.Text = ""
                Call RescatarTotalGestiones()
            Else
                MessageBox("Gestionar Alerta", "Contacto ya Gestionado", Page, Master, "W")
            End If
        End If


    End Sub


    Private Sub CargarBitacoraEscalamiento()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CBE', '" & Session("num_viaje_g_tc") & "', '" & Session("idcallcenter_g_tc") & "'"
            grdEscalamientos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdEscalamientos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarBitacoraEscalamiento", Err, Page, Master)
        End Try
    End Sub


    Private Sub RescatarTiempoGestionado()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'VTG', '" & Session("idcallcenter_g_tc") & "', '" & Session("num_viaje_g_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lbl2.Text = rdoReader(0).ToString
                    lbl1.Text = rdoReader(1).ToString
                End If
            Catch ex As Exception
                MessageBoxError("RescatarTiempoGestionado", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub RescatarTotalGestiones()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'RUG', '" & Session("idcallcenter_g_tc") & "', '" & Session("num_viaje_g_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    If rdoReader(0).ToString = 0 Then

                        lblTituloNivel.Text = "NIVEL 1"
                        btnN1.CssClass = btnN1.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
                        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")

                        hdnNumNivel.Value = 1

                        btnN1.Enabled = True
                        btnN2.Enabled = False
                        btnN3.Enabled = False
                        btnN4.Enabled = False
                        btnN5.Enabled = False


                    ElseIf rdoReader(0).ToString = 1 Then
                        lblTituloNivel.Text = "NIVEL 2"
                        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN2.CssClass = btnN2.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
                        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")

                        hdnNumNivel.Value = 2
                        btnN1.Enabled = False
                        btnN2.Enabled = True
                        btnN3.Enabled = False
                        btnN4.Enabled = False
                        btnN5.Enabled = False

                    ElseIf rdoReader(0).ToString = 2 Then

                        lblTituloNivel.Text = "NIVEL 3"
                        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN3.CssClass = btnN3.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
                        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")

                        hdnNumNivel.Value = 3
                        btnN1.Enabled = False
                        btnN2.Enabled = False
                        btnN3.Enabled = True
                        btnN4.Enabled = False
                        btnN5.Enabled = False

                    ElseIf rdoReader(0).ToString = 3 Then

                        lblTituloNivel.Text = "NIVEL 4"
                        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN4.CssClass = btnN4.CssClass.Replace("bg-primary text-center", "bg-warning text-center")
                        btnN5.CssClass = btnN5.CssClass.Replace("bg-warning text-center", "bg-primary text-center")

                        hdnNumNivel.Value = 4
                        btnN1.Enabled = False
                        btnN2.Enabled = False
                        btnN3.Enabled = False
                        btnN4.Enabled = True
                        btnN5.Enabled = False

                    ElseIf rdoReader(0).ToString = 4 Then

                        lblTituloNivel.Text = "NIVEL 5"
                        btnN1.CssClass = btnN1.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN2.CssClass = btnN2.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN3.CssClass = btnN3.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN4.CssClass = btnN4.CssClass.Replace("bg-warning text-center", "bg-primary text-center")
                        btnN5.CssClass = btnN5.CssClass.Replace("bg-primary text-center", "bg-warning text-center")

                        hdnNumNivel.Value = 5
                        btnN1.Enabled = False
                        btnN2.Enabled = False
                        btnN3.Enabled = False
                        btnN4.Enabled = False
                        btnN5.Enabled = True

                    End If
                    btnGestion.Visible = False
                    chkContactado.Visible = False
                    chkContactado.Checked = False
                    lblCorreo.Text = ""
                    lblFono.Text = ""
                    lblScript.Text = ""
                    lblTitExplicacion.Visible = False
                    txtObs.Visible = False
                    cboExplicacion.Visible = False
                    cboExplicacion.ClearSelection()
                    Call RescatarIdNivel(rdoReader(0).ToString + 1)

                    Call VerificarNivelOpcional()


                    If hdnCantidadNiveles.Value = hdnNumNivel.Value Or hdnNivelObligatorio.Value = 0 Then
                        btnFinGestion.Visible = True
                    ElseIf hdnNivelObligatorio.Value = 1 Then
                        btnFinGestion.Visible = False
                    End If




                End If
            Catch ex As Exception
                MessageBoxError("RescatarTotalGestiones", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub timerSLA_Tick(sender As Object, e As EventArgs) Handles timerSLA.Tick
        lbl1.Text = lbl1.Text + 1
        If Val(lbl1.Text) > 59 Then
            lbl1.Text = 0
            lbl2.Text = lbl2.Text + 1
            If Val(lbl2.Text) > 59 Then
                lbl2.Text = 0
                lbl3.Text = lbl3.Text + 1
            End If
        End If
    End Sub

    Private Sub RescatarNombresNiveles()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_alertas 'NOM', '" & Session("id_alerta_g_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                Do While (rdoReader.Read())
                    If rdoReader(0).ToString = 1 Then
                        lblN1.Text = rdoReader(1).ToString
                    ElseIf rdoReader(0).ToString = 2 Then
                        lblN2.Text = rdoReader(1).ToString
                    ElseIf rdoReader(0).ToString = 3 Then
                        lblN3.Text = rdoReader(1).ToString
                    ElseIf rdoReader(0).ToString = 4 Then
                        lblN4.Text = rdoReader(1).ToString
                    ElseIf rdoReader(0).ToString = 5 Then
                        lblN5.Text = rdoReader(1).ToString
                    End If
                Loop
            Catch ex As Exception
                MessageBoxError("RescatarNombresNiveles", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    'Protected Sub chkContactado_CheckedChanged(sender As Object, e As EventArgs) Handles chkContactado.CheckedChanged
    '    If hdnNivelObligatorio.Value = 1 Then
    '        btnFinGestion.Visible = False
    '    Else
    '        If hdnCantidadNiveles.Value <> hdnNumNivel.Value Then

    '            If chkContactado.Checked = True Then
    '                btnGestion.Visible = True
    '                btnFinGestion.Visible = True
    '            Else
    '                btnGestion.Visible = True
    '                btnFinGestion.Visible = False
    '            End If
    '        End If
    '    End If

    'End Sub
End Class