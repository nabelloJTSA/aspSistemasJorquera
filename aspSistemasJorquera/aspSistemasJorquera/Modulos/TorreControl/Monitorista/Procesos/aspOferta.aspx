﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspOferta.aspx.vb" Inherits="aspSistemasJorquera.aspOferta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    
    <script type='text/javascript'>
        function openModal() {
            $('[id*=ModalDocumentos]').modal('show');
        }
    </script>


    <script type='text/javascript'>
        function openModalACT() {
            $('[id*=mdlActualizarGrilla]').modal('show');
        }
    </script>


    <style type="text/css">
        #global {
            height: 850px;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes {
            height: auto;
        }

        #mensajes2 {
            height: auto;
        }

        .texto {
            padding: 4px;
            background: #fff;
        }
    </style>



    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-4 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="OFERTA"></asp:Label>
                    <small></small>
                </h3>
            </div>
        </div>

        <!-- SELECT2 EXAMPLE -->

        <div class="row btn-sm">

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Servicio</label>
                <div>
                    <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Grupo Operativo</label>
                <div>
                    <asp:DropDownList ID="cboGrupoOperativo" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>



            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Trasnportista</label>
                <div>

                    <asp:DropDownList ID="cboTrasnportista" runat="server" AutoPostBack="true"  CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="col-md-3 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Conductor</label>
                <div>
                                  
                    <asp:DropDownList ID="cboConductores" runat="server" AutoPostBack="true"  CssClass="form-control">
                    </asp:DropDownList>                
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <asp:Button ID="btnLimpiarFiltros" runat="server" CssClass="btn btn-primary" Text="Limpiar Filtros" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 form-group  ">
                <div>
                    <h4>
                        <asp:Label ID="Label2" CssClass="text-primary" runat="server" Text="TOTAL REGISTROS: "></asp:Label>
                        <b>
                            <asp:Label ID="lblTotReg" CssClass="text-primary" runat="server" Text=""></asp:Label></b></h4>
                </div>
            </div>


            <div class="col-md-1 form-group  pull-right">
                <div>
                    <asp:LinkButton ID="btnExportar" CssClass="btn btn-block botonesTC-descargar"  runat="server"> <i class="fa fa-download"></i> Tabla</asp:LinkButton>
                </div>
            </div>
        </div>
        


        <div class="row">
            <div class="col-md-12">

                 <div class="row">
                    <div class="col-md-12 form-group">
                        <asp:Label ID="Label1" runat="server" ForeColor="#00cc00" Font-Bold="true" Text="GPS BUENO/DOCTOS AL DIA"></asp:Label>
                        <asp:Label ID="Label4" runat="server" ForeColor="#ff0000" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;    GPS SEÑAL TARDIA/DOCTOS VENCIDOS"></asp:Label>
                         <asp:Label ID="Label5" runat="server" ForeColor="#999999" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;   SIN GPS"></asp:Label>
                        <asp:Label ID="Label32" runat="server" ForeColor="#ff9900" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;   DOCTOS POR VENCER"></asp:Label>
                    </div>
                </div>

                <div class="row btn-sm table-responsive">

                    <div class="col-md-12">

                        <div style="width: 100%; overflow: scroll; height: 813px;">

                            <asp:GridView ID="grdRecursos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                <Columns>

                                    <asp:TemplateField HeaderText="CAMIÓN">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="btnVerDocCamion" CssClass="text-green" OnClick="btnVerDocCamion_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                            <br />
                                            <asp:Label ID="lblCamion" Font-Bold="true" runat="server" Text='<%# Bind("nom_patente")%>' />
                                           <br />
                                            <b>
                                                <asp:Label ID="lblGPS" runat="server" Text="GPS: " /></b>
                                            <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                            <br />
                                            <b>
                                                <asp:Label ID="Label3" runat="server" Text="DOCTOS: " /></b>
                                            <asp:ImageButton ID="imgBtnCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="COD. CAMIÓN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodCamion" runat="server" Text='<%# Bind("cod_vehiculo")%>' />

                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                            <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps")%>' />
                                            <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("es_gps_arrastre")%>' />

                                            <asp:HiddenField ID="hdnFecVecConductor" runat="server" Value='<%# Bind("fec_venc_conductor")%>' />
                                            <asp:HiddenField ID="hdnVecVencCamion" runat="server" Value='<%# Bind("fec_venc_camion")%>' />
                                            <asp:HiddenField ID="hdnFecVecnArrastre" runat="server" Value='<%# Bind("fec_venc_arrastre")%>' />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ARRASTRE">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="btnVerDocArrastre" CssClass="text-green" OnClick="btnVerDocArrastre_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                            <br />

                                           <b> <asp:LinkButton ID="btnArrastre" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="ActArrastreClickG" Text='<%# Bind("pat_arrastre")%>'> </asp:LinkButton></b>                                           
                                            <br />
                                            <b>
                                                <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS: " /></b>
                                            <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                            <br />
                                            <b>
                                                <asp:Label ID="Label31" runat="server" Text="DOCTOS: " /></b>
                                            <asp:ImageButton ID="imgBotonArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="CONDUCTOR">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="btnVerDocConductor" CssClass="text-green" OnClick="btnVerDocConductor_ClickG" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                            <br />
                                            <b><asp:LinkButton ID="btnRutConductor" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="btnActRecursosConductor" Text='<%# Bind("rut_conductor")%>'> </asp:LinkButton></b>
                                            <br />                                           
                                            <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                            <br />
                                           
                                            <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' />
                                           <br />
                                            <b>
                                                <asp:Label ID="Label32" runat="server" Text="DOCTOS: " /></b>
                                            <asp:ImageButton ID="imgBtnConductor" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="TRASNPORTISTA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTrasnportista" runat="server" Text='<%# Bind("nom_transportista")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="GRUPO OPERATIVO">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnGO" Enabled="false" runat="server" OnClick="btnGOClickG" Text='<%# Bind("nom_grupo_operativo")%>'> </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                               <%--     <asp:TemplateField HeaderText="FECHA DISPONIBLE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecDisponible" runat="server" Text='<%# Bind("fec_disponible")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>--%>


<%--
                                    <asp:TemplateField HeaderText="BLOQUEADO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBloqueo" runat="server" Text='<%# Bind("est_bloqueo")%>' ToolTip="BLOQUEADO" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="DISPONIBLE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisponible" runat="server" Text='<%# Bind("est_disponible")%>' ToolTip="DISPONIBLE" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>--%>



                                    <asp:TemplateField HeaderText="DOCTO. CAMION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoctoCamion" runat="server" Text='<%# Bind("est_docto_camion")%>' ToolTip="DOCTOS. CAMION" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="DOCTO. ARRASTRE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoctoarrastre" runat="server" Text='<%# Bind("est_docto_arrastre")%>' ToolTip="DOCTOS. ARRASTRE" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="DOCTO. CONDUCTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDoctoConductor" runat="server" Text='<%# Bind("est_docto_conductor")%>' ToolTip="DOCTOS. CONDUCTOR" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CAMION TALLER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTallerCamion" runat="server" Text='<%# Bind("est_taller_camion")%>' ToolTip="CAMION TALLER" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="AUSENCIA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAusencia" runat="server" Text='<%# Bind("ausencia_conductor")%>' ToolTip="AUSENCIA CONDUCTOR" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="TIPO EQUIPO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoEquipo" runat="server" Text='<%# Bind("tipo_equipo")%>' ToolTip="TIPO EQUIPO" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <%--  <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <h4>
                                                <asp:LinkButton ID="btnSiniestrado" runat="server" CssClass="btn btn-warning btn-sm" OnClick="CCC" ToolTip="">SINIESTRADO</asp:LinkButton>
                                            </h4>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>--%>


                                 <%--   <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <h4>
                                                <asp:LinkButton ID="btnDisponible" runat="server" CssClass="btn bg-orange btn-sm" OnClick="btnDisponibleClick" ToolTip="">DISPONIBILIZAR</asp:LinkButton>
                                            </h4>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <h4>
                                                <asp:LinkButton ID="btnBloquear" runat="server" CssClass="btn btn-danger btn-sm" OnClick="btnBloquearClick" ToolTip="">RESERVAR</asp:LinkButton>
                                            </h4>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>--%>

                                </Columns>

                             <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                            </asp:GridView>
                        </div>
                    </div>


                    <!-- Modal CONFIRMACION -->
                    <div class="modal" id="mdlActualizarGrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <asp:UpdatePanel runat="server" ID="updClientes">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnConfirmar" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <div class="modal-header bg-success">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title ">
                                                    <asp:Label ID="lblTituloActualizar" runat="server" Text=""></asp:Label>
                                                </h4>
                                            </div>
                                            <div class="modal-body">

                                                <div class="row justify-content-center">
                                                    <div class="col-md-12 form-group justify-content-center">

                                                        <asp:Panel ID="pnlGO" runat="server" Visible="false">
                                                            <div class="modal-body">
                                                                <label for="fname"
                                                                    class="control-label col-form-label">
                                                                    SELECCIONAR GRUPO OPERATIVO</label>
                                                                <asp:DropDownList ID="cboGO" CssClass="form-control" runat="server"></asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>


                                                        <asp:Panel ID="pnlArrastre" runat="server" Visible="false">
                                                            <div class="row">
                                                                <div class="col-md-4 form-group">
                                                                    <label for="fname"
                                                                        class="control-label col-form-label">
                                                                        SELECCIONE ARRASTRE</label>
                                                                    <%--<asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>
                                                                    <div>
                                                                        <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." CssClass="form-control" Enabled="true"></asp:TextBox>

                                                                        <label class="text-info">Estado GPS: </label>
                                                                        <asp:Image ID="imgGPSArrastre" runat="server" Width="15px" Height="15px" Visible="false" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>



                                                        <asp:Panel ID="pnlConductor" runat="server" Visible="false">
                                                            <div class="row">

                                                                <div class="col-md-3 form-group">
                                                                    <label for="fname"
                                                                        class="control-label col-form-label">
                                                                        RUT</label>
                                                                    <div>
                                                                        <asp:TextBox ID="txtRutconductor" runat="server" placeholder="RUT..." AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-6 form-group">
                                                                    <label for="fname"
                                                                        class="control-label col-form-label">
                                                                        Nombre Conductor</label>
                                                                    <%--<asp:LinkButton ID="btnVerDocConductor" CssClass="text-orange" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>--%>
                                                                    <div>
                                                                        <asp:TextBox ID="txtNomconductor" runat="server" placeholder="Nombre..." Enabled="false" CssClass="form-control"></asp:TextBox>

                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3 form-group">
                                                                    <label for="fname"
                                                                        class="control-label col-form-label">
                                                                        Fono</label>
                                                                    <div>
                                                                        <asp:TextBox ID="txtFono" runat="server" placeholder="Fono..." Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                        <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>

                                                    </div>
                                                </div>


                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="modal-footer">

                                    <asp:Button ID="btnConfirmar" CssClass="btn btn-primery " OnClientClick="$('#modalClientes').modal('hide');" runat="server" Visible="true" Text="ACTUALIZAR" />
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--fin modal--%>



                    <div>
                        <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">

                                    <asp:Panel ID="pnlDoctos" runat="server" Visible="false">
                                        <div class="modal-header bg-success">
                                            <h3 class="modal-title text-light">
                                                <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label><asp:Label ID="lblIdentificador" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                            <h3>
                                                <asp:Label ID="lblTrasnportista" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label></h3>
                                            <button type="button" class="close" data-dismiss="modal">
                                                &times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="DOCUMENTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("nom_docto") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="VENCIMIENTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("fec_vencimiento", "{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                 <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <asp:HiddenField ID="hdnID" runat="server" />

</asp:Content>



