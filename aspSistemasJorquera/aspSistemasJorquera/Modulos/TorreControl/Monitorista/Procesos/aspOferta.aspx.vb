﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Imports System.Net.Mail

Public Class aspOferta
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Call ComboGO()
                Call ComboServicios()
                Call CargarTrasnportistas()
                Call CargarConductores()
                Call CargarGrillaRecursos()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub ComboGO()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_conf_recursos 'GRU', '" & Session("cod_usu_tc") & "'"
            cboGrupoOperativo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboGrupoOperativo.DataTextField = "nom_grupo_operativo"
            cboGrupoOperativo.DataValueField = "cod_grupo_operativo"
            cboGrupoOperativo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGO", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarTrasnportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_conf_recursos 'CTR', '" & Session("cod_usu_tc") & "'"
            cboTrasnportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasnportista.DataTextField = "nom_transportista"
            cboTrasnportista.DataValueField = "rut_transportista"
            cboTrasnportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTrasnportistas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarConductores()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_conf_recursos 'CCH', '" & Session("cod_usu_tc") & "', '" & cboTrasnportista.SelectedValue & "'"
            cboConductores.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductores.DataTextField = "nom_conductor"
            cboConductores.DataValueField = "cod_conductor"
            cboConductores.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarConductores", Err, Page, Master)
        End Try
    End Sub


    Private Sub ComboServicios()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_conf_recursos 'CCS', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("ComboServicios", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaRecursos()
        Try
            Dim strNomTabla As String = "TCMae_vehiculos"
            Dim strSQL As String = "Exec pro_conf_recursos 'CGR', '" & cboGrupoOperativo.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & Trim(cboTrasnportista.SelectedValue) & "', '" & Trim(cboConductores.SelectedValue) & "', '" & Session("cod_usu_tc") & "'"

            grdRecursos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdRecursos.DataBind()

            lblTotReg.Text = grdRecursos.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaRecursos", Err, Page, Master)
        End Try
    End Sub



    Protected Sub btnVerDocCamion_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)
        pnlDoctos.Visible = True
        pnlGO.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("lblCamion"), Label).Text, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("lblCamion"), Label).Text, "-", "")) & "/ " & CType(row.FindControl("lblCodCamion"), Label).Text

        lblTrasnportista.Text = CType(row.FindControl("lblTrasnportista"), Label).Text

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocArrastre_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)
        pnlDoctos.Visible = True

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("btnArrastre"), LinkButton).Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("btnArrastre"), LinkButton).Text, "-", ""))
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Protected Sub btnVerDocConductor_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)
        pnlDoctos.Visible = True

        Call CargarGrillaDocumentos(2, Trim(CType(row.FindControl("btnRutConductor"), LinkButton).Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("btnRutConductor"), LinkButton).Text, "-", "")) & " / " & Trim(CType(row.FindControl("lblFono"), Label).Text)
        lblTrasnportista.Text = ""

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_conf_recursos 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentos", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdRecursos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdRecursos.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If

                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnFecVecConductor"), HiddenField).Value <> "" Then
                    If CDate(CType(row.FindControl("hdnFecVecConductor"), HiddenField).Value) <= CDate(Date.Now.ToString) Then
                        CType(row.FindControl("btnRutConductor"), LinkButton).ForeColor = System.Drawing.Color.Red
                        CType(row.FindControl("imgBtnConductor"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                    End If

                    If DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnFecVecConductor"), HiddenField).Value)) <= 15 Then
                        CType(row.FindControl("imgBtnConductor"), ImageButton).ImageUrl = "~/Imagen/regular.png"
                    ElseIf DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnFecVecConductor"), HiddenField).Value)) > 15 Then
                        CType(row.FindControl("imgBtnConductor"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                    End If
                Else
                    CType(row.FindControl("imgBtnConductor"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If



                If CType(row.FindControl("hdnVecVencCamion"), HiddenField).Value <> "" Then
                    If CDate(CType(row.FindControl("hdnVecVencCamion"), HiddenField).Value) < CDate(Date.Now.ToString) Then
                        CType(row.FindControl("lblCodCamion"), Label).ForeColor = System.Drawing.Color.Red
                        CType(row.FindControl("imgBtnCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                    ElseIf DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnVecVencCamion"), HiddenField).Value)) < 15 Then
                        CType(row.FindControl("imgBtnCamion"), ImageButton).ImageUrl = "~/Imagen/regular.png"
                    ElseIf DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnVecVencCamion"), HiddenField).Value)) > 15 Then
                        CType(row.FindControl("imgBtnCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                    End If
                Else
                    CType(row.FindControl("imgBtnCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnFecVecnArrastre"), HiddenField).Value <> "" Then
                    If CDate(CType(row.FindControl("hdnFecVecnArrastre"), HiddenField).Value) <= CDate(Date.Now.ToString) Then
                        CType(row.FindControl("btnArrastre"), LinkButton).ForeColor = System.Drawing.Color.Red
                        CType(row.FindControl("imgBotonArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                    ElseIf DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnFecVecnArrastre"), HiddenField).Value)) < 15 And CDate(CType(row.FindControl("hdnFecVecnArrastre"), HiddenField).Value) > CDate(Date.Now.ToString) Then
                        CType(row.FindControl("imgBotonArrastre"), ImageButton).ImageUrl = "~/Imagen/regular.png"
                    ElseIf DateDiff(DateInterval.Day, CDate(Date.Now.ToString), CDate(CType(row.FindControl("hdnFecVecnArrastre"), HiddenField).Value)) > 15 Then
                        CType(row.FindControl("imgBotonArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                    End If
                Else
                    CType(row.FindControl("imgBotonArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdViajes_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGO()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_conf_recursos 'CGO'"
            cboGO.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboGO.DataTextField = "nom_grupo_operativo"
            cboGO.DataValueField = "cod_grupo_operativo"
            cboGO.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGO", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGOClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)
        pnlGO.Visible = True
        pnlArrastre.Visible = False
        pnlConductor.Visible = False

        Call CargarGO()
        lblTituloActualizar.Text = "GRUPO OPERATIVO ACTUAL: " & Trim(CType(row.FindControl("btnGO"), LinkButton).Text)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalACT();", True)
    End Sub


    Protected Sub ActArrastreClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)

        pnlGO.Visible = False
        pnlArrastre.Visible = True
        pnlConductor.Visible = False

        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        txtArrastre.Text = ""
        lblTituloActualizar.Text = "ARRASTRE ACTUAL: " & CType(row.FindControl("btnArrastre"), LinkButton).Text
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalACT();", True)
    End Sub


    Protected Sub txtArrastre_TextChanged(sender As Object, e As EventArgs) Handles txtArrastre.TextChanged
        If Trim(txtArrastre.Text) = "" Then
            txtArrastre.BackColor = System.Drawing.Color.White
            imgGPSArrastre.Visible = False
        Else
            Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)
            Call VerificarGPSArrastrePRO()
        End If
    End Sub


    Private Sub VerificarGPSArrastrePRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(txtArrastre.Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSArrastre.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 3 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSArrastre.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSArrastre", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosArrastre(ByVal patente As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    MessageBox("Arrastre", "Arrastre NO registrado en Msfot, comicarse con operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnActRecursosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)
        pnlGO.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = True

        lblTituloActualizar.Text = "CONDUCTOR ACTUAL: " & CType(row.FindControl("btnRutConductor"), LinkButton).Text
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalACT();", True)
    End Sub

    Protected Sub txtRutconductor_TextChanged(sender As Object, e As EventArgs) Handles txtRutconductor.TextChanged

        If Trim(txtRutconductor.Text) = "" Then
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtNomconductor.Text = ""
            txtFono.Text = ""
            lblTrasnportista.Text = ""
        Else

            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), ".", "")
            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), "-", "")
            If Len(Trim(txtRutconductor.Text)) = 8 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 7) + "-" + Right(Trim(txtRutconductor.Text), 1)
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 8) + "-" + Right(Trim(txtRutconductor.Text), 1)
            End If

            If Len(Trim(txtRutconductor.Text)) = 10 Then
                txtRutconductor.Text = Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO(Trim(txtRutconductor.Text))
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    txtNomconductor.Text = ""
                End If
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = "0" + Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO(Trim(txtRutconductor.Text))
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    txtNomconductor.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub RescatarDatosConductorPRO(ByVal rut As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'BDC', '" & rut & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtRutconductor.Text = Trim(rdoReader(0).ToString)
                    txtNomconductor.Text = Trim(rdoReader(1).ToString)
                    txtFono.Text = Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(txtRutconductor.Text, txtRutconductor)
                Else
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text

                If CDate(objFechaVenc) <= CDate(Date.Now.Date) Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarRecursos(ByVal tipo As String, ByVal pat_arrastre As String, ByVal cod_conductor As String, ByVal nom_conductor As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "URE"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = tipo
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = pat_arrastre
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = cod_conductor
            comando.Parameters.Add("@nom_conductor", SqlDbType.NVarChar)
            comando.Parameters("@nom_conductor").Value = nom_conductor
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarRecursos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarGO(ByVal cod_grupo_operativo As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UGO"
            comando.Parameters.Add("@cod_grupo_operativo", SqlDbType.NVarChar)
            comando.Parameters("@cod_grupo_operativo").Value = cod_grupo_operativo
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarGO", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click

        If pnlArrastre.Visible = True Then
            Call ActualizarRecursos("A", Trim(txtArrastre.Text), "", "", hdnID.Value)
            MessageBox("Actualizar Arrastre", "Arrastre Actualizados", Page, Master, "S")
        ElseIf pnlConductor.Visible = True Then
            Call ActualizarRecursos("C", "", hdnCodConductor.Value, Trim(txtNomconductor.Text), hdnID.Value)
            MessageBox("Actualizar Conductor", "Conductor Actualizados", Page, Master, "S")

        ElseIf pnlGO.Visible = True Then
            Call ActualizarGO(cboGO.SelectedValue, hdnID.Value)
            MessageBox("Actualizar Recursos", "Recursos Actualizados", Page, Master, "S")
        End If

        Call CargarGrillaRecursos()
    End Sub


    Protected Sub btnBloquearClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)

        Call RescatarDatosConductorPRO(CType(row.FindControl("btnRutConductor"), LinkButton).Text)

        Call Bloquear(CType(row.FindControl("lblCamion"), Label).Text, CType(row.FindControl("btnArrastre"), LinkButton).Text, hdnCodConductor.Value)
        Call CargarGrillaRecursos()
        MessageBox("Bloquear Recursos", "Recursos BLOQUEADOS", Page, Master, "S")


    End Sub

    Protected Sub btnDisponibleClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)

        Call RescatarDatosConductorPRO(CType(row.FindControl("btnRutConductor"), LinkButton).Text)

        Call Disponibilizar(CType(row.FindControl("lblCamion"), Label).Text, CType(row.FindControl("btnArrastre"), LinkButton).Text, hdnCodConductor.Value)
        Call CargarGrillaRecursos()
        MessageBox("Disponibilizar Recursos", "Recursos DISPONIBLES", Page, Master, "S")
    End Sub



    Protected Sub btnSiniestradoClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdRecursos.Rows(rowIndex)

        Call Siniestrado(CType(row.FindControl("hdnID"), HiddenField).Value)
        Call CargarGrillaRecursos()
        MessageBox("Siniestro", "Recurso SINIESTRADO", Page, Master, "S")
    End Sub

    Protected Sub Siniestrado(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "SIN"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Siniestrado", Err, Page, Master)
        End Try
    End Sub

    Protected Sub Disponibilizar(ByVal pat_camion As String, ByVal pat_arrastre As String, ByVal cod_conductor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "DIS"
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = pat_camion
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = pat_arrastre
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = cod_conductor
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Disponibilizar", Err, Page, Master)
        End Try
    End Sub



    Protected Sub Bloquear(ByVal pat_camion As String, ByVal pat_arrastre As String, ByVal cod_conductor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "BLO"
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = pat_camion
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = pat_arrastre
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = cod_conductor
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Bloquear", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdRecursos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdRecursos.SelectedIndexChanged

    End Sub

    Protected Sub cboGrupoOperativo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGrupoOperativo.SelectedIndexChanged
        Call CargarGrillaRecursos()
    End Sub

    Protected Sub cboServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicio.SelectedIndexChanged
        Call CargarGrillaRecursos()
    End Sub





    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_conf_recursos 'EXP', '" & cboGrupoOperativo.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & Trim(cboTrasnportista.SelectedValue) & "', '" & Trim(cboConductores.SelectedValue) & "', '" & Session("cod_usu_tc") & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Recursos_: " & Date.Now.Date & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("lnkExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnLimpiarFiltros_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltros.Click
        cboGrupoOperativo.ClearSelection()
        cboServicio.ClearSelection()
        cboTrasnportista.ClearSelection()
        cboConductores.ClearSelection()
        Call CargarGrillaRecursos()
    End Sub

    Protected Sub cboTrasnportista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTrasnportista.SelectedIndexChanged
        Call CargarGrillaRecursos()
        Call CargarConductores()
    End Sub

    Protected Sub cboConductores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConductores.SelectedIndexChanged
        Call CargarGrillaRecursos()
    End Sub


End Class