﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspIngDemandaP.aspx.vb" Inherits="aspSistemasJorquera.aspIngDemandaP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportar" />
            <asp:PostBackTrigger ControlID="btnExportarDia" />

            <asp:PostBackTrigger ControlID="bgnDesPlantilla1" />
            <asp:PostBackTrigger ControlID="btnCargarDocto" />
        </Triggers>


        <ContentTemplate>

            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>


            <script type="text/javascript">
                $(function () {
                    var today = new Date();
                    var month = ('0' + (today.getMonth() + 1)).slice(-2);
                    var day = ('0' + today.getDate()).slice(-2);
                    var year = today.getFullYear();
                    var date = year + '-' + month + '-' + day;
                    $('[id*=txtFechaServicio]').attr('min', date);
                });
            </script>




            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text="INGRESO DEMANDA"></asp:Label>
                            <small>Planificador</small>
                        </h3>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>

                            <button type="button" id="btnvolver1" runat="server" onclick="onClikSalir" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalVolver">
                                Asignación
                            </button>
                        </div>


                        <div class="modal" id="modalVolver" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-md">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnConfirnarVolver" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <div class="modal-header bg-success">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title ">IR A ASIGNACION</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>RECUERDE CONFIRMAR DEMANDA ANTES DE SALIR <br /><br /> <b>¿ESTA SEGURO QUE DESEA IR A ASIGNACION? </b></p>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                    </div>


                                    <div class="modal-footer">
                                        <asp:Button ID="btnConfirnarVolver" CssClass="btn btn-primary " Width="100px" OnClientClick="$('#modalVolver').modal('hide');" runat="server" Visible="true" Text="IR" />
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12  margin-bottom ">
                        <div class="row  margin-bottom">

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa  fa-ticket"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">DEMANDAS</span>
                                        <h4>
                                            <asp:Label ID="lbltotalDemandas" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnTotalKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa  fa-truck"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">OFERTA</span>
                                        <h4>
                                            <asp:Label ID="lblDisponibles" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnDisponiblesKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">ASIGNADOS</span>
                                        <h4>
                                            <asp:Label ID="lblAsignados" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnAsignadosKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">SIN ASIGNACION</span>
                                        <h4>
                                            <asp:Label ID="lblNoAsignados" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnNOAsignadosKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 form-group pull-right margin-bottom">
                                <div>
                                    <asp:FileUpload ID="fuplCargar" Visible="false" CssClass="form-control btn-primary" runat="server" />
                                </div>
                            </div>

                            <div class="col-md-1 form-group pull-right margin-bottom">
                                <div>
                                    <asp:LinkButton ID="bgnDesPlantilla1" Visible="false" CssClass="btn botonesTC-descargar btn-block " runat="server"> <i class="fa fa-download"></i> Plantilla</asp:LinkButton>
                                </div>
                            </div>


                            <div class="col-md-1 form-group pull-right">
                                <div>
                                    <asp:LinkButton ID="btnCargarDocto" Visible="false" CssClass="btn btn-primary btn-block " runat="server">C. Masiva</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicio" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Producto</label>
                                <div>
                                    <asp:DropDownList ID="cboProducto" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    OS/N°Solicitud</label>
                                <div class="">
                                    <asp:TextBox ID="txtOS" runat="server" TextMode="Number" AutoPostBack="true" placeholder="N°" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Tipo</label>
                                <div>
                                    <asp:DropDownList ID="cboTipoLugar" CssClass="form-control" runat="server" AutoPostBack="true">
                                        <asp:ListItem Value="0">Origen (Carga)</asp:ListItem>
                                        <asp:ListItem Value="1">Destino (Descarga)</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Ubicación</label>
                                <div>
                                    <asp:DropDownList ID="cboLugar" CssClass="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
                                    <asp:Label ID="lblDepto" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hora</label>
                                <div>
                                    <asp:TextBox ID="txtHoraservicio" runat="server" TextMode="Time" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Fecha Servicio</label>
                                <div>
                                    <asp:TextBox ID="txtFechaServicio" runat="server" TextMode="Date" CssClass="form-control" required></asp:TextBox>
                                </div>
                            </div>

                        </div>

                        <br />

                        <asp:Panel ID="pnlPreasignar" runat="server">
                            <div class="row">

                                <div class="col-md-2 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Cla. Operativa</label>
                                    <div>
                                        <asp:DropDownList ID="cboClaOperativa" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                    </div>
                                </div>


                                <div class="col-md-2 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Camión</label>
                                    <asp:LinkButton ID="btnVerDocCamion" CssClass="small-box-footer text-green" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>

                                    <div>
                                        <%--<asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" placeholder="Camión..." CssClass="form-control"></asp:TextBox>--%>
                                        <asp:DropDownList ID="cboCamion" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="false"></asp:DropDownList>
                                        <label class="text-info">Estado GPS: </label>
                                        <asp:Image ID="imgGPSCamion" runat="server" Width="15px" Height="15px" Visible="false" />
                                        <br />
                                        <asp:Label ID="lblTrasnportista" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-1 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Arrastre</label>
                                    <asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-green" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                    <div>
                                        <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." CssClass="form-control" Enabled="false"></asp:TextBox>
                                        <label class="text-info">Estado GPS: </label>
                                        <asp:Image ID="imgGPSArrastre" runat="server" Width="15px" Height="15px" Visible="false" />
                                    </div>
                                </div>

                                <div class="col-md-1 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        RUT</label>
                                    <div>
                                        <asp:TextBox ID="txtRutconductor" runat="server" Enabled="false" placeholder="RUT..." AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="col-md-3 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Nombre Conductor</label>
                                    <asp:LinkButton ID="btnVerDocConductor" CssClass="text-green" runat="server" Visible="False"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                    <div>

                                        <asp:DropDownList ID="cboConductor" CssClass="form-control" AutoPostBack="true" runat="server" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-1 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Fono</label>
                                    <div>
                                        <asp:TextBox ID="txtFono" runat="server" placeholder="Fono..." Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>


                                <%--      <div class="col-md-1 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Tipo Camión</label>
                                    <div>
                                        <asp:DropDownList ID="DropDownList1" Enabled="false" CssClass="form-control" runat="server" AutoPostBack="True">
                                            <asp:ListItem>SELECCIONAR</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>

                                <%--  <div class="col-md-1 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Extra Costo</label>
                                    <div>
                                        <asp:DropDownList ID="DropDownList2" Enabled="false" CssClass="form-control" runat="server" AutoPostBack="True">
                                            <asp:ListItem>SELECCIONAR</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>--%>
                            </div>


                            <div class="row">

                                <div class="col-md-3 form-group pull-left">
                                    <div>
                                        <asp:Image ID="Image2" Width="20px" Height="20px" runat="server" ImageUrl="~/Imagen/salmon.jpg" />
                                        <asp:Label ID="lblTit1" runat="server" CssClass="text-danger" Text="FECHA Y HORA MENOR A LA ACTUAL"></asp:Label>
                                    </div>
                                </div>

                                <div class="col-md-3 form-group pull-left">
                                    <div>
                                        <asp:Image ID="Image1" Width="20px" Height="20px" runat="server" ImageUrl="~/Imagen/tomato.jpg" />
                                        <asp:Label ID="Label1" runat="server" CssClass="text-warning" Text="GRUPO OPERATIVO DIFERENTE AL CAMION"></asp:Label>
                                    </div>
                                </div>


                                <div class="col-md-1 form-group pull-right">
                                    <div>
                                        <asp:LinkButton ID="btnExportar" CssClass="btn btn-block botonesTC-descargar" runat="server"> <i class="fa fa-download"></i> Tabla</asp:LinkButton>
                                    </div>
                                </div>



                                <div class="col-md-1 form-group  pull-right">
                                    <div>
                                        <asp:LinkButton ID="btnAgregar" CssClass="btn btn-primary btn-block " runat="server">Agregar</asp:LinkButton>
                                    </div>
                                </div>


                                <div class="col-md-1 form-group pull-right">
                                    <div>
                                        <button type="button" id="btnConfirmModal" runat="server" class="btn btn-primary  btn-block" visible="false" data-toggle="modal" data-target="#modalClientes">
                                            Confirmar
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </asp:Panel>


                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-12">

                                <asp:GridView ID="grdIngresoDemanda" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="OS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOS" runat="server" Text='<%# Bind("num_orden_servicio")%>'></asp:Label>
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnNomTipo" runat="server" Value='<%# Bind("nom_tipo")%>' />
                                                <asp:HiddenField ID="hdnNumOS" runat="server" Value='<%# Bind("num_orden_servicio")%>' />
                                                <asp:HiddenField ID="hdnRutCliente" runat="server" Value='<%# Bind("rut_cliente")%>' />
                                                <asp:HiddenField ID="hdnCodLugar" runat="server" Value='<%# Bind("cod_lugar")%>' />
                                                <asp:HiddenField ID="hdnCodProducto" runat="server" Value='<%# Bind("cod_producto")%>' />
                                                <asp:HiddenField ID="hdnIdServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                <asp:HiddenField ID="hdnCodTipo" runat="server" Value='<%# Bind("tipo")%>' />
                                                <asp:HiddenField ID="hdnCodCliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                <asp:HiddenField ID="hdnCodGrupoOperativo" runat="server" Value='<%# Bind("cod_grupo_operativo")%>' />
                                                <asp:HiddenField ID="hdnPatenteAnterior" runat="server" Value='<%# Bind("pat_camion")%>' />
                                                <asp:HiddenField ID="hdnArrastreAnterior" runat="server" Value='<%# Bind("pat_arrastre")%>' />
                                                <asp:HiddenField ID="hdnRutConductorAnterior" runat="server" Value='<%# Bind("rut_conductor")%>' />
                                                <asp:HiddenField ID="hdnPatenteCamion" runat="server" Value='<%# Bind("pat_camion")%>' />
                                                <asp:HiddenField ID="hdnEstMasivo" runat="server" Value='<%# Bind("est_carga_masiva")%>' />
                                                <asp:HiddenField ID="hdnGrupoCamion" runat="server" Value='<%# Bind("grupo_operativo_camion")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="30px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CLIENTE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomCliente" runat="server" Text='<%# Bind("nom_cliente")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Servicio">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboServicio" runat="server" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ActualizarRecursosGrilla" CssClass="form-control" DataTextField="nom_servicio" DataValueField="id_servicio" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Producto">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboProducto" runat="server" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ActualizarRecursosGrilla" CssClass="form-control" DataTextField="nom_producto" DataValueField="cod_producto" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboTipoLugar" CssClass="form-control" OnSelectedIndexChanged="CargarLugaresGrilla" runat="server" AutoPostBack="true">
                                                    <asp:ListItem Value="0">Origen (Carga)</asp:ListItem>
                                                    <asp:ListItem Value="1">Destino (Descarga)</asp:ListItem>
                                                </asp:DropDownList>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lugar">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="cboLugar" runat="server" CssClass="form-control" DataTextField="nom_lugar" DataValueField="cod_lugar" />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="180px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="FECHA">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFecha" Text='<%# Bind("fechaaa")%>' Width="160px" AutoPostBack="true" OnTextChanged="VerificarFechaGrilla" TextMode="date" CssClass="form-control" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="160px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA ">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtHora" Text='<%# Bind("hora")%>' CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="90px" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="CAMIÓN">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocCamion" CssClass="small-box-footer text-green" OnClick="btnVerDocCamion_Click" runat="server" Visible="false"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                                <%--<asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" placeholder="Camión..." Text='<%# Bind("pat_camion")%>' OnTextChanged="CargarDatosCamion" CssClass="form-control"></asp:TextBox>--%>

                                                <asp:DropDownList ID="cboCamion" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CargarDatosCamion" DataTextField="nom_patente" DataValueField="nom_patente" runat="server"></asp:DropDownList>

                                                <%-- <b>
                                            <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>--%>
                                                <%--<asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />--%>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ARRASTRE">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-green" OnClick="btnVerDocArrastre_Click" runat="server" Visible="false"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                                <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." Text='<%# Bind("pat_arrastre")%>' OnTextChanged="CargarDatosArrastre" CssClass="form-control"></asp:TextBox>
                                                <%--<b>
                                            <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>--%>
                                                <%--<asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />--%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocConductor" CssClass="small-box-footer text-green" OnClick="btnVerDocConductor_Click" runat="server" Visible="false"><i class="fa  fa-info-circle"></i></asp:LinkButton>
                                                <asp:TextBox ID="txtRutconductor" runat="server" AutoPostBack="true" placeholder="Rut..." OnTextChanged="CargarDoctosConductor" Text='<%# Bind("rut_conductor")%>' CssClass="form-control"></asp:TextBox>
                                                <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                <%--<asp:Label ID="lblSlash" runat="server" Text="/" />--%>
                                                <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' /><asp:Label ID="lblCodConductor" Visible="false" runat="server" Text='<%# Bind("cod_conductor")%>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="200px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:LinkButton ID="btnEliminar" OnClick="EliminarClick" runat="server" ToolTip=""><i class="fa fa-trash"></i></asp:LinkButton>
                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                                                               


                                    </Columns>

                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>


                                <asp:GridView ID="grdConfirmados" runat="server" Visible="false" Enabled="false" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="OS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOS" runat="server" Text='<%# Bind("num_orden_servicio")%>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="30px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CLIENTE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomCliente" runat="server" Text='<%# Bind("nom_cliente")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Servicio">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("nom_servicio")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Producto">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProducto" runat="server" Text='<%# Bind("nom_producto")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tipo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("nom_tipo")%>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Lugar">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLugar" runat="server" Text='<%# Bind("nom_lugar")%>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="FECHA">
                                            <ItemTemplate>
                                                <asp:Label ID="lblfecha" runat="server" Text='<%# Bind("fechaaa")%>'></asp:Label>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="120px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblhora" runat="server" Text='<%# Bind("hora")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="90px" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="CAMIÓN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("pat_camion")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ARRASTRE">
                                            <ItemTemplate>

                                                <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("pat_arrastre")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("conductor")%>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="200px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstadoTraspaso" runat="server" CssClass="text-green" Text='<%# Bind("estado")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                                        </asp:TemplateField>

                                    </Columns>

                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>
                        </div>
                        <br />

                        <div class="box box-success">
                            <div class="box-header with-border bg-success ">
                                <h3 class="box-title text-green">Demandas Del Dia</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Desde</label>
                                        <div>
                                            <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            <%--   <asp:CheckBox ID="chkSemana" runat="server" AutoPostBack="true" />
                                   &nbsp; VER SEMANA--%>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Hasta</label>
                                        <div>
                                            <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Cliente</label>
                                        <div>
                                            <asp:DropDownList ID="cboClienteKPI" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                            Servicio</label>
                                        <div>
                                            <asp:DropDownList ID="cboServicioKPI" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:Button ID="btnLimpiarFiltros" runat="server" CssClass="btn btn-primary" Text="Limpiar Filtros" />
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:LinkButton ID="btnExportarDia" CssClass="btn botonesTC-descargar" runat="server"> <i class="fa fa-download"></i> Tabla</asp:LinkButton>
                                        </div>
                                    </div>



                                </div>


                                <div class="row btn-sm">
                                    <div class="col-md-12 form-group">

                                        <asp:GridView ID="grdDemandaDia" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                            <Columns>

                                                <asp:TemplateField HeaderText="N° OS">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOS" runat="server" Text='<%# Eval("os") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="CLIENTE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("nom_cliente") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="CAMION">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_pat_camion") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ARRASTRE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_pat_arrastre") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ESTADO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEstado" runat="server" Text='<%# Eval("estado_viaje") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ORIGEN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_origen") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FECHA/HORA ORIGEN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFHOrigen" runat="server" Text='<%# Eval("FechaHoraOrigen") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="DESTINO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("nom_destino") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FECHA/HORA DESTINO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFHDestino" runat="server" Text='<%# Eval("FechaHoraDestino") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PRODUCTO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProducto" runat="server" Text='<%# Eval("nom_producto") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SERVICIO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--                                                      <asp:TemplateField HeaderText="CONDUCTOR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("nom_conductor") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                        </asp:GridView>



                                    </div>
                                </div>
                            </div>
                        </div>




                        <!-- Modal CONFIRMACION -->
                        <div class="modal" id="modalClientes" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                            <div class="modal-dialog  modal-md">
                                <div class="modal-content">

                                    <div class="modal-body">
                                        <asp:UpdatePanel runat="server" ID="updClientes">
                                            <%--     <Triggers>
                                                <asp:PostBackTrigger ControlID="btnConfirmar" />
                                            </Triggers>--%>
                                            <ContentTemplate>
                                                <div class="modal-header bg-success">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title ">Confirmar Demanda</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>¿ESTA SEGURO QUE DESEA CONFIRMAR ESTA DEMANDA? &hellip;</p>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                                            <ProgressTemplate>

                                                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                                                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="loader">
                                                                        <div class="loader-inner">
                                                                            <div class="loading one"></div>
                                                                        </div>
                                                                        <div class="loader-inner">
                                                                            <div class="loading two"></div>
                                                                        </div>
                                                                        <div class="loader-inner">
                                                                            <div class="loading three"></div>
                                                                        </div>
                                                                        <div class="loader-inner">
                                                                            <div class="loading four"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>


                                    </div>

                                    <div class="modal-footer">

                                        <asp:Button ID="btnConfirmar" CssClass="btn btn-primary " OnClientClick="$('#modalClientes').modal('hide');" runat="server" Visible="false" Text="REGISTRAR DEMANDA" />
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--fin modal--%>

                        <!-- Modal DOCUMENTACION -->
                        <div>
                            <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                                <div class="modal-dialog modal-lg ">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <h3 class="modal-title text-light">
                                                <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                <asp:Label ID="lblIdentificador" runat="server" CssClass="text-green" Text=""></asp:Label></h3>
                                            <button type="button" class="close" data-dismiss="modal">
                                                &times;</button>
                                        </div>
                                        <div class="modal-body">


                                            <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="DOCUMENTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("Documento") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="VENCIMIENTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("Fecha_Vencimiento", "{0:d}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>


                                            <asp:GridView ID="grdDisponiblesKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="CAMIÓN">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcamion" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ARRASTRE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblArrastre" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="CONDUCTOR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("conductor") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SERVICIO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>


                                            <asp:GridView ID="grdDemandasKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>

                                                    <asp:TemplateField HeaderText="N° OS">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOS" runat="server" Text='<%# Eval("os") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <%--                                                    <asp:TemplateField HeaderText="N° DEMANDA">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIdDemanda" runat="server" Text='<%# Eval("id_demanda") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="CLIENTE">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("nom_cliente") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ORIGEN">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_origen") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="FECHA/HORA ORIGEN">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFHOrigen" runat="server" Text='<%# Eval("FechaHoraOrigen") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="DESTINO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("nom_destino") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="FECHA/HORA DESTINO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFHDestino" runat="server" Text='<%# Eval("FechaHoraDestino") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="PRODUCTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProducto" runat="server" Text='<%# Eval("nom_producto") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SERVICIO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <%--                                                      <asp:TemplateField HeaderText="CONDUCTOR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("nom_conductor") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIN MODAL -->
                        <asp:HiddenField ID="hdnRegistros" runat="server" Value="0" />

                        <asp:HiddenField ID="hdnOrigenDestino" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnExisteGeocerca" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnRutCliente" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnNomcliente" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnRutconductor" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnCodConductor" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnCodTrasnportista" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnEstGPS" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnNomEstGPS" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnMismaFechaHora" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnHoraAtrasGrilla" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnConfirmar" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnSemana" runat="server" Value="0" />
                    </div>
                </div>

                <%--GRILLA OCULTA PARA CARGA MASIVA--%>
                <asp:GridView ID="grdTempMasiva" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" Visible="false" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="CLIENTE">
                            <ItemTemplate>
                                <asp:Label ID="lblNomCliente" runat="server" Text='<%# Bind("rut_cliente")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Servicio">
                            <ItemTemplate>
                                <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("id_servicio")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tipo_od")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="OS">
                            <ItemTemplate>
                                <asp:Label ID="lblOS" runat="server" Text='<%# Bind("os")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Lugar">
                            <ItemTemplate>
                                <asp:Label ID="lblCodLugar" runat="server" Text='<%# Bind("cod_lugar")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Fecha
                                    ">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Hora ">
                            <ItemTemplate>
                                <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Producto">
                            <ItemTemplate>
                                <asp:Label ID="lblCodProducto" runat="server" Text='<%# Bind("cod_producto")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Camion">
                            <ItemTemplate>
                                <asp:Label ID="lblCamion" runat="server" Text='<%# Bind("camion")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Arrastre">
                            <ItemTemplate>
                                <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("arrastre")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Condcutor">
                            <ItemTemplate>
                                <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("rut_conductor")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" />
                        </asp:TemplateField>


                    </Columns>

                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                </asp:GridView>

            </section>
            <!-- /.content -->

        </ContentTemplate>

    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>


