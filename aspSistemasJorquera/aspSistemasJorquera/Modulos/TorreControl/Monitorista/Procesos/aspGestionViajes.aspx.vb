﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspGestionViajes
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        'Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Session("est_asignado_tc") = 0
                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "CONTROL ALERTAS"

                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")
                Call CargarClientes()
                Call CargarServicios()
                Call CargarTransportistas()
                Call CargarAlertas()
                Call CargarEstadoAlertas()
                Call CargarEstadoViajes()
                Call CargarComboOrigenes()

                Call CargarGrillaViajes()
                Call CargarGrillaAlertas()

                Call CargarCarga()
                Call CargarKPI()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrupoOperativo()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CGO', '" & Session("cod_usu_tc") & "'"
            cboGO.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboGO.DataTextField = "nom_grupo_operativo"
            cboGO.DataValueField = "cod_grupo_operativo"
            cboGO.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrupoOperativo", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnControlVW_Click(sender As Object, e As EventArgs) Handles btnControlVW.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "CONTROL ALERTAS"
        TimerViajes.Enabled = True
        btnControlVW.CssClass = btnControlVW.CssClass.Replace("bg-success", "bg-green")
        btnAlertasVW.CssClass = btnAlertasVW.CssClass.Replace("bg-green", "bg-success")
        btnCicloVW.CssClass = btnCicloVW.CssClass.Replace("bg-green", "bg-success")

        Call CargarKPI()
        Call CargarGrillaViajes()
    End Sub


    Protected Sub btnAlertasVW_Click(sender As Object, e As EventArgs) Handles btnAlertasVW.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "ALERTAS"
        TimerViajes.Enabled = False
        btnControlVW.CssClass = btnControlVW.CssClass.Replace("bg-green", "bg-success")
        btnAlertasVW.CssClass = btnAlertasVW.CssClass.Replace("bg-success", "bg-green")
        btnCicloVW.CssClass = btnCicloVW.CssClass.Replace("bg-green", "bg-success")

        txtDesdeAlertas.Text = Session("fec_actual")
        txtHastaAlertas.Text = Session("fec_actual")
        Call CargarClientesAlertas()

        Call CargarKPIAlertas()
        Call CargarGrillaAlertasTipo()
        Call CargarInformacionAlerta()
        Call CargarGrillaAlertasViajes()
    End Sub


    Protected Sub btnCicloVW_Click(sender As Object, e As EventArgs) Handles btnCicloVW.Click
        MultiView1.ActiveViewIndex = 2
        lbltitulo.Text = "CICLO LOGISTICO"
        TimerViajes.Enabled = False
        btnControlVW.CssClass = btnControlVW.CssClass.Replace("bg-green", "bg-success")
        btnAlertasVW.CssClass = btnAlertasVW.CssClass.Replace("bg-green", "bg-success")
        btnCicloVW.CssClass = btnCicloVW.CssClass.Replace("bg-success", "bg-green ")

        txtDesdeCiclo.Text = Session("fec_actual")
        txtHastaciclo.Text = Session("fec_actual")
        Call CargarClientesCiclo()
        Call CargarInformacionCiclo()
        Call CargarKPICiclo()
        Call CargarGrillaTiempos()
    End Sub


    Private Sub CargarKPI()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_viajes 'KPI', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "' , '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & cboAlertas.SelectedValue & "',  '" & cboEstado.SelectedValue & "', '" & cboOrigen.SelectedValue & "','" & Session("cod_usu_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblTotalViajes.Text = Trim(rdoReader(0).ToString)
                    lblEnRuta.Text = Trim(rdoReader(1).ToString)
                    lblTerminados.Text = Trim(rdoReader(2).ToString)
                    lblTermiandosPOD.Text = Trim(rdoReader(3).ToString)
                    lblTerminadosPODValido.Text = Trim(rdoReader(4).ToString)
                    lblEnOrigen.Text = Trim(rdoReader(5).ToString)
                    lblEnDestino.Text = Trim(rdoReader(6).ToString)
                    lblTotalAlertas.Text = Trim(rdoReader(7).ToString)
                    lblAlertasGestionadas.Text = Trim(rdoReader(8).ToString)
                    lblAlertasPendientes.Text = Trim(rdoReader(9).ToString)

                    If Val(lblAlertasPendientes.Text) < 2 Then
                        imgAlertasPendientes.ImageUrl = "~/Imagen/ok.png"
                    ElseIf Val(lblAlertasPendientes.Text) >= 2 And Val(lblAlertasPendientes.Text) <= 4 Then
                        imgAlertasPendientes.ImageUrl = "~/Imagen/regular.png"
                    ElseIf Val(lblAlertasPendientes.Text) > 4 Then
                        imgAlertasPendientes.ImageUrl = "~/Imagen/malo.png"
                    End If

                    lblCumplimiento.Text = Trim(rdoReader(10).ToString)

                    If Val(lblCumplimiento.Text) <= 95 Then
                        imgCumplViajes.ImageUrl = "~/Imagen/malo.png"
                    ElseIf Val(lblCumplimiento.Text) > 95 Then
                        imgCumplViajes.ImageUrl = "~/Imagen/ok.png"
                    End If

                    lblCumplimientoAlertas.Text = Trim(rdoReader(11).ToString)
                    lblTotDemandas.Text = Trim(rdoReader(12).ToString)
                    lblAsignados.Text = Trim(rdoReader(13).ToString)

                End If
            Catch ex As Exception
                MessageBoxError("CargarKPI", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarKPIAlertas()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_viajes 'KPI2', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "','" & Session("cod_usu_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                cmdTemporal.CommandTimeout = 99999999
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblTotalAlertasAlerta.Text = Trim(rdoReader(0).ToString)
                    lblGestionadasAlerta.Text = Trim(rdoReader(1).ToString)
                    lblPendientesAlerta.Text = Trim(rdoReader(2).ToString)
                End If
            Catch ex As Exception
                MessageBoxError("CargarKPIAlertas", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarKPICiclo()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_gestion_viajes 'KPI', '" & CDate(txtDesdeCiclo.Text) & "', '" & CDate(txtHastaciclo.Text) & "', '" & cboClientesCiclo.SelectedValue & "', '" & cboServiciosCiclo.SelectedValue & "' , '" & cboTransportistaCiclo.SelectedValue & "', '" & cboPODCiclo.SelectedValue & "', '" & cboAlertasCiclo.SelectedValue & "',  '" & cboEstadoCiclo.SelectedValue & "', '" & cboOrigenCiclo.SelectedValue & "','" & Session("cod_usu_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then


                    lblTotalViajesCiclo.Text = Trim(rdoReader(0).ToString)
                    lblEnRutaCiclo.Text = Trim(rdoReader(1).ToString)
                    lblTerminadosCiclo.Text = Trim(rdoReader(2).ToString)
                    lblTermiandosPODCiclo.Text = Trim(rdoReader(3).ToString)
                    lblTerminadosPODValidoCiclo.Text = Trim(rdoReader(4).ToString)
                    lblEnOrigenCiclo.Text = Trim(rdoReader(5).ToString)
                    lblEnDestinoCiclo.Text = Trim(rdoReader(6).ToString)
                    lblTotalAlertasCiclo.Text = Trim(rdoReader(7).ToString)
                    lblAlertasGestionadasCiclo.Text = Trim(rdoReader(8).ToString)
                    lblAlertasPendientesCiclo.Text = Trim(rdoReader(9).ToString)


                    If Val(lblAlertasPendientesCiclo.Text) < 2 Then
                        imgAlertasPendientesCiclo.ImageUrl = "~/Imagen/ok.png"
                    ElseIf Val(lblAlertasPendientesCiclo.Text) >= 2 And Val(lblAlertasPendientesCiclo.Text) <= 4 Then
                        imgAlertasPendientesCiclo.ImageUrl = "~/Imagen/regular.png"
                    ElseIf Val(lblAlertasPendientesCiclo.Text) > 4 Then
                        imgAlertasPendientesCiclo.ImageUrl = "~/Imagen/malo.png"
                    End If

                    lblCumplimientoCiclo.Text = Trim(rdoReader(10).ToString)

                    If Val(lblCumplimientoCiclo.Text) <= 95 Then
                        imgCumplCiclo.ImageUrl = "~/Imagen/malo.png"
                    ElseIf Val(lblCumplimientoCiclo.Text) > 95 Then
                        imgCumplCiclo.ImageUrl = "~/Imagen/ok.png"
                    End If

                    lblCumplimientoAlertasCiclo.Text = Trim(rdoReader(11).ToString)
                    lblTotDemandasCiclo.Text = Trim(rdoReader(12).ToString)
                    lblAsignadosCiclo.Text = Trim(rdoReader(13).ToString)

                End If
            Catch ex As Exception
                MessageBoxError("CargarKPICiclo", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCC', '" & Session("cod_usu_tc") & "'"
            cboClientes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientes.DataTextField = "nom_cliente1"
            cboClientes.DataValueField = "cod_msoft"
            cboClientes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarClientesCiclo()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCC', '" & Session("cod_usu_tc") & "'"
            cboClientesCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientesCiclo.DataTextField = "nom_cliente1"
            cboClientesCiclo.DataValueField = "cod_msoft"
            cboClientesCiclo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientesCiclo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarClientesAlertas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec rpt_ciclo_logistico 'CRC','" & Session("cod_usu_tc") & "'"
            cboClientealertas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientealertas.DataTextField = "nom_cliente"
            cboClientealertas.DataValueField = "rut_cliente"
            cboClientealertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientesAlertas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEstadoAlertas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CEA'"
            cboEstadoAlertas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstadoAlertas.DataTextField = "nom_estado_alerta"
            cboEstadoAlertas.DataValueField = "id_estado_alerta"
            cboEstadoAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstadoAlertas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCS', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()

            cboServiciosCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServiciosCiclo.DataTextField = "nom_servicios"
            cboServiciosCiclo.DataValueField = "cod_servicios"
            cboServiciosCiclo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCT'"
            cboTrasportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasportista.DataTextField = "nom_transportista"
            cboTrasportista.DataValueField = "cod_msoft"
            cboTrasportista.DataBind()

            cboTransportistaCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTransportistaCiclo.DataTextField = "nom_transportista"
            cboTransportistaCiclo.DataValueField = "cod_msoft"
            cboTransportistaCiclo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarAlertas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCA'"
            cboAlertas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboAlertas.DataTextField = "nom_alerta"
            cboAlertas.DataValueField = "id_alerta"
            cboAlertas.DataBind()

            cboAlertasCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboAlertasCiclo.DataTextField = "nom_alerta"
            cboAlertasCiclo.DataValueField = "id_alerta"
            cboAlertasCiclo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAlertas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaViajes()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'CGV2', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & cboEstado.SelectedValue & "', '" & cboOrigen.SelectedValue & "', '" & Trim(txtExp.Text) & "', '" & cboDestinoKPI.SelectedValue & "', '" & Trim(txtGuia.Text) & "'"
            grdViajes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdViajes.DataBind()
            lblTotReg.Text = grdViajes.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaViajes", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnExportarViajes_Click(sender As Object, e As EventArgs) Handles btnExportarViajes.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_gestion_viajes 'CGVX', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & cboEstado.SelectedValue & "', '" & cboOrigen.SelectedValue & "', '" & Trim(txtExp.Text) & "', '" & cboDestinoKPI.SelectedValue & "', '" & Trim(txtGuia.Text) & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Viajes.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarAlertas_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEstadoViajes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CEV'"
            cboEstado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstado.DataTextField = "nom_estado"
            cboEstado.DataValueField = "id"
            cboEstado.DataBind()

            cboEstadoCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstadoCiclo.DataTextField = "nom_estado"
            cboEstadoCiclo.DataValueField = "id"
            cboEstadoCiclo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstadoViajes", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboOrigenes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'COR'"
            cboOrigen.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboOrigen.DataTextField = "nom_lugar"
            cboOrigen.DataValueField = "cod_lugar"
            cboOrigen.DataBind()

            cboOrigenCiclo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboOrigenCiclo.DataTextField = "nom_lugar"
            cboOrigenCiclo.DataValueField = "cod_lugar"
            cboOrigenCiclo.DataBind()


            cboDestinoKPI.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboDestinoKPI.DataTextField = "nom_lugar"
            cboDestinoKPI.DataValueField = "cod_lugar"
            cboDestinoKPI.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarComboOrigenes", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'CGA', '" & hdnIdViaje.Value & "', '" & cboEstadoAlertas.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & Session("cod_usu_tc") & "', '', '" & cboAlertas.SelectedValue & "', '" & cboOrigen.SelectedValue & "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
            lbltotRegistrosdAlertas.Text = "  (TOTAL REGISTROS: " & grdAlertas.Rows.Count & ")"
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertas", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnExportarAlertas_Click(sender As Object, e As EventArgs) Handles btnExportarAlertas.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_gestion_viajes 'CGA', '" & hdnIdViaje.Value & "', '" & cboEstadoAlertas.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & Session("cod_usu_tc") & "', '""', '" & cboAlertas.SelectedValue & "', '" & cboOrigen.SelectedValue & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=AlertasActuales.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarAlertas_Click", Err, Page, Master)
        End Try
    End Sub



    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        'strDatos = "<b>N° VIAJE: </b> " & hdnIdViaje.Value & "<br /><b>Camión: </b> " & hdnPatente.Value & "<br /><b>Arrastre: </b> " & hdnPatArrastre.Value & " <br /><b>Conductor: </b> " & hdnNomconductor.Value & "<br /><b>Fono: </b> " & hdnfonoConductor.Value & "<br /><b>--- ALERTAS --- </b><br /><b>Tipo Alerta: </b> " & hdnTipoAlerta.Value & "<br /><b>Fecha Alerta: </b> " & hdnFecAlerta.Value & ""
        strDatos = "--- ALERTAS --- </b><br /><b>Tipo Alerta: </b> " & hdnTipoAlerta.Value & "<br /><b>Fecha Alerta: </b> " & hdnFecAlerta.Value & ""

        strDatos = "" & strDatos & ""
        Return strDatos
    End Function


    Protected Sub VerMapaAlerta(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)

        hdnLat.Value = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        hdnLon.Value = CType(row.FindControl("hdnLongitud"), HiddenField).Value
        hdnFecAlerta.Value = CType(row.FindControl("lblFechaAlerta"), Label).Text
        hdnPatente.Value = CType(row.FindControl("lblPatente"), Label).Text
        hdnTipoAlerta.Value = CType(row.FindControl("lblTipoAlerta"), Label).Text

    End Sub

    Protected Sub VerAlertas(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        hdnLat.Value = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        hdnLon.Value = CType(row.FindControl("hdnLonViaje"), HiddenField).Value

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnPatente.Value = CType(row.FindControl("btnCamion"), LinkButton).Text
        hdnPatArrastre.Value = CType(row.FindControl("btnArrastre"), LinkButton).Text
        hdnNomconductor.Value = CType(row.FindControl("lblNomConductor"), Label).Text
        hdnfonoConductor.Value = CType(row.FindControl("lblFono"), Label).Text

        Call CargarGrillaAlertas()

    End Sub



    Protected Sub VerFichaViaje(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("transportista_tc") = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value
        Session("origen_tc") = CType(row.FindControl("lblOrigen"), Label).Text
        Session("destino_tc") = CType(row.FindControl("btnDestino"), LinkButton).Text
        Session("camion_tc") = CType(row.FindControl("btnCamion"), LinkButton).Text
        Session("arrastre_tc") = CType(row.FindControl("btnArrastre"), LinkButton).Text
        Session("conductor_tc") = CType(row.FindControl("lblNomConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("btnRutConductor"), LinkButton).Text

        Session("nun_fono_tc") = CType(row.FindControl("hdnFono"), HiddenField).Value

        Session("score") = CType(row.FindControl("hdnScoreConductor"), HiddenField).Value
        Session("eta_TC") = CType(row.FindControl("lblETA"), Label).Text

        Session("lat_ficha_tc") = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        Session("lon_ficha_tc") = CType(row.FindControl("hdnLonViaje"), HiddenField).Value
        Session("origen_ficha_tc") = CType(row.FindControl("hdnCodOrigen"), HiddenField).Value

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspFichaViaje.aspx")

    End Sub

    Protected Sub CargarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspPOD.aspx")

    End Sub


    Protected Sub GestionarAlerta(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)

        Session("idcallcenter_g_tc") = CType(row.FindControl("hdnID"), HiddenField).Value
        Session("descripcion_g_tc") = CType(row.FindControl("hdnNomDescripcion"), HiddenField).Value



        Session("score_viaje_g_tc") = CType(row.FindControl("hdnScoreViaje"), HiddenField).Value


        Session("tipo_alerta_g_tc") = CType(row.FindControl("lblTipoAlerta"), Label).Text
        Session("id_alerta_g_tc") = CType(row.FindControl("hdnIdAlerta"), HiddenField).Value
        Session("camion_g_tc") = CType(row.FindControl("lblPatente"), Label).Text
        Session("num_viaje_g_tc") = CType(row.FindControl("hdnNumViaje"), HiddenField).Value
        Session("conductor_g_tc") = CType(row.FindControl("hdnNomConductor"), HiddenField).Value
        Session("origen_g_tc") = CType(row.FindControl("hdnOrigen"), HiddenField).Value
        Session("destino_g_tc") = CType(row.FindControl("hdnDestino"), HiddenField).Value
        Session("cliente_g_tc") = CType(row.FindControl("hdnCliente"), HiddenField).Value

        Session("latitud_alerta_tc") = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        Session("longitud_alerta_tc") = CType(row.FindControl("hdnLongitud"), HiddenField).Value


        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspGestionAlerta.aspx")

    End Sub

    Protected Sub grdAlertas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAlertas.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstAlerta"), HiddenField).Value <> 1 Then
                    CType(row.FindControl("BTNgESTIONAR"), LinkButton).Visible = False
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdAlertas_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text

                If CDate(objFechaVenc) <= CDate(Date.Now.Date) Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub



    Protected Sub btnVerDocCamion_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", ""))

        lblTrasnportista.Text = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocArrastre_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", ""))
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub CargarDatosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'RDC', '" & Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    CType(row.FindControl("txtCamion"), TextBox).Text = Replace(Trim(rdoReader(0).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("btnGPSCamion"), ImageButton))

                    CType(row.FindControl("hdnCodConductor"), HiddenField).Value = Trim(rdoReader(1).ToString)
                    CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(rdoReader(4).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))

                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(rdoReader(3).ToString)
                    CType(row.FindControl("lblNomConductor"), Label).Text = Trim(rdoReader(2).ToString) & " / " & Trim(rdoReader(5).ToString)

                    Call VerificarDoctosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))

                Else

                    CType(row.FindControl("txtArrastre"), TextBox).Text = ""
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = ""
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""

                    CType(row.FindControl("txtCamion"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"

                    MessageBox("Buscar", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")

                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosVehiculo(ByVal patente As String, ByRef txt As TextBox, ByRef btn As LinkButton)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                        ' btn.Visible = False
                    Else
                        txt.BackColor = System.Drawing.Color.White
                        ' btn.Visible = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub RescatarEstadoGPSPRO(ByVal patente As String, ByRef img As ImageButton)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GPS', '" & Trim(Replace(patente, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = 1 Then
                        img.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        img.ImageUrl = "~/Imagen/malo.png"
                    Else
                        img.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    img.ImageUrl = "~/Imagen/nulo.png"
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarEstadoGPSPRO", Err, Page, Master)
        End Try
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub GPSArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtArrastre"), TextBox).Text), "-", "")

        If CType(row.FindControl("txtArrastre"), TextBox).Text = "" Then
            CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
        Else
            Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
        End If

        Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))
    End Sub

    Protected Sub DoctosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        If CType(row.FindControl("txtRutconductor"), TextBox).Text = "" Then
            CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
        Else
            If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                CType(row.FindControl("lblNomConductor"), Label).Text = ""
            Else
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), ".", "")
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), "-", "")
                If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 8 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 7) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 8) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                End If


                If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                Else
                    If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 10 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)
                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = "0" + Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)

                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    End If
                End If
            End If
        End If
    End Sub


    Private Sub RescatarDatosConductor(ByVal rut As String, ByRef nombre As String, ByRef txtRut As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'BDC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    rut = Trim(rdoReader(0).ToString)
                    nombre = Trim(rdoReader(1).ToString) '& " " & Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(rut, txtRut, btn, txtCamion, txtArrastre)
                Else
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnVerDocConductor_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(2, Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("lblNomConductor"), Label).Text, "-", "")) & " / " & Trim(CType(row.FindControl("hdnFono"), HiddenField).Value)
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnTipoViajeClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Call CargarGrillaMultipaso(CType(row.FindControl("hdnOS"), HiddenField).Value, CType(row.FindControl("hdnCodcliente"), HiddenField).Value)

        pnlDoctos.Visible = False
        pnlDetalleViaje.Visible = True
        pnlKPI.Visible = False

        lblNumOS.Text = Trim(Replace(CType(row.FindControl("lblOS"), Label).Text, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Private Sub CargarGrillaMultipaso(ByVal os As String, ByVal cod_cliente As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CDM' , '" & os & "', '" & cod_cliente & "'"
            grdMultipaso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdMultipaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaMultipaso", Err, Page, Master)
        End Try
    End Sub


    Protected Sub AddIndicencias(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("transportista_tc") = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value
        Session("nom_cli_tc") = CType(row.FindControl("hdnNomCliente"), HiddenField).Value
        Session("cod_cli_tc") = CType(row.FindControl("hdnCodCliente"), HiddenField).Value
        Session("nom_pro_tc") = CType(row.FindControl("hdnNomProducto"), HiddenField).Value
        Session("origen_tc") = CType(row.FindControl("lblOrigen"), Label).Text
        Session("destino_tc") = CType(row.FindControl("btnDestino"), LinkButton).Text
        Session("camion_tc") = CType(row.FindControl("btnCamion"), LinkButton).Text
        Session("arrastre_tc") = CType(row.FindControl("btnArrastre"), LinkButton).Text
        Session("conductor_tc") = CType(row.FindControl("lblNomConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("btnRutConductor"), LinkButton).Text
        Session("nom_ser_tc") = CType(row.FindControl("hdnNomServicio"), HiddenField).Value
        Session("nom_go_tc") = CType(row.FindControl("hdnNomGrupoOperativo"), HiddenField).Value
        Session("cod_go_tc") = CType(row.FindControl("hdnGrupoOperativo"), HiddenField).Value
        Session("cod_pro_tc") = CType(row.FindControl("hdnCodProducto"), HiddenField).Value
        Session("num_telefono_tc") = CType(row.FindControl("hdnFono"), HiddenField).Value

        Session("lat_incidencia_tc") = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        Session("lon_incidencia_tc") = CType(row.FindControl("hdnLonViaje"), HiddenField).Value

        Session("nom_sup_tc") = "SUPERVISOR ..."

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspIncidencias.aspx")

    End Sub


    Protected Sub CancelarAsignacion(ByVal est_viaje As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UEA"
            comando.Parameters.Add("@id_est_viaje", SqlDbType.NVarChar)
            comando.Parameters("@id_est_viaje").Value = est_viaje
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@obs", SqlDbType.NVarChar)
            comando.Parameters("@obs").Value = Trim(txtOBS.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Call CargarGrillaViajes()
            MessageBox("Cancelar", "Registro Cancelado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("CancelarAsignacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdViajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdViajes.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 6 Then
                    CType(row.FindControl("btnCancelarAsignacion"), LinkButton).Visible = True
                End If


                If CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 6 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 12 Then
                    CType(row.FindControl("btnDestino"), LinkButton).Enabled = True
                End If

                If CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 5 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 7 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 8 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 13 Then
                    CType(row.FindControl("btnCerrarViaje"), LinkButton).Visible = False
                End If

                If CType(row.FindControl("hdnAlertasPendientes"), HiddenField).Value = True Then
                    e.Row.BackColor = System.Drawing.Color.LightGray
                End If


                If CType(row.FindControl("hdnIncidencias"), HiddenField).Value > 0 Then
                    CType(row.FindControl("imgIncidenciasActivas"), Image).Visible = True
                    CType(row.FindControl("imgIncidenciasActivas"), Image).ImageUrl = "~/Imagen/Incidenciasactivas.png"
                End If


                If CType(row.FindControl("hdnOnTime"), HiddenField).Value = "" Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                ElseIf CType(row.FindControl("hdnOnTime"), HiddenField).Value = 0 Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                ElseIf CType(row.FindControl("hdnOnTime"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdViajes_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdTiempos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdTiempos.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnOnTime"), HiddenField).Value = "" Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                ElseIf CType(row.FindControl("hdnOnTime"), HiddenField).Value = 0 Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                ElseIf CType(row.FindControl("hdnOnTime"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnOnTime"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdTiempos_RowDataBound", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarDestinos(ByVal cliente As String)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'COD', '" & 1 & "', '" & cliente & "', '" & Session("cod_usu_tc") & "'"
            cboDestinos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboDestinos.DataTextField = "nom_lugar"
            cboDestinos.DataValueField = "cod_lugar"
            cboDestinos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDestinos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub CerrarViaje()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_conf_recursos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UCV"
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = hdnIdViaje.Value
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = Trim(txtOBS.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CerrarViaje", Err, Page, Master)
        End Try
    End Sub


    Protected Sub CancelarUT(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCerrarViaje.Visible = False
        pnlCancelarViaje.Visible = True
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = False
        txtOBS.Text = ""
        hdnIdViaje.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        lblTituloActualizar.Text = "CANCELAR VIAJE: " & CType(row.FindControl("btnNumViaje"), LinkButton).Text & " OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub cboGO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGO.SelectedIndexChanged
        Call CargarPatentes(cboGO.SelectedValue)
    End Sub


    Protected Sub CerrarViajeClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCerrarViaje.Visible = True
        pnlCancelarViaje.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = False
        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        txtOBS.Text = ""
        lblTituloActualizar.Text = "CERRAR VIAJE :" & CType(row.FindControl("btnNumViaje"), LinkButton).Text & " OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub IncidenciasClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Call CargarGrillaIncidencias(CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value)

        grdAlertasPopUp.Visible = False
        grdIncidencias.Visible = True

        lblTituloIncidencias.Text = "INCIDENCIAS VIAJE :" & CType(row.FindControl("btnNumViaje"), LinkButton).Text & " OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalncidencias();", True)
    End Sub


    Protected Sub VerAlertasClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Call CargarGrillaAlertasPopUp(CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value)

        grdAlertasPopUp.Visible = True
        grdIncidencias.Visible = False

        lblTituloIncidencias.Text = "ALERTAS VIAJE :" & CType(row.FindControl("btnNumViaje"), LinkButton).Text & " OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalncidencias();", True)
    End Sub

    Private Sub CargarGrillaIncidencias(ByVal num_viaje As String)
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CGI', '" & num_viaje & "'"
            grdIncidencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIncidencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaIncidencias", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaAlertasPopUp(ByVal num_viaje As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'AFV', '" & num_viaje & "'"
            grdAlertasPopUp.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertasPopUp.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertasPopUp", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnActDestino(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = True
        pnlCerrarViaje.Visible = False
        pnlCancelarViaje.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = False
        txtOBS.Visible = True
        txtOBS.Text = ""
        hdnLatViaje.Value = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        hdnLonViaje.Value = CType(row.FindControl("hdnLonViaje"), HiddenField).Value

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        txtOBS.Text = ""
        lblTituloActualizar.Text = "ACTUALIZAR DESTINO OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        Call CargarDestinos(CType(row.FindControl("hdnCodcliente"), HiddenField).Value)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub ActualizarRecursosMSFOT()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ACT"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = hdnIdViaje.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarRecursosMSFOT", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        If pnlDestinos.Visible = True Then
            Call ActualizarDestino(cboDestinos.SelectedValue, hdnID.Value, hdnIdViaje.Value)
            Call ActualizarRecursosMSFOT()

            MessageBox("Actualizar Recursos", "Destino Actualizado", Page, Master, "S")
        ElseIf pnlCamion.Visible = True Then
            Call ActualizarRecursos("CAMION", cboCamion.SelectedValue, hdnID.Value, hdnIdViaje.Value)
            Call ActualizarRecursosMSFOT()
            MessageBox("Actualizar Recursos", "Camión Actualizado", Page, Master, "S")
        ElseIf pnlArrastre.Visible = True Then
            Call ActualizarRecursos("ARRASTRE", txtArrastre.Text, hdnID.Value, hdnIdViaje.Value)
            Call ActualizarRecursosMSFOT()
            MessageBox("Actualizar Recursos", "Arrastre Actualizado", Page, Master, "S")
        ElseIf pnlConductor.Visible = True Then
            Call ActualizarRecursos("CONDUCTOR", hdnCodConductor.Value, hdnID.Value, hdnIdViaje.Value)
            Call ActualizarRecursosMSFOT()
            MessageBox("Actualizar Recursos", "Conductor Actualizado", Page, Master, "S")
        ElseIf pnlCerrarViaje.Visible = True Then
            Call CerrarViaje()
            Call ActualizarRecursosMSFOT()
            MessageBox("Cerrar Viaje", "Viaje Cerrado", Page, Master, "S")

        ElseIf pnlCancelarViaje.Visible = True Then
            Call CancelarAsignacion(5, hdnIdViaje.Value)
            Call ActualizarRecursosMSFOT()
            MessageBox("Cancelar Viaje", "Viaje Cancelado", Page, Master, "S")
        End If

        Call CargarGrillaViajes()
    End Sub


    Protected Sub ActualizarDestino(ByVal destino As String, ByVal id As String, ByVal num_viaje As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UDV"
            comando.Parameters.Add("@cod_destino", SqlDbType.NVarChar)
            comando.Parameters("@cod_destino").Value = destino
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = num_viaje
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@obs", SqlDbType.NVarChar)
            comando.Parameters("@obs").Value = Trim(txtOBS.Text)
            comando.Parameters.Add("@lat_viaje", SqlDbType.Float)
            comando.Parameters("@lat_viaje").Value = hdnLatViaje.Value
            comando.Parameters.Add("@lon_viaje", SqlDbType.Float)
            comando.Parameters("@lon_viaje").Value = hdnLonViaje.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDestino", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarRecursos(ByVal tipo As String, ByVal recurso As String, ByVal id As String, ByVal num_viaje As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "URV"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = tipo
            comando.Parameters.Add("@recurso", SqlDbType.NVarChar)
            comando.Parameters("@recurso").Value = recurso
            comando.Parameters.Add("@obs", SqlDbType.NVarChar)
            comando.Parameters("@obs").Value = Trim(txtOBS.Text)
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = num_viaje
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@lat_viaje", SqlDbType.Float)
            comando.Parameters("@lat_viaje").Value = hdnLatViaje.Value
            comando.Parameters.Add("@lon_viaje", SqlDbType.Float)
            comando.Parameters("@lon_viaje").Value = hdnLonViaje.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarRecursos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnActRecursosArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCerrarViaje.Visible = False
        pnlCancelarViaje.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = True
        pnlConductor.Visible = False
        txtOBS.Visible = True
        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        lblTituloActualizar.Text = "ACTUALIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        txtOBS.Text = ""

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub btnActRecursosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCerrarViaje.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = True
        txtOBS.Visible = True
        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        lblTituloActualizar.Text = "ACTUALIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        txtOBS.Text = ""

        txtRutconductor.Text = ""
        txtFono.Text = ""
        cboConductor.ClearSelection()
        Call Cargarconductores()

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Private Sub Cargarconductores()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_programacion 'CHO' "
            cboConductor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductor.DataTextField = "nom_conductor"
            cboConductor.DataValueField = "rut_conductor"
            cboConductor.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargarconductores", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cboConductor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConductor.SelectedIndexChanged
        Call RescatarDatosConductorPRO(cboConductor.SelectedValue)

    End Sub


    Protected Sub btnActRecursosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCerrarViaje.Visible = False
        pnlCancelarViaje.Visible = False
        pnlCamion.Visible = True
        pnlArrastre.Visible = False
        pnlConductor.Visible = False
        txtOBS.Visible = True
        hdnIdServicio.Value = CType(row.FindControl("hdnCodServicio"), HiddenField).Value
        hdnFechaServicio.Value = CType(row.FindControl("lblFecOrigen"), Label).Text
        hdnhoraservicio.Value = CType(row.FindControl("lblHoraOrigen"), Label).Text
        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        Call CargarGrupoOperativo()

        ' Call CargarPatentes(CType(row.FindControl("hdnGrupoOperativo"), HiddenField).Value)

        Call CargarPatentes(cboGO.SelectedValue)

        lblTituloActualizar.Text = "ACTUALIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        txtOBS.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Private Sub CargarPatentes(ByVal cla_operativa As String)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CPT', '" & cla_operativa & "','" & hdnIdServicio.Value & "', '" & CDate(hdnFechaServicio.Value) & "',  '" & hdnhoraservicio.Value & "', '" & Session("cod_usu_tc") & "'"
            cboCamion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCamion.DataTextField = "cod_camion"
            cboCamion.DataValueField = "nom_patente"
            cboCamion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPatentes", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cboCamion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCamion.SelectedIndexChanged
        If cboCamion.SelectedValue = "0-SELECCIONE" Then
            txtArrastre.Text = ""
            txtArrastre.Enabled = False
            txtRutconductor.Text = ""
            ' txtNomconductor.Text = ""
            txtRutconductor.Enabled = False
            ' btnVerDocCamion.Visible = False
            btnVerDocConductor.Visible = False
            ' btnVerDocArrastre.Visible = False
            imgGPSArrastre.Visible = False
            imgGPSCamion.Visible = False
            txtArrastre.BackColor = System.Drawing.Color.White
            cboCamion.BackColor = System.Drawing.Color.White
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtFono.Text = ""
            txtFono.Enabled = False
            lblTrasnportista.Text = ""
        Else
            Call CargarDatosCamionPRO()
            txtArrastre.Enabled = True
            txtRutconductor.Enabled = True
            'btnVerDocCamion.Visible = True
            btnVerDocConductor.Visible = True
            '  btnVerDocArrastre.Visible = True
            imgGPSArrastre.Visible = True
            imgGPSCamion.Visible = True
            txtFono.Enabled = True
        End If
    End Sub


    Private Sub CargarDatosCamionPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'RDC', '" & Trim(Replace(cboCamion.SelectedValue, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    cboCamion.SelectedValue = Trim(rdoReader(0).ToString)
                    Call VerificarGPSCamionPRO()
                    Call VerificarDoctosCamion(cboCamion.SelectedValue, cboCamion)

                    hdnCodConductor.Value = Trim(rdoReader(1).ToString)
                    'txtArrastre.Text = Trim(rdoReader(4).ToString)
                    ' Call VerificarGPSArrastrePRO()

                    'Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)

                    ' hdnRutconductor.Value = Trim(rdoReader(3).ToString)
                    ' txtRutconductor.Text = Trim(rdoReader(3).ToString)
                    ' txtNomconductor.Text = Trim(rdoReader(2).ToString)
                    ' txtFono.Text = Trim(rdoReader(5).ToString)
                    hdnCodTrasnportista.Value = Trim(rdoReader(6).ToString)
                    lblTrasnportista.Text = Trim(rdoReader(7).ToString)

                    ' Call VerificarDoctosConductorPRO(hdnRutconductor.Value, txtRutconductor)

                    'If Trim(txtArrastre.Text) = "" Then
                    '    btnVerDocArrastre.Visible = False
                    'Else
                    '    btnVerDocArrastre.Visible = True
                    'End If

                    If Trim(txtRutconductor.Text) = "" Then
                        btnVerDocConductor.Visible = False
                    Else
                        btnVerDocConductor.Visible = True
                    End If

                    'btnVerDocCamion.Visible = True
                Else
                    MessageBox("Camión NO Valido", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                    txtArrastre.Text = ""
                    hdnRutconductor.Value = 0
                    ' txtNomconductor.Text = ""
                    txtFono.Text = ""
                    'btnVerDocCamion.Visible = False
                    ' btnVerDocArrastre.Visible = False
                    btnVerDocConductor.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamionPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub



    Private Sub VerificarGPSCamionPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(cboCamion.SelectedValue, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSCamion.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 3 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSCamion.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosCamion(ByVal patente As String, ByRef txt As DropDownList)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarGPSArrastrePRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(txtArrastre.Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSArrastre.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 3 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSArrastre.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSArrastre", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosArrastre(ByVal patente As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    MessageBox("Arrastre", "Arrastre NO registrado en Msfot, comicarse con operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarDoctosConductorPRO(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub txtArrastre_TextChanged(sender As Object, e As EventArgs) Handles txtArrastre.TextChanged
        If Trim(txtArrastre.Text) = "" Then
            txtArrastre.BackColor = System.Drawing.Color.White
            imgGPSArrastre.Visible = False
            ' btnVerDocArrastre.Visible = False

        Else
            ' btnVerDocArrastre.Visible = True
            Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)
            Call VerificarGPSArrastrePRO()

        End If
    End Sub

    Protected Sub txtRutconductor_TextChanged(sender As Object, e As EventArgs) Handles txtRutconductor.TextChanged

        If Trim(txtRutconductor.Text) = "" Then
            txtRutconductor.BackColor = System.Drawing.Color.White
            'txtNomconductor.Text = ""
            txtFono.Text = ""
            lblTrasnportista.Text = ""
            btnVerDocConductor.Visible = False
        Else

            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), ".", "")
            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), "-", "")
            If Len(Trim(txtRutconductor.Text)) = 8 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 7) + "-" + Right(Trim(txtRutconductor.Text), 1)
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 8) + "-" + Right(Trim(txtRutconductor.Text), 1)
            End If

            If Len(Trim(txtRutconductor.Text)) = 10 Then
                txtRutconductor.Text = Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    ' txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO(txtRutconductor.Text)
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    ' txtNomconductor.Text = ""
                End If
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = "0" + Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    ' txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO(txtRutconductor.Text)
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    ' txtNomconductor.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub RescatarDatosConductorPRO(ByVal rut As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'BDC', '" & rut & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtRutconductor.Text = Trim(rdoReader(0).ToString)
                    ' txtNomconductor.Text = Trim(rdoReader(1).ToString)
                    txtFono.Text = Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(txtRutconductor.Text, txtRutconductor)
                    btnVerDocConductor.Visible = True
                Else
                    btnVerDocConductor.Visible = False
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    'Protected Sub btnVerDocCamion_Click(sender As Object, e As EventArgs) Handles btnVerDocCamion.Click
    '    Call CargarGrillaDocumentos(1, Trim(Replace(cboCamion.SelectedValue, "-", "")))
    '    lblNomModal.Text = "PATENTE CAMION: "
    '    lblIdentificador.Text = Trim(Replace(cboCamion.SelectedValue, "-", ""))
    '    ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    'End Sub

    'Protected Sub btnVerDocArrastre_Click(sender As Object, e As EventArgs) Handles btnVerDocArrastre.Click
    '    Call CargarGrillaDocumentos(1, Trim(Replace(txtArrastre.Text, "-", "")))
    '    lblNomModal.Text = "PATENTE ARRASTRE: "
    '    lblIdentificador.Text = Trim(Replace(txtArrastre.Text, "-", ""))
    '    ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    'End Sub

    Protected Sub btnVerDocConductor_Click(sender As Object, e As EventArgs) Handles btnVerDocConductor.Click
        Call CargarGrillaDocumentos(2, Trim(txtRutconductor.Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = cboConductor.SelectedItem.ToString
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub cboClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub

    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs) Handles txtDesde.TextChanged
        If CDate(txtDesde.Text) > CDate(txtHasta.Text) Then
            txtHasta.Text = txtDesde.Text
        End If

        Call CargarGrillaViajes()
        Call CargarGrillaAlertas()
        Call CargarKPI()
    End Sub

    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs) Handles txtHasta.TextChanged
        If CDate(txtHasta.Text) < CDate(txtDesde.Text) Then
            txtDesde.Text = txtHasta.Text
        End If

        Call CargarGrillaViajes()
        Call CargarGrillaAlertas()
        Call CargarKPI()
    End Sub

    Protected Sub TimerViajes_Tick(sender As Object, e As EventArgs) Handles TimerViajes.Tick
        Call CargarGrillaViajes()
        Call CargarGrillaAlertas()
        Call CargarKPI()

        ' MessageBox("Alertas Pendientes", lblAlertasPendientes.Text, Page, Master, "I")
    End Sub

    'Protected Sub TimerAlerta_Tick(sender As Object, e As EventArgs) Handles TimerAlerta.Tick
    '    Call CargarGrillaAlertas()
    'End Sub

    Protected Sub cboServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicio.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub

    Protected Sub cboTrasportista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTrasportista.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub

    Protected Sub cboPOD_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPOD.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub

    Protected Sub cboAlertas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAlertas.SelectedIndexChanged
        Call CargarKPI()
        Call CargarGrillaAlertas()
    End Sub

    Protected Sub txtExp_TextChanged(sender As Object, e As EventArgs) Handles txtExp.TextChanged
        Call CargarGrillaViajes()
        hdnIdViaje.Value = Trim(txtExp.Text)
        Call CargarGrillaAlertas()
    End Sub

    Protected Sub txtGuia_TextChanged(sender As Object, e As EventArgs) Handles txtGuia.TextChanged
        Call CargarGrillaViajes()
    End Sub


    Protected Sub grdViajes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdViajes.SelectedIndexChanged

    End Sub

    Protected Sub cboEstadoAlertas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEstadoAlertas.SelectedIndexChanged
        Call CargarGrillaAlertas()
    End Sub

    Protected Sub cboEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEstado.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub

    Protected Sub cboOrigen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOrigen.SelectedIndexChanged
        Call CargarGrillaViajes()
        Call CargarKPI()
    End Sub


    Protected Sub InfoTipoAlertaClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertasTipo.Rows(rowIndex)

        hdnTipoAlertaGrillaInformacion.Value = CType(row.FindControl("hdnIdAlerta"), HiddenField).Value
        lblTitAlertasTipo.Text = CType(row.FindControl("btnTipoAlerta"), LinkButton).Text & " :"

        Call CargarInformacionAlerta()
        Call CargarGrillaAlertasViajes()
    End Sub



    ' CICLO LOGISTICO -------------------------------------------------------------------------------------------------------------------------------

    Private Sub CargarInformacionCiclo()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec rpt_ciclo_logistico 'CTT', '" & CDate(txtDesdeCiclo.Text) & "', '" & CDate(txtHastaciclo.Text) & "', '" & cboClientesCiclo.SelectedValue & "', '" & Session("cod_usu_tc") & "' "
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblMinimoOrigen.Text = Trim(rdoReader(0).ToString)
                    lblMaximoOrigen.Text = Trim(rdoReader(1).ToString)
                    lblMinimoruta.Text = Trim(rdoReader(2).ToString)
                    lblMaximoRuta.Text = Trim(rdoReader(3).ToString)
                    lblMinimoDestino.Text = Trim(rdoReader(4).ToString)
                    lblMaximoDestino.Text = Trim(rdoReader(5).ToString)
                    lblPromedioOrigen.Text = Trim(rdoReader(6).ToString)
                    lblPromedioRuta.Text = Trim(rdoReader(7).ToString)
                    lblPromedioDestino.Text = Trim(rdoReader(8).ToString)

                End If
            Catch ex As Exception
                MessageBoxError("CargarInformacionCiclo", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub txtDesdeCiclo_TextChanged(sender As Object, e As EventArgs) Handles txtDesdeCiclo.TextChanged
        If CDate(txtDesdeCiclo.Text) > CDate(txtHastaciclo.Text) Then
            txtHastaciclo.Text = txtDesdeCiclo.Text
        End If
        Call CargarKPICiclo()
        Call CargarInformacionCiclo()
    End Sub

    Protected Sub txtHastaciclo_TextChanged(sender As Object, e As EventArgs) Handles txtHastaciclo.TextChanged
        If CDate(txtHastaciclo.Text) < CDate(txtDesdeCiclo.Text) Then
            txtDesdeCiclo.Text = txtHastaciclo.Text
        End If

        Call CargarKPICiclo()
        Call CargarInformacionCiclo()
    End Sub

    Protected Sub cboClientesCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientesCiclo.SelectedIndexChanged
        Call CargarInformacionCiclo()
        Call CargarKPICiclo()
    End Sub





    Function GraficoOrigen() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("nom_dia", GetType(String)))
        Datos.Columns.Add(New DataColumn("min_origen", GetType(String)))
        Datos.Columns.Add(New DataColumn("max_origen", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GEO', '" & cboClientesCiclo.SelectedValue & "', '" & Session("cod_usu_tc") & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1), rdoReader(2)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['Dia', 'Tiempo (MINUTOS) Minimo ', 'Tiempo (MINUTOS) Máximo'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1) & ", " & dr(2)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    Function GraficoRUTA() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("nom_dia", GetType(String)))
        Datos.Columns.Add(New DataColumn("min_ruta", GetType(String)))
        Datos.Columns.Add(New DataColumn("max_ruta", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GER', '" & cboClientesCiclo.SelectedValue & "', '" & Session("cod_usu_tc") & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1), rdoReader(2)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['Dia', 'Tiempo (MINUTOS) Minimo ', 'Tiempo (MINUTOS) Máximo'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1) & ", " & dr(2)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    Function GraficoDESTINO() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("nom_dia", GetType(String)))
        Datos.Columns.Add(New DataColumn("min_destino", GetType(String)))
        Datos.Columns.Add(New DataColumn("max_destino", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GED', '" & cboClientesCiclo.SelectedValue & "', '" & Session("cod_usu_tc") & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1), rdoReader(2)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['Dia', 'Tiempo (MINUTOS) Minimo ', 'Tiempo (MINUTOS) Máximo'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1) & ", " & dr(2)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    ' ALERTAS -------------------------------------------------------------------------------------------------------------------------------------------------------
    Private Sub CargarGrillaAlertasViajes()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec rpt_ciclo_logistico 'GVA', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "','" & hdnTipoAlertaGrillaInformacion.Value & "'"
            grdViajesAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdViajesAlertas.DataBind()
            lblTotRegistrosRelacioandos.Text = grdViajesAlertas.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertasViajes", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaAlertasTipo()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec rpt_ciclo_logistico 'ATI', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "','" & hdnTipoAlertaGrillaInformacion.Value & "'"
            grdAlertasTipo.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertasTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertasTipo", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarInformacionAlerta()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec rpt_ciclo_logistico 'TAL', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & hdnTipoAlertaGrillaInformacion.Value & "' "
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblTotalAlertasMesActual.Text = Trim(rdoReader(0).ToString) & ""
                End If
            Catch ex As Exception
                MessageBoxError("CargarInformacionAlerta", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub btnExportarRelacionados_Click(sender As Object, e As EventArgs) Handles btnExportarRelacionados.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String

            strSQL = "Exec rpt_ciclo_logistico 'GVA', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "','" & hdnTipoAlertaGrillaInformacion.Value & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Viajes_Relacionados.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarRelacionados_Click", Err, Page, Master)
        End Try
    End Sub


    Function EvolutivoAlertas() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("num_dia", GetType(String)))
        Datos.Columns.Add(New DataColumn("cant_mes_ant", GetType(String)))
        Datos.Columns.Add(New DataColumn("cant_mes_actual", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GA','" & Session("cod_usu_tc") & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1), rdoReader(2)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['Dia', 'Cantidad Mes ANTERIOR', 'Cantidad Mes ACTUAL'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1) & ", " & dr(2)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    Function CargarGraficoMapaCalor() As String
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strDatos As String = ""

        comando.CommandText = "Exec pro_gestion_alertas 'MCA', '" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & Trim(cboClientealertas.SelectedValue) & "', '" & Session("cod_usu_tc") & "', '" & hdnTipoAlertaGrillaInformacion.Value & "'"
        comando.Connection = conx
        comando.CommandTimeout = 90000
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            strDatos = rdoReader(0).ToString
        End If

        rdoReader.Close()
        conx.Close()

        Return strDatos

    End Function

    Function GraficoGestionadas() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("NOM_TIPO", GetType(String)))
        Datos.Columns.Add(New DataColumn("total", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GAG','" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & hdnTipoAlertaGrillaInformacion.Value & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['NOM_TIPO', 'total'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    Function GraficoCriticidad() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("criticidad", GetType(String)))
        Datos.Columns.Add(New DataColumn("total", GetType(String)))

        strSCodU = "Exec rpt_ciclo_logistico 'GCR','" & CDate(txtDesdeAlertas.Text) & "', '" & CDate(txtHastaAlertas.Text) & "', '" & cboClientealertas.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & hdnTipoAlertaGrillaInformacion.Value & "'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        strDatos = "[['TIPO', 'Cantidad'],"

        For Each dr As DataRow In Datos.Rows
            strDatos = strDatos & "["
            strDatos = strDatos & "'" & dr(0) & "'" & "," & dr(1)
            strDatos = strDatos & "],"
        Next
        strDatos = strDatos & "]"
        Return strDatos
    End Function


    Private Sub CargarGrillaTiempos()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'CGR', '" & CDate(txtDesdeCiclo.Text) & "', '" & CDate(txtHastaciclo.Text) & "', '" & cboClientesCiclo.SelectedValue & "', '" & cboServiciosCiclo.SelectedValue & "', '" & cboTransportistaCiclo.SelectedValue & "', '" & cboPODCiclo.SelectedValue & "', '" & Session("cod_usu_tc") & "', '" & cboEstadoCiclo.SelectedValue & "', '" & cboOrigenCiclo.SelectedValue & "'"
            grdTiempos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTiempos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTiempos", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarCarga()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCTC'"
            cboBCargas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboBCargas.DataTextField = "nom_carga"
            cboBCargas.DataValueField = "cod_carga"
            cboBCargas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoCarga", Err, Page, Master)
        End Try
    End Sub
    Protected Sub CargarGrillaBaremos(ByVal id As Integer)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'CGBAR', " & id & ""
            grdBar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdBar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaBaremos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub InsertarBaremos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "INB"
            comando.Parameters.Add("@num_viaje", SqlDbType.Int)
            comando.Parameters("@num_viaje").Value = hdnViajeBaremos.Value
            comando.Parameters.Add("@num_secuencia", SqlDbType.Int)
            comando.Parameters("@num_secuencia").Value = cboBPuntoPaso.SelectedValue
            comando.Parameters.Add("@valor_baremos", SqlDbType.Int)
            comando.Parameters("@valor_baremos").Value = txtBValor.Text
            comando.Parameters.Add("@tipo_carga", SqlDbType.Int)
            comando.Parameters("@tipo_carga").Value = cboBCargas.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("InsertarBaremos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnBaremos_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        hdnViajeBaremos.Value = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        hdnSecuenciaBaremos.Value = CType(row.FindControl("hdnNumSecuencia"), HiddenField).Value
        Call CargarGrillaBaremos(hdnViajeBaremos.Value)
        Call CargarPuntos(hdnViajeBaremos.Value)
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "mdlBar", "$('#mdlBar').modal();", True)
    End Sub

    Protected Sub btnGuardarBaremos_Click(sender As Object, e As EventArgs)
        Try
            Call InsertarBaremos()
        Catch ex As Exception
            MessageBox("Insertar", "Registro guardado", Page, Master, "S")
        End Try
    End Sub

    Protected Sub btnInsertarBaremos_Click(sender As Object, e As EventArgs)
        Try
            Call InsertarBaremos()
            txtBValor.Text = ""
            cboBCargas.ClearSelection()
            cboBPuntoPaso.ClearSelection()
            Call CargarGrillaBaremos(hdnViajeBaremos.Value)
            MessageBox("Insertar", "Registro guardado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("BtnGuardarBaremos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarBaremos(ByVal id As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "DELB"
            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("BorrarBaremos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CargarPuntos(ByVal id As Integer)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCSEC'," & id & ""
            cboBPuntoPaso.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboBPuntoPaso.DataTextField = "nom_paso"
            cboBPuntoPaso.DataValueField = "num_secuencia"
            cboBPuntoPaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPuntosdePaso", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnBBorrar_Click(sender As Object, e As EventArgs)

        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdBar.Rows(rowIndex)
        Dim id As Integer = CType(row.FindControl("hdnIDBaremos"), HiddenField).Value
        Call EliminarBaremos(id)
        Call CargarGrillaBaremos(hdnViajeBaremos.Value)
        MessageBox("Eliminar", "Registro eliminado", Page, Master, "S")

    End Sub

    Protected Sub txtDesdeAlertas_TextChanged(sender As Object, e As EventArgs) Handles txtDesdeAlertas.TextChanged
        If CDate(txtDesdeAlertas.Text) > CDate(txtHastaAlertas.Text) Then
            txtHastaAlertas.Text = txtDesdeAlertas.Text
        End If

        Call CargarGrillaAlertasTipo()
        Call CargarInformacionAlerta()
        Call CargarKPIAlertas()
        Call CargarGrillaAlertasViajes()
    End Sub

    Protected Sub txtHastaAlertas_TextChanged(sender As Object, e As EventArgs) Handles txtHastaAlertas.TextChanged
        If CDate(txtHastaAlertas.Text) < CDate(txtDesdeAlertas.Text) Then
            txtDesdeAlertas.Text = txtHastaAlertas.Text
        End If

        Call CargarGrillaAlertasTipo()
        Call CargarInformacionAlerta()
        Call CargarKPIAlertas()
        Call CargarGrillaAlertasViajes()
    End Sub

    Protected Sub cboClientealertas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientealertas.SelectedIndexChanged
        Call CargarGrillaAlertasTipo()
        Call CargarInformacionAlerta()
        Call CargarKPIAlertas()
        Call CargarGrillaAlertasViajes()
    End Sub

    Protected Sub btnLimpiarFiltros_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltros.Click
        hdnTipoAlertaGrillaInformacion.Value = 0
        cboClientealertas.ClearSelection()
        lblTitAlertasTipo.Text = "TOTAL ALERTAS: "
        Call CargarInformacionAlerta()
        Call CargarGrillaAlertasTipo()
        Call CargarKPIAlertas()
        Call CargarGrillaAlertasViajes()
    End Sub

    Protected Sub cboServiciosCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServiciosCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboTransportistaCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTransportistaCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboPODCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPODCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboEstadoCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEstadoCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboOrigenCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOrigenCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboAlertasCiclo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAlertasCiclo.SelectedIndexChanged
        Call CargarKPICiclo()
    End Sub

    Protected Sub cboDestinoKPI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDestinoKPI.SelectedIndexChanged
        Call CargarGrillaViajes()
    End Sub

    Protected Sub btnLimpiarFiltroGral_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltroGral.Click
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")


        cboClientes.ClearSelection()
        cboServicio.ClearSelection()
        cboTrasportista.ClearSelection()
        cboAlertas.ClearSelection()
        cboEstadoAlertas.ClearSelection()
        cboEstado.ClearSelection()
        cboOrigen.ClearSelection()
        hdnIdViaje.Value = 0
        txtExp.Text = ""
        txtGuia.Text = ""

        Call CargarGrillaViajes()
        Call CargarGrillaAlertas()

        Call CargarKPI()
    End Sub

    Protected Sub btnLimpiarAlertas_Click(sender As Object, e As EventArgs) Handles btnLimpiarAlertas.Click
        hdnIdViaje.Value = 0
        cboAlertas.ClearSelection()
        cboEstadoAlertas.ClearSelection()
        cboOrigen.ClearSelection()
        Call CargarGrillaAlertas()
    End Sub

    Protected Sub btnLimpiarFiltrosCiclo_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltrosCiclo.Click

        txtDesdeCiclo.Text = Session("fec_actual")
        txtHastaciclo.Text = Session("fec_actual")
        cboClientesCiclo.ClearSelection()

        Call CargarInformacionCiclo()
        Call CargarKPICiclo()
        Call CargarGrillaTiempos()


    End Sub
End Class