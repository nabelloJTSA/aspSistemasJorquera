﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspFichaViaje
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then

                If Session("ficha_HR") = 1 Then
                    btnASignar.Visible = False
                    btnVolverHR.Visible = True
                End If

                If Session("ficha_FACT") = 1 Then
                    btnASignar.Visible = False
                    btnVolverFac.Visible = True
                End If

                Session("est_asignado_tc") = 0
                txtNumViaje.Text = Session("num_viaje_tc")
                txtTransportista.Text = Session("transportista_tc")
                txtOrigen.Text = Session("origen_tc")
                txtDestino.Text = Session("destino_tc")
                txtCamion.Text = Session("camion_tc")
                txtArrastre.Text = Session("arrastre_tc")
                txtConductor.Text = Session("conductor_tc")
                txtRut.Text = Session("rut_conductor_tc")
                txtScore.Text = Session("score")
                txtEtaOrigen.Text = Session("eta_TC")

                Call CargarGrillaEstados()
                Call CargarGrillaAlertas()
                Call CargarGrillaIncidencias()
                Call CargarGrillaPOD()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaPOD()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_pod 'CGD', '" & Session("num_viaje_tc") & "'"
            grdPOD.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPOD.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPOD", Err, Page, Master)
        End Try
    End Sub

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(Session("lat_ficha_tc"), ",", ".") & "" & "," & Replace(Session("lon_ficha_tc"), ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function


    Private Sub CargarGrillaAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'AFV', '" & Session("num_viaje_tc") & "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertas", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarGrillaEstados()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CGE', '" & Session("num_viaje_tc") & "'"
            grdEstados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdEstados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEstados", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaIncidencias()
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CGI', '" & Session("num_viaje_tc") & "'"
            grdIncidencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIncidencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaIncidencias", Err, Page, Master)
        End Try
    End Sub

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>Camión: </b> " & Session("camion_tc") & "<br /><b>Arrastre: </b> " & Session("arrastre_tc") & " <br /><b>Conductor: </b> " & Session("conductor_tc") & "<br /><b>Fono: </b> " & Session("nun_fono_tc") & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    Function VerImagenMapa() As String
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim URL As String = ""

        comando.CommandText = "exec pro_gestion_alertas 'MAP','" & Session("camion_tc") & "'"
        comando.Connection = conx
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            URL = rdoReader(0).ToString
        End If

        rdoReader.Close()
        conx.Close()

        Return URL
    End Function

    Private Sub DescargarFTP(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/POD_VIAJES/Viaje_" & Session("num_viaje_tc") & "/" & Trim(nombre), Server.MapPath(archivo), "usuarioftp", "Ftp2018.")

        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub

    Protected Sub DescargarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPOD.Rows(rowIndex)

        Dim archivo As String = "~/Temp/" & CType(row.FindControl("lblNomTipo"), Label).Text

        Call DescargarFTP(archivo, CType(row.FindControl("lblNomTipo"), Label).Text)

        Response.ContentType = "application/ms-word"
        Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(row.FindControl("lblNomTipo"), Label).Text)
        Response.TransmitFile(Server.MapPath(archivo))
        Response.End()
    End Sub


    Protected Sub verGestionesClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)

        Call CargarGrillaGestionessPopUp(CType(row.FindControl("hdnID"), HiddenField).Value)

        lblTituloIncidencias.Text = "GESTIONES REALIZADAS VIAJE: " & CType(row.FindControl("lblNomAlerta"), Label).Text & "/ ID ALERTA: " & CType(row.FindControl("hdnID"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalGestiones();", True)
    End Sub

    Private Sub CargarGrillaGestionessPopUp(ByVal id_call_center As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_gestion_viajes 'GEA', '" & id_call_center & "'"
            grdGestiones.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdGestiones.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaGestionessPopUp", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdAlertas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAlertas.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If CType(row.FindControl("hdnEstAlerta"), HiddenField).Value = 3 Then
                    CType(row.FindControl("btnGestiones"), LinkButton).Visible = True
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdAlertas_RowDataBound", Err, Page, Master)
        End Try
    End Sub


End Class