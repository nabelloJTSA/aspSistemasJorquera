﻿<%@ Page Title="" Language="vb" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspIncidencias.aspx.vb" Inherits="aspSistemasJorquera.aspIncidencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <section class="content">
                <div class="row">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text="REGISTRO INCIDENCIA"></asp:Label>
                        </h3>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnVolver" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx">Volver</asp:LinkButton>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <!-- left column -->
                    <div class="col-md-4">
                        <div class="box  box-success">
                            <div class="box-header with-border bg-success">
                                <h3 class="box-title">Información del Viaje</h3>
                            </div>
                            <div class="box-body">
                                <div class="row btn-sm">

                                    <div class="col-xs-12">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">N° VIAJE</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtNumViaje" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">TRANSPORTISTA</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtTransportista" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">CLIENTE</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtCliente" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">PRODUCTO</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtProducto" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">ORIGEN:</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtOrigen" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">DESTINO</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtDestino" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">CAMION:</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtCamion" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">ARRASTRE:</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtArrastre" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">CONDUCTOR:</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtConductor" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">RUT</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtRut" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">SERVICIO</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtServicio" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">G. OPERATIVO</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtGrupoOperativo" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">SUPERVISOR</label>

                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtSupervisor" Enabled="false" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="box box-success">
                            <div class="box-header with-border bg-success">
                                <h3 class="box-title">Ingreso Incidencia</h3>
                            </div>

                            <div class="box-body">
                                <div class="row btn-sm">

                                    <div class="col-xs-7">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">FEC. LLAMADA:</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtFecLlamada" TextMode="Date" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">FEC. SUCESO:</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtFecSuceso" TextMode="Date" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">LUGAR:</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtLugar" CssClass="form-control" Text="" runat="server"></asp:TextBox>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">DENUNCIANTE</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtDenunciante" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">INCIDENCIA</label>

                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="cboIncidencia" CssClass="form-control" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">EST. CONDUCTOR</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtEstConductor" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="col-xs-5">

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">HORA LLAMADA:</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtHoraLlamada" TextMode="Time" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">HORA SUCESO:</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtHoraSuceso" TextMode="Time" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">TIPO LUGAR:</label>

                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="cboTipoLugar" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Ruta</asp:ListItem>
                                                    <asp:ListItem Value="2">Planta</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">FONO</label>

                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtFonoDenunciante" CssClass="form-control" Text="" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">TERCEROS</label>

                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="cboTerceros" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">NO</asp:ListItem>
                                                    <asp:ListItem Value="1">SI</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row btn-sm">

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtObs" Enabled="true" TextMode="MultiLine" CssClass="form-control" placeholder="Descripción Incidencia......" runat="server" required></asp:TextBox>

                                        </div>
                                        <div class="box-footer">
                                            <asp:Button ID="btnGuardar" CssClass="btn btn-primary pull-right" runat="server" Text="Guardar" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="box box-success">
                            <div class="box-header with-border bg-success">
                                <h3 class="box-title">Bitacora Incidencias</h3>
                            </div>

                            <div class="box-body">
                                <div class="row ">

                                    <div class="col-xs-12">
                                        <asp:GridView ID="grdIncidencias" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                            <Columns>

                                                <asp:TemplateField HeaderText="TIPO INCIDENCIA">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTipoIncidencia" runat="server" Text='<%# Bind("tipo_incidencia")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="OBSERVACIONES">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblObsIncidencia" runat="server" Text='<%# Bind("obs")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="FECHA">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecIncidencia" runat="server" Text='<%# Bind("fec_incidencia")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--/.col (right) -->
                </div>


                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">

                        <div id="map" class="table-responsive" style="width: 100%; height: 400px;">
                        </div>

                        <script>                                               
                                
                            var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                maxZoom: 18,
                                attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                    '<a href="https://www.mapbox.com/">Mapbox</a>',
                                id: 'mapbox/streets-v11',
                                tileSize: 512,
                                zoomOffset: -1
                            }).addTo(mymap);

                            L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>").openPopup();
                                                                                                                               

                        </script>


                    </div>



                </div>




            </section>
            <!-- /.content -->



            <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
            <asp:HiddenField ID="hdnCodProducto" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnCodGO" runat="server"></asp:HiddenField>




        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>



</asp:Content>



