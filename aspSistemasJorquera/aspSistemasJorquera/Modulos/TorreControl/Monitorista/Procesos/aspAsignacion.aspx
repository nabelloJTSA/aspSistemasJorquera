﻿<%@ Page Title="" Language="vb" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAsignacion.aspx.vb" Inherits="aspSistemasJorquera.aspAsignacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportar" />

        </Triggers>

        <ContentTemplate>


            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>


            <script type='text/javascript'>
                function openModalMultipunto() {
                    $('[id*=mdlDetalleMultipunto]').modal('show');
                }
            </script>


            <style type="text/css">
                #global {
                    height: 250px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                #global1 {
                    height: 450px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes2 {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>



            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-md-4 form-group">
                        <h3>
                            <asp:Label ID="lbltitulo" runat="server" Text="ASIGNACIÓN"></asp:Label>
                            <small>Planificador</small>
                        </h3>
                    </div>

                    <div class="col-md-1 form-group pull-right">
                        <label for="fname"
                            class="control-label col-form-label">
                        </label>
                        <div>
                            <asp:LinkButton ID="btnASignar" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Procesos/aspIngDemandaP.aspx">Demandas</asp:LinkButton>
                        </div>

                    </div>
                </div>

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">

                        <div class="row btn-sm">

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa  fa-ticket"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">DEMANDAS</span>
                                        <h4>
                                            <asp:Label ID="lbltotalDemandas" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnTotalKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa  fa-truck"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">OFERTA</span>
                                        <h4>
                                            <asp:Label ID="lblDisponibles" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnDisponiblesKPI" CssClass="small-box-footer text-primary " runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">ASIGNADOS</span>
                                        <h4>
                                            <asp:Label ID="lblAsignados" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnAsignadosKPI" CssClass="small-box-footer text-primary" runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>




                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-thumbs-o-down"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">SIN ASIGNACION</span>
                                        <h4>
                                            <asp:Label ID="lblNoAsignados" runat="server" Font-Bold="true" Text=""></asp:Label></h4>
                                        <asp:LinkButton ID="btnNOAsignadosKPI" CssClass="small-box-footer text-primary  " runat="server">Ver Detalles <i class="fa fa-arrow-circle-right text-primary"></i></asp:LinkButton>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-INFO">
                                    <span class="info-box-icon"><i class="fa fa-line-chart"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">CUMPLIMIENTO</span>
                                        <h4>
                                            <asp:Label ID="lblCumplimiento" runat="server" Font-Bold="true" Text=""></asp:Label>
                                            <asp:Image ID="imgCumplViajes" ImageUrl="" Width="18px" Height="18px" runat="server" /></h4>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row btn-sm">
                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Desde</label>
                                <div>
                                    <asp:TextBox ID="txtDesde" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Hasta</label>
                                <div>
                                    <asp:TextBox ID="txtHasta" runat="server" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Familia</label>
                                <div>
                                    <asp:DropDownList ID="cboFamilia" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Servicio</label>
                                <div>
                                    <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Grupo Operativo</label>
                                <div>
                                    <asp:DropDownList ID="cboGO" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Cliente</label>
                                <div>
                                    <asp:DropDownList ID="cboClientes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>

                                    <asp:CheckBox ID="chkDemandas" runat="server" Visible="false" CssClass="checkbox-inline text-primary" AutoPostBack="true" Text=" VER TODAS LAS DEMANDAS" />

                                </div>
                            </div>


                            <%--      <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Turno</label>
                                <div>
                                    <asp:DropDownList ID="cboTurno" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>--%>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Estado Viaje</label>
                                <div>
                                    <asp:DropDownList ID="cboEstadoViajes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    Origen</label>
                                <div>
                                    <asp:DropDownList ID="cboOrigenes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                    OS/Expedición</label>
                                <div>

                                    <asp:TextBox ID="txtOsExp" CssClass="form-control" runat="server" AutoPostBack="True"></asp:TextBox>
                                </div>
                            </div>



                            <div class="col-md-1 form-group">
                                <label for="fname"
                                    class="control-label col-form-label">
                                </label>
                                <div>
                                    <asp:Button ID="btnLimpiarFiltros" runat="server" CssClass="btn btn-primary" Text="Limpiar Filtros" />
                                </div>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-4 form-group">

                                <b>
                                    <asp:Label ID="Label2" CssClass="text-primary" runat="server" Font-Size="Large" Text="TOTAL REGISTROS:"></asp:Label>
                                    <asp:Label ID="lblTotReg" CssClass="text-primary" runat="server" Font-Size="Large" Text=""></asp:Label></b>
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group pull-right">
                                <asp:LinkButton ID="btnExportar" CssClass="btn botonesTC-descargar btn-block" runat="server"> <i class="fa fa-download"></i> Tabla</asp:LinkButton>
                            </div>

                        </div>



                        <div class="row btn-sm table-responsive">
                            <div class="col-md-12">
                                <asp:GridView ID="grdAsignacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" AllowPaging="false" PageSize="4">
                                    <Columns>

                                        <asp:TemplateField HeaderText="OS/EXPEDICIÓN">
                                            <ItemTemplate>
                                                <h5>
                                                   <b>OS:</b> <asp:Label ID="lblOS" Font-Bold="true" runat="server" Text='<%# Bind("OS")%>'></asp:Label><br /><b>EX:</b> <asp:Label ID="lblNumViaje" Font-Bold="true" runat="server" Text='<%# Bind("num_viaje")%>'></asp:Label></h5>
                                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                <asp:HiddenField ID="hdnOS" runat="server" Value='<%# Bind("OS")%>' />
                                                <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />
                                                <asp:HiddenField ID="hdnCodServicio" runat="server" Value='<%# Bind("id_servicio")%>' />
                                                <asp:HiddenField ID="hdnEstadoviaje" runat="server" Value='<%# Bind("id_est_viaje")%>' />
                                                <asp:HiddenField ID="hdnCodcliente" runat="server" Value='<%# Bind("cod_cliente")%>' />
                                                <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                                <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                                <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("num_fono")%>' />
                                                <asp:HiddenField ID="hdnNomTrasnportista" runat="server" Value='<%# Bind("nom_trasnportista")%>' />
                                                <asp:HiddenField ID="hdnCodUnidad" runat="server" Value='<%# Bind("cod_unidad")%>' />
                                                <asp:HiddenField ID="hdnGrupoOperativo" runat="server" Value='<%# Bind("gruoo_operativo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="PRODUCTO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CLIENTE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblnomcliente" runat="server" Text='<%# Bind("nom_cliente")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="FECHA ORIGEN">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA ORIGEN">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtHoraOrigen" Text='<%# Bind("hor_origen_estimada")%>'  CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="FECHA DESTINO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="HORA DESTINO">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtHoraDestino" Text='<%# Bind("hor_destino_estimada")%>'  CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CAMIÓN">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocCamion" CssClass="small-box-footer text-primary" OnClick="btnVerDocCamion_Click" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                                <asp:TextBox ID="txtCamion" runat="server" AutoPostBack="true" placeholder="Camión..." Text='<%# Bind("nom_pat_camion")%>' Enabled="false" OnTextChanged="CargarDatosCamion" CssClass="form-control"></asp:TextBox>
                                                <b>
                                                    <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />


                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ARRASTRE">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocArrastre" CssClass="small-box-footer text-primary" OnClick="btnVerDocArrastre_Click" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                                <asp:TextBox ID="txtArrastre" runat="server" AutoPostBack="true" placeholder="Arrastre..." Text='<%# Bind("nom_pat_arrastre")%>' Enabled="false" OnTextChanged="GPSArrastre" CssClass="form-control"></asp:TextBox>
                                                <b>
                                                    <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerDocConductor" CssClass="small-box-footer text-primary" OnClick="btnVerDocConductor_Click" runat="server" Visible="true">DOCUMENTOS</asp:LinkButton>
                                                <asp:TextBox ID="txtRutconductor" runat="server" AutoPostBack="true" placeholder="Rut..." OnTextChanged="DoctosConductor" Enabled="false" Text='<%# Bind("rut_conductor")%>' CssClass="form-control"></asp:TextBox>
                                                <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                                <asp:Label ID="lblSlash" runat="server" Text="/" />
                                                <asp:Label ID="lblFono" runat="server" Text='<%# Bind("num_fono")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="TIPO VIAJE">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnTipoViaje" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="btnTipoViajeClick" Text='<%# Bind("tipo_viaje")%>'> </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ESTADO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" CssClass="text-primary" Font-Bold="true" runat="server" Text='<%# Bind("nom_est_viaje")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="SCORE">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:Label ID="lblScore" runat="server" Text='<%# Bind("score_conductor")%>' /></h4>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="btnAsignarPre" runat="server" Visible="false" OnClick="AsignarUT" CssClass="btn btn-primary  btn-sm" ToolTip="Asignar Recursos">ASIGNAR</asp:LinkButton>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnAsignarPre" />
                                                    </Triggers>
                                                </asp:UpdatePanel>


                                                <asp:LinkButton ID="btnOfertar" runat="server" Visible="false" OnClick="AsignarRecursosOferta" CssClass="btn btn-primary   btn-sm" ToolTip="Ofertar">ASIGNAR</asp:LinkButton>

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <h4>
                                                    <asp:UpdatePanel runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="btnCancelarAsignacion" Visible="false" runat="server" OnClick="CancelarUT" ToolTip="Cancelar"><i class="fa fa-trash"></i></asp:LinkButton>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnCancelarAsignacion" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </h4>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>

                                    </Columns>

                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                </asp:GridView>
                            </div>


                            <div>
                                <div class="modal fade bd-example-modal-lg" id="ModalDocumentos" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <!-- Modal content-->
                                        <div class="modal-content">

                                            <asp:Panel ID="pnlDoctos" runat="server" Visible="false">
                                                <div class="modal-header bg-success">
                                                    <h3 class="modal-title text-light">
                                                        <asp:Label ID="lblNomModal" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label><asp:Label ID="lblIdentificador" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                                    <h3>
                                                        <asp:Label ID="lblTrasnportista" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label></h3>
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <asp:GridView ID="grdDocumentacion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="DOCUMENTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("nom_docto") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("fec_vencimiento", "{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </asp:Panel>



                                            <asp:Panel ID="pnlKPI" runat="server" Visible="false">
                                                <div class="modal-header bg-success">
                                                    <h3 class="modal-title text-light">
                                                        <asp:Label ID="lblTituloKPI" CssClass="text-green" Font-Bold="true" runat="server" Text=""></asp:Label>

                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <asp:GridView ID="grdDemandasKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="OS">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblIdDemanda" runat="server" Text='<%# Eval("os") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CLIENTE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCliente" runat="server" Text='<%# Eval("nom_cliente") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ORIGEN">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOrigen" runat="server" Text='<%# Eval("nom_origen") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="FECHA/HORA ORIGEN">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFHOrigen" runat="server" Text='<%# Eval("FechaHoraOrigen") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="DESTINO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("nom_destino") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="FECHA/HORA DESTINO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFHDestino" runat="server" Text='<%# Eval("FechaHoraDestino") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="PRODUCTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblProducto" runat="server" Text='<%# Eval("nom_producto") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="SERVICIO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>


                                                    <asp:GridView ID="grdDisponiblesKPI" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="CAMIÓN">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcamion" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ARRASTRE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblArrastre" runat="server" Text='<%# Eval("nom_patente") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="CONDUCTOR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConductor" runat="server" Text='<%# Eval("conductor") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="SERVICIO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblServicio" runat="server" Text='<%# Eval("nom_servicio") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <!-- Modal CONFIRMACION -->
                            <div class="modal" id="mdlDetalleMultipunto" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                                <div class="modal-dialog  modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-body">
                                            <asp:UpdatePanel runat="server" ID="updClientes">
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnConfirmar" />
                                                </Triggers>
                                                <ContentTemplate>
                                                    <div class="modal-header bg-success">
                                                        <h3 class="modal-title text-light">
                                                            <asp:Label ID="Label1" CssClass="text-green" Font-Bold="true" runat="server" Text="Ordern De Servicio: "></asp:Label><asp:Label ID="lblNumOS" runat="server" CssClass="text-green" Text="-"></asp:Label></h3>
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:GridView ID="grdMultipaso" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>
                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("id")%>' />
                                                                        <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />



                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="HORA ORIGEN">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtHoraOrigen" Enabled="false" Text='<%# Bind("hor_origen_estimada")%>' CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="FECHA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="HORA DESTINO">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtHoraDestino" Enabled="false" Text='<%# Bind("hor_destino_estimada")%>' CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle />
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>

                                                    <asp:HiddenField ID="hdnEstViaje" runat="server"></asp:HiddenField>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                        <div class="modal-footer">

                                            <asp:Button ID="btnConfirmar" CssClass="btn btn-primary " runat="server" Text="ACTUALIZAR" />
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--fin modal--%>
                        </div>





                        <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
                        <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

                        <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

                        <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnFecRegistroGPS" runat="server"></asp:HiddenField>
                        <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
                        <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
                    </div>
                </div>

            </section>

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>

