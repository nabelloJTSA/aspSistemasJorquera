﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspAsigRecursos.aspx.vb" Inherits="aspSistemasJorquera.aspAsigRecursos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <script type='text/javascript'>
        function openModal() {
            $('[id*=MdlConfirmar]').modal('show');
        }
    </script>

    <script type='text/javascript'>
        function openModalOferta() {
            $('[id*=ModalOferta]').modal('show');
        }
    </script>


    <style type="text/css">
        #global {
            height: 250px;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes {
            height: auto;
        }

        #global1 {
            height: 450px;
            border: 1px solid #ddd;
            background: #f1f1f1;
            overflow-y: scroll;
        }

        #mensajes2 {
            height: auto;
        }

        .texto {
            padding: 4px;
            background: #fff;
        }
    </style>




    <!-- Main content -->
    <section class="content">
           
        <div class="row">
            <div class="col-md-11 form-group">
                <h3>
                    <%--<asp:Label ID="lbltitulo" runat="server" Text="Asignar Recursos"></asp:Label>--%>
                    <h3>
                        <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="text-primary" Text="ASIGNAR RECURSOS OS: "></asp:Label><asp:Label ID="lblNumOSOferta" runat="server" Font-Bold="true" CssClass="text-primary" Text=""></asp:Label></h3>
                    <asp:GridView ID="grdMultipaso" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                        <Columns>

                            <asp:TemplateField HeaderText="ORIGEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("nom_origen")%>'></asp:Label>

                                    <asp:HiddenField ID="hdnIdDemanda" runat="server" Value='<%# Bind("id_demanda")%>' />

                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA ORIGEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblFecOrigen" runat="server" Text='<%# Bind("fec_origen_estimada", "{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="HORA ORIGEN">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtHoraOrigen" Text='<%# Bind("hor_origen_estimada")%>' CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DESTINO">
                                <ItemTemplate>
                                    <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("nom_destino")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FECHA DESTINO">
                                <ItemTemplate>
                                    <asp:Label ID="lblFecDestino" runat="server" Text='<%# Bind("fec_destino_estimada", "{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="HORA DESTINO">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtHoraDestino" Text='<%# Bind("hor_destino_estimada")%>' CssClass="form-control" TextMode="time" runat="server"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PRODUCTO">
                                <ItemTemplate>
                                    <asp:Label ID="lblNomProducto" runat="server" Text='<%# Bind("nom_producto")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" />
                            </asp:TemplateField>


                        </Columns>
                       <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                    </asp:GridView>
                </h3>
            </div>

            <div class="col-md-1 form-group pull-right">
                <div>
                    <asp:LinkButton ID="btnVolver" CssClass="btn btn-primary btn-block" runat="server" PostBackUrl="/Modulos/TorreControl/Monitorista/Procesos/aspAsignacion.aspx">Volver</asp:LinkButton>
                </div>
            </div>
        </div>


        <div class="row btn-sm">
            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Grupo Operativo</label>
                <div>
                    <asp:DropDownList ID="cboGrupoOperativo" CssClass="form-control" OnSelectedIndexChanged="cboGrupoOperativo_SelectedIndexChanged1" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Conductor</label>
                <div>
                    <asp:DropDownList ID="cboConductor" CssClass="form-control" OnSelectedIndexChanged="cboConductor_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname"
                    class="control-label col-form-label">
                    Transportistas</label>
                <div>
                    <asp:DropDownList ID="cboTrasnportistas" OnSelectedIndexChanged="cboTrasnportistas_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>


        <!-- SELECT2 EXAMPLE -->

        <section class="content">
              <div class="row">
                    <div class="col-md-12 form-group">
                        <asp:Label ID="Label1" runat="server" ForeColor="#00cc00" Font-Bold="true" Text="GPS BUENO/DOCTOS AL DIA"></asp:Label>
                        <asp:Label ID="Label4" runat="server" ForeColor="#ff0000" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;    GPS SEÑAL TARDIA/DOCTOS VENCIDOS"></asp:Label>
                         <asp:Label ID="Label5" runat="server" ForeColor="#999999" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;   SIN GPS"></asp:Label>            
                    </div>
                </div>
            <div class="row btn-sm">
                <div class="col-md-9 form-group">

                    <div id="global1">
                        <div id="mensajes1">
                            <asp:GridView ID="grdOferta" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                <Columns>

                                    <asp:TemplateField HeaderText="CODIGO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodCamion" runat="server" Text='<%# Bind("cod_camion")%>' />

                                            <asp:HiddenField ID="hdnLon" runat="server" Value='<%# Bind("num_longitud")%>' />
                                            <asp:HiddenField ID="hdnNumImei" runat="server" Value='<%# Bind("num_imei")%>' />
                                            <asp:HiddenField ID="hdnFecUltimoRegistro" runat="server" Value='<%# Bind("fec_registro")%>' />
                                            <asp:HiddenField ID="hdnCodConductor" runat="server" Value='<%# Bind("cod_conductor")%>' />
                                            <asp:HiddenField ID="hdnFonoConductor" runat="server" Value='<%# Bind("fono_conductor")%>' />
                                            <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                            <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />
                                            <asp:HiddenField ID="hdnLatitud" runat="server" Value='<%# Bind("num_latitud")%>' />

                                            <asp:HiddenField ID="hdnEstDoctoCamion" runat="server" Value='<%# Bind("est_docto_camion")%>' />
                                            <asp:HiddenField ID="hdnEstDoctoArrastre" runat="server" Value='<%# Bind("est_docto_arrastre")%>' />
                                            <asp:HiddenField ID="hdnEstDcotoConductor" runat="server" Value='<%# Bind("est_docto_conductor")%>' />
                                               <asp:HiddenField ID="hdnRutConductor" runat="server" Value='<%# Bind("rut_conductor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CAMION / GPS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPatenteCamion" runat="server" Text='<%# Bind("nom_patente")%>'></asp:Label><asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip='<%# Eval("nom_est_gps_camion") %>' Width="15px" Height="15px" />

                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ARRASTRE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPatenteArrastre" runat="server" Text='<%# Bind("pat_arrastre")%>'></asp:Label><asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CONDUCTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomConductor" runat="server" Text='<%# Bind("nom_conductor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SERVICIO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomServicio" runat="server" Text='<%# Bind("nom_servicio")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FONO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFono" runat="server" Text='<%# Bind("fono_conductor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SCORE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScore" runat="server" Text='<%# Bind("score")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="TOTAL VIAJES ">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalViajes" runat="server" Text='<%# Bind("total_viajes_mes_actual")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="HORAS DISPONIBLES">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHHDisponibles" runat="server" Text="HH D" />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ETA">
                                        <ItemTemplate>
                                            <asp:Label ID="lblETA" runat="server" Text='<%# Bind("tiempo_eta")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="TIPO EQUIPO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipoEquipo" runat="server" Text='<%# Bind("nom_tipo_equipo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DISPONIBLE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisponible" runat="server" Text='<%# Bind("fec_disponible")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnAsignarOferta" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="AsignarRecursosOferta" CssClass="btn btn-primary" ToolTip="Asignar Recurso">
                                                       Asignar                                                
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnVerMapa" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerMapa" CssClass="btn bg-primary" ToolTip="Ver Mapa">
                                                       Mapa                                                  
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 form-group justify-content-center">
                    <div id="map" class="table-responsive" style="width: 100%; height: 450px;">
                    </div>

                    <script>                                             
                        var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                            maxZoom: 18,
                            attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                '<a href="https://www.mapbox.com/">Mapbox</a>',
                            id: 'mapbox/streets-v11',
                            tileSize: 512,
                            zoomOffset: -1
                        }).addTo(mymap);

                        L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>");                                                                                                                                                            
                    </script>
                </div>





                <div class="modal" id="MdlConfirmar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                    <div class="modal-dialog  modal-lg">
                        <div class="modal-content">

                            <div class="modal-body">
                                <asp:UpdatePanel runat="server" ID="updClientes">
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnConfirmar" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <div class="modal-header bg-success">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title ">Asignar Recursos OS: 
                                                        <asp:Label ID="lblOSconfirmar" Font-Bold="true" runat="server" Text=""></asp:Label></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-4 form-group">


                                                    <asp:GridView ID="grdDoctoCamion" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="DOCTO CAMION">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("Documento") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("Fecha_Vencimiento", "{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                               <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>

                                                <div class="col-md-4 form-group">

                                                    <asp:GridView ID="grdDoctoArrastre" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="DOCTO ARRASTRE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("Documento") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("Fecha_Vencimiento", "{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                          <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>

                                                <div class="col-md-4 form-group">

                                                    <asp:GridView ID="grdDoctoChofer" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false" GridLines="None">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="DOCTO CHOFER">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCategoria" runat="server" Text='<%# Eval("Documento") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="VENCIMIENTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFecVencimiento" runat="server" Text='<%# Eval("Fecha_Vencimiento", "{0:d}") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>

                                            </div>


                                            <div class="row">
                                                <div class="col-md-2 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        COD. UNIDAD</label>
                                                    <div>
                                                        <asp:Label ID="lblCodUnidadOferta" CssClass="text-primary" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        CAMIÓN</label>
                                                    <div>
                                                        <asp:Label ID="lblCamionOferta" CssClass="text-primary" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        ARRASTRE
                                                    </label>
                                                    <div>
                                                        <asp:Label ID="lblArrastreOferta" CssClass="text-primary" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        CONDUCTOR</label>
                                                    <div>
                                                        <asp:Label ID="lblConductorOferta" CssClass="text-primary" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 form-group">
                                                    <label for="fname"
                                                        class="control-label col-form-label">
                                                        FONO</label>
                                                    <div>
                                                        <asp:Label ID="lblFonoOferta" CssClass="text-primary" Font-Bold="true" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="modal-footer">

                                <asp:Button ID="btnConfirmar" CssClass="btn btn-primary "  runat="server" Text="Asignar" />
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </section>



        <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
        <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

        <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>

        <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnFecRegistroGPS" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
        <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />


        <asp:HiddenField ID="hdnEdicionHoras" runat="server" Value="0" />

    </section>



</asp:Content>

