﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb


Public Class aspIncidencias
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call Inicializar()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaIncidencias()
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec pro_gestion_alertas 'CGI', '" & Session("num_viaje_tc") & "'"
            grdIncidencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIncidencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaIncidencias", Err, Page, Master)
        End Try
    End Sub

    Private Sub Inicializar()
        txtFecLlamada.Text = Session("fec_actual")
        txtFecSuceso.Text = Session("fec_actual")
        txtHoraLlamada.Text = "12:00"
        txtHoraSuceso.Text = "10:00"
        Call CargarGrillaIncidencias()
        txtNumViaje.Text = Session("num_viaje_tc")
        txtTransportista.Text = Session("transportista_tc")
        txtCliente.Text = Session("nom_cli_tc")
        txtProducto.Text = Session("nom_pro_tc")
        txtOrigen.Text = Session("origen_tc")
        txtDestino.Text = Session("destino_tc")
        txtCamion.Text = Session("camion_tc")
        txtArrastre.Text = Session("arrastre_tc")
        txtConductor.Text = Session("conductor_tc")
        txtRut.Text = Session("rut_conductor_tc")
        txtServicio.Text = Session("nom_ser_tc")
        txtGrupoOperativo.Text = Session("nom_go_tc")
        txtSupervisor.Text = Session("nom_sup_tc")
        hdnCodCliente.Value = Session("cod_cli_tc")
        hdnCodProducto.Value = Session("cod_pro_tc")
        hdnCodGO.Value = Session("cod_go_tc")
        Call CargarIncidencias()
    End Sub


    Private Sub CargarIncidencias()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec pro_incidencias 'CCI'"
            cboIncidencia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboIncidencia.DataTextField = "des_tipo"
            cboIncidencia.DataValueField = "id_tipo_incidencia"
            cboIncidencia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarIncidencias", Err, Page, Master)
        End Try
    End Sub


    Private Sub GuardarFono()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_incidencias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = txtRut.Text
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = txtCamion.Text
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = txtArrastre.Text
            comando.Parameters.Add("@est_conductor", SqlDbType.NVarChar)
            comando.Parameters("@est_conductor").Value = txtEstConductor.Text
            comando.Parameters.Add("@nom_lugar", SqlDbType.NVarChar)
            comando.Parameters("@nom_lugar").Value = txtLugar.Text
            comando.Parameters.Add("@id_lugar", SqlDbType.NVarChar)
            comando.Parameters("@id_lugar").Value = cboTipoLugar.SelectedValue
            comando.Parameters.Add("@fec_llamado", SqlDbType.NVarChar)
            comando.Parameters("@fec_llamado").Value = CDate(txtFecLlamada.Text)
            comando.Parameters.Add("@hor_llamado", SqlDbType.NVarChar)
            comando.Parameters("@hor_llamado").Value = txtHoraLlamada.Text
            comando.Parameters.Add("@fec_fono", SqlDbType.NVarChar)
            comando.Parameters("@fec_fono").Value = CDate(txtFecSuceso.Text)
            comando.Parameters.Add("@hor_fono", SqlDbType.NVarChar)
            comando.Parameters("@hor_fono").Value = txtHoraSuceso.Text
            comando.Parameters.Add("@des_fono", SqlDbType.NVarChar)
            comando.Parameters("@des_fono").Value = txtObs.Text
            comando.Parameters.Add("@nom_denunciante", SqlDbType.NVarChar)
            comando.Parameters("@nom_denunciante").Value = txtDenunciante.Text
            comando.Parameters.Add("@num_fono_denunciante", SqlDbType.NVarChar)
            comando.Parameters("@num_fono_denunciante").Value = txtFonoDenunciante.Text
            comando.Parameters.Add("@cod_servicio", SqlDbType.NVarChar)
            comando.Parameters("@cod_servicio").Value = hdnIdServicio.Value
            comando.Parameters.Add("@cod_familia", SqlDbType.NVarChar)
            comando.Parameters("@cod_familia").Value = hdnCodGO.Value
            comando.Parameters.Add("@nom_supervisor", SqlDbType.NVarChar)
            comando.Parameters("@nom_supervisor").Value = txtSupervisor.Text
            comando.Parameters.Add("@act_terceros", SqlDbType.NVarChar)
            comando.Parameters("@act_terceros").Value = cboTerceros.SelectedValue
            comando.Parameters.Add("@id_supervisor", SqlDbType.NVarChar)
            comando.Parameters("@id_supervisor").Value = 111
            comando.Parameters.Add("@cod_clientemsoft", SqlDbType.NVarChar)
            comando.Parameters("@cod_clientemsoft").Value = hdnCodCliente.Value
            comando.Parameters.Add("@nom_cliente", SqlDbType.NVarChar)
            comando.Parameters("@nom_cliente").Value = txtCliente.Text
            comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
            comando.Parameters("@cod_producto").Value = hdnCodProducto.Value
            comando.Parameters.Add("@nom_carga", SqlDbType.NVarChar)
            comando.Parameters("@nom_carga").Value = txtProducto.Text
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = txtNumViaje.Text
            comando.Parameters.Add("@id_tipo_incidencia", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_incidencia").Value = cboIncidencia.SelectedValue
            comando.Parameters.Add("@num_latitud", SqlDbType.Float)
            comando.Parameters("@num_latitud").Value = Session("lat_incidencia_tc")
            comando.Parameters.Add("@num_longitud", SqlDbType.Float)
            comando.Parameters("@num_longitud").Value = Session("lon_incidencia_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Call GuardarFono()
        MessageBox("Guardar", "Incicendia Registrada", Page, Master, "S")
    End Sub


    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(Session("lat_incidencia_tc"), ",", ".") & "" & "," & Replace(Session("lon_incidencia_tc"), ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>N° VIAJE: </b> " & Session("num_viaje_tc") & "<br /><b>Camión: </b> " & Session("camion_tc") & "<br /><b>Arrastre: </b> " & Session("arrastre_tc") & " <br /><b>Conductor: </b> " & Session("conductor_tc") & "<br /><b>Fono: </b> " & Session("num_telefono_tc") & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function




End Class