﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspAsignacion
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")
                Call CargarFamilia()
                Call CargarGrupoOperativo()
                Call CargarClientes()
                Call CargarServicios()

                Call CargarEstadoViajes()
                Call CargarComboOrigenes()
                'Call CargarGrillaOferta()
                'Call CargarTurnos()

                Call CargarGrillaAsignacion("")
                Call CargarKPI()

                If Session("est_asignado_tc") = 1 Then
                    MessageBox("Recursos Asignados", "Camion: " & Session("pat_camion_asig") & ", Arastre: " & Session("pat_arr_asig") & ", Conductor: " & Session("conductor_asig"), Page, Master, "S")
                End If

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaAsignacion(ByVal chek As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CGVSF', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboEstadoViajes.SelectedValue & "', '" & cboOrigenes.SelectedValue & "', '" & Trim(txtOsExp.Text) & "', '" & Session("cod_usu_tc") & "', '" & cboFamilia.SelectedValue & "', '" & cboGO.SelectedValue & "','" & chek & "'"
            grdAsignacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAsignacion.DataBind()
            lblTotReg.Text = grdAsignacion.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaAsignacion", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCC', '" & Session("cod_usu_tc") & "'"
            cboClientes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientes.DataTextField = "nom_cliente1"
            cboClientes.DataValueField = "cod_msoft"
            cboClientes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarFamilia()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCF', '" & Session("cod_usu_tc") & "'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "nom_familia"
            cboFamilia.DataValueField = "cod_familia"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilia", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrupoOperativo()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CGO', '" & Session("cod_usu_tc") & "'"
            cboGO.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboGO.DataTextField = "nom_grupo_operativo"
            cboGO.DataValueField = "cod_grupo_operativo"
            cboGO.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrupoOperativo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCS', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub

    'Private Sub CargarTurnos()
    '    Try
    '        Dim strNomTablaR As String = "TCMae_lugares"
    '        Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCT'"
    '        cboTurno.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
    '        cboTurno.DataTextField = "nom_turno"
    '        cboTurno.DataValueField = "id_turno"
    '        cboTurno.DataBind()
    '    Catch ex As Exception
    '        MessageBoxError("CargarTurnos", Err, Page, Master)
    '    End Try
    'End Sub

    Private Sub CargarEstadoViajes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CEV'"
            cboEstadoViajes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstadoViajes.DataTextField = "nom_estado"
            cboEstadoViajes.DataValueField = "id"
            cboEstadoViajes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstadoViajes", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboOrigenes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'COR'"
            cboOrigenes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboOrigenes.DataTextField = "nom_lugar"
            cboOrigenes.DataValueField = "cod_lugar"
            cboOrigenes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboOrigenes", Err, Page, Master)
        End Try
    End Sub


    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs) Handles txtDesde.TextChanged
        If CDate(txtDesde.Text) > CDate(txtHasta.Text) Then
            txtHasta.Text = txtDesde.Text
        End If

        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If


        Call CargarKPI()

    End Sub

    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs) Handles txtHasta.TextChanged
        If CDate(txtHasta.Text) < CDate(txtDesde.Text) Then
            txtDesde.Text = txtHasta.Text
        End If


        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If


        Call CargarKPI()
    End Sub


    Private Sub CargarKPI()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'KPI', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboEstadoViajes.SelectedValue & "', '" & cboOrigenes.SelectedValue & "', '" & Trim(txtOsExp.Text) & "',  '" & cboFamilia.SelectedValue & "', '" & cboGO.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lbltotalDemandas.Text = Trim(rdoReader(0).ToString)
                    lblAsignados.Text = Trim(rdoReader(1).ToString)
                    lblNoAsignados.Text = Trim(rdoReader(2).ToString)
                    lblDisponibles.Text = Trim(rdoReader(3).ToString)
                    lblCumplimiento.Text = Trim(rdoReader(4).ToString) & " %"


                    If Val(lblCumplimiento.Text) <= 95 Then
                        imgCumplViajes.ImageUrl = "~/Imagen/malo.png"
                    ElseIf Val(lblCumplimiento.Text) > 95 Then
                        imgCumplViajes.ImageUrl = "~/Imagen/ok.png"
                    End If

                End If
            Catch ex As Exception
                MessageBoxError("CargarKPI", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub grdAsignacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAsignacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                If CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 6 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 7 Then

                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-yellow", "badge bg-green")

                    CType(row.FindControl("btnAsignarPre"), LinkButton).Visible = False
                    CType(row.FindControl("btnOfertar"), LinkButton).Visible = False
                    CType(row.FindControl("btnCancelarAsignacion"), LinkButton).Visible = False

                    CType(row.FindControl("txtCamion"), TextBox).Visible = True
                    CType(row.FindControl("txtArrastre"), TextBox).Visible = True
                    CType(row.FindControl("txtRutconductor"), TextBox).Visible = True
                    CType(row.FindControl("txtCamion"), TextBox).Enabled = False
                    CType(row.FindControl("txtArrastre"), TextBox).Enabled = False
                    CType(row.FindControl("txtRutconductor"), TextBox).Enabled = False

                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = False
                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = False


                ElseIf CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 8 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 12 Or CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 2 Then

                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-green", "badge bg-red")
                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-yellow", "badge bg-red")

                    CType(row.FindControl("btnAsignarPre"), LinkButton).Visible = False
                    CType(row.FindControl("btnOfertar"), LinkButton).Visible = False
                    CType(row.FindControl("btnCancelarAsignacion"), LinkButton).Visible = False

                    CType(row.FindControl("txtCamion"), TextBox).Visible = True
                    CType(row.FindControl("txtArrastre"), TextBox).Visible = True
                    CType(row.FindControl("txtRutconductor"), TextBox).Visible = True
                    CType(row.FindControl("txtCamion"), TextBox).Enabled = False
                    CType(row.FindControl("txtArrastre"), TextBox).Enabled = False
                    CType(row.FindControl("txtRutconductor"), TextBox).Enabled = False
                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = False
                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = False


                ElseIf CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 3 Then
                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-green", "badge bg-yellow")
                    CType(row.FindControl("btnAsignarPre"), LinkButton).Visible = False
                    CType(row.FindControl("btnOfertar"), LinkButton).Visible = True
                    CType(row.FindControl("btnCancelarAsignacion"), LinkButton).Visible = True

                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = False
                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = False

                    CType(row.FindControl("txtCamion"), TextBox).Visible = False
                    CType(row.FindControl("btnVerDocCamion"), LinkButton).Visible = False
                    CType(row.FindControl("txtArrastre"), TextBox).Visible = False
                    CType(row.FindControl("btnVerDocArrastre"), LinkButton).Visible = False
                    CType(row.FindControl("txtRutconductor"), TextBox).Visible = False
                    CType(row.FindControl("btnVerDocConductor"), LinkButton).Visible = False
                    CType(row.FindControl("lblNomConductor"), Label).Visible = False
                    CType(row.FindControl("lblGPS"), Label).Visible = False
                    CType(row.FindControl("lblSlash"), Label).Visible = False
                    CType(row.FindControl("lblGPSArrastre"), Label).Visible = False
                    CType(row.FindControl("btnGPSCamion"), ImageButton).Visible = False
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).Visible = False

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call VerificarDoctosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))


                ElseIf CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value = 11 Then
                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-green", "badge bg-light-blue")
                    CType(row.FindControl("lblEstado"), Label).CssClass = CType(row.FindControl("lblEstado"), Label).CssClass.Replace("badge bg-yellow", "badge bg-light-blue")

                    CType(row.FindControl("txtCamion"), TextBox).Visible = True
                    CType(row.FindControl("btnVerDocCamion"), LinkButton).Visible = True
                    CType(row.FindControl("txtArrastre"), TextBox).Visible = True
                    CType(row.FindControl("btnVerDocArrastre"), LinkButton).Visible = True
                    CType(row.FindControl("txtRutconductor"), TextBox).Visible = True
                    CType(row.FindControl("btnVerDocConductor"), LinkButton).Visible = True
                    CType(row.FindControl("lblNomConductor"), Label).Visible = True
                    CType(row.FindControl("lblGPS"), Label).Visible = True
                    CType(row.FindControl("lblSlash"), Label).Visible = True
                    CType(row.FindControl("lblGPSArrastre"), Label).Visible = True
                    CType(row.FindControl("btnGPSCamion"), ImageButton).Visible = True
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).Visible = True
                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = True
                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = True

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call VerificarDoctosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))

                    CType(row.FindControl("btnCancelarAsignacion"), LinkButton).Visible = True

                    If CType(row.FindControl("txtCamion"), TextBox).BackColor = System.Drawing.Color.LightSteelBlue Then
                        CType(row.FindControl("btnAsignarPre"), LinkButton).Visible = False
                        CType(row.FindControl("btnOfertar"), LinkButton).Visible = True
                    Else
                        CType(row.FindControl("btnAsignarPre"), LinkButton).Visible = True
                        CType(row.FindControl("btnOfertar"), LinkButton).Visible = False
                    End If


                    If CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.LightSteelBlue Then
                        CType(row.FindControl("txtArrastre"), TextBox).Enabled = True
                    End If


                    If CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.LightSteelBlue Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Enabled = True
                    End If

                End If



                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                Else
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("btnTipoViaje"), LinkButton).Text = "MULTIPUNTO" Then
                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = False
                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = False
                Else
                    If CType(row.FindControl("txtHoraOrigen"), TextBox).Text = "" Or CType(row.FindControl("txtHoraOrigen"), TextBox).Text = "" Then
                        CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = True
                        CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = True
                    End If
                End If





            End If
        Catch ex As Exception
            MessageBoxError("grdAsignacion_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub VerificarDoctosVehiculo(ByVal patente As String, ByRef txt As TextBox, ByRef btn As LinkButton)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                        ' btn.Visible = False
                    Else
                        txt.BackColor = System.Drawing.Color.White
                        ' btn.Visible = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                        'btn.Visible = False
                    Else
                        txt.BackColor = System.Drawing.Color.White

                        'If txtCamion.BackColor = System.Drawing.Color.LightSalmon Or txtArrastre.BackColor = System.Drawing.Color.LightSalmon Then
                        '    btn.Visible = False
                        'Else
                        '    btn.Visible = True
                        'End If

                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub CargarDatosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'RDC', '" & Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    CType(row.FindControl("txtCamion"), TextBox).Text = Replace(Trim(rdoReader(0).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("btnGPSCamion"), ImageButton))

                    CType(row.FindControl("hdnCodConductor"), HiddenField).Value = Trim(rdoReader(1).ToString)
                    CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(rdoReader(4).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))

                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(rdoReader(3).ToString)
                    CType(row.FindControl("lblNomConductor"), Label).Text = Trim(rdoReader(2).ToString) & " / " & Trim(rdoReader(5).ToString)

                    Call VerificarDoctosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))

                Else

                    CType(row.FindControl("txtArrastre"), TextBox).Text = ""
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = ""
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""

                    CType(row.FindControl("txtCamion"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"

                    MessageBox("Buscar", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")

                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub GPSArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtArrastre"), TextBox).Text), "-", "")

        If CType(row.FindControl("txtArrastre"), TextBox).Text = "" Then
            CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
        Else
            Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
        End If

        Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))
    End Sub


    Private Sub RescatarEstadoGPSPRO(ByVal patente As String, ByRef img As ImageButton)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GPS', '" & Trim(Replace(patente, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = 1 Then
                        img.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        img.ImageUrl = "~/Imagen/malo.png"
                    Else
                        img.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    img.ImageUrl = "~/Imagen/nulo.png"
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarEstadoGPSPRO", Err, Page, Master)
        End Try
    End Sub

    Protected Sub DoctosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        If CType(row.FindControl("txtRutconductor"), TextBox).Text = "" Then
            CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
        Else
            If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                CType(row.FindControl("lblNomConductor"), Label).Text = ""
            Else
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), ".", "")
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), "-", "")
                If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 8 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 7) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 8) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                End If


                If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                Else
                    If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 10 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)
                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = "0" + Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)

                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    End If
                End If
            End If
        End If
    End Sub


    Private Sub RescatarDatosConductor(ByVal rut As String, ByRef nombre As String, ByRef txtRut As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'BDC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    rut = Trim(rdoReader(0).ToString)
                    nombre = Trim(rdoReader(1).ToString) '& " " & Trim(rdoReader(2).ToString)

                    Call VerificarDoctosConductor(rut, txtRut, btn, txtCamion, txtArrastre)
                Else
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnVerDocCamion_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        pnlDoctos.Visible = True
        'pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", ""))

        lblTrasnportista.Text = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Protected Sub btnVerDocArrastre_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        pnlDoctos.Visible = True
        'pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", ""))
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocConductor_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        pnlDoctos.Visible = True
        'pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(2, Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("lblNomConductor"), Label).Text, "-", "")) & " / " & Trim(CType(row.FindControl("hdnFono"), HiddenField).Value)
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text

                If CDate(objFechaVenc) <= CDate(Date.Now.Date) Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaMultipaso(ByVal os As String, ByVal cod_cliente As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CDM' , '" & os & "', '" & cod_cliente & "'"
            grdMultipaso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdMultipaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaMultipaso", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnTipoViajeClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        If CType(row.FindControl("btnTipoViaje"), LinkButton).Text = "MULTIPUNTO" Then

            hdnEstViaje.Value = CType(row.FindControl("hdnEstadoviaje"), HiddenField).Value

            If hdnEstViaje.Value = 3 Or hdnEstViaje.Value = 11 Then
                btnConfirmar.Visible = True
            Else
                btnConfirmar.Visible = False
            End If

            Call CargarGrillaMultipaso(CType(row.FindControl("hdnOS"), HiddenField).Value, CType(row.FindControl("hdnCodcliente"), HiddenField).Value)

            lblNumOS.Text = Trim(Replace(CType(row.FindControl("lblOS"), Label).Text, "-", ""))
            ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalMultipunto();", True)
        End If

    End Sub


    Private Sub grdMultipaso_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdMultipaso.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                If hdnEstViaje.Value = 3 Or hdnEstViaje.Value = 11 Then
                    CType(row.FindControl("txtHoraOrigen"), TextBox).Enabled = True
                    CType(row.FindControl("txtHoraDestino"), TextBox).Enabled = True
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdMultipaso_RowDataBound", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Try
            For iRow = 0 To grdMultipaso.Rows.Count - 1
                Call ActualizarHoraGrilla(CType(grdMultipaso.Rows(iRow).FindControl("txtHoraOrigen"), TextBox).Text, CType(grdMultipaso.Rows(iRow).FindControl("txtHoraDestino"), TextBox).Text, CType(grdMultipaso.Rows(iRow).FindControl("hdnIdDemanda"), HiddenField).Value)
            Next

            If chkDemandas.Checked = True Then
                Call CargarGrillaAsignacion(1)
            Else
                Call CargarGrillaAsignacion("")
            End If
            MessageBox("Actualizar Horas", "registros Actualizados", Page, Master, "S")

        Catch ex As Exception
            MessageBoxError("btnConfirmar_Click", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ActualizarHoraGrilla(ByVal hora_origen As String, ByVal hora_destino As String, ByVal id_demanda As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UHG"
            comando.Parameters.Add("@hor_servicio", SqlDbType.NVarChar)
            comando.Parameters("@hor_servicio").Value = hora_origen
            comando.Parameters.Add("@hor_servicio_destino", SqlDbType.NVarChar)
            comando.Parameters("@hor_servicio_destino").Value = hora_destino
            comando.Parameters.Add("@id_demanda", SqlDbType.NVarChar)
            comando.Parameters("@id_demanda").Value = id_demanda
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("ActualizarHoraGrilla", Err, Page, Master)
        End Try
    End Sub


    ' ------------------------------------------- OFERTAR ---------------------------------------------------
    ' ------------------------------------------- OFERTAR ---------------------------------------------------

    Protected Sub GenerarFTP(ByVal demanda As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "FTP"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = hdnCodCliente.Value
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = hdnNumOS.Value
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = demanda
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GenerarFTP", Err, Page, Master)
        End Try
    End Sub


    'Private Function GenerarCSV(ByVal cliente As String, ByVal os As String) As Boolean
    '    Dim strPathFile As String
    '    Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
    '    Dim strSQL As String = "", strNomArchivo As String = ""
    '    Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
    '    Dim cnxBDTC_QA As New SqlConnection(strCnx)
    '    cnxBDTC_QA.Open()

    '    strNomArchivo = "Demanda_CL_" & hdnCodCliente.Value & "_OS_" & hdnNumOS.Value & ".csv"
    '    strPathFile = "C:\Demandas\" + Trim(strNomArchivo)
    '    If (String.IsNullOrEmpty(strPathFile) = False) Then

    '        Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
    '        If (toDownExiFile.Exists) Then
    '            Dim FileToDelete As String = strPathFile
    '            If File.Exists(FileToDelete) = True Then
    '                File.Delete(FileToDelete)
    '            End If
    '        End If
    '        File.AppendAllText(strPathFile, Trim(strNomArchivo))
    '        Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)
    '        strSQL = "Exec pro_asignacion_viajes 'FTP', '" & hdnCodCliente.Value & "', '" & hdnNumOS.Value & "'"
    '        cmdTemporal = New SqlCommand(strSQL, cnxBDTC_QA)
    '        cmdTemporal.CommandTimeout = 20000
    '        rdoReader = cmdTemporal.ExecuteReader()
    '        Do While rdoReader.Read
    '            strTextoDetalle1 = ""
    '            strTextoDetalle1 = Trim(rdoReader(0))
    '            z_varocioStreamWriter.WriteLine(strTextoDetalle1)
    '        Loop
    '        rdoReader.Close()
    '        rdoReader = Nothing
    '        cmdTemporal.Dispose()
    '        cmdTemporal = Nothing
    '        z_varocioStreamWriter.Close()
    '        blnError = True
    '    End If
    '    Return blnError
    'End Function

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>Cod. Unidad:</b> " & hdnCodUnidad.Value & " <br /><b>Camión:</b> " & hdnPatente.Value & "<br /><b>Arrastre:</b> " & hdnPatArrastre.Value & " <br /><b>Conductor:</b> " & hdnNomconductor.Value & "<br /><b>Fono</b> " & hdnfonoConductor.Value & "<br /><b>Ultimo Registro:</b> " & hdnFecRegistroGPS.Value & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function


    Function CargarGrafico3() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("num_latitud", GetType(String)))
        Datos.Columns.Add(New DataColumn("num_longitud", GetType(String)))

        strSCodU = "Exec pro_asignacion_viajes 'CMP'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        For Each dr As DataRow In Datos.Rows
            If strDatos = "" Then
                strDatos = dr(0) & "" & "," & dr(1) & ""
            Else
                strDatos = strDatos & "," & dr(0) & "" & "," & dr(1)
            End If
        Next
        strDatos = "[" & strDatos & "]"

        Return strDatos
    End Function



    Protected Sub AsignarUT(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        If CType(row.FindControl("txtHoraOrigen"), TextBox).Text = "" Or CType(row.FindControl("txtHoraOrigen"), TextBox).Text = "" Then
            MessageBox("Asignar", "Ingrese Hora Valida", Page, Master, "W")
        Else
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'VAC1', '" & Trim(CType(row.FindControl("txtCamion"), TextBox).Text) & "','" & Trim(CType(row.FindControl("lblFecOrigen"), Label).Text) & "', '" & Trim(CType(row.FindControl("txtHoraOrigen"), TextBox).Text) & "', '" & Trim(CType(row.FindControl("lblFecDestino"), Label).Text) & "', '" & Trim(CType(row.FindControl("txtHoraDestino"), TextBox).Text) & "', '" & CType(row.FindControl("hdnOS"), HiddenField).Value & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()

                    If rdoReader.Read Then
                        If rdoReader(0).ToString <> 0 Then
                            MessageBox("Asignar", "Recursos en uso, favor verificar Viaje N°: " & rdoReader(0).ToString, Page, Master, "W")
                            Exit Sub
                        ElseIf rdoReader(1).ToString <> 0 Then
                            MessageBox("Asignar", "Recursos en uso, favor verificar Viaje N°: " & rdoReader(1).ToString, Page, Master, "W")
                            Exit Sub
                        Else
                            Call ActualizarHoraGrilla(CType(row.FindControl("txtHoraOrigen"), TextBox).Text, CType(row.FindControl("txtHoraDestino"), TextBox).Text, CType(row.FindControl("hdnIdDemanda"), HiddenField).Value)

                            hdnNumOS.Value = CType(row.FindControl("hdnOS"), HiddenField).Value
                            hdnCodCliente.Value = CType(row.FindControl("hdnCodcliente"), HiddenField).Value

                            Dim conx As New SqlConnection(strCnx)
                            Dim comando As New SqlCommand
                            comando.CommandType = CommandType.StoredProcedure
                            comando.CommandText = "pro_asignacion_viajes"
                            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                            comando.Parameters("@tipo").Value = "UDV"
                            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
                            comando.Parameters("@cod_conductor").Value = CType(row.FindControl("hdnCodConductor"), HiddenField).Value
                            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
                            comando.Parameters("@pat_camion").Value = Trim(CType(row.FindControl("txtCamion"), TextBox).Text)
                            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
                            comando.Parameters("@pat_arrastre").Value = Trim(CType(row.FindControl("txtArrastre"), TextBox).Text)
                            comando.Parameters.Add("@cod_servicio", SqlDbType.NVarChar)
                            comando.Parameters("@cod_servicio").Value = CType(row.FindControl("hdnCodServicio"), HiddenField).Value
                            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
                            comando.Parameters("@busqueda").Value = Val(hdnNumOS.Value)
                            comando.Parameters.Add("@id_turno", SqlDbType.Int)
                            comando.Parameters("@id_turno").Value = 1
                            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
                            comando.Parameters("@busqueda2").Value = Trim(hdnCodCliente.Value)
                            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
                            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                            'Call GenerarCSV(CType(row.FindControl("hdnCodcliente"), HiddenField).Value, CType(row.FindControl("lblOS"), Label).Text)
                            Call GenerarFTP(CType(row.FindControl("hdnIdDemanda"), HiddenField).Value)

                            If chkDemandas.Checked = True Then
                                Call CargarGrillaAsignacion(1)
                            Else
                                Call CargarGrillaAsignacion("")
                            End If
                            Call CargarKPI()
                            MessageBox("Asignar", "Recursos Asignados", Page, Master, "S")
                        End If
                    End If



                Catch ex As Exception
                    MessageBoxError("RescatarDatosConductor", Err, Page, Master)
                    Exit Sub
                Finally
                    cnxAcceso.Close()
                End Try
            End Using

        End If



    End Sub




    Protected Sub AsignarRecursosOferta(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        '  Call ActualizarHoraGrilla(CType(row.FindControl("txtHoraOrigen"), TextBox).Text, CType(row.FindControl("txtHoraDestino"), TextBox).Text, CType(row.FindControl("hdnIdDemanda"), HiddenField).Value)

        Session("os_asig_tc") = CType(row.FindControl("lblOS"), Label).Text
        Session("cod_cli_tc") = CType(row.FindControl("hdnCodcliente"), HiddenField).Value
        Session("id_dem_tc") = CType(row.FindControl("hdnIdDemanda"), HiddenField).Value
        Session("id_ser_tc") = CType(row.FindControl("hdnCodServicio"), HiddenField).Value

        Session("id_ser_tc") = CType(row.FindControl("hdnCodServicio"), HiddenField).Value

        Session("grupo_operativo_tc") = CType(row.FindControl("hdnGrupoOperativo"), HiddenField).Value

        Session("est_asignado_tc") = 0

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspAsigRecursos.aspx")
    End Sub


    Protected Sub OfertarUT(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        Call ActualizarHoraGrilla(CType(row.FindControl("txtHoraOrigen"), TextBox).Text, CType(row.FindControl("txtHoraDestino"), TextBox).Text, CType(row.FindControl("hdnIdDemanda"), HiddenField).Value)

        hdnCodCliente.Value = CType(row.FindControl("hdnCodcliente"), HiddenField).Value
        hdnIdDemanda.Value = CType(row.FindControl("hdnIdDemanda"), HiddenField).Value
        ' lblNumOSOferta.Text = CType(row.FindControl("lblOS"), Label).Text

        hdnIdServicio.Value = CType(row.FindControl("hdnCodServicio"), HiddenField).Value
    End Sub

    Protected Sub CancelarUT(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAsignacion.Rows(rowIndex)

        Call CancelarAsignacion(10, CType(row.FindControl("hdnID"), HiddenField).Value)
    End Sub


    Protected Sub CancelarAsignacion(ByVal est_viaje As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UES"
            comando.Parameters.Add("@id_est_viaje", SqlDbType.NVarChar)
            comando.Parameters("@id_est_viaje").Value = est_viaje
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            If chkDemandas.Checked = True Then
                Call CargarGrillaAsignacion(1)
            Else
                Call CargarGrillaAsignacion("")
            End If
            MessageBox("Cancelar", "Registro Cancelado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("CancelarAsignacion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedIndexChanged
        If cboClientes.SelectedValue = "0" Then
            chkDemandas.Checked = False
            chkDemandas.Visible = False
        Else
            chkDemandas.Visible = True
        End If

        Call CargarServicios()
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        Call CargarKPI()
    End Sub

    Protected Sub cboServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicio.SelectedIndexChanged

        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        Call CargarKPI()
    End Sub


    Private Sub CargaGrillaKPI(ByVal tipo As String)
        'pnlDetalleViaje.Visible = False
        pnlDoctos.Visible = False
        pnlKPI.Visible = True

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
        Try
            If tipo = "TOTAL_DEMANDAS" Then
                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "TOTAL DEMANDAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDI', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()

            ElseIf tipo = "ASIGNADOS" Then

                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "DEMANDAS ASIGNADAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDA', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()

            ElseIf tipo = "NO_ASIGNADOS" Then
                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "DEMANDAS NO ASIGNADAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDN', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()


            ElseIf tipo = "DISPONIBLES" Then
                grdDemandasKPI.Visible = False
                grdDisponiblesKPI.Visible = True
                lblNomModal.Text = "CAMIONES DISPONIBLES"

                Dim strNomTabla As String = "VW_Patentes_Disponibles"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GPT', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
                grdDisponiblesKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDisponiblesKPI.DataBind()
            End If

        Catch ex As Exception
            MessageBoxError("CargaGrillaKPI", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnTotalKPI_Click(sender As Object, e As EventArgs) Handles btnTotalKPI.Click
        Call CargaGrillaKPI("TOTAL_DEMANDAS")
        lblTituloKPI.Text = "TOTAL_DEMANDAS"
    End Sub

    Protected Sub btnAsignadosKPI_Click(sender As Object, e As EventArgs) Handles btnAsignadosKPI.Click
        Call CargaGrillaKPI("ASIGNADOS")
        lblTituloKPI.Text = "ASIGNADOS"
    End Sub

    Protected Sub btnNOAsignadosKPI_Click(sender As Object, e As EventArgs) Handles btnNOAsignadosKPI.Click
        Call CargaGrillaKPI("NO_ASIGNADOS")
        lblTituloKPI.Text = "NO ASIGNADOS"
    End Sub

    Protected Sub btnDisponiblesKPI_Click(sender As Object, e As EventArgs) Handles btnDisponiblesKPI.Click
        Call CargaGrillaKPI("DISPONIBLES")
        lblTituloKPI.Text = "DISPONIBLES"
    End Sub

    Private Sub grdAsignacion_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdAsignacion.PageIndexChanging
        Try
            grdAsignacion.PageIndex = e.NewPageIndex
            If chkDemandas.Checked = True Then
                Call CargarGrillaAsignacion(1)
            Else
                Call CargarGrillaAsignacion("")
            End If

        Catch ex As Exception
            MessageBoxError("grdAsignacion_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    'Protected Sub cboTurno_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTurno.SelectedIndexChanged

    '    Call CargarGrillaAsignacion()

    '    Call CargarKPI()
    'End Sub

    Protected Sub grdAsignacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdAsignacion.SelectedIndexChanged

    End Sub

    Protected Sub cboEstadoViajes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEstadoViajes.SelectedIndexChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        'Call CargarGrillaOferta()
        Call CargarKPI()
    End Sub

    Protected Sub cboOrigenes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOrigenes.SelectedIndexChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        'Call CargarGrillaOferta()
        Call CargarKPI()
    End Sub

    Protected Sub txtOsExp_TextChanged(sender As Object, e As EventArgs) Handles txtOsExp.TextChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
    End Sub

    Protected Sub cboFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFamilia.SelectedIndexChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        Call CargarKPI()
    End Sub

    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_asignacion_viajes 'CGV', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboEstadoViajes.SelectedValue & "', '" & cboOrigenes.SelectedValue & "', '" & Trim(txtOsExp.Text) & "', '" & Session("cod_usu_tc") & "', '" & cboFamilia.SelectedValue & "','" & cboGO.SelectedValue & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Registros_Asignacion.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("lnkExportar_Click", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnLimpiarFiltros_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltros.Click
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")

        cboFamilia.ClearSelection()
        cboGO.ClearSelection()
        cboClientes.ClearSelection()
        cboServicio.ClearSelection()
        cboEstadoViajes.ClearSelection()
        cboOrigenes.ClearSelection()
        txtOsExp.Text = ""

        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        Call CargarKPI()
    End Sub

    Protected Sub chkDemandas_CheckedChanged(sender As Object, e As EventArgs) Handles chkDemandas.CheckedChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
    End Sub

    Protected Sub cboGO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboGO.SelectedIndexChanged
        If chkDemandas.Checked = True Then
            Call CargarGrillaAsignacion(1)
        Else
            Call CargarGrillaAsignacion("")
        End If
        'Call CargarGrillaOferta()
        Call CargarKPI()
    End Sub
End Class