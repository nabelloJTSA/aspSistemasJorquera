﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Imports System.Net

Public Class aspPOD
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "POD VIAJE N°: " & Session("num_viaje_tc")
                Call CargarTipodocto()
                Call CargarGrillaPOD()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarTipodocto()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_pod 'CTD'"
            cbotipodocto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cbotipodocto.DataTextField = "nom_tipo_docto"
            cbotipodocto.DataValueField = "id_tipo_docto"
            cbotipodocto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipodocto", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaPOD()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_pod 'CGD', '" & Session("num_viaje_tc") & "'"
            grdPOD.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPOD.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaPOD", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarRecursosMSFOT()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ACT"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Session("num_viaje_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarRecursosMSFOT", Err, Page, Master)
        End Try
    End Sub


    Protected Sub brnRelacionar_Click(sender As Object, e As EventArgs) Handles brnRelacionar.Click

        If uplCDocumento.HasFile = False Then
            MessageBox("Guardar POD", "Seleccione archivo", Page, Master, "W")
        Else
            If ExisteCarpeta("Viaje_" & Session("num_viaje_tc"), "usuarioftp", "Ftp2018.") = False Then
                Call CrearCarpeta("Viaje_" & Session("num_viaje_tc"), "usuarioftp", "Ftp2018.")
            End If

            If cbotipodocto.SelectedValue = 1 Then
                If Path.GetExtension(uplCDocumento.FileName) = ".jpg" Or Path.GetExtension(uplCDocumento.FileName) = ".jpeg" Or Path.GetExtension(uplCDocumento.FileName) = ".png" Then
                    Call SubirFTPDcto("Viaje_" & Session("num_viaje_tc"), "usuarioftp", "Ftp2018.")
                    Call GuardarDocto()
                    Call ActualizarRecursosMSFOT()
                Else
                    MessageBox("Guardar POD", "Los Documento deben ser .JPG, JPEG o PNG", Page, Master, "W")
                End If

            ElseIf cbotipodocto.SelectedValue = 2 Then
                If Path.GetExtension(uplCDocumento.FileName) = ".pdf" Then
                    Call SubirFTPDcto("Viaje_" & Session("num_viaje_tc"), "usuarioftp", "Ftp2018.")
                    Call GuardarDocto()
                    Call ActualizarRecursosMSFOT()
                Else
                    MessageBox("Guardar POD", "Los Documento deben ser .pdf", Page, Master, "W")
                End If
            End If
        End If



    End Sub

    Protected Sub GuardarDocto()
        Try
            Dim NomArchivo As String = Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_")

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_pod"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IDO"
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = Session("num_viaje_tc")
            comando.Parameters.Add("@url_pod", SqlDbType.NVarChar)
            comando.Parameters("@url_pod").Value = Replace(NomArchivo, ",", ".")
            comando.Parameters.Add("@id_tipo_docto", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_docto").Value = cbotipodocto.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@num_docto", SqlDbType.NVarChar)
            comando.Parameters("@num_docto").Value = Trim(txtNumDocto.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar POD", "POD Registrado", Page, Master, "S")

            Call CargarGrillaPOD()
        Catch ex As Exception
            MessageBoxError("GuardarDocto", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarDocto(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_pod"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EDO"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = Session("num_viaje_tc")
            comando.Parameters.Add("@busqueda4", SqlDbType.NVarChar)
            comando.Parameters("@busqueda4").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarDocto", Err, Page, Master)
        End Try
    End Sub


    Function ExisteCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String) As Boolean
        Dim Request As FtpWebRequest = CType(WebRequest.Create(New Uri("ftp://192.168.10.28/POD_VIAJES/" & ruta)), FtpWebRequest)
        Request.Credentials = New NetworkCredential(usuario, pass)
        Request.Method = WebRequestMethods.Ftp.GetDateTimestamp
        Request.UsePassive = False
        Try
            Dim respuesta As FtpWebResponse
            respuesta = CType(Request.GetResponse(), FtpWebResponse)
            Return True
        Catch ex As WebException
            Return False
        End Try
    End Function

    Public Sub CrearCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Dim crearDirectorio As FtpWebRequest = DirectCast(System.Net.FtpWebRequest.Create("ftp://192.168.10.28/POD_VIAJES/" & Trim(ruta)), System.Net.FtpWebRequest)
        crearDirectorio.Credentials = New NetworkCredential(usuario, pass)
        crearDirectorio.Method = WebRequestMethods.Ftp.MakeDirectory
        Dim respuesta As FtpWebResponse = crearDirectorio.GetResponse()
    End Sub

    Public Sub SubirFTPDcto(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Try
            Dim NomArchivo As String = Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_")
            Dim clsRequest As FtpWebRequest = DirectCast(System.Net.WebRequest.Create("ftp://192.168.10.28/POD_VIAJES/" & Trim(ruta) & "/" & Replace(NomArchivo, ",", ".")), System.Net.FtpWebRequest)
            clsRequest.Credentials = New System.Net.NetworkCredential(usuario, pass)
            clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            Dim bFile As Byte() = uplCDocumento.FileBytes
            Dim clsStream As System.IO.Stream = clsRequest.GetRequestStream()
            clsStream.Write(bFile, 0, bFile.Length)
            clsStream.Close()
            clsStream.Dispose()

        Catch ex As Exception
            MessageBoxError("SubirFTPDcto", Err, Page, Master)
        End Try
    End Sub


    Private Sub DescargarFTP(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/POD_VIAJES/Viaje_" & Session("num_viaje_tc") & "/" & Trim(nombre), Server.MapPath(archivo), "usuarioftp", "Ftp2018.")

        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub


    Private Sub BorrarFicheroFTP(ByVal codigo As String, ByVal ruta As String)
        '**********************************************
        '*** Borramos el Fichero del FTP ya tratado
        '**********************************************
        Dim DireccionyFichero As String
        DireccionyFichero = "ftp://192.168.10.28/POD_VIAJES/Viaje_" + codigo + "/" + Trim(ruta)
        Dim peticionFTP As FtpWebRequest
        ' Creamos una petición FTP con la dirección del fichero a eliminar
        peticionFTP = CType(WebRequest.Create(New Uri(DireccionyFichero)), FtpWebRequest)
        ' Fijamos el usuario y la contraseña de la petición
        peticionFTP.Credentials = New NetworkCredential("usuarioftp", "Ftp2018.")
        ' Seleccionamos el comando que vamos a utilizar: Eliminar un fichero
        peticionFTP.Method = WebRequestMethods.Ftp.DeleteFile
        peticionFTP.UsePassive = False
        Try
            Dim respuestaFTP As FtpWebResponse
            respuestaFTP = CType(peticionFTP.GetResponse(), FtpWebResponse)
            respuestaFTP.Close()
        Catch ex As Exception
            ' Si se produce algún fallo, se devolverá el mensaje del error
            ' MsgBox("Error al borrar fichero" & ex.Message)
        End Try
    End Sub


    Protected Sub DescargarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPOD.Rows(rowIndex)

        Dim archivo As String = "~/Temp/" & CType(row.FindControl("lblNomTipo"), Label).Text

        Call DescargarFTP(archivo, CType(row.FindControl("lblNomTipo"), Label).Text)

        Response.ContentType = "application/ms-word"
        Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(row.FindControl("lblNomTipo"), Label).Text)
        Response.TransmitFile(Server.MapPath(archivo))
        Response.End()

    End Sub


    Protected Sub EliminarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPOD.Rows(rowIndex)

        Dim archivo As String = "~/Temp/" & CType(row.FindControl("lblNomTipo"), Label).Text
        Call EliminarDocto(CType(row.FindControl("hdnID"), HiddenField).Value)
        Call BorrarFicheroFTP(Session("num_viaje_tc"), CType(row.FindControl("lblNomTipo"), Label).Text)
        Call CargarGrillaPOD()
    End Sub

    Protected Sub ValidarPOD(ByVal valido As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_pod"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "VAL"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = valido
            comando.Parameters.Add("@usr_valido", SqlDbType.NVarChar)
            comando.Parameters("@usr_valido").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = id
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = Session("num_viaje_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ValidarPOD", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ValidarPODClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPOD.Rows(rowIndex)

        Call ValidarPOD(CType(row.FindControl("lblValidado"), Label).Text, CType(row.FindControl("hdnID"), HiddenField).Value)
        MessageBox("Validar POD", "POD Validado", Page, Master, "S")
        Call CargarGrillaPOD()

    End Sub


End Class