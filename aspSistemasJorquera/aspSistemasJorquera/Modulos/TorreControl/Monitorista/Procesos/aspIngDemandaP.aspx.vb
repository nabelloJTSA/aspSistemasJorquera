﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Imports System.Net.Mail

Public Class aspIngDemandaP
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        grdConfirmados.Visible = False
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Session("est_asignado_tc") = 0
                Call Inicializarcontroles()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnConfirnarVolver_Click(sender As Object, e As EventArgs) Handles btnConfirnarVolver.Click
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspAsignacion.aspx")
    End Sub

    Private Sub CargarDemandasDiaActual()
        Try
            grdConfirmados.Visible = True
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_ingreso_demanda 'DDD', '" & Session("cod_usu_tc") & "'"
            grdConfirmados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdConfirmados.DataBind()

            If grdConfirmados.Rows.Count > 0 Then
                grdConfirmados.Visible = True
            Else
                grdConfirmados.Visible = False
            End If

        Catch ex As Exception
            MessageBoxError("CargaGrillaConfirmados", Err, Page, Master)
        End Try
    End Sub

    Protected Sub EliminarTablaTemporal()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EGT"
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarTablaTemporal", Err, Page, Master)
        End Try
    End Sub

    Private Sub Inicializarcontroles()
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")
        ' Call CargarTurnosKPI()
        'Call EliminarTablaTemporal()
        txtFechaServicio.Text = Session("fec_actual")


        Call CargarClientes()
        Call CargarServicio()
        Call CargarGrupoOperativo()
        Call CargarProductos()
        Call CargarLugares()

        Call CargarServiciosKPI()
        Call CargarClientesKPI()

        Call CargaGrillaTemporal()
        Call Cargarconductores()

        Call CargarKPI()
        ' Call CargarDemandasDiaActual()
        Call CargarDemandasDias()
    End Sub

    Private Sub CargarKPI()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'KPI2', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lbltotalDemandas.Text = Trim(rdoReader(0).ToString)
                    lblAsignados.Text = Trim(rdoReader(1).ToString)
                    lblNoAsignados.Text = Trim(rdoReader(2).ToString)
                    lblDisponibles.Text = Trim(rdoReader(3).ToString)
                End If
            Catch ex As Exception
                MessageBoxError("CargarKPI", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarLugares()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'COD', '" & cboTipoLugar.SelectedValue & "', '" & cboClientes.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            cboLugar.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboLugar.DataTextField = "nom_lugar"
            cboLugar.DataValueField = "cod_lugar"
            cboLugar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarLugares", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarServicio()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CCS', '" & cboClientes.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicio"
            cboServicio.DataValueField = "id_servicio"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicio", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarServiciosKPI()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CCSK', '" & Session("cod_usu_tc") & "'"
            cboServicioKPI.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicioKPI.DataTextField = "nom_servicios"
            cboServicioKPI.DataValueField = "cod_servicios"
            cboServicioKPI.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServiciosKPI", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrupoOperativo()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CGO', '" & cboServicio.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            cboClaOperativa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClaOperativa.DataTextField = "nom_grupo_operativo"
            cboClaOperativa.DataValueField = "cod_grupo_operativo"
            cboClaOperativa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrupoOperativo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicio.SelectedIndexChanged
        Call CargarGrupoOperativo()
        Call LimpiarRecursos()
        txtOS.Text = ""
    End Sub

    Private Sub LimpiarRecursos()
        cboClaOperativa.ClearSelection()
        cboClaOperativa.Enabled = True
        cboCamion.ClearSelection()
        cboCamion.Enabled = False
        txtArrastre.Text = ""
        txtArrastre.Enabled = False
        txtRutconductor.Text = ""
        cboConductor.ClearSelection()
        ' txtRutconductor.Enabled = False
        btnVerDocCamion.Visible = False
        btnVerDocConductor.Visible = False
        btnVerDocArrastre.Visible = False
        imgGPSArrastre.Visible = False
        imgGPSCamion.Visible = False
        txtArrastre.BackColor = System.Drawing.Color.White
        cboCamion.BackColor = System.Drawing.Color.White
        txtRutconductor.BackColor = System.Drawing.Color.White
        txtFono.Text = ""
        txtFono.Enabled = False
        hdnCodConductor.Value = 0
        hdnHoraAtrasGrilla.Value = 0
        hdnRegistros.Value = 0
        lblTrasnportista.Text = ""
    End Sub

    Protected Sub cboClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedIndexChanged
        Call RescatarInfoclientes()
        Call CargarLugares()
        Call CargarServicio()
        Call CargarGrupoOperativo()
        Call CargarProductos()
        txtOS.Text = ""
        cboProducto.Enabled = True
        cboServicio.Enabled = True
        Call RescatarDepto()
        Call LimpiarRecursos()
    End Sub

    Private Sub Cargarconductores()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_programacion 'CHO' "
            cboConductor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductor.DataTextField = "nom_conductor"
            cboConductor.DataValueField = "rut_conductor"
            cboConductor.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargarconductores", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboConductor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConductor.SelectedIndexChanged
        Call RescatarDatosConductorPRO(cboConductor.SelectedValue)
    End Sub

    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CCC', '" & Session("cod_usu_tc") & "'"
            cboClientes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientes.DataTextField = "nom_cliente1"
            cboClientes.DataValueField = "cod_msoft"
            cboClientes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarClientesKPI()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CCK','" & Session("cod_usu_tc") & "'"
            cboClienteKPI.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClienteKPI.DataTextField = "nom_cliente1"
            cboClienteKPI.DataValueField = "cod_msoft"
            cboClienteKPI.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientesKPI", Err, Page, Master)
        End Try
    End Sub

    'Private Sub CargarTurnosKPI()
    '    Try
    '        Dim strNomTablaR As String = "TCMae_lugares"
    '        Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCT'"
    '        cboTurnoKPI.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
    '        cboTurnoKPI.DataTextField = "nom_turno"
    '        cboTurnoKPI.DataValueField = "id_turno"
    '        cboTurnoKPI.DataBind()
    '    Catch ex As Exception
    '        MessageBoxError("CargarTurnosKPI", Err, Page, Master)
    '    End Try
    'End Sub

    Private Sub CargarProductos()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CCP', '" & cboClientes.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            cboProducto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboProducto.DataTextField = "nom_producto"
            cboProducto.DataValueField = "cod_producto"
            cboProducto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarProductos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboTipoLugar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoLugar.SelectedIndexChanged
        Call CargarLugares()
        If cboLugar.SelectedValue <> "0" Then
            Call VerificarGeocercas(cboLugar.SelectedValue, cboLugar.SelectedItem.ToString)
        End If
    End Sub

    Private Sub CargaGrillaTemporal()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_ingreso_demanda 'GTP', '" & Session("cod_usu_tc") & "'"
            grdIngresoDemanda.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIngresoDemanda.DataBind()

            If grdIngresoDemanda.Rows.Count > 1 Then
                btnConfirmar.Visible = True
                btnConfirmModal.Visible = True
            Else
                btnConfirmar.Visible = False
                btnConfirmModal.Visible = False
            End If
        Catch ex As Exception
            MessageBoxError("CargaGrillaTemporal", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaGrillaConfirmados()
        Try
            grdConfirmados.Visible = True
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_ingreso_demanda 'GAS', '" & Session("cod_usu_tc") & "'"
            grdConfirmados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdConfirmados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaGrillaConfirmados", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPatentes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CPT', '" & cboClaOperativa.SelectedValue & "','" & cboServicio.SelectedValue & "', '" & CDate(txtFechaServicio.Text) & "',  '" & txtHoraservicio.Text & "', '" & Session("cod_usu_tc") & "'"
            cboCamion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCamion.DataTextField = "nom_patente"
            cboCamion.DataValueField = "nom_patente"
            cboCamion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPatentes", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarInfoclientes()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_ingreso_demanda 'RRC', '" & cboClientes.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnRutCliente.Value = rdoReader(0).ToString
                    hdnNomcliente.Value = rdoReader(1).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarInfoclientes", Err, Page, Master)
        End Try
    End Sub

    'Private Sub grdLugares_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdIngresoDemanda.RowCommand
    '    Try
    '        If Trim(LCase(e.CommandName)) = "1" Then
    '            Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
    '            Dim row As GridViewRow = grdIngresoDemanda.Rows(intRow)

    '            Call ActualizarDemandaTemporal()
    '            Call EliminarTemp(CType(row.FindControl("hdnId"), HiddenField).Value)
    '            Call CargaGrillaTemporal()
    '            Call CargarGrupoOperativo()
    '            cboClaOperativa.Enabled = True
    '            Call CargarKPI()

    '        End If
    '    Catch ex As Exception
    '        MessageBoxError("grdLugares_RowCommand", Err, Page, Master)
    '    End Try
    'End Sub


    Protected Sub EliminarClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        Call ActualizarDemandaTemporal()
        Call EliminarTemp(CType(row.FindControl("hdnId"), HiddenField).Value)

        Call CargaGrillaTemporal()

        Call CargarGrupoOperativo()
        cboClaOperativa.Enabled = True
        Call CargarKPI()
    End Sub


    Protected Sub EliminarTemp(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ETP"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarTemp", Err, Page, Master)
        End Try
    End Sub


    Private Sub LimpiarUnitario()
        txtOS.Text = ""
        btnConfirmar.Visible = False
        hdnOrigenDestino.Value = 0
        hdnRutCliente.Value = 0
        cboTipoLugar.ClearSelection()
        btnConfirmModal.Visible = False
        hdnConfirmar.Value = 0
        cboCamion.ClearSelection()
        cboCamion.Enabled = False
        txtArrastre.Text = ""
        txtArrastre.Enabled = False
        txtRutconductor.Text = ""
        cboConductor.ClearSelection()
        'txtRutconductor.Enabled = False
        btnVerDocCamion.Visible = False
        btnVerDocConductor.Visible = False
        btnVerDocArrastre.Visible = False
        imgGPSArrastre.Visible = False
        imgGPSCamion.Visible = False
        txtArrastre.BackColor = System.Drawing.Color.White
        cboCamion.BackColor = System.Drawing.Color.White
        txtRutconductor.BackColor = System.Drawing.Color.White
        txtFono.Text = ""
        txtFono.Enabled = False
        hdnHoraAtrasGrilla.Value = 0
        lblTrasnportista.Text = ""

        Call CargarClientes()
        Call RescatarInfoclientes()
        Call CargarLugares()
        Call CargarServicio()
        Call CargarProductos()
        Call CargarKPI()
        Call CargaGrillaTemporal()
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub grdIngresoDemanda_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdIngresoDemanda.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim objCboCamion As DropDownList = CType(row.FindControl("cboCamion"), DropDownList)
                Dim objCboLugar As DropDownList = CType(row.FindControl("cboLugar"), DropDownList)
                Dim objCboProducto As DropDownList = CType(row.FindControl("cboProducto"), DropDownList)
                Dim objCboServicio As DropDownList = CType(row.FindControl("cboServicio"), DropDownList)
                Dim objCboTipo As DropDownList = CType(row.FindControl("cboTipoLugar"), DropDownList)

                Dim objOS As Label = CType(row.FindControl("lblOS"), Label)
                Dim objRutCliente As HiddenField = CType(row.FindControl("hdnRutCliente"), HiddenField)
                Dim objfecha As TextBox = CType(row.FindControl("txtFecha"), TextBox)
                Dim objHora As TextBox = CType(row.FindControl("txtHora"), TextBox)


                Dim objCodTipo As HiddenField = CType(row.FindControl("hdnCodTipo"), HiddenField)
                Dim objCodCliente As HiddenField = CType(row.FindControl("hdnCodCliente"), HiddenField)
                Dim objCodLugar As HiddenField = CType(row.FindControl("hdnCodLugar"), HiddenField)
                Dim objCodProducto As HiddenField = CType(row.FindControl("hdnCodProducto"), HiddenField)
                Dim objIdServicio As HiddenField = CType(row.FindControl("hdnIdServicio"), HiddenField)
                Dim objGrupoOperativo As HiddenField = CType(row.FindControl("hdnCodGrupoOperativo"), HiddenField)
                Dim objPatenteCamion As HiddenField = CType(row.FindControl("hdnPatenteCamion"), HiddenField)
                Dim objEstMasivo As HiddenField = CType(row.FindControl("hdnEstMasivo"), HiddenField)


                If CDate(objfecha.Text).ToString("yyyy-MM-dd") < CDate(Date.Now.ToString).ToString("yyyy-MM-dd") Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                    hdnHoraAtrasGrilla.Value = 1
                Else
                    If objHora.Text <> "" Then
                        If CDate(objfecha.Text).ToString("yyyy-MM-dd") = CDate(Date.Now.ToString).ToString("yyyy-MM-dd") And CDate(objHora.Text).ToString("HH:mm") <= CDate(Date.Now.ToLocalTime).ToString("HH:mm") Then
                            e.Row.BackColor = System.Drawing.Color.LightSalmon
                            hdnHoraAtrasGrilla.Value = 1
                        End If
                    End If
                End If

                objCboLugar.SelectedValue = objCodLugar.Value
                objCboProducto.SelectedValue = objCodProducto.Value
                objCboServicio.SelectedValue = objIdServicio.Value
                objCboTipo.SelectedValue = objCodTipo.Value

                Call CargarComboServicios(objCboServicio, objCodCliente.Value)
                Call CargarComboProductoGrilla(objCboProducto, objCodCliente.Value)
                Call CargarComboLugares(objCboLugar, objCboTipo.SelectedValue, objCodCliente.Value)


                If objEstMasivo.Value = True Then
                    Call CargarComboCamionesGRILLA(objCboCamion, objCboServicio.SelectedValue, objGrupoOperativo.Value)
                    objCboCamion.SelectedValue = objPatenteCamion.Value
                    objCboCamion.Enabled = True
                    CType(row.FindControl("txtArrastre"), TextBox).Enabled = True
                    CType(row.FindControl("txtRutconductor"), TextBox).Enabled = True

                    If objCboCamion.SelectedItem.ToString = "0-SELECCIONE" Then
                        CType(row.FindControl("txtArrastre"), TextBox).Text = ""
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = ""
                        CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        CType(row.FindControl("lblFono"), Label).Text = ""
                        CType(row.FindControl("lblCodConductor"), Label).Text = ""
                    End If
                Else

                    If objGrupoOperativo.Value <> "0" Then
                        Call CargarComboCamionesGRILLA(objCboCamion, objIdServicio.Value, objGrupoOperativo.Value)
                        objCboCamion.SelectedValue = objPatenteCamion.Value
                        objCboCamion.Enabled = True
                        CType(row.FindControl("txtArrastre"), TextBox).Enabled = True
                        CType(row.FindControl("txtRutconductor"), TextBox).Enabled = True
                    Else
                        objCboCamion.Enabled = False
                        CType(row.FindControl("txtArrastre"), TextBox).Enabled = False
                        CType(row.FindControl("txtRutconductor"), TextBox).Enabled = False
                    End If

                    If objCboCamion.SelectedValue = "0-SELECCIONE" Then
                        hdnHoraAtrasGrilla.Value = 1
                        hdnRegistros.Value = 1
                        e.Row.BackColor = System.Drawing.Color.LightSalmon
                    End If

                End If


                Call VerificarDoctosCamion(CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("cboCamion"), DropDownList))
                Call VerificarDoctosArrastre(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox))
                Call VerificarDoctosConductorGrilla(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox))


                If hdnConfirmar.Value = 1 Then
                    If CType(row.FindControl("cboCamion"), DropDownList).BackColor = System.Drawing.Color.LightSteelBlue Then
                        Call EnviarCorreoDoctos(1, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue)
                    End If

                    If CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.LightSteelBlue Then
                        Call EnviarCorreoDoctos(1, CType(row.FindControl("txtArrastre"), TextBox).Text)
                    End If

                    If CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.LightSteelBlue Then
                        Call EnviarCorreoDoctos(2, CType(row.FindControl("txtRutconductor"), TextBox).Text)
                    End If
                End If


                Dim objGrupoCamion As HiddenField = CType(row.FindControl("hdnGrupoCamion"), HiddenField)
                If objGrupoCamion.Value <> objGrupoOperativo.Value Then
                    e.Row.BackColor = System.Drawing.Color.Tomato
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdIngresoDemanda_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub VerificarFechaGrilla(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        Dim objfecha As TextBox = CType(row.FindControl("txtFecha"), TextBox)
        Dim objHora As TextBox = CType(row.FindControl("txtHora"), TextBox)


        If CDate(objfecha.Text).ToString("yyyy-MM-dd") < CDate(Date.Now.ToString).ToString("yyyy-MM-dd") Then
            objfecha.Text = Session("fec_actual")
            objHora.Text = CDate(Date.Now.ToShortTimeString).ToString("HH:mm")

        End If
    End Sub


    Protected Sub CargarLugaresGrilla(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)
        Dim objCboLugar As DropDownList = CType(row.FindControl("cboLugar"), DropDownList)
        Dim objCboTipo As DropDownList = CType(row.FindControl("cboTipoLugar"), DropDownList)
        Dim objCodCliente As HiddenField = CType(row.FindControl("hdnCodCliente"), HiddenField)

        Dim objOS As Label = CType(row.FindControl("lblOS"), Label)
        Dim objRutCliente As HiddenField = CType(row.FindControl("hdnRutCliente"), HiddenField)
        Dim objfecha As TextBox = CType(row.FindControl("txtFecha"), TextBox)

        Call CargarComboLugares(objCboLugar, objCboTipo.SelectedValue, objCodCliente.Value)
        Call ActualizarDemandaTemporal()
    End Sub


    Private Sub CargarComboCamionesGRILLA(ByRef objCombo As DropDownList, ByVal servicio As String, ByVal cla As String)
        Dim strSQL As String = "Exec pro_ingreso_demanda 'CAG', '" & cla & "','" & servicio & "','" & Session("cod_usu_tc") & "'"

        Dim strNomTabla As String = "TCMov_demandas"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboCamionesGRILLA", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarComboLugares(ByRef objCombo As DropDownList, ByVal tipo_lugar As String, ByVal cliente As String)
        Dim strSQL As String = "Exec pro_ingreso_demanda 'COD', '" & tipo_lugar & "', '" & cliente & "','" & Session("cod_usu_tc") & "'"
        Dim strNomTabla As String = "TCMov_demandas"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBox("Cargar Lugares", "Lugar NO corresponde a Cliente", Page, Master, "W")
        End Try
    End Sub

    Private Sub CargarComboProducto(ByRef objCombo As DropDownList, ByVal cliente As String)
        Dim strSQL As String = "Exec pro_ingreso_demanda 'CCP', '" & cliente & "'"
        Dim strNomTabla As String = "TCMov_demandas"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBox("Cargar Productos", "Producto NO corresponde a Cliente", Page, Master, "W")
        End Try
    End Sub

    Private Sub CargarComboProductoGrilla(ByRef objCombo As DropDownList, ByVal cliente As String)
        Dim strSQL As String = "Exec pro_ingreso_demanda 'CPG', '" & cliente & "','" & Session("cod_usu_tc") & "'"
        Dim strNomTabla As String = "TCMov_demandas"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBox("Cargar Productos Grilla", "Producto NO corresponde a Cliente", Page, Master, "W")
        End Try
    End Sub

    Private Sub CargarComboServicios(ByRef objCombo As DropDownList, ByVal cliente As String)
        Dim strSQL As String = "Exec pro_ingreso_demanda 'CCS', '" & cliente & "', '" & Session("cod_usu_tc") & "'"

        Dim strNomTabla As String = "TCMov_demandas"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBox("Cargar Servicio", "Servicio NO corresponde a Cliente", Page, Master, "W")
        End Try
    End Sub


    Private Sub ExisteOrigenDestino()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'VOD', '" & Session("cod_usu_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnOrigenDestino.Value = 1
                Else
                    hdnOrigenDestino.Value = 0
                End If
            Catch ex As Exception
                MessageBoxError("ExisteOrigenDestino", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Call ActualizarDemandaTemporal()

        Call ExisteOrigenDestino()

        If hdnOrigenDestino.Value = 0 Then

            hdnConfirmar.Value = 1
            hdnHoraAtrasGrilla.Value = 0

            Call CargaGrillaTemporal()

            If hdnHoraAtrasGrilla.Value = 1 Then
                MessageBox("Confirmar Demanda", "Verifique los registros ingresados (FECHA, HORA, CAMIÓN)", Page, Master, "W")
            Else
                Call CrearDemanda()
                Call LimpiarUnitario()

                ' Call CargaGrillaConfirmados()
                grdConfirmados.Visible = False
                Call CargarDemandasDias()
                MessageBox("Confirmar Demanda", "Solicitud Enviada", Page, Master, "S")
            End If
        Else
            MessageBox("Confirmar Demanda", "Debe ingresar un origen y destino por OS", Page, Master, "W")
            Exit Sub
        End If
    End Sub

    Private Sub BloquearRecursos()
        cboClaOperativa.Enabled = False
        cboCamion.Enabled = False
        txtArrastre.Enabled = False
        cboConductor.Enabled = False
        ' txtRutconductor.Enabled = False
        cboProducto.Enabled = False
        cboServicio.Enabled = False
        'btnVerDocCamion.Enabled = False
        'btnVerDocConductor.Enabled = False
        'btnVerDocArrastre.Enabled = False
        imgGPSArrastre.Enabled = True
        imgGPSCamion.Enabled = True
        txtFono.Enabled = True
        hdnHoraAtrasGrilla.Value = 0
    End Sub


    Protected Sub InsertarTemporal()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "TMP"
            comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
            comando.Parameters("@rut_cliente").Value = hdnRutCliente.Value
            comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
            comando.Parameters("@id_servicio").Value = cboServicio.SelectedValue
            comando.Parameters.Add("@num_orden_servicio", SqlDbType.NVarChar)
            comando.Parameters("@num_orden_servicio").Value = Trim(txtOS.Text)
            comando.Parameters.Add("@tipo_viaje", SqlDbType.NVarChar)
            comando.Parameters("@tipo_viaje").Value = cboTipoLugar.SelectedValue
            comando.Parameters.Add("@cod_lugar", SqlDbType.NVarChar)
            comando.Parameters("@cod_lugar").Value = cboLugar.SelectedValue
            comando.Parameters.Add("@fec_servicio", SqlDbType.DateTime)
            comando.Parameters("@fec_servicio").Value = txtFechaServicio.Text
            comando.Parameters.Add("@hor_servicio", SqlDbType.NVarChar)
            comando.Parameters("@hor_servicio").Value = txtHoraservicio.Text
            comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
            comando.Parameters("@cod_producto").Value = cboProducto.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = Trim(Replace(cboCamion.SelectedValue, "-", ""))
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = Trim(Replace(txtArrastre.Text, "-", ""))
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = hdnCodConductor.Value
            comando.Parameters.Add("@cod_grupo_operativo", SqlDbType.NVarChar)
            comando.Parameters("@cod_grupo_operativo").Value = cboClaOperativa.SelectedValue
            comando.Parameters.Add("@des_cliente", SqlDbType.NVarChar)
            comando.Parameters("@des_cliente").Value = hdnNomcliente.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Call CargaGrillaTemporal()
            Call BloquearRecursos()
            cboLugar.ClearSelection()
        Catch ex As Exception
            MessageBoxError("InsertarTemporal", Err, Page, Master)
        End Try
    End Sub


    Protected Sub CrearDemanda()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IDE"
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CrearDemanda", Err, Page, Master)
        End Try
    End Sub

    Private Sub MismaFechaHora()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'MFH', '" & Trim(txtOS.Text) & "', '" & hdnRutCliente.Value & "', '" & Session("cod_usu_tc") & "', '" & CDate(txtFechaServicio.Text) & "', '" & txtHoraservicio.Text & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMismaFechaHora.Value = 1
                Else
                    hdnMismaFechaHora.Value = 0

                End If
            Catch ex As Exception
                MessageBoxError("MismaFechaHora", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub DevolverMail(ByVal nom_lugar As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec mant_correos 'CGE'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    Dim correo As New MailMessage
                    correo.From = New MailAddress("sistemas@viatrack.cl")
                    correo.To.Add(rdoReader(0).ToString)
                    correo.Subject = "[TC] Geocerca " & nom_lugar & " NO REGISTRADA"
                    correo.Body = "Estimado (a), se informa que la geocerca para el lugar: " & nom_lugar & " , no esta creada, favor revisar."
                    correo.IsBodyHtml = True
                    correo.Priority = MailPriority.Normal
                    Dim smtp As New SmtpClient
                    smtp.Host = "mail.viatrack.cl"
                    smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
                    smtp.Send(correo)
                    correo.Attachments.Clear()
                    correo.Dispose()

                End If
            Catch ex As Exception
                MessageBoxError("DevolverMail", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub



    Private Sub VerificarGeocercas(ByVal cod_lugar As String, ByVal nom_lugar As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'GEO', '" & cod_lugar & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnExisteGeocerca.Value = 1
                Else
                    MessageBox("Geocerca no existe", "La geocerca para  " & cboLugar.SelectedItem.ToString & " no esta creda, favor revisar", Page, Master, "W")
                    Call DevolverMail(nom_lugar)
                End If
            Catch ex As Exception
                MessageBoxError("VeriricarGeocercas", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub cboLugar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboLugar.SelectedIndexChanged
        If cboLugar.SelectedValue <> "0" Then
            Call VerificarGeocercas(cboLugar.SelectedValue, cboLugar.SelectedItem.ToString)
        End If

    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If Trim(txtOS.Text) = "" Or Trim(txtOS.Text) = "0" Or cboProducto.SelectedValue = "0" Or cboServicio.SelectedValue = "0" Or cboLugar.SelectedValue = "0" Then
            MessageBox("Agregar Registro", "Ingrese Servicio, Numero OS, Producto, Ubicación", Page, Master, "W")
        Else
            If CDate(txtFechaServicio.Text).ToString("yyyy-MM-dd") < CDate(Date.Now.ToString).ToString("yyyy-MM-dd") Then
                MessageBox("Agregar Registro", "La fecha seleccionada es menor a la actual", Page, Master, "W")
            Else

                If txtHoraservicio.Text = "" Then
                    If cboClaOperativa.SelectedValue <> "0" Then
                        If Trim(txtArrastre.Text) = "" Or Trim(txtRutconductor.Text) = "" Then
                            MessageBox("Agregar Registro", "Ingrese registros de pre asignación validos", Page, Master, "W")
                        Else

                            If cboCamion.BackColor = System.Drawing.Color.LightSteelBlue Then
                                Call EnviarCorreoDoctos(1, cboCamion.SelectedValue)
                            End If

                            If txtArrastre.BackColor = System.Drawing.Color.LightSteelBlue Then
                                Call EnviarCorreoDoctos(1, txtArrastre.Text)
                            End If

                            If txtRutconductor.BackColor = System.Drawing.Color.LightSteelBlue Then
                                Call EnviarCorreoDoctos(2, txtRutconductor.Text)
                            End If


                            Call ActualizarDemandaTemporal()
                            Call InsertarTemporal()
                        End If
                    Else
                        Call ActualizarDemandaTemporal()
                        Call InsertarTemporal()
                    End If
                Else
                    Call MismaFechaHora()

                    If hdnMismaFechaHora.Value = 1 Then
                        MessageBox("Agregar Registro", "Fecha y hora para OS ya existe", Page, Master, "W")
                    Else
                        If CDate(txtFechaServicio.Text).ToString("yyyy-MM-dd") = CDate(Date.Now.ToString).ToString("yyyy-MM-dd") And CDate(txtHoraservicio.Text).ToString("HH:mm") <= CDate(Date.Now.ToLocalTime).ToString("HH:mm") Then
                            MessageBox("Agregar Registro", "La hora seleccionada es menor a la actual", Page, Master, "W")
                        Else
                            If cboClaOperativa.SelectedValue <> "0" Then
                                If Trim(txtArrastre.Text) = "" Or Trim(txtRutconductor.Text) = "" Then
                                    MessageBox("Agregar Registro", "Ingrese registros de pre asignación validos", Page, Master, "W")
                                Else

                                    If cboCamion.BackColor = System.Drawing.Color.LightSteelBlue Then
                                        Call EnviarCorreoDoctos(1, cboCamion.SelectedValue)
                                    End If

                                    If txtArrastre.BackColor = System.Drawing.Color.LightSteelBlue Then
                                        Call EnviarCorreoDoctos(1, txtArrastre.Text)
                                    End If

                                    If txtRutconductor.BackColor = System.Drawing.Color.LightSteelBlue Then
                                        Call EnviarCorreoDoctos(2, txtRutconductor.Text)
                                    End If


                                    Call ActualizarDemandaTemporal()
                                    Call InsertarTemporal()
                                End If
                            Else
                                Call ActualizarDemandaTemporal()
                                Call InsertarTemporal()
                            End If
                        End If
                    End If
                End If
                Call CargarKPI()
            End If
        End If
    End Sub


    Protected Sub ActualizarDemandaTemporal()
        Try
            For iRow = 0 To grdIngresoDemanda.Rows.Count - 1
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_ingreso_demanda"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "UPD"
                comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
                comando.Parameters("@id_servicio").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("cboServicio"), DropDownList).SelectedValue
                comando.Parameters.Add("@tipo_viaje", SqlDbType.NVarChar)
                comando.Parameters("@tipo_viaje").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("cboTipoLugar"), DropDownList).SelectedValue
                comando.Parameters.Add("@cod_lugar", SqlDbType.NVarChar)
                comando.Parameters("@cod_lugar").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("cboLugar"), DropDownList).SelectedValue
                comando.Parameters.Add("@fecha", SqlDbType.DateTime)
                comando.Parameters("@fecha").Value = CDate(CType(grdIngresoDemanda.Rows(iRow).FindControl("txtFecha"), TextBox).Text)
                comando.Parameters.Add("@hora", SqlDbType.NVarChar)
                comando.Parameters("@hora").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("txtHora"), TextBox).Text
                comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
                comando.Parameters("@cod_producto").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("cboProducto"), DropDownList).SelectedValue
                comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
                comando.Parameters("@pat_camion").Value = Trim(Replace(CType(grdIngresoDemanda.Rows(iRow).FindControl("cboCamion"), DropDownList).SelectedValue, "-", ""))
                comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
                comando.Parameters("@pat_arrastre").Value = Trim(Replace(CType(grdIngresoDemanda.Rows(iRow).FindControl("txtArrastre"), TextBox).Text, "-", ""))
                comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
                comando.Parameters("@cod_conductor").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("lblCodConductor"), Label).Text
                comando.Parameters.Add("@id", SqlDbType.NVarChar)
                comando.Parameters("@id").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("hdnID"), HiddenField).Value
                comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
                comando.Parameters("@rut_cliente").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("hdnRutCliente"), HiddenField).Value
                comando.Parameters.Add("@num_orden_servicio", SqlDbType.NVarChar)
                comando.Parameters("@num_orden_servicio").Value = CType(grdIngresoDemanda.Rows(iRow).FindControl("hdnNumOS"), HiddenField).Value
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Next
        Catch ex As Exception
            MessageBoxError("ActualizarDemandaTemporal", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarRecursos(ByVal os As String, ByVal rut_cliente As String, ByVal pat_camion As String, ByVal pat_arrastre As String, ByVal cod_conductor As String, ByVal id_servicio As String, ByVal cod_producto As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "URE"
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = Trim(Replace(pat_camion, "-", ""))
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = Trim(Replace(pat_arrastre, "-", ""))
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = cod_conductor
            comando.Parameters.Add("@num_orden_servicio", SqlDbType.NVarChar)
            comando.Parameters("@num_orden_servicio").Value = os
            comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
            comando.Parameters("@rut_cliente").Value = rut_cliente
            comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
            comando.Parameters("@id_servicio").Value = id_servicio
            comando.Parameters.Add("@cod_producto", SqlDbType.NVarChar)
            comando.Parameters("@cod_producto").Value = cod_producto

            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("ActualizarRecursos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub CargarDatosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        If CType(row.FindControl("cboCamion"), DropDownList).SelectedValue = "" Then
            CType(row.FindControl("cboCamion"), DropDownList).SelectedValue = CType(row.FindControl("hdnPatenteAnterior"), HiddenField).Value
        Else
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'RDC', '" & Trim(Replace(CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                ' Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    CType(row.FindControl("cboCamion"), DropDownList).SelectedValue = Replace(Trim(rdoReader(0).ToString), "-", "")
                    Call VerificarDoctosCamion(CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("cboCamion"), DropDownList))

                    CType(row.FindControl("lblCodConductor"), Label).Text = Trim(rdoReader(1).ToString)
                    CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(rdoReader(4).ToString), "-", "")
                    Call VerificarDoctosArrastre(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox))

                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(rdoReader(3).ToString)
                    CType(row.FindControl("lblNomConductor"), Label).Text = Trim(rdoReader(2).ToString) & " / " & Trim(rdoReader(5).ToString)
                    Call VerificarDoctosConductorGrilla(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox))

                    Call ActualizarDemandaTemporal()
                    Call ActualizarRecursos(CType(row.FindControl("lblOS"), Label).Text, CType(row.FindControl("hdnRutCliente"), HiddenField).Value, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("lblCodConductor"), Label).Text, CType(row.FindControl("cboServicio"), DropDownList).SelectedValue, CType(row.FindControl("cboProducto"), DropDownList).SelectedValue)
                    Call CargaGrillaTemporal()
                    Call CargarPatentes()
                Else
                    CType(row.FindControl("cboCamion"), DropDownList).Text = CType(row.FindControl("hdnPatenteAnterior"), HiddenField).Value
                    MessageBox("Camión NO Valido", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
                'Catch ex As Exception
                '    MessageBoxError("CargarDatosCamion", Err, Page, Master)
                '    Exit Sub
                'Finally
                '    cnxAcceso.Close()
                'End Try
            End Using
        End If
    End Sub


    Private Sub VerificarDoctosConductorGrilla(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductorGrilla", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub ActualizarRecursosGrilla(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, DropDownList).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        Call ActualizarRecursos(CType(row.FindControl("lblOS"), Label).Text, CType(row.FindControl("hdnRutCliente"), HiddenField).Value, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("lblCodConductor"), Label).Text, CType(row.FindControl("cboServicio"), DropDownList).SelectedValue, CType(row.FindControl("cboProducto"), DropDownList).SelectedValue)

        Call CargaGrillaTemporal()
        Call ActualizarDemandaTemporal()
    End Sub



    Protected Sub CargarDatosArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        If CType(row.FindControl("txtArrastre"), TextBox).Text = "" Then
            CType(row.FindControl("txtArrastre"), TextBox).Text = CType(row.FindControl("hdnArrastreAnterior"), HiddenField).Value
        Else
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'RDC', '" & Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        Call VerificarDoctosArrastre(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox))
                        Call ActualizarRecursos(CType(row.FindControl("lblOS"), Label).Text, CType(row.FindControl("hdnRutCliente"), HiddenField).Value, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("lblCodConductor"), Label).Text, CType(row.FindControl("cboServicio"), DropDownList).SelectedValue, CType(row.FindControl("cboProducto"), DropDownList).SelectedValue)
                    Else
                        CType(row.FindControl("txtArrastre"), TextBox).Text = CType(row.FindControl("hdnArrastreAnterior"), HiddenField).Value
                        MessageBox("Arrastre NO Valido", "Arrastre no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                    End If
                Catch ex As Exception
                    MessageBoxError("CargarDatosCamion", Err, Page, Master)
                    Exit Sub
                Finally
                    cnxAcceso.Close()
                End Try
            End Using
        End If
    End Sub


    Protected Sub CargarDoctosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIngresoDemanda.Rows(rowIndex)

        If CType(row.FindControl("txtRutconductor"), TextBox).Text = "" Then
            CType(row.FindControl("txtRutconductor"), TextBox).Text = CType(row.FindControl("hdnRutConductorAnterior"), HiddenField).Value
        Else

            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'BDC', '" & Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then

                        CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), ".", "")
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), "-", "")
                        If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 8 Then
                            CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 7) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                        ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                            CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 8) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                        End If

                        If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 10 Then
                            CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)
                            If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                                CType(row.FindControl("lblNomConductor"), Label).Text = ""
                                Call RescatarDatosConductorGrilla(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("lblCodConductor"), Label).Text)
                                Call ActualizarRecursos(CType(row.FindControl("lblOS"), Label).Text, CType(row.FindControl("hdnRutCliente"), HiddenField).Value, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("lblCodConductor"), Label).Text, CType(row.FindControl("cboServicio"), DropDownList).SelectedValue, CType(row.FindControl("cboProducto"), DropDownList).SelectedValue)

                            Else
                                MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                                CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            End If
                        ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                            CType(row.FindControl("txtRutconductor"), TextBox).Text = "0" + Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)

                            If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                                CType(row.FindControl("lblNomConductor"), Label).Text = ""
                                Call RescatarDatosConductorGrilla(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("lblCodConductor"), Label).Text)
                                Call ActualizarRecursos(CType(row.FindControl("lblOS"), Label).Text, CType(row.FindControl("hdnRutCliente"), HiddenField).Value, CType(row.FindControl("cboCamion"), DropDownList).SelectedValue, CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("lblCodConductor"), Label).Text, CType(row.FindControl("cboServicio"), DropDownList).SelectedValue, CType(row.FindControl("cboProducto"), DropDownList).SelectedValue)

                            Else
                                MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                                CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            End If
                        End If

                    Else
                        MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = CType(row.FindControl("hdnRutConductorAnterior"), HiddenField).Value
                    End If
                Catch ex As Exception
                    MessageBoxError("RescatarDatosConductor", Err, Page, Master)
                    Exit Sub
                Finally
                    cnxAcceso.Close()
                End Try
            End Using

        End If

    End Sub


    Private Sub RescatarDatosConductorGrilla(ByVal rut As String, ByRef nombre As String, ByRef txtRut As TextBox, ByRef cod_conductor As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'BDC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    rut = Trim(rdoReader(0).ToString)
                    nombre = Trim(rdoReader(1).ToString) & " / " & Trim(rdoReader(2).ToString)
                    cod_conductor = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductorGrilla(rut, txtRut)
                Else
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductorGrilla", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub




    Private Sub RescatarEstadoGPSPRO(ByVal patente As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(patente, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnEstGPS.Value = rdoReader(0).ToString
                    hdnNomEstGPS.Value = rdoReader(1).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarEstadoGPS", Err, Page, Master)
        End Try
    End Sub


    Private Sub VerificarGPSArrastrePRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(txtArrastre.Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSArrastre.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 0 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSArrastre.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSArrastre", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub CargarDatosCamionPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'RDC', '" & Trim(Replace(cboCamion.SelectedValue, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    cboCamion.SelectedValue = Trim(rdoReader(0).ToString)

                    Call VerificarDoctosCamion(cboCamion.SelectedValue, cboCamion)

                    hdnCodConductor.Value = Trim(rdoReader(1).ToString)
                    txtArrastre.Text = Trim(rdoReader(4).ToString)


                    cboConductor.SelectedValue = Trim(rdoReader(3).ToString)

                    Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)

                    hdnRutconductor.Value = Trim(rdoReader(3).ToString)
                    txtRutconductor.Text = Trim(rdoReader(3).ToString)

                    '    txtNomconductor.Text = Trim(rdoReader(2).ToString)

                    txtFono.Text = Trim(rdoReader(5).ToString)
                    hdnCodTrasnportista.Value = Trim(rdoReader(6).ToString)
                    lblTrasnportista.Text = Trim(rdoReader(7).ToString)

                    If rdoReader(8).ToString = 1 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(8).ToString = 2 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(8).ToString = 0 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/nulo.png"
                    End If


                    If rdoReader(9).ToString = 1 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(9).ToString = 2 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(9).ToString = 0 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/nulo.png"
                    End If

                    Call VerificarDoctosConductorPRO(hdnRutconductor.Value, txtRutconductor)

                    If Trim(txtArrastre.Text) = "" Then
                        btnVerDocArrastre.Visible = False
                    Else
                        btnVerDocArrastre.Visible = True
                    End If

                    If Trim(txtRutconductor.Text) = "" Then
                        btnVerDocConductor.Visible = False
                    Else
                        btnVerDocConductor.Visible = True
                    End If

                    btnVerDocCamion.Visible = True
                Else
                    MessageBox("Camión NO Valido", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                    txtArrastre.Text = ""
                    hdnRutconductor.Value = 0
                    cboConductor.ClearSelection()
                    txtFono.Text = ""
                    btnVerDocCamion.Visible = False
                    btnVerDocArrastre.Visible = False
                    btnVerDocConductor.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamionPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Public Function GetGridviewData(gv As GridView) As String
        Dim strBuilder As New StringBuilder()
        Dim strWriter As New StringWriter(strBuilder)
        Dim htw As New HtmlTextWriter(strWriter)
        gv.RenderControl(htw)
        Return strBuilder.ToString()
    End Function


    Private Sub EnviarCorreoDoctos(ByVal tipo As String, ByVal id As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec mant_correos 'RCS', '" & cboServicio.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    Dim grdResumen As New GridView
                    grdResumen.CellPadding = 5
                    grdResumen.CellSpacing = 5
                    grdResumen.BorderStyle = BorderStyle.None
                    grdResumen.Font.Size = 10

                    strSQL = "Exec pro_programacion 'GED', '" & tipo & "', '" & id & "'"
                    Dim strNomTabla As String = "VJSMov_planificacion"
                    grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                    grdResumen.DataBind()

                    Dim correo As New MailMessage
                    correo.From = New MailAddress("sistemas@viatrack.cl")
                    correo.To.Add(rdoReader(0).ToString)
                    correo.Subject = "[TC] Documentos Vencidos: " & id
                    correo.Body = " Estimados, se esta intentando preasingar un recurso con documentos vencidos, RECURSO: <b> " & id & " </b>. <br /><br /> Documentos registrados: <br />" & GetGridviewData(grdResumen) & ".<br /><br /> "
                    correo.IsBodyHtml = True
                    correo.Priority = MailPriority.Normal
                    Dim smtp As New SmtpClient
                    smtp.Host = "mail.viatrack.cl"
                    smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
                    smtp.Send(correo)
                    correo.Attachments.Clear()
                    correo.Dispose()
                End If
            Catch ex As Exception
                MessageBoxError("EnviarCorreoDoctos", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarDoctosCamion(ByVal patente As String, ByRef txt As DropDownList)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosArrastre(ByVal patente As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarDoctosConductorPRO(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    'Protected Sub txtRutconductor_TextChanged(sender As Object, e As EventArgs) Handles txtRutconductor.TextChanged

    '    If Trim(txtRutconductor.Text) = "" Then
    '        txtRutconductor.BackColor = System.Drawing.Color.White
    '        txtNomconductor.Text = ""
    '        txtFono.Text = ""
    '        lblTrasnportista.Text = ""
    '        btnVerDocConductor.Visible = False
    '    Else

    '        txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), ".", "")
    '        txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), "-", "")
    '        If Len(Trim(txtRutconductor.Text)) = 8 Then
    '            txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 7) + "-" + Right(Trim(txtRutconductor.Text), 1)
    '        ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
    '            txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 8) + "-" + Right(Trim(txtRutconductor.Text), 1)
    '        End If

    '        If Len(Trim(txtRutconductor.Text)) = 10 Then
    '            txtRutconductor.Text = Trim(txtRutconductor.Text)
    '            If ValidarRut(txtRutconductor) = True Then
    '                txtNomconductor.Text = ""
    '                Call RescatarDatosConductorPRO()
    '            Else
    '                MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
    '                txtNomconductor.Text = ""
    '            End If
    '        ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
    '            txtRutconductor.Text = "0" + Trim(txtRutconductor.Text)
    '            If ValidarRut(txtRutconductor) = True Then
    '                txtNomconductor.Text = ""
    '                Call RescatarDatosConductorPRO()
    '            Else
    '                MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
    '                txtNomconductor.Text = ""
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub RescatarDatosConductorPRO(ByVal rut As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'BDC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtRutconductor.Text = Trim(rdoReader(0).ToString)
                    'txtNomconductor.Text = Trim(rdoReader(1).ToString)
                    txtFono.Text = Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(txtRutconductor.Text, txtRutconductor)
                    btnVerDocConductor.Visible = True
                Else
                    btnVerDocConductor.Visible = False
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub RescatarDepto()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'DEP', " & cboClientes.SelectedValue & ",'" & Session("cod_usu_tc") & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblDepto.Text = Trim(rdoReader(0).ToString)
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDepto", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub txtArrastre_TextChanged(sender As Object, e As EventArgs) Handles txtArrastre.TextChanged
        If Trim(txtArrastre.Text) = "" Then
            txtArrastre.BackColor = System.Drawing.Color.White
            imgGPSArrastre.Visible = False
            btnVerDocArrastre.Visible = False

        Else
            btnVerDocArrastre.Visible = True
            Call VerificarGPSArrastrePRO()
            Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)
        End If
    End Sub

    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_programacion 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnVerDocCamion_Click(sender As Object, e As EventArgs) Handles btnVerDocCamion.Click
        Call CargarGrillaDocumentos(1, Trim(Replace(cboCamion.SelectedValue, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(cboCamion.SelectedValue, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocArrastre_Click(sender As Object, e As EventArgs) Handles btnVerDocArrastre.Click
        Call CargarGrillaDocumentos(1, Trim(Replace(txtArrastre.Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(txtArrastre.Text, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocConductor_Click(sender As Object, e As EventArgs) Handles btnVerDocConductor.Click
        Call CargarGrillaDocumentos(2, Trim(txtRutconductor.Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(cboConductor.SelectedItem.ToString, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text
                If CDate(objFechaVenc) <= CDate(Date.Now.AddDays(5)) Then
                    e.Row.BackColor = System.Drawing.Color.LightSteelBlue
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboCamion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCamion.SelectedIndexChanged
        If cboCamion.SelectedValue = "0-SELECCIONE" Then
            txtArrastre.Text = ""
            txtArrastre.Enabled = False
            txtRutconductor.Text = ""
            cboConductor.ClearSelection()
            cboConductor.Enabled = False
            btnVerDocCamion.Visible = False
            btnVerDocConductor.Visible = False
            btnVerDocArrastre.Visible = False
            imgGPSArrastre.Visible = False
            imgGPSCamion.Visible = False
            txtArrastre.BackColor = System.Drawing.Color.White
            cboCamion.BackColor = System.Drawing.Color.White
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtFono.Text = ""
            txtFono.Enabled = False
            lblTrasnportista.Text = ""
        Else
            Call CargarDatosCamionPRO()
            txtArrastre.Enabled = True
            cboConductor.Enabled = True
            btnVerDocCamion.Visible = True
            btnVerDocConductor.Visible = True
            btnVerDocArrastre.Visible = True
            imgGPSArrastre.Visible = True
            imgGPSCamion.Visible = True
            txtFono.Enabled = True
        End If
    End Sub


    Protected Sub cboClaOperativa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClaOperativa.SelectedIndexChanged
        If cboClaOperativa.SelectedValue = "0" Then
            cboCamion.ClearSelection()
            cboCamion.Enabled = False
            txtArrastre.Text = ""
            txtArrastre.Enabled = False
            txtRutconductor.Text = ""
            cboConductor.ClearSelection()
            cboConductor.Enabled = False
            btnVerDocCamion.Visible = False
            btnVerDocConductor.Visible = False
            btnVerDocArrastre.Visible = False
            imgGPSArrastre.Visible = False
            imgGPSCamion.Visible = False
            hdnCodConductor.Value = 0
            txtArrastre.BackColor = System.Drawing.Color.White
            cboCamion.BackColor = System.Drawing.Color.White
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtFono.Text = ""
            txtFono.Enabled = False
            lblTrasnportista.Text = ""
        Else
            Call CargarPatentes()
            Call Cargarconductores()
            cboCamion.ClearSelection()
            cboCamion.Enabled = True
            txtArrastre.Text = ""
            txtArrastre.Enabled = False
            txtRutconductor.Text = ""
            cboConductor.ClearSelection()
            cboConductor.Enabled = False
            btnVerDocCamion.Visible = False
            btnVerDocConductor.Visible = False
            btnVerDocArrastre.Visible = False
            imgGPSArrastre.Visible = False
            imgGPSCamion.Visible = False
            hdnCodConductor.Value = 0
            txtArrastre.BackColor = System.Drawing.Color.White
            cboCamion.BackColor = System.Drawing.Color.White
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtFono.Text = ""
            txtFono.Enabled = True
            lblTrasnportista.Text = ""
        End If
    End Sub

    Protected Sub txtOS_TextChanged(sender As Object, e As EventArgs) Handles txtOS.TextChanged
        Call LimpiarRecursos()
        cboProducto.Enabled = True
        cboServicio.Enabled = True
        'Call CargarPatentes()
    End Sub


    ' **********************************************************************************************  CARGA  **********************************************************************************
    ' **************************************************************************************  CARGA A TRAVES DE PLANILLA ************************************************************************

    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\DemandasExcel\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath
        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()
                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("Carga Masiva", Err, Page, Master)
        End Try
    End Sub


    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBDTC_QA As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut_cliente", GetType(String)),
                                        New DataColumn("id_servicio", GetType(String)),
                                        New DataColumn("tipo_od", GetType(String)),
                                        New DataColumn("os", GetType(String)),
                                        New DataColumn("cod_lugar", GetType(String)),
                                        New DataColumn("fecha", GetType(String)),
                                        New DataColumn("hora", GetType(String)),
                                        New DataColumn("cod_producto", GetType(String)),
                                        New DataColumn("camion", GetType(String)),
                                        New DataColumn("arrastre", GetType(String)),
                                        New DataColumn("rut_conductor", GetType(String))
                                             })

            cnxBDTC_QA.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT * FROM [Hoja1$]", cnxBDTC_QA)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFilaN.Item(2) = dtTemporal(i).Item(2)
                dtFilaN.Item(3) = dtTemporal(i).Item(3)
                dtFilaN.Item(4) = dtTemporal(i).Item(4)
                dtFilaN.Item(5) = dtTemporal(i).Item(5)
                dtFilaN.Item(6) = dtTemporal(i).Item(6)
                dtFilaN.Item(7) = dtTemporal(i).Item(7)
                dtFilaN.Item(8) = dtTemporal(i).Item(8)
                dtFilaN.Item(9) = dtTemporal(i).Item(9)
                dtFilaN.Item(10) = dtTemporal(i).Item(10)

                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBDTC_QA.Close()
            cnxBDTC_QA.Dispose()
        End Try
        Return dtFinal
    End Function



    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function
    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\DemandasExcel\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Protected Sub btnCargarDocto_Click(sender As Object, e As EventArgs) Handles btnCargarDocto.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdTempMasiva)

            If GrabarDatos() = True Then
                Call InsertarMasivoATemp()
                Call CargaGrillaTemporal()
                If hdnRegistros.Value = 1 Then
                    MessageBox("Carga Masiva", "Registros cargados pero con observaciones (Fecha menor a la actual, Camión mal digitado)", Page, Master, "I")
                Else
                    MessageBox("Carga Masiva", "Registros Cargados Exitosamente", Page, Master, "S")
                End If
                hdnRegistros.Value = 0

            Else
                MessageBox("Carga Masiva", "Se produjo un error al cargar, favor revisar registros a ingresar", Page, Master, "W")
            End If
        Else
            MessageBox("Carga Masiva", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub

    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBDTC_QA As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBDTC_QA.Open()
        Try
            For intRow = 0 To grdTempMasiva.Rows.Count - 1

                Dim objCliente As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblNomCliente"), Label)
                Dim objServicio As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblServicio"), Label)
                Dim objTipoViaje As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblTipo"), Label)
                Dim objOS As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblOS"), Label)
                Dim objLugar As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblCodLugar"), Label)
                Dim objFecha As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblFecha"), Label)
                Dim objHora As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblHora"), Label)
                Dim objProducto As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblCodProducto"), Label)
                Dim objCamion As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblCamion"), Label)
                Dim objArrastre As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblArrastre"), Label)
                Dim objConductor As Label = CType(grdTempMasiva.Rows(intRow).FindControl("lblConductor"), Label)

                If CDate(objFecha.Text) < CDate(Date.Now.ToString) Then
                    hdnRegistros.Value = 1
                End If

                Call InsertarCargaDemanda(objCliente.Text, objServicio.Text, objTipoViaje.Text, objOS.Text, objLugar.Text, objFecha.Text, objHora.Text, objProducto.Text, objCamion.Text, objArrastre.Text, objConductor.Text)


            Next
            cnxBDTC_QA.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Protected Sub InsertarMasivoATemp()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IMT"
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsertarMasivoATemp", Err, Page, Master)
        End Try
    End Sub

    Protected Sub InsertarCargaDemanda(ByVal cliente As String, ByVal servicio As String, ByVal tipo_viaje As String, ByVal os As String, ByVal lugar As String, ByVal fecha As String, ByVal hora As String, ByVal producto As String, ByVal camion As String, ByVal arrastre As String, ByVal conductor As String)
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_ingreso_demanda"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ICD"
            comando.Parameters.Add("@rut_cliente", SqlDbType.NVarChar)
            comando.Parameters("@rut_cliente").Value = Trim(cliente)
            comando.Parameters.Add("@id_servicio_masivo", SqlDbType.NVarChar)
            comando.Parameters("@id_servicio_masivo").Value = Trim(servicio)
            comando.Parameters.Add("@tipo_viaje", SqlDbType.NVarChar)
            comando.Parameters("@tipo_viaje").Value = Trim(tipo_viaje)
            comando.Parameters.Add("@os_masivo", SqlDbType.NVarChar)
            comando.Parameters("@os_masivo").Value = Trim(os)
            comando.Parameters.Add("@cod_lugar_masivo", SqlDbType.NVarChar)
            comando.Parameters("@cod_lugar_masivo").Value = Trim(lugar)
            comando.Parameters.Add("@fecha_masivo", SqlDbType.NVarChar)
            comando.Parameters("@fecha_masivo").Value = Trim(fecha)
            comando.Parameters.Add("@hora", SqlDbType.NVarChar)
            comando.Parameters("@hora").Value = Trim(hora)
            comando.Parameters.Add("@cod_producto_masivo", SqlDbType.NVarChar)
            comando.Parameters("@cod_producto_masivo").Value = Trim(producto)
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = Trim(camion)
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = Trim(arrastre)
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = Trim(conductor)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("InsertarDemanda", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec pro_ingreso_demanda 'EXE', '" & Session("cod_usu_tc") & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=DemandasIngresadas_: " & Date.Now.Date & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("lnkExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnExportarDia_Click(sender As Object, e As EventArgs) Handles btnExportarDia.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String

            strSQL = "Exec pro_ingreso_demanda 'GDI', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
            Dim strNomTabla As String = "VJSMov_planificacion"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Demandas_: " & Date.Now.Date & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarDia_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs) Handles txtDesde.TextChanged
        If CDate(txtDesde.Text) > CDate(txtHasta.Text) Then
            txtHasta.Text = txtDesde.Text
        End If

        Call CargarKPI()
        Call CargarDemandasDias()
    End Sub

    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs) Handles txtHasta.TextChanged
        If CDate(txtHasta.Text) < CDate(txtDesde.Text) Then
            txtDesde.Text = txtHasta.Text
        End If

        Call CargarKPI()
        Call CargarDemandasDias()
    End Sub

    Protected Sub cboClienteKPI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClienteKPI.SelectedIndexChanged
        Call CargarKPI()
        Call CargarDemandasDias()
    End Sub

    Protected Sub cboServicioKPI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicioKPI.SelectedIndexChanged
        Call CargarKPI()
        Call CargarDemandasDias()
    End Sub


    Private Sub CargarDemandasDias()
        Dim strNomTabla As String = "TCMov_viajes"
        Dim strSQL As String = "Exec pro_ingreso_demanda 'GDI', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
        grdDemandaDia.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
        grdDemandaDia.DataBind()
    End Sub


    Private Sub CargaGrillaKPI(ByVal tipo As String)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
        Try
            If tipo = "TOTAL_DEMANDAS" Then
                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "TOTAL DEMANDAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDI', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()

            ElseIf tipo = "ASIGNADOS" Then

                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "DEMANDAS ASIGNADAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDA', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()


            ElseIf tipo = "NO_ASIGNADOS" Then
                grdDemandasKPI.Visible = True
                grdDisponiblesKPI.Visible = False

                lblNomModal.Text = "DEMANDAS NO ASIGNADAS"

                Dim strNomTabla As String = "TCMov_viajes"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GDN', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
                grdDemandasKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDemandasKPI.DataBind()


            ElseIf tipo = "DISPONIBLES" Then
                grdDemandasKPI.Visible = False
                grdDisponiblesKPI.Visible = True
                lblNomModal.Text = "CAMIONES DISPONIBLES"

                Dim strNomTabla As String = "VW_Patentes_Disponibles"
                Dim strSQL As String = "Exec pro_ingreso_demanda 'GPT', '" & Session("cod_usu_tc") & "','" & cboClienteKPI.SelectedValue & "', '" & cboServicioKPI.SelectedValue & "', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & hdnSemana.Value & "'"
                grdDisponiblesKPI.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
                grdDisponiblesKPI.DataBind()
            End If

        Catch ex As Exception
            MessageBoxError("CargaGrillaKPI", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnTotalKPI_Click(sender As Object, e As EventArgs) Handles btnTotalKPI.Click
        Call CargaGrillaKPI("TOTAL_DEMANDAS")
    End Sub

    Protected Sub btnAsignadosKPI_Click(sender As Object, e As EventArgs) Handles btnAsignadosKPI.Click
        Call CargaGrillaKPI("ASIGNADOS")
    End Sub

    Protected Sub btnNOAsignadosKPI_Click(sender As Object, e As EventArgs) Handles btnNOAsignadosKPI.Click
        Call CargaGrillaKPI("NO_ASIGNADOS")
    End Sub

    Protected Sub btnDisponiblesKPI_Click(sender As Object, e As EventArgs) Handles btnDisponiblesKPI.Click
        Call CargaGrillaKPI("DISPONIBLES")
    End Sub

    'Protected Sub chkSemana_CheckedChanged(sender As Object, e As EventArgs) Handles chkSemana.CheckedChanged
    '    If chkSemana.Checked = True Then
    '        hdnSemana.Value = 1
    '    Else
    '        hdnSemana.Value = 0
    '    End If

    '    Call CargarKPI()

    'End Sub

    Private Sub DescargarFTP(ByRef archivo As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/PlantillaDemanda/Carga_Masiva.xlsx", Server.MapPath(archivo), "usuarioftp", "Ftp2018.")

        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub




    Protected Sub bgnDesPlantilla1_Click(sender As Object, e As EventArgs) Handles bgnDesPlantilla1.Click
        Dim archivo As String = "~/Temp/Carga_Masiva.xlsx"

        Call DescargarFTP(archivo)

        Response.ContentType = "application/ms-word"
        Response.AppendHeader("Content-Disposition", "attachment;filename= Plantilla Carga Masiva.xlsx")
        Response.TransmitFile(Server.MapPath(archivo))
        Response.End()
    End Sub

    Protected Sub btnLimpiarFiltros_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltros.Click
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")

        cboClienteKPI.ClearSelection()
        cboServicioKPI.ClearSelection()

        Call CargarServiciosKPI()
        Call CargarClientesKPI()

        Call CargarKPI()
        Call CargarDemandasDias()
    End Sub
End Class