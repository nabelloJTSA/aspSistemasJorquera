﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class aspGestionViajes
    
    '''<summary>
    '''Control cboEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstado As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnControlVW.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnControlVW As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnAlertasVW.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAlertasVW As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnCicloVW.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCicloVW As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lbltitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltitulo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtDesde.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDesde As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtHasta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHasta As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnASignar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnASignar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control LinkButton1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LinkButton1 As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control MultiView1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MultiView1 As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Control vwIngreso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents vwIngreso As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control lblTotDemandas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotDemandas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTotalViajes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalViajes As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAsignados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAsignados As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnOrigen As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnRuta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnRuta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnDestino As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTerminados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTerminados As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTermiandosPOD.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTermiandosPOD As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTerminadosPODValido.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTerminadosPODValido As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblCumplimiento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCumplimiento As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control imgCumplViajes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgCumplViajes As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control lblTotalAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalAlertas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label7.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAlertasGestionadas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAlertasGestionadas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label8.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label8 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAlertasPendientes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAlertasPendientes As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control imgAlertasPendientes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgAlertasPendientes As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control Label5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblCumplimientoAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCumplimientoAlertas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboClientes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboClientes As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboServicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboServicio As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboTrasportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTrasportista As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboPOD.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPOD As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboOrigen As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboDestinoKPI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDestinoKPI As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboAlertas As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control txtExp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtExp As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtGuia.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtGuia As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnLimpiarFiltroGral.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarFiltroGral As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lbltotRegistrosdAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltotRegistrosdAlertas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboEstadoAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstadoAlertas As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnLimpiarAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarAlertas As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnExportarAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportarAlertas As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control grdAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdAlertas As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control Label11.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTotReg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotReg As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control btnExportarViajes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportarViajes As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control grdViajes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdViajes As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnViajeBaremos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnViajeBaremos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnSecuenciaBaremos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnSecuenciaBaremos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control Label1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtBValor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtBValor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cboBCargas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboBCargas As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboBPuntoPaso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboBPuntoPaso As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control grdBar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdBar As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control UpdatePanel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Control btnBInsertarBaremos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnBInsertarBaremos As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control pnlDoctos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDoctos As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control lblNomModal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNomModal As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblIdentificador.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblIdentificador As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTrasnportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTrasnportista As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdDocumentacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdDocumentacion As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control pnlDetalleViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDetalleViaje As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control Label2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblNumOS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblNumOS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdMultipaso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdMultipaso As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control pnlKPI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlKPI As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control lblTituloKPI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTituloKPI As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdDemandasKPI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdDemandasKPI As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control grdDisponiblesKPI.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdDisponiblesKPI As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control lblTituloIncidencias.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTituloIncidencias As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdIncidencias.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdIncidencias As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control grdAlertasPopUp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdAlertasPopUp As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control updClientes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents updClientes As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Control lblTituloActualizar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTituloActualizar As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control pnlCerrarViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCerrarViaje As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control pnlCancelarViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCancelarViaje As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control pnlDestinos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDestinos As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control cboDestinos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDestinos As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control pnlCamion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCamion As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control cboGO.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboGO As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboCamion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboCamion As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control imgGPSCamion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgGPSCamion As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control Label3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control hdnFechaServicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFechaServicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control pnlArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlArrastre As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control txtArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtArrastre As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control imgGPSArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgGPSArrastre As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control pnlConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlConductor As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control txtRutconductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRutconductor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnVerDocConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnVerDocConductor As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control cboConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboConductor As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control txtFono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFono As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control hdnCodConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodConductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control txtOBS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtOBS As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnConfirmar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnConfirmar As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control TimerViajes.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TimerViajes As Global.System.Web.UI.Timer
    
    '''<summary>
    '''Control vwAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents vwAlertas As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control lblTotalAlertasAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalAlertasAlerta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblGestionadasAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblGestionadasAlerta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblPendientesAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPendientesAlerta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtDesdeAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDesdeAlertas As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtHastaAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHastaAlertas As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cboClientealertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboClientealertas As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnLimpiarFiltros.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarFiltros As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Control lblTitAlertasTipo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTitAlertasTipo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTotalAlertasMesActual.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalAlertasMesActual As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdAlertasTipo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdAlertasTipo As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control lblTotRegistrosRelacioandos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotRegistrosRelacioandos As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control btnExportarRelacionados.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportarRelacionados As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control grdViajesAlertas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdViajesAlertas As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control vwCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents vwCiclo As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control lblTotalViajesCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalViajesCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTotDemandasCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotDemandasCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAsignadosCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAsignadosCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnOrigenCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnOrigenCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnRutaCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnRutaCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblEnDestinoCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEnDestinoCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTerminadosCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTerminadosCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTermiandosPODCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTermiandosPODCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblTerminadosPODValidoCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTerminadosPODValidoCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label13.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label13 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblCumplimientoCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCumplimientoCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control imgCumplCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgCumplCiclo As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control lblTotalAlertasCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblTotalAlertasCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label10.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label10 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAlertasGestionadasCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAlertasGestionadasCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control Label9.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label9 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblAlertasPendientesCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblAlertasPendientesCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control imgAlertasPendientesCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents imgAlertasPendientesCiclo As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''Control Label14.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Label14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblCumplimientoAlertasCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblCumplimientoAlertasCiclo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control txtDesdeCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDesdeCiclo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtHastaciclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtHastaciclo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cboClientesCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboClientesCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboServiciosCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboServiciosCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboTransportistaCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboTransportistaCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboPODCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPODCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboEstadoCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEstadoCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboOrigenCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboOrigenCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboAlertasCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboAlertasCiclo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control btnLimpiarFiltrosCiclo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarFiltrosCiclo As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lblPromedioOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPromedioOrigen As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMinimoOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMinimoOrigen As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMaximoOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMaximoOrigen As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblPromedioRuta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPromedioRuta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMinimoruta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMinimoruta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMaximoRuta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMaximoRuta As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblPromedioDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblPromedioDestino As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMinimoDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMinimoDestino As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control lblMaximoDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblMaximoDestino As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control grdTiempos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdTiempos As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnIdViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdViaje As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnhoraservicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnhoraservicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodTrasnportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodTrasnportista As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodCliente As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdDemanda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdDemanda As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLat.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLat As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLon.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLon As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLonViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLonViaje As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLatViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLatViaje As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLatAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLatAlerta As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLonAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLonAlerta As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutconductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutconductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNomconductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNomconductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnfonoConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnfonoConductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPatArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPatArrastre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodUnidad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodUnidad As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPatente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPatente As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnGPS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnGPS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEstGPS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEstGPS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFecAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFecAlerta As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumOS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumOS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdServicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdServicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnID.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnTipoAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnTipoAlerta As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnTipoAlertaGrillaInformacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnTipoAlertaGrillaInformacion As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control updProgress2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents updProgress2 As Global.System.Web.UI.UpdateProgress
End Class
