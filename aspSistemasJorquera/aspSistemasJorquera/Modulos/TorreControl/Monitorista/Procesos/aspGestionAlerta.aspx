﻿<%@ Page Title="" Language="vb" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspGestionAlerta.aspx.vb" Inherits="aspSistemasJorquera.aspGestionAlerta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <script type='text/javascript'>
        function openModal() {
            $('[id*=modalFinalizar]').modal('show');
        }
    </script>


    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-9 form-group">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="GESTION ALERTAS"></asp:Label>
                    <h3>
                        <asp:Label ID="lblNomAlerta" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label>
                        /
                        <asp:Label ID="Label14" runat="server" CssClass="text-primary" Font-Bold="true" Text=" ID:"></asp:Label>
                        <asp:Label ID="lblIdCallCentar" runat="server" CssClass="text-primary" Font-Bold="true" Text=" ID - "></asp:Label>
                        <br />
                        <h4>
                            <asp:Label ID="lblDescripcion" runat="server" CssClass="text-primary" Text="aaa"></asp:Label>
                        </h4>
                    </h3>

                </h3>


                <asp:Label ID="label3" runat="server" CssClass="text-primary" Font-Bold="true" Text="VIAJE: "></asp:Label>
                <asp:Label ID="lblNumviaje" Font-Size="X-Large" runat="server" CssClass="text-primary" Font-Bold="true" Text="0"></asp:Label>
                <asp:Label ID="label1" runat="server" CssClass="text-primary" Font-Bold="true" Text="/ PATENTE: "></asp:Label>
                <asp:Label ID="lblPatCamion" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                <asp:Label ID="label2" runat="server" CssClass="text-primary" Font-Bold="true" Text="/ CONDUCTOR: "></asp:Label>
                <asp:Label ID="lblConductor" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                <asp:Label ID="label5" runat="server" CssClass="text-primary" Font-Bold="true" Text="/ ORIGEN: "></asp:Label>
                <asp:Label ID="lblOrigen" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                <asp:Label ID="label7" runat="server" CssClass="text-primary" Font-Bold="true" Text="/ DESTINO: "></asp:Label>
                <asp:Label ID="lblDestino" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                <asp:Label ID="label9" runat="server" CssClass="text-primary" Font-Bold="true" Text="/ CLIENTE: "></asp:Label>
                <asp:Label ID="lblCliente" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>


                <h4>
                    <asp:Label ID="label12" runat="server" CssClass="text-primary" Font-Bold="true" Text="SCORE VIAJE: "></asp:Label>
                    <asp:Label ID="lblScoreViaje" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>

                </h4>

            </div>

            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <button type="button" id="btnvolver1" runat="server" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalVolver">
                        Volver
                    </button>
                </div>
            </div>

            <%--  <div class="col-md-2 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>
                    <button type="button" id="btnFinGestion" runat="server" class="btn bg-orange btn-block" visible="false" data-toggle="modal" data-target="#modalFinalizar">
                        Finalizar Gestión
                    </button>
                </div>
            </div>--%>


            <!-- Modal CONFIRMACION -->
            <div class="modal" id="modalFinalizar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                <div class="modal-dialog  modal-md">
                    <div class="modal-content">

                        <div class="modal-body">
                            <asp:UpdatePanel runat="server" ID="updClientes">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnConfirmarFinalizar" />
                                </Triggers>
                                <ContentTemplate>
                                    <div class="modal-header bg-success">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title ">Finalizar Gestion Alerta</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿ESTA SEGURO QUE DESEA FINALIZAR LA GESTIÓN? &hellip;</p>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div class="modal-footer">

                            <asp:Button ID="btnConfirmarFinalizar" CssClass="btn btn-primary " OnClientClick="$('#modalFinalizar').modal('hide');" runat="server" Visible="true" Text="FINALIZAR" />
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
            <%--fin modal--%>

            <div class="modal" id="modalVolver" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modalClientesLabel" aria-hidden="true">
                <div class="modal-dialog  modal-md">
                    <div class="modal-content">

                        <div class="modal-body">
                            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnConfirnarVolver" />
                                </Triggers>
                                <ContentTemplate>
                                    <div class="modal-header bg-success">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title ">VOLVER</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>¿ESTA SEGURO QUE DESEA VOLVER A GESTIÓN VIAJES? &hellip;</p>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div class="modal-footer">

                            <asp:Button ID="btnConfirnarVolver" CssClass="btn btn-primary " OnClientClick="$('#modalVolver').modal('hide');" runat="server" Visible="true" Text="VOLVER" />
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>


            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                <ContentTemplate>
                    <center> 
                    <div class="col-md-2 col-sm-6 col-xs-12 form-group pull-right">                        
                        <h1> <asp:Label ID="lbl3" runat="server" CssClass="text-primary" Font-Bold="true" Visible="false" Text="0"></asp:Label>
                            <asp:Label ID="lbl2" runat="server" CssClass="text-primary" Font-Bold="true" Text="00"></asp:Label><asp:Label ID="Label4" runat="server" CssClass="text-primary" Font-Bold="true" Text=":"></asp:Label>
                            <asp:Label ID="lbl1" runat="server" CssClass="text-primary" Font-Bold="true" Text="00"></asp:Label>
                        </h1>   
                         <h4><span class="text-muted text-primary">SLA</span></h4>                                                
                    </div>                 
                              
                        </center>
                    <asp:Timer ID="timerSLA" runat="server" Interval="1000"></asp:Timer>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

        <!-- SELECT2 EXAMPLE -->

        <div class="row">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li>
                                <asp:LinkButton ID="btnN1" Width="280px" CssClass="bg-primary text-center" runat="server">
                                    <asp:Label ID="lblN1" runat="server" Text=""></asp:Label>
                                </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="btnN2" Width="280px" CssClass="bg-primary text-center" Enabled="false" runat="server">
                                    <asp:Label ID="lblN2" runat="server" Text=""></asp:Label>
                                </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="btnN3" Width="280px" CssClass="bg-primary text-center" Enabled="false" runat="server">
                                    <asp:Label ID="lblN3" runat="server" Text=""></asp:Label>
                                </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="btnN4" Width="280px" CssClass="bg-primary text-center" Enabled="false" runat="server">
                                    <asp:Label ID="lblN4" runat="server" Text=""></asp:Label>
                                </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="btnN5" Width="280px" CssClass="bg-primary text-center" Enabled="false" runat="server">
                                    <asp:Label ID="lblN5" runat="server" Text=""></asp:Label>
                                </asp:LinkButton></li>
                        </ul>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">

                        <br />

                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-6">

                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title">
                                            <asp:Label ID="lblTituloNivel" runat="server" Text="NIVEL 1"></asp:Label>
                                            - Contactar a:</h3>
                                    </div>
                                    <div class="box-body">

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-sm-8">

                                                    <asp:GridView ID="grdContactos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                        <Columns>


                                                            <asp:TemplateField HeaderText="CONTACTO">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNomContacto" runat="server" Text='<%# Bind("nom_contacto")%>' />
                                                                    <asp:HiddenField ID="hdnIdContacto" runat="server" Value='<%# Bind("id_contacto")%>' />
                                                                    <asp:HiddenField ID="hdnFono" runat="server" Value='<%# Bind("fono")%>' />
                                                                    <asp:HiddenField ID="hdnCorreo" runat="server" Value='<%# Bind("correo")%>' />
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="EstFilasGrilla" />
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnContactar" CssClass="btn btn-primary" OnClick="btnContactarClick" runat="server" Visible="true"><i class="fa fa-phone-square"></i> CONTACTAR</asp:LinkButton>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>

                                                <div class="col-sm-4">

                                                    <asp:Label ID="lblCorreo" runat="server" CssClass="text-primary" Font-Bold="true" Text=""></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblFono" runat="server" CssClass="text-primary" Font-Bold="true" Text=""></asp:Label>

                                                </div>
                                            </div>
                                        </div>

                                        <br />

                                        <div class="row">
                                            <div class="col-xs-12">

                                                <div class="col-sm-12">
                                                    <asp:Label ID="lblScript" runat="server" CssClass="text-primary" Font-Bold="true" Text=""></asp:Label>
                                                </div>

                                            </div>
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-sm-8">
                                                    <span class="text-muted text-primary">
                                                        <asp:Label ID="lblTitExplicacion" runat="server" CssClass="text-primary" Visible="false" Font-Bold="true" Text="Explicacion"></asp:Label></span>
                                                    <asp:DropDownList ID="cboExplicacion" CssClass="form-control" Visible="false" runat="server"></asp:DropDownList>
                                                </div>

                                                <div class="col-sm-3">
                                                    <span class="text-muted text-primary pull-right">
                                                        <br />
                                                        <asp:CheckBox ID="chkContactado"  CssClass="checkbox" runat="server" Text="CONTACTADO" />
                                                    </span>

                                                </div>

                                            </div>
                                        </div>

                                        <br />
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-sm-12">

                                                    <asp:TextBox ID="txtObs" runat="server" CssClass="form-control" Visible="false" placeholder="Observaciones..." TextMode="MultiLine"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>

                                        <br />
                                        <div class="row ">

                                            <div class="col-md-6 form-group">
                                                <button type="button" id="btnFinGestion" runat="server" class="btn btn-primary btn-block" visible="false" data-toggle="modal" data-target="#modalFinalizar">
                                                    Finalizar Gestión
                                                </button>
                                            </div>

                                            <div class="col-md-6 form-group">

                                                <asp:Button ID="btnGestion" CssClass="btn btn-primary btn-block" Visible="false" Enabled="false" runat="server" Text="Siguiente Escalamiento" />

                                            </div>


                                        </div>
                                    </div>
                                </div>
                             

                                <div class="box box-success">
                                    <div class="box-header with-border bg-success">
                                        <h3 class="box-title ">Bitacora Escalamiento</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->

                                    <div class="box-body table-responsive">
                                        <div class="form-group table-responsive">

                                            <asp:GridView ID="grdEscalamientos" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ALERTA">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAlerta" runat="server" Text='<%# Bind("ALERTA")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="NIVEL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNivel" runat="server" Text='<%# Bind("NIVEL")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="CONTACTO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblContacto" runat="server" Text='<%# Bind("CONTACTO")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="EXPLICACION">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblExplicacion" runat="server" Text='<%# Bind("EXPLICACION")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OBSERVACION">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOBS" runat="server" Text='<%# Bind("OBSERVACION")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                       <asp:TemplateField HeaderText="FECHA">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("FECHA")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TIEMPO GESTION">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTiempo" runat="server" Text='<%# Bind("TIEMPO_GESTION")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                       <asp:TemplateField HeaderText="GESTIONADO POR">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGestioandoPor" runat="server" Text='<%# Bind("GESTIONADO_POR")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                       <asp:TemplateField HeaderText="CONTACTADO">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblContactado" runat="server" Text='<%# Bind("CONTACTADO")%>' />
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                            </asp:GridView>

                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                </div>
                            </div>


                            <div class="col-md-6">
                                <!-- Horizontal Form -->
                                <div class="box box-success">
                                    <div class="box-body">
                                        <div class="box-group" id="accordion">
                                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                            <div class="">
                                                <div class="box-header with-border">
                                                    <h4 class="box-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Alertas Asociadas
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse">
                                                    <div class="box-body">

                                                        <div style="overflow: auto; height: 300px">
                                                            <asp:GridView ID="grdAlertas" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ID ALERTA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblIdAlerta" runat="server" Text='<%# Bind("id_call_center")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="N° VIAJE">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblNomAlerta" runat="server" Text='<%# Bind("id_viaje")%>' />

                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="TIPO ALERTA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTipoAlerta" runat="server" Text='<%# Bind("nom_alerta")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="FECHA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFechaAlerta" runat="server" Text='<%# Bind("fec_alerta")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="PATENTE">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("nom_patente")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CRITICIDAD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCriticidad" runat="server" Text='<%# Bind("cri_alerta")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CATEGORIA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("nom_categoria")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ESTADO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" />
                                                                    </asp:TemplateField>


                                                                </Columns>
                                                                <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel box box-primary">
                                                <div class="box-header with-border">
                                                    <h4 class="box-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Ubicación Alerta
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse in">
                                                    <div class="box-body">


                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <div id="map" class="table-responsive" style="width: 100%; height: 300px;">
                                                                </div>

                                                                <script>                                               
                                
                                                                    var mymap = L.map('map').setView(<%=CargarMapa()%>, 10);                                      
                                                                                
                                                                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                                                                        maxZoom: 18,
                                                                        attribution: '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>, ' +
                                                                            '<a href="https://www.mapbox.com/">Mapbox</a>',
                                                                        id: 'mapbox/streets-v11',
                                                                        tileSize: 512,
                                                                        zoomOffset: -1
                                                                    }).addTo(mymap);

                                                                    L.marker(<%=CargarMapa()%>).addTo(mymap)
                                            .bindPopup("<%=Rescatarinformacion()%>").openPopup();
                                                                                                                               

                                                                </script>


                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="panel box box-primary">
                                                <div class="box-header with-border">
                                                    <h4 class="box-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Bitacora Estados Viaje
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="box-body">

                                                        <asp:GridView ID="grdEstados" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ESTADO">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FECHA">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fec_add")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="OBSERVACIONES">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOBS" runat="server" Text='<%# Bind("obs")%>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle CssClass="EstFilasGrilla" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel box box-primary">
                                                <div class="box-header with-border">
                                                    <h4 class="box-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseIndicadores">Indicadores
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseIndicadores" class="panel-collapse collapse">
                                                    <div class="box-body">
                                                        <h4>
                                                            <asp:Label ID="label6" runat="server" CssClass="text-primary" Font-Bold="true" Text="DURACION VIAJE: "></asp:Label>
                                                            <asp:Label ID="Label8" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                                                            /
               
                    <asp:Label ID="label10" runat="server" CssClass="text-primary" Font-Bold="true" Text="KM. RECORRIDOS: "></asp:Label>
                                                            <asp:Label ID="Label11" runat="server" CssClass="text-primary" Font-Bold="false" Text="0"></asp:Label>
                                                        </h4>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.col (right) -->
                        </div>
                    </div>
                </div>

                <asp:HiddenField ID="hdnContacto" runat="server" Value="0"></asp:HiddenField>

                <asp:HiddenField ID="hdnNivelObligatorio" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnNumNivel" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hdnIDNivel" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnCantidadNiveles" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnContactado" runat="server"></asp:HiddenField>

                <asp:HiddenField ID="hdnCodTrasnportista" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnCodCliente" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnIdDemanda" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
                <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />
                <asp:HiddenField ID="hdnCodConductor" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnNomconductor" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnfonoConductor" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnPatArrastre" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnCodUnidad" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnPatente" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnGPS" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnEstGPS" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnFecRegistroGPS" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnNumOS" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdServicio" runat="server" Value="0" />
                <asp:HiddenField ID="hdnGestion" runat="server" Value="0" />

                <asp:HiddenField ID="hdnNomRol" runat="server" Value="0" />

                <asp:HiddenField ID="hdnMail" runat="server" Value="0" />

                <asp:HiddenField ID="hdnEnvioCorreo" runat="server" Value="0" />

                <asp:HiddenField ID="hdnNomContacto" runat="server" Value="0" />
            </div>



        </div>


    </section>


</asp:Content>

