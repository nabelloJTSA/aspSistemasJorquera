﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspAsigRecursos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        '  Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Session("est_asignado_tc") = 0

                lblNumOSOferta.Text = Session("os_asig_tc")
                Call CargarGrillaMultipaso(Session("os_asig_tc"), Session("cod_cli_tc"), Session("id_dem_tc"))
                Call CargarGrupoOperativo()
                Call CargarConductores()
                Call CargarTransportistas()

                Call CargarGrillaOferta()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrupoOperativo()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CGO', '" & Session("cod_usu_tc") & "'"
            cboGrupoOperativo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboGrupoOperativo.DataTextField = "nom_grupo_operativo"
            cboGrupoOperativo.DataValueField = "cod_grupo_operativo"
            cboGrupoOperativo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrupoOperativo", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarConductores()
        Try
            Dim strNomTablaR As String = "TCMae_conductores"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CCH'"
            cboConductor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductor.DataTextField = "nom_conductor"
            cboConductor.DataValueField = "cod_msoft"
            cboConductor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarConductores", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_asignacion_viajes 'CTR'"
            cboTrasnportistas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasnportistas.DataTextField = "nom_transportista"
            cboTrasnportistas.DataValueField = "rut_transportista"
            cboTrasnportistas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaOferta()
        Try
            Dim strNomTabla As String = "ADAMMov_evidencia"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CCD2' ,'" & Session("id_ser_tc") & "','" & Session("cod_usu_tc") & "'"
            grdOferta.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdOferta.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaOferta", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaOfertaGrupoOperativo()
        Try
            Dim strNomTabla As String = "ADAMMov_evidencia"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GGO' ,'" & cboGrupoOperativo.SelectedValue & "','" & cboConductor.SelectedItem.ToString() & "','" & cboTrasnportistas.SelectedValue & "','" & Session("cod_usu_tc") & "'"
            grdOferta.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdOferta.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaOfertaGrupoOperativo", Err, Page, Master)
        End Try
    End Sub


    Private Sub GenerarCSV(ByVal cliente As String, ByVal os As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "FTP"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = cliente
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = os
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = Session("id_dem_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GenerarCSV", Err, Page, Master)
        End Try
    End Sub



    'Private Function GenerarCSV(ByVal cliente As String, ByVal os As String) As Boolean
    '    Dim strPathFile As String
    '    Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
    '    Dim strSQL As String = "", strNomArchivo As String = ""
    '    Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
    '    Dim cnxBDTC_QA As New SqlConnection(strCnx)
    '    cnxBDTC_QA.Open()

    '    strNomArchivo = "Demanda_CL_" & cliente & "_OS_" & os & ".csv"
    '    strPathFile = "C: \Demandas\" + Trim(strNomArchivo)
    '    If (String.IsNullOrEmpty(strPathFile) = False) Then

    '        Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
    '        If (toDownExiFile.Exists) Then
    '            Dim FileToDelete As String = strPathFile
    '            If File.Exists(FileToDelete) = True Then
    '                File.Delete(FileToDelete)
    '            End If
    '        End If
    '        File.AppendAllText(strPathFile, Trim(strNomArchivo))

    '        Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

    '        strSQL = "Exec pro_asignacion_viajes 'FTP', '" & cliente & "', '" & os & "'"

    '        cmdTemporal = New SqlCommand(strSQL, cnxBDTC_QA)
    '        ' cmdTemporal.CommandTimeout = 90000
    '        rdoReader = cmdTemporal.ExecuteReader()
    '        Do While rdoReader.Read
    '            strTextoDetalle1 = ""
    '            strTextoDetalle1 = Trim(rdoReader(0))
    '            z_varocioStreamWriter.WriteLine(strTextoDetalle1)
    '        Loop
    '        rdoReader.Close()
    '        rdoReader = Nothing
    '        cmdTemporal.Dispose()
    '        cmdTemporal = Nothing
    '        z_varocioStreamWriter.Close()
    '        blnError = True
    '    End If
    '    Return blnError
    'End Function

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function


    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>Cod. Unidad:</b> " & hdnCodUnidad.Value & " <br /><b>Camión:</b> " & hdnPatente.Value & "<br /><b>Arrastre:</b> " & hdnPatArrastre.Value & " <br /><b>Conductor:</b> " & hdnNomconductor.Value & "<br /><b>Fono</b> " & hdnfonoConductor.Value & "<br /><b>Ultimo Registro:</b> " & hdnFecRegistroGPS.Value & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    Protected Sub VerMapa(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdOferta.Rows(rowIndex)

        hdnLat.Value = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        hdnLon.Value = CType(row.FindControl("hdnLon"), HiddenField).Value
        hdnFecRegistroGPS.Value = CType(row.FindControl("hdnFecUltimoRegistro"), HiddenField).Value
        hdnCodUnidad.Value = CType(row.FindControl("lblCodCamion"), Label).Text

        hdnNomconductor.Value = CType(row.FindControl("lblNomConductor"), Label).Text
        hdnPatArrastre.Value = CType(row.FindControl("lblPatenteArrastre"), Label).Text
        hdnPatente.Value = CType(row.FindControl("lblPatenteCamion"), Label).Text
        hdnfonoConductor.Value = CType(row.FindControl("hdnfonoConductor"), HiddenField).Value
    End Sub


    Function CargarGrafico3() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        Dim strSCodU As String
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim cnxBDTC_QA As SqlConnection

        'columna de datos
        Datos.Columns.Add(New DataColumn("num_latitud", GetType(String)))
        Datos.Columns.Add(New DataColumn("num_longitud", GetType(String)))

        strSCodU = "Exec pro_asignacion_viajes 'CMP'"

        cnxBDTC_QA = New SqlConnection(strCnx)
        cnxBDTC_QA.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBDTC_QA)
        rdoReader = cmdTemporal.ExecuteReader()

        While rdoReader.Read()
            Datos.Rows.Add(New Object() {rdoReader(0), rdoReader(1)})
        End While

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing

        For Each dr As DataRow In Datos.Rows
            If strDatos = "" Then
                strDatos = dr(0) & "" & "," & dr(1) & ""
            Else
                strDatos = strDatos & "," & dr(0) & "" & "," & dr(1)
            End If
        Next
        strDatos = "[" & strDatos & "]"

        Return strDatos
    End Function


    Private Sub grdOferta_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdOferta.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 0 Or CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = "" Then
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If

                If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
                ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 0 Or CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = "" Then
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                End If


                If CType(row.FindControl("hdnEstDoctoCamion"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstDoctoArrastre"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstDcotoConductor"), HiddenField).Value = 1 Then
                    e.Row.ForeColor = System.Drawing.Color.Red
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdLugares_RowDataBound", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaMultipaso(ByVal os As String, ByVal cod_cliente As String, ByVal id_demanda As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CDM' , '" & os & "', '" & cod_cliente & "', '" & id_demanda & "'"
            grdMultipaso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdMultipaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaMultipaso", Err, Page, Master)
        End Try
    End Sub


    Protected Sub ActualizarHoraGrilla(ByVal hora_origen As String, ByVal hora_destino As String, ByVal id_demanda As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UHG"
            comando.Parameters.Add("@hor_servicio", SqlDbType.NVarChar)
            comando.Parameters("@hor_servicio").Value = hora_origen
            comando.Parameters.Add("@hor_servicio_destino", SqlDbType.NVarChar)
            comando.Parameters("@hor_servicio_destino").Value = hora_destino
            comando.Parameters.Add("@id_demanda", SqlDbType.NVarChar)
            comando.Parameters("@id_demanda").Value = id_demanda
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("ActualizarHoraGrilla", Err, Page, Master)
        End Try
    End Sub


    Protected Sub AsignarRecursosOferta(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdOferta.Rows(rowIndex)

        hdnCodConductor.Value = CType(row.FindControl("hdnCodConductor"), HiddenField).Value
        hdnPatente.Value = CType(row.FindControl("lblPatenteCamion"), Label).Text
        hdnPatArrastre.Value = CType(row.FindControl("lblPatenteArrastre"), Label).Text

        lblCodUnidadOferta.Text = CType(row.FindControl("lblCodCamion"), Label).Text
        lblCamionOferta.Text = CType(row.FindControl("lblPatenteCamion"), Label).Text
        lblArrastreOferta.Text = CType(row.FindControl("lblPatenteArrastre"), Label).Text
        lblConductorOferta.Text = CType(row.FindControl("lblNomConductor"), Label).Text
        lblFonoOferta.Text = CType(row.FindControl("hdnFonoConductor"), HiddenField).Value
        lblOSconfirmar.Text = lblNumOSOferta.Text

        For iRow = 0 To grdMultipaso.Rows.Count - 1
            If CType(grdMultipaso.Rows(iRow).FindControl("txtHoraOrigen"), TextBox).Text <> "" And CType(grdMultipaso.Rows(iRow).FindControl("txtHoraDestino"), TextBox).Text <> "" Then
                Call ActualizarHoraGrilla(CType(grdMultipaso.Rows(iRow).FindControl("txtHoraOrigen"), TextBox).Text, CType(grdMultipaso.Rows(iRow).FindControl("txtHoraDestino"), TextBox).Text, CType(grdMultipaso.Rows(iRow).FindControl("hdnIdDemanda"), HiddenField).Value)
            Else
                MessageBox("Asignar", "debe configurar las horas correspondientes", Page, Master, "W")
                Exit Sub
            End If
        Next

        Call CargarGrillaDocumentosCamion(1, Trim(hdnPatente.Value))
        Call CargarGrillaDocumentosArrastre(1, Trim(hdnPatArrastre.Value))
        Call CargarGrillaDocumentosConductor(2, Trim(CType(row.FindControl("hdnRutConductor"), HiddenField).Value))

        If CType(row.FindControl("hdnEstDoctoCamion"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstDoctoArrastre"), HiddenField).Value = 1 Or CType(row.FindControl("hdnEstDcotoConductor"), HiddenField).Value = 1 Then
            btnConfirmar.Visible = False
        Else
            btnConfirmar.Visible = True
        End If

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub


    Private Sub CargarGrillaDocumentosCamion(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_programacion 'GED', '" & tipo & "', '" & id & "'"
            grdDoctoCamion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDoctoCamion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentosCamion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDoctoCamion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDoctoCamion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text
                If CDate(objFechaVenc) <= CDate(Date.Now.AddDays(5)) Then
                    e.Row.BackColor = System.Drawing.Color.LightSteelBlue
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdDoctoCamion_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaDocumentosArrastre(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_programacion 'GED', '" & tipo & "', '" & id & "'"
            grdDoctoArrastre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDoctoArrastre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentosArrastre", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDoctoArrastre_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDoctoArrastre.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text
                If CDate(objFechaVenc) <= CDate(Date.Now.AddDays(5)) Then
                    e.Row.BackColor = System.Drawing.Color.LightSteelBlue
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdDoctoArrastre_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaDocumentosConductor(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_programacion 'GED', '" & tipo & "', '" & id & "'"
            grdDoctoChofer.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDoctoChofer.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentosConductor", Err, Page, Master)
        End Try
    End Sub


    Protected Sub grdDoctoChofer_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDoctoChofer.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text
                If CDate(objFechaVenc) <= CDate(Date.Now.AddDays(5)) Then
                    e.Row.BackColor = System.Drawing.Color.LightSteelBlue
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdDoctoChofer_RowDataBound", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnConfirmarAsignacion_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Call AsignarModal()

        Session("pat_camion_asig") = hdnPatente.Value
        Session("pat_arr_asig") = hdnPatArrastre.Value
        Session("conductor_asig") = lblConductorOferta.Text & "/" & lblFonoOferta.Text
        Session("est_asignado_tc") = 1

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspAsignacion.aspx")
    End Sub


    Protected Sub AsignarModal()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UDV"
            comando.Parameters.Add("@cod_conductor", SqlDbType.NVarChar)
            comando.Parameters("@cod_conductor").Value = hdnCodConductor.Value
            comando.Parameters.Add("@pat_camion", SqlDbType.NVarChar)
            comando.Parameters("@pat_camion").Value = Trim(hdnPatente.Value)
            comando.Parameters.Add("@pat_arrastre", SqlDbType.NVarChar)
            comando.Parameters("@pat_arrastre").Value = Trim(hdnPatArrastre.Value)
            comando.Parameters.Add("@cod_servicio", SqlDbType.NVarChar)
            comando.Parameters("@cod_servicio").Value = Session("id_ser_tc")
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Val(lblNumOSOferta.Text)
            comando.Parameters.Add("@id_turno", SqlDbType.Int)
            comando.Parameters("@id_turno").Value = 1
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("cod_cli_tc")
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call GenerarCSV(Session("cod_cli_tc"), lblNumOSOferta.Text)


        Catch ex As Exception
            MessageBoxError("AsignarModal", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboGrupoOperativo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        Call CargarGrillaOfertaGrupoOperativo()

    End Sub

    Protected Sub cboConductor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarGrillaOfertaGrupoOperativo()

    End Sub

    Protected Sub cboTrasnportistas_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarGrillaOfertaGrupoOperativo()

    End Sub

    Protected Sub grdOferta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdOferta.SelectedIndexChanged

    End Sub
End Class