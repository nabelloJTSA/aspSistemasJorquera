﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspRETurnosPla.aspx.vb" Inherits="aspSistemasJorquera.aspRETurnosPla" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <Triggers>
            <asp:PostBackTrigger ControlID="btnGuardar" />

        </Triggers>
        <ContentTemplate>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server"></asp:Label>
                </h3>
                <style>
                    .bg-azul {
                        background-color: #367FA9;
                        color: white;
                    }
                </style>
            </section>

            <script lang="javascript" type="text/javascript">
                function CallPrint(strid) {
                    var prtContent = document.getElementById(strid);
                    var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
                    WinPrint.document.write(prtContent.innerHTML);
                    WinPrint.document.close();
                    WinPrint.focus();
                    WinPrint.print();
                }
            </script>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="form-inline" style="text-align: right">
                             <asp:LinkButton ID="linCargar" runat="server" Text="Actualizar Reporte" CssClass="btn btn-primary"></asp:LinkButton>
                            <asp:LinkButton ID="linDescargar" runat="server" CssClass="btn botonesTC-descargar" OnClientClick="javascript:CallPrint('reporte1');">Descargar Reporte</asp:LinkButton>
                            <asp:LinkButton ID="linVUT" runat="server" CssClass="btn btn-primary" OnClick="lnkComentario_Click">Comentarios de Turno</asp:LinkButton>
                            <asp:LinkButton ID="linVConductor" runat="server" CssClass="btn btn-primary" OnClick="lnkGestion_Click">Gestión de Alertas</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row" id="reporte">
                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdPerformance" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>REPORTE DE TURNO</td></tr></table>'
                            CaptionAlign="Top">
                            <Columns>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPERFFamilias" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>
                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdDisponible" runat="server" CssClass="table table-bordered table-hover " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered"><tr><td>DISPONIBILIDAD EQ</td></tr></table>'
                            CaptionAlign="Top">
                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDispTotal" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDispDisp" runat="server" Text='<%# Bind("DISPONIBLE")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDispPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                            
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>

                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdCumplimiento" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>CUMPLIMIENTO</td></tr></table>'
                            CaptionAlign="Top">
                            <Columns>
                                <asp:TemplateField HeaderText="ASIG" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCumpViajes" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PROG" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCumpDemanda" runat="server" Text='<%# Bind("DEMANDA")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCumpPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>

                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdDocs" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>DOCUMENTOS</td></tr></table>'
                            CaptionAlign="Top">
                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDOCSTotal" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VENCIDOS" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDOCSVencidos" runat="server" Text='<%# Bind("VENC")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDOCSPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                             
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>

                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdSinGPS" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SIN GPS</td></tr></table>'
                            CaptionAlign="Top">

                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSGPSViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSGPSDemanda" runat="server" Text='<%# Bind("NOCONECTADO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSGPSPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>
                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdSinConductor" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SIN COND</td></tr></table>'
                            CaptionAlign="Top">

                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCONViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SIN COND" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCONSincon" runat="server" Text='<%# Bind("SINCON")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCONPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>
                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdSiniestrados" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA SINIESTRADA</td></tr></table>'
                            CaptionAlign="Top">

                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSINIViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SINIESTRADO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSINISincon" runat="server" Text='<%# Bind("SINIESTRADOS")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSINIPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>
                    <div class="col-md-2" style="width: 12.5%">
                        <asp:GridView ID="grdTaller" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE"
                            Caption='<table width="100%" style="margin-bottom:-10px;text-align:center;font-weight:bold;background:#9BC2E6; color:white" class="table table-bordered "><tr><td>FLOTA TALLER</td></tr></table>'
                            CaptionAlign="Top">

                            <Columns>
                                <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTALLViajes" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TALLER" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTALLSincon" runat="server" Text='<%# Bind("TALLER")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTALLPorcentaje" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                    <ItemStyle />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                        </asp:GridView>
                    </div>
                </div>

                <div class="modal fade" id="modalTurnos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 40%">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" onclick="$('#modalTurnos').modal('hide');" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">
                                    <asp:Label ID="Label2" runat="server" Text="Comentarios de Turno"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-bottom: 5%">
                                    <div class="col-md-12" style="background-color: white">
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <asp:DropDownList ID="cboCategoria" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Comentario"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="grdComentarios" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Turno" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTurno" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Categoría" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoria") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("usuario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comentario" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("comentario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hora" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn bg-green" Text="Agregar comentario" OnClick="btnGuardar_Click" />
                            </div>
                        </div>

                    </div>
                </div>
            </section>





            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="justify" Style="display: none; vertical-align: top" CssClass="EstPanel" Visible="True" Height="2000px" Width="700px">
                <div id="reporte1" class="btn-sm">
                    <table id="Table2" style="vertical-align: top;">

                        <h2>
                            <p><b>REPORTE TURNO PLANIFICADOR</b></p>
                        </h2>

                        <tr>
                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">PERFORMANCE</label>
                                <asp:GridView ID="grdPerformance1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                    <Columns>
                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPERFFamilias" Font-Size="X-Small" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>
                            </td>

                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">DISPONIBLES</label>

                                <asp:GridView ID="grdDisponible1" runat="server" CssClass="table table-bordered table-hover " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDispTotal" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DISPPP" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDispDisp" Font-Size="Smaller" runat="server" Text='<%# Bind("DISPONIBLE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDispPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                            
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>

                            </td>

                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">CUMPLIMIENTO</label>
                                <asp:GridView ID="grdCumplimiento1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ASIG" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCumpViajes" Font-Size="Smaller" runat="server" Text='<%# Bind("VIAJES")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PROG" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCumpDemanda" Font-Size="Smaller" runat="server" Text='<%# Bind("DEMANDA")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCumpPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>
                            </td>


                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">DOCUMENTOS</label>
                                <asp:GridView ID="grdDocs1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDOCSTotal" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VENCIDOS" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDOCSVencidos" Font-Size="Smaller" runat="server" Text='<%# Bind("VENC")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDOCSPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                             
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>
                            </td>


                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">SIN GPS</label>
                                <asp:GridView ID="grdSinGPS1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">

                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSGPSViajes" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DISP" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSGPSDemanda" Font-Size="Smaller" runat="server" Text='<%# Bind("NOCONECTADO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSGPSPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>

                            </td>



                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">SIN COND</label>
                                <asp:GridView ID="grdSinConductor1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">

                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSCONViajes" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SIN COND" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSCONSincon" Font-Size="Smaller" runat="server" Text='<%# Bind("SINCON")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSCONPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>

                            </td>


                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">SINIESTRADO</label>

                                <asp:GridView ID="grdSiniestrados1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">

                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSINIViajes" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SINIESTRADO" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSINISincon" Font-Size="Smaller" runat="server" Text='<%# Bind("SINIESTRADOS")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSINIPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>
                            </td>


                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">TALLER</label>

                                <asp:GridView ID="grdTaller1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">

                                    <Columns>
                                        <asp:TemplateField HeaderText="TOTAL" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTALLViajes" Font-Size="Smaller" runat="server" Text='<%# Bind("TOTAL")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TALLER" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTALLSincon" Font-Size="Smaller" runat="server" Text='<%# Bind("TALLER")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTALLPorcentaje" Font-Size="Smaller" runat="server" Text='<%# Bind("PORCENTAJE")%>'></asp:Label>%                                              
                                            </ItemTemplate>
                                            <ItemStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="X-Small" />
                                </asp:GridView>
                            </td>

                        </tr>



                    </table>

                    <table id="Table22" style="vertical-align: top;">
                        <tr>
                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">COMENTARIOS</label>

                                <asp:GridView ID="grdComentarios1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">

                                    <Columns>
                                        <asp:TemplateField HeaderText="Turno" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTurno" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Categoría" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoria") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("usuario") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comentario" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("comentario") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Hora" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Small" />

                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>




        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>


