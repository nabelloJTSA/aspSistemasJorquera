﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspRETurnosPla
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Turno Planificador"

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarDisponible()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DISP','" & Session("cod_usu_tc") & "'"
            grdDisponible.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDisponible.DataBind()
            grdDisponible1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDisponible1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDisponible", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCumplimiento()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'CUMP','" & Session("cod_usu_tc") & "'"
            grdCumplimiento.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCumplimiento.DataBind()
            grdCumplimiento1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCumplimiento1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCumplimiento", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSGPS()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SGPS','" & Session("cod_usu_tc") & "'"
            grdSinGPS.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinGPS.DataBind()
            grdSinGPS1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinGPS1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSGPS", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSCON()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SCON','" & Session("cod_usu_tc") & "'"
            grdSinConductor.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinConductor.DataBind()
            grdSinConductor1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinConductor1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSCON", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSINI()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SINI','" & Session("cod_usu_tc") & "'"
            grdSiniestrados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSiniestrados.DataBind()
            grdSiniestrados1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSiniestrados1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSINI", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTaller()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'TALL','" & Session("cod_usu_tc") & "'"
            grdTaller.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTaller.DataBind()
            grdTaller1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTaller1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTaller", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarVencidos()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DOCS','" & Session("cod_usu_tc") & "'"
            grdDocs.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocs.DataBind()
            grdDocs1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocs1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarVencidos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarFamilias()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'FAMI','" & Session("cod_usu_tc") & "'"
            grdPerformance.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance.DataBind()
            grdPerformance1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Protected Sub lnkComentario_Click(sender As Object, e As EventArgs)
        Call CargarTabla()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalTurnos", "$('#modalTurnos').modal();", True)
    End Sub
    Private Sub CargarTabla()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_comentarios 'G','2'"
            grdComentarios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdComentarios.DataBind()

            grdComentarios1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdComentarios1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTabla", Err, Page, Master)
        End Try
    End Sub
    Private Sub InsComentario()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "rpt_comentarios"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"

            comando.Parameters.Add("@categoria", SqlDbType.NVarChar)
            comando.Parameters("@categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@usuario", SqlDbType.Int)
            comando.Parameters("@usuario").Value = Session("cod_usu_tc")

            comando.Parameters.Add("@comentario", SqlDbType.NVarChar)
            comando.Parameters("@comentario").Value = txtComentario.Text

            comando.Parameters.Add("@tipoCo", SqlDbType.Int)
            comando.Parameters("@tipoCo").Value = 2

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsComentario", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs)
        Call InsComentario()
        Call CargarTabla()
        txtComentario.Text = ""
        MessageBox("Guardar", "Comentario guardado", Page, Master, "S")
    End Sub

    Protected Sub lnkGestion_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx")
    End Sub
    Private Sub CargarCategoria()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_comentarios 'CBOCAT'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "des_tipo"
            cboCategoria.DataValueField = "id_tipo_incidencia"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategoria", Err, Page, Master)
        End Try
    End Sub

    Protected Sub linCargar_Click(sender As Object, e As EventArgs) Handles linCargar.Click
        Call CargarFamilias()
        Call CargarDisponible()
        Call CargarCumplimiento()
        Call CargarSGPS()
        Call CargarSCON()
        Call CargarSINI()
        Call CargarTaller()
        Call CargarVencidos()
        Call CargarCategoria()
        Call CargarTabla()
    End Sub
End Class