﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class aspREIncidenciasFicha
    
    '''<summary>
    '''Control UpdatePanel1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Control lbltitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltitulo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control btnASignar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnASignar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control txtNumViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNumViaje As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtTransportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTransportista As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtEtaOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtEtaOrigen As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtOrigen As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtDestino.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDestino As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtCamion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtCamion As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtArrastre As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtConductor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtRut.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRut As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtScore.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtScore As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control grdGestion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdGestion As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnIdViaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdViaje As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFechaServicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFechaServicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnhoraservicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnhoraservicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLatOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLatOrigen As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLonOrigen.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLonOrigen As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodTrasnportista.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodTrasnportista As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodCliente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodCliente As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdDemanda.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdDemanda As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLat.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLat As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnLon.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnLon As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutconductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutconductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodConductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNomconductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNomconductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnfonoConductor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnfonoConductor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPatArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPatArrastre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodUnidad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodUnidad As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPatente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPatente As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnArrastre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnArrastre As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnGPS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnGPS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEstGPS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEstGPS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFecAlerta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFecAlerta As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumOS.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumOS As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdServicio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdServicio As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnAlertaTipo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnAlertaTipo As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnAlertaCrit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnAlertaCrit As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnAlertaEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnAlertaEstado As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control updProgress2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents updProgress2 As Global.System.Web.UI.UpdateProgress
End Class
