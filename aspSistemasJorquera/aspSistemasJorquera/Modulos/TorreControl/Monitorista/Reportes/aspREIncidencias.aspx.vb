﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREIncidencias
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "EVENTOS - INCIDENCIAS"
                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")

                Call CargarTransportistas()
                Call CargarTipos()
                Call CargarFamilias()
                Call CargarServicios()
                Call CargarClientes()
                Call CargarIncidencias()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarIncidencias()
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec rpt_incidencias 'RE-CI', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboTipo.SelectedValue & "','" & cboTrasportista.SelectedValue & "','" & cboFamilia.SelectedValue & "','" & cboServicio.SelectedValue & "','" & cboCliente.SelectedValue & "'"
            grdIncidencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIncidencias.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Incidencias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_incidencias 'CCF'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "nom_familia"
            cboFamilia.DataValueField = "cod_familia"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_incidencias 'CCS'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_incidencias 'CCC'"
            cboCliente.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCliente.DataTextField = "nom_cliente1"
            cboCliente.DataValueField = "cod_msoft"
            cboCliente.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTipos()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_incidencias 'CCI'"
            cboTipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTipo.DataTextField = "nom_tipo"
            cboTipo.DataValueField = "id_tipo"
            cboTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Tipos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_transportistas"
            Dim strSQLR As String = "Exec rpt_incidencias 'CCT'"
            cboTrasportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasportista.DataTextField = "nom_transportista"
            cboTrasportista.DataValueField = "id_transportista"
            cboTrasportista.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Transportistas", Err, Page, Master)
        End Try
    End Sub

    Protected Sub linDescargar_Click(sender As Object, e As EventArgs)
        Call ExportarTabla()
    End Sub

    'FILTROS DE TABLA
    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub
    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub
    Protected Sub cboTipo_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub
    Protected Sub cboTrasportista_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub

    'DESCARGAR EXCEL
    Private Sub ExportarTabla()
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec rpt_incidencias 'EX-CI', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboTipo.SelectedValue & "','" & cboTrasportista.SelectedValue & "','" & cboFamilia.SelectedValue & "','" & cboServicio.SelectedValue & "','" & cboCliente.SelectedValue & "'"
            Dim strNomTabla As String = "TCMov_incidencias"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"

            Response.AddHeader("Content-Disposition", "attachment;filename=Incidencias.xls")

            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("Exportar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnNumViaje_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdIncidencias.Rows(rowIndex)
        Session("num_viaje") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("id_incidencia") = CType(row.FindControl("hdnIDIncidencia"), HiddenField).Value
        Session("transportista") = CType(row.FindControl("lblTransportista"), Label).Text
        Session("origen_tc") = CType(row.FindControl("hdnOrigen"), HiddenField).Value
        Session("destino_tc") = CType(row.FindControl("hdnDestino"), HiddenField).Value
        Session("camion_tc") = CType(row.FindControl("hdnCamion"), HiddenField).Value
        Session("arrastre_tc") = CType(row.FindControl("hdnArrastre"), HiddenField).Value
        Session("conductor_tc") = CType(row.FindControl("lblConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("lblRut"), Label).Text
        Session("score") = "Score"
        Session("latitud") = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        Session("longitud") = CType(row.FindControl("hdnLongitud"), HiddenField).Value
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREIncidenciasFicha.aspx")
    End Sub

    Protected Sub cboFamilia_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub

    Protected Sub cboServicio_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub

    Protected Sub cboCliente_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarIncidencias()
    End Sub
End Class