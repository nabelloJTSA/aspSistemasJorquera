﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspRETurnosDetalle
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Dim fecha As String = Session("fec_actual")
                lbltitulo.Text = "Turno Detalle: " & fecha
                Call CargarFamilias()
                Call CargarServicios()
                Call CargarViaje()
                Call CargarTransportista()
                Call CargarTracto()
                Call CargarConductor()
                Call CargarCategoria()

                Call CargarDetalle()
                Call CargarSyS()
                Call CargarPerformance()
                Call CargarCumplimiento()
                Call CargarTabla()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarDetalle()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DETALLE','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdDetalle.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDetalle.DataBind()
            grdDetalle1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDetalle1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDetalle", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSyS()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DETALLESYS','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdSafety.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSafety.DataBind()
            grdSafety1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSafety1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSyS", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPerformance()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DETPERF','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdPerformance.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance.DataBind()
            grdPerformance1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPerformance", Err, Page, Master)
        End Try
    End Sub

    'CARGAR DROPDOWNS
    Private Sub CargarFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboFAM','" & Session("cod_usu_tc") & "'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "FAMILIA"
            cboFamilia.DataValueField = "CODIGO"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboSER','" & Session("cod_usu_tc") & "'"
            cboServicios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicios.DataTextField = "SERVICIO"
            cboServicios.DataValueField = "CODIGO"
            cboServicios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarViaje()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboVIA','" & Session("cod_usu_tc") & "'"
            cboViaje.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboViaje.DataTextField = "VIAJE"
            cboViaje.DataValueField = "VIAJE"
            cboViaje.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarViaje", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTransportista()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboTRA','" & Session("cod_usu_tc") & "'"
            cboTransportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTransportista.DataTextField = "TRANSPORTISTA"
            cboTransportista.DataValueField = "CODIGO"
            cboTransportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportista", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTracto()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboCAM','" & Session("cod_usu_tc") & "'"
            cboTracto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTracto.DataTextField = "TRACTO"
            cboTracto.DataValueField = "TRACTO"
            cboTracto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTracto", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarConductor()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboCON','" & Session("cod_usu_tc") & "'"
            cboConductor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductor.DataTextField = "CONDUCTOR"
            cboConductor.DataValueField = "CODIGO"
            cboConductor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarConductor", Err, Page, Master)
        End Try
    End Sub
    'ACTUALIZAR TABLAS DE DETALLE
    Protected Sub cboFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFamilia.SelectedIndexChanged
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub cboServicios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicios.SelectedIndexChanged
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub cboViaje_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboViaje.SelectedIndexChanged
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub cboTransportista_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub cboTracto_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub cboConductor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarDetalle()
        Call CargarSyS()
        Call CargarPerformance()
        Call CargarCumplimiento()
    End Sub
    Protected Sub lnkComentario_Click(sender As Object, e As EventArgs)
        Call CargarTabla()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalTurnos", "$('#modalTurnos').modal();", True)
    End Sub
    Private Sub CargarTabla()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_comentarios 'G','1'"
            grdComentarios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdComentarios.DataBind()

            grdComentarios1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdComentarios1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTabla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCumplimiento()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'ASI-DEM','" & Session("cod_usu_tc") & "', '" & cboFamilia.SelectedValue & "', '" & cboServicios.SelectedValue & "', '" & cboTransportista.SelectedValue & "', '" & cboViaje.SelectedValue & "', '" & cboTracto.SelectedValue & "' , '" & cboConductor.SelectedValue & "'"
            grdCumplimiento.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCumplimiento.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCumplimiento", Err, Page, Master)
        End Try
    End Sub
    Private Sub InsComentario()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "rpt_comentarios"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"

            comando.Parameters.Add("@categoria", SqlDbType.NVarChar)
            comando.Parameters("@categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@usuario", SqlDbType.Int)
            comando.Parameters("@usuario").Value = Session("cod_usu_tc")

            comando.Parameters.Add("@comentario", SqlDbType.NVarChar)
            comando.Parameters("@comentario").Value = txtComentario.Text

            comando.Parameters.Add("@tipoCo", SqlDbType.Int)
            comando.Parameters("@tipoCo").Value = 1

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsComentario", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs)
        Call InsComentario()
        Call CargarTabla()
        txtComentario.Text = ""
        MessageBox("Guardar", "Comentario guardado", Page, Master, "S")
    End Sub
    Protected Sub lnkGestion_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx")
    End Sub
    Private Sub CargarCategoria()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_comentarios 'CBOCAT'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "des_tipo"
            cboCategoria.DataValueField = "id_tipo_incidencia"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategoria", Err, Page, Master)
        End Try
    End Sub
End Class