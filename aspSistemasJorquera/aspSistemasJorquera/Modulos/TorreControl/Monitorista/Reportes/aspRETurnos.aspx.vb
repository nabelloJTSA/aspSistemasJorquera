﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspRETurnos
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then

                'CBO
                Call CargarCBOFamilias()
                Call CargarCBOServicios()
                Call CargarViaje()
                Call CargarCBOTransportista()
                Call CargarTracto()
                Call CargarConductor()
                Call CargarCategoria()


                'Call CargarFamilias()
                'Call CargarDisponible()
                'Call CargarCumplimiento()
                'Call CargarOTIF()
                'Call CargarVueltas()
                'Call CargarSobrestadia()
                'Call CargarPOD()
                'Call CargarReportabilidad()
                'Call CargarAlertas()
                'Call CargarAtendidas()
                'Call CargarTiempo()
                'Call CargarExceso()
                'Call CargarDetenciones()
                'Call CargarSGPS()
                'Call CargarSCON()
                'Call CargarSINI()
                'Call CargarTaller()
                'Call CargarVencidos()
                'Call CargarCategoria()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarDisponible()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DISP','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdDisponible.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDisponible.DataBind()
            grdDisponible1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDisponible1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDisponible", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCumplimiento()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'CUMP','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdCumplimiento.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCumplimiento.DataBind()
            grdCumplimiento1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdCumplimiento1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCumplimiento", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarOTIF()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'OTIF','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdOTIF.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdOTIF.DataBind()
            grdOTIF1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdOTIF1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarOTIF", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarVueltas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'VUEL','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdVueltas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVueltas.DataBind()
            grdVueltas1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVueltas1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarVueltas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSobrestadia()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SOBR','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdSobrestadia.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSobrestadia.DataBind()
            grdSobrestadia1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSobrestadia1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSobrestadia", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPOD()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'POD','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdPOD.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPOD.DataBind()
            grdPOD1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPOD1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPOD", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarReportabilidad()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'REPO','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdReportabilidad.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdReportabilidad.DataBind()
            grdReportabilidad1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdReportabilidad1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarReportabilidad", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'ALER','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
            grdAlertas1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAlertas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAtendidas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'ATEN','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdAtendidas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAtendidas.DataBind()
            grdAtendidas1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAtendidas1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAtendidas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTiempo()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'TIEM','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdTiempo.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTiempo.DataBind()
            grdTiempo1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTiempo1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTiempo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarExceso()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'EXCE','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdVelocidad.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVelocidad.DataBind()
            grdVelocidad1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVelocidad1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarExceso", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarDetenciones()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DETE','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdDetenciones.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDetenciones.DataBind()
            grdDetenciones1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDetenciones1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDetenciones", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSGPS()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SGPS','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdSinGPS.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinGPS.DataBind()
            grdSinGPS1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinGPS1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSGPS", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSCON()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SCON','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdSinConductor.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinConductor.DataBind()
            grdSinConductor1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSinConductor1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSCON", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarSINI()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'SINI','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdSiniestrados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSiniestrados.DataBind()
            grdSiniestrados1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSiniestrados1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSINI", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTaller()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'TALL','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdTaller.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTaller.DataBind()
            grdTaller1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdTaller1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTaller", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarVencidos()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'DOCS','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "','" & cboServicios.SelectedValue & "','" & cboTransportista.SelectedValue & "','" & cboViaje.SelectedValue & "','" & cboTracto.SelectedValue & "','" & cboConductor.SelectedValue & "'"
            grdVencidos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVencidos.DataBind()
            grdVencidos1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdVencidos1.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarVencidos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarFamilias()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_kpi_turnos 'FAMI','" & Session("cod_usu_tc") & "','" & cboFamilia.SelectedValue & "'"
            grdPerformance.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSafety.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdRecurso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdSafety1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdRecursos1.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPerformance.DataBind()
            grdSafety.DataBind()
            grdRecurso.DataBind()
            grdPerformance1.DataBind()
            grdSafety1.DataBind()
            grdRecursos1.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCBOFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboFAM','" & Session("cod_usu_tc") & "'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "FAMILIA"
            cboFamilia.DataValueField = "CODIGO"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCBOServicios()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboSER','" & Session("cod_usu_tc") & "'"
            cboServicios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicios.DataTextField = "SERVICIO"
            cboServicios.DataValueField = "CODIGO"
            cboServicios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCBOTransportista()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboTRA','" & Session("cod_usu_tc") & "'"

            cboTransportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTransportista.DataTextField = "TRANSPORTISTA"
            cboTransportista.DataValueField = "CODIGO"
            cboTransportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportista", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarViaje()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboVIA','" & Session("cod_usu_tc") & "'"
            cboViaje.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboViaje.DataTextField = "VIAJE"
            cboViaje.DataValueField = "VIAJE"
            cboViaje.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarViaje", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTracto()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboCAM','" & Session("cod_usu_tc") & "'"
            cboTracto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTracto.DataTextField = "TRACTO"
            cboTracto.DataValueField = "TRACTO"
            cboTracto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTracto", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarConductor()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_kpi_turnos 'cboCON','" & Session("cod_usu_tc") & "'"
            cboConductor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboConductor.DataTextField = "CONDUCTOR"
            cboConductor.DataValueField = "CODIGO"
            cboConductor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarConductor", Err, Page, Master)
        End Try
    End Sub

    Protected Sub lnkComentario_Click(sender As Object, e As EventArgs)
        Call CargarTabla()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "modalTurnos", "$('#modalTurnos').modal();", True)
    End Sub
    Private Sub CargarTabla()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_comentarios 'G','1'"
            grdComentarios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdComentarios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTabla", Err, Page, Master)
        End Try
    End Sub
    Private Sub InsComentario()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "rpt_comentarios"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"

            comando.Parameters.Add("@categoria", SqlDbType.Int)
            comando.Parameters("@categoria").Value = cboCategoria.SelectedValue

            comando.Parameters.Add("@usuario", SqlDbType.Int)
            comando.Parameters("@usuario").Value = Session("cod_usu_tc")

            comando.Parameters.Add("@comentario", SqlDbType.NVarChar)
            comando.Parameters("@comentario").Value = txtComentario.Text

            comando.Parameters.Add("@tipoCo", SqlDbType.Int)
            comando.Parameters("@tipoCo").Value = 1

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("InsComentario", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs)
        Call InsComentario()
        Call CargarTabla()
        txtComentario.Text = ""
        MessageBox("Guardar", "Comentario guardado", Page, Master, "S")
    End Sub
    Protected Sub lnkGestion_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspGestionViajes.aspx")
    End Sub
    Private Sub CargarCategoria()
        Try
            Dim strNomTablaR As String = "TCMae_tipo_incidencia"
            Dim strSQLR As String = "Exec rpt_comentarios 'CBOCAT'"
            cboCategoria.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCategoria.DataTextField = "des_tipo"
            cboCategoria.DataValueField = "id_tipo_incidencia"
            cboCategoria.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCategoria", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarReporte()
        Call CargarFamilias()
        Call CargarDisponible()
        Call CargarCumplimiento()
        Call CargarOTIF()
        Call CargarVueltas()
        Call CargarSobrestadia()
        Call CargarPOD()
        Call CargarReportabilidad()
        Call CargarAlertas()
        Call CargarAtendidas()
        Call CargarTiempo()
        Call CargarExceso()
        Call CargarDetenciones()
        Call CargarSGPS()
        Call CargarSCON()
        Call CargarSINI()
        Call CargarTaller()
        Call CargarVencidos()
    End Sub

    'Protected Sub cboFamilia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFamilia.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    'Protected Sub cboServicios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboServicios.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    'Protected Sub cboTransportista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTransportista.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    'Protected Sub cboViaje_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboViaje.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    'Protected Sub cboTracto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTracto.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    'Protected Sub cboConductor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConductor.SelectedIndexChanged
    '    Call CargarReporte()
    'End Sub

    Protected Sub linCargar_Click(sender As Object, e As EventArgs) Handles linCargar.Click
        Call CargarReporte()
    End Sub

End Class