﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREAlertasFicha
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '  Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                txtNumViaje.Text = Session("num_viaje")
                txtTransportista.Text = Session("transportista")
                txtOrigen.Text = Session("origen_tc")
                txtDestino.Text = Session("destino_tc")
                txtCamion.Text = Session("camion_tc")
                txtArrastre.Text = Session("arrastre_tc")
                txtConductor.Text = Session("conductor_tc")
                txtRut.Text = Session("rut_conductor_tc")
                txtScore.Text = Session("score")
                hdnLat.Value = Session("lat_ficha_tc")
                hdnLon.Value = Session("lon_ficha_tc")

                Call CargarDetalle()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    'CARGA DE DATOS MAPA
    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function
    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>" & hdnAlertaTipo.Value & "</br><b>Alerta: </b> " & Session("map_alerta") & "</br><b>Categoría: </b> " & Session("map_categoria") & "</br> <b>Fecha Alerta: </b> " & Session("map_fecha") & "<br /><b>Fecha Registro: </b> " & Session("map_fecharegistro") & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    'CARGAR TABLA DE DETALLE
    Private Sub CargarDetalle()
        Try
            Dim strNomTabla As String = "TCDet_gestion_alertas"
            Dim strSQL As String = "Exec rpt_alertas 'CFG', '" & Session("id_call_center") & "'"
            grdGestion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdGestion.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Alertas", Err, Page, Master)
        End Try
    End Sub
End Class