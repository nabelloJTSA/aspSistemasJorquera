﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREPosiciones
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "REPORTE DE POSICIONES"
                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")

                Call CargarTransportistas()
                Call CargarEstadoGPS()
                Call CargarPosiciones()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    'CARGAR CBO
    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec rpt_posiciones 'CCT'"
            cboTransportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTransportista.DataTextField = "nom_transportista"
            cboTransportista.DataValueField = "cod_msoft"
            cboTransportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarEstadoGPS()
        Try
            Dim strNomTablaR As String = "WFMae_posiciones"
            Dim strSQLR As String = "Exec rpt_posiciones 'CCG'"
            cboEstadoGPS.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstadoGPS.DataTextField = "nom_estado"
            cboEstadoGPS.DataValueField = "nom_estado"
            cboEstadoGPS.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstadoGPS", Err, Page, Master)
        End Try
    End Sub

    'CARGAR GRILLA
    Private Sub CargarPosiciones()
        Try
            Dim strNomTabla As String = "TCMov_ultima_posicion"
            Dim strSQL As String = "Exec rpt_posiciones 'CGP', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboEstadoGPS.SelectedValue & "','" & cboTransportista.SelectedValue & "'"
            grdPosiciones.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPosiciones.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Posiciones", Err, Page, Master)
        End Try
    End Sub
    Protected Sub txtDesde_TextChanged(sender As Object, e As EventArgs)
        Call CargarPosiciones()
    End Sub
    Protected Sub txtHasta_TextChanged(sender As Object, e As EventArgs)
        Call CargarPosiciones()
    End Sub
    Protected Sub cboTransportista_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarPosiciones()
    End Sub
    Protected Sub cboEstadoGPS_SelectedIndexChanged(sender As Object, e As EventArgs)
        Call CargarPosiciones()
    End Sub

    'DESCARGAR TABLA
    Protected Sub linDescargar_Click(sender As Object, e As EventArgs)
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strNomTabla As String = "TCMov_ultima_posicion"
            Dim strSQL As String = "Exec rpt_posiciones 'CGP', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboEstadoGPS.SelectedValue & "','" & cboTransportista.SelectedValue & "'"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"

            Response.AddHeader("Content-Disposition", "attachment;filename=Posiciones.xls")

            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("Exportar", Err, Page, Master)
        End Try
    End Sub
End Class