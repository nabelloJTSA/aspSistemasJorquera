﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREFichaViaje
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                txtNumViaje.Text = Session("num_viaje_tc")
                txtTransportista.Text = Session("transportista_tc")
                txtOrigen.Text = Session("origen_tc")
                txtDestino.Text = Session("destino_tc")
                txtCamion.Text = Session("camion_tc")
                txtArrastre.Text = Session("arrastre_tc")
                txtConductor.Text = Session("conductor_tc")
                txtRut.Text = Session("rut_conductor_tc")
                txtScore.Text = Session("score")

                Call CargarGrillaEstados()
                Call CargarGrillaAlertas()
                Call CargarGrillaIncidencias()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Private Sub CargarGrillaAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec rpt_fichaViajes 'RE-CGA', '" & Session("num_viaje_tc") & "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAlertas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaEstados()
        Try
            Dim strNomTabla As String = "TCLog_viajes"
            Dim strSQL As String = "Exec rpt_fichaViajes 'RE-CGE', '" & Session("num_viaje_tc") & "'"
            grdEstados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdEstados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEstados", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaIncidencias()
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec rpt_fichaViajes 'RE-CGI', '" & Session("num_viaje_tc") & "'"
            grdIncidencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdIncidencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaIncidencias", Err, Page, Master)
        End Try
    End Sub

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>Camión: </b> " & hdnPatente.Value & "<br /><b>Arrastre: </b> " & hdnArrastre.Value & " <br /><b>Fecha Alerta: </b> " & hdnFecAlerta.Value & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    Protected Sub VerMapaAlerta(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)

        hdnLat.Value = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        hdnLon.Value = CType(row.FindControl("hdnLongitud"), HiddenField).Value
        hdnFecAlerta.Value = CType(row.FindControl("lblFechaAlerta"), Label).Text
        hdnPatente.Value = CType(row.FindControl("lblPatente"), Label).Text
        hdnArrastre.Value = CType(row.FindControl("lblArrastre"), Label).Text
        'hdnTipoAlerta.Value = CType(row.FindControl("lblTipoAlerta"), Label).Text
    End Sub

    'Private Sub LatLonOrigen()
    '    Dim cmdTemporal As SqlCommand
    '    Dim rdoReader As SqlDataReader
    '    Dim strSQL As String = "Exec pro_ingreso_demanda 'KPI', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboServicio.SelectedValue & "' , '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & cboPOD.SelectedValue & "''" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
    '    Using cnxAcceso As New SqlConnection(strCnx)
    '        cnxAcceso.Open()
    '        Try
    '            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
    '            rdoReader = cmdTemporal.ExecuteReader()
    '            If rdoReader.Read Then
    '                lbltotalDemandas.Text = Trim(rdoReader(0).ToString)
    '                lblAsignados.Text = Trim(rdoReader(1).ToString)

    '            End If
    '        Catch ex As Exception
    '            MessageBoxError("CargarKPI", Err, Page, Master)
    '            Exit Sub
    '        Finally
    '            cnxAcceso.Close()
    '        End Try
    '    End Using
    'End Sub



    'Private Sub EjecutarEta()
    '    Dim cmdTemporal As SqlCommand
    '    Dim rdoReader As SqlDataReader
    '    Dim strSQL As String = "Exec pro_ingreso_demanda 'KPI', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboServicio.SelectedValue & "' , '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & cboPOD.SelectedValue & "''" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
    '    Using cnxAcceso As New SqlConnection(strCnx)
    '        cnxAcceso.Open()
    '        Try
    '            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
    '            rdoReader = cmdTemporal.ExecuteReader()
    '            If rdoReader.Read Then
    '                lbltotalDemandas.Text = Trim(rdoReader(0).ToString)
    '                lblAsignados.Text = Trim(rdoReader(1).ToString)
    '                lblNoAsignados.Text = Trim(rdoReader(2).ToString)
    '                lblDisponibles.Text = Trim(rdoReader(3).ToString)
    '            End If
    '        Catch ex As Exception
    '            MessageBoxError("CargarKPI", Err, Page, Master)
    '            Exit Sub
    '        Finally
    '            cnxAcceso.Close()
    '        End Try
    '    End Using
    'End Sub
End Class
