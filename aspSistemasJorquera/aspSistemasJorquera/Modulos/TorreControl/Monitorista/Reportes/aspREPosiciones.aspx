﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREPosiciones.aspx.vb" Inherits="aspSistemasJorquera.aspREPosiciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <%--        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportar" />
          
        </Triggers>--%>
        <ContentTemplate>



            <script type='text/javascript'>
                function openModal() {
                    $('[id*=ModalDocumentos]').modal('show');
                }
            </script>

            <script type='text/javascript'>
                function openModalD() {
                    $('[id*=mdlActualizarGrilla]').modal('show');
                }
            </script>

            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>


            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">

                        <asp:MultiView ID="MultiView1" runat="server">
                            <asp:View ID="vwIngreso" runat="server">
                                <div class="row btn-sm">
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Desde</label>
                                        <div>
                                            <asp:TextBox ID="txtDesde" runat="server" OnTextChanged="txtDesde_TextChanged" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Hasta</label>
                                        <div>
                                            <asp:TextBox ID="txtHasta" runat="server" OnTextChanged="txtHasta_TextChanged" TextMode="Date" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Transportista</label>
                                        <div>
                                            <asp:DropDownList ID="cboTransportista" OnSelectedIndexChanged="cboTransportista_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Estado GPS</label>
                                        <div>
                                            <asp:DropDownList ID="cboEstadoGPS" OnSelectedIndexChanged="cboEstadoGPS_SelectedIndexChanged" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:LinkButton ID="linDescargar" OnClick="linDescargar_Click" runat="server" Text="Descargar Tabla" CssClass="btn bg-green"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <!-- ./box-body -->
                                            <div class="box-footer">
                                                <div class="row table-responsive">
                                                    <div class="col-sm-12 col-xs-6">
                                                        <div class="description-block border-right">
                                                            <asp:GridView ID="grdPosiciones" runat="server" CssClass="table table-bordered table-hover bg-warning " AutoGenerateColumns="false">
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="CAMIÓN">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("patente")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CÓDIGO UNIDAD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCodigo" runat="server" Text='<%# Bind("codigo")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="TRANSPORTISTA">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltransportista" runat="server" Text='<%# Bind("transportista")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="CARDINALIDAD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCardinalidad" runat="server" Text='<%# Bind("cardinalidad")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="VELOCIDAD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblVelocidad" runat="server" Text='<%# Bind("velocidad")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="DIRECCIÓN">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDireccion" runat="server" Text='<%# Bind("direccion")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="1px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ESTADO GPS">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblGPS" runat="server" Text='<%# Bind("gps")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="LATITUD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbllatitud" runat="server" Text='<%# Bind("latitud")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="LONGITUD">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbllongitud" runat="server" Text='<%# Bind("longitud")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="FECHA REGISTRO">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("fecha")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Left" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <HeaderStyle HorizontalAlign="center" BackColor="#FF900B" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </asp:View>
                        </asp:MultiView>
                    </div>
                </div>

            </section>




        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <!-- /.content -->
</asp:Content>

