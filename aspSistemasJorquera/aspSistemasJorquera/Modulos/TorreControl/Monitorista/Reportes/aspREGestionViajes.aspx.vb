﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREGestionViajes
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "LISTADO DE VIAJES"

                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")
                Call CargarClientes()
                Call CargarServicios()
                Call CargarTransportistas()
                Call CargarFamilias()
                Call CargarEstados()
                Call CargarGrillaViajes()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    'Protected Sub btnControlVW_Click(sender As Object, e As EventArgs) Handles btnControlVW.Click
    '    MultiView1.ActiveViewIndex = 0
    '    lbltitulo.Text = "CONTROL VIAJES"
    'End Sub

    'Protected Sub btnAlertasVW_Click(sender As Object, e As EventArgs) Handles btnAlertasVW.Click
    '    MultiView1.ActiveViewIndex = 1
    '    lbltitulo.Text = "ALERTAS"
    'End Sub

    'Protected Sub btnCicloVW_Click(sender As Object, e As EventArgs) Handles btnCicloVW.Click
    '    MultiView1.ActiveViewIndex = 2
    '    lbltitulo.Text = "CICLO LOGISTICO"
    'End Sub


    'Private Sub CargarKPI()
    '    Dim cmdTemporal As SqlCommand
    '    Dim rdoReader As SqlDataReader
    '    Dim strSQL As String = "Exec pro_ingreso_demanda 'KPI', '" & Session("cod_usu_tc") & "','" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboServicio.SelectedValue & "' , '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "', '" & cboPOD.SelectedValue & "''" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "'"
    '    Using cnxAcceso As New SqlConnection(strCnx)
    '        cnxAcceso.Open()
    '        Try
    '            cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
    '            rdoReader = cmdTemporal.ExecuteReader()
    '            If rdoReader.Read Then
    '                lbltotalDemandas.Text = Trim(rdoReader(0).ToString)
    '                lblAsignados.Text = Trim(rdoReader(1).ToString)
    '                lblNoAsignados.Text = Trim(rdoReader(2).ToString)
    '                lblDisponibles.Text = Trim(rdoReader(3).ToString)
    '            End If
    '        Catch ex As Exception
    '            MessageBoxError("CargarKPI", Err, Page, Master)
    '            Exit Sub
    '        Finally
    '            cnxAcceso.Close()
    '        End Try
    '    End Using
    'End Sub

    Private Sub CargarClientes()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec rpt_gestion_viajes 'CCC'"
            cboClientes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboClientes.DataTextField = "nom_cliente1"
            cboClientes.DataValueField = "cod_msoft"
            cboClientes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClientes", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec rpt_gestion_viajes 'CCS'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicios"
            cboServicio.DataValueField = "cod_servicios"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_gestion_viajes 'CCT'"
            cboTrasportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTrasportista.DataTextField = "nom_transportista"
            cboTrasportista.DataValueField = "cod_msoft"
            cboTrasportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaViajes()
        Dim strNomTabla As String = "TCMov_alertas_nativas"
        Dim strSQL As String = "Exec rpt_gestion_viajes 'CGV', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "','" + cboFamilia.SelectedValue + "','" + txtCamion.Text + "','" + txtConductor.Text + "','" + cboEstados.SelectedValue + "','" + cboPropietario.SelectedValue + "'"
        grdViajes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
        grdViajes.DataBind()
    End Sub
    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function

    Function Rescatarinformacion() As String
        Dim strDatos As String = ""
        strDatos = "<b>N° VIAJE: </b> " & hdnIdViaje.Value & "<br /><b>Camión: </b> " & hdnPatente.Value & "<br /><b>Arrastre: </b> " & hdnPatArrastre.Value & " <br /><b>Conductor: </b> " & hdnNomconductor.Value & "<br /><b>Fono: </b> " & hdnfonoConductor.Value & "<br /><b>--- ALERTAS --- </b><br /><b>Tipo Alerta: </b> " & hdnTipoAlerta.Value & "<br /><b>Fecha Alerta: </b> " & hdnFecAlerta.Value & ""
        strDatos = "" & strDatos & ""
        Return strDatos
    End Function

    Protected Sub VerAlertas(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)


        hdnLat.Value = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        hdnLon.Value = CType(row.FindControl("hdnLonViaje"), HiddenField).Value

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnPatente.Value = CType(row.FindControl("btnCamion"), Label).Text
        hdnPatArrastre.Value = CType(row.FindControl("btnArrastre"), Label).Text
        hdnNomconductor.Value = CType(row.FindControl("lblNomConductor"), Label).Text
        hdnfonoConductor.Value = CType(row.FindControl("lblFono"), Label).Text

    End Sub

    Protected Sub VerFichaViaje(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("transportista_tc") = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value
        Session("origen_tc") = CType(row.FindControl("lblOrigen"), Label).Text
        Session("destino_tc") = CType(row.FindControl("btnDestino"), Label).Text
        Session("camion_tc") = CType(row.FindControl("btnCamion"), Label).Text
        Session("arrastre_tc") = CType(row.FindControl("btnArrastre"), Label).Text
        Session("conductor_tc") = CType(row.FindControl("lblNomConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("btnRutConductor"), Label).Text
        Session("score") = "Score"

        Session("lat_ficha_tc") = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        Session("lon_ficha_tc") = CType(row.FindControl("hdnLonViaje"), HiddenField).Value
        Session("origen_ficha_tc") = CType(row.FindControl("hdnCodOrigen"), HiddenField).Value



        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREFichaViaje.aspx")

    End Sub
    Private Sub CargarGrillaDocumentos(ByVal tipo As String, ByVal id As String)
        Try
            Dim strNomTabla As String = "SSARTMae_usuarios"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GED', '" & tipo & "', '" & id & "'"
            grdDocumentacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdDocumentacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDocumentos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDocumentacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDocumentacion.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFechaVenc As String = CType(row.FindControl("lblFecVencimiento"), Label).Text

                If CDate(objFechaVenc) <= CDate(Date.Now.Date) Then
                    e.Row.BackColor = System.Drawing.Color.LightSalmon
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdLugaresPRO_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnVerDocCamion_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE CAMION: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", ""))

        lblTrasnportista.Text = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub btnVerDocArrastre_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(1, Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", "")))
        lblNomModal.Text = "PATENTE ARRASTRE: "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("txtArrastre"), TextBox).Text, "-", ""))
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Protected Sub CargarDatosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'RDC', '" & Trim(Replace(CType(row.FindControl("txtCamion"), TextBox).Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    CType(row.FindControl("txtCamion"), TextBox).Text = Replace(Trim(rdoReader(0).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtCamion"), TextBox).Text, CType(row.FindControl("btnGPSCamion"), ImageButton))

                    CType(row.FindControl("hdnCodConductor"), HiddenField).Value = Trim(rdoReader(1).ToString)
                    CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(rdoReader(4).ToString), "-", "")

                    Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
                    Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))

                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(rdoReader(3).ToString)
                    CType(row.FindControl("lblNomConductor"), Label).Text = Trim(rdoReader(2).ToString) & " / " & Trim(rdoReader(5).ToString)

                    Call VerificarDoctosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))

                Else

                    CType(row.FindControl("txtArrastre"), TextBox).Text = ""
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = ""
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""

                    CType(row.FindControl("txtCamion"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                    CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
                    CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"

                    MessageBox("Buscar", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")

                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosVehiculo(ByVal patente As String, ByRef txt As TextBox, ByRef btn As LinkButton)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                        ' btn.Visible = False
                    Else
                        txt.BackColor = System.Drawing.Color.White
                        ' btn.Visible = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub RescatarEstadoGPSPRO(ByVal patente As String, ByRef img As ImageButton)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_asignacion_viajes 'GPS', '" & Trim(Replace(patente, "-", "")) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = 1 Then
                        img.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        img.ImageUrl = "~/Imagen/malo.png"
                    Else
                        img.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    img.ImageUrl = "~/Imagen/nulo.png"
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarEstadoGPSPRO", Err, Page, Master)
        End Try
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub GPSArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        CType(row.FindControl("txtArrastre"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtArrastre"), TextBox).Text), "-", "")

        If CType(row.FindControl("txtArrastre"), TextBox).Text = "" Then
            CType(row.FindControl("txtArrastre"), TextBox).BackColor = System.Drawing.Color.White
        Else
            Call VerificarDoctosVehiculo(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("txtArrastre"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton))
        End If

        Call RescatarEstadoGPSPRO(CType(row.FindControl("txtArrastre"), TextBox).Text, CType(row.FindControl("btnGPSArrastre"), ImageButton))
    End Sub

    Protected Sub DoctosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, TextBox).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        If CType(row.FindControl("txtRutconductor"), TextBox).Text = "" Then
            CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
        Else
            If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                CType(row.FindControl("txtRutconductor"), TextBox).BackColor = System.Drawing.Color.White
                CType(row.FindControl("lblNomConductor"), Label).Text = ""
            Else
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), ".", "")
                CType(row.FindControl("txtRutconductor"), TextBox).Text = Replace(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), "-", "")
                If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 8 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 7) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                    CType(row.FindControl("txtRutconductor"), TextBox).Text = Left(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 8) + "-" + Right(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text), 1)
                End If


                If Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text) = "" Then
                    CType(row.FindControl("lblNomConductor"), Label).Text = ""
                Else
                    If Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 10 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)
                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    ElseIf Len(Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)) = 9 Then
                        CType(row.FindControl("txtRutconductor"), TextBox).Text = "0" + Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text)

                        If ValidarRut(CType(row.FindControl("txtRutconductor"), TextBox)) = True Then
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                            Call RescatarDatosConductor(CType(row.FindControl("txtRutconductor"), TextBox).Text, CType(row.FindControl("lblNomConductor"), Label).Text, CType(row.FindControl("txtRutconductor"), TextBox), CType(row.FindControl("btnAsignarPre"), LinkButton), CType(row.FindControl("txtCamion"), TextBox), CType(row.FindControl("txtArrastre"), TextBox))
                        Else
                            MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                            CType(row.FindControl("lblNomConductor"), Label).Text = ""
                        End If
                    End If
                End If
            End If
        End If
    End Sub


    Private Sub RescatarDatosConductor(ByVal rut As String, ByRef nombre As String, ByRef txtRut As TextBox, ByRef btn As LinkButton, ByRef txtCamion As TextBox, ByRef txtArrastre As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'BDC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    rut = Trim(rdoReader(0).ToString)
                    nombre = Trim(rdoReader(1).ToString) '& " " & Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(rut, txtRut, btn, txtCamion, txtArrastre)
                Else
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnVerDocConductor_ClickG(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        pnlDoctos.Visible = True
        pnlDetalleViaje.Visible = False
        pnlKPI.Visible = False

        Call CargarGrillaDocumentos(2, Trim(CType(row.FindControl("txtRutconductor"), TextBox).Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(CType(row.FindControl("lblNomConductor"), Label).Text, "-", "")) & " / " & Trim(CType(row.FindControl("hdnFono"), HiddenField).Value)
        lblTrasnportista.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub




    Protected Sub btnTipoViajeClick(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)


        Call CargarGrillaMultipaso(CType(row.FindControl("hdnOS"), HiddenField).Value, CType(row.FindControl("hdnCodcliente"), HiddenField).Value)

        pnlDoctos.Visible = False
        pnlDetalleViaje.Visible = True
        pnlKPI.Visible = False

        lblNumOS.Text = Trim(Replace(CType(row.FindControl("lblOS"), Label).Text, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub

    Private Sub CargarGrillaMultipaso(ByVal os As String, ByVal cod_cliente As String)
        Try
            Dim strNomTabla As String = "TCMov_alertas_nativas"
            Dim strSQL As String = "Exec pro_asignacion_viajes 'CDM' , '" & os & "', '" & cod_cliente & "'"
            grdMultipaso.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdMultipaso.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaMultipaso", Err, Page, Master)
        End Try
    End Sub


    Protected Sub CancelarUT(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Call CancelarAsignacion(8, CType(row.FindControl("hdnID"), HiddenField).Value)
    End Sub


    Protected Sub AddIndicencias(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)

        Session("num_viaje_tc") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("transportista_tc") = CType(row.FindControl("hdnNomTrasnportista"), HiddenField).Value
        Session("nom_cli_tc") = CType(row.FindControl("hdnNomCliente"), HiddenField).Value
        Session("cod_cli_tc") = CType(row.FindControl("hdnCodCliente"), HiddenField).Value
        Session("nom_pro_tc") = CType(row.FindControl("hdnNomProducto"), HiddenField).Value
        Session("origen_tc") = CType(row.FindControl("lblOrigen"), Label).Text
        Session("destino_tc") = CType(row.FindControl("btnDestino"), LinkButton).Text
        Session("camion_tc") = CType(row.FindControl("btnCamion"), LinkButton).Text
        Session("arrastre_tc") = CType(row.FindControl("btnArrastre"), LinkButton).Text
        Session("conductor_tc") = CType(row.FindControl("lblNomConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("btnRutConductor"), LinkButton).Text
        Session("nom_ser_tc") = CType(row.FindControl("hdnNomServicio"), HiddenField).Value
        Session("nom_go_tc") = CType(row.FindControl("hdnNomGrupoOperativo"), HiddenField).Value
        Session("cod_go_tc") = CType(row.FindControl("hdnGrupoOperativo"), HiddenField).Value
        Session("cod_pro_tc") = CType(row.FindControl("hdnCodProducto"), HiddenField).Value
        Session("num_telefono_tc") = CType(row.FindControl("hdnFono"), HiddenField).Value




        Session("lat_incidencia_tc") = CType(row.FindControl("hdnLatViaje"), HiddenField).Value
        Session("lon_incidencia_tc") = CType(row.FindControl("hdnLonViaje"), HiddenField).Value

        Session("nom_sup_tc") = "SUPERVISOR ..."


        Response.Redirect("~/Modulos/TorreControl/Monitorista/Procesos/aspIncidencias.aspx")

    End Sub


    Protected Sub CancelarAsignacion(ByVal est_viaje As String, ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_asignacion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UEA"
            comando.Parameters.Add("@id_est_viaje", SqlDbType.NVarChar)
            comando.Parameters("@id_est_viaje").Value = est_viaje
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("cod_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Call CargarGrillaViajes()
            MessageBox("Cancelar", "Registro Cancelado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("CancelarAsignacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdViajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdViajes.RowDataBound
        '  Try
        Dim row As GridViewRow = e.Row
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
            ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
            Else
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
            End If


            If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
            ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
            Else
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
            End If

            'Dim objCboLugar As DropDownList = CType(row.FindControl("cboLugar"), DropDownList)
            'Call CargarComboLugares(objCboLugar, 1, CType(row.FindControl("hdnCodcliente"), HiddenField).Value)
            'objCboLugar.SelectedValue = CType(row.FindControl("hdnCodDestino"), HiddenField).Value

        End If
        'Catch ex As Exception
        '    MessageBoxError("grdViajes_RowDataBound", Err, Page, Master)
        'End Try
    End Sub

    'Private Sub CargarComboLugares(ByRef objCombo As DropDownList, ByVal tipo_lugar As String, ByVal cliente As String)
    '    Dim strSQL As String = "Exec pro_ingreso_demanda 'COD', '" & tipo_lugar & "', '" & cliente & "'"
    '    Dim strNomTabla As String = "TCMov_demandas"
    '    Try
    '        objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBDTC_QA").Tables(strNomTabla).DefaultView
    '        objCombo.DataBind()
    '    Catch ex As Exception
    '        'MessageBoxError("CargarComboLugares", Err, Page, Master)
    '        MessageBox("Cargar Lugares", "Lugar NO corresponde a Cliente", Page, Master, "W")
    '    End Try
    'End Sub



    Private Sub CargarDestinos(ByVal cliente As String)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'COD', '" & 1 & "', '" & cliente & "'"
            cboDestinos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboDestinos.DataTextField = "nom_lugar"
            cboDestinos.DataValueField = "cod_lugar"
            cboDestinos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDestinos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnActDestino(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = True
        'pnlrecursos.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = False

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value
        txtOBS.Text = ""
        lblTituloActualizar.Text = "ACTULIZAR DESTINO OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        Call CargarDestinos(CType(row.FindControl("hdnCodcliente"), HiddenField).Value)
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click

        If pnlDestinos.Visible = True Then
            Call ActualizarDestino(cboDestinos.SelectedValue, hdnID.Value, hdnIdViaje.Value)

        ElseIf pnlCamion.Visible = True Then
            Call ActualizarRecursos("CAMION", cboCamion.SelectedValue, hdnID.Value, hdnIdViaje.Value)
        ElseIf pnlArrastre.Visible = True Then
            Call ActualizarRecursos("ARRASTRE", txtArrastre.Text, hdnID.Value, hdnIdViaje.Value)
        ElseIf pnlConductor.Visible = True Then
            Call ActualizarRecursos("CONDUCTOR", hdnCodConductor.Value, hdnID.Value, hdnIdViaje.Value)
        End If

        MessageBox("Actualizar Recursos", "Recusros Actualizados", Page, Master, "S")
        Call CargarGrillaViajes()
    End Sub

    Protected Sub ActualizarDestino(ByVal destino As String, ByVal id As String, ByVal num_viaje As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UDV"
            comando.Parameters.Add("@cod_destino", SqlDbType.NVarChar)
            comando.Parameters("@cod_destino").Value = destino
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = num_viaje
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Parameters.Add("@obs", SqlDbType.NVarChar)
            comando.Parameters("@obs").Value = Trim(txtOBS.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDestino", Err, Page, Master)
        End Try
    End Sub

    Protected Sub ActualizarRecursos(ByVal tipo As String, ByVal recurso As String, ByVal id As String, ByVal num_viaje As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_gestion_viajes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "URV"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = tipo
            comando.Parameters.Add("@recurso", SqlDbType.NVarChar)
            comando.Parameters("@recurso").Value = recurso
            comando.Parameters.Add("@obs", SqlDbType.NVarChar)
            comando.Parameters("@obs").Value = Trim(txtOBS.Text)
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = id
            comando.Parameters.Add("@num_viaje", SqlDbType.NVarChar)
            comando.Parameters("@num_viaje").Value = num_viaje
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("nom_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarRecursos", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnActRecursosArrastre(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        'pnlrecursos.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = True
        pnlConductor.Visible = False

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        lblTituloActualizar.Text = "ACTULIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

        txtOBS.Text = ""

        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub btnActRecursosConductor(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        ' pnlrecursos.Visible = False
        pnlCamion.Visible = False
        pnlArrastre.Visible = False
        pnlConductor.Visible = True

        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        lblTituloActualizar.Text = "ACTULIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        txtOBS.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub


    Protected Sub btnActRecursosCamion(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdViajes.Rows(rowIndex)
        pnlDestinos.Visible = False
        pnlCamion.Visible = True
        pnlArrastre.Visible = False
        pnlConductor.Visible = False

        hdnIdServicio.Value = CType(row.FindControl("hdnCodServicio"), HiddenField).Value
        hdnFechaServicio.Value = CType(row.FindControl("lblFecOrigen"), Label).Text
        hdnhoraservicio.Value = CType(row.FindControl("lblHoraOrigen"), Label).Text
        hdnIdViaje.Value = CType(row.FindControl("hdnNumeroViaje"), HiddenField).Value
        hdnID.Value = CType(row.FindControl("hdnID"), HiddenField).Value

        Call CargarPatentes(CType(row.FindControl("hdnGrupoOperativo"), HiddenField).Value)

        lblTituloActualizar.Text = "ACTULIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value
        txtOBS.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    End Sub




    'Protected Sub btnActRecursos(sender As Object, e As EventArgs)
    '    Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
    '    Dim row As GridViewRow = grdViajes.Rows(rowIndex)
    '    pnlDestinos.Visible = False
    '    'pnlrecursos.Visible = True

    '    hdnIdServicio.Value = CType(row.FindControl("hdnCodServicio"), HiddenField).Value
    '    hdnFechaServicio.Value = CType(row.FindControl("lblFecOrigen"), Label).Text
    '    hdnhoraservicio.Value = CType(row.FindControl("lblHoraOrigen"), Label).Text

    '    Call CargarGrupoOperativo(hdnIdServicio.Value)

    '    Call CargarPatentes()

    '    hdnIdViaje.Value = CType(row.FindControl("hdnID"), HiddenField).Value
    '    lblTituloActualizar.Text = "ACTULIZAR RECURSOS OS: " & CType(row.FindControl("hdnOS"), HiddenField).Value

    '    ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModalD();", True)
    'End Sub

    'Private Sub CargarGrupoOperativo(ByVal servicio As String)
    '    Try
    '        Dim strNomTablaR As String = "TCMae_lugares"
    '        Dim strSQLR As String = "Exec pro_ingreso_demanda 'CGO', '" & servicio & "'"
    '        cboClaOperativa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
    '        cboClaOperativa.DataTextField = "nom_grupo_operativo"
    '        cboClaOperativa.DataValueField = "cod_grupo_operativo"
    '        cboClaOperativa.DataBind()
    '    Catch ex As Exception
    '        MessageBoxError("CargarGrupoOperativo", Err, Page, Master)
    '    End Try
    'End Sub

    'Protected Sub cboClaOperativa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClaOperativa.SelectedIndexChanged
    '    If cboClaOperativa.SelectedValue = "0" Then
    '        cboCamion.ClearSelection()
    '        cboCamion.Enabled = False
    '        txtArrastre.Text = ""
    '        txtArrastre.Enabled = False
    '        txtRutconductor.Text = ""
    '        txtNomconductor.Text = ""
    '        txtRutconductor.Enabled = False
    '        btnVerDocCamion.Visible = False
    '        btnVerDocConductor.Visible = False
    '        'btnVerDocArrastre.Visible = False
    '        imgGPSArrastre.Visible = False
    '        imgGPSCamion.Visible = False
    '        hdnCodConductor.Value = 0
    '        txtArrastre.BackColor = System.Drawing.Color.White
    '        cboCamion.BackColor = System.Drawing.Color.White
    '        txtRutconductor.BackColor = System.Drawing.Color.White
    '        txtFono.Text = ""
    '        txtFono.Enabled = False
    '        lblTrasnportista.Text = ""
    '    Else
    '        Call CargarPatentes()
    '        cboCamion.ClearSelection()
    '        cboCamion.Enabled = True
    '        txtArrastre.Text = ""
    '        txtArrastre.Enabled = False
    '        txtRutconductor.Text = ""
    '        txtNomconductor.Text = ""
    '        txtRutconductor.Enabled = False
    '        btnVerDocCamion.Visible = False
    '        btnVerDocConductor.Visible = False
    '        'btnVerDocArrastre.Visible = False
    '        imgGPSArrastre.Visible = False
    '        imgGPSCamion.Visible = False
    '        hdnCodConductor.Value = 0
    '        txtArrastre.BackColor = System.Drawing.Color.White
    '        cboCamion.BackColor = System.Drawing.Color.White
    '        txtRutconductor.BackColor = System.Drawing.Color.White
    '        txtFono.Text = ""
    '        txtFono.Enabled = True
    '        lblTrasnportista.Text = ""
    '    End If
    'End Sub

    Private Sub CargarPatentes(ByVal cla_operativa As String)
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec pro_ingreso_demanda 'CPT', '" & cla_operativa & "','" & hdnIdServicio.Value & "', '" & CDate(hdnFechaServicio.Value) & "',  '" & hdnhoraservicio.Value & "'"
            cboCamion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCamion.DataTextField = "nom_patente"
            cboCamion.DataValueField = "nom_patente"
            cboCamion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPatentes", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboCamion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCamion.SelectedIndexChanged
        If cboCamion.SelectedValue = "0-SELECCIONE" Then
            txtArrastre.Text = ""
            txtArrastre.Enabled = False
            txtRutconductor.Text = ""
            txtNomconductor.Text = ""
            txtRutconductor.Enabled = False
            ' btnVerDocCamion.Visible = False
            btnVerDocConductor.Visible = False
            ' btnVerDocArrastre.Visible = False
            imgGPSArrastre.Visible = False
            imgGPSCamion.Visible = False
            txtArrastre.BackColor = System.Drawing.Color.White
            cboCamion.BackColor = System.Drawing.Color.White
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtFono.Text = ""
            txtFono.Enabled = False
            lblTrasnportista.Text = ""
        Else
            Call CargarDatosCamionPRO()
            txtArrastre.Enabled = True
            txtRutconductor.Enabled = True
            'btnVerDocCamion.Visible = True
            btnVerDocConductor.Visible = True
            '  btnVerDocArrastre.Visible = True
            imgGPSArrastre.Visible = True
            imgGPSCamion.Visible = True
            txtFono.Enabled = True
        End If
    End Sub


    Private Sub CargarDatosCamionPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'RDC', '" & Trim(Replace(cboCamion.SelectedValue, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    cboCamion.SelectedValue = Trim(rdoReader(0).ToString)
                    Call VerificarGPSCamionPRO()
                    Call VerificarDoctosCamion(cboCamion.SelectedValue, cboCamion)

                    hdnCodConductor.Value = Trim(rdoReader(1).ToString)
                    'txtArrastre.Text = Trim(rdoReader(4).ToString)
                    ' Call VerificarGPSArrastrePRO()

                    'Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)

                    ' hdnRutconductor.Value = Trim(rdoReader(3).ToString)
                    ' txtRutconductor.Text = Trim(rdoReader(3).ToString)
                    ' txtNomconductor.Text = Trim(rdoReader(2).ToString)
                    ' txtFono.Text = Trim(rdoReader(5).ToString)
                    hdnCodTrasnportista.Value = Trim(rdoReader(6).ToString)
                    lblTrasnportista.Text = Trim(rdoReader(7).ToString)

                    ' Call VerificarDoctosConductorPRO(hdnRutconductor.Value, txtRutconductor)

                    'If Trim(txtArrastre.Text) = "" Then
                    '    btnVerDocArrastre.Visible = False
                    'Else
                    '    btnVerDocArrastre.Visible = True
                    'End If

                    If Trim(txtRutconductor.Text) = "" Then
                        btnVerDocConductor.Visible = False
                    Else
                        btnVerDocConductor.Visible = True
                    End If

                    'btnVerDocCamion.Visible = True
                Else
                    MessageBox("Camión NO Valido", "Camión no registrado en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                    txtArrastre.Text = ""
                    hdnRutconductor.Value = 0
                    txtNomconductor.Text = ""
                    txtFono.Text = ""
                    'btnVerDocCamion.Visible = False
                    ' btnVerDocArrastre.Visible = False
                    btnVerDocConductor.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("CargarDatosCamionPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub



    Private Sub VerificarGPSCamionPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(cboCamion.SelectedValue, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSCamion.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 3 Then
                        imgGPSCamion.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSCamion.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosCamion(ByVal patente As String, ByRef txt As DropDownList)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarGPSArrastrePRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'GPS', '" & Trim(Replace(txtArrastre.Text, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    imgGPSArrastre.Visible = True
                    If rdoReader(0).ToString = 1 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/ok.png"
                    ElseIf rdoReader(0).ToString = 2 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/malo.png"
                    ElseIf rdoReader(0).ToString = 3 Then
                        imgGPSArrastre.ImageUrl = "~/Imagen/nulo.png"
                    End If
                Else
                    imgGPSArrastre.Visible = False
                End If
            Catch ex As Exception
                MessageBoxError("VerificarGPSArrastre", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosArrastre(ByVal patente As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_asignacion_viajes 'VVD', '" & Trim(Replace(patente, "-", "")) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    MessageBox("Arrastre", "Arrastre NO registrado en Msfot, comicarse con operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosCamion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub VerificarDoctosConductorPRO(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) <= CDate(Date.Now.AddDays(5)) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                Else
                    txt.BackColor = System.Drawing.Color.White
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Protected Sub txtArrastre_TextChanged(sender As Object, e As EventArgs) Handles txtArrastre.TextChanged
        If Trim(txtArrastre.Text) = "" Then
            txtArrastre.BackColor = System.Drawing.Color.White
            imgGPSArrastre.Visible = False
            ' btnVerDocArrastre.Visible = False

        Else
            ' btnVerDocArrastre.Visible = True
            Call VerificarDoctosArrastre(txtArrastre.Text, txtArrastre)
            Call VerificarGPSArrastrePRO()

        End If
    End Sub

    Protected Sub txtRutconductor_TextChanged(sender As Object, e As EventArgs) Handles txtRutconductor.TextChanged

        If Trim(txtRutconductor.Text) = "" Then
            txtRutconductor.BackColor = System.Drawing.Color.White
            txtNomconductor.Text = ""
            txtFono.Text = ""
            lblTrasnportista.Text = ""
            btnVerDocConductor.Visible = False
        Else

            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), ".", "")
            txtRutconductor.Text = Replace(Trim(txtRutconductor.Text), "-", "")
            If Len(Trim(txtRutconductor.Text)) = 8 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 7) + "-" + Right(Trim(txtRutconductor.Text), 1)
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = Left(Trim(txtRutconductor.Text), 8) + "-" + Right(Trim(txtRutconductor.Text), 1)
            End If

            If Len(Trim(txtRutconductor.Text)) = 10 Then
                txtRutconductor.Text = Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO()
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    txtNomconductor.Text = ""
                End If
            ElseIf Len(Trim(txtRutconductor.Text)) = 9 Then
                txtRutconductor.Text = "0" + Trim(txtRutconductor.Text)
                If ValidarRut(txtRutconductor) = True Then
                    txtNomconductor.Text = ""
                    Call RescatarDatosConductorPRO()
                Else
                    MessageBox("Conductor", "Rut Incorrecto", Page, Master, "E")
                    txtNomconductor.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub RescatarDatosConductorPRO()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_programacion 'BDC', '" & Trim(txtRutconductor.Text) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtRutconductor.Text = Trim(rdoReader(0).ToString)
                    txtNomconductor.Text = Trim(rdoReader(1).ToString)
                    txtFono.Text = Trim(rdoReader(2).ToString)
                    hdnCodConductor.Value = Trim(rdoReader(3).ToString)
                    Call VerificarDoctosConductor(txtRutconductor.Text, txtRutconductor)
                    btnVerDocConductor.Visible = True
                Else
                    btnVerDocConductor.Visible = False
                    MessageBox("Conductor", "Conductor NO Existe en Msoft, favor comunicarse con Operaciones", Page, Master, "W")
                End If
            Catch ex As Exception
                MessageBoxError("RescatarDatosConductorPRO", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarDoctosConductor(ByVal rut As String, ByRef txt As TextBox)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_ingreso_demanda 'VVC', '" & Trim(rut) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If CDate(rdoReader(0).ToString) < CDate(Date.Now.ToString) Then
                        txt.BackColor = System.Drawing.Color.LightSteelBlue
                        txt.ForeColor = System.Drawing.Color.Black
                    Else
                        txt.BackColor = System.Drawing.Color.White
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("VerificarDoctosConductor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    'Protected Sub btnVerDocCamion_Click(sender As Object, e As EventArgs) Handles btnVerDocCamion.Click
    '    Call CargarGrillaDocumentos(1, Trim(Replace(cboCamion.SelectedValue, "-", "")))
    '    lblNomModal.Text = "PATENTE CAMION: "
    '    lblIdentificador.Text = Trim(Replace(cboCamion.SelectedValue, "-", ""))
    '    ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    'End Sub

    'Protected Sub btnVerDocArrastre_Click(sender As Object, e As EventArgs) Handles btnVerDocArrastre.Click
    '    Call CargarGrillaDocumentos(1, Trim(Replace(txtArrastre.Text, "-", "")))
    '    lblNomModal.Text = "PATENTE ARRASTRE: "
    '    lblIdentificador.Text = Trim(Replace(txtArrastre.Text, "-", ""))
    '    ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    'End Sub

    Protected Sub btnVerDocConductor_Click(sender As Object, e As EventArgs) Handles btnVerDocConductor.Click
        Call CargarGrillaDocumentos(2, Trim(txtRutconductor.Text))
        lblNomModal.Text = "CONDUCTOR:<br /> "
        lblIdentificador.Text = Trim(Replace(txtNomconductor.Text, "-", ""))
        ScriptManager.RegisterStartupScript(Me.Page, [GetType](), "Pop", "openModal();", True)
    End Sub
    Protected Sub linDescargar_Click(sender As Object, e As EventArgs)
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String = "Exec rpt_gestion_viajes 'EX-CGV', '" & CDate(txtDesde.Text) & "', '" & CDate(txtHasta.Text) & "', '" & cboClientes.SelectedValue & "', '" & cboServicio.SelectedValue & "', '" & cboTrasportista.SelectedValue & "', '" & cboPOD.SelectedValue & "','" + cboFamilia.SelectedValue + "','" + txtCamion.Text + "','" + txtConductor.Text + "','" + cboEstados.SelectedValue + "','" + cboPropietario.SelectedValue + "'"

            Dim strNomTabla As String = "TCMov_viajes"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"


            Response.AddHeader("Content-Disposition", "attachment;filename=Viajes.xls")


            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("Exportar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub linCargar_Click(sender As Object, e As EventArgs) Handles linCargar.Click
        Call CargarGrillaViajes()
    End Sub
    Protected Sub linLimpiar_Click(sender As Object, e As EventArgs)
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")
        cboFamilia.ClearSelection()
        cboServicio.ClearSelection()
        txtCamion.Text = ""
        txtConductor.Text = ""
        cboEstados.ClearSelection()
        cboPropietario.ClearSelection()
    End Sub
    Private Sub CargarFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCF'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "nom_familia"
            cboFamilia.DataValueField = "id_familia"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarEstados()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_gestion_viajes 'CCE'"
            cboEstados.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstados.DataTextField = "nom_estado"
            cboEstados.DataValueField = "id_estado"
            cboEstados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstados", Err, Page, Master)
        End Try
    End Sub


End Class