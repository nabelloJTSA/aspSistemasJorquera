﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspREHome
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session("cod_usu_tc") = Session("cod_usu_tc")
        'Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                Call CargarEstados()
                Call CargarGrillaPatentes()
                lblFecActualHome.Text = CDate(Session("fec_actual"))

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function


    Function CargarGrafico3() As String
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strDatos As String = ""

        If hdnPatente.Value = "0" And cboEstado.SelectedValue = "0" Then
            comando.CommandText = "Exec rpt_mapa 'MAP'"

        ElseIf hdnPatente.Value <> "0" Then
            ' cboEstado.ClearSelection()
            comando.CommandText = "Exec rpt_mapa 'PAT', '" & hdnPatente.Value & "'"

        ElseIf cboEstado.SelectedValue <> "0" Then
            hdnPatente.Value = 0
            comando.CommandText = "Exec rpt_mapa 'EST', '" & cboEstado.SelectedValue & "'"
        End If

        comando.Connection = conx
        comando.CommandTimeout = 90000
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            strDatos = rdoReader(0).ToString
        End If

        rdoReader.Close()
        conx.Close()
        Return strDatos

    End Function

    Private Sub CargarEstados()
        Try
            Dim strNomTablaR As String = "TCMae_lugares"
            Dim strSQLR As String = "Exec rpt_mapa 'CES'"
            cboEstado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstado.DataTextField = "nom_estado"
            cboEstado.DataValueField = "id"
            cboEstado.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstados", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnLimpiarFiltros_Click(sender As Object, e As EventArgs) Handles btnLimpiarFiltros.Click
        cboEstado.ClearSelection()
        hdnPatente.Value = "0"
        Call CargarGrillaPatentes()
    End Sub


    Private Sub CargarGrillaPatentes()
        Try
            Dim strNomTabla As String = "ADAMMov_evidencia"
            Dim strSQL As String = "Exec rpt_mapa 'GPA', '" & cboEstado.SelectedValue & "'"
            grdPatentes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdPatentes.DataBind()
            lblTotRegistros.Text = "TOTAL REGISTROS: " & grdPatentes.Rows.Count
        Catch ex As Exception
            MessageBoxError("CargarGrillaPatentes", Err, Page, Master)
        End Try
    End Sub

    Protected Sub VerMapa(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdPatentes.Rows(rowIndex)

        hdnPatente.Value = CType(row.FindControl("lblPatenteCamion"), Label).Text
    End Sub

    Protected Sub cboEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEstado.SelectedIndexChanged
        hdnPatente.Value = "0"
        Call CargarGrillaPatentes()
    End Sub

    Protected Sub grdPatentes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdPatentes.SelectedIndexChanged

    End Sub

    Private Sub grdPatentes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdPatentes.RowDataBound
        '  Try
        Dim row As GridViewRow = e.Row
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            If CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 1 Then
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/ok.png"
            ElseIf CType(row.FindControl("hdnEstGPSCamion"), HiddenField).Value = 2 Then
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/malo.png"
            Else
                CType(row.FindControl("btnGPSCamion"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
            End If


            If CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 1 Then
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/ok.png"
            ElseIf CType(row.FindControl("hdnEstGPSArrastre"), HiddenField).Value = 2 Then
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/malo.png"
            Else
                CType(row.FindControl("btnGPSArrastre"), ImageButton).ImageUrl = "~/Imagen/nulo.png"
            End If


        End If
        'Catch ex As Exception
        '    MessageBoxError("grdPatentes_RowDataBound", Err, Page, Master)
        'End Try
    End Sub
End Class