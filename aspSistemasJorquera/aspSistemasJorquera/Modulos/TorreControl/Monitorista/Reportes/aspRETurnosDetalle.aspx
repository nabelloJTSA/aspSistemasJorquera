﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspRETurnosDetalle.aspx.vb" Inherits="aspSistemasJorquera.aspRETurnosDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <Triggers>
            <asp:PostBackTrigger ControlID="btnGuardar" />

        </Triggers>
        <ContentTemplate>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server"></asp:Label>
                </h3>
                <style>
                    .bg-azul {
                        background-color: #367FA9;
                        color: white;
                    }

                    .columna.top-scrollbars {
                        transform: rotateX(180deg);
                    }
                </style>
            </section>


            <script lang="javascript" type="text/javascript">
                function CallPrint(strid) {
                    var prtContent = document.getElementById(strid);
                    var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
                    WinPrint.document.write(prtContent.innerHTML);
                    WinPrint.document.close();
                    WinPrint.focus();
                    WinPrint.print();
                }
            </script>


            <section class="content">
                <div class="row">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Familia</label>
                        <asp:DropDownList ID="cboFamilia" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboFamilia_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Servicio</label>
                        <asp:DropDownList ID="cboServicios" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboServicios_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Nro. de Viaje</label>
                        <asp:DropDownList ID="cboViaje" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboViaje_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Transportista</label>
                        <asp:DropDownList ID="cboTransportista" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboTransportista_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tracto</label>
                        <asp:DropDownList ID="cboTracto" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboTracto_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Conductor</label>
                        <asp:DropDownList ID="cboConductor" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboConductor_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <div class="form-inline" style="text-align: right">
                            <asp:LinkButton ID="linDescargar" runat="server" CssClass="btn botonesTC-descargar" OnClientClick="javascript:CallPrint('reporte1');">Descargar Reporte</asp:LinkButton>
                            <asp:LinkButton ID="linVUT" runat="server" CssClass="btn btn-primary" OnClick="lnkComentario_Click">Comentarios de Turno</asp:LinkButton>
                            <asp:LinkButton ID="linVConductor" runat="server" CssClass="btn btn-primary" OnClick="lnkGestion_Click">Gestión de Alertas</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:GridView ID="grdCumplimiento" runat="server" CssClass="table table-bordered table-hover" Width="100%" BackColor="#BDD7EE" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="DEMANDA" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDEMANDA" runat="server" Text='<%# Bind("DEMANDA")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ASIGNADOS" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblASIGNADOS" runat="server" Text='<%# Bind("ASIGNADOS")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PROMEDIO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPROMEDIO" runat="server" Text='<%# Bind("PROM")%>'></asp:Label>%                                              
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="row" id="reporte">
                    <div class="col-md-4">
                        <asp:GridView ID="grdDetalle" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="FAMILIA" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFAMILIA" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="OS" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOS" runat="server" Text='<%# Bind("VIAJE")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TRANSPORTISTA" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransportista" runat="server" Text='<%# Bind("TRANSPORTISTA")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="170" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TRACTO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTracto" runat="server" Text='<%# Bind("TRACTO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="90" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="REMOLQUE" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemolque" runat="server" Text='<%# Bind("REMOLQUE")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CONDUCTOR" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("CONDUCTOR")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TIPO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("TIPO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="90" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SERVICIO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServicio" runat="server" Text='<%# Bind("SERVICIO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#9BC2E6" Height="60" />
                            <RowStyle Height="100" />
                        </asp:GridView>
                    </div>

                    <div class="col-md-3" style="margin-left: 100px">
                        <asp:GridView ID="grdSafety" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="ALERTAS" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblALERTAS" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ATENDIDAS" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblATENDIDAS" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DETENCIÓN" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetención" runat="server" Text='<%# Bind("DETENCION")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ATRASO ORIGEN" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblATRASOO" runat="server" Text='<%# Bind("ATRASO_O")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ATRASO DESTINO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblATRASOD" runat="server" Text='<%# Bind("ATRASO_D")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PANNE" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPANNE" runat="server" Text='<%# Bind("PANNE")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="INCIDENTE" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblINCIDENTE" runat="server" Text='<%# Bind("INCIDENTE")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VELOCIDAD" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVELOCIDAD" runat="server" Text='<%# Bind("VELOCIDAD")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CONEX" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCONEX" runat="server" Text='<%# Bind("CONEX")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#9BC2E6" />
                            <RowStyle Height="100" />
                        </asp:GridView>
                    </div>

                    <div class="col-md-2" style="margin-left: 250px">

                        <asp:GridView ID="grdPerformance" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="ONTIME" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblONTIME" runat="server" Text='<%# Bind("ONTIME")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="INFULL" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblINFULL" runat="server" Text='<%# Bind("INFULL")%>'></asp:Label>                                              
                            </ItemTemplate>
                            <ItemStyle />
                        </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="SOBRESTADÍA" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSOBRESTADIA" runat="server" Text='<%# Bind("SOBRESTADIA")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPOD" runat="server" Text='<%# If(Eval("POD") = "True", "1", "0") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CERRADO" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCERRADO" runat="server" Text='<%# Bind("CERRADO")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Font-Size="Medium" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#9BC2E6" Height="60" />
                            <RowStyle Height="100" />
                        </asp:GridView>
                    </div>
                </div>

                <div class="modal fade" id="modalTurnos" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 40%">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" onclick="$('#modalTurnos').modal('hide');" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">
                                    <asp:Label ID="Label2" runat="server" Text="Comentarios de Turno"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <div class="row" style="margin-bottom: 5%">
                                    <div class="col-md-12" style="background-color: white">
                                        <div class="row">
                                            <div class="col-md-4 form-group">
                                                <asp:DropDownList ID="cboCategoria" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <asp:TextBox ID="txtComentario" runat="server" CssClass="form-control" TextMode="MultiLine" placeholder="Comentario"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="grdComentarios" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Turno" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTurno" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Categoría" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoria") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("usuario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comentario" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("comentario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Hora" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="btnGuardar" runat="server" CssClass="btn bg-green" Text="Agregar comentario" OnClick="btnGuardar_Click" />
                            </div>
                        </div>

                    </div>
                </div>
            </section>



            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="justify" Style="display: none; vertical-align: top" CssClass="EstPanel" Visible="True" Height="2000px" Width="700px">
                <div id="reporte1" class="btn-sm">
                    <table id="Table2" style="vertical-align: top;">

                        <h2>
                            <p><b>REPORTE TURNO DETALLE</b></p>
                        </h2>

                        <tr>
                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">DETALLES</label>
                                <asp:GridView ID="grdDetalle1" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="FAMILIA" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFAMILIA" Font-Size="Smaller" runat="server" Text='<%# Bind("FAMILIA")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="150" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OS" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOS" Font-Size="Smaller" runat="server" Text='<%# Bind("VIAJE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TRANSPORTISTA" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTransportista" Font-Size="Smaller" runat="server" Text='<%# Bind("TRANSPORTISTA")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="170" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TRACTO" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTracto" Font-Size="Smaller" runat="server" Text='<%# Bind("TRACTO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="90" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="REMOLQUE" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemolque" Font-Size="Smaller" runat="server" Text='<%# Bind("REMOLQUE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="100" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CONDUCTOR" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblConductor" Font-Size="Smaller" runat="server" Text='<%# Bind("CONDUCTOR")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="200" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TIPO" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTipo" Font-Size="Smaller" runat="server" Text='<%# Bind("TIPO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="90" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SERVICIO" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServicio" Font-Size="Smaller" runat="server" Text='<%# Bind("SERVICIO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="150" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                </asp:GridView>
                            </td>
                        </tr>


                        <tr>
                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">SAFETY</label>
                                <div class="col-md-3" style="margin-left: 100px">
                                    <asp:GridView ID="grdSafety1" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ALERTAS" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblALERTAS" Font-Size="Smaller" runat="server" Text='<%# Bind("ALERTAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATENDIDAS" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATENDIDAS" Font-Size="Smaller" runat="server" Text='<%# Bind("ATENDIDAS")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DETENCIÓN" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDetención" Font-Size="Smaller" runat="server" Text='<%# Bind("DETENCION")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATRASO ORIGEN" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATRASOO" Font-Size="Smaller" runat="server" Text='<%# Bind("ATRASO_O")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ATRASO DESTINO" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblATRASOD" Font-Size="Smaller" runat="server" Text='<%# Bind("ATRASO_D")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PANNE" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPANNE" Font-Size="Smaller" runat="server" Text='<%# Bind("PANNE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="INCIDENTE" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblINCIDENTE" Font-Size="Smaller" runat="server" Text='<%# Bind("INCIDENTE")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="VELOCIDAD" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVELOCIDAD" Font-Size="Smaller" runat="server" Text='<%# Bind("VELOCIDAD")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CONEX" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCONEX" Font-Size="Smaller" runat="server" Text='<%# Bind("CONEX")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Size="Medium" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">PERFORMANCE</label>
                                <asp:GridView ID="grdPerformance1" runat="server" CssClass="table table-bordered table-hover" BackColor="#BDD7EE" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ONTIME" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblONTIME" Font-Size="Smaller" runat="server" Text='<%# Bind("ONTIME")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Size="Medium" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SOBRESTADÍA" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSOBRESTADIA" Font-Size="Smaller" runat="server" Text='<%# Bind("SOBRESTADIA")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Size="Medium" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="POD" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPOD" Font-Size="Smaller" runat="server" Text='<%# If(Eval("POD") = "True", "1", "0") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Size="Medium" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CERRADO" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCERRADO" v runat="server" Text='<%# Bind("CERRADO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Font-Size="Medium" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" Font-Size="Smaller" />
                                </asp:GridView>
                            </td>
                        </tr>




                        <tr>

                            <td style="vertical-align: top;">
                                <label style="font-size: 13px">COMENTARIOS</label>
                                <asp:GridView ID="grdComentarios1" runat="server" CssClass="table table-bordered table-hover  " AutoGenerateColumns="false" BackColor="#BDD7EE">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Turno" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTurno" runat="server" Text='<%# Bind("turno") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Categoría" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategoria" runat="server" Text='<%# Bind("categoria") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Usuario" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("usuario") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comentario" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComentario" runat="server" Text='<%# Bind("comentario") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("fecha") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Hora" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblHora" runat="server" Text='<%# Bind("hora") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#DDEBF7" />
                                </asp:GridView>
                            </td>
                        </tr>

                    </table>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


</asp:Content>

