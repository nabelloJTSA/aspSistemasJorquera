﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Imports Microsoft.Reporting.WebForms
Public Class aspREVentasServicio
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
                Call CargarServicios()
                Call CargarFamilias()
                Call CargarAnio()
                Call CargarMeses()

                'Links
                linVCliente.CssClass = "btn btn-sm bg-gray"
                linVUT.CssClass = "btn btn-sm bg-gray"
                linVConductor.CssClass = "btn btn-sm bg-gray"
                linVServicio.CssClass = "btn btn-sm bg-orange"
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Public Sub GenerarReporte(ByVal servicio As Integer, ByVal familia As String, ByVal anno As Integer, ByVal mes As Integer)
        Dim paramList As New Generic.List(Of ReportParameter)
        Try
            rptVisor.Visible = True
            rptVisor.ServerReport.ReportPath = "/SGT/rptVentaServicio"
            paramList.Add(New ReportParameter("id_servicio", servicio, False))
            paramList.Add(New ReportParameter("familia", familia, False))
            paramList.Add(New ReportParameter("anno", anno, False))
            paramList.Add(New ReportParameter("mes", mes, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        Catch ex As Exception
            MessageBoxError("Error", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_servicios"
            Dim strSQLR As String = "Exec rpt_ventas 'CCS','" & Session("cod_usu_tc") & "'"
            cboServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicio.DataTextField = "nom_servicio"
            cboServicio.DataValueField = "cod_servicio"
            cboServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_familia"
            Dim strSQLR As String = "Exec rpt_ventas 'CCF','" & Session("cod_usu_tc") & "'"
            cboFamilia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilia.DataTextField = "nom_familia"
            cboFamilia.DataValueField = "nom_familia"
            cboFamilia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "TCMae_servicios"
            Dim strSQLR As String = "Exec rpt_ventas 'CCA'"
            cboAnno.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboAnno.DataTextField = "anno"
            cboAnno.DataValueField = "anno"
            cboAnno.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarMeses()
        Try
            Dim strNomTablaR As String = "TCMae_servicios"
            Dim strSQLR As String = "Exec rpt_ventas 'CCM'"
            cboMes.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboMes.DataTextField = "nom_mes"
            cboMes.DataValueField = "num_mes"
            cboMes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarMeses", Err, Page, Master)
        End Try
    End Sub

    Protected Sub linReporte_Click(sender As Object, e As EventArgs)
        Call GenerarReporte(cboServicio.SelectedValue, cboFamilia.SelectedValue, cboAnno.SelectedValue, cboMes.SelectedValue)
    End Sub

    Protected Sub linVCliente_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREVentasCliente.aspx")
    End Sub

    Protected Sub linVUT_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREVentasUT.aspx")
    End Sub

    Protected Sub linVConductor_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREVentasConductor.aspx")
    End Sub

    Protected Sub linVServicio_Click(sender As Object, e As EventArgs)
        'Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREVentasServicio.aspx")
    End Sub
End Class