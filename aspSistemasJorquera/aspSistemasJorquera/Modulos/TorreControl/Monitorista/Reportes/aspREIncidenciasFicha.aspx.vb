﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREIncidenciasFicha
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '  Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarIncidencias()
                txtNumViaje.Text = Session("num_viaje")
                txtTransportista.Text = Session("transportista")
                txtOrigen.Text = Session("origen_tc")
                txtDestino.Text = Session("destino_tc")
                txtCamion.Text = Session("camion_tc")
                txtArrastre.Text = Session("arrastre_tc")
                txtConductor.Text = Session("conductor_tc")
                txtRut.Text = Session("rut_conductor_tc")
                txtScore.Text = Session("score")
                hdnLat.Value = Session("latitud")
                hdnLon.Value = Session("longitud")

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarIncidencias()
        Try
            Dim strNomTabla As String = "TCMov_incidencias"
            Dim strSQL As String = "Exec rpt_incidencias 'CFI','" & Session("id_incidencia") & "'"
            grdGestion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdGestion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarIncidencias", Err, Page, Master)
        End Try
    End Sub

    'CARGA DE DATOS MAPA
    Function CargarMapa() As String
        Dim Datos As DataTable = New DataTable()
        Dim strDatos As String = ""
        strDatos = Replace(hdnLat.Value, ",", ".") & "" & "," & Replace(hdnLon.Value, ",", ".") & ""
        strDatos = "[" & strDatos & "]"
        Return strDatos
    End Function
    Function rescatarinformacion() As String
        Dim strdatos As String = ""
        strdatos = "<b>" & hdnAlertaTipo.Value & "</br><b>alerta: </b> " & Session("map_alerta") & "</br><b>alerta base: </b> " & Session("map_alertabase") & "</br> <b>categoría: </b> " & Session("map_categoria") & "</br> <b>fecha alerta: </b> " & Session("map_fecha") & "<br /><b>fecha registro: </b> " & Session("map_fecharegistro") & ""
        strdatos = "" & strdatos & ""
        Return strdatos
    End Function
End Class