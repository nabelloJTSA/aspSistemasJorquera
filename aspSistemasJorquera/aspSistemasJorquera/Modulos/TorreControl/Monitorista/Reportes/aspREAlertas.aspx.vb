﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb

Public Class aspREAlertas
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBDTC_QA").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call CargarMenu(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_modulo_tc"), Session("cod_usu_tc"))
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))

        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "REPORTE DE ALERTAS"
                txtDesde.Text = Session("fec_actual")
                txtHasta.Text = Session("fec_actual")
                Call CargarTipos()
                Call CargarCategorias()
                Call CargarFamilias()
                Call CargarServicios()
                Call CargarTransportistas()
                Call CargarEstados()
                ' Call CargarAlertas()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAlertas()
        Try
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            Dim strSQL As String = "Exec rpt_alertas 'CGA', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboTipo.SelectedValue & "','" & cboCategorias.SelectedValue & "','" & cboCriticidad.SelectedValue & "','" + cboFamilias.SelectedValue + "','" + cboServicios.SelectedValue + "','" + cboTransportista.SelectedValue + "','" + txtCamion.Text + "','" + txtConductor.Text + "','" + cboEstados.SelectedValue + "','" + cboPropietario.SelectedValue + "'"
            grdAlertas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdAlertas.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Alertas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTipos()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCA'"
            cboTipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTipo.DataTextField = "nom_alerta"
            cboTipo.DataValueField = "id_alerta_base"
            cboTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Alertas Base", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCategorias()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCC'"
            cboCategorias.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboCategorias.DataTextField = "nom_categoria"
            cboCategorias.DataValueField = "id_categoria"
            cboCategorias.DataBind()
        Catch ex As Exception
            MessageBoxError("Cargar Categorias", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarServicios()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCS'"
            cboServicios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboServicios.DataTextField = "nom_servicio"
            cboServicios.DataValueField = "id_servicio"
            cboServicios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarServicios", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarEstados()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCE'"
            cboEstados.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboEstados.DataTextField = "nom_estado"
            cboEstados.DataValueField = "id_estado"
            cboEstados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEstados", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarTransportistas()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCT'"
            cboTransportista.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboTransportista.DataTextField = "nom_transportista"
            cboTransportista.DataValueField = "id_transportista"
            cboTransportista.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTransportistas", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarFamilias()
        Try
            Dim strNomTablaR As String = "TCMae_alerta_base"
            Dim strSQLR As String = "Exec rpt_alertas 'CCF'"
            cboFamilias.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBDTC_QA").Tables(Trim(strNomTablaR)).DefaultView
            cboFamilias.DataTextField = "nom_familia"
            cboFamilias.DataValueField = "id_familia"
            cboFamilias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamilias", Err, Page, Master)
        End Try
    End Sub
    Protected Sub linDescargar_Click(sender As Object, e As EventArgs)
        Call ExportarTabla()
    End Sub

    'DESCARGAR EXCEL
    Private Sub ExportarTabla()
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String
            strSQL = "Exec rpt_alertas 'RE-CGA', '" & CDate(txtDesde.Text) & "','" & CDate(txtHasta.Text) & "','" & cboTipo.SelectedValue & "','" & cboCategorias.SelectedValue & "','" & cboCriticidad.SelectedValue & "','" + cboFamilias.SelectedValue + "','" + cboServicios.SelectedValue + "','" + cboTransportista.SelectedValue + "','" + txtCamion.Text + "','" + txtConductor.Text + "','" + cboEstados.SelectedValue + "','" + cboPropietario.SelectedValue + "'"
            Dim strNomTabla As String = "TCMov_alertas_callcenter"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBDTC_QA").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"

            Response.AddHeader("Content-Disposition", "attachment;filename=Alertas.xls")

            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("Exportar", Err, Page, Master)
        End Try
    End Sub

    'IR A LA FICHA
    Protected Sub btnNumViaje_Click(sender As Object, e As EventArgs)
        Dim rowIndex As Integer = Convert.ToInt32(TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex)
        Dim row As GridViewRow = grdAlertas.Rows(rowIndex)
        Session("num_viaje") = CType(row.FindControl("btnNumViaje"), LinkButton).Text
        Session("id_call_center") = CType(row.FindControl("hdnIDCall"), HiddenField).Value
        Session("transportista") = CType(row.FindControl("lblTransportista"), Label).Text
        Session("origen_tc") = CType(row.FindControl("hdnOrigen"), HiddenField).Value
        Session("destino_tc") = CType(row.FindControl("hdnDestino"), HiddenField).Value
        Session("camion_tc") = CType(row.FindControl("hdnCamion"), HiddenField).Value
        Session("arrastre_tc") = CType(row.FindControl("hdnArrastre"), HiddenField).Value
        Session("conductor_tc") = CType(row.FindControl("lblConductor"), Label).Text
        Session("rut_conductor_tc") = CType(row.FindControl("lblRut"), Label).Text
        Session("score") = "Score"

        Session("lat_ficha_tc") = CType(row.FindControl("hdnLatitud"), HiddenField).Value
        Session("lon_ficha_tc") = CType(row.FindControl("hdnLongitud"), HiddenField).Value
        'Session("origen_ficha_tc") = CType(row.FindControl("hdnCodOrigen"), HiddenField).Value

        Session("map_alerta") = CType(row.FindControl("lblNombreAlerta"), Label).Text
        'Session("map_alertabase") = CType(row.FindControl("lblNombreAlertaBase"), Label).Text
        Session("map_categoria") = CType(row.FindControl("lblCategoria"), Label).Text
        Session("map_fecha") = CType(row.FindControl("lblFechaAlerta"), Label).Text
        Session("map_fecharegistro") = CType(row.FindControl("lblFechaRegistro"), Label).Text

        Response.Redirect("~/Modulos/TorreControl/Monitorista/Reportes/aspREAlertasFicha.aspx")
    End Sub

    Protected Sub linCargar_Click(sender As Object, e As EventArgs) Handles linCargar.Click
        Try
            Call CargarAlertas()
        Catch ex As Exception
            MessageBoxError("linCargar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub linLimpiar_Click(sender As Object, e As EventArgs)
        txtDesde.Text = Session("fec_actual")
        txtHasta.Text = Session("fec_actual")
        cboCategorias.ClearSelection()
        cboTipo.ClearSelection()
        cboCriticidad.ClearSelection()
        cboFamilias.ClearSelection()
        cboServicios.ClearSelection()
        txtCamion.Text = ""
        txtConductor.Text = ""
        cboEstados.ClearSelection()
        cboPropietario.ClearSelection()
    End Sub


End Class