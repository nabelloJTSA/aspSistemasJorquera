﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspREVentasServicio.aspx.vb" Inherits="aspSistemasJorquera.aspREVentasServicio" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentMonitorista" runat="server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <%--        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportar" />
          
        </Triggers>--%>
        <ContentTemplate>





            <style type="text/css">
                #global {
                    height: 400px;
                    border: 1px solid #ddd;
                    background: #f1f1f1;
                    overflow-y: scroll;
                }

                #mensajes {
                    height: auto;
                }

                .texto {
                    padding: 4px;
                    background: #fff;
                }
            </style>

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h3>
                    <asp:Label ID="lbltitulo" runat="server" Text="Producción por Servicio"></asp:Label>
                </h3>
            </section>

            <!-- Main content -->
            <section class="content">

                <!-- SELECT2 EXAMPLE -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <div class="form-inline">
                                    <asp:LinkButton ID="linVCliente" runat="server" OnClick="linVCliente_Click">Producción por Cliente</asp:LinkButton>
                                    <asp:LinkButton ID="linVUT" runat="server" OnClick="linVUT_Click">Producción por UT</asp:LinkButton>
                                    <asp:LinkButton ID="linVConductor" runat="server" OnClick="linVConductor_Click">Producción por Conductor</asp:LinkButton>
                                    <asp:LinkButton ID="linVServicio" runat="server" OnClick="linVServicio_Click">Producción por Servicio</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row btn-sm">

                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Familia</label>
                                        <div>
                                            <asp:DropDownList ID="cboFamilia" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Servicio</label>
                                        <div>
                                            <asp:DropDownList ID="cboServicio" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Año</label>
                                        <div>
                                            <asp:DropDownList ID="cboAnno" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Mes</label>
                                        <div>
                                            <asp:DropDownList ID="cboMes" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group">
                                        <label for="fname"
                                            class="control-label col-form-label">
                                        </label>
                                        <div>
                                            <asp:LinkButton ID="linReporte" OnClick="linReporte_Click" runat="server" Text="Cargar Reporte" CssClass="btn bg-orange"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="linReporte" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <!-- ./box-body -->
                                    <div class="box-footer">
                                        <div class="row table-responsive">
                                            <div class="col-sm-12 col-xs-6">
                                                <div class="description-block border-right">
                                                    <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana"
                                                        waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                                                    </rsreport:ReportViewer>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>


        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>

            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="loader">
                                    <div class="loader-inner">
                                        <div class="loading one"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading two"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading three"></div>
                                    </div>
                                    <div class="loader-inner">
                                        <div class="loading four"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <!-- /.content -->
</asp:Content>
