﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspTipoTrabCargo
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvAreas.ActiveViewIndex = 0
                lbltituloP.Text = "Tipo Trabajador"
                Call CargarEmpresas()
                Call CargarGrilla()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresas()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_tipo_trabajador 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_tipo_trabajador"
            Dim strSQL As String = "Exec mant_RRHH_tipo_trabajador 'G'"
            grdTipoTrab.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdTipoTrab.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tipo_trabajador"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_tipo_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo_trabajador").Value = Trim(txtTipo.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tipo_trabajador"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_tipo_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo_trabajador").Value = Trim(txtTipo.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_tipo_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_trabajador").Value = hdnId.Value
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdTipoTrab_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdTipoTrab.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdTipoTrab.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_cod_emp As HiddenField = CType(row.FindControl("hdn_cod_emp"), HiddenField)
            Dim obj_Nom As Label = CType(row.FindControl("lblTipo"), Label)

            hdnId.Value = obj_id.Value
            txtTipo.Text = obj_Nom.Text
            hdnActivo.Value = 1
            cboEmpresa.SelectedValue = obj_cod_emp.Value

        Catch ex As Exception
            MessageBoxError("grdTipoTrab_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtTipo.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdTipoTrab_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdTipoTrab.PageIndexChanging
        Try
            grdTipoTrab.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdTipoTrab_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub btnTipoTrab_Click(sender As Object, e As EventArgs) Handles btnTipoTrab.Click
        MtvAreas.ActiveViewIndex = 0
        lbltituloP.Text = "Tipo Trabajador"
    End Sub


    '*********************************
    Protected Sub btnCargo_Click(sender As Object, e As EventArgs) Handles btnCargo.Click
        MtvAreas.ActiveViewIndex = 1
        lbltituloP.Text = "Cargos"
        Call CargarEmpresasCargo()
        Call CargarGrillaCargo()
    End Sub


    Private Sub CargarGrillaCargo()
        Try
            Dim strNomTabla As String = "RRHHMae_cargo"
            Dim strSQL As String = "Exec mant_RRHH_cargo 'G', '" & cboEmpresaCargo.SelectedValue & "'"
            grdCargos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCargos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresasCargo()
        Try
            Dim strNomTablaR As String = "RRHHMae_cargo"
            Dim strSQLR As String = "Exec mant_RRHH_cargo 'CCE'"
            cboEmpresaCargo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaCargo.DataTextField = "nom_empresa"
            cboEmpresaCargo.DataValueField = "cod_empresa"
            cboEmpresaCargo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresas", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarCargo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_cargo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_cargo", SqlDbType.NVarChar)
            comando.Parameters("@nom_cargo").Value = Trim(txtNomCargo.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaCargo.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaCargo()
            Call LimpiarCargo()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarCargo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_cargo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
            comando.Parameters("@cod_cargo").Value = hdnIdCargo.Value
            comando.Parameters.Add("@nom_cargo", SqlDbType.NVarChar)
            comando.Parameters("@nom_cargo").Value = Trim(txtNomCargo.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaCargo.SelectedValue
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaCargo()
            Call LimpiarCargo()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdAreas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCargos.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdCargos.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_emp As HiddenField = CType(row.FindControl("hdn_emp"), HiddenField)
            Dim obj_NomCargo As Label = CType(row.FindControl("lblNombreArea"), Label)
            hdnIdCargo.Value = obj_id.Value
            cboEmpresaCargo.SelectedValue = obj_emp.Value
            txtNomCargo.Text = obj_NomCargo.Text
            hdnActivoCargo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdAreas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarCargo()
        cboEmpresaCargo.ClearSelection()
        txtNomCargo.Text = ""
        hdnActivoCargo.Value = 0
        Call CargarGrillaCargo()
    End Sub

    Private Sub grdAreas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdCargos.PageIndexChanging
        Try
            grdCargos.PageIndex = e.NewPageIndex
            Call CargarGrillaCargo()
        Catch ex As Exception
            MessageBoxError("grdAreas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarCargo_Click(sender As Object, e As EventArgs) Handles btnGuardarCargo.Click
        If hdnActivoCargo.Value = 0 Then
            Call GuardarCargo()
        Else
            Call ActualizarCargo()
        End If
    End Sub

    Protected Sub btnCancelarCargo_Click(sender As Object, e As EventArgs) Handles btnCancelarCargo.Click
        Call LimpiarCargo()
    End Sub

    Protected Sub cboEmpresaCargo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaCargo.SelectedIndexChanged
        Call CargarGrillaCargo()
    End Sub
End Class