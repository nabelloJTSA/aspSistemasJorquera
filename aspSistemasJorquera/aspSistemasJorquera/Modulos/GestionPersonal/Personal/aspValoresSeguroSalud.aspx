﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspValoresSeguroSalud.aspx.vb" Inherits="aspSistemasJorquera.aspValoresSeguroSalud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboempresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Porcentaje Empresa (*)</label>
                <div>
                     <asp:TextBox ID="txtPorEmp" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>                  
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Area</label>
                <div>
                    <asp:DropDownList ID="cboArea" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Porcentaje Trabajador</label>
                <div>
                    <asp:TextBox ID="txtPorTrab" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Valor Titular (*)</label>
                <div>
                    <asp:TextBox ID="txtValTitular" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group ">
                <label for="fname" class="control-label col-form-label">Valor Carga Especial</label>
                <div>
                    <asp:TextBox ID="txtValCargaEsp" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

            <div class="col-md-2 form-group ">
                <label for="fname" class="control-label col-form-label">Valor Carga (*)</label>
                <div>
                    <asp:TextBox ID="txtValCarga" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>

        </div>


        <div class="row justify-content-center table-responsive">
            <div class="col-md-8 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnLimpiar" CssClass="btn bg-orange btn-block" runat="server">Limpiar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnexportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                </div>
            </div>

        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-12">                
                    <asp:GridView ID="grdValores" AllowPaging="true" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20">
                        <Columns>
                            <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                <ItemStyle  HorizontalAlign="Center" Width="20px" />
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Empresa">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                    <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_valores")%>' />
                                    <asp:HiddenField ID="hdnCodEmpresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                    <asp:HiddenField ID="hdnid_area" runat="server" Value='<%# Bind("id_area")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Area">
                                <ItemTemplate>
                                    <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nom_area")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="180px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Porcentaje Empresa">
                                <ItemTemplate>
                                    <asp:Label ID="lblPorcetajeEmp" runat="server" Text='<%# Bind("porcentaje_empresa")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Porcentaje Trabajor">
                                <ItemTemplate>
                                    <asp:Label ID="lblPorcentajeTrab" runat="server" Text='<%# Bind("porcentaje_trabajador")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valor Titular">
                                <ItemTemplate>
                                    <asp:Label ID="lblValTitular" runat="server" Text='<%# Bind("val_titular")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valor Carga">
                                <ItemTemplate>
                                    <asp:Label ID="lblValCarga" runat="server" Text='<%# Bind("val_carga")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valor Carga Especial">
                                <ItemTemplate>
                                    <asp:Label ID="lblValCargaEspe" runat="server" Text='<%# Bind("val_carga_especial")%>' />
                                </ItemTemplate>
                                <ItemStyle  HorizontalAlign="center" Width="120px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                    </asp:GridView>              

            </div>

        </div>

    </section>

    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnID" runat="server" />



</asp:Content>
