﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspEmpresaSuc
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvAreas.ActiveViewIndex = 0
                lbltituloP.Text = "Empresas"
                Call CargaMutual()
                Call CargaCCAF()
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaMutual()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_empresa 'CCM'"
            cboMutual.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMutual.DataTextField = "glosa"
            cboMutual.DataValueField = "id_mutualidad"
            cboMutual.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaMutual", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaCCAF()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec mant_RRHH_empresa 'CCC'"
            cboCCFA.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCCFA.DataTextField = "glosa"
            cboCCFA.DataValueField = "id_ccaf"
            cboCCFA.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaCCAF", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "REMMae_empresa"
            Dim strSQL As String = "Exec mant_RRHH_empresa 'G'"
            grdEmpresas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdEmpresas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_empresa"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = Trim(txtCodEmpresa.Text)
            comando.Parameters.Add("@nom_empresa", SqlDbType.NVarChar)
            comando.Parameters("@nom_empresa").Value = Trim(txtNomEmpresa.Text)
            comando.Parameters.Add("@nom_direccion", SqlDbType.NVarChar)
            comando.Parameters("@nom_direccion").Value = Trim(txtDireccion.Text)
            comando.Parameters.Add("@nom_giro", SqlDbType.NVarChar)
            comando.Parameters("@nom_giro").Value = Trim(txtGiro.Text)
            comando.Parameters.Add("@nom_razon_social", SqlDbType.NVarChar)
            comando.Parameters("@nom_razon_social").Value = Trim(txtRazonSocial.Text)
            comando.Parameters.Add("@rut_empresa", SqlDbType.NVarChar)
            comando.Parameters("@rut_empresa").Value = Trim(txtRutEmpresa.Text)
            comando.Parameters.Add("@rut_replegal", SqlDbType.NVarChar)
            comando.Parameters("@rut_replegal").Value = Trim(txtRutRepLegal.Text)
            comando.Parameters.Add("@nom_replegal", SqlDbType.NVarChar)
            comando.Parameters("@nom_replegal").Value = Trim(txtRepLegal.Text)
            comando.Parameters.Add("@ciudad", SqlDbType.NVarChar)
            comando.Parameters("@ciudad").Value = Trim(txtCiudad.Text)
            comando.Parameters.Add("@cod_empresa_fin700", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa_fin700").Value = Trim(txtCodEmpresaF700.Text)
            comando.Parameters.Add("@cod_actividad", SqlDbType.NVarChar)
            comando.Parameters("@cod_actividad").Value = Trim(txtCodActEc.Text)
            comando.Parameters.Add("@id_mutualidad", SqlDbType.NVarChar)
            comando.Parameters("@id_mutualidad").Value = Trim(cboMutual.SelectedValue)
            comando.Parameters.Add("@id_ccaf", SqlDbType.NVarChar)
            comando.Parameters("@id_ccaf").Value = Trim(cboCCFA.SelectedValue)
            comando.Parameters.Add("@direccion_abrev", SqlDbType.NVarChar)
            comando.Parameters("@direccion_abrev").Value = Trim(txtDireccionAbrev.Text)
            comando.Parameters.Add("@num_tasa", SqlDbType.Float)
            comando.Parameters("@num_tasa").Value = txtTasa.Text
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@num_tasa_mutualidad", SqlDbType.Float)
            comando.Parameters("@num_tasa_mutualidad").Value = txtTasaMutualidad.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_empresa"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_empresa", SqlDbType.NVarChar)
            comando.Parameters("@nom_empresa").Value = Trim(txtNomEmpresa.Text)
            comando.Parameters.Add("@nom_direccion", SqlDbType.NVarChar)
            comando.Parameters("@nom_direccion").Value = Trim(txtDireccion.Text)
            comando.Parameters.Add("@nom_giro", SqlDbType.NVarChar)
            comando.Parameters("@nom_giro").Value = Trim(txtGiro.Text)
            comando.Parameters.Add("@nom_razon_social", SqlDbType.NVarChar)
            comando.Parameters("@nom_razon_social").Value = Trim(txtRazonSocial.Text)
            comando.Parameters.Add("@rut_empresa", SqlDbType.NVarChar)
            comando.Parameters("@rut_empresa").Value = Trim(txtRutEmpresa.Text)
            comando.Parameters.Add("@rut_replegal", SqlDbType.NVarChar)
            comando.Parameters("@rut_replegal").Value = Trim(txtRutRepLegal.Text)
            comando.Parameters.Add("@nom_replegal", SqlDbType.NVarChar)
            comando.Parameters("@nom_replegal").Value = Trim(txtRepLegal.Text)
            comando.Parameters.Add("@ciudad", SqlDbType.NVarChar)
            comando.Parameters("@ciudad").Value = Trim(txtCiudad.Text)
            comando.Parameters.Add("@cod_empresa_fin700", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa_fin700").Value = Trim(txtCodEmpresaF700.Text)
            comando.Parameters.Add("@cod_actividad", SqlDbType.NVarChar)
            comando.Parameters("@cod_actividad").Value = Trim(txtCodActEc.Text)
            comando.Parameters.Add("@id_mutualidad", SqlDbType.NVarChar)
            comando.Parameters("@id_mutualidad").Value = Trim(cboMutual.SelectedValue)
            comando.Parameters.Add("@id_ccaf", SqlDbType.NVarChar)
            comando.Parameters("@id_ccaf").Value = Trim(cboCCFA.SelectedValue)
            comando.Parameters.Add("@direccion_abrev", SqlDbType.NVarChar)
            comando.Parameters("@direccion_abrev").Value = Trim(txtDireccionAbrev.Text)
            comando.Parameters.Add("@num_tasa", SqlDbType.Float)
            comando.Parameters("@num_tasa").Value = txtTasa.Text
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnId.Value
            comando.Parameters.Add("@num_tasa_mutualidad", SqlDbType.Float)
            comando.Parameters("@num_tasa_mutualidad").Value = txtTasaMutualidad.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdEmpresas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdEmpresas.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdEmpresas.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim hdnDireccion As HiddenField = CType(row.FindControl("hdnDireccion"), HiddenField)
            Dim hdnGiro As HiddenField = CType(row.FindControl("hdnGiro"), HiddenField)
            Dim hdnRZ As HiddenField = CType(row.FindControl("hdnRazonSocial"), HiddenField)
            Dim hdnDireccionAbrev As HiddenField = CType(row.FindControl("hdnDireccionAbrev"), HiddenField)
            Dim hdnCodF700 As HiddenField = CType(row.FindControl("hdnCodEmpF700"), HiddenField)
            Dim hdnCodAct As HiddenField = CType(row.FindControl("hdnCodAct"), HiddenField)
            Dim hdnCCAF As HiddenField = CType(row.FindControl("hdnCCAF"), HiddenField)
            Dim hdnMutual As HiddenField = CType(row.FindControl("hdnMutualidad"), HiddenField)
            Dim hdnCiudad As HiddenField = CType(row.FindControl("hdnCiudad"), HiddenField)
            Dim obj_Tasa As HiddenField = CType(row.FindControl("hdnNumTasa"), HiddenField)
            Dim obj_TasaMutualidad As HiddenField = CType(row.FindControl("hdnNumTasaMutualidad"), HiddenField)

            Dim obj_NomEmpresa As Label = CType(row.FindControl("lblNomEmpresa"), Label)
            Dim obj_RutEmpresa As Label = CType(row.FindControl("lblRutEmp"), Label)
            Dim obj_RepLegal As Label = CType(row.FindControl("lblRepLegal"), Label)
            Dim obj_RutRepLegal As Label = CType(row.FindControl("lblRutRepLegal"), Label)
            Dim obj_CCAF As Label = CType(row.FindControl("lblCCAF"), Label)

            hdnId.Value = hdnid_obj.Value
            txtNomEmpresa.Text = obj_NomEmpresa.Text
            txtRutEmpresa.Text = obj_RutEmpresa.Text
            txtCodEmpresa.Text = hdnid_obj.Value
            txtDireccion.Text = hdnDireccion.Value
            txtRazonSocial.Text = hdnRZ.Value
            txtDireccionAbrev.Text = hdnDireccionAbrev.Value
            txtCodEmpresaF700.Text = hdnCodF700.Value
            txtGiro.Text = hdnGiro.Value
            txtCiudad.Text = hdnCiudad.Value
            txtRepLegal.Text = obj_RepLegal.Text
            txtRutRepLegal.Text = obj_RutRepLegal.Text
            txtCodActEc.Text = hdnCodAct.Value
            cboMutual.SelectedValue = hdnMutual.Value
            cboCCFA.SelectedValue = hdnCCAF.Value
            txtTasa.Text = obj_Tasa.Value
            txtTasaMutualidad.Text = obj_TasaMutualidad.Value
            txtCodEmpresa.Enabled = False
            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdEmpresas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtNomEmpresa.Text = ""
        txtRutEmpresa.Text = ""
        txtCodEmpresa.Text = ""
        txtDireccion.Text = ""
        txtRazonSocial.Text = ""
        txtDireccionAbrev.Text = ""
        txtCodEmpresaF700.Text = ""
        txtGiro.Text = ""
        txtCiudad.Text = ""
        txtRepLegal.Text = ""
        txtRutRepLegal.Text = ""
        txtCodActEc.Text = ""
        cboMutual.ClearSelection()
        cboCCFA.ClearSelection()
        txtTasa.Text = ""
        hdnActivo.Value = 0
        txtTasaMutualidad.Text = ""
        txtCodEmpresa.Enabled = True
        Call CargarGrilla()
    End Sub

    Private Sub grdEmpresas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdEmpresas.PageIndexChanging
        Try
            grdEmpresas.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdEmpresas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Function Existe() As Boolean
        Dim strSQL As String = ""
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Try
            cnxBaseDatos.Open()
            strSQL = "Exec mant_RRHH_empresa 'RD','" & Trim(txtCodEmpresa.Text) & "'"
            cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read() Then
                Existe = True
            Else
                Existe = False
            End If
            rdoReader.Close()
            rdoReader = Nothing
            cmdTemporal.Dispose()
            cmdTemporal = Nothing
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("Existe", Err, Page, Master)
            Existe = True
        End Try
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            If Existe() = True Then
                MessageBox("Guardar", "Codigo de empresa ya existe", Page, Master, "W")
            Else
                Call Guardar()
            End If
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub
    '**********************

    Private Sub CargarGrillaSuc()
        Try
            Dim strNomTabla As String = "RRHHMae_sucursal"
            Dim strSQL As String = "Exec mant_RRHH_sucursal 'G','" & cboEmpresa.SelectedValue & "'"
            grdSucursal.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_departamento"
            Dim strSQLR As String = "Exec mant_RRHH_sucursal 'CCS'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarSuc()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_sucursal"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@nom_sucursal").Value = Trim(txtNom.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaSuc()
            Call LimpiarSuc()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarSuc()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_sucursal"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@nom_sucursal").Value = Trim(txtNom.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@id_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@id_sucursal").Value = hdnIdSuc.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaSuc()
            Call LimpiarSuc()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSucursal.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdSucursal.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_area As HiddenField = CType(row.FindControl("hdn_Empresa"), HiddenField)

            Dim obj_NomDepto As Label = CType(row.FindControl("lblNombreSucursal"), Label)

            hdnIdSuc.Value = obj_id.Value
            cboEmpresa.SelectedValue = obj_area.Value
            txtNom.Text = obj_NomDepto.Text

            hdnActivoSuc.Value = 1
        Catch ex As Exception
            MessageBoxError("grdDepto_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarSuc()
        cboEmpresa.ClearSelection()
        txtNom.Text = ""
        hdnActivoSuc.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdDepto_SelectedIndexChanged(sender As Object, e As GridViewPageEventArgs) Handles grdSucursal.PageIndexChanging
        Try
            grdSucursal.PageIndex = e.NewPageIndex
            Call CargarGrillaSuc()
        Catch ex As Exception
            MessageBoxError("grdDepto_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarSuc_Click(sender As Object, e As EventArgs) Handles btnGuardarSuc.Click
        If hdnActivoSuc.Value = 0 Then
            Call GuardarSuc()
        Else
            Call ActualizarSuc()
        End If
    End Sub

    Protected Sub btnCancelarSuc_Click(sender As Object, e As EventArgs) Handles btnCancelarSuc.Click
        Call LimpiarSuc()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrillaSuc()
    End Sub

    Protected Sub btnEmpresa_Click(sender As Object, e As EventArgs) Handles btnEmpresa.Click
        MtvAreas.ActiveViewIndex = 0
        lbltituloP.Text = "Empresas"
    End Sub

    Protected Sub btnSucursal_Click(sender As Object, e As EventArgs) Handles btnSucursal.Click
        MtvAreas.ActiveViewIndex = 1
        Call CargarEmpresa()
        Call CargarGrillaSuc()
        lbltituloP.Text = "Sucursal"
    End Sub
End Class