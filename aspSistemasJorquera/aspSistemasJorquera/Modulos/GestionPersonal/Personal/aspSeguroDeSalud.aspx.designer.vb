﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class aspSeguroDeSalud
    
    '''<summary>
    '''Control btnIPersonas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnIPersonas As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnProcesarSeguro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnProcesarSeguro As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lbltitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltitulo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control MtvPrincipal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MtvPrincipal As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Control VwIngreso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents VwIngreso As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control txtBNombreAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtBNombreAgregar As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control PnlRecurso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PnlRecurso As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control grdAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdAgregar As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control lblperiodoactivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblperiodoactivo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAgregar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnlimpiar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnlimpiar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control VwNomina.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents VwNomina As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control cboempresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboempresa As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control cboPeriodo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPeriodo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control txtPoliza.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtPoliza As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtValUF.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtValUF As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtBNombreValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtBNombreValores As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control btnvalidar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnvalidar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnCalcular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCalcular As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnExportarP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportarP As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control pnlMP.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlMP As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control grdValoresPersonal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdValoresPersonal As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnID.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRut.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRut As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodEmpresaAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodEmpresaAgregar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodEmpresa As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdArea.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdArea As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIncluido.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIncluido As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutIncluido.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutIncluido As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPorEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPorEmpresa As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPorTrabajador.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPorTrabajador As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnValTitular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnValTitular As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnValCarga.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnValCarga As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnValCargaEspecial.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnValCargaEspecial As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdValores As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnExiste.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnExiste As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdVC.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdVC As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCostoCargaPesos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCostoCargaPesos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCostoCargaEspecialPesos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCostoCargaEspecialPesos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnTotalPagaCargasUF.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnTotalPagaCargasUF As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnTotalPagoCargaPesos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnTotalPagoCargaPesos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCostoTitularPesos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCostoTitularPesos As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnMontoPagadoTitular.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnMontoPagadoTitular As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnMontoPagadoEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnMontoPagadoEmpresa As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnTotalAPagar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnTotalAPagar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPorEmpresaValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPorEmpresaValores As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPorTrabajadorValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPorTrabajadorValores As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodEmpresaValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodEmpresaValores As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdAreaValores.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdAreaValores As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodPeriodoAgregar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodPeriodoAgregar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnUFActual.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnUFActual As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCantidadCargas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCantidadCargas As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCantidadCargasEsp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCantidadCargasEsp As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCArgaUFModificar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCArgaUFModificar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCArgaEspecialUFModificar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCArgaEspecialUFModificar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCostoTutilarUFModificar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCostoTutilarUFModificar As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnChkActivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnChkActivo As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRetroactivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRetroactivo As Global.System.Web.UI.WebControls.HiddenField
End Class
