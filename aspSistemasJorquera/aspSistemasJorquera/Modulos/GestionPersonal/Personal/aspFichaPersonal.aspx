﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspFichaPersonal.aspx.vb" Inherits="aspSistemasJorquera.aspFichaPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
    <section class="content-header">
        <h1>
            <asp:Label ID="lbltitulo" runat="server" Text="Ficha Personal"></asp:Label>
        </h1>
        <div class="row">
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                    <asp:LinkButton ID="btnDatosContrato" Visible="false" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspFichaContrato.aspx" CssClass="btn bg-orange btn-block">Contrato <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>
            <div class="col-md-1 form-group pull-right">
                <label for="fname"
                    class="control-label col-form-label">
                </label>
                <div>

                    <asp:LinkButton ID="btnInstAsciadas" Visible="false" CssClass="btn bg-orange btn-block" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspInstAsociadas.aspx">Instituciones <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                </div>

            </div>

        </div>
        
          </ContentTemplate>
        </asp:UpdatePanel>
        <%--  <div class="breadcrumb">
            <div class="row">
                <div class="col-md-12 form-group">
                    <asp:LinkButton ID="btnInstAsciadas" runat="server" PostBackUrl="~/Modulos/GestionPersonal/Personal/aspInstAsociadas.aspx" CssClass="pull-right bg-orange" >Instituciones Asociadas <i class="fa fa-fw fa-angle-double-right"></i></asp:LinkButton>
                  
                </div>
            </div>
        </div>--%>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="box box-danger" data-select2-id="14">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos Personales</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>

                    <div class="box-body" style="" data-select2-id="13">
                        <div class="box box-widget widget-user-2">
                            <div class="widget-user-header bg-orange">
                                <div class="widget-user-image">
                                    <asp:Image ID="imgavatar" ImageUrl="/Imagen/Sfoto.png" runat="server" CssClass="img-circle" />
                                </div>

                            </div>
                        </div>
                        <br></br>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Rut</label>
                                    <div>
                                        <asp:TextBox ID="txtRut" runat="server" AutoCompleteType="Disabled" AutoPostBack="True" CssClass="form-control text-uppercase" MaxLength="12" placeholder="Rut..."></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Nombres</label>
                                    <div>
                                        <asp:TextBox ID="txtNombres" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Ape. Paterno</label>
                                    <div>
                                        <asp:TextBox ID="txtApePaterno" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Ape. Materno</label>
                                    <div>
                                        <asp:TextBox ID="txtApeMaterno" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Imagen</label>
                                    <div>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:FileUpload ID="uplCDocumento" runat="server" CssClass="form-control text-uppercase" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Genero</label>
                                    <div>
                                        <asp:DropDownList ID="cboGenero" runat="server" CssClass="form-control text-uppercase">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Fec. Nacto.</label>
                                    <div>
                                        <asp:TextBox ID="txtFecNacto" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase" TextMode="Date"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Est. Civil</label>
                                    <div>
                                        <asp:DropDownList ID="cboEstCivil" runat="server" CssClass="form-control text-uppercase">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Celular</label>
                                    <div>
                                        <asp:TextBox ID="txtCelular" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Tel. Fijo</label>
                                    <div>
                                        <asp:TextBox ID="txtTelFijo" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Mail</label>
                                    <div>
                                        <asp:TextBox ID="txtMail" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Nº Calzado</label>
                                    <div>
                                        <asp:TextBox ID="txtCalzado" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Talla Top</label>
                                    <div>
                                        <asp:TextBox ID="txtTallaTop" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Talla Pantalon</label>
                                    <div>
                                        <asp:TextBox ID="txtNumPantalon" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Peso</label>
                                    <div>
                                        <asp:TextBox ID="txtPeso" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Estatura</label>
                                    <div>
                                        <asp:TextBox ID="txtEstatura" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Nacionalidad</label>
                                    <div>
                                        <asp:DropDownList ID="cboNacionalidad" runat="server" CssClass="form-control text-uppercase">
                                            <asp:ListItem Value="1">Chilena</asp:ListItem>
                                            <asp:ListItem Value="2">Extranjero</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Des. Nacionalidad</label>
                                    <div>
                                        <asp:TextBox ID="txtDesNacionalidad" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label col-form-label" for="fname">
                                        Res. Trabajo</label>
                                    <div>
                                        <asp:TextBox ID="txtResTrabajo" runat="server" AutoCompleteType="Disabled" CssClass="form-control text-uppercase"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="row">

                    <div class="col-md-6">
                        <div class="box box-danger" data-select2-id="15">
                            <div class="box-header with-border">
                                <h3 class="box-title">Licencias / Credenciales</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="" data-select2-id="16">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Tipo</label>
                                            <div>
                                                <asp:DropDownList ID="cboTipoLic" runat="server" CssClass="form-control text-uppercase" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Fecha Venc.</label>
                                            <div>
                                                <asp:TextBox ID="txtFecVencLic" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                            <div>
                                                <asp:LinkButton ID="btnAgregarLic" runat="server" CssClass="btn bg-orange btn-block">Agregar</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-5">

                                        <asp:GridView ID="grdLicencias" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Tipo Licencia">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLicencia" runat="server" Text='<%# Bind("nom_tipo")%>' />
                                                        <asp:HiddenField ID="hdnIdLicencia" runat="server" Value='<%# Bind("id_licencia")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vencimiento">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="dtFechavencimiento" runat="server" CssClass="EstEditCtrGrilla" Height="20px" Text='<%# Bind("fec_vencimiento")%>' Width="80px" Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="9" CssClass="btn btn-sm btn-secondary fa fa-trash" ToolTip="Eliminar"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box box-danger" data-select2-id="17">
                            <div class="box-header with-border">
                                <h3 class="box-title">Grupo Familiar</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body" style="" data-select2-id="18">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Nombre</label>
                                            <div>
                                                <asp:TextBox ID="txtGrupoFamiliar" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">Parentesco</label>
                                            <div>
                                                <asp:DropDownList ID="cboParentescoGF" runat="server" CssClass="form-control text-uppercase"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                            <div>
                                                <asp:LinkButton ID="btnAgregarGF" runat="server" CssClass="btn bg-orange btn-block">Agregar</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-5">

                                        <asp:GridView ID="grdGrupoFamiliar" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Tipo Licencia">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLicencia" runat="server" Text='<%# Bind("nom_tipo")%>' />
                                                        <asp:HiddenField ID="hdnIdLicencia" runat="server" Value='<%# Bind("id_licencia")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Vencimiento">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="dtFechavencimiento" runat="server" CssClass="EstEditCtrGrilla" Height="20px" Text='<%# Bind("fec_vencimiento")%>' Width="80px" Enabled="false" />

                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="9" CssClass="btn btn-sm btn-secondary fa fa-trash" ToolTip="Eliminar"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Domicilio</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Region</label>
                                <div>
                                    <asp:DropDownList ID="cboRegion" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Provincia</label>
                                <div>
                                    <asp:DropDownList ID="cboProvincia" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Ciudad</label>
                                <div>
                                    <asp:DropDownList ID="cboCiudad" runat="server" CssClass="form-control text-uppercase">
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-1">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Nom. Calle</label>
                                <div>
                                    <asp:TextBox ID="txtNomCalle" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Nº Domicilio</label>
                                <div>
                                    <asp:TextBox ID="txtNumDomicilio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Nom. Poblacion</label>
                                <div>
                                    <asp:TextBox ID="txtPoblacion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Des. Referencia</label>
                                <div>
                                    <asp:TextBox ID="txtDesRef" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        <div class="box box-danger">
                            <div class="box-header">
                                <h3 class="box-title">Datos Contacto</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                                </div>
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Nom Contacto</label>
                                        <div>
                                            <asp:TextBox ID="txtNomContacto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Fono Contacto</label>
                                        <div>
                                            <asp:TextBox ID="txtFonoContacto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Parentesco</label>
                                        <div>
                                            <asp:TextBox ID="txtParentesco" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-danger">
                            <div class="box-header">
                                <h3 class="box-title">Otros</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                                </div>
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">F.F.A.A</label>
                                        <div>
                                            <asp:DropDownList ID="cboFFAA" runat="server" CssClass="form-control text-uppercase">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="fname" class="control-label col-form-label">Des. F.F.A.A</label>
                                        <div>
                                            <asp:TextBox ID="txtDesFFAA" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">Discapacidad</label>
                                        <div>
                                            <asp:DropDownList ID="cboDiscapacidad" runat="server" CssClass="form-control text-uppercase">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                        <div>
                                            <asp:TextBox ID="txtDesDiscapacidad" runat="server" Visible="False" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Información Academica</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Estudios</label>
                                <div>
                                    <asp:DropDownList ID="cboEstudios" runat="server" CssClass="form-control text-uppercase"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Estado Estudios</label>
                                <div>
                                    <asp:DropDownList ID="cboEstEstudios" runat="server" CssClass="form-control text-uppercase">
                                        <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                        <asp:ListItem Value="1">Completo</asp:ListItem>
                                        <asp:ListItem Value="2">Incompleto</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">Años Cursados</label>
                                <div>
                                    <asp:TextBox ID="txtAniosEstudios" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2 form-group">
                                <label for="fname" class="control-label col-form-label">Des. Titulo</label>
                                <div>
                                    <asp:TextBox ID="txtDesTitulo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1 form-group">
                                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                <div>
                                    <asp:LinkButton ID="btnAgregarEstudios" runat="server" CssClass="btn bg-orange btn-block">Agregar</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">

                                <asp:GridView ID="grdEstudios" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Estudios">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstudios" runat="server" Text='<%# Bind("nom_estudio")%>' />
                                                <asp:HiddenField ID="hdn_id_estudio" runat="server" Value='<%# Bind("id_estudio")%>' />
                                                <asp:HiddenField ID="hdnIdEstado" runat="server" Value='<%# Bind("id_estado")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("Estado")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="80px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Años Cursados">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAnios" runat="server" Text='<%# Bind("num_anios")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="100px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("des_titulo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="200px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar0" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="9" CssClass="btn btn-sm btn-secondary fa fa-trash" ToolTip="Eliminar"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Top" Width="20px" Wrap="False" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 pull-right">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnGuardar" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hdnId" runat="server" Value="0" />
        <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
        <asp:HiddenField ID="hdnUrlFoto" runat="server" Value="0" />

        <asp:Label ID="lblActivo" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>

                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">
                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="loader">
                                        <div class="loader-inner">
                                            <div class="loading one"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading two"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading three"></div>
                                        </div>
                                        <div class="loader-inner">
                                            <div class="loading four"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>
</asp:Content>
