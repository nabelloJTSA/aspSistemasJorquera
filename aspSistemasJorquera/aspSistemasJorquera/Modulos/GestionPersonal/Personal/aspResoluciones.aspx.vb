﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspResoluciones
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarEmpresa()
                Call CargarSucursalFaena()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Resolucion 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSucursalFaena()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Resolucion 'CCS','" & cboEmpresa.SelectedValue & "'"
            cboFaena.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFaena.DataTextField = "nom_sucursal"
            cboFaena.DataValueField = "id_sucursal"
            cboFaena.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSucursalFaena", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarResolucionesEmpresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Resolucion 'GE','" & cboEmpresa.SelectedValue & "'"
            grdResoluciones.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdResoluciones.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarResolucionesEmpresa", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarResolucionesSucursal()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Resolucion 'GS','" & cboEmpresa.SelectedValue & "','" & cboFaena.SelectedValue & "'"
            grdResoluciones.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdResoluciones.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarResolucionesSucursal", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_Resolucion"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "I"
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
        comando.Parameters.Add("@cod_faena", SqlDbType.NVarChar)
        comando.Parameters("@cod_faena").Value = cboFaena.SelectedValue
        comando.Parameters.Add("@num_resolucion", SqlDbType.NVarChar)
        comando.Parameters("@num_resolucion").Value = txtNResolucion.Text
        comando.Parameters.Add("@num_promedio", SqlDbType.NVarChar)
        comando.Parameters("@num_promedio").Value = txtPromedio.Text
        comando.Parameters.Add("@fec_emision", SqlDbType.NVarChar)
        comando.Parameters("@fec_emision").Value = txtFechaEmision.Text
        comando.Parameters.Add("@fec_vencimiento", SqlDbType.NVarChar)
        comando.Parameters("@fec_vencimiento").Value = txtFechaVencimiento.Text
        comando.Parameters.Add("@des_observacion", SqlDbType.NVarChar)
        comando.Parameters("@des_observacion").Value = txtObservacion.Text
        comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
        comando.Parameters("@usr_add").Value = Session("id_usu_tc")
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_Resolucion"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "U"
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
        comando.Parameters.Add("@cod_faena", SqlDbType.NVarChar)
        comando.Parameters("@cod_faena").Value = cboFaena.SelectedValue
        comando.Parameters.Add("@num_resolucion", SqlDbType.NVarChar)
        comando.Parameters("@num_resolucion").Value = txtNResolucion.Text
        comando.Parameters.Add("@num_promedio", SqlDbType.NVarChar)
        comando.Parameters("@num_promedio").Value = txtPromedio.Text
        comando.Parameters.Add("@fec_emision", SqlDbType.NVarChar)
        comando.Parameters("@fec_emision").Value = txtFechaEmision.Text
        comando.Parameters.Add("@fec_vencimiento", SqlDbType.NVarChar)
        comando.Parameters("@fec_vencimiento").Value = txtFechaVencimiento.Text
        comando.Parameters.Add("@des_observacion", SqlDbType.NVarChar)
        comando.Parameters("@des_observacion").Value = txtObservacion.Text
        comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
        comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = hdnIdResolucion.Value
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarSucursalFaena()
        Call CargarResolucionesEmpresa()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtFechaVencimiento.Text < txtFechaEmision.Text Then
            MessageBox("Resoluciones", "La fecha de vencimiento ni puede ser menor a la de emision", Page, Master, "W")
        Else
            If hdnActualizar.Value = True Then
                Call Actualizar()
                hdnActualizar.Value = 0
                Call CargarResolucionesSucursal()
                MessageBox("Resoluciones", "Registro Actualizado", Page, Master, "S")
            Else
                Call Guardar()
                Call CargarResolucionesSucursal()
                MessageBox("Resoluciones", "Registro Guardado", Page, Master, "S")
            End If
        End If
    End Sub

    Protected Sub cboFaena_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFaena.SelectedIndexChanged
        If hdnActualizar.Value = 0 Then
            Call CargarResolucionesSucursal()
        End If
    End Sub

    Protected Sub grdResoluciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdResoluciones.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdResoluciones.SelectedRow

            cboEmpresa.SelectedValue = CType(row.FindControl("lblCodEmpresa"), Label).Text
            cboFaena.SelectedValue = CType(row.FindControl("lblCodFaena"), Label).Text
            txtFechaEmision.Text = CType(row.FindControl("lblFecEmision"), Label).Text
            txtNResolucion.Text = CType(row.FindControl("lblNResolucion"), Label).Text
            txtPromedio.Text = CType(row.FindControl("lblNPromedio"), Label).Text
            txtFechaVencimiento.Text = CType(row.FindControl("lblVencimiento"), Label).Text
            txtObservacion.Text = CType(row.FindControl("lblObservacion"), Label).Text
            hdnIdResolucion.Value = CType(row.FindControl("hdn_id"), HiddenField).Value
            hdnActualizar.Value = 1
        Catch ex As Exception
            MessageBoxError("grdMedioPago_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        cboEmpresa.ClearSelection()
        cboFaena.ClearSelection()
        txtPromedio.Text = ""
        txtObservacion.Text = ""
        txtNResolucion.Text = ""
        txtFechaEmision.Text = ""
        txtFechaVencimiento.Text = ""
        hdnActualizar.Value = 0
        hdnIdResolucion.Value = 0
    End Sub
End Class