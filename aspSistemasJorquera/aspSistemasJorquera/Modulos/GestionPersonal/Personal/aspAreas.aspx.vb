﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspAreas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvAreas.ActiveViewIndex = 0
                lbltituloP.Text = "Áreas"
                Call CargarEmpresas()
                Call CargarGrilla()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAreas()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHH_Subarea 'CCA','" & cboEmpresaSub.SelectedValue & "'"
            cboArea.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboArea.DataTextField = "nom_area"
            cboArea.DataValueField = "cod_area"
            cboArea.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAreas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaSUB()
        Try
            Dim strNomTabla As String = "RRHHMae_Area"
            Dim strSQL As String = "Exec mant_RRHH_Subarea 'G','" & cboArea.SelectedValue & "'"
            grdSubAreas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdSubAreas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_Area"
            Dim strSQL As String = "Exec mant_RRHH_area 'G'"
            grdAreas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdAreas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresas()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHH_area 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()

            cboEmpresaSub.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaSub.DataTextField = "nom_empresa"
            cboEmpresaSub.DataValueField = "cod_empresa"
            cboEmpresaSub.DataBind()

            cboEmpresaDpt.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaDpt.DataTextField = "nom_empresa"
            cboEmpresaDpt.DataValueField = "cod_empresa"
            cboEmpresaDpt.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresas", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_area"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_area", SqlDbType.NVarChar)
            comando.Parameters("@nom_area").Value = Trim(txtNomArea.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaSub.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaSUB()
            Call Limpiarsub()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_area"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_area", SqlDbType.NVarChar)
            comando.Parameters("@nom_area").Value = Trim(txtNomArea.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaSub.SelectedValue
            comando.Parameters.Add("@cod_area", SqlDbType.NVarChar)
            comando.Parameters("@cod_area").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdAreas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdAreas.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdAreas.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_emp As HiddenField = CType(row.FindControl("hdn_emp"), HiddenField)
            Dim obj_NomArea As Label = CType(row.FindControl("lblNombreArea"), Label)
            hdnId.Value = obj_id.Value
            cboEmpresa.SelectedValue = obj_emp.Value
            txtNomArea.Text = obj_NomArea.Text
            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdAreas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        cboEmpresa.ClearSelection()
        txtNomArea.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdAreas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdAreas.PageIndexChanging
        Try
            grdAreas.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdAreas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub


    '***************************SUBAREA
    Private Sub Guardarsub()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_Subarea"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_subarea", SqlDbType.NVarChar)
            comando.Parameters("@nom_subarea").Value = Trim(txtNom.Text)
            comando.Parameters.Add("@cod_area", SqlDbType.NVarChar)
            comando.Parameters("@cod_area").Value = cboArea.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaSUB()
            Call Limpiarsub()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizarsub()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_Subarea"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_subarea", SqlDbType.NVarChar)
            comando.Parameters("@nom_subarea").Value = Trim(txtNom.Text)
            comando.Parameters.Add("@cod_area", SqlDbType.NVarChar)
            comando.Parameters("@cod_area").Value = cboArea.SelectedValue
            comando.Parameters.Add("@cod_subarea", SqlDbType.NVarChar)
            comando.Parameters("@cod_subarea").Value = hdnIdSub.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaSUB()
            Call Limpiarsub()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdSubAreas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSubAreas.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdSubAreas.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_area As HiddenField = CType(row.FindControl("hdn_area"), HiddenField)

            Dim obj_NomArea As Label = CType(row.FindControl("lblNombreSubArea"), Label)

            hdnIdSub.Value = obj_id.Value
            cboArea.SelectedValue = obj_area.Value
            txtNom.Text = obj_NomArea.Text


            hdnActivoSub.Value = 1
        Catch ex As Exception
            MessageBoxError("grdSubAreas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiarsub()
        cboArea.ClearSelection()
        txtNom.Text = ""
        hdnActivoSub.Value = 0
        Call CargarGrillaSUB()
    End Sub

    Private Sub grdSubAreas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdSubAreas.PageIndexChanging
        Try
            grdSubAreas.PageIndex = e.NewPageIndex
            Call CargarGrillaSUB()
        Catch ex As Exception
            MessageBoxError("grdSubAreas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarsub_Click(sender As Object, e As EventArgs) Handles btnGuardarSub.Click
        If hdnActivoSub.Value = 0 Then
            Call Guardarsub()
        Else
            Call Actualizarsub()
        End If
    End Sub

    Protected Sub btnCancelarsub_Click(sender As Object, e As EventArgs) Handles btnCancelarSub.Click
        Call Limpiarsub()
    End Sub

    Protected Sub cboArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboArea.SelectedIndexChanged
        Call CargarGrillaSUB()
    End Sub

    Protected Sub cboEmpresasub_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaSub.SelectedIndexChanged
        Call CargarAreas()
        Call CargarGrillaSUB()
    End Sub

    Protected Sub btnAreas_Click(sender As Object, e As EventArgs) Handles btnAreas.Click
        MtvAreas.ActiveViewIndex = 0
        lbltituloP.Text = "Áreas"
    End Sub

    Protected Sub btbSubareas_Click(sender As Object, e As EventArgs) Handles btbSubareas.Click
        MtvAreas.ActiveViewIndex = 1
        lbltituloP.Text = "Subáreas"
        Call CargarAreas()
        Call CargarGrillaSUB()
    End Sub

    '************************************DEPTOS

    Private Sub GuardarDpt()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_departamento"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_departamento", SqlDbType.NVarChar)
            comando.Parameters("@nom_departamento").Value = Trim(txtNomDpt.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaDpt.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaDPT()
            Call LimpiarDpt()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarDpt()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_Subarea"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_departamento", SqlDbType.NVarChar)
            comando.Parameters("@nom_departamento").Value = Trim(txtNomDpt.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaDpt.SelectedValue
            comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
            comando.Parameters("@cod_departamento").Value = hdnIdDpt.Value
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaDPT()
            Call LimpiarDpt()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdDepto.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdDepto.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_area As HiddenField = CType(row.FindControl("hdn_sub_area"), HiddenField)

            Dim obj_NomDepto As Label = CType(row.FindControl("lblNombreDepto"), Label)

            hdnIdDpt.Value = obj_id.Value
            cboEmpresaDpt.SelectedValue = obj_area.Value
            txtNomDpt.Text = obj_NomDepto.Text

            hdnActivoDpt.Value = 1
        Catch ex As Exception
            MessageBoxError("grdDepto_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarDpt()
        cboEmpresaDpt.ClearSelection()
        txtNomDpt.Text = ""
        hdnActivoDpt.Value = 0
        Call CargarGrillaDPT()
    End Sub

    Private Sub grdDepto_SelectedIndexChanged(sender As Object, e As GridViewPageEventArgs) Handles grdDepto.PageIndexChanging
        Try
            grdDepto.PageIndex = e.NewPageIndex
            Call CargarGrillaDPT()
        Catch ex As Exception
            MessageBoxError("grdDepto_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarDpt_Click(sender As Object, e As EventArgs) Handles btnGuardarDpt.Click
        If hdnActivoDpt.Value = 0 Then
            Call GuardarDpt()
        Else
            Call ActualizarDpt()
        End If
    End Sub

    Protected Sub btnCancelarDpt_Click(sender As Object, e As EventArgs) Handles btnCancelarDpt.Click
        Call LimpiarDpt()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrillaDPT()
    End Sub

    Private Sub CargarGrillaDPT()
        Try
            Dim strNomTabla As String = "RRHHMae_departamento"
            Dim strSQL As String = "Exec mant_RRHH_departamento 'G','" & cboEmpresa.SelectedValue & "'"
            grdDepto.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub



    Protected Sub btnDetps_Click(sender As Object, e As EventArgs) Handles btnDetps.Click
        MtvAreas.ActiveViewIndex = 2
        lbltituloP.Text = "Departamentos"
        Call CargarAreas()
        Call CargarGrillaDPT()
    End Sub
End Class