﻿
Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspTipoContrato
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "REMMae_tipo_contrato"
            Dim strSQL As String = "Exec mant_RRHH_tipo_contrato 'G'"
            grdContratos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdContratos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tipo_contrato"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_contrato", SqlDbType.NVarChar)
            comando.Parameters("@nom_contrato").Value = Trim(txtTipo.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tipo_contrato"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_contrato", SqlDbType.NVarChar)
            comando.Parameters("@nom_contrato").Value = Trim(txtTipo.Text)
            comando.Parameters.Add("@cod_contrato", SqlDbType.NVarChar)
            comando.Parameters("@cod_contrato").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdContratos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdContratos.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdContratos.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_Nom As Label = CType(row.FindControl("lbltipoContrato"), Label)
            hdnId.Value = obj_id.Value
            txtTipo.Text = obj_Nom.Text
            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdBanco_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtTipo.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdContratos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdContratos.PageIndexChanging
        Try
            grdContratos.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdContratos_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

End Class
