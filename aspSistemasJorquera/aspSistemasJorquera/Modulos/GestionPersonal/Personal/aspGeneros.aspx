﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspGeneros.aspx.vb" Inherits="aspSistemasJorquera.aspGeneros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text="Género"></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>

            <section class="content">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Estado Civil</label>
                            <div>
                                <asp:TextBox ID="txtGenero" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>

                    </div>


                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-1 form-group ">
                            <div>
                            </div>
                        </div>

                        <div class="col-md-1 form-group ">
                            <div>
                                <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>

                        <div class="col-md-1 form-group ">
                            <div>
                                <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                            </div>
                        </div>

                    </div>

                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3">

                            <asp:GridView ID="grdGenero" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                    </asp:CommandField>

                                    <asp:TemplateField HeaderText="Gènero">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("nom_genero")%>' />
                                            <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_genero")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>

                        </div>

                    </div>
                </asp:Panel>

                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />

            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
