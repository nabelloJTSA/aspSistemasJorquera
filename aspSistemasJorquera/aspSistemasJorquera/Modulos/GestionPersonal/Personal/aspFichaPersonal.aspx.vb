﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspFichaPersonal
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargaGenero()
                Call CargaEstCivil()
                Call CargaRegion()
                Call CargaProvincia()
                Call CargaCiudad()
                Call CargaLicencias()
                Call CargaEstudios()
                Call CargaGrupoFamiliar()
                Session("dtLicencias") = CrearTablaDetLicencias()
                Session("dtFamilia") = CrearTablaDetFamilia()
                Session("dtEstudios") = CrearTablaDetEstudios()

                If Session("rut_persona") = "" Then
                    Session("rut_persona") = ""
                    Session("nom_persona") = ""
                    Call Limpiar()
                Else
                    Call BuscarDatos()
                    Call ValidarPermisosDeptos()
                End If
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Function CrearTablaDetLicencias() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("rut_personal", Type.GetType("System.String"))
            dtTemporal.Columns.Add("fec_vencimiento", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_licencia", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_tipo", Type.GetType("System.String"))
        Catch ex As Exception
            MessageBoxError("CrearTablaDetLicencias", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Function CrearTablaDetFamilia() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("nom_persona", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_grupo", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_grupo", Type.GetType("System.String"))

        Catch ex As Exception
            MessageBoxError("CrearTablaDetFamilia", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Function CrearTablaDetEstudios() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("nom_estudio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("Estado", Type.GetType("System.String"))
            dtTemporal.Columns.Add("num_anios", Type.GetType("System.String"))
            dtTemporal.Columns.Add("des_titulo", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_estudio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_estado", Type.GetType("System.String"))
        Catch ex As Exception
            MessageBoxError("CrearTablaDetEstudios", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Sub CargaGenero()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCG'"
            cboGenero.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboGenero.DataTextField = "nom_genero"
            cboGenero.DataValueField = "id_genero"
            cboGenero.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaGenero", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaEstCivil()
        Try
            Dim strNomTablaR As String = "REMMae_estado_civil"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCE'"
            cboEstCivil.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEstCivil.DataTextField = "nom_estado_civil"
            cboEstCivil.DataValueField = "cod_estado_civil"
            cboEstCivil.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaCCAF", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaRegion()
        Try
            Dim strNomTablaR As String = "mae_regiones"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCR'"
            cboRegion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboRegion.DataTextField = "nom_region"
            cboRegion.DataValueField = "cod_region"
            cboRegion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaRegion", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaProvincia()
        Try
            Dim strNomTablaR As String = "mae_provincias"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCP', '" & cboRegion.SelectedValue & "'"
            cboProvincia.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboProvincia.DataTextField = "nom_provincia"
            cboProvincia.DataValueField = "cod_provincia"
            cboProvincia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaProvincia", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaCiudad()
        Try
            Dim strNomTablaR As String = "mae_comunas"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCC','" & cboProvincia.SelectedValue & "'"
            cboCiudad.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCiudad.DataTextField = "nom_comuna"
            cboCiudad.DataValueField = "cod_comuna"
            cboCiudad.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaCiudad", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaEstudios()
        Try
            Dim strNomTablaR As String = "RRHHMae_estudios"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CCS'"
            cboEstudios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEstudios.DataTextField = "nom_estudio"
            cboEstudios.DataValueField = "id_estudio"
            cboEstudios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaEstudios", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaLicencias()
        Try
            Dim strNomTablaR As String = "RRHHMae_tipo_discapacidad"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CHL'"
            cboTipoLic.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoLic.DataTextField = "nom_tipo"
            cboTipoLic.DataValueField = "id_licencia"
            cboTipoLic.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaDiscapacidades", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaGrupoFamiliar()
        Try
            Dim strNomTablaR As String = "RRHHMae_tipo_discapacidad"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_personal 'CGF'"
            cboParentescoGF.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboParentescoGF.DataTextField = "nom_grupo"
            cboParentescoGF.DataValueField = "id_grupo"
            cboParentescoGF.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaGrupoFamiliar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboRegion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRegion.SelectedIndexChanged
        Call CargaProvincia()
        Call CargaCiudad()
    End Sub

    Protected Sub cboProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProvincia.SelectedIndexChanged
        Call CargaCiudad()
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Session("rut_persona") = Trim(txtRut.Text)
                Call BuscarDatos()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Session("rut_persona") = Trim(txtRut.Text)
                Call BuscarDatos()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
        UpdatePanel3.Update()
    End Sub

    Protected Sub cboDiscapacidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDiscapacidad.SelectedIndexChanged
        If cboDiscapacidad.SelectedValue = 0 Then
            txtDesDiscapacidad.Visible = False
        Else
            txtDesDiscapacidad.Visible = True
        End If
    End Sub

    Private Sub grdLicencias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLicencias.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "9" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdLicencias.Rows(intRow)
                Dim ocar As New DataTable
                ocar = Session("dtLicencias")
                ocar.Rows(intRow).Delete()
                grdLicencias.DataSource = ocar
                grdLicencias.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdLicencias_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnAgregarLic_Click(sender As Object, e As EventArgs) Handles btnAgregarLic.Click
        If cboTipoLic.SelectedValue = 0 Or txtFecVencLic.Text = "" Then
            MessageBox("Agregar Licecias", "Ingrese Datos", Page, Master, "W")
        Else
            Call GuardarDetTemp(Trim(txtRut.Text), txtFecVencLic.Text, cboTipoLic.SelectedValue, cboTipoLic.SelectedItem.ToString)
        End If
    End Sub

    Private Sub GuardarDetTemp(ByVal rut As String, ByVal fec_venc As String, ByVal cbo_licencia As String, ByVal nom_tipo As String)
        Dim ocar As New DataTable
        ocar = Session("dtLicencias")
        With ocar.Rows.Add
            .Item("rut_personal") = Trim(rut)
            .Item("fec_vencimiento") = Trim(CDate(fec_venc))
            .Item("id_licencia") = cbo_licencia
            .Item("nom_tipo") = nom_tipo
        End With
        grdLicencias.DataSource = ocar
        grdLicencias.DataBind()
    End Sub

    Protected Sub cboFFAA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFFAA.SelectedIndexChanged
        If cboFFAA.SelectedValue = 0 Then
            txtDesFFAA.Enabled = False
        Else
            txtDesFFAA.Enabled = True
        End If
    End Sub

    Protected Sub btnAgregarGF_Click(sender As Object, e As EventArgs) Handles btnAgregarGF.Click
        If txtGrupoFamiliar.Text = "" Or cboParentescoGF.SelectedValue = 0 Then
            MessageBox("Agregar Grupo Familiar", "Ingrese Datos", Page, Master, "W")
        Else
            Call GuardarDetTempFamilia(Trim(txtGrupoFamiliar.Text), cboParentescoGF.SelectedItem.ToString, cboParentescoGF.SelectedValue)
        End If
    End Sub

    Private Sub GuardarDetTempFamilia(ByVal gf As String, ByVal parentesco As String, ByVal id_parentesco As String)
        Dim ocar As New DataTable
        ocar = Session("dtFamilia")
        With ocar.Rows.Add
            .Item("nom_persona") = Trim(gf)
            .Item("nom_grupo") = Trim(parentesco)
            .Item("id_grupo") = Trim(id_parentesco)
        End With
        grdGrupoFamiliar.DataSource = ocar
        grdGrupoFamiliar.DataBind()
    End Sub

    Private Sub grdGrupoFamiliar_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdGrupoFamiliar.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "9" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdGrupoFamiliar.Rows(intRow)
                Dim ocar As New DataTable
                ocar = Session("dtFamilia")
                ocar.Rows(intRow).Delete()
                grdGrupoFamiliar.DataSource = ocar
                grdGrupoFamiliar.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdGrupoFamiliar_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnAgregarEstudios_Click(sender As Object, e As EventArgs) Handles btnAgregarEstudios.Click
        If cboEstudios.SelectedValue = 0 Or cboEstEstudios.SelectedValue = 0 Or Trim(txtAniosEstudios.Text) = "" Or Trim(txtDesTitulo.Text) = "" Then
            MessageBox("Guardar Estudios", "Seleccione datos a registrar", Page, Master, "W")
        Else
            Call GuardarDetTempEstudios(cboEstudios.SelectedItem.ToString, cboEstEstudios.SelectedItem.ToString, Trim(txtAniosEstudios.Text), Trim(txtDesTitulo.Text), cboEstudios.SelectedValue, cboEstEstudios.SelectedValue)
        End If
    End Sub

    Private Sub GuardarDetTempEstudios(ByVal estudio As String, ByVal estado As String, ByVal anios As String, ByVal des As String, ByVal id As String, ByVal id_est As String)
        Dim ocar As New DataTable
        ocar = Session("dtEstudios")
        With ocar.Rows.Add
            .Item("nom_estudio") = Trim(estudio)
            .Item("Estado") = Trim(estado)
            .Item("num_anios") = Trim(anios)
            .Item("des_titulo") = Trim(des)
            .Item("id_estudio") = Trim(id)
            .Item("id_estado") = Trim(id_est)
        End With
        grdEstudios.DataSource = ocar
        grdEstudios.DataBind()
    End Sub

    Private Sub grdEstudios_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdEstudios.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "9" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdEstudios.Rows(intRow)
                Dim ocar As New DataTable
                ocar = Session("dtEstudios")
                ocar.Rows(intRow).Delete()
                grdEstudios.DataSource = ocar
                grdEstudios.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("grdEstudios_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarLicencias()
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Dim cmdTemporal As SqlCommand = cnxBaseDatos.CreateCommand()
            Dim trsBaseDatos As SqlTransaction
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Guardar_Det")
            cmdTemporal.Connection = cnxBaseDatos
            cmdTemporal.Transaction = trsBaseDatos
            Try
                For iRow = 0 To grdLicencias.Rows.Count - 1
                    Dim obj_fec_venc As TextBox = CType(grdLicencias.Rows(iRow).FindControl("dtFechavencimiento"), TextBox)
                    Dim obj_id_lic As HiddenField = CType(grdLicencias.Rows(iRow).FindControl("hdnIdLicencia"), HiddenField)

                    Dim conx As New SqlConnection(strCnx)
                    Dim comando As New SqlCommand
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "mant_RRHH_ficha_personal"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "ILC"
                    comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                    comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
                    comando.Parameters.Add("@id_licencia", SqlDbType.NVarChar)
                    comando.Parameters("@id_licencia").Value = obj_id_lic.Value
                    comando.Parameters.Add("@fec_vencimiento", SqlDbType.NVarChar)
                    comando.Parameters("@fec_vencimiento").Value = obj_fec_venc.Text
                    comando.Connection = conx
                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()
                Next
            Catch ex As Exception
                MessageBoxError("GuardarLicencias", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub GuardarGrupoFamiliar()
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Dim cmdTemporal As SqlCommand = cnxBaseDatos.CreateCommand()
            Dim trsBaseDatos As SqlTransaction
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Guardar_Det")
            cmdTemporal.Connection = cnxBaseDatos
            cmdTemporal.Transaction = trsBaseDatos
            Try
                For iRow = 0 To grdGrupoFamiliar.Rows.Count - 1

                    Dim obj_id_parentesco As HiddenField = CType(grdGrupoFamiliar.Rows(iRow).FindControl("hdniD"), HiddenField)
                    Dim obj_nombre As Label = CType(grdGrupoFamiliar.Rows(iRow).FindControl("lblNombre"), Label)

                    Dim conx As New SqlConnection(strCnx)
                    Dim comando As New SqlCommand
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "mant_RRHH_ficha_personal"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "IFG"
                    comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                    comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
                    comando.Parameters.Add("@id_grupo", SqlDbType.NVarChar)
                    comando.Parameters("@id_grupo").Value = obj_id_parentesco.Value
                    comando.Parameters.Add("@nom_persona", SqlDbType.NVarChar)
                    comando.Parameters("@nom_persona").Value = obj_nombre.Text
                    comando.Connection = conx
                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()
                Next
            Catch ex As Exception
                MessageBoxError("GuardarGrupoFamiliar", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub GuardarEstudios()
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Dim cmdTemporal As SqlCommand = cnxBaseDatos.CreateCommand()
            Dim trsBaseDatos As SqlTransaction
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Guardar_Det")
            cmdTemporal.Connection = cnxBaseDatos
            cmdTemporal.Transaction = trsBaseDatos
            Try
                For iRow = 0 To grdEstudios.Rows.Count - 1

                    Dim obj_id_estudio As HiddenField = CType(grdEstudios.Rows(iRow).FindControl("hdn_id_estudio"), HiddenField)
                    Dim obj_id_est_estudio As HiddenField = CType(grdEstudios.Rows(iRow).FindControl("hdnIdEstado"), HiddenField)
                    Dim obj_anios As Label = CType(grdEstudios.Rows(iRow).FindControl("lblAnios"), Label)
                    Dim obj_des_titulo As Label = CType(grdEstudios.Rows(iRow).FindControl("lblDescripcion"), Label)

                    Dim conx As New SqlConnection(strCnx)
                    Dim comando As New SqlCommand
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "mant_RRHH_ficha_personal"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "IFE"
                    comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                    comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
                    comando.Parameters.Add("@id_estudio", SqlDbType.NVarChar)
                    comando.Parameters("@id_estudio").Value = obj_id_estudio.Value
                    comando.Parameters.Add("@id_estado", SqlDbType.NVarChar)
                    comando.Parameters("@id_estado").Value = obj_id_est_estudio.Value
                    comando.Parameters.Add("@num_anios", SqlDbType.NVarChar)
                    comando.Parameters("@num_anios").Value = obj_anios.Text
                    comando.Parameters.Add("@des_titulo", SqlDbType.NVarChar)
                    comando.Parameters("@des_titulo").Value = obj_des_titulo.Text
                    comando.Connection = conx
                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()
                Next
            Catch ex As Exception
                MessageBoxError("GuardarEstudios", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub EliminarNubs()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_ficha_personal"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "DTN"
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = Trim(txtRut.Text)
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub BuscarDatos()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec mant_RRHH_ficha_personal 'G', '" & Session("rut_persona") & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                txtRut.Text = rdoReader(0).ToString
                txtNombres.Text = rdoReader(1).ToString
                txtApePaterno.Text = rdoReader(2).ToString
                txtApeMaterno.Text = rdoReader(3).ToString

                Session("rut_persona") = Trim(txtRut.Text)
                Session("nom_persona") = Trim(UCase(txtNombres.Text)) & " " & Trim(UCase(txtApePaterno.Text)) & " " & Trim(UCase(txtApeMaterno.Text))

                cboGenero.SelectedValue = rdoReader(4).ToString
                txtFecNacto.Text = CDate(rdoReader(6).ToString)
                cboEstCivil.SelectedValue = rdoReader(8).ToString
                txtNomCalle.Text = rdoReader(10).ToString
                txtPoblacion.Text = rdoReader(11).ToString
                txtNumDomicilio.Text = rdoReader(12).ToString
                txtDesRef.Text = rdoReader(13).ToString
                cboRegion.SelectedValue = rdoReader(14).ToString
                Call CargaProvincia()
                cboProvincia.SelectedValue = rdoReader(16).ToString
                Call CargaCiudad()
                cboCiudad.SelectedValue = rdoReader(18).ToString
                cboNacionalidad.SelectedValue = rdoReader(20).ToString
                txtDesNacionalidad.Text = rdoReader(21).ToString
                txtCelular.Text = rdoReader(22).ToString
                txtTelFijo.Text = rdoReader(23).ToString
                txtNomContacto.Text = rdoReader(24).ToString
                txtFonoContacto.Text = rdoReader(25).ToString
                txtParentesco.Text = rdoReader(26).ToString
                If rdoReader(27).ToString = True Then
                    cboFFAA.SelectedValue = 1
                    txtDesFFAA.Enabled = True
                Else
                    cboFFAA.SelectedValue = 0
                    txtDesFFAA.Enabled = False
                End If
                txtDesFFAA.Text = rdoReader(28).ToString
                If rdoReader(29).ToString = True Then
                    cboDiscapacidad.SelectedValue = 1
                    txtDesDiscapacidad.Visible = True
                Else
                    cboDiscapacidad.SelectedValue = 0
                    txtDesDiscapacidad.Visible = False
                End If
                txtDesDiscapacidad.Text = rdoReader(30).ToString

                txtMail.Text = rdoReader(31).ToString
                txtCalzado.Text = rdoReader(32).ToString
                txtNumPantalon.Text = rdoReader(33).ToString
                txtTallaTop.Text = rdoReader(34).ToString
                txtResTrabajo.Text = rdoReader(35).ToString
                txtPeso.Text = rdoReader(37).ToString
                txtEstatura.Text = rdoReader(38).ToString
                Call CargarGrillaLicencias()
                For Each rowTEMP As GridViewRow In grdLicencias.Rows
                    Dim obj_IdLicencia As HiddenField = CType(rowTEMP.FindControl("hdnIdLicencia"), HiddenField)
                    Dim obj_Id_itemTemp As HiddenField = CType(rowTEMP.FindControl("hdnIdItem"), HiddenField)
                    Dim obj_FecVenc As TextBox = CType(rowTEMP.FindControl("dtFechavencimiento"), TextBox)
                    Dim obj_TipoLic As Label = CType(rowTEMP.FindControl("lblLicencia"), Label)
                    Call GuardarDetTemp(Trim(txtRut.Text), obj_FecVenc.Text, obj_IdLicencia.Value, obj_TipoLic.Text)
                Next

                Call CargarGrillaGrupoFamiliar()
                For Each rowTEMP As GridViewRow In grdGrupoFamiliar.Rows
                    Dim obj_IdParentesco As HiddenField = CType(rowTEMP.FindControl("hdniD"), HiddenField)
                    Dim obj_NombreGF As Label = CType(rowTEMP.FindControl("lblNombre"), Label)
                    Dim obj_NomParentesco As Label = CType(rowTEMP.FindControl("lblParentesco"), Label)
                    Call GuardarDetTempFamilia(Trim(obj_NombreGF.Text), obj_NomParentesco.Text, obj_IdParentesco.Value)
                Next

                Call CargarGrillaEstudios()
                For Each rowTEMP As GridViewRow In grdEstudios.Rows
                    Dim obj_IdEstudio As HiddenField = CType(rowTEMP.FindControl("hdn_id_estudio"), HiddenField)
                    Dim obj_IdEstEstudio As HiddenField = CType(rowTEMP.FindControl("hdnIdEstado"), HiddenField)
                    Dim obj_NomEstudios As Label = CType(rowTEMP.FindControl("lblEstudios"), Label)
                    Dim obj_Anios As Label = CType(rowTEMP.FindControl("lblAnios"), Label)
                    Dim obj_Des As Label = CType(rowTEMP.FindControl("lblDescripcion"), Label)
                    Dim obj_Estado As Label = CType(rowTEMP.FindControl("lblEstado"), Label)
                    Call GuardarDetTempEstudios(obj_NomEstudios.Text, obj_Estado.Text, obj_Anios.Text, obj_Des.Text, obj_IdEstudio.Value, obj_IdEstEstudio.Value)
                Next

                Dim url As String = rdoReader(36).ToString
                hdnUrlFoto.Value = url
                ' Dim urlcompleta As String = "ftp://usarioftp:Ftp2018.@192.168.10.28/RRHH/ImagenFicha/Doctos_" & Trim(txtRut.Text) & "/" & url
                'imgavatar.ImageUrl = urlcompleta
                '200.27.135.170

                If hdnUrlFoto.Value <> "" Then
                    Dim archivo As String = "~/Temp/imgFicha" & url
                    Call DescargarFTP(archivo, url)
                    imgavatar.ImageUrl = archivo
                    'imgAvatarModal.ImageUrl = archivo
                End If


                If rdoReader(39).ToString = True Then
                    txtRut.BackColor = Drawing.Color.LightGreen
                Else
                    txtRut.BackColor = Drawing.Color.LightSalmon
                End If

                btnDatosContrato.Visible = True
                btnInstAsciadas.Visible = True
                hdnActivo.Value = 1
                lblActivo.Text = 1
            Else
                hdnActivo.Value = 0
                lblActivo.Text = 0
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarDatos", Err, Page, Master)
        End Try
    End Sub

    Private Sub DescargarFTP(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usarioftp:Ftp2018.@192.168.10.28/RRHH/ImagenFicha/Doctos_" & Trim(txtRut.Text) & "/" & Trim(nombre), Server.MapPath(archivo), "usuarioftp", "Ftp2018.")
        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaLicencias()
        Try
            Dim strNomTabla As String = "ADMMae_licencias_conducir"
            Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'CLA', '" & Trim(txtRut.Text) & "'"
            grdLicencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaLicencias", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaGrupoFamiliar()
        Try
            Dim strNomTabla As String = "RRHHMae_grupo_familiar"
            Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'GGF', '" & Trim(txtRut.Text) & "'"
            grdGrupoFamiliar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdGrupoFamiliar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaGrupoFamiliar", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaEstudios()
        Try
            Dim strNomTabla As String = "RRHHNub_ficha_estudios"
            Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'GEF', '" & Trim(txtRut.Text) & "'"
            grdEstudios.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdEstudios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaEstudios", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_personal"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@nom_personal", SqlDbType.NVarChar)
            comando.Parameters("@nom_personal").Value = Trim(txtNombres.Text)
            comando.Parameters.Add("@ape_paterno", SqlDbType.NVarChar)
            comando.Parameters("@ape_paterno").Value = Trim(txtApePaterno.Text)
            comando.Parameters.Add("@ape_materno", SqlDbType.NVarChar)
            comando.Parameters("@ape_materno").Value = Trim(txtApeMaterno.Text)
            comando.Parameters.Add("@id_genero", SqlDbType.NVarChar)
            comando.Parameters("@id_genero").Value = cboGenero.SelectedValue
            comando.Parameters.Add("@fec_nacto", SqlDbType.NVarChar)
            comando.Parameters("@fec_nacto").Value = Trim(txtFecNacto.Text)
            comando.Parameters.Add("@est_civil", SqlDbType.NVarChar)
            comando.Parameters("@est_civil").Value = cboEstCivil.SelectedValue
            comando.Parameters.Add("@nom_calle", SqlDbType.NVarChar)
            comando.Parameters("@nom_calle").Value = Trim(txtNomCalle.Text)
            comando.Parameters.Add("@nom_poblacion", SqlDbType.NVarChar)
            comando.Parameters("@nom_poblacion").Value = Trim(txtPoblacion.Text)
            comando.Parameters.Add("@num_domicilio", SqlDbType.NVarChar)
            comando.Parameters("@num_domicilio").Value = Trim(txtNumDomicilio.Text)
            comando.Parameters.Add("@des_referencia", SqlDbType.NVarChar)
            comando.Parameters("@des_referencia").Value = Trim(txtDesRef.Text)
            comando.Parameters.Add("@cod_region", SqlDbType.NVarChar)
            comando.Parameters("@cod_region").Value = cboRegion.SelectedValue
            comando.Parameters.Add("@cod_comuna", SqlDbType.NVarChar)
            comando.Parameters("@cod_comuna").Value = cboProvincia.SelectedValue
            comando.Parameters.Add("@cod_ciudad", SqlDbType.NVarChar)
            comando.Parameters("@cod_ciudad").Value = cboCiudad.SelectedValue
            comando.Parameters.Add("@tip_nacionalidad", SqlDbType.NVarChar)
            comando.Parameters("@tip_nacionalidad").Value = cboNacionalidad.SelectedValue
            comando.Parameters.Add("@des_nacionalidad", SqlDbType.NVarChar)
            comando.Parameters("@des_nacionalidad").Value = Trim(txtDesNacionalidad.Text)
            comando.Parameters.Add("@num_celular", SqlDbType.NVarChar)
            comando.Parameters("@num_celular").Value = Trim(txtCelular.Text)
            comando.Parameters.Add("@num_fijo", SqlDbType.NVarChar)
            comando.Parameters("@num_fijo").Value = Trim(txtTelFijo.Text)
            comando.Parameters.Add("@nom_referencia", SqlDbType.NVarChar)
            comando.Parameters("@nom_referencia").Value = Trim(txtNomContacto.Text)
            comando.Parameters.Add("@num_referencia", SqlDbType.NVarChar)
            comando.Parameters("@num_referencia").Value = Trim(txtFonoContacto.Text)
            comando.Parameters.Add("@des_parentesco", SqlDbType.NVarChar)
            comando.Parameters("@des_parentesco").Value = Trim(txtParentesco.Text)
            comando.Parameters.Add("@est_fuerzas_armadas", SqlDbType.NVarChar)
            comando.Parameters("@est_fuerzas_armadas").Value = cboFFAA.SelectedValue
            comando.Parameters.Add("@des_fuerzas_armadas", SqlDbType.NVarChar)
            comando.Parameters("@des_fuerzas_armadas").Value = Trim(txtDesFFAA.Text)
            comando.Parameters.Add("@est_discapacidad", SqlDbType.NVarChar)
            comando.Parameters("@est_discapacidad").Value = cboDiscapacidad.SelectedValue
            comando.Parameters.Add("@des_discapacidad", SqlDbType.NVarChar)
            comando.Parameters("@des_discapacidad").Value = Trim(txtDesDiscapacidad.Text)
            comando.Parameters.Add("@nom_mail", SqlDbType.NVarChar)
            comando.Parameters("@nom_mail").Value = Trim(txtMail.Text)
            comando.Parameters.Add("@num_calzado", SqlDbType.NVarChar)
            comando.Parameters("@num_calzado").Value = Trim(txtCalzado.Text)
            comando.Parameters.Add("@num_talla_pantalon", SqlDbType.NVarChar)
            comando.Parameters("@num_talla_pantalon").Value = Trim(txtNumPantalon.Text)
            comando.Parameters.Add("@num_talla_torso", SqlDbType.NVarChar)
            comando.Parameters("@num_talla_torso").Value = Trim(txtTallaTop.Text)
            comando.Parameters.Add("@des_restriccion_trabajo", SqlDbType.NVarChar)
            comando.Parameters("@des_restriccion_trabajo").Value = Trim(txtResTrabajo.Text)

            comando.Parameters.Add("@peso", SqlDbType.NVarChar)
            comando.Parameters("@peso").Value = Trim(txtPeso.Text)
            comando.Parameters.Add("@estatura", SqlDbType.NVarChar)
            comando.Parameters("@estatura").Value = Trim(txtEstatura.Text)

            If uplCDocumento.HasFile = True Then
                comando.Parameters.Add("@url_foto", SqlDbType.NVarChar)
                comando.Parameters("@url_foto").Value = Trim(Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_"))
            End If

            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub



    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_personal"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@nom_personal", SqlDbType.NVarChar)
            comando.Parameters("@nom_personal").Value = Trim(txtNombres.Text)
            comando.Parameters.Add("@ape_paterno", SqlDbType.NVarChar)
            comando.Parameters("@ape_paterno").Value = Trim(txtApePaterno.Text)
            comando.Parameters.Add("@ape_materno", SqlDbType.NVarChar)
            comando.Parameters("@ape_materno").Value = Trim(txtApeMaterno.Text)
            comando.Parameters.Add("@id_genero", SqlDbType.NVarChar)
            comando.Parameters("@id_genero").Value = cboGenero.SelectedValue
            comando.Parameters.Add("@fec_nacto", SqlDbType.NVarChar)
            comando.Parameters("@fec_nacto").Value = Trim(txtFecNacto.Text)
            comando.Parameters.Add("@est_civil", SqlDbType.NVarChar)
            comando.Parameters("@est_civil").Value = cboEstCivil.SelectedValue
            comando.Parameters.Add("@nom_calle", SqlDbType.NVarChar)
            comando.Parameters("@nom_calle").Value = Trim(txtNomCalle.Text)
            comando.Parameters.Add("@nom_poblacion", SqlDbType.NVarChar)
            comando.Parameters("@nom_poblacion").Value = Trim(txtPoblacion.Text)
            comando.Parameters.Add("@num_domicilio", SqlDbType.NVarChar)
            comando.Parameters("@num_domicilio").Value = Trim(txtNumDomicilio.Text)
            comando.Parameters.Add("@des_referencia", SqlDbType.NVarChar)
            comando.Parameters("@des_referencia").Value = Trim(txtDesRef.Text)
            comando.Parameters.Add("@cod_region", SqlDbType.NVarChar)
            comando.Parameters("@cod_region").Value = cboRegion.SelectedValue
            comando.Parameters.Add("@cod_comuna", SqlDbType.NVarChar)
            comando.Parameters("@cod_comuna").Value = cboProvincia.SelectedValue
            comando.Parameters.Add("@cod_ciudad", SqlDbType.NVarChar)
            comando.Parameters("@cod_ciudad").Value = cboCiudad.SelectedValue
            comando.Parameters.Add("@tip_nacionalidad", SqlDbType.NVarChar)
            comando.Parameters("@tip_nacionalidad").Value = cboNacionalidad.SelectedValue
            comando.Parameters.Add("@des_nacionalidad", SqlDbType.NVarChar)
            comando.Parameters("@des_nacionalidad").Value = Trim(txtDesNacionalidad.Text)
            comando.Parameters.Add("@num_celular", SqlDbType.NVarChar)
            comando.Parameters("@num_celular").Value = Trim(txtCelular.Text)
            comando.Parameters.Add("@num_fijo", SqlDbType.NVarChar)
            comando.Parameters("@num_fijo").Value = Trim(txtTelFijo.Text)
            comando.Parameters.Add("@nom_referencia", SqlDbType.NVarChar)
            comando.Parameters("@nom_referencia").Value = Trim(txtNomContacto.Text)
            comando.Parameters.Add("@num_referencia", SqlDbType.NVarChar)
            comando.Parameters("@num_referencia").Value = Trim(txtFonoContacto.Text)
            comando.Parameters.Add("@des_parentesco", SqlDbType.NVarChar)
            comando.Parameters("@des_parentesco").Value = Trim(txtParentesco.Text)
            comando.Parameters.Add("@est_fuerzas_armadas", SqlDbType.NVarChar)
            comando.Parameters("@est_fuerzas_armadas").Value = cboFFAA.SelectedValue
            comando.Parameters.Add("@des_fuerzas_armadas", SqlDbType.NVarChar)
            comando.Parameters("@des_fuerzas_armadas").Value = Trim(txtDesFFAA.Text)
            comando.Parameters.Add("@est_discapacidad", SqlDbType.NVarChar)
            comando.Parameters("@est_discapacidad").Value = cboDiscapacidad.SelectedValue
            comando.Parameters.Add("@des_discapacidad", SqlDbType.NVarChar)
            comando.Parameters("@des_discapacidad").Value = Trim(txtDesDiscapacidad.Text)
            comando.Parameters.Add("@nom_mail", SqlDbType.NVarChar)
            comando.Parameters("@nom_mail").Value = Trim(txtMail.Text)
            comando.Parameters.Add("@num_calzado", SqlDbType.NVarChar)
            comando.Parameters("@num_calzado").Value = Trim(txtCalzado.Text)
            comando.Parameters.Add("@num_talla_pantalon", SqlDbType.NVarChar)
            comando.Parameters("@num_talla_pantalon").Value = Trim(txtNumPantalon.Text)
            comando.Parameters.Add("@num_talla_torso", SqlDbType.NVarChar)
            comando.Parameters("@num_talla_torso").Value = Trim(txtTallaTop.Text)
            comando.Parameters.Add("@des_restriccion_trabajo", SqlDbType.NVarChar)
            comando.Parameters("@des_restriccion_trabajo").Value = Trim(txtResTrabajo.Text)
            comando.Parameters.Add("@peso", SqlDbType.NVarChar)
            comando.Parameters("@peso").Value = Trim(txtPeso.Text)
            comando.Parameters.Add("@estatura", SqlDbType.NVarChar)
            comando.Parameters("@estatura").Value = Trim(txtEstatura.Text)

            If uplCDocumento.HasFile = True Then
                comando.Parameters.Add("@url_foto", SqlDbType.NVarChar)
                comando.Parameters("@url_foto").Value = Trim(Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_"))
            Else
                comando.Parameters.Add("@url_foto", SqlDbType.NVarChar)
                comando.Parameters("@url_foto").Value = hdnUrlFoto.Value
            End If

            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtRut.Text = ""
        txtNombres.Text = ""
        txtApeMaterno.Text = ""
        txtApePaterno.Text = ""
        cboGenero.ClearSelection()
        txtFecNacto.Text = ""
        cboEstCivil.ClearSelection()
        txtCelular.Text = ""
        txtTelFijo.Text = ""
        cboNacionalidad.ClearSelection()
        txtDesNacionalidad.Text = ""
        cboRegion.ClearSelection()
        cboProvincia.ClearSelection()
        cboCiudad.ClearSelection()
        txtNomCalle.Text = ""
        txtPoblacion.Text = ""
        txtNumDomicilio.Text = ""
        txtDesRef.Text = ""
        txtNomContacto.Text = ""
        txtFonoContacto.Text = ""
        txtParentesco.Text = ""
        cboEstudios.ClearSelection()
        cboEstEstudios.ClearSelection()
        txtAniosEstudios.Text = ""
        txtDesTitulo.Text = ""
        cboFFAA.ClearSelection()
        txtDesFFAA.Text = ""
        cboDiscapacidad.ClearSelection()
        txtDesDiscapacidad.Text = ""
        txtDesDiscapacidad.Visible = False
        cboTipoLic.ClearSelection()
        txtFecVencLic.Text = ""
        txtGrupoFamiliar.Text = ""
        cboParentescoGF.ClearSelection()
        txtMail.Text = ""
        txtCalzado.Text = ""
        txtTallaTop.Text = ""
        txtNumPantalon.Text = ""
        txtResTrabajo.Text = ""
        hdnUrlFoto.Value = ""
        imgavatar.ImageUrl = "/Imagen/Sfoto.png"
        'imgAvatarModal.ImageUrl = "~/imagen/fpi.png"
        txtRut.BackColor = Drawing.Color.White
        txtPeso.Text = ""
        txtEstatura.Text = ""

        Dim ocar As New DataTable
        ocar = Session("dtLicencias")
        ocar.Clear()
        grdLicencias.DataSource = ocar
        grdLicencias.DataBind()

        Dim ocar1 As New DataTable
        ocar1 = Session("dtFamilia")
        ocar1.Clear()
        grdGrupoFamiliar.DataSource = ocar1
        grdGrupoFamiliar.DataBind()

        Dim ocar2 As New DataTable
        ocar2 = Session("dtEstudios")
        ocar2.Clear()
        grdEstudios.DataSource = ocar2
        grdEstudios.DataBind()

        'btnDatosContrato.Visible = False
        'btnInstAsciadas.Visible = False
        hdnActivo.Value = 0
        lblActivo.Text = 0
        Session("rut_persona") = ""
        Session("nom_persona") = ""
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If lblActivo.Text = 0 Then
            If uplCDocumento.HasFile = True Then
                If Path.GetExtension(uplCDocumento.FileName) = ".jpg" Or Path.GetExtension(uplCDocumento.FileName) = ".png" Then
                    If ExisteCarpeta("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.") = False Then
                        Call CrearCarpeta("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.")
                    End If
                    Call SubirFTPDcto("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.")

                    Call Guardar()
                    Call GuardarLicencias()
                    Call GuardarGrupoFamiliar()
                    Call GuardarEstudios()
                    MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
                    Call Limpiar()
                Else
                    MessageBox("Guardar", "Seleccione imagenes .jpg o .png", Page, Master, "W")
                End If
            Else
                Call Guardar()
                Call GuardarLicencias()
                Call GuardarGrupoFamiliar()
                Call GuardarEstudios()
                MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
                Call Limpiar()
            End If
        Else
            If uplCDocumento.HasFile = True Then
                If Path.GetExtension(uplCDocumento.FileName) = ".jpg" Or Path.GetExtension(uplCDocumento.FileName) = ".png" Then
                    Call BorrarFicheroFTP(Trim(txtRut.Text), hdnUrlFoto.Value)

                    If ExisteCarpeta("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.") = False Then
                        Call CrearCarpeta("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.")
                    End If
                    Call SubirFTPDcto("Doctos_" & Trim(txtRut.Text), "usuarioftp", "Ftp2018.")

                    Call Actualizar()
                    Call EliminarNubs()
                    Call GuardarLicencias()
                    Call GuardarGrupoFamiliar()
                    Call GuardarEstudios()
                    MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
                    Call Limpiar()
                Else
                    MessageBox("Guardar", "Seleccione imagenes .jpg o .png", Page, Master, "W")
                End If
            Else
                Call Actualizar()
                Call EliminarNubs()
                Call GuardarLicencias()
                Call GuardarGrupoFamiliar()
                Call GuardarEstudios()
                MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
                Call Limpiar()
            End If
        End If
    End Sub

    Function ExisteCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String) As Boolean
        Dim Request As FtpWebRequest = CType(WebRequest.Create(New Uri("ftp://192.168.10.28/RRHH/ImagenFicha/" & ruta)), FtpWebRequest)
        Request.Credentials = New NetworkCredential(usuario, pass)
        Request.Method = WebRequestMethods.Ftp.GetDateTimestamp
        Request.UsePassive = False
        Try
            Dim respuesta As FtpWebResponse
            respuesta = CType(Request.GetResponse(), FtpWebResponse)
            Return True
        Catch ex As WebException
            Return False
        End Try
    End Function

    Public Sub CrearCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Dim crearDirectorio As FtpWebRequest = DirectCast(System.Net.FtpWebRequest.Create("ftp://192.168.10.28/RRHH/ImagenFicha/" & Trim(ruta)), System.Net.FtpWebRequest)
        crearDirectorio.Credentials = New NetworkCredential(usuario, pass)
        crearDirectorio.Method = WebRequestMethods.Ftp.MakeDirectory
        Dim respuesta As FtpWebResponse = crearDirectorio.GetResponse()
    End Sub

    Public Sub SubirFTPDcto(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Try
            Dim NomArchivo As String = Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_")
            Dim clsRequest As FtpWebRequest = DirectCast(System.Net.WebRequest.Create("ftp://192.168.10.28/RRHH/ImagenFicha/" & Trim(ruta) & "/" & Replace(NomArchivo, ",", ".")), System.Net.FtpWebRequest)
            clsRequest.Credentials = New System.Net.NetworkCredential(usuario, pass)
            clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            Dim bFile As Byte() = uplCDocumento.FileBytes
            Dim clsStream As System.IO.Stream = clsRequest.GetRequestStream()
            clsStream.Write(bFile, 0, bFile.Length)
            clsStream.Close()
            clsStream.Dispose()
        Catch ex As Exception
            MessageBoxError("SubirFTPDcto", Err, Page, Master)
        End Try
    End Sub

    Private Sub EliminarDoctosAntiguos()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_ficha_personal"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "UFF"
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = Trim(txtRut.Text)
        comando.Parameters.Add("@url_foto", SqlDbType.NVarChar)
        comando.Parameters("@url_foto").Value = Replace(Path.GetFileName(uplCDocumento.FileName), " ", "_")
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub BorrarFicheroFTP(ByVal codigo As String, ByVal ruta As String)
        '**********************************************
        '*** Borramos el Fichero del FTP ya tratado
        '**********************************************
        Dim DireccionyFichero As String
        DireccionyFichero = "ftp://192.168.10.28/RRHH/ImagenFicha/Doctos_" + codigo + "/" + Trim(ruta)
        Dim peticionFTP As FtpWebRequest
        ' Creamos una petición FTP con la dirección del fichero a eliminar
        peticionFTP = CType(WebRequest.Create(New Uri(DireccionyFichero)), FtpWebRequest)
        ' Fijamos el usuario y la contraseña de la petición
        peticionFTP.Credentials = New NetworkCredential("usuarioftp", "Ftp2018.")
        ' Seleccionamos el comando que vamos a utilizar: Eliminar un fichero
        peticionFTP.Method = WebRequestMethods.Ftp.DeleteFile
        peticionFTP.UsePassive = False
        Try
            Dim respuestaFTP As FtpWebResponse
            respuestaFTP = CType(peticionFTP.GetResponse(), FtpWebResponse)
            respuestaFTP.Close()
        Catch ex As Exception
            ' Si se produce algún fallo, se devolverá el mensaje del error
            ' MsgBox("Error al borrar fichero" & ex.Message)
        End Try
    End Sub

    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnDatosContrato.Enabled = True
                Else
                    MessageBox("Permisos", "No puede ver datos de contrato de esta persona", Page, Master, "W")
                    btnDatosContrato.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Protected Sub btnDatosContrato_Click(sender As Object, e As EventArgs) Handles btnDatosContrato.Click

    End Sub
End Class