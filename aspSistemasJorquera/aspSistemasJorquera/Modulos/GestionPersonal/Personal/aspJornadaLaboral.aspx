﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspJornadaLaboral.aspx.vb" Inherits="aspSistemasJorquera.aspJornadaLaboral" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>
            <section class="content">

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre</label>
                        <div>
                            <asp:TextBox ID="txtNomJornada" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                  
                </div>

                <div class="row justify-content-center table-responsive">
                      <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Des. Jornada</label>
                        <div>
                            <asp:TextBox ID="txtDesJornada" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">4 rol</label>
                        <div>
                            <asp:CheckBox ID="chkRol" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label"></label>
                        <div>
                            <asp:TextBox ID="txtHrsMen" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label"></label>
                        <div>
                            <asp:TextBox ID="txtHrsSem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label"></label>
                        <div>
                            <asp:TextBox ID="txtHrsDia" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Visible="False"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-4 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grdJornadas" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                            <Columns>
                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                </asp:CommandField>

                                <asp:TemplateField HeaderText="Nombre">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNomJor" runat="server" Text='<%# Bind("nom_jornada")%>' />
                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_jornada")%>' />
                                        <asp:HiddenField ID="hdn4Rol" runat="server" Value='<%# Bind("est_cuarto_rol")%>' />

                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Distribucion Jornada">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDes" runat="server" Text='<%# Bind("des_jornada")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="4º Rol">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4Rol" runat="server" Text='<%# Bind("des_rol")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>


                                <%--  <asp:TemplateField HeaderText="Hrs. Mensuales">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHm" runat="server" Text='<%# Bind("hor_mes")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="110px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Hrs. Semanales">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHs" runat="server" Text='<%# Bind("hor_semana")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Hrs. Diarias">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHd" runat="server" Text='<%# Bind("hor_dia")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                                </asp:TemplateField>--%>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>
            </section>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
