﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspAdmPeriodoSeguros
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Periodos"
                hdnActivo.Value = ""
                Call BuscarPeriodo()
                Call CargarCombo()
                Call CargarGrilla(cboanio.SelectedValue)
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCombo()
        Dim strNomTabla As String = "PERMae_periodo"
        Dim strSQL As String = "Exec proc_periodo_seguros 'AN'"
        Try
            cboanio.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            cboanio.DataTextField = "num_anio"
            cboanio.DataValueField = "num_anio"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombo", Err, Page, Master)
        End Try
    End Sub

    Private Sub BuscarPeriodo()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Select num_periodo, bit_activo From JQRSGT.dbo.PERMae_periodo Where bit_activo=1"
        lblPeriodo.Text = "NINGUNO"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblPeriodo.Text = rdoReader(0).ToString & " Activo"
                    hdnActivo.Value = rdoReader(1).ToString
                Else
                    hdnActivo.Value = False
                End If

                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("BuscarPeriodo", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
    End Sub

    Private Sub RescatarIngresados()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec proc_periodo_seguros 'SP', '" & hdnPeriodoAbir.Value & "','',''"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIngresado.Value = 1
                Else
                    hdnIngresado.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarIngresados", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub CargarGrilla(ByVal anio As Integer)
        Dim strNomTabla As String = "PERMae_periodo"
        Dim strSQL As String = "Exec proc_periodo_seguros 'PE', '" & cboanio.SelectedValue & "','',''"
        Try
            grdPeriodo.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub AgregarPersonas()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 9000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_periodo_seguros"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IP"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = hdnPeriodoCierre.Value
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = hdnPeriodoAbir.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("AgregarPersonas", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdPeriodo_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdPeriodo.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdPeriodo.Rows(intRow)
        Dim objActivo As HiddenField = CType(row.FindControl("hdnActivo"), HiddenField)
        Dim objCodigo As HiddenField = CType(row.FindControl("hdnCodigo"), HiddenField)
        Dim objPeriodo As HiddenField = CType(row.FindControl("hdnPeriodo"), HiddenField)

        Call BuscarPeriodo()

        Try
            If objActivo.Value = False Then
                If hdnActivo.Value = False Then

                    Call ActualizarPeriodo(1, objCodigo.Value)
                    hdnPeriodoAbir.Value = objPeriodo.Value
                    hdnPeriodoCierre.Value = objPeriodo.Value - 1

                    Call RescatarIngresados()

                    If hdnIngresado.Value = 0 Then
                        Call AgregarPersonas()
                    End If

                ElseIf hdnActivo.Value = True Then
                    MessageBox("Guardar", "No se puede Abrir mas de un Periodo", Page, Master, "E")
                End If

            ElseIf objActivo.Value = True Then

                hdnPeriodoCierre.Value = objPeriodo.Value
                Call ActualizarPeriodo(0, objCodigo.Value)
                Call CargarGrilla(cboanio.SelectedValue)

            End If
            Call CargarGrilla(cboanio.SelectedValue)

        Catch ex As Exception
            MessageBoxError("grdPeriodo_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdPeriodo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdPeriodo.RowDataBound
        Dim estado As Boolean
        Dim objActivo As HiddenField = CType(e.Row.FindControl("hdnActivo"), HiddenField)

        If e.Row.RowType = DataControlRowType.DataRow Then
            estado = objActivo.Value

            Dim image As LinkButton = e.Row.FindControl("btnCostear")

            If IsDBNull(image) = False Then
                If estado = False Then

                    image.Text = "<i class=""fa fa-fw fa-circle-o""></i>"
                    image.ForeColor = Nothing
                    e.Row.BackColor = System.Drawing.Color.White
                ElseIf estado = True Then
                    image.Text = "<i class=""fa fa-fw fa-check-circle-o""></i>"
                    e.Row.BackColor = System.Drawing.Color.LightGreen
                    image.ForeColor = System.Drawing.Color.Black
                End If
            End If
        End If
    End Sub

    Private Sub ActualizarPeriodo(ByVal bit As Integer, ByVal Codigo As Integer)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando = New SqlCommand
            comando.CommandType = CommandType.Text
            comando.CommandText = "Update JQRSGT.dbo.PERMae_periodo set bit_activo=" & bit & " Where cod_periodo=" & Codigo & " "

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarCuenta", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarGrilla(cboanio.SelectedValue)
    End Sub

End Class