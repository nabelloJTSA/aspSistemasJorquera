﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspValoresSeguroSalud
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Ingresar Personas"
                Call carga_combo_empresa()
                Call CargarGrilla()
                Call carga_combo_area()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> (5)  order by cod_empresa"
            cboempresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboempresa.DataTextField = "nom_empresa"
            cboempresa.DataValueField = "cod_empresa"
            cboempresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_area()
        Try
            Dim strNomTablaR As String = "REMMae_area"
            Dim strSQLR As String = "Select id_area=0, nom_area='Ninguna' UNION select id_area, nom_area from REMMae_area where cod_empresa = 3 order by id_area"
            cboArea.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboArea.DataTextField = "nom_area"
            cboArea.DataValueField = "id_area"
            cboArea.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_area", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Dim strNomTabla As String = "PERMov_valores_seguros"
        Dim strSQL As String = "Exec proc_valores_vida_camara 'G'"
        Try
            grdValores.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdValores.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If cboempresa.SelectedValue = 0 Or txtValCarga.Text = "" Or txtValTitular.Text = "" Or txtPorEmp.Text = "" Or txtPorTrab.Text = "" Then
            MessageBox("Guardar", "Debe ingresar datos obligatorios (*)", Page, Master, "W")
        Else
            If hdnActivo.Value = 0 Then
                Call guardar()
            Else
                Call actualizar()
            End If
        End If
    End Sub

    Private Sub Limpiar()
        txtPorEmp.Text = ""
        txtPorTrab.Text = ""
        txtValTitular.Text = ""
        txtValCarga.Text = ""
        txtValCargaEsp.Text = ""
        cboempresa.ClearSelection()
        cboArea.ClearSelection()
        hdnActivo.Value = 0
        cboArea.Enabled = False
        Call CargarGrilla()
    End Sub

    Private Sub guardar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_valores_vida_camara"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboempresa.SelectedValue
            comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
            comando.Parameters("@id_area").Value = cboArea.SelectedValue
            comando.Parameters.Add("@porcentaje_empresa", SqlDbType.NVarChar)
            comando.Parameters("@porcentaje_empresa").Value = Replace(txtPorEmp.Text, ",", ".")
            comando.Parameters.Add("@porcentaje_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@porcentaje_trabajador").Value = Replace(txtPorTrab.Text, ",", ".")
            comando.Parameters.Add("@val_titular", SqlDbType.NVarChar)
            comando.Parameters("@val_titular").Value = Replace(txtValTitular.Text, ",", ".")
            comando.Parameters.Add("@val_carga", SqlDbType.NVarChar)
            comando.Parameters("@val_carga").Value = Replace(txtValCarga.Text, ",", ".")
            comando.Parameters.Add("@val_carga_especial", SqlDbType.NVarChar)
            comando.Parameters("@val_carga_especial").Value = Replace(txtValCargaEsp.Text, ",", ".")
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu")
            comando.Parameters.Add("@fec_add", SqlDbType.NVarChar)
            comando.Parameters("@fec_add").Value = Date.Now.ToString
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")


            Call Limpiar()
            Call CargarGrilla()

        Catch ex As Exception
            MessageBoxError("guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_valores_vida_camara"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnID.Value
            comando.Parameters.Add("@porcentaje_empresa", SqlDbType.NVarChar)
            comando.Parameters("@porcentaje_empresa").Value = Replace(txtPorEmp.Text, ",", ".")
            comando.Parameters.Add("@porcentaje_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@porcentaje_trabajador").Value = Replace(txtPorTrab.Text, ",", ".")
            comando.Parameters.Add("@val_titular", SqlDbType.NVarChar)
            comando.Parameters("@val_titular").Value = Replace(txtValTitular.Text, ",", ".")
            comando.Parameters.Add("@val_carga", SqlDbType.NVarChar)
            comando.Parameters("@val_carga").Value = Replace(txtValCarga.Text, ",", ".")
            comando.Parameters.Add("@val_carga_especial", SqlDbType.NVarChar)
            comando.Parameters("@val_carga_especial").Value = Replace(txtValCargaEsp.Text, ",", ".")
            comando.Parameters.Add("@user_edit", SqlDbType.NVarChar)
            comando.Parameters("@user_edit").Value = Session("cod_usu")
            comando.Parameters.Add("@fec_edit", SqlDbType.NVarChar)
            comando.Parameters("@fec_edit").Value = Date.Now.ToString
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Actualizado Exitosamente", Page, Master, "S")

            Call Limpiar()
            Call CargarGrilla()

        Catch ex As Exception
            MessageBoxError("actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdValores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdValores.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdValores.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_cod_emp As HiddenField = CType(row.FindControl("hdnCodEmpresa"), HiddenField)
            Dim obj_id_area As HiddenField = CType(row.FindControl("hdnid_area"), HiddenField)

            Dim obj_por_emp As Label = CType(row.FindControl("lblPorcetajeEmp"), Label)
            Dim obj_por_trab As Label = CType(row.FindControl("lblPorcentajeTrab"), Label)
            Dim obj_val_titular As Label = CType(row.FindControl("lblValTitular"), Label)
            Dim obj_val_carga As Label = CType(row.FindControl("lblValCarga"), Label)
            Dim obj_val_carga_esp As Label = CType(row.FindControl("lblValCargaEspe"), Label)

            hdnID.Value = hdnid_obj.Value

            cboempresa.SelectedValue = obj_cod_emp.Value
            If obj_cod_emp.Value = 0 Then
                cboArea.ClearSelection()
                cboArea.Enabled = False
            Else
                cboArea.SelectedValue = obj_id_area.Value
                cboArea.Enabled = True
            End If

            txtPorEmp.Text = obj_por_emp.Text
            txtPorTrab.Text = obj_por_trab.Text
            txtValTitular.Text = obj_val_titular.Text
            txtValCarga.Text = obj_val_carga.Text
            txtValCargaEsp.Text = obj_val_carga_esp.Text

            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdValores_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdValores_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdValores.PageIndexChanging
        Try
            grdValores.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdValores_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboempresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboempresa.SelectedIndexChanged
        If cboempresa.SelectedValue = 3 Then
            cboArea.Enabled = True
            Call carga_combo_area()
        Else
            cboArea.Enabled = False
            cboArea.ClearSelection()
        End If
    End Sub

    Protected Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        Call Limpiar()
    End Sub

End Class