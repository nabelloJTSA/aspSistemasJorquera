﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspBancos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla()
                MtvBancos.ActiveViewIndex = 0
                lbltituloP.Text = "Bancos"
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "mae_banco"
            Dim strSQL As String = "Exec mant_RRHH_banco 'G'"
            grdBanco.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdBanco.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_banco"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@num_codigo", SqlDbType.NVarChar)
            comando.Parameters("@num_codigo").Value = Trim(txtCodBanco.Text)
            comando.Parameters.Add("@nom_banco", SqlDbType.NVarChar)
            comando.Parameters("@nom_banco").Value = Trim(txtNomBanco.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_banco"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@num_codigo", SqlDbType.NVarChar)
            comando.Parameters("@num_codigo").Value = Trim(txtCodBanco.Text)
            comando.Parameters.Add("@nom_banco", SqlDbType.NVarChar)
            comando.Parameters("@nom_banco").Value = Trim(txtNomBanco.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdBanco.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdBanco.SelectedRow

            Dim obj_Cod As Label = CType(row.FindControl("lblCod"), Label)
            Dim obj_NomBanco As Label = CType(row.FindControl("lblBanco"), Label)

            hdnId.Value = obj_Cod.Text

            txtCodBanco.Text = obj_Cod.Text
            txtNomBanco.Text = obj_NomBanco.Text
            txtCodBanco.Enabled = False

            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdBanco_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtCodBanco.Text = ""
        txtNomBanco.Text = ""
        hdnActivo.Value = 0
        txtCodBanco.Enabled = True
        Call CargarGrilla()
    End Sub

    Private Sub grdBanco_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdBanco.PageIndexChanging
        Try
            grdBanco.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdBanco_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Function Existe() As Boolean
        Dim strSQL As String = ""
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Try
            cnxBaseDatos.Open()
            strSQL = "Exec mant_RRHH_banco 'RD','" & Trim(txtCodBanco.Text) & "'"
            cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read() Then
                Existe = True
            Else
                Existe = False
            End If
            rdoReader.Close()
            rdoReader = Nothing
            cmdTemporal.Dispose()
            cmdTemporal = Nothing
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("Existe", Err, Page, Master)
            Existe = True
        End Try
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            If Existe() = True Then
                MessageBox("Guardar", "Codigo de Banco ya existe", Page, Master, "W")
            Else
                Call Guardar()
            End If
        Else
            Call Actualizar()
        End If
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub btnBanco_Click(sender As Object, e As EventArgs) Handles btnBanco.Click
        MtvBancos.ActiveViewIndex = 0
        lbltituloP.Text = "Bancos"
    End Sub

    '**************************************Medios 
    Protected Sub btnMP_Click(sender As Object, e As EventArgs) Handles btnMP.Click
        MtvBancos.ActiveViewIndex = 1
        Call CargarGrillaMP()
        lbltituloP.Text = "Medios de Pago"
    End Sub


    Private Sub CargarGrillaMP()
        Try
            Dim strNomTabla As String = "RRHHMae_mutualidad"
            Dim strSQL As String = "Exec mant_RRHH_medio_pago 'G'"
            grdMedioPago.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdMedioPago.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarMP()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_medio_pago"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@num_codigo ", SqlDbType.NVarChar)
            comando.Parameters("@num_codigo ").Value = Trim(txtCodigoMP.Text)
            comando.Parameters.Add("@nom_mediopago", SqlDbType.NVarChar)
            comando.Parameters("@nom_mediopago").Value = Trim(txtMedioPago.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaMP()
            Call LimpiarMP()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarMP()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_medio_pago"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@num_codigo ", SqlDbType.NVarChar)
            comando.Parameters("@num_codigo ").Value = hdnId.Value
            comando.Parameters.Add("@nom_mediopago", SqlDbType.NVarChar)
            comando.Parameters("@nom_mediopago").Value = Trim(txtMedioPago.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaMP()
            Call LimpiarMP()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdMedioPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdMedioPago.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdMedioPago.SelectedRow
            Dim hdnid_obj As Label = CType(row.FindControl("lblCod"), Label)
            Dim obj_MedioPago As Label = CType(row.FindControl("lblMedPago"), Label)

            hdnIdMP.Value = hdnid_obj.Text
            txtMedioPago.Text = obj_MedioPago.Text
            txtCodigoMP.Text = hdnid_obj.Text

            txtCodigoMP.Enabled = False
            hdnActivoMP.Value = 1
        Catch ex As Exception
            MessageBoxError("grdMedioPago_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarMP()
        txtMedioPago.Text = ""
        txtCodigoMP.Text = ""
        txtCodigoMP.Enabled = True
        hdnActivoMP.Value = 0
        Call CargarGrillaMP()
    End Sub

    Private Sub grdMedioPago_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdMedioPago.PageIndexChanging
        Try
            grdMedioPago.PageIndex = e.NewPageIndex
            Call CargarGrillaMP()
        Catch ex As Exception
            MessageBoxError("grdMedioPago_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Function ExisteMP() As Boolean
        Dim strSQL As String = ""
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Try
            cnxBaseDatos.Open()
            strSQL = "Exec mant_RRHH_medio_pago 'RD','" & Trim(txtCodigoMP.Text) & "'"
            cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read() Then
                ExisteMP = True
            Else
                ExisteMP = False
            End If
            rdoReader.Close()
            rdoReader = Nothing
            cmdTemporal.Dispose()
            cmdTemporal = Nothing
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("Existe", Err, Page, Master)
            ExisteMP = True
        End Try
    End Function

    Protected Sub btnGuardarMP_Click(sender As Object, e As EventArgs) Handles btnGuardarMP.Click
        If hdnActivoMP.Value = 0 Then
            If ExisteMP() = True Then
                MessageBox("Guardar", "Codigo ya existe", Page, Master, "W")
            Else
                Call GuardarMP()
            End If
        Else
            Call ActualizarMP()
        End If
    End Sub

    Protected Sub btnCancelarMP_Click(sender As Object, e As EventArgs) Handles btnCancelarMP.Click
        Call LimpiarMP()
    End Sub

    '*************************Clasificacion

    Private Sub CargarGrillaCL()
        Try
            Dim strNomTabla As String = "RRHHMae_clasificacion_pago"
            Dim strSQL As String = "Exec mant_RRHH_clasificacion_pago 'G'"
            grdClasificacion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdClasificacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresas()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHH_clasificacion_pago 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresas", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarCL()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_clasificacion_pago"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_clasificacion", SqlDbType.NVarChar)
            comando.Parameters("@nom_clasificacion").Value = Trim(txtNomCL.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaCL()
            Call LimpiarCL()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarCL()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_clasificacion_pago"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_clasificacion", SqlDbType.NVarChar)
            comando.Parameters("@nom_clasificacion").Value = Trim(txtNomCL.Text)
            comando.Parameters.Add("@cod_clasificacion", SqlDbType.NVarChar)
            comando.Parameters("@cod_clasificacion").Value = hdnIdCl.Value
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaCL()
            Call LimpiarCL()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdClasificacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdClasificacion.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdClasificacion.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_cod_emp As HiddenField = CType(row.FindControl("hdn_cod_empresa"), HiddenField)
            Dim obj_NomDepto As Label = CType(row.FindControl("lblClasificacion"), Label)

            hdnIdCl.Value = obj_id.Value
            txtNomCL.Text = obj_NomDepto.Text
            cboEmpresa.SelectedValue = obj_cod_emp.Value
            hdnActivoCL.Value = 1

        Catch ex As Exception
            MessageBoxError("grdClasificacion_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarCL()
        txtNomCL.Text = ""
        hdnActivoCL.Value = 0
        Call CargarGrillaCL()
    End Sub

    Private Sub grdClasificacion_SelectedIndexChanged(sender As Object, e As GridViewPageEventArgs) Handles grdClasificacion.PageIndexChanging
        Try
            grdClasificacion.PageIndex = e.NewPageIndex
            Call CargarGrillaCL()
        Catch ex As Exception
            MessageBoxError("grdClasificacion_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarCL_Click(sender As Object, e As EventArgs) Handles btnGuardarCl.Click
        If hdnActivoCL.Value = 0 Then
            Call GuardarCL()
        Else
            Call ActualizarCL()
        End If
    End Sub

    Protected Sub btnCancelarCL_Click(sender As Object, e As EventArgs) Handles btnCancelarCl.Click
        Call LimpiarCL()
    End Sub

    Protected Sub btnDetps_Click(sender As Object, e As EventArgs) Handles btnDetps.Click
        Call CargarEmpresas()
        MtvBancos.ActiveViewIndex = 2
        lbltituloP.Text = "Clasificación de Pago"
        Call CargarGrillaCL()
    End Sub
End Class