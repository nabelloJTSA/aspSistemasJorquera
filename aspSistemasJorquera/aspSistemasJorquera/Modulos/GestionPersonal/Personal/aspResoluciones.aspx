﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspResoluciones.aspx.vb" Inherits="aspSistemasJorquera.aspResoluciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text="Resoluciones"></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>

            <section class="content">


                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Faena</label>
                        <div>
                            <asp:DropDownList ID="cboFaena" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Nº Resolución</label>
                        <div>
                            <asp:TextBox ID="txtNResolucion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Promedio</label>
                        <div>
                            <asp:TextBox ID="txtPromedio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">


                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Emisión</label>
                        <div>
                            <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Fecha Vencimiento</label>
                        <div>
                            <asp:TextBox ID="txtFechaVencimiento" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Observación</label>
                        <div>
                            <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-4 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grdResoluciones" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                            <Columns>
                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                </asp:CommandField>
                                <asp:TemplateField HeaderText="Cod. Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCodEmpresa" runat="server" Text='<%# Bind("cod_empresa")%>' />
                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_resolucion")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("empresa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cod Faena">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCodFaena" runat="server" Text='<%# Bind("cod_faena")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Faena">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFaena" runat="server" Text='<%# Bind("faena")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nº Resolución">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNResolucion" runat="server" Text='<%# Bind("num_resolucion")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nº Promedio">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNPromedio" runat="server" Text='<%# Bind("num_promedio")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emision">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecEmision" runat="server" Text='<%# Bind("fec_emision", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vencimiento">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVencimiento" runat="server" Text='<%# Bind("fec_vencimiento", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Observacion">
                                    <ItemTemplate>
                                        <asp:Label ID="lblObservacion" runat="server" Text='<%# Bind("des_observacion")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>



                <asp:HiddenField ID="hdnActualizar" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdResolucion" runat="server" />


            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
