﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspFichaContrato
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lblRut.Text = Session("rut_persona")
                lblNombre.Text = Session("nom_persona")
                Call CargarEmpresa()
                Call CargarTipoContrato()
                Call CargarContratoAsociado()
                Call CargarTipoTrabajador()
                Call CargarCargo()
                Call CargarClasificacionPago()
                Call CargarFamiliaServicios()
                Call CargarCiudad()
                Call CargarJornada()
                Call CargarMedioPago()
                Call CargarBancos()
                Call CargarIC()
                Session("dtIC") = CrearTablaIC()
                Call BuscarDatos()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Function CrearTablaIC() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("nom_ic", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_ic", Type.GetType("System.String"))
            dtTemporal.Columns.Add("beneficios", Type.GetType("System.String"))
            dtTemporal.Columns.Add("est_beneficio", Type.GetType("System.String"))
        Catch ex As Exception
            MessageBoxError("CrearTablaIC", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Sub CargarTipoContrato()
        Try
            Dim strNomTablaR As String = "REMMae_tipo_contrato"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CTC'"
            cboTipoContrato.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoContrato.DataTextField = "nom_contrato"
            cboTipoContrato.DataValueField = "cod_contrato"
            cboTipoContrato.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoContrato", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarContratoAsociado()
        Try
            Dim strNomTablaR As String = "RRHHMae_contrato"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCA'"
            cboContratoAsociado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboContratoAsociado.DataTextField = "nom_contrato"
            cboContratoAsociado.DataValueField = "id_contrato"
            cboContratoAsociado.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarContratoAsociado", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarIC()
        Try
            Dim strNomTablaR As String = "RRHHMae_contrato"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CIC', '" & cboEmpresa.SelectedValue & "'"
            cboInsColectivo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInsColectivo.DataTextField = "nom_instrumento"
            cboInsColectivo.DataValueField = "cod_instrumento"
            cboInsColectivo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarIC", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipoTrabajador()
        Try
            Dim strNomTablaR As String = "RRHHMae_tipo_trabajador"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CTT', '" & cboEmpresa.SelectedValue & "'"
            cboTipoTrabajador.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajador.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajador.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajador.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSucursal()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCS','" & cboEmpresa.SelectedValue & "'"
            cboSucursal.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSucursal.DataTextField = "nom_sucursal"
            cboSucursal.DataValueField = "cod_sucursal"
            cboSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSucursal", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarArea()
        Try
            Dim strNomTablaR As String = "RRHHMae_area"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CDA','" & cboEmpresa.SelectedValue & "'"
            cboArea.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboArea.DataTextField = "nom_area"
            cboArea.DataValueField = "cod_area"
            cboArea.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarArea", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSubArea()
        Try
            Dim strNomTablaR As String = "RRHHMae_area"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CSA','" & cboArea.SelectedValue & "'"
            cboSubArea.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSubArea.DataTextField = "nom_subarea"
            cboSubArea.DataValueField = "cod_subarea"
            cboSubArea.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSubArea", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepartamento()
        Try
            Dim strNomTablaR As String = "RRHHMae_departamento"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCD','" & cboEmpresa.SelectedValue & "'"
            cboDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboDepto.DataTextField = "nom_departamento"
            cboDepto.DataValueField = "cod_departamento"
            cboDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSubArea", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCargo()
        Try
            Dim strNomTablaR As String = "RRHHMae_cargo"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCC','" & cboEmpresa.SelectedValue & "'"
            cboCargo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCargo.DataTextField = "nom_cargo"
            cboCargo.DataValueField = "cod_cargo"
            cboCargo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCargo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarClasificacionPago()
        Try
            Dim strNomTablaR As String = "RRHHMae_clasificacion_pago"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCP','" & cboEmpresa.SelectedValue & "'"
            cboClaPago.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboClaPago.DataTextField = "nom_clasificacion"
            cboClaPago.DataValueField = "cod_clasificacion"
            cboClaPago.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarClasificacionPago", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarFamiliaServicios()
        Try
            Dim strNomTablaR As String = "TRANSPOR"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CFS'"
            cboFamiliaServicio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFamiliaServicio.DataTextField = "FamiliaServicio"
            cboFamiliaServicio.DataValueField = "tracod"
            cboFamiliaServicio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarFamiliaServicios", Err, Page, Master)
        End Try
    End Sub

    'Private Sub CargarInstrumentoColectivo()
    '    Try
    '        Dim strNomTablaR As String = "RRHHMae_instrumento_colectivo"
    '        Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CIC'"
    '        cboIC.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
    '        cboIC.DataTextField = "nom_instrumento"
    '        cboIC.DataValueField = "cod_instrumento"
    '        cboIC.DataBind()
    '    Catch ex As Exception
    '        MessageBoxError("CargarInstrumentoColectivo", Err, Page, Master)
    '    End Try
    'End Sub

    Private Sub CargarCiudad()
        Try
            Dim strNomTablaR As String = "mae_provincias"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CDC'"
            cboCiudad.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCiudad.DataTextField = "nom_provincia"
            cboCiudad.DataValueField = "cod_provincia"
            cboCiudad.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCiudad", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarJornada()
        Try
            Dim strNomTablaR As String = "RRHHMae_jornada"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CCJ', '" & cboEmpresa.SelectedValue & "'"
            cboJornada.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboJornada.DataTextField = "nom_jornada"
            cboJornada.DataValueField = "id_jornada"
            cboJornada.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarJornada", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarMedioPago()
        Try
            Dim strNomTablaR As String = "mae_medio_pago"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CMP'"
            cboMedioPago.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMedioPago.DataTextField = "nom_mediopago"
            cboMedioPago.DataValueField = "num_codigo"
            cboMedioPago.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarMedioPago", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarBancos()
        Try
            Dim strNomTablaR As String = "Mae_banco"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_contrato 'CDB'"
            cboBanco.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboBanco.DataTextField = "nom_banco"
            cboBanco.DataValueField = "num_codigo"
            cboBanco.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarBancos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarSucursal()
        Call CargarArea()
        Call CargarIC()
        Call CargarTipoTrabajador()
        Call CargarDepartamento()
        Call CargarCargo()
        Call CargarClasificacionPago()
        Call CargarJornada()
    End Sub

    Protected Sub cboArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboArea.SelectedIndexChanged
        Call CargarSubArea()
    End Sub

    Protected Sub cboSubArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSubArea.SelectedIndexChanged
        Call CargarDepartamento()
    End Sub

    Protected Sub cboJornada_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboJornada.SelectedIndexChanged
        Call CargarDatosJornada()
    End Sub

    Private Sub Rescatar4Rol()
        Dim cmdTemporal As SqlCommand, blnError As Boolean = False
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec mant_RRHH_ficha_contrato 'R4R','" & lblRut.Text & "'"
        Try
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtFecIngresoRol.Text = CDate(rdoReader(0).ToString).ToString("yyyy-MM-dd")
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("Rescatar4Rol", Err, Page, Me)
        End Try
    End Sub

    Private Sub CargarDatosJornada()
        Dim cmdTemporal As SqlCommand, blnError As Boolean = False
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec mant_RRHH_ficha_contrato 'CDJ','" & cboJornada.SelectedValue & "'"
        Try
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblDesJornada.Text = rdoReader(2).ToString
                    If rdoReader(3).ToString = True Then
                        lblRol.Visible = True
                        txtFecIngresoRol.Visible = True
                        Call Rescatar4Rol()
                        hdn4Rol.Value = 1
                    Else
                        lblRol.Visible = False
                        txtFecIngresoRol.Visible = False
                        hdn4Rol.Value = 0
                    End If
                Else
                    lblDesJornada.Text = ""
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("CargarDatosJornada", Err, Page, Me)
        End Try
    End Sub

    Function ValidarGuardado() As Boolean
        Dim guardar As Boolean = True
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec mant_RRHH_ficha_contrato 'CFC','" & Session("rut_persona") & "'"
        Try
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0) >= 1 Then
                        guardar = False
                    Else
                        guardar = True
                    End If
                Else
                    guardar = True
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarGuardado", Err, Page, Me)
        End Try
        Return guardar
    End Function

    Protected Sub cboTipoTrabajador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoTrabajador.SelectedIndexChanged
        If cboTipoTrabajador.SelectedValue = 1 Then
            lblFamiliaServicio.Visible = True
            cboFamiliaServicio.Visible = True
        Else
            lblFamiliaServicio.Visible = False
            cboFamiliaServicio.Visible = False
            Call CargarFamiliaServicios()
        End If
    End Sub

    Private Sub EliminarIC()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        'Try
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_ficha_contrato"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "DNI"
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = Session("rut_persona")
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        'Catch ex As Exception
        '    MessageBoxError("EliminarIC", Err, Page, Master)
        'End Try
    End Sub


    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_contrato"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Session("rut_persona")
            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@fec_ingreso").Value = Trim(txtFecIngreso.Text)
            comando.Parameters.Add("@cod_contrato", SqlDbType.NVarChar)
            comando.Parameters("@cod_contrato").Value = cboTipoContrato.SelectedValue
            comando.Parameters.Add("@id_contrato", SqlDbType.NVarChar)
            comando.Parameters("@id_contrato").Value = cboContratoAsociado.SelectedValue
            comando.Parameters.Add("@id_tipo_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_trabajador").Value = cboTipoTrabajador.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@cod_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@cod_sucursal").Value = cboSucursal.SelectedValue
            comando.Parameters.Add("@cod_area", SqlDbType.NVarChar)
            comando.Parameters("@cod_area").Value = cboArea.SelectedValue
            comando.Parameters.Add("@cod_subarea", SqlDbType.NVarChar)
            comando.Parameters("@cod_subarea").Value = cboSubArea.SelectedValue
            comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
            comando.Parameters("@cod_departamento").Value = cboDepto.SelectedValue
            comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
            comando.Parameters("@cod_cargo").Value = cboCargo.SelectedValue
            comando.Parameters.Add("@est_activo", SqlDbType.NVarChar)
            comando.Parameters("@est_activo").Value = cboActivo.SelectedValue
            comando.Parameters.Add("@cod_clasificacion_pago", SqlDbType.NVarChar)
            comando.Parameters("@cod_clasificacion_pago").Value = cboClaPago.SelectedValue
            comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
            comando.Parameters("@id_servicio").Value = cboFamiliaServicio.SelectedValue
            'comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
            'comando.Parameters("@cod_instrumento").Value = cboIC.SelectedValue
            comando.Parameters.Add("@cod_ciudad", SqlDbType.NVarChar)
            comando.Parameters("@cod_ciudad").Value = cboCiudad.SelectedValue
            comando.Parameters.Add("@id_jornada", SqlDbType.NVarChar)
            comando.Parameters("@id_jornada").Value = cboJornada.SelectedValue
            comando.Parameters.Add("@est_pacto", SqlDbType.NVarChar)
            comando.Parameters("@est_pacto").Value = cboPacto.SelectedValue
            comando.Parameters.Add("@fec_vencimiento", SqlDbType.NVarChar)
            comando.Parameters("@fec_vencimiento").Value = txtFecTerminoContrato.Text
            comando.Parameters.Add("@cod_medio_pago", SqlDbType.NVarChar)
            comando.Parameters("@cod_medio_pago").Value = cboMedioPago.SelectedValue
            comando.Parameters.Add("@cod_banco", SqlDbType.NVarChar)
            comando.Parameters("@cod_banco").Value = cboBanco.SelectedValue
            comando.Parameters.Add("@num_cuenta", SqlDbType.NVarChar)
            comando.Parameters("@num_cuenta").Value = Trim(txtNumCuenta.Text)
            comando.Parameters.Add("@num_monto_anticipo", SqlDbType.NVarChar)
            comando.Parameters("@num_monto_anticipo").Value = Trim(txtAntFijo.Text)
            comando.Parameters.Add("@hor_dia", SqlDbType.NVarChar)
            comando.Parameters("@hor_dia").Value = Trim(txtHrsDiarias.Text)
            comando.Parameters.Add("@hor_semana", SqlDbType.NVarChar)
            comando.Parameters("@hor_semana").Value = Trim(txtHrsSemanales.Text)
            comando.Parameters.Add("@hor_mes", SqlDbType.NVarChar)
            comando.Parameters("@hor_mes").Value = Trim(txtHrsMensuales.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@num_sueldo_base", SqlDbType.NVarChar)
            comando.Parameters("@num_sueldo_base").Value = Trim(txtSueldoBase.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()

            Call RescatarIdPerVac()
            Call GrabarMovVacaciones(Trim(lblRut.Text), hdnIdPerVac.Value, 0, cboEmpresa.SelectedValue)

        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub
    Public Sub GrabarMovVacaciones(ByVal rut As String, ByVal id_periodo_vac As Integer, ByVal num_saldo As Integer, ByVal cod_empresa As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "sel_tdi_usuario"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "GMV"
        comando.Parameters.Add("@rut_usuario", SqlDbType.NVarChar)
        comando.Parameters("@rut_usuario").Value = Trim(rut)
        comando.Parameters.Add("@id_periodo_vac", SqlDbType.NVarChar)
        comando.Parameters("@id_periodo_vac").Value = id_periodo_vac
        comando.Parameters.Add("@num_saldo", SqlDbType.NVarChar)
        comando.Parameters("@num_saldo").Value = num_saldo
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = cod_empresa
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
    End Sub

    Private Sub RescatarIdPerVac()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "select id_periodo_vac from REMMae_periodo_vacaciones where año_correspondiente =  YEAR(getdate())"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnIdPerVac.Value = rdoReader(0).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarDatos", Err, Page, Master)
        End Try
    End Sub


    Private Sub VerificarExiste4Rol()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec mant_RRHH_ficha_contrato 'VE4','" & Trim(lblRut.Text) & "'"
        'Try
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        cnxBaseDatos.Open()
        cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
        rdoReader = cmdTemporal.ExecuteReader()
        If rdoReader.Read Then
            If hdn4Rol.Value = 1 Then
                Call Actualizar4Rol(1)
            Else
                Call Actualizar4Rol(0)
            End If
        Else
            If hdn4Rol.Value = 1 Then
                Call Guardar4Rol()
            End If
        End If
        rdoReader.Close()
        cmdTemporal.Dispose()
        cnxBaseDatos.Close()
        rdoReader = Nothing
        cmdTemporal = Nothing
        cnxBaseDatos = Nothing
        'Catch ex As Exception
        '    MessageBoxError("VerificarExiste4Rol", Err, Page, Master)
        'End Try
    End Sub


    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        'Try
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_ficha_contrato"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "U"
        comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
        comando.Parameters("@rut_personal").Value = Session("rut_persona")
        comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
        comando.Parameters("@fec_ingreso").Value = Trim(txtFecIngreso.Text)
        comando.Parameters.Add("@cod_contrato", SqlDbType.NVarChar)
        comando.Parameters("@cod_contrato").Value = cboTipoContrato.SelectedValue
        comando.Parameters.Add("@id_contrato", SqlDbType.NVarChar)
        comando.Parameters("@id_contrato").Value = cboContratoAsociado.SelectedValue
        comando.Parameters.Add("@id_tipo_trabajador", SqlDbType.NVarChar)
        comando.Parameters("@id_tipo_trabajador").Value = cboTipoTrabajador.SelectedValue
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
        comando.Parameters.Add("@cod_sucursal", SqlDbType.NVarChar)
        comando.Parameters("@cod_sucursal").Value = cboSucursal.SelectedValue
        comando.Parameters.Add("@cod_area", SqlDbType.NVarChar)
        comando.Parameters("@cod_area").Value = cboArea.SelectedValue
        comando.Parameters.Add("@cod_subarea", SqlDbType.NVarChar)
        comando.Parameters("@cod_subarea").Value = cboSubArea.SelectedValue
        comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
        comando.Parameters("@cod_departamento").Value = cboDepto.SelectedValue
        comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
        comando.Parameters("@cod_cargo").Value = cboCargo.SelectedValue
        comando.Parameters.Add("@est_activo", SqlDbType.NVarChar)
        comando.Parameters("@est_activo").Value = cboActivo.SelectedValue
        comando.Parameters.Add("@cod_clasificacion_pago", SqlDbType.NVarChar)
        comando.Parameters("@cod_clasificacion_pago").Value = cboClaPago.SelectedValue
        comando.Parameters.Add("@id_servicio", SqlDbType.NVarChar)
        comando.Parameters("@id_servicio").Value = cboFamiliaServicio.SelectedValue
        'comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
        'comando.Parameters("@cod_instrumento").Value = cboIC.SelectedValue
        comando.Parameters.Add("@cod_ciudad", SqlDbType.NVarChar)
        comando.Parameters("@cod_ciudad").Value = cboCiudad.SelectedValue
        comando.Parameters.Add("@id_jornada", SqlDbType.NVarChar)
        comando.Parameters("@id_jornada").Value = cboJornada.SelectedValue
        comando.Parameters.Add("@est_pacto", SqlDbType.NVarChar)
        comando.Parameters("@est_pacto").Value = cboPacto.SelectedValue
        comando.Parameters.Add("@fec_vencimiento", SqlDbType.NVarChar)
        comando.Parameters("@fec_vencimiento").Value = txtFecTerminoContrato.Text
        comando.Parameters.Add("@cod_medio_pago", SqlDbType.NVarChar)
        comando.Parameters("@cod_medio_pago").Value = cboMedioPago.SelectedValue
        comando.Parameters.Add("@cod_banco", SqlDbType.NVarChar)
        comando.Parameters("@cod_banco").Value = cboBanco.SelectedValue
        comando.Parameters.Add("@num_cuenta", SqlDbType.NVarChar)
        comando.Parameters("@num_cuenta").Value = Trim(txtNumCuenta.Text)
        comando.Parameters.Add("@num_monto_anticipo", SqlDbType.NVarChar)
        comando.Parameters("@num_monto_anticipo").Value = Trim(txtAntFijo.Text)
        comando.Parameters.Add("@hor_dia", SqlDbType.NVarChar)
        comando.Parameters("@hor_dia").Value = Trim(Replace(txtHrsDiarias.Text, ",", "."))
        comando.Parameters.Add("@hor_semana", SqlDbType.NVarChar)
        comando.Parameters("@hor_semana").Value = Trim(txtHrsSemanales.Text)
        comando.Parameters.Add("@hor_mes", SqlDbType.NVarChar)
        comando.Parameters("@hor_mes").Value = Trim(txtHrsMensuales.Text)
        comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
        comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
        comando.Parameters.Add("@num_sueldo_base", SqlDbType.NVarChar)
        comando.Parameters("@num_sueldo_base").Value = Trim(txtSueldoBase.Text)
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        'Catch ex As Exception
        '    MessageBoxError("Actualizar", Err, Page, Master)
        'End Try
    End Sub

    Private Sub BuscarDatos()
        If ValidarGuardado() = False Then
            Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
            Dim strSql As String = "Exec mant_RRHH_ficha_contrato 'G', '" & Trim(Session("rut_persona")) & "'"
            Try
                Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
                cnxBaseDatos.Open()
                cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    cboEmpresa.SelectedValue = rdoReader(9)
                    txtFecIngreso.Text = CDate(rdoReader(2)).ToString("yyyy-MM-dd")
                    Call Antiguedad()
                    cboTipoContrato.SelectedValue = rdoReader(3)
                    If cboTipoContrato.SelectedValue = 1 Then
                        txtFecTerminoContrato.Enabled = False
                        txtFecTerminoContrato.Text = "01-01-1900"
                    Else
                        txtFecTerminoContrato.Enabled = True
                    End If
                    cboContratoAsociado.SelectedValue = rdoReader(5)

                    Call CargarTipoTrabajador()
                    cboTipoTrabajador.SelectedValue = rdoReader(7)

                    Call CargarSucursal()
                    cboSucursal.SelectedValue = rdoReader(11)
                    Call CargarArea()
                    cboArea.SelectedValue = rdoReader(13)
                    Call CargarSubArea()
                    cboSubArea.SelectedValue = rdoReader(15)
                    Call CargarDepartamento()
                    cboDepto.SelectedValue = rdoReader(17)
                    Call CargarCargo()
                    cboCargo.SelectedValue = rdoReader(19)
                    Call CargarClasificacionPago()
                    cboClaPago.SelectedValue = rdoReader(21)
                    cboFamiliaServicio.SelectedValue = rdoReader(23)
                    Call CargarIC()
                    'cboIC.SelectedValue = rdoReader(25)
                    cboCiudad.SelectedValue = rdoReader(27)
                    Call CargarJornada()
                    cboJornada.SelectedValue = rdoReader(29)
                    Call CargarDatosJornada()
                    If rdoReader(35) = True Then
                        cboPacto.SelectedValue = 1
                    Else
                        cboPacto.SelectedValue = 0
                    End If
                    ' cboPacto.SelectedValue = rdoReader(35)
                    txtFecTerminoContrato.Text = CDate(rdoReader(36)).ToString("yyyy-MM-dd")

                    If rdoReader(37) = True Then
                        cboActivo.SelectedValue = 1
                    Else
                        cboActivo.SelectedValue = 0
                    End If

                    If rdoReader(38) = 0 Then
                        txtFecFiniquito.Text = ""
                        txtCausal.Text = ""
                        lblCausal.Visible = False
                        lblFecFiniquito.Visible = False
                        txtFecFiniquito.Visible = False
                        txtCausal.Visible = False
                    Else
                        txtFecFiniquito.Text = rdoReader(39)
                        txtCausal.Text = rdoReader(40)
                        lblCausal.Visible = True
                        lblFecFiniquito.Visible = True
                        txtFecFiniquito.Visible = True
                        txtCausal.Visible = True
                    End If
                    cboMedioPago.SelectedValue = rdoReader(45)
                    cboBanco.SelectedValue = rdoReader(47)
                    txtNumCuenta.Text = Trim(rdoReader(49))
                    txtAntFijo.Text = Trim(rdoReader(50))

                    txtHrsDiarias.Text = Trim(rdoReader(32))
                    txtHrsSemanales.Text = Trim(rdoReader(33))
                    txtHrsMensuales.Text = Trim(rdoReader(34))

                    Call CargarGrillaIC()
                    For Each rowTEMP As GridViewRow In grdIC.Rows
                        Dim obj_cod_ic As HiddenField = CType(rowTEMP.FindControl("hdn_cod_ic"), HiddenField)
                        Dim obj_est_b As HiddenField = CType(rowTEMP.FindControl("hdn_est_b"), HiddenField)
                        Dim obj_IC As Label = CType(rowTEMP.FindControl("lblIc"), Label)
                        Dim obj_Beneficio As Label = CType(rowTEMP.FindControl("lblBeneficio"), Label)

                        If obj_est_b.Value = False Then
                            obj_est_b.Value = 0
                        Else
                            obj_est_b.Value = 1
                        End If

                        Call GuardarDetTempIC(obj_IC.Text, obj_cod_ic.Value, obj_Beneficio.Text, obj_est_b.Value)
                    Next
                Else
                End If
                txtSueldoBase.Text = Trim(rdoReader(51))
                rdoReader.Close()
                cmdTemporal.Dispose()
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
                cnxBaseDatos = Nothing
            Catch ex As Exception
                MessageBoxError("BuscarDatos", Err, Page, Master)
            End Try
        End If
    End Sub

    Private Sub GuardarDetTempIC(ByVal instrumento As String, ByVal cod_inst As String, ByVal beneficios As String, ByVal est_beneficios As String)
        Dim ocar As New DataTable
        ocar = Session("dtIC")
        With ocar.Rows.Add
            .Item("nom_ic") = Trim(instrumento)
            .Item("cod_ic") = Trim(cod_inst)
            .Item("beneficios") = Trim(beneficios)
            .Item("est_beneficio") = Trim(est_beneficios)
        End With
        grdIC.DataSource = ocar
        grdIC.DataBind()
    End Sub


    Private Sub CargarGrillaIC()
        Try
            Dim strNomTabla As String = "RRHHMae_grupo_familiar"
            Dim strSQL As String = "Exec mant_RRHH_ficha_contrato 'SIC', '" & Trim(lblRut.Text) & "'"
            grdIC.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdIC.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaIC", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtFecIngreso.Text = ""
        txtNumCuenta.Text = ""
        txtAntFijo.Text = ""
        Call CargarTipoContrato()
        Call CargarContratoAsociado()
        Call CargarTipoTrabajador()
        Call CargarEmpresa()
        Call CargarCargo()
        Call CargarClasificacionPago()
        Call CargarFamiliaServicios()
        ' Call CargarInstrumentoColectivo()
        Call CargarIC()
        Call CargarCiudad()
        Call CargarJornada()
        Call CargarMedioPago()
        Call CargarBancos()
        Call CargarSucursal()
        Call CargarArea()
        Call CargarSubArea()
        Call CargarDepartamento()
        Call CargarDatosJornada()
        txtAntiguedad.Text = ""
        txtHrsDiarias.Text = ""
        txtHrsMensuales.Text = ""
        txtHrsSemanales.Text = ""
        txtSueldoBase.Text = ""
        txtFecFiniquito.Text = ""
        txtFecTerminoContrato.Text = ""
        txtCausal.Text = ""
    End Sub

    Private Sub GuardarIC()
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Dim cmdTemporal As SqlCommand = cnxBaseDatos.CreateCommand()
            Dim trsBaseDatos As SqlTransaction
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Guardar_Det")
            cmdTemporal.Connection = cnxBaseDatos
            cmdTemporal.Transaction = trsBaseDatos
            'Try
            For iRow = 0 To grdIC.Rows.Count - 1

                Dim obj_cod_ic As HiddenField = CType(grdIC.Rows(iRow).FindControl("hdn_cod_ic"), HiddenField)
                Dim obj_est_b As HiddenField = CType(grdIC.Rows(iRow).FindControl("hdn_est_b"), HiddenField)

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "mant_RRHH_ficha_contrato"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "IIC"
                comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                comando.Parameters("@rut_personal").Value = lblRut.Text
                comando.Parameters.Add("@cod_instrumento_colectivo", SqlDbType.NVarChar)
                comando.Parameters("@cod_instrumento_colectivo").Value = obj_cod_ic.Value
                comando.Parameters.Add("@est_beneficios", SqlDbType.NVarChar)
                comando.Parameters("@est_beneficios").Value = obj_est_b.Value
                comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
                comando.Parameters("@usr_add").Value = Session("id_usu_tc")
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Next
            'Catch ex As Exception
            '    MessageBoxError("GuardarIC", Err, Page, Master)
            'End Try
        End Using
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If ValidarGuardado() = True Then
            Call Guardar()
            Call EliminarIC()
            Call GuardarIC()

            If hdn4Rol.Value = 1 Then
                Call Guardar4Rol()
            End If

            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
        Else
            Call Actualizar()
            Call EliminarIC()
            Call GuardarIC()

            Call VerificarExiste4Rol()

            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
        End If
    End Sub


    Private Sub Guardar4Rol()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_contrato"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I4R"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(lblRut.Text)
            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@fec_ingreso").Value = CDate(txtFecIngresoRol.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBoxError("Guardar4Rol", Err, Page, Master)
        End Try
    End Sub


    Private Sub Actualizar4Rol(ByVal vigencia As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_contrato"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U4R"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(lblRut.Text)
            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@fec_ingreso").Value = CDate(txtFecIngresoRol.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = vigencia
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBoxError("Actualizar4Rol", Err, Page, Master)
        End Try
    End Sub

    Private Sub Antiguedad()
        Dim mesdia = New Integer() {31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
        Dim desde As DateTime
        Dim hasta As DateTime
        Dim anio As Integer
        Dim mes As Integer
        Dim dia As Integer

        If CDate(txtFecIngreso.Text) > DateTime.Now Then
            desde = DateTime.Now
            hasta = CDate(txtFecIngreso.Text)

        Else
            desde = CDate(txtFecIngreso.Text)
            hasta = DateTime.Now
        End If

        Dim incrementar As Integer = 0
        If desde.Day > hasta.Day Then
            incrementar = mesdia(desde.Month - 1)
        End If

        If incrementar = 1 Then
            If DateTime.IsLeapYear(desde.Year) Then
                incrementar = 29
            Else
                incrementar = 28
            End If
        End If

        If incrementar <> 0 Then
            dia = (hasta.Day + incrementar) - desde.Day
            incrementar = 1
        Else
            dia = hasta.Day - desde.Day
        End If

        If (desde.Month + incrementar) > hasta.Month Then
            mes = (hasta.Month + 12) - (desde.Month + incrementar)
            incrementar = 1
        Else
            mes = (hasta.Month) - (desde.Month + incrementar)
            incrementar = 0
        End If

        anio = hasta.Year - (desde.Year + incrementar)
        txtAntiguedad.Text = CStr(anio) + " año(s) " + CStr(mes) + " mes(es) " + CStr(dia) + " dia(s) "
    End Sub

    Protected Sub txtFecIngreso_TextChanged(sender As Object, e As EventArgs) Handles txtFecIngreso.TextChanged
        Call Antiguedad()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub cboTipoContrato_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoContrato.SelectedIndexChanged
        If cboTipoContrato.SelectedValue = 1 Then
            txtFecTerminoContrato.Enabled = False
            txtFecTerminoContrato.Text = "01-01-1900"
        Else
            txtFecTerminoContrato.Enabled = True
        End If
    End Sub

    Protected Sub btnAgregarIC_Click(sender As Object, e As EventArgs) Handles btnAgregarIC.Click
        If cboBeneficios.SelectedValue = 9 Then
            MessageBox("Agregar Instrumento Colectivo", "Selecione I.C y Beneficios", Page, Master, "W")
        Else
            Call GuardarDetTempIC(cboInsColectivo.SelectedItem.ToString, cboInsColectivo.SelectedValue, cboBeneficios.SelectedItem.ToString, cboBeneficios.SelectedValue)
        End If
    End Sub

    Protected Sub cboInsColectivo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboInsColectivo.SelectedIndexChanged

    End Sub


End Class