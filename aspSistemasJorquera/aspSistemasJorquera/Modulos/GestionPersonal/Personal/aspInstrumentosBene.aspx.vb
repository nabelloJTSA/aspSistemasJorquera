﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspInstrumentosBene
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                lbltituloP.Text = "Instrumento"
                Call CargarEmpresa()
                Call CargarTipo()
                Call CargarGrilla()
                MultiView1.ActiveViewIndex = 0

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_cargo"
            Dim strSQL As String = "Exec mant_RRHH_instrumento_colectivo 'G', '" & cboEmpresa.SelectedValue & "'"
            grdIntrumento.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdIntrumento.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarEmpresaRelacion()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresaRelacion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaRelacion.DataTextField = "nom_empresa"
            cboEmpresaRelacion.DataValueField = "cod_empresa"
            cboEmpresaRelacion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresaRelacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipo()
        Try
            Dim strNomTablaR As String = "RRHHMae_tipo_instrumento"
            Dim strSQLR As String = "Exec mant_RRHH_instrumento_colectivo 'CTI'"
            cboTipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipo.DataTextField = "nom_tipo_instrumento"
            cboTipo.DataValueField = "id_tipo_instrumento"
            cboTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipo", Err, Page, Master)
        End Try

    End Sub
    Private Sub CargarInstrumento()
        Try
            Dim strNomTablaR As String = "RRHHMae_tipo_instrumento"
            Dim strSQLR As String = "Exec mant_RRHH_instrumento_colectivo 'CCI', '" & cboEmpresaRelacion.SelectedValue & "'"
            cboInstrumento.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInstrumento.DataTextField = "nom_instrumento"
            cboInstrumento.DataValueField = "cod_instrumento"
            cboInstrumento.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarInstrumento", Err, Page, Master)
        End Try

    End Sub


    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_instrumento_colectivo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@nom_instrumento").Value = Trim(txtNomInstrumento.Text)
            comando.Parameters.Add("@id_tipo_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_instrumento").Value = cboTipo.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_instrumento_colectivo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@cod_instrumento").Value = hdnId.Value
            comando.Parameters.Add("@nom_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@nom_instrumento").Value = Trim(txtNomInstrumento.Text)
            comando.Parameters.Add("@id_tipo_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_instrumento").Value = cboTipo.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If cboEmpresaRelacion.SelectedValue = 0 Then
            MessageBox("GuardarRelacion", "Debe seleccionar empresa", Page, Master, "S")
        Else
            If hdnActivo.Value = 0 Then
                Call Guardar()
            Else
                Call Actualizar()
            End If
        End If

    End Sub
    Private Sub Limpiar()
        cboEmpresa.ClearSelection()
        cboTipo.ClearSelection()
        txtNomInstrumento.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Private Sub CargarCargos()
        Try
            Dim strNomTabla As String = "RRHHMae_cargo"
            Dim strSQL As String = "Exec mant_RRHH_cargo 'G', '" & cboEmpresaRelacion.SelectedValue & "'"
            chkCargos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            chkCargos.DataTextField = "nom_cargo"
            chkCargos.DataValueField = "cod_cargo"
            chkCargos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCargos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdIntrumento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdIntrumento.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdIntrumento.SelectedRow
            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_emp As HiddenField = CType(row.FindControl("hdn_emp"), HiddenField)
            Dim obj_Nominstrumento As Label = CType(row.FindControl("lblInstrumento"), Label)
            Dim obj_idIntrumento As HiddenField = CType(row.FindControl("hdn_tipo"), HiddenField)
            hdnId.Value = obj_id.Value
            cboEmpresa.SelectedValue = obj_emp.Value
            txtNomInstrumento.Text = obj_Nominstrumento.Text
            cboTipo.SelectedValue = obj_idIntrumento.Value
            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdIntrumento_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla()
    End Sub

    Protected Sub btnInstrumento_Click(sender As Object, e As EventArgs) Handles btnInstrumento.Click
        MultiView1.ActiveViewIndex = 0
        lbltituloP.Text = "Instrumentos"
    End Sub

    Protected Sub btnRelacion_Click(sender As Object, e As EventArgs) Handles btnRelacion.Click
        MultiView1.ActiveViewIndex = 1
        lbltituloP.Text = "Relación"
        Call CargarEmpresaRelacion()
        Call CargarInstrumento()
        Call CargarCargos()
    End Sub

    Protected Sub cboEmpresaRelacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaRelacion.SelectedIndexChanged
        Call CargarInstrumento()
        Call CargarCargos()
    End Sub

    Public Sub GuardarRelacion(ByVal cargo As Integer, ByVal estado As Boolean)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_instrumento_colectivo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IIC"
            comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@cod_instrumento").Value = cboInstrumento.SelectedValue
            comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
            comando.Parameters("@cod_cargo").Value = cargo
            comando.Parameters.Add("@est_activo", SqlDbType.NVarChar)
            comando.Parameters("@est_activo").Value = estado
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("GuardarRelacion", "Registros Guardados", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Public Sub RecorrerCheck()

        For intCheck = 0 To chkCargos.Items.Count - 1
            Call GuardarRelacion(chkCargos.Items(intCheck).Value, chkCargos.Items(intCheck).Selected)
        Next

    End Sub
    Protected Sub btnGuardarRelacion_Click(sender As Object, e As EventArgs) Handles btnGuardarRelacion.Click
        If cboEmpresaRelacion.SelectedValue = 0 Or cboInstrumento.SelectedValue = 0 Then
            MessageBox("GuardarRelacion", "Debe seleccionar empresa e instrumento", Page, Master, "S")
        Else
            Call RecorrerCheck()
        End If
    End Sub

    Public Sub RecorrerCheckActivos()

        For intCheck = 0 To chkCargos.Items.Count - 1
            Call CargarRelacion(chkCargos.Items(intCheck).Value, cboInstrumento.SelectedValue, intCheck)
        Next

    End Sub

    Public Sub CargarRelacion(ByVal cargo As Integer, ByVal instrumento As Integer, ByVal posicion As Integer)
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec mant_RRHH_instrumento_colectivo 'CIC', '" & cargo & "', '" & instrumento & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()

            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                chkCargos.Items(posicion).Selected = rdoReader(1)
            End If
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("CargarRelacion", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboInstrumento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboInstrumento.SelectedIndexChanged
        Call RecorrerCheckActivos()
    End Sub




    '***************************
    Protected Sub btnBeneficios_Click(sender As Object, e As EventArgs) Handles btnBeneficios.Click
        MultiView1.ActiveViewIndex = 2
        lbltituloP.Text = "Beneficios"
        Call CargarEmpresaBN()
        Call CargarDeptoBN()
        Call CargarCargoBN()
        Call CargarInstrumentoBN()
        Call CargarItemBN()
    End Sub


    Private Sub CargarDeptoBN()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCD', '" & cboEmpresaBN.SelectedValue & "'"
            cboDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboDepto.DataTextField = "nom_departamento"
            cboDepto.DataValueField = "cod_departamento"
            cboDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresaBN()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Beneficio 'CCE'"
            cboEmpresaBN.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaBN.DataTextField = "nom_empresa"
            cboEmpresaBN.DataValueField = "cod_empresa"
            cboEmpresaBN.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarItemBN()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Beneficio 'CCI','" & cboEmpresaBN.SelectedValue & "'"
            cboItem.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboItem.DataTextField = "nom_item"
            cboItem.DataValueField = "id_item"
            cboItem.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarItem", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarCargoBN()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Beneficio 'CCC','" & cboInstrumentoBN.SelectedValue & "'"
            cboCargo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCargo.DataTextField = "nom_cargo"
            cboCargo.DataValueField = "cod_cargo"
            cboCargo.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarCargo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarInstrumentoBN()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Beneficio 'CIC','" & cboEmpresaBN.SelectedValue & "'"
            cboInstrumentoBN.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInstrumentoBN.DataTextField = "nom_instrumento"
            cboInstrumentoBN.DataValueField = "cod_instrumento"
            cboInstrumentoBN.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarInstrumento", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarBeneficiosBN()
        Try
            Dim tipo As String
            If cboEmpresaBN.SelectedValue = 1 Then
                tipo = "GJT"
            Else
                tipo = "G"
            End If
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_Beneficio '" & tipo & "','" & cboInstrumentoBN.SelectedValue & "','" & cboCargo.SelectedValue & "','" & cboDepto.SelectedValue & "'"
            grdBeneficios.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdBeneficios.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarBeneficios", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresaBN_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaBN.SelectedIndexChanged
        Try
            Call CargarCargoBN()
            Call CargarInstrumentoBN()
            Call CargarItemBN()

            If cboEmpresaBN.SelectedValue = 1 Then
                Call CargarDeptoBN()
                lblDeptoBN.Visible = True
                cboDepto.Visible = True
            Else
                lblDeptoBN.Visible = False
                cboDepto.Visible = False
            End If
            Call CargarBeneficiosBN()
            txtDescripcion.Text = cboItem.SelectedItem.ToString()
        Catch ex As Exception
            MessageBoxError("CargarBeneficios", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarBN()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_Beneficio"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "I"
        comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
        comando.Parameters("@cod_instrumento").Value = cboInstrumentoBN.SelectedValue
        comando.Parameters.Add("@id_item", SqlDbType.NVarChar)
        comando.Parameters("@id_item").Value = cboItem.SelectedValue
        comando.Parameters.Add("@des_beneficio", SqlDbType.NVarChar)
        comando.Parameters("@des_beneficio").Value = txtDescripcion.Text
        comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
        comando.Parameters("@num_valor").Value = txtValor.Text
        comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
        comando.Parameters("@cod_cargo").Value = cboCargo.SelectedValue
        comando.Parameters.Add("@num_antiguedad_necesaria", SqlDbType.NVarChar)
        comando.Parameters("@num_antiguedad_necesaria").Value = txtAntiguedad.Text
        If txtAntiguedaF.Text = "" Or txtAntiguedaF.Text = "0" Then
            txtAntiguedaF.Text = "99"
        End If
        comando.Parameters.Add("@num_antiguedad_necesaria_fin", SqlDbType.NVarChar)
        comando.Parameters("@num_antiguedad_necesaria_fin").Value = txtAntiguedaF.Text
        comando.Parameters.Add("@num_pago_desfase", SqlDbType.NVarChar)
        comando.Parameters("@num_pago_desfase").Value = txtDesfase.Text
        comando.Parameters.Add("@est_antiguedad", SqlDbType.NVarChar)
        comando.Parameters("@est_antiguedad").Value = chkAntiguedad.Checked
        comando.Parameters.Add("@est_falla_accidente", SqlDbType.NVarChar)
        comando.Parameters("@est_falla_accidente").Value = chkFallaAcc.Checked
        comando.Parameters.Add("@falla_accidente_control1", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control1").Value = txtControlAcc1.Text
        comando.Parameters.Add("@falla_accidente_valor1", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor1").Value = txtValorAcc1.Text
        comando.Parameters.Add("@falla_accidente_control2", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control2").Value = txtControlAcc2.Text
        comando.Parameters.Add("@falla_accidente_valor2", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor2").Value = txtValorAcc2.Text
        comando.Parameters.Add("@falla_accidente_control3", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control3").Value = txtControlAcc3.Text
        comando.Parameters.Add("@falla_accidente_valor3", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor3").Value = txtValorAcc3.Text
        comando.Parameters.Add("@est_produccion", SqlDbType.NVarChar)
        comando.Parameters("@est_produccion").Value = chkProduccion.Checked
        comando.Parameters.Add("@num_lim_produccion", SqlDbType.NVarChar)
        comando.Parameters("@num_lim_produccion").Value = txtTopeProduccion.Text
        comando.Parameters.Add("@val_lim_produccion", SqlDbType.NVarChar)
        comando.Parameters("@val_lim_produccion").Value = Replace(txtPrecioTope.Text, ",", ".")
        comando.Parameters.Add("@num_dif_produccion", SqlDbType.NVarChar)
        comando.Parameters("@num_dif_produccion").Value = txtMinimoProduccion.Text
        If cboEmpresaBN.SelectedValue = 1 Then
            comando.Parameters.Add("@cod_depto", SqlDbType.NVarChar)
            comando.Parameters("@cod_depto").Value = cboDepto.SelectedValue
        End If

        comando.Parameters.Add("@val_dif_produccion", SqlDbType.NVarChar)
        comando.Parameters("@val_dif_produccion").Value = txtPrecioMenor.Text
        comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
        comando.Parameters("@usr_add").Value = Session("id_usu_tc")
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
        Call CargarBeneficiosBN()

    End Sub

    Private Sub ActualizarBN()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_Beneficio"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "U"
        comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
        comando.Parameters("@cod_instrumento").Value = cboInstrumentoBN.SelectedValue
        comando.Parameters.Add("@id_item", SqlDbType.NVarChar)
        comando.Parameters("@id_item").Value = cboItem.SelectedValue
        comando.Parameters.Add("@des_beneficio", SqlDbType.NVarChar)
        comando.Parameters("@des_beneficio").Value = txtDescripcion.Text
        comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
        comando.Parameters("@num_valor").Value = txtValor.Text
        comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
        comando.Parameters("@cod_cargo").Value = cboCargo.SelectedValue
        comando.Parameters.Add("@num_antiguedad_necesaria", SqlDbType.NVarChar)
        comando.Parameters("@num_antiguedad_necesaria").Value = txtAntiguedad.Text
        If txtAntiguedaF.Text = "" Or txtAntiguedaF.Text = "0" Then
            txtAntiguedaF.Text = "99"
        End If
        comando.Parameters.Add("@num_antiguedad_necesaria_fin", SqlDbType.NVarChar)
        comando.Parameters("@num_antiguedad_necesaria_fin").Value = txtAntiguedaF.Text
        comando.Parameters.Add("@num_pago_desfase", SqlDbType.NVarChar)
        comando.Parameters("@num_pago_desfase").Value = txtDesfase.Text
        comando.Parameters.Add("@est_antiguedad", SqlDbType.NVarChar)
        comando.Parameters("@est_antiguedad").Value = chkAntiguedad.Checked
        comando.Parameters.Add("@est_falla_accidente", SqlDbType.NVarChar)
        comando.Parameters("@est_falla_accidente").Value = chkFallaAcc.Checked
        comando.Parameters.Add("@falla_accidente_control1", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control1").Value = txtControlAcc1.Text
        comando.Parameters.Add("@falla_accidente_valor1", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor1").Value = txtValorAcc1.Text
        comando.Parameters.Add("@falla_accidente_control2", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control2").Value = txtControlAcc2.Text
        comando.Parameters.Add("@falla_accidente_valor2", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor2").Value = txtValorAcc2.Text
        comando.Parameters.Add("@falla_accidente_control3", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_control3").Value = txtControlAcc3.Text
        comando.Parameters.Add("@falla_accidente_valor3", SqlDbType.NVarChar)
        comando.Parameters("@falla_accidente_valor3").Value = txtValorAcc3.Text
        comando.Parameters.Add("@est_produccion", SqlDbType.NVarChar)
        comando.Parameters("@est_produccion").Value = chkProduccion.Checked
        comando.Parameters.Add("@num_lim_produccion", SqlDbType.NVarChar)
        comando.Parameters("@num_lim_produccion").Value = txtTopeProduccion.Text
        comando.Parameters.Add("@val_lim_produccion", SqlDbType.NVarChar)
        comando.Parameters("@val_lim_produccion").Value = Replace(txtPrecioTope.Text, ",", ".")
        comando.Parameters.Add("@num_dif_produccion", SqlDbType.NVarChar)
        comando.Parameters("@num_dif_produccion").Value = txtMinimoProduccion.Text
        comando.Parameters.Add("@val_dif_produccion", SqlDbType.NVarChar)
        comando.Parameters("@val_dif_produccion").Value = txtPrecioMenor.Text
        If cboEmpresaBN.SelectedValue = 1 Then
            comando.Parameters.Add("@cod_depto", SqlDbType.NVarChar)
            comando.Parameters("@cod_depto").Value = cboDepto.SelectedValue
        End If
        comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
        comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = hdnIdBeneficio.Value
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
        Call CargarBeneficiosBN()
    End Sub

    Protected Sub btnGuardarBN_Click(sender As Object, e As EventArgs) Handles btnGuardarBN.Click
        If hdnIdBeneficio.Value > 0 Then
            Call ActualizarBN()
            Call CargarBeneficiosBN()
            MessageBox("Beneficios", "Registro Actualizado", Page, Master, "S")
            hdnIdBeneficio.Value = 0

        Else
            Call GuardarBN()
            Call CargarBeneficiosBN()
            MessageBox("Beneficios", "Registro Guardado", Page, Master, "S")
            hdnIdBeneficio.Value = 0
        End If

    End Sub

    Protected Sub chkAntiguedad_CheckedChanged(sender As Object, e As EventArgs) Handles chkAntiguedad.CheckedChanged
        If chkAntiguedad.Checked = True Then
            pnlAntiguedad.Visible = True
        Else
            pnlAntiguedad.Visible = False
        End If
    End Sub
    Protected Sub chkFallaAcc_CheckedChanged(sender As Object, e As EventArgs) Handles chkFallaAcc.CheckedChanged
        If chkFallaAcc.Checked = True Then
            pnlAccidenteAusencia.Visible = True
        Else
            pnlAccidenteAusencia.Visible = False
        End If
    End Sub

    Protected Sub chkProduccion_CheckedChanged(sender As Object, e As EventArgs) Handles chkProduccion.CheckedChanged
        If chkProduccion.Checked = True Then
            pnlProduccion.Visible = True
        Else
            pnlProduccion.Visible = False
        End If
    End Sub

    Protected Sub cboCargo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCargo.SelectedIndexChanged
        Call CargarBeneficiosBN()
    End Sub

    Protected Sub grdBeneficios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdBeneficios.SelectedIndexChanged
        Dim row As GridViewRow = grdBeneficios.SelectedRow
        cboInstrumentoBN.SelectedValue = CType(row.FindControl("hdnCodInstrumento"), HiddenField).Value
        cboCargo.SelectedValue = CType(row.FindControl("hdnCodCargo"), HiddenField).Value
        cboItem.SelectedValue = CType(row.FindControl("hdnIdItem"), HiddenField).Value
        txtDescripcion.Text = CType(row.FindControl("hdnDescripcion"), HiddenField).Value
        txtValor.Text = CType(row.FindControl("lblValor"), Label).Text
        chkAntiguedad.Checked = CType(row.FindControl("chkAntiguedad"), CheckBox).Checked
        hdnIdBeneficio.Value = CType(row.FindControl("hdnIdBeneficios"), HiddenField).Value
        lblIdBeneficio.Text = CType(row.FindControl("hdnIdBeneficios"), HiddenField).Value
        If chkAntiguedad.Checked = True Then
            pnlAntiguedad.Visible = True
            txtAntiguedad.Text = CType(row.FindControl("hdnAntiguedadNecesaria"), HiddenField).Value
            txtDesfase.Text = CType(row.FindControl("hdnPagoDesfase"), HiddenField).Value
            txtAntiguedaF.Text = CType(row.FindControl("hdnAntiguedadNecesariaFin"), HiddenField).Value
        Else
            pnlAntiguedad.Visible = False
            txtAntiguedad.Text = ""
            txtAntiguedaF.Text = ""
            txtDesfase.Text = ""
        End If
        chkFallaAcc.Checked = CType(row.FindControl("chkFalla"), CheckBox).Checked
        If chkFallaAcc.Checked = True Then
            pnlAccidenteAusencia.Visible = True
            txtControlAcc1.Text = CType(row.FindControl("hdnFallaControl1"), HiddenField).Value
            txtValorAcc1.Text = CType(row.FindControl("hdnFallaValor1"), HiddenField).Value
            txtControlAcc2.Text = CType(row.FindControl("hdnFallaControl2"), HiddenField).Value
            txtValorAcc2.Text = CType(row.FindControl("hdnFallaValor2"), HiddenField).Value
            txtControlAcc3.Text = CType(row.FindControl("hdnFallaControl3"), HiddenField).Value
            txtValorAcc3.Text = CType(row.FindControl("hdnFallaValor3"), HiddenField).Value
        Else
            pnlAccidenteAusencia.Visible = False
            txtControlAcc1.Text = ""
            txtControlAcc2.Text = ""
            txtControlAcc3.Text = ""
            txtValorAcc1.Text = ""
            txtValorAcc2.Text = ""
            txtValorAcc3.Text = ""
        End If
        chkProduccion.Checked = CType(row.FindControl("chkProduccion"), CheckBox).Checked
        If chkProduccion.Checked = True Then
            pnlProduccion.Visible = True
            txtTopeProduccion.Text = CType(row.FindControl("hdnNumLimiteP"), HiddenField).Value
            txtPrecioTope.Text = CType(row.FindControl("hdnValorLimiteP"), HiddenField).Value
            txtMinimoProduccion.Text = CType(row.FindControl("hdnNumDiferenciaP"), HiddenField).Value
            txtPrecioMenor.Text = CType(row.FindControl("hdnValorDiferenciaP"), HiddenField).Value
        Else
            pnlProduccion.Visible = False
            txtTopeProduccion.Text = ""
            txtPrecioTope.Text = ""
            txtMinimoProduccion.Text = ""
            txtPrecioMenor.Text = ""
        End If
    End Sub

    Private Sub cboInstrumentoBN_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboInstrumentoBN.SelectedIndexChanged
        Call CargarBeneficiosBN()
        Call CargarCargoBN()
    End Sub

    Private Sub btnCancelarBN_Click(sender As Object, e As EventArgs) Handles btnCancelarBN.Click


        Call LimpiarBN()
    End Sub

    Public Sub LimpiarBN()
        Call CargarEmpresaBN()
        Call CargarCargoBN()
        Call CargarInstrumentoBN()
        Call CargarDeptoBN()
        Call CargarItemBN()
        grdBeneficios.DataBind()
        txtDescripcion.Text = ""
        txtValor.Text = ""
        chkAntiguedad.Checked = False
        chkFallaAcc.Checked = False
        chkProduccion.Checked = False
        txtAntiguedad.Text = ""
        txtAntiguedaF.Text = ""
        txtDesfase.Text = ""
        txtControlAcc1.Text = ""
        txtValorAcc1.Text = ""
        txtControlAcc2.Text = ""
        txtValorAcc2.Text = ""
        txtControlAcc3.Text = ""
        txtValorAcc3.Text = ""
        txtTopeProduccion.Text = ""
        txtPrecioTope.Text = ""
        txtMinimoProduccion.Text = ""
        txtPrecioMenor.Text = ""
        pnlAntiguedad.Visible = False
        pnlAccidenteAusencia.Visible = False
        pnlProduccion.Visible = False
        hdnIdBeneficio.Value = ""
    End Sub


    Function verificarExistencia() As Boolean

        Dim tipo As String
        If cboEmpresa.SelectedValue = 1 Then
            tipo = "VJT"
        Else
            tipo = "V"
        End If

        Dim strSCodU As String = "Exec mant_RRHH_Beneficio '" & tipo & "','','','','" & cboDepto.SelectedValue & "','','" & cboInstrumentoBN.SelectedValue & "','" & cboItem.SelectedValue & "','','','" & cboCargo.SelectedValue & "'"

        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)

        cnxBaseDatos.Open()
        cmdTemporal = New SqlCommand(strSCodU, cnxBaseDatos)
        rdoReader = cmdTemporal.ExecuteReader()

        If rdoReader.Read() Then
            Return True
        Else
            Return False
        End If

        rdoReader.Close()
        cmdTemporal.Dispose()
        rdoReader = Nothing
        cmdTemporal = Nothing
    End Function

    Private Sub cboItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboItem.SelectedIndexChanged
        txtDescripcion.Text = cboItem.SelectedItem.ToString()
    End Sub

    Protected Sub cboDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDepto.SelectedIndexChanged
        Call CargarBeneficiosBN()
    End Sub
End Class