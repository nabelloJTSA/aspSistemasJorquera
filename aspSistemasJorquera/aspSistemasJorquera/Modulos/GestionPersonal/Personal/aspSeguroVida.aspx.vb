﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspSeguroVida
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Ingresar Personas"
                MtvPrincipal.ActiveViewIndex = 0
                Call CargarPeriodo()
                Call CargarComboBox()
                Call RescatarUF()
                txtuf.Text = hdnUFActual.Value
                Call IniControles()
                Call CargarGrillaP()
                Call CargarPoliza()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarUF()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        'Dim periodoAnt As String = ""
        'If Right(lblperiodoactivo.Text, 2) = "01" Then
        '    periodoAnt = Left(lblperiodoactivo.Text, 4) - 1 + "12"
        'Else
        '    periodoAnt = lblperiodoactivo.Text - 1
        'End If

        Dim strSQL As String = "select num_valor from PERMae_uf_seguros where cod_periodo = '" & lblperiodoactivo.Text & "' and tip_seguro = 1"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnUFActual.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarUF", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub CargarValores()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader

        comando.CommandText = "Exec sel_datos_seguros_vida 'VS', '', '" & cboempresa.SelectedValue & "', '" & cboseguro.SelectedValue & "', '" & lblperiodoactivo.Text & "', 0"
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            txtexentosvida.Text = leer("val_exento_segvida_uf")
            txtexentosmuerte.Text = leer("val_exento_segmuerte_uf")
            txtafectoinvalidez.Text = leer("val_afecto_seginvalidez_uf")
        End If
        leer.Close()
        conx.Close()
    End Sub

    Private Sub IniControles()
        Try
            Call CargarGrillaN()
            txtrut.Text = ""
            txtnombre.Text = ""
            txtvaloruf.Text = ""
            txtcontacto1.Text = ""
            txtcelularcto1.Text = ""
            txtcontacto2.Text = ""
            txtcelularcto2.Text = ""
            txtbusqueda.Text = ""
            txtuf.Text = ""
            cbotiposeguro.ClearSelection()
        Catch ex As Exception
            MessageBoxError("IniControles", Err, Page, Master)
        End Try
    End Sub


    Private Sub EnviarCorreoValidacion()
        Dim strTitulo As String, strMensaje As String, strAsunto As String
        Try
            strTitulo = "Nómina Validada"
            strMensaje = "Nómina de Empresa " & cboempresa.SelectedItem.Text & " y " & cboseguro.SelectedItem.Text & " Validada para Proceso de Exportación y Cargar a FIN700"
            strAsunto = "Nómina " & cboempresa.SelectedItem.Text & " " & cboseguro.SelectedItem.Text
            Call SentMail("carolina.pradenas@jtsa.cl", "", strAsunto, strMensaje)

        Catch ex As Exception
            MessageBoxError("EnviarCorreoValidacion", Err, Page, Master)
        End Try
    End Sub
    Private Sub grdNominaSegV_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdNominaSegV.PageIndexChanging
        Try
            grdNominaSegV.PageIndex = e.NewPageIndex

            Call CargarGrillaN()
        Catch ex As Exception
            MessageBoxError("grdNominaSegV_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdNominaSegV_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdNominaSegV.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then

                Dim obj_fec_nacto As HiddenField = CType(row.FindControl("hdnFecNacto"), HiddenField)
                Dim control As Integer = 0

                Dim edad As Integer

                edad = Year(Date.Now.Date) - Year(CDate(obj_fec_nacto.Value))

                If CType(row.FindControl("hdnvigente"), HiddenField).Value = False Then
                    control = 1
                    row.BackColor = Drawing.Color.Gold
                    CType(row.FindControl("btnEliminar"), LinkButton).ForeColor = Drawing.Color.White
                End If

                If edad >= 65 Then
                    control = control + 1
                    row.BackColor = Drawing.Color.Coral
                    CType(row.FindControl("btnEliminar"), LinkButton).ForeColor = Drawing.Color.White
                End If

                If control = 2 Then
                    row.BackColor = Drawing.Color.Salmon
                    CType(row.FindControl("btnEliminar"), LinkButton).ForeColor = Drawing.Color.White
                End If

                If control = 0 Then
                    row.BackColor = Drawing.Color.White
                    CType(row.FindControl("btnEliminar"), LinkButton).ForeColor = Nothing
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdNominaSegV_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdNominaSegV_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdNominaSegV.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "2" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdNominaSegV.Rows(intRow)
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand

                Dim objTSeguro As HiddenField = CType(row.FindControl("hdntseguro"), HiddenField)
                Dim objEmpresa As HiddenField = CType(row.FindControl("hdnempresa"), HiddenField)
                Dim objRut As HiddenField = CType(row.FindControl("hdnrut"), HiddenField)
                Dim hdnactivo As HiddenField = CType(row.FindControl("hdnactivo"), HiddenField)

                If hdnactivo.Value = "True" Then
                    hdnactivo.Value = 1
                ElseIf hdnactivo.Value = "False" Then
                    hdnactivo.Value = 0
                End If

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.sel_datos_seguros_vida"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "EN"
                comando.Parameters.Add("@buscar", SqlDbType.NVarChar)
                comando.Parameters("@buscar").Value = objRut.Value
                comando.Parameters.Add("@empresa", SqlDbType.Int)
                comando.Parameters("@empresa").Value = objEmpresa.Value
                comando.Parameters.Add("@tseguro", SqlDbType.Int)
                comando.Parameters("@tseguro").Value = objTSeguro.Value
                comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
                comando.Parameters("@periodo").Value = lblperiodoactivo.Text
                comando.Parameters.Add("@est_activo", SqlDbType.Bit)
                comando.Parameters("@est_activo").Value = CInt(hdnactivo.Value)
                comando.Connection = conx

                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                Call CargarGrillaN()
                Call IniControles()
            End If
        Catch ex As Exception
            MessageBoxError("grdNominaSegV_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub ValidarNomina()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim sqlString As String = "Exec sel_datos_seguros_vida 'NFE', '', '" & cboempresa.SelectedValue & "', '" & cboseguro.SelectedValue & "', '" & lblperiodoactivo.Text & "', 0"
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            If leer("bit_activo") = True Then
                btnvalidar.BackColor = Drawing.Color.Green
                btnvalidar.ForeColor = Drawing.Color.White
                btnvalidar.Text = "Nómina Validada"
                If leer("bit_exportar") = True Then
                    btnvalidar.Enabled = False
                ElseIf leer("bit_exportar") = False Then
                    btnvalidar.Enabled = True
                End If

                Call EnviarCorreoValidacion()
            ElseIf leer("bit_activo") = False Then
                btnvalidar.BackColor = Drawing.Color.Red
                btnvalidar.ForeColor = Drawing.Color.White
                btnvalidar.Text = "Validar Nómina"

                If leer("bit_exportar") = True Then
                    btnvalidar.Enabled = False
                ElseIf leer("bit_exportar") = False Then
                    btnvalidar.Enabled = True
                End If
            End If
        Else
            btnvalidar.BackColor = Drawing.Color.Red
            btnvalidar.ForeColor = Drawing.Color.White
            btnvalidar.Text = "Validar Nómina"
            Call EnviarCorreoValidacion()
        End If
        leer.Close()
        conx.Close()
    End Sub


    Private Sub CargarComboBox()
        Dim strNomTabla As String = "PERMae_tipo_seguro"
        Dim strSQL As String = "Exec sel_datos_seguros_vida 'TS'"
        Dim strNomTablaE As String = "REMMae_empresa"
        Dim strSQLE As String = "Select cod_empresa, nom_empresa From dbo.REMMae_empresa"
        Try
            cbotiposeguro.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            cbotiposeguro.DataTextField = "nom_tipo_seguro"
            cbotiposeguro.DataValueField = "cod_tipo_seguro"
            cbotiposeguro.DataBind()

            cboseguro.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            cboseguro.DataTextField = "nom_tipo_seguro"
            cboseguro.DataValueField = "cod_tipo_seguro"
            cboseguro.DataBind()

            cboempresa.DataSource = dtsTablas(strSQLE, Trim(strNomTablaE), "cnxBaseDatos").Tables(Trim(strNomTablaE)).DefaultView
            cboempresa.DataTextField = "nom_empresa"
            cboempresa.DataValueField = "cod_empresa"
            cboempresa.DataBind()

            'cboEmpresaComparar.DataSource = dtsTablas(strSQLE, Trim(strNomTablaE), "cnxBaseDatos").Tables(Trim(strNomTablaE)).DefaultView
            'cboEmpresaComparar.DataTextField = "nom_empresa"
            'cboEmpresaComparar.DataValueField = "cod_empresa"
            'cboEmpresaComparar.DataBind()


        Catch ex As Exception
            MessageBoxError("CargarComboBox", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnPersonas_Click(sender As Object, e As EventArgs) Handles btnPersonas.Click
        lbltitulo.Text = "Ingresar Personas"
        MtvPrincipal.ActiveViewIndex = 0
    End Sub

    Protected Sub btnNomina_Click(sender As Object, e As EventArgs) Handles btnNomina.Click
        lbltitulo.Text = "Nomina Seguros"
        Call CargarGrillaN()
        Call CargarValores()
        Call RescatarUF()
        txtuf.Text = hdnUFActual.Value
        'Call ValidarNomina()
        MtvPrincipal.ActiveViewIndex = 1
    End Sub

    'Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
    '    MtvPrincipal.ActiveViewIndex = 2
    'End Sub
    Private Sub CargarGrillaP()
        Dim strSQL As String = "Exec sel_datos_seguros_vida 'P', '" & txtbusqueda.Text & "', 0, 0, '" & lblperiodoactivo.Text & "'"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            grdBuscar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdBuscar.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarGrillaP", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtbusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtbusqueda.TextChanged
        Try
            Call CargarGrillaP()
        Catch ex As Exception
            MessageBoxError("btnbuscarr_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader

        comando.CommandText = "Select top 1 num_periodo From JQRSGT..PERMov_nomina_seguro_vida Order By num_periodo desc"
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            lblperiodoactivo.Text = leer("num_periodo")

            If Right(CInt(lblperiodoactivo.Text), 2) = "12" Then
                lblperiodonuevo.Text = CInt(Left(lblperiodoactivo.Text, 4) + 1) & "01"
            Else
                lblperiodonuevo.Text = CInt(lblperiodoactivo.Text) + 1
            End If
        End If
        leer.Close()
        conx.Close()
    End Sub

    Private Sub CargarPoliza()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim sqlString As String = "sel_datos_seguros_vida 'CPS',''," & cboempresa.SelectedValue & "," & cboseguro.SelectedValue & ""
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            txtPoliza.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub


    Protected Sub chkvidacamara_actualizar(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)

        Call ActualizarVC(CType(row.FindControl("hdnempresa"), HiddenField).Value, CType(row.FindControl("chkvidacamara"), CheckBox).Checked, CType(row.FindControl("hdnrut"), HiddenField).Value)
        Call CargarGrillaP()
    End Sub

    Private Sub ActualizarVC(ByVal empresa As Integer, ByVal activo As Boolean, ByVal rut As String)
        Dim rdoReader As SqlDataReader, trsBaseDatos As SqlTransaction
        Dim cmdTemporal As SqlCommand, intRow As Integer
        Dim strSQL As String = "Select 1 From PERMov_nomina_vida_camara Where rut_personal = '" & rut & "' and num_periodo='" & lblperiodoactivo.Text & "' and cod_empresa='" & empresa & "' "
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Grabar")
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                cmdTemporal.Transaction = trsBaseDatos
                rdoReader = cmdTemporal.ExecuteReader()

                If rdoReader.Read Then
                    strSQL = "Update JQRSGT.dbo.PERMov_nomina_vida_camara set act_vida_camara='" & activo & "' Where rut_personal='" & rut & "' and num_periodo='" & lblperiodoactivo.Text & "' and cod_empresa='" & empresa & "' "
                Else
                    strSQL = "INSERT INTO PERMov_nomina_vida_camara(rut_personal, act_vida_camara, num_periodo, cod_empresa) " &
                    "Values('" & rut & "', '" & activo & "', '" & lblperiodoactivo.Text & "', '" & empresa & "')"
                End If
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing

                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                cmdTemporal.Transaction = trsBaseDatos
                intRow = cmdTemporal.ExecuteNonQuery()
                If intRow < 1 Then
                    MessageBox("Actualizar", "Proceso ha concluido con errores, verifiquelo.", Page, Master, "E")
                End If
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
                trsBaseDatos.Commit()

            Catch ex As Exception
                trsBaseDatos.Rollback()
                MessageBoxError("ActualizarVC", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                trsBaseDatos.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
    End Sub

    Protected Sub chkfalp_actualizar(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)

        Call ActualizarF(CType(row.FindControl("hdnempresa"), HiddenField).Value, CType(row.FindControl("chkfalp"), CheckBox).Checked, CType(row.FindControl("hdnrut"), HiddenField).Value)
        Call CargarGrillaP()
    End Sub


    Private Sub ActualizarF(ByVal empresa As Integer, ByVal activo As Boolean, ByVal rut As String)

        Dim rdoReader As SqlDataReader, trsBaseDatos As SqlTransaction
        Dim cmdTemporal As SqlCommand, intRow As Integer
        Dim strSQL As String = "Select 1 From PERMov_nomina_falp Where rut_personal = '" & rut & "' and num_periodo='" & lblperiodoactivo.Text & "' and cod_empresa='" & empresa & "' "
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            trsBaseDatos = cnxBaseDatos.BeginTransaction("Grabar")
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                cmdTemporal.Transaction = trsBaseDatos
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    strSQL = "Update JQRSGT.dbo.PERMov_nomina_falp set act_falp='" & activo & "' Where rut_personal='" & rut & "' and num_periodo='" & lblperiodoactivo.Text & "' and cod_empresa='" & empresa & "' "
                Else
                    strSQL = "INSERT INTO JQRSGT.dbo.PERMov_nomina_falp(rut_personal, act_falp, num_periodo, cod_empresa) " &
                    "Values('" & rut & "', '" & activo & "', '" & lblperiodoactivo.Text & "', '" & empresa & "')"
                End If
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing

                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                cmdTemporal.Transaction = trsBaseDatos
                intRow = cmdTemporal.ExecuteNonQuery()
                If intRow < 1 Then
                    MessageBox("Actualizar", "Proceso ha concluido con errores, verifiquelo.", Page, Master, "E")
                End If
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
                trsBaseDatos.Commit()


            Catch ex As Exception
                trsBaseDatos.Rollback()
                MessageBoxError("ActualizarF", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                trsBaseDatos.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
    End Sub

    Private Sub grdBuscar_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBuscar.RowCommand
        Dim blnBloquear As Boolean = False
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdBuscar.Rows(index)
        hdnEmpresa.Value = CType(row.FindControl("hdnempresa"), HiddenField).Value
        Dim cmdTemporal As SqlCommand, rdoReader As SqlDataReader
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim strSQL As String = "Select num_asegurado From PERMov_nomina_seguro_vida Where rut_personal='" & row.Cells(1).Text & "'and num_periodo='" & lblperiodoactivo.Text & "' and est_activo=1 and cod_empresa='" & hdnEmpresa.Value & "' and (cod_tipo_seguro=1  or cod_tipo_seguro=3 or cod_tipo_seguro=4)"
        Dim strSQLCodigo As String

        cnxBaseDatos.Open()
        Try

            strSQLCodigo = strSQL
            cmdTemporal = New SqlCommand(strSQLCodigo, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()

            If rdoReader.Read() Then

                MessageBox("Seleccionar", "Esta persona ya tiene seguros asociados", Page, Master, "W")
                btnConfirmar.OnClientClick = "return confirm('¿Desea guardar el nuevo seguro ?');" 'asigna el atributo onclick aun objeto que no lo posee  
                txtrut.Text = row.Cells(1).Text
                txtnombre.Text = HttpUtility.HtmlDecode(row.Cells(2).Text)
                hdnEmpresa.Value = CType(row.FindControl("hdnempresa"), HiddenField).Value
                hdnFecNac.Value = row.Cells(3).Text
                txtcontacto1.Text = CType(row.FindControl("hdncontacto1"), HiddenField).Value
                txtcontacto2.Text = CType(row.FindControl("hdncontacto2"), HiddenField).Value
                txtcelularcto1.Text = CType(row.FindControl("hdncelularc1"), HiddenField).Value
                txtcelularcto2.Text = CType(row.FindControl("hdncelularc2"), HiddenField).Value
            Else
                btnConfirmar.OnClientClick = ""
                txtrut.Text = row.Cells(1).Text
                txtnombre.Text = HttpUtility.HtmlDecode(row.Cells(2).Text)
                hdnEmpresa.Value = CType(row.FindControl("hdnempresa"), HiddenField).Value
                hdnFecNac.Value = row.Cells(3).Text
                txtcontacto1.Text = CType(row.FindControl("hdncontacto1"), HiddenField).Value
                txtcontacto2.Text = CType(row.FindControl("hdncontacto2"), HiddenField).Value
                txtcelularcto1.Text = CType(row.FindControl("hdncelularc1"), HiddenField).Value
                txtcelularcto2.Text = CType(row.FindControl("hdncelularc2"), HiddenField).Value
            End If

            rdoReader.Close()
            rdoReader = Nothing
            cmdTemporal.Dispose()
            cmdTemporal = Nothing

        Catch ex As Exception
            MessageBoxError("grdBuscar_RowCommand", Err, Page, Master)
        End Try

    End Sub

    Private Sub ConfirmarSeguro()
        Dim strSQL As String = "Select num_asegurado From PERMov_nomina_seguro_vida Where rut_personal='" & txtrut.Text & "'and num_periodo='" & lblperiodoactivo.Text & "' and est_activo=1 and cod_empresa='" & hdnEmpresa.Value & "' and (cod_tipo_seguro=1  or cod_tipo_seguro=3 or cod_tipo_seguro=4)"
        Dim cmdTemporal As SqlCommand, rdoReader As SqlDataReader
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim strSQLCodigo As String
        cnxBaseDatos.Open()
        Try
            strSQLCodigo = strSQL
            cmdTemporal = New SqlCommand(strSQLCodigo, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            If CInt(txtvaloruf.Text) > 0 Then

                If rdoReader.Read() Then
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "dbo.sel_datos_seguros_vida"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "INUP"
                    comando.Parameters.Add("@buscar", SqlDbType.NVarChar)
                    comando.Parameters("@buscar").Value = txtrut.Text
                    comando.Parameters.Add("@empresa", SqlDbType.Int)
                    comando.Parameters("@empresa").Value = hdnEmpresa.Value
                    comando.Parameters.Add("@tseguro", SqlDbType.Int)
                    comando.Parameters("@tseguro").Value = cbotiposeguro.SelectedValue
                    comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
                    comando.Parameters("@periodo").Value = lblperiodoactivo.Text
                    comando.Parameters.Add("@Valorasegurado", SqlDbType.Int)
                    comando.Parameters("@Valorasegurado").Value = txtvaloruf.Text
                    comando.Parameters.Add("@rut", SqlDbType.NVarChar)
                    comando.Parameters("@rut").Value = txtrut.Text
                    comando.Connection = conx

                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()

                    Call CargarGrillaP()
                    Call Limpiar()
                Else
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "dbo.sel_datos_seguros_vida"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "IN"
                    comando.Parameters.Add("@buscar", SqlDbType.NVarChar)
                    comando.Parameters("@buscar").Value = txtrut.Text
                    comando.Parameters.Add("@empresa", SqlDbType.Int)
                    comando.Parameters("@empresa").Value = hdnEmpresa.Value
                    comando.Parameters.Add("@tseguro", SqlDbType.Int)
                    comando.Parameters("@tseguro").Value = cbotiposeguro.SelectedValue
                    comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
                    comando.Parameters("@periodo").Value = lblperiodoactivo.Text
                    comando.Parameters.Add("@Valorasegurado", SqlDbType.Int)
                    comando.Parameters("@Valorasegurado").Value = txtvaloruf.Text
                    comando.Parameters.Add("@rut", SqlDbType.NVarChar)
                    comando.Parameters("@rut").Value = txtrut.Text
                    comando.Connection = conx

                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()

                    Call CargarGrillaP()
                    Call Limpiar()
                End If
                MessageBox("Guardar", "Seguro Guardado", Page, Master, "S")
            Else
                MessageBox("Guardar", "El Valor de la UF no puede menor o igual a Cero", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("ConfirmarSeguro", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdBuscar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdBuscar.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
                e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"
            End If
        Catch ex As Exception
            MessageBoxError("grdBuscar_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        Call CargarGrillaP()
        Call IniControles()
    End Sub

    Protected Sub btnConfirmar_Click(sender As Object, e As EventArgs) Handles btnConfirmar.Click
        Try
            Call ConfirmarSeguro()
        Catch ex As Exception
            MessageBoxError("btnConfirmar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnlimpiar_Click(sender As Object, e As EventArgs) Handles btnlimpiar.Click
        Call Limpiar()
    End Sub

    Protected Sub btnperiodo_Click(sender As Object, e As EventArgs) Handles btnperiodo.Click
        Call HabilitarPeriodo(lblperiodonuevo.Text)
        Call CargarPeriodo()
    End Sub

    Private Sub HabilitarPeriodo(ByVal Periodo As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando = New SqlCommand
            comando.CommandType = CommandType.Text
            comando.CommandText = "Exec pro_periodo_seguro_vida '" & Periodo & "'"

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("HabilitarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtdiastot_actualizar(sender As Object, e As EventArgs)
        Dim txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(txt.NamingContainer, GridViewRow)
        Dim objasegurado As TextBox = CType(row.FindControl("txtasegurado"), TextBox)
        Dim hdnasegurado As HiddenField = CType(row.FindControl("hdnasegurado"), HiddenField)
        Dim hdnactivo As HiddenField = CType(row.FindControl("hdnactivo"), HiddenField)
        Dim txtexentosegmuerte As TextBox = CType(row.FindControl("txtexentosegmuerte"), TextBox)
        Dim txtafectoseginvalidez As TextBox = CType(row.FindControl("txtafectoseginvalidez"), TextBox)
        Dim txtivaseginvalidez As TextBox = CType(row.FindControl("txtivaseginvalidez"), TextBox)
        hdnasegurado.Value = objasegurado.Text

        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            If hdnactivo.Value = "True" Then
                hdnactivo.Value = 1
            ElseIf hdnactivo.Value = "False" Then
                hdnactivo.Value = 0
            End If

            If CType(row.FindControl("txtdiastot"), TextBox).Text > 0 Then

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.pro_actualizar_seguro_vida"
                comando.Parameters.Add("@empresa", SqlDbType.Int)
                comando.Parameters("@empresa").Value = CInt(cboempresa.SelectedValue)
                comando.Parameters.Add("@tseguro", SqlDbType.Int)
                comando.Parameters("@tseguro").Value = CInt(cboseguro.SelectedValue)
                comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
                comando.Parameters("@periodo").Value = lblperiodoactivo.Text
                comando.Parameters.Add("@uf", SqlDbType.Float)
                comando.Parameters("@uf").Value = 0
                comando.Parameters.Add("@segvida_exento", SqlDbType.Float)
                comando.Parameters("@segvida_exento").Value = 0
                comando.Parameters.Add("@segmuerte_exento", SqlDbType.Float)
                comando.Parameters("@segmuerte_exento").Value = 0
                comando.Parameters.Add("@seginvalidez_afecto", SqlDbType.Float)
                comando.Parameters("@seginvalidez_afecto").Value = 0
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "IP"
                comando.Parameters.Add("@rut", SqlDbType.NVarChar)
                comando.Parameters("@rut").Value = CType(row.FindControl("txtrutn"), TextBox).Text
                comando.Parameters.Add("@dias", SqlDbType.NVarChar)
                comando.Parameters("@dias").Value = CType(row.FindControl("txtdiastot"), TextBox).Text
                comando.Parameters.Add("@num_Asegurado", SqlDbType.Int)
                comando.Parameters("@num_Asegurado").Value = CInt(hdnasegurado.Value)
                comando.Parameters.Add("@est_activo", SqlDbType.Bit)
                comando.Parameters("@est_activo").Value = CInt(hdnactivo.Value)
                comando.Connection = conx

                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                Call CargarGrillaN()
            Else
                MessageBox("Actualizar", "Si el Valor de días es 0 o nulo, entonces elimine el Registro", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("txtdiastot_actualizar", Err, Page, Master)
        End Try

    End Sub

    Private Sub CargarGrillaN()
        Dim strSQL As String = "Exec sel_datos_seguros_vida 'NS', '" & txtnombrenomina.Text & "', '" & cboempresa.SelectedValue & "', '" & cboseguro.SelectedValue & "', '" & lblperiodoactivo.Text & "', '', '" & Trim(txtBuscarRut.Text) & "' "
        Dim strNomTabla As String = "PERMov_nomina_seguro_vida"
        Try
            grdNominaSegV.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdNominaSegV.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaN", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnactualizar_Click(sender As Object, e As EventArgs) Handles btnactualizar.Click
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            If txtuf.Text <> "" Then

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.pro_actualizar_seguro_vida"
                comando.Parameters.Add("@empresa", SqlDbType.Int)
                comando.Parameters("@empresa").Value = CInt(cboempresa.SelectedValue)
                comando.Parameters.Add("@tseguro", SqlDbType.Int)
                comando.Parameters("@tseguro").Value = CInt(cboseguro.SelectedValue)
                comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
                comando.Parameters("@periodo").Value = lblperiodoactivo.Text
                comando.Parameters.Add("@uf", SqlDbType.Float)
                comando.Parameters("@uf").Value = txtuf.Text

                comando.Parameters.Add("@segvida_exento", SqlDbType.Float)
                comando.Parameters("@segvida_exento").Value = txtexentosvida.Text
                comando.Parameters.Add("@segmuerte_exento", SqlDbType.Float)
                comando.Parameters("@segmuerte_exento").Value = txtexentosmuerte.Text
                comando.Parameters.Add("@seginvalidez_afecto", SqlDbType.Float)
                comando.Parameters("@seginvalidez_afecto").Value = txtafectoinvalidez.Text
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "IT"
                comando.Connection = conx

                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                Call CargarGrillaP()
                Call Limpiar()
                Call RescatarUF()
                txtuf.Text = hdnUFActual.Value
            Else
                MessageBox("Actualizar", "El Valor de la UF no puede menor o igual a Cero", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("btnactualizar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboempresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboempresa.SelectedIndexChanged
        Call CargarPoliza()
        Call CargarGrillaN()
        Call CargarValores()
        'Call ValidarNomina()
    End Sub

    Protected Sub cboseguro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboseguro.SelectedIndexChanged
        Call CargarPoliza()
        Call CargarGrillaN()
        Call CargarValores()
        ' Call ValidarNomina()
    End Sub

    Private Sub actualizarPoliza()
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            If txtPoliza.Text <> "" Then

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.sel_datos_seguros_vida"
                comando.Parameters.Add("@empresa", SqlDbType.Int)
                comando.Parameters("@empresa").Value = CInt(cboempresa.SelectedValue)
                comando.Parameters.Add("@tseguro", SqlDbType.Int)
                comando.Parameters("@tseguro").Value = CInt(cboseguro.SelectedValue)
                comando.Parameters.Add("@buscar", SqlDbType.NVarChar)
                comando.Parameters("@buscar").Value = txtPoliza.Text
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "APS"
                comando.Connection = conx

                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Else
                MessageBox("Poliza", "El Valor de la UF no puede menor o igual a Cero", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("txtPoliza_TextChanged", Err, Page, Master)
        End Try
    End Sub
    Protected Sub txtPoliza_TextChanged(sender As Object, e As EventArgs) Handles txtPoliza.TextChanged
        Call actualizarPoliza()
    End Sub

    Protected Sub txtnombrenomina_TextChanged(sender As Object, e As EventArgs) Handles txtnombrenomina.TextChanged
        Try
            Call CargarGrillaN()
            Call CargarValores()
        Catch ex As Exception
            MessageBoxError("btnbuscarr_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtBuscarRut_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarRut.TextChanged
        Try
            Call CargarGrillaN()
            Call CargarValores()
        Catch ex As Exception
            MessageBoxError("btnbuscarr_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnexportar_Click1(sender As Object, e As EventArgs) Handles btnexportar.Click
        Try
            Dim grdResumen As New DataGrid

            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None

            grdResumen.Font.Size = 10


            Dim strSQL As String = "Exec sel_datos_seguros_vida 'NSE', '', '" & cboempresa.SelectedValue & "', " & cboseguro.SelectedValue & ", '" & lblperiodoactivo.Text & "' "
            Dim strNomTabla As String = "PERMov_nomina_seguro_vida"

            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdResumen.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Nomina" & cboempresa.SelectedItem.Text & "-" & cboseguro.SelectedItem.Text & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnexportar_Click", Err, Page, Master)
        End Try
    End Sub
    Private Sub IValidarNomina()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.sel_datos_seguros_vida"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "INF"
            comando.Parameters.Add("@buscar", SqlDbType.NVarChar)
            comando.Parameters("@buscar").Value = ""
            comando.Parameters.Add("@empresa", SqlDbType.Int)
            comando.Parameters("@empresa").Value = cboempresa.SelectedValue
            comando.Parameters.Add("@tseguro", SqlDbType.Int)
            comando.Parameters("@tseguro").Value = cboseguro.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = lblperiodoactivo.Text
            comando.Parameters.Add("@Valorasegurado", SqlDbType.Int)
            comando.Parameters("@Valorasegurado").Value = 0
            comando.Connection = conx

            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("IValidarNomina", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnvalidar_Click(sender As Object, e As EventArgs) Handles btnvalidar.Click
        Call IValidarNomina()
        Call ValidarNomina()
    End Sub

    Protected Sub txtuf_TextChanged(sender As Object, e As EventArgs) Handles txtuf.TextChanged

    End Sub

    Protected Sub chkInv_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)
        Dim chkInv As CheckBox = CType(row.FindControl("chkInv"), CheckBox)
        Dim objasegurado As TextBox = CType(row.FindControl("txtasegurado"), TextBox)
        Dim hdnasegurado As HiddenField = CType(row.FindControl("hdnasegurado"), HiddenField)
        Dim hdnactivo As HiddenField = CType(row.FindControl("hdnactivo"), HiddenField)
        Dim txtexentosegmuerte As TextBox = CType(row.FindControl("txtexentosegmuerte"), TextBox)
        Dim txtafectoseginvalidez As TextBox = CType(row.FindControl("txtafectoseginvalidez"), TextBox)
        Dim txtivaseginvalidez As TextBox = CType(row.FindControl("txtivaseginvalidez"), TextBox)
        hdnasegurado.Value = objasegurado.Text

        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_actualizar_seguro_vida"
            comando.Parameters.Add("@empresa", SqlDbType.Int)
            comando.Parameters("@empresa").Value = CInt(cboempresa.SelectedValue)
            comando.Parameters.Add("@tseguro", SqlDbType.Int)
            comando.Parameters("@tseguro").Value = CInt(cboseguro.SelectedValue)
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = lblperiodoactivo.Text
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "AI"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = CType(row.FindControl("txtrutn"), TextBox).Text
            comando.Parameters.Add("@dias", SqlDbType.NVarChar)
            comando.Parameters("@dias").Value = CType(row.FindControl("txtdiastot"), TextBox).Text
            comando.Parameters.Add("@num_Asegurado", SqlDbType.Int)
            comando.Parameters("@num_Asegurado").Value = CInt(hdnasegurado.Value)
            comando.Parameters.Add("@est_activo", SqlDbType.Bit)
            If hdnactivo.Value = True Then
                comando.Parameters("@est_activo").Value = 1
            Else
                comando.Parameters("@est_activo").Value = 0
            End If
            comando.Parameters.Add("@est_inv", SqlDbType.Bit)
            If chkInv.Checked = True Then
                comando.Parameters("@est_inv").Value = 1
            Else
                comando.Parameters("@est_inv").Value = 0
            End If

            comando.Connection = conx

            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call CargarGrillaN()

        Catch ex As Exception
            MessageBoxError("chkInv_CheckedChanged", Err, Page, Master)
        End Try

    End Sub

    Protected Sub chkVida_CheckedChanged(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)
        Dim chkVida As CheckBox = CType(row.FindControl("chkVida"), CheckBox)
        Dim objasegurado As TextBox = CType(row.FindControl("txtasegurado"), TextBox)
        Dim hdnasegurado As HiddenField = CType(row.FindControl("hdnasegurado"), HiddenField)
        Dim hdnactivo As HiddenField = CType(row.FindControl("hdnactivo"), HiddenField)
        Dim txtexentosegmuerte As TextBox = CType(row.FindControl("txtexentosegmuerte"), TextBox)
        Dim txtafectoseginvalidez As TextBox = CType(row.FindControl("txtafectoseginvalidez"), TextBox)
        Dim txtivaseginvalidez As TextBox = CType(row.FindControl("txtivaseginvalidez"), TextBox)
        hdnasegurado.Value = objasegurado.Text

        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_actualizar_seguro_vida"
            comando.Parameters.Add("@empresa", SqlDbType.Int)
            comando.Parameters("@empresa").Value = CInt(cboempresa.SelectedValue)
            comando.Parameters.Add("@tseguro", SqlDbType.Int)
            comando.Parameters("@tseguro").Value = CInt(cboseguro.SelectedValue)
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = lblperiodoactivo.Text
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "AM"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = CType(row.FindControl("txtrutn"), TextBox).Text
            comando.Parameters.Add("@dias", SqlDbType.NVarChar)
            comando.Parameters("@dias").Value = CType(row.FindControl("txtdiastot"), TextBox).Text
            comando.Parameters.Add("@num_Asegurado", SqlDbType.Int)
            comando.Parameters("@num_Asegurado").Value = CInt(hdnasegurado.Value)
            comando.Parameters.Add("@est_activo", SqlDbType.Bit)
            If hdnactivo.Value = True Then
                comando.Parameters("@est_activo").Value = 1
            Else
                comando.Parameters("@est_activo").Value = 0
            End If
            comando.Parameters.Add("@est_vida", SqlDbType.Bit)
            If chkVida.Checked = True Then
                comando.Parameters("@est_vida").Value = 1
            Else
                comando.Parameters("@est_vida").Value = 0
            End If
            comando.Connection = conx

            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call CargarGrillaN()

        Catch ex As Exception
            MessageBoxError("chkVida_CheckedChanged", Err, Page, Master)
        End Try
    End Sub

    'Protected Sub btnExportarIngreso_Click(sender As Object, e As EventArgs) Handles btnExportarIngreso.Click
    '    Try
    '        Dim grdResumen As New DataGrid

    '        grdResumen.CellPadding = 0
    '        grdResumen.CellSpacing = 0
    '        grdResumen.BorderStyle = BorderStyle.None

    '        grdResumen.Font.Size = 10


    '        Dim strSQL As String = "Exec sel_datos_seguros_vida 'NSE', '', '" & cboempresa.SelectedValue & "', " & cboseguro.SelectedValue & ", '" & lblperiodoactivo.Text & "' "
    '        Dim strNomTabla As String = "PERMov_nomina_seguro_vida"

    '        grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
    '        grdResumen.DataBind()

    '        Me.EnableViewState = False
    '        Dim tw As New System.IO.StringWriter
    '        Dim hw As New System.Web.UI.HtmlTextWriter(tw)

    '        grdResumen.RenderControl(hw)

    '        Response.Clear()
    '        Response.Buffer = True
    '        Response.ContentType = "application/vnd.ms-excel"
    '        Response.AddHeader("Content-Disposition", "attachment;filename=Nomina" & cboempresa.SelectedItem.Text & "-" & cboseguro.SelectedItem.Text & ".xls")
    '        Response.Charset = "UTF-8"
    '        Response.ContentEncoding = Encoding.Default

    '        Response.Write(tw.ToString())
    '        Response.End()
    '    Catch ex As Exception
    '        MessageBoxError("btnexportar_Click", Err, Page)
    '    End Try
    'End Sub

    'Protected Sub btnExportarPersonal_Click(sender As Object, e As EventArgs) Handles btnExportarPersonal.Click
    '    If rblPersonal.SelectedValue = "" Then
    '        MessageBox("Exportar", "Seleccione opcion a exportar", Page, Master, "W")
    '    Else
    '        If rblPersonal.SelectedValue = 1 Then
    '            Try
    '                Dim grdResumen As New DataGrid

    '                grdResumen.CellPadding = 0
    '                grdResumen.CellSpacing = 0
    '                grdResumen.BorderStyle = BorderStyle.None

    '                grdResumen.Font.Size = 10

    '                Dim strSQL As String = "select rut_personal, nom_personal, fec_ingreso from REMMae_ficha_personal where est_vigencia = 1 and cod_empresa = '" & cboEmpresaComparar.SelectedValue & "' "
    '                Dim strNomTabla As String = "REMMae_ficha_personal"

    '                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
    '                grdResumen.DataBind()

    '                Me.EnableViewState = False
    '                Dim tw As New System.IO.StringWriter
    '                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

    '                grdResumen.RenderControl(hw)

    '                Response.Clear()
    '                Response.Buffer = True
    '                Response.ContentType = "application/vnd.ms-excel"
    '                Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Activo_" & cboEmpresaComparar.SelectedItem.Text & ".xls")
    '                Response.Charset = "UTF-8"
    '                Response.ContentEncoding = Encoding.Default

    '                Response.Write(tw.ToString())
    '                Response.End()
    '            Catch ex As Exception
    '                MessageBoxError("btnexportar_Click", Err, Page, Master)
    '            End Try

    '        ElseIf rblPersonal.SelectedValue = 2 Then
    '            Try
    '                Dim grdResumen As New DataGrid

    '                grdResumen.CellPadding = 0
    '                grdResumen.CellSpacing = 0
    '                grdResumen.BorderStyle = BorderStyle.None

    '                grdResumen.Font.Size = 10

    '                Dim strSQL As String = "select rut_personal, (select top 1 nom_personal from REMMae_ficha_personal where rut_personal = PERMov_nomina_seguro_vida.rut_personal) as nombre_personal, (select  fec_ingreso from REMMae_ficha_personal where rut_personal = PERMov_nomina_seguro_vida.rut_personal) as fec_ingreso from PERMov_nomina_seguro_vida where cod_empresa = '" & cboEmpresaComparar.SelectedValue & "' and num_periodo = '" & lblperiodoactivo.Text & "' and cod_tipo_seguro = 1"
    '                Dim strNomTabla As String = "PERMov_nomina_seguro_vida"

    '                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
    '                grdResumen.DataBind()

    '                Me.EnableViewState = False
    '                Dim tw As New System.IO.StringWriter
    '                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

    '                grdResumen.RenderControl(hw)

    '                Response.Clear()
    '                Response.Buffer = True
    '                Response.ContentType = "application/vnd.ms-excel"
    '                Response.AddHeader("Content-Disposition", "attachment;filename=Personal_SEGURO_VIDA_" & cboEmpresaComparar.SelectedItem.Text & ".xls")
    '                Response.Charset = "UTF-8"
    '                Response.ContentEncoding = Encoding.Default

    '                Response.Write(tw.ToString())
    '                Response.End()
    '            Catch ex As Exception
    '                MessageBoxError("btnexportar_Click", Err, Page, Master)
    '            End Try

    '        ElseIf rblPersonal.SelectedValue = 3 Then
    '            Try
    '                Dim grdResumen As New DataGrid

    '                grdResumen.CellPadding = 0
    '                grdResumen.CellSpacing = 0
    '                grdResumen.BorderStyle = BorderStyle.None

    '                grdResumen.Font.Size = 10

    '                Dim strSQL As String = "select rut_personal, nom_personal, fec_ingreso from REMMae_ficha_personal where est_vigencia = 1 and cod_empresa =  '" & cboEmpresaComparar.SelectedValue & "'  and rut_personal not in (select rut_personal from PERMov_nomina_seguro_vida where rut_personal = REMMae_ficha_personal.rut_personal)"
    '                Dim strNomTabla As String = "PERMov_nomina_seguro_vida"

    '                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
    '                grdResumen.DataBind()

    '                Me.EnableViewState = False
    '                Dim tw As New System.IO.StringWriter
    '                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

    '                grdResumen.RenderControl(hw)

    '                Response.Clear()
    '                Response.Buffer = True
    '                Response.ContentType = "application/vnd.ms-excel"
    '                Response.AddHeader("Content-Disposition", "attachment;filename=Personal_sin_seguro_" & cboEmpresaComparar.SelectedItem.Text & ".xls")
    '                Response.Charset = "UTF-8"
    '                Response.ContentEncoding = Encoding.Default

    '                Response.Write(tw.ToString())
    '                Response.End()
    '            Catch ex As Exception
    '                MessageBoxError("btnexportar_Click", Err, Page, Master)
    '            End Try
    '        End If
    '    End If
    'End Sub

    'Protected Sub btnExportarIPersonal_Click(sender As Object, e As EventArgs) Handles btnExportarIPersonal.Click
    '    lbltitulo.Text = "Exportar Personal"
    '    MtvPrincipal.ActiveViewIndex = 2
    'End Sub
End Class
