﻿
Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspSeguroDeSalud
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Ingresar Personas"
                MtvPrincipal.ActiveViewIndex = 0
                Call BuscarPeriodo()
                Call carga_combo_empresa()
                Call CargarGrillaAgregar("")


                Call cargar_periodo_agregar()
                Call carga_combo_empresa_agregar()
                Call RescatarUF()
                txtValUF.Text = hdnUFActual.Value
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub BuscarPeriodo()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select num_periodo, cod_periodo from PERMae_periodo where bit_activo = 1"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblperiodoactivo.Text = rdoReader(0).ToString
                    hdnCodPeriodoAgregar.Value = rdoReader(1).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("BuscarPeriodo", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa in (select cod_empresa from REMMae_encargados where cod_usuario = '" & Session("cod_usu") & "')"
            cboempresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboempresa.DataTextField = "nom_empresa"
            cboempresa.DataValueField = "cod_empresa"
            cboempresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaAgregar(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.PERMov_personal_vida_camara"
            Dim strSQL As String = "Exec proc_personal_vida_camara 'G1' , '" & nombre & "', '" & lblperiodoactivo.Text & "'"
            grdAgregar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdAgregar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAgregar", Err, Page, Master)
        End Try
    End Sub



    Private Sub LimpiarAgregar()
        txtBNombreAgregar.Text = ""
        Call CargarGrillaAgregar("")
    End Sub

    Protected Sub btnlimpiar_Click(sender As Object, e As EventArgs) Handles btnlimpiar.Click
        Call LimpiarAgregar()
    End Sub

    Private Sub RescatarUF()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        'Dim periodoAnt As String = ""
        'If Right(lblperiodoactivo.Text, 2) = "01" Then
        '    periodoAnt = Left(lblperiodoactivo.Text, 4) - 1 + "12"
        'Else
        '    periodoAnt = lblperiodoactivo.Text - 1
        'End If

        Dim strSQL As String = "select num_valor from PERMae_uf_seguros where cod_periodo = '" & cboPeriodo.SelectedItem.Text & "' and tip_seguro = 2"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnUFActual.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarUF", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarValoresAgregarArea()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select porcentaje_empresa, porcentaje_trabajador, val_titular, val_carga,val_carga_especial from PERMov_valores_seguros where cod_empresa = '" & hdnCodEmpresa.Value & "' and id_area = '" & hdnIdArea.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPorEmpresa.Value = rdoReader(0).ToString
                    hdnPorTrabajador.Value = rdoReader(1).ToString
                    hdnValTitular.Value = rdoReader(2).ToString
                    hdnValCarga.Value = rdoReader(3).ToString
                    hdnValCargaEspecial.Value = rdoReader(4).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarValoresAgregar", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarValoresAgregarTodos()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select porcentaje_empresa, porcentaje_trabajador, val_titular, val_carga,val_carga_especial from PERMov_valores_seguros where cod_empresa = '" & hdnCodEmpresa.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPorEmpresa.Value = rdoReader(0).ToString
                    hdnPorTrabajador.Value = rdoReader(1).ToString
                    hdnValTitular.Value = rdoReader(2).ToString
                    hdnValCarga.Value = rdoReader(3).ToString
                    hdnValCargaEspecial.Value = rdoReader(4).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarValoresAgregarTodos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarPersonalIngresado()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec proc_personal_vida_camara 'RPI', '" & hdnRut.Value & "', '" & lblperiodoactivo.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnExiste.Value = 1
                Else
                    hdnExiste.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarPersonalIngresado", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub AgregarPersonas()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 9000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_personal_vida_camara"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = hdnRut.Value
            comando.Parameters.Add("@est_seguro", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro").Value = 1
            comando.Parameters.Add("@periodo_seguro", SqlDbType.NVarChar)
            comando.Parameters("@periodo_seguro").Value = lblperiodoactivo.Text
            comando.Parameters.Add("@val_UF_actual", SqlDbType.NVarChar)
            comando.Parameters("@val_UF_actual").Value = Replace(hdnUFActual.Value, ",", ".")
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnCodEmpresa.Value
            comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
            comando.Parameters("@id_area").Value = hdnIdArea.Value
            comando.Parameters.Add("@costo_carga_UF", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_UF").Value = Replace(hdnValCarga.Value, ",", ".")
            comando.Parameters.Add("@costo_carga_especial_UF", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_especial_UF").Value = Replace(hdnValCargaEspecial.Value, ",", ".")
            comando.Parameters.Add("@costo_titular_uf", SqlDbType.NVarChar)
            comando.Parameters("@costo_titular_uf").Value = Replace(hdnValTitular.Value, ",", ".")
            comando.Parameters.Add("@fec_add", SqlDbType.NVarChar)
            comando.Parameters("@fec_add").Value = Date.Now.ToString
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            MessageBox("Guardar", "Personal Agregado", Page, Master, "S")
            Call LimpiarAgregar()

        Catch ex As Exception
            MessageBoxError("AgregarPersonas", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar_Valores()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.proc_personal_vida_camara"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U_VAL"
            comando.Parameters.Add("@val_UF_actual", SqlDbType.NVarChar)
            comando.Parameters("@val_UF_actual").Value = Replace(hdnUFActual.Value, ",", ".")
            comando.Parameters.Add("@cant_cargas", SqlDbType.NVarChar)
            comando.Parameters("@cant_cargas").Value = Replace(hdnCantidadCargas.Value, ",", ".")
            comando.Parameters.Add("@cant_cargas_especiales", SqlDbType.NVarChar)
            comando.Parameters("@cant_cargas_especiales").Value = Replace(hdnCantidadCargasEsp.Value, ",", ".")
            comando.Parameters.Add("@costo_carga_UF", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_UF").Value = Replace(hdnCArgaUFModificar.Value, ",", ".")
            comando.Parameters.Add("@costo_carga_especial_UF", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_especial_UF").Value = Replace(hdnCArgaEspecialUFModificar.Value, ",", ".")
            comando.Parameters.Add("@costo_titular_uf", SqlDbType.NVarChar)
            comando.Parameters("@costo_titular_uf").Value = Replace(hdnCostoTutilarUFModificar.Value, ",", ".")
            comando.Parameters.Add("@costo_carga_pesos", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_pesos").Value = Replace(hdnCostoCargaPesos.Value, ",", ".")
            comando.Parameters.Add("@costo_carga_especial_pesos", SqlDbType.NVarChar)
            comando.Parameters("@costo_carga_especial_pesos").Value = Replace(hdnCostoCargaEspecialPesos.Value, ",", ".")
            comando.Parameters.Add("@total_pago_cargas_UF", SqlDbType.NVarChar)
            comando.Parameters("@total_pago_cargas_UF").Value = Replace(hdnTotalPagaCargasUF.Value, ",", ".")
            comando.Parameters.Add("@total_pago_cargas_pesos", SqlDbType.NVarChar)
            comando.Parameters("@total_pago_cargas_pesos").Value = Replace(hdnTotalPagoCargaPesos.Value, ",", ".")
            comando.Parameters.Add("@costo_titular_pesos", SqlDbType.NVarChar)
            comando.Parameters("@costo_titular_pesos").Value = Replace(hdnCostoTitularPesos.Value, ",", ".")
            comando.Parameters.Add("@monto_pagado_titular", SqlDbType.NVarChar)
            comando.Parameters("@monto_pagado_titular").Value = Replace(hdnMontoPagadoTitular.Value, ",", ".")
            comando.Parameters.Add("@monto_pagado_empresa", SqlDbType.NVarChar)
            comando.Parameters("@monto_pagado_empresa").Value = Replace(hdnMontoPagadoEmpresa.Value, ",", ".")
            comando.Parameters.Add("@monto_total_a_pagar", SqlDbType.NVarChar)
            comando.Parameters("@monto_total_a_pagar").Value = Replace(hdnTotalAPagar.Value, ",", ".")
            comando.Parameters.Add("@retroactivo", SqlDbType.NVarChar)
            comando.Parameters("@retroactivo").Value = hdnChkActivo.Value
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = Val(hdnIdValores.Value)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar_Valores", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Try
            For Each row As GridViewRow In grdAgregar.Rows
                Dim chkSeleccionar As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)
                Dim obj_rut As String = CType(row.FindControl("lblRut"), Label).Text
                Dim obj_cod_emp As String = CType(row.FindControl("hdnCodEmp"), HiddenField).Value
                Dim obj_id_area As String = CType(row.FindControl("hdnIdArea"), HiddenField).Value

                hdnCodEmpresa.Value = obj_cod_emp
                hdnIdArea.Value = obj_id_area

                If chkSeleccionar.Checked = True Then

                    If obj_cod_emp = 3 Then
                        Call RescatarValoresAgregarArea()
                    Else
                        Call RescatarValoresAgregarTodos()
                    End If

                    hdnRut.Value = obj_rut

                    Call RescatarPersonalIngresado()

                    If hdnExiste.Value = 0 Then
                        Call AgregarPersonas()
                    End If
                End If
            Next

        Catch ex As Exception
            MessageBoxError("btnAgregar_Click", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdAgregar_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAgregar.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
            e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"
            Dim objRut As Label = CType(row.FindControl("lblRut"), Label)
            Dim objCod_Emp As HiddenField = CType(row.FindControl("hdnCodEmp"), HiddenField)

            hdnRutIncluido.Value = objRut.Text
            hdnCodEmpresaAgregar.Value = objCod_Emp.Value

            Call Rescatar_Personal_No_Ingresado()

            If hdnIncluido.Value = 1 Then
                Dim objChk As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)
                objChk.Checked = True
            Else
                Dim objChk As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)
                objChk.Checked = False
            End If
        End If
    End Sub

    Private Sub grdAgregar_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdAgregar.PageIndexChanging
        Try
            grdAgregar.PageIndex = e.NewPageIndex
            If txtBNombreAgregar.Text = "" Then
                Call CargarGrillaAgregar("")
            Else
                Call CargarGrillaAgregar(txtBNombreAgregar.Text)
            End If
        Catch ex As Exception
            MessageBoxError("grdAgregar_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Protected Sub btnIPersonas_Click(sender As Object, e As EventArgs) Handles btnIPersonas.Click
        MtvPrincipal.ActiveViewIndex = 0
        lbltitulo.Text = "Ingresar Personas"
        Call CargarGrillaAgregar("")

    End Sub

    Protected Sub btnProcesarSeguro_Click(sender As Object, e As EventArgs) Handles btnProcesarSeguro.Click
        MtvPrincipal.ActiveViewIndex = 1
        lbltitulo.Text = "Nomina Seguros"
        If cboempresa.SelectedValue = 0 Then
            pnlMP.Visible = False
        Else
            pnlMP.Visible = True
            Call CargarGrillaValores("")
        End If
    End Sub

    Private Sub cargar_periodo_agregar()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "select cod_periodo, num_periodo from PERMae_periodo where bit_activo = 1"
        cboPeriodo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboPeriodo.DataTextField = "num_periodo"
        cboPeriodo.DataValueField = "cod_periodo"
        cboPeriodo.DataBind()
    End Sub

    Private Sub carga_combo_empresa_agregar()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> (5)  order by cod_empresa"
            cboempresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboempresa.DataTextField = "nom_empresa"
            cboempresa.DataValueField = "cod_empresa"
            cboempresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa_agregar", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaValores(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.PERMov_personal_vida_camara"
            Dim strSQL As String = "Exec proc_personal_vida_camara 'G2T' , '" & nombre & "', '" & cboempresa.SelectedValue & "', '" & cboPeriodo.SelectedItem.ToString & "'"
            grdValoresPersonal.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdValoresPersonal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaValores", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click
        If txtValUF.Text = "" Then
            MessageBox("Calcular", "Ingrese valor de UF correspondiente", Page, Master, "W")
        Else
            For Each row As GridViewRow In grdValoresPersonal.Rows
                Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                Dim obj_cod_emp_valores As HiddenField = CType(row.FindControl("hdnCodEmp"), HiddenField)
                Dim obj_id_area_valores As HiddenField = CType(row.FindControl("hdnIdArea"), HiddenField)
                Dim objRut As Label = CType(row.FindControl("lblRutVal"), Label)

                Dim obj_val_uf As Label = CType(row.FindControl("lblValUFActualVal"), Label)
                Dim obj_costo_carga_pesos As Label = CType(row.FindControl("lblCostoCargaPesos"), Label)
                Dim obj_costo_carga_especial_pesos As Label = CType(row.FindControl("lblCostoCargaEspecialPesos"), Label)
                Dim obj_total_pago_carga_uf As Label = CType(row.FindControl("lblTotalPagoCargaUFVal"), Label)
                Dim obj_total_pago_carga_pesos As Label = CType(row.FindControl("lblTotPagoCargaPesosVal"), Label)
                Dim obj_costo_titular_pesos As Label = CType(row.FindControl("lblCostoTitularPesos"), Label)
                Dim obj_monto_pagado_titular_pesos As Label = CType(row.FindControl("lblMontoPagadoTitularPesosVal"), Label)
                Dim obj_costo_emp_pesos As Label = CType(row.FindControl("lblCostoempPesosVal"), Label)
                Dim obj_monto_pagado_empresa_pesos As Label = CType(row.FindControl("lblMontoPagadoEmpresaVal"), Label)

                Dim obj_costo_carga_uf As TextBox = CType(row.FindControl("txtCostoCargaUF"), TextBox)
                Dim obj_num_cargas As TextBox = CType(row.FindControl("txtNumCargas"), TextBox)
                Dim obj_num_cargas_especiales As TextBox = CType(row.FindControl("txtNumCargaEspeciales"), TextBox)
                Dim obj_costo_carga_especial_uf As TextBox = CType(row.FindControl("txtCostoCargaEspecialUF"), TextBox)
                Dim obj_costo_titular_uf As TextBox = CType(row.FindControl("txtCostoTitularUF"), TextBox)

                Dim obj_chk1 As CheckBox = CType(row.FindControl("chkRetroactivo"), CheckBox)

                hdnCantidadCargas.Value = obj_num_cargas.Text
                hdnCantidadCargasEsp.Value = obj_num_cargas_especiales.Text

                hdnIdValores.Value = obj_id.Value
                hdnCodEmpresaValores.Value = obj_cod_emp_valores.Value
                hdnIdAreaValores.Value = obj_id_area_valores.Value

                If obj_cod_emp_valores.Value = 3 Then
                    Call RescatarValoresAreaVal()
                Else
                    Call RescatarValoresTodosVal()
                End If

                hdnCostoCargaPesos.Value = Math.Ceiling((CDbl(obj_costo_carga_uf.Text) * CDbl(txtValUF.Text)) * (1.19))
                hdnCostoCargaEspecialPesos.Value = Math.Ceiling((CDbl(obj_costo_carga_especial_uf.Text) * CDbl(txtValUF.Text)) * (1.19))

                If obj_num_cargas.Text = 0 And obj_num_cargas_especiales.Text <> 0 Then
                    hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_especial_uf.Text)) * (CDbl(obj_num_cargas_especiales.Text)))
                    hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaEspecialPesos.Value)) * (CDbl(obj_num_cargas_especiales.Text))))
                ElseIf obj_num_cargas.Text = 0 And obj_num_cargas_especiales.Text = 0 Then
                    hdnTotalPagaCargasUF.Value = 0
                    hdnTotalPagoCargaPesos.Value = 0

                ElseIf obj_num_cargas.Text <> 0 And obj_num_cargas_especiales.Text <> 0 Then
                    If obj_chk1.Checked = True Then
                        hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text) + CDbl(obj_costo_carga_especial_uf.Text)) * (CDbl(obj_num_cargas_especiales.Text) + CDbl(obj_num_cargas.Text)))
                        hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value) * (CDbl(obj_num_cargas.Text)))) + CDbl(((hdnCostoCargaEspecialPesos.Value) * (CDbl(obj_num_cargas_especiales.Text)))))
                    Else
                        If obj_num_cargas.Text > 3 Then
                            hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text) + CDbl(obj_costo_carga_especial_uf.Text)) * (CDbl(obj_num_cargas_especiales.Text) + CDbl(3)))
                            hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value) * (CDbl(3)))) + CDbl(((hdnCostoCargaEspecialPesos.Value) * (CDbl(obj_num_cargas_especiales.Text)))))
                        Else
                            hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text) + CDbl(obj_costo_carga_especial_uf.Text)) * (CDbl(obj_num_cargas_especiales.Text) + CDbl(obj_num_cargas.Text)))
                            hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value) * (CDbl(obj_num_cargas.Text)))) + CDbl(((hdnCostoCargaEspecialPesos.Value) * (CDbl(obj_num_cargas_especiales.Text)))))
                        End If
                    End If
                ElseIf obj_num_cargas.Text <> 0 And obj_num_cargas_especiales.Text = 0 Then
                    If obj_chk1.Checked = True Then
                        hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text)) * (CDbl(obj_num_cargas.Text)))
                        hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value)) * ((CDbl(obj_num_cargas.Text)))))
                    Else
                        If obj_num_cargas.Text > 3 Then
                            hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text)) * (CDbl(3)))
                            hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value)) * ((CDbl(3)))))
                        Else
                            hdnTotalPagaCargasUF.Value = ((CDbl(obj_costo_carga_uf.Text)) * (CDbl(obj_num_cargas.Text)))
                            hdnTotalPagoCargaPesos.Value = (Math.Ceiling((CDbl(hdnCostoCargaPesos.Value)) * ((CDbl(obj_num_cargas.Text)))))
                        End If
                    End If
                End If

                hdnCostoTitularPesos.Value = Math.Ceiling((CDbl(obj_costo_titular_uf.Text) * CDbl(txtValUF.Text)) * (1.19))

                If Trim(objRut.Text) = "16983014-2" Or Trim(objRut.Text) = "10271073-8" Or Trim(objRut.Text) = "08015123-3" Or Trim(objRut.Text) = "14068371-K" Or Trim(objRut.Text) = "17933656-1" Then
                    hdnMontoPagadoTitular.Value = Math.Ceiling(Val(hdnTotalPagoCargaPesos.Value) + Val((hdnCostoTitularPesos.Value) * (0.4)))
                    hdnMontoPagadoEmpresa.Value = Math.Ceiling(Val(hdnCostoTitularPesos.Value) * (0.6))
                    hdnTotalAPagar.Value = Val(hdnMontoPagadoTitular.Value) + Val(hdnMontoPagadoEmpresa.Value)
                Else
                    hdnMontoPagadoTitular.Value = Math.Ceiling(Val(hdnTotalPagoCargaPesos.Value) + (Val(hdnCostoTitularPesos.Value) * CDbl(Val(hdnPorTrabajador.Value) / (100))))
                    hdnMontoPagadoEmpresa.Value = Math.Ceiling(Val(hdnCostoTitularPesos.Value) * CDbl(Val(hdnPorEmpresa.Value) / (100)))
                    hdnTotalAPagar.Value = Val(hdnMontoPagadoTitular.Value) + Val(hdnMontoPagadoEmpresa.Value)
                End If


                hdnCArgaUFModificar.Value = obj_costo_carga_uf.Text
                hdnCArgaEspecialUFModificar.Value = obj_costo_carga_especial_uf.Text
                hdnCostoTutilarUFModificar.Value = obj_costo_titular_uf.Text

                If obj_chk1.Checked = True Then
                    hdnChkActivo.Value = 1
                Else
                    hdnChkActivo.Value = 0
                End If

                Call Actualizar_Valores()
            Next

            If txtBNombreValores.Text = "" Then
                Call CargarGrillaValores("")
            Else
                Call CargarGrillaValores(txtBNombreValores.Text)
            End If
        End If
    End Sub

    Private Sub CargarPoliza()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim sqlString As String = "proc_personal_vida_camara 'CPS'," & cboempresa.SelectedValue & ""
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            txtPoliza.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboempresa.SelectedIndexChanged
        If cboempresa.SelectedValue = 0 Then
            pnlMP.Visible = False
        Else
            pnlMP.Visible = True
            Call CargarPoliza()
            Call CargarGrillaValores("")
        End If
    End Sub

    Protected Sub btnExportarP_Click(sender As Object, e As EventArgs) Handles btnExportarP.Click
        Try
            Dim grdcargar As New DataGrid
            grdcargar.ShowHeader = True
            grdcargar.CellPadding = 0
            grdcargar.CellSpacing = 0
            grdcargar.BorderStyle = BorderStyle.Double
            grdcargar.Font.Size = 11

            If txtBNombreValores.Text = "" Then
                Dim strNomTabla As String = "REMMov_item_personal"
                Dim strSQL As String = "Exec proc_personal_vida_camara 'G2EX' , '', '" & cboempresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "'"
                grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdcargar.DataBind()
            Else
                Dim strNomTabla As String = "REMMov_item_personal"
                Dim strSQL As String = "Exec proc_personal_vida_camara 'G2EX' , '" & txtBNombreValores.Text & "', '" & cboempresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "'"
                grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdcargar.DataBind()
            End If

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdcargar.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Vida_Camara_" & cboPeriodo.SelectedItem.ToString & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarP_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub Rescatar_Personal_No_Ingresado()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select rut_personal from PERMov_personal_vida_camara where rut_personal = '" & hdnRutIncluido.Value & "' and cod_empresa = '" & hdnCodEmpresaAgregar.Value & "' and periodo_seguro = '" & lblperiodoactivo.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIncluido.Value = 1
                Else
                    hdnIncluido.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("Rescatar_Personal_No_Ingresado", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub grdValoresPersonal_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdValoresPersonal.PageIndexChanging
        Try
            grdValoresPersonal.PageIndex = e.NewPageIndex

            If txtBNombreValores.Text = "" Then
                Call CargarGrillaValores("")
            Else
                Call CargarGrillaValores(txtBNombreValores.Text)
            End If
        Catch ex As Exception
            MessageBoxError("grdValoresPersonal_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdValoresPersonal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdValoresPersonal.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "2" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdValoresPersonal.Rows(intRow)
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand

                Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim objRut As Label = CType(row.FindControl("lblRutVal"), Label)

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.proc_personal_vida_camara"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "EP"
                comando.Parameters.Add("@est_seguro", SqlDbType.NVarChar)
                comando.Parameters("@est_seguro").Value = 0
                comando.Parameters.Add("@cod_empresa", SqlDbType.Int)
                comando.Parameters("@cod_empresa").Value = cboempresa.SelectedValue
                comando.Parameters.Add("@periodo_seguro", SqlDbType.Int)
                comando.Parameters("@periodo_seguro").Value = cboPeriodo.SelectedItem.ToString
                comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                comando.Parameters("@rut_personal").Value = objRut.Text
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                If txtBNombreValores.Text = "" Then
                    Call CargarGrillaValores("")
                Else
                    Call CargarGrillaValores(txtBNombreValores.Text)
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdValoresPersonal_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarValores()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select porcentaje_empresa, porcentaje_trabajador from PERMov_valores_seguros where cod_empresa = '" & hdnCodEmpresa.Value & "' and id_area = '" & hdnIdArea.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPorEmpresaValores.Value = rdoReader(0).ToString
                    hdnPorTrabajadorValores.Value = rdoReader(1).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarValores", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarValoresAreaVal()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select porcentaje_empresa, porcentaje_trabajador, val_titular, val_carga,val_carga_especial from PERMov_valores_seguros where cod_empresa = '" & hdnCodEmpresaValores.Value & "' and id_area = '" & hdnIdAreaValores.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPorEmpresa.Value = rdoReader(0).ToString
                    hdnPorTrabajador.Value = rdoReader(1).ToString
                    hdnValTitular.Value = rdoReader(2).ToString
                    hdnValCarga.Value = rdoReader(3).ToString
                    hdnValCargaEspecial.Value = rdoReader(4).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarValoresAreaVal", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarValoresTodosVal()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select porcentaje_empresa, porcentaje_trabajador, val_titular, val_carga,val_carga_especial from PERMov_valores_seguros where cod_empresa = '" & hdnCodEmpresaValores.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPorEmpresa.Value = rdoReader(0).ToString
                    hdnPorTrabajador.Value = rdoReader(1).ToString
                    hdnValTitular.Value = rdoReader(2).ToString
                    hdnValCarga.Value = rdoReader(3).ToString
                    hdnValCargaEspecial.Value = rdoReader(4).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarValoresTodosVal", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarRetroactivos()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "select coalesce(retroactivo, 0) as retroactivo from PERMov_personal_vida_camara where id_personal_vc = '" & hdnIdVC.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnRetroactivo.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarRetroactivos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub grdValoresPersonal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdValoresPersonal.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
            e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"

            Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

            Dim obj_chk As CheckBox = CType(row.FindControl("chkRetroactivo"), CheckBox)

            hdnIdVC.Value = obj_id.Value

            Call RescatarRetroactivos()

            If hdnRetroactivo.Value = 0 Then
                obj_chk.Checked = False
            Else
                obj_chk.Checked = True
            End If
        End If
    End Sub

    Protected Sub txtBNombreAgregar_TextChanged(sender As Object, e As EventArgs) Handles txtBNombreAgregar.TextChanged
        Call CargarGrillaAgregar(txtBNombreAgregar.Text)
    End Sub

    Protected Sub txtBNombreValores_TextChanged(sender As Object, e As EventArgs) Handles txtBNombreValores.TextChanged
        If txtBNombreValores.Text = "" Then
            Call CargarGrillaValores("")
        Else
            Call CargarGrillaValores(txtBNombreValores.Text)
        End If
    End Sub

    Protected Sub txtPoliza_TextChanged(sender As Object, e As EventArgs) Handles txtPoliza.TextChanged
        Call actualizarPoliza()
    End Sub
    Private Sub actualizarPoliza()
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            If txtPoliza.Text <> "" Then

                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "dbo.proc_personal_vida_camara"
                comando.Parameters.Add("@busqueda", SqlDbType.Int)
                comando.Parameters("@busqueda").Value = CInt(cboempresa.SelectedValue)
                comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
                comando.Parameters("@busqueda2").Value = txtPoliza.Text
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "APS"
                comando.Connection = conx

                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()
            Else
                MessageBox("Poliza", "El Valor de la UF no puede menor o igual a Cero", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("txtPoliza_TextChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub ValidarSeguros()
        Try

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.proc_personal_vida_camara"
            comando.Parameters.Add("@busqueda", SqlDbType.Int)
            comando.Parameters("@busqueda").Value = CInt(cboempresa.SelectedValue)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = lblperiodoactivo.Text
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "VSM"
            comando.Connection = conx

            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ValidarSeguros", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnvalidar_Click(sender As Object, e As EventArgs) Handles btnvalidar.Click
        Call ValidarSeguros()
    End Sub

    Protected Sub txtValUF_TextChanged(sender As Object, e As EventArgs) Handles txtValUF.TextChanged

    End Sub

    'Protected Sub btnExportarIngreso_Click(sender As Object, e As EventArgs) Handles btnExportarIngreso.Click
    '    Try
    '        Dim grdResumen As New DataGrid
    '        grdResumen.CellPadding = 0
    '        grdResumen.CellSpacing = 0
    '        grdResumen.BorderStyle = BorderStyle.None
    '        grdResumen.Font.Size = 10
    '        Dim strNomTabla As String = "Licencias"
    '        Dim strSQL As String = ""
    '        Dim nombre As String = ""

    '        strSQL = "Exec proc_personal_vida_camara 'ETL' , '" & nombre & "', '" & lblperiodoactivo.Text & "'"

    '        grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
    '        grdResumen.DataBind()

    '        Me.EnableViewState = False
    '        Dim tw As New System.IO.StringWriter
    '        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
    '        grdResumen.RenderControl(hw)
    '        Response.Clear()
    '        Response.Buffer = True
    '        Response.ContentType = "application/vnd.ms-excel"
    '        Response.AddHeader("Content-Disposition", "attachment;filename= HistoricoSeguros.xls")
    '        Response.Charset = "UTF-8"
    '        Response.ContentEncoding = Encoding.Default
    '        Response.Write(tw.ToString())
    '        Response.End()

    '    Catch ex As Exception
    '        MessageBoxError("btnExportar_Click", Err, Page, Master)
    '    End Try
    'End Sub

    Protected Sub grdAgregar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdAgregar.SelectedIndexChanged

    End Sub
End Class