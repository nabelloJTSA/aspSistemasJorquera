﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspSeguroVida.aspx.vb" Inherits="aspSistemasJorquera.aspSeguroVida" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnPersonas" runat="server">Ingresar Personas</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnNomina" runat="server">Nomina Seguros</asp:LinkButton>
        </li>
        <%-- <li>
            <asp:LinkButton ID="btnExportarIPersonal" runat="server">Exportar Personal</asp:LinkButton>
        </li>--%>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <asp:MultiView ID="MtvPrincipal" runat="server">
        <asp:View ID="VwIngreso" runat="server">
            <!-- Main content -->
            <section class="content">

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                        <div>
                            <asp:TextBox ID="txtbusqueda" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">
                        <asp:Panel ID="PnlRecurso" runat="server" Style="overflow: auto;" Height="270px" Width="100%">
                            <asp:GridView ID="grdBuscar" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnDetalle" runat="server" CommandName="btnDetalle"
                                                ImageUrl='~/imagen/select.png' CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                            <asp:HiddenField ID="hdnempresa" runat="server" Value='<%# Bind("cod_empresa")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnrut" runat="server" Value='<%# Bind("rut_personal")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdncontacto1" runat="server" Value='<%# Bind("nom_contacto1")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdncontacto2" runat="server" Value='<%# Bind("nom_contacto2")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdncelularc1" runat="server" Value='<%# Bind("num_celularc1")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdncelularc2" runat="server" Value='<%# Bind("num_celularc2")%>'></asp:HiddenField>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="30px" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="rut_personal" HeaderText="RUT" ReadOnly="True" SortExpression="rut_personal">
                                        <ItemStyle Width="80px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="nom_personal" HeaderText="Nombre" ReadOnly="True" SortExpression="nom_personal">
                                        <ItemStyle Width="400px" HorizontalAlign="left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="fec_nacimiento" HeaderText="Fec. Nac." ReadOnly="True" SortExpression="fec_nacimiento">
                                        <ItemStyle Width="100px" HorizontalAlign="left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="seguro_normal" HeaderText="Seguro Normal" SortExpression="seguro_normal">
                                        <ItemStyle Width="80px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="seguro_voluntario" HeaderText="Seguro Voluntario" SortExpression="seguro_voluntario">
                                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="seguro_especial" HeaderText="Seguro Especial" SortExpression="seguro_especial">
                                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="biodiversa" HeaderText="Biodiversa" SortExpression="biodiversa">
                                        <ItemStyle Width="80px" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Vida Cámara">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkvidacamara" runat="server" Checked='<%# Bind("vida_camara")%>' AutoPostBack="true" OnCheckedChanged="chkvidacamara_actualizar" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="100px" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FALP">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkfalp" runat="server" Checked='<%# Bind("falp")%>' AutoPostBack="true" OnCheckedChanged="chkfalp_actualizar" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="100px" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo Activo</label>
                        <div>
                            <asp:TextBox ID="lblperiodoactivo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Cambiar a Período</label>
                        <div>
                            <asp:TextBox ID="lblperiodonuevo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <label for="fname" class="control-label col-form-label"></label>
                        <div>
                            <asp:LinkButton ID="btnperiodo" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Rut</label>
                        <div>
                            <asp:TextBox ID="txtrut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="False"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre</label>
                        <div>
                            <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="False"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Contacto 1:</label>
                        <div>
                            <asp:TextBox ID="txtcontacto1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Celular:</label>
                        <div>
                            <asp:TextBox ID="txtcelularcto1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="False"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Cobertura</label>
                        <div>
                            <asp:TextBox ID="txtvaloruf" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Seguro</label>
                        <div>
                            <asp:DropDownList ID="cbotiposeguro" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Contacto 2:</label>
                        <div>
                            <asp:TextBox ID="txtcontacto2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Celular:</label>
                        <div>
                            <asp:TextBox ID="txtcelularcto2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="False"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnConfirmar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnlimpiar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                        </div>
                    </div>

                </div>
            </section>
        </asp:View>

        <asp:View ID="VwNomina" runat="server">
            <section class="content">

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboempresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Seguro</label>
                        <div>
                            <asp:DropDownList ID="cboseguro" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Poliza</label>
                        <div>
                            <asp:TextBox ID="txtPoliza" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Seguro Vida</label>
                        <div>
                            <asp:TextBox ID="txtexentosvida" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <label for="fname" class="control-label col-form-label">Seguro Muerte</label>
                        <div>
                            <asp:TextBox ID="txtexentosmuerte" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <label for="fname" class="control-label col-form-label">Seguro Invalidez</label>
                        <div>
                            <asp:TextBox ID="txtafectoinvalidez" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <label for="fname" class="control-label col-form-label">Valor UF</label>
                        <div>
                            <asp:TextBox ID="txtuf" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Buscar por Nombre</label>
                        <div>
                            <asp:TextBox ID="txtnombrenomina" runat="server" placeholder="Buscar por Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Buscar por RUT</label>
                        <div>
                            <asp:TextBox ID="txtBuscarRut" runat="server" placeholder="Buscar por RUT" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnvalidar" CssClass="btn bg-red btn-block" runat="server">Validar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnactualizar" CssClass="btn bg-orange btn-block" runat="server">Actualizar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnexportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-12">

                        <asp:Panel ID="Panel1" runat="server" Style="overflow: auto;" Height="300px" Width="100%">
                            <asp:GridView ID="grdNominaSegV" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField SortExpression="rut_personal" HeaderText="Rut" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtrutn" Text='<%# Bind("rut_personal")%>'
                                                runat="server" Width="100px" BorderStyle="None" ReadOnly="true" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="nom_nombre" HeaderText="Nombre" ReadOnly="True"
                                        SortExpression="nom_nombre">
                                        <HeaderStyle Width="200px" HorizontalAlign="Center" />
                                        <ItemStyle />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="fec_nacimiento" HeaderText="Fecha Nac." ReadOnly="True"
                                        SortExpression="fec_nacimiento">
                                        <HeaderStyle Width="100px" HorizontalAlign="Center" />
                                        <ItemStyle />
                                    </asp:BoundField>
                                    <asp:TemplateField SortExpression="num_asegurado" HeaderText="Asegurado" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtasegurado" Text='<%# Bind("num_asegurado")%>' runat="server" Width="50px" BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="40px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="num_dias" HeaderText="Días Tot." HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtdiastot" Text='<%# Bind("num_dias")%>' runat="server" Width="50px" BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="40px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>


                                    <asp:TemplateField SortExpression="val_exento_segvida_uf" HeaderText="Exento Seg. Vida" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtexentosegvida" Text='<%# Bind("val_exento_segvida_uf")%>' runat="server" Width="70px" BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="est_vida" HeaderText="Activo" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkVida" runat="server" Checked='<%# Bind("est_vida")%>' AutoPostBack="true" OnCheckedChanged="chkVida_CheckedChanged" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_exento_segmuerte_uf" HeaderText="Exento Seg. Muerte" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtexentosegmuerte" Text='<%# Bind("val_exento_segmuerte_uf")%>' runat="server" Width="70px" BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="est_inv" HeaderText="Activo" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkInv" runat="server" Checked='<%# Bind("est_inv")%>' AutoPostBack="true" OnCheckedChanged="chkInv_CheckedChanged" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_afecto_seginvalidez_uf" HeaderText="Afecto Seg. Invalidez" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtafectoseginvalidez" runat="server" Width="60px" Text='<%# Bind("val_afecto_seginvalidez_uf")%>' BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdnrut" runat="server" Value='<%# Bind("rut_personal")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnempresa" runat="server" Value='<%# Bind("cod_empresa")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdntseguro" runat="server" Value='<%# Bind("cod_tipo_seguro")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnasegurado" runat="server"></asp:HiddenField>
                                            <asp:HiddenField ID="hdnactivo" runat="server" Value='<%# Bind("est_activo")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnFecNacto" runat="server" Value='<%# Bind("fec_nacimiento")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnvigente" runat="server" Value='<%# Bind("vigente")%>'></asp:HiddenField>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_iva_seginvalidez_uf" HeaderText="IVA Seg. Invalidez" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtivaseginvalidez" runat="server" Width="60px" Text='<%# Bind("val_iva_seginvalidez_uf")%>' BorderStyle="None" OnTextChanged="txtdiastot_actualizar" AutoPostBack="true"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_neto_anual_uf" HeaderText="Neto Anual" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtnetoanual" runat="server" Width="60px" Text='<%# Bind("val_neto_anual_uf")%>' ReadOnly="true" BorderStyle="None"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_total_anual_uf" HeaderText="Total Anual" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txttotalanual" Text='<%# Bind("val_total_anual_uf")%>'
                                                runat="server" Width="70px" ReadOnly="true" BorderStyle="None" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_total_mensual_uf" HeaderText="Total Mensual" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txttotalmensual" Text='<%# Bind("val_total_mensual_uf")%>'
                                                runat="server" Width="70px" ReadOnly="true" BorderStyle="None" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="val_total_pesos" HeaderText="Total $" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txttotalpesos" Text='<%# Bind("val_total_pesos")%>'
                                                runat="server" Width="70px" ReadOnly="true" BorderStyle="None" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="80px" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Width="50px" Wrap="False"></ItemStyle>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </asp:Panel>

                    </div>

                </div>

            </section>
        </asp:View>
        <%--<asp:View ID="View1" runat="server">
            <section class="content">

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaComparar" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">
                   
                        <div class="col-md-6 form-group">
                            <label for="fname" class="control-label col-form-label"></label>
                            <div>
                                <span class="EstCampo">
                                    <asp:RadioButtonList ID="rblPersonal" runat="server" RepeatDirection="Horizontal" CssClass="control-label col-form-label">
                                        <asp:ListItem Value="1">Personal Activo</asp:ListItem>
                                        <asp:ListItem Value="2">Personal Con Seguro de Vida</asp:ListItem>
                                        <asp:ListItem Value="3">Personal Sin Seguro</asp:ListItem>
                                    </asp:RadioButtonList></span>
                            </div>
                        </div>
                  
                </div>



                <div class="row justify-content-center table-responsive">
                    <div class="col-md-3 form-group ">
                        <div>
                        </div>
                    </div>


                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportarPersonal" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>
            </section>
        </asp:View>--%>
    </asp:MultiView>

    <asp:HiddenField ID="txtAlto" runat="server" />
    <asp:HiddenField ID="hdnEmpresa" runat="server" />
    <asp:HiddenField ID="hdnUFActual" runat="server" />
    <asp:HiddenField ID="hdnFecNac" runat="server" />
</asp:Content>
