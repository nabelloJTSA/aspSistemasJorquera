﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspBancos.aspx.vb" Inherits="aspSistemasJorquera.aspBancos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <ul class="nav nav-tabs bg-info ">
                <li>
                    <asp:LinkButton ID="btnBanco" runat="server">Bancos</asp:LinkButton>

                </li>
                <li>
                    <asp:LinkButton ID="btnMP" runat="server">Medios de Pago</asp:LinkButton>

                </li>
                <li>
                    <asp:LinkButton ID="btnDetps" runat="server">Departamentos</asp:LinkButton>

                </li>
            </ul>
            <section class="content-header">
                <div class="row btn-sm">
                    <div class="col-md-4 form-group">
                        <h2>
                            <asp:Label ID="lbltituloP" runat="server" Text="Bancos"></asp:Label>
                        </h2>
                    </div>
                </div>
            </section>

            <section class="content">
                <asp:MultiView ID="MtvBancos" runat="server">
                    <asp:View ID="VwBancos" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Codigo</label>
                                <div>
                                    <asp:TextBox ID="txtCodBanco" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Nombre Banco</label>
                                <div>
                                    <asp:TextBox ID="txtNomBanco" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdBanco" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Codigo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCod" runat="server" Text='<%# Bind("num_codigo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Banco">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBanco" runat="server" Text='<%# Bind("nom_banco")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>
                    <asp:View ID="VwMP" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Codigo</label>
                                <div>
                                    <asp:TextBox ID="txtCodigoMP" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Medio de Pago</label>
                                <div>
                                    <asp:TextBox ID="txtMedioPago" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardarMP" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelarMP" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdMedioPago" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>

                                        <asp:TemplateField HeaderText="Codigo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCod" runat="server" Text='<%# Bind("num_codigo")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="10px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Medio Pago">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMedPago" runat="server" Text='<%# Bind("nom_mediopago")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>

                    </asp:View>
                    <asp:View ID="VwClaPago" runat="server">
                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Empresa</label>
                                <div>
                                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="fname" class="control-label col-form-label">Clasificación</label>
                                <div>
                                    <asp:TextBox ID="txtNomCL" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>

                        </div>


                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-4 form-group ">
                                <div>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnGuardarCl" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                </div>
                            </div>

                            <div class="col-md-1 form-group ">
                                <div>
                                    <asp:LinkButton ID="btnCancelarCl" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                </div>
                            </div>

                        </div>

                        <div class="row justify-content-center table-responsive">

                            <div class="col-md-5">

                                <asp:GridView ID="grdClasificacion" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                    <Columns>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                                        </asp:CommandField>


                                        <asp:TemplateField HeaderText="Clasificacion">
                                            <ItemTemplate>
                                                <asp:Label ID="lblClasificacion" runat="server" Text='<%# Bind("nom_clasificacion")%>' />
                                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("cod_clasificacion")%>' />
                                                <asp:HiddenField ID="hdn_cod_empresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Empresa">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" Width="400px" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                </asp:GridView>

                            </div>

                        </div>
                    </asp:View>

                </asp:MultiView>


                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdMP" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivoMP" runat="server" Value="0" />

                <asp:HiddenField ID="hdnIdCl" runat="server" Value="0" />
                <asp:HiddenField ID="hdnActivoCL" runat="server" Value="0" />

            </section>

        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                    <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                    <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
