﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspReporteSeguros
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Reportes Seguros"
                Call carga_combo_empresa_consulta()
                Call carga_anio_consulta()
                Call cargar_periodo_consulta()
                Call CargarTipoSeguro()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipoSeguro()
        Try
            Dim strNomTablaR As String = "PERMae_tipo_seguro"
            Dim strSQLR As String = "Exec rpt_PER_seguros 'CTS' "
            cboTipoSeguro.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoSeguro.DataTextField = "nom_tipo_seguro"
            cboTipoSeguro.DataValueField = "cod_tipo_seguro"
            cboTipoSeguro.DataBind()
        Catch ex As Exception
            MessageBoxError("cboTipoSeguro", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_empresa_consulta()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> (5)  order by cod_empresa"
            cboEmpresaC.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaC.DataTextField = "nom_empresa"
            cboEmpresaC.DataValueField = "cod_empresa"
            cboEmpresaC.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub carga_anio_consulta()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "select distinct num_anio from PERMae_periodo order by num_anio DESC"
        cboAnioC.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboAnioC.DataTextField = "num_anio"
        cboAnioC.DataValueField = "num_anio"
        cboAnioC.DataBind()
    End Sub

    Private Sub cargar_periodo_consulta()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "Select num_periodo from PERMae_periodo where num_anio = '" & cboAnioC.SelectedValue & "' order by num_periodo"
        cboPeriodoC.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboPeriodoC.DataTextField = "num_periodo"
        cboPeriodoC.DataValueField = "num_periodo"
        cboPeriodoC.DataBind()
    End Sub

    Protected Sub btnExportarC_Click(sender As Object, e As EventArgs) Handles btnExportarC.Click
        Try
            If cboTipoSeguro.SelectedValue = 0 And chkMostrarTodoC.Checked = True Then
                MessageBox("Exportar", "Debe seleccionar un tipo de Seguro", Page, Master, "W")
            Else
                If cboTipoSeguro.SelectedValue = 999 Then
                    Dim grdcargar As New DataGrid
                    grdcargar.ShowHeader = True
                    grdcargar.CellPadding = 0
                    grdcargar.CellSpacing = 0
                    grdcargar.BorderStyle = BorderStyle.Double
                    grdcargar.Font.Size = 11

                    If chkMostrarTodoC.Checked = True Then
                        If txtBNombreC.Text = "" Then
                            Dim strNomTabla As String = "PERMov_personal_vida_camara"
                            Dim strSQL As String = "Exec proc_personal_vida_camara 'G3TEX' , ''"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec proc_personal_vida_camara 'G3T' , '" & txtBNombreC.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBNombreC.Text = "" Then
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec proc_personal_vida_camara 'G3FEX' , '', '" & cboEmpresaC.SelectedValue & "', '" & cboPeriodoC.SelectedValue & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec proc_personal_vida_camara 'G3FEX' , '" & txtBNombreC.Text & "', '" & cboEmpresaC.SelectedValue & "', '" & cboPeriodoC.SelectedValue & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If

                    Me.EnableViewState = False
                    Dim tw As New System.IO.StringWriter
                    Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                    grdcargar.RenderControl(hw)
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "application/vnd.ms-excel"

                    If chkMostrarTodoC.Checked = True Then
                        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Seguro_Medico_Todos.xls")
                    Else
                        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Seguro_Medico_" & cboPeriodoC.SelectedItem.ToString & ".xls")
                    End If

                    Response.Charset = "UTF-8"
                    Response.ContentEncoding = Encoding.Default
                    Response.Write(tw.ToString())
                    Response.End()
                Else
                    Dim grdcargar As New DataGrid
                    grdcargar.ShowHeader = True
                    grdcargar.CellPadding = 0
                    grdcargar.CellSpacing = 0
                    grdcargar.BorderStyle = BorderStyle.Double
                    grdcargar.Font.Size = 11

                    If chkMostrarTodoC.Checked = True Then
                        If txtBNombreC.Text = "" Then
                            Dim strNomTabla As String = "PERMov_personal_vida_camara"
                            Dim strSQL As String = "Exec rpt_PER_seguros 'GT' ,'','','', ''"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec rpt_PER_seguros 'GT' ,'','','', '" & txtBNombreC.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBNombreC.Text = "" Then
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec rpt_PER_seguros 'GTC' , '" & cboEmpresaC.SelectedValue & "', '" & cboTipoSeguro.SelectedValue & "', '" & cboPeriodoC.SelectedValue & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_item_personal"
                            Dim strSQL As String = "Exec rpt_PER_seguros 'GTC' , '" & cboEmpresaC.SelectedValue & "', '" & cboTipoSeguro.SelectedValue & "', '" & cboPeriodoC.SelectedValue & "', '" & txtBNombreC.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If

                    Me.EnableViewState = False
                    Dim tw As New System.IO.StringWriter
                    Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                    grdcargar.RenderControl(hw)
                    Response.Clear()
                    Response.Buffer = True
                    Response.ContentType = "application/vnd.ms-excel"

                    If chkMostrarTodoC.Checked = True Then
                        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Seguro_Salud_Todos.xls")
                    Else
                        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Seguro_Salud_" & cboPeriodoC.SelectedItem.ToString & ".xls")
                    End If

                    Response.Charset = "UTF-8"
                    Response.ContentEncoding = Encoding.Default
                    Response.Write(tw.ToString())
                    Response.End()
                End If
            End If

        Catch ex As Exception
            MessageBoxError("btnExportarC_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub chkMostrarTodoC_CheckedChanged(sender As Object, e As EventArgs) Handles chkMostrarTodoC.CheckedChanged
        If chkMostrarTodoC.Checked = True Then
            cboEmpresaC.Enabled = False
            cboAnioC.Enabled = False
            cboPeriodoC.Enabled = False
            cboTipoSeguro.Enabled = False
        Else
            cboEmpresaC.Enabled = True
            cboAnioC.Enabled = True
            cboPeriodoC.Enabled = True
            cboTipoSeguro.Enabled = True
        End If
    End Sub
    'Private Sub CargarKP1()
    '    Dim conx As New SqlConnection(strCnx)
    '    Dim comando As New SqlCommand
    '    Dim leer As SqlDataReader
    '    Dim tipoSeguro As String = "%"
    '    Dim empresa As String = "%"
    '    If cboTipoSeguro.SelectedValue <> 0 Then
    '        tipoSeguro = cboTipoSeguro.SelectedValue
    '    End If
    '    If cboEmpresaC.SelectedValue <> 0 Then
    '        empresa = cboEmpresaC.SelectedValue
    '    End If
    '    Dim sqlString As String = "exec rpt_PER_seguros 'KP1','" & empresa & "','" & tipoSeguro & "','" & cboPeriodoC.SelectedValue & "' "
    '    comando.CommandTimeout = 180000
    '    comando.CommandText = sqlString
    '    comando.Connection = conx
    '    conx.Open()

    '    leer = comando.ExecuteReader

    '    If leer.Read Then
    '        lblK1.Text = leer(0)
    '    End If
    '    leer.Close()
    '    conx.Close()
    'End Sub



    Private Sub CargarDetalleKP1()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim tipoSeguro As String = "%"
        Dim empresa As String = "%"
        If cboTipoSeguro.SelectedValue <> 0 Then
            tipoSeguro = cboTipoSeguro.SelectedValue
        End If
        If cboEmpresaC.SelectedValue <> 0 Then
            empresa = cboEmpresaC.SelectedValue
        End If

        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_seguros 'DKP1','" & empresa & "','" & tipoSeguro & "','" & cboPeriodoC.SelectedValue & "' "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_asegurado.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub



    'Private Sub CargarKP2()
    '    Dim conx As New SqlConnection(strCnx)
    '    Dim comando As New SqlCommand
    '    Dim leer As SqlDataReader
    '    Dim tipoSeguro As String = "%"
    '    Dim empresa As String = "%"
    '    If cboTipoSeguro.SelectedValue <> 0 Then
    '        tipoSeguro = cboTipoSeguro.SelectedValue
    '    End If
    '    If cboEmpresaC.SelectedValue <> 0 Then
    '        empresa = cboEmpresaC.SelectedValue
    '    End If
    '    Dim sqlString As String = "exec rpt_PER_seguros 'KP2','" & empresa & "','" & tipoSeguro & "','" & cboPeriodoC.SelectedValue & "' "
    '    comando.CommandTimeout = 180000
    '    comando.CommandText = sqlString
    '    comando.Connection = conx
    '    conx.Open()

    '    leer = comando.ExecuteReader

    '    If leer.Read Then
    '        lblK2.Text = leer(0)
    '    End If
    '    leer.Close()
    '    conx.Close()
    'End Sub

    Private Sub CargarDetalleKP2()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim tipoSeguro As String = "%"
        Dim empresa As String = "%"
        If cboTipoSeguro.SelectedValue <> 0 Then
            tipoSeguro = cboTipoSeguro.SelectedValue
        End If
        If cboEmpresaC.SelectedValue <> 0 Then
            empresa = cboEmpresaC.SelectedValue
        End If

        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_seguros 'KP2','" & empresa & "','" & tipoSeguro & "','" & cboPeriodoC.SelectedValue & "' "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_no_asegurado.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub

    'Private Sub CargarKP3()
    '    Dim conx As New SqlConnection(strCnx)
    '    Dim comando As New SqlCommand
    '    Dim leer As SqlDataReader
    '    Dim sqlString As String = "exec rpt_PER_seguros 'KP3'," & cboEmpresaC.SelectedValue & " "
    '    comando.CommandTimeout = 180000
    '    comando.CommandText = sqlString
    '    comando.Connection = conx
    '    conx.Open()

    '    leer = comando.ExecuteReader

    '    If leer.Read Then
    '        lblK3.Text = leer(0)
    '    End If
    '    leer.Close()
    '    conx.Close()
    'End Sub

    Private Sub CargarDetalleKP3()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim tipoSeguro As String = "%"
        Dim empresa As String = "%"
        If cboTipoSeguro.SelectedValue <> 0 Then
            tipoSeguro = cboTipoSeguro.SelectedValue
        End If
        If cboEmpresaC.SelectedValue <> 0 Then
            empresa = cboEmpresaC.SelectedValue
        End If

        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_seguros 'KP3'," & cboEmpresaC.SelectedValue & " "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Total.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub

    'Private Sub CargarKP4()
    '    Dim conx As New SqlConnection(strCnx)
    '    Dim comando As New SqlCommand
    '    Dim leer As SqlDataReader
    '    Dim tipoSeguro As String = "%"
    '    Dim empresa As String = "%"
    '    If cboTipoSeguro.SelectedValue <> 0 Then
    '        tipoSeguro = cboTipoSeguro.SelectedValue
    '    End If
    '    If cboEmpresaC.SelectedValue <> 0 Then
    '        empresa = cboEmpresaC.SelectedValue
    '    End If
    '    Dim sqlString As String = "exec rpt_PER_seguros 'KP4','" & empresa & "','" & tipoSeguro & "','" & cboPeriodoC.SelectedValue & "' "
    '    comando.CommandTimeout = 180000
    '    comando.CommandText = sqlString
    '    comando.Connection = conx
    '    conx.Open()

    '    leer = comando.ExecuteReader

    '    If leer.Read Then
    '        lblK4.Text = leer(0)
    '    End If
    '    leer.Close()
    '    conx.Close()
    'End Sub
    Protected Sub cboEmpresaC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaC.SelectedIndexChanged
        'Call CargarKP1()
        'Call CargarKP2()
        'Call CargarKP3()
        'Call CargarKP4()
    End Sub

    Protected Sub cboTipoSeguro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoSeguro.SelectedIndexChanged
        'Call CargarKP1()
        'Call CargarKP2()
        'Call CargarKP3()
        'Call CargarKP4()
    End Sub

    Protected Sub cboPeriodoC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoC.SelectedIndexChanged
        'Call CargarKP1()
        'Call CargarKP2()
        'Call CargarKP3()
        'Call CargarKP4()
    End Sub

    'Private Sub btnKp1_Click(sender As Object, e As EventArgs) Handles btnKp1.Click
    '    Call CargarDetalleKP1()
    'End Sub

    Protected Sub txtBNombreC_TextChanged(sender As Object, e As EventArgs) Handles txtBNombreC.TextChanged

    End Sub
End Class