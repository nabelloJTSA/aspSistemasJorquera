﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspLicencias.aspx.vb" Inherits="aspSistemasJorquera.aspLicencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnLComun" runat="server">Licencia Médica</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnLAccidente" runat="server">Licencia Laboral</asp:LinkButton></li>

    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <asp:MultiView ID="MtvLicencias" runat="server">
        <asp:View ID="VwLComun" runat="server">
            <!-- Main content -->
            <section class="content">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">RUT</label>
                        <div>
                            <asp:TextBox ID="txtrut" runat="server" placeholder="Rut Trabajador..." MaxLength="12" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre</label>
                        <div>
                            <asp:TextBox ID="txtNombreLicComun" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:TextBox ID="lblEmpLicComun" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">N° Comprobante</label>
                        <div>
                            <asp:TextBox ID="txtComprobante" runat="server" placeholder="N° Comprobante..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">


                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Inicio</label>
                        <div>
                            <asp:TextBox ID="txtfechai" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Dias</label>
                        <div>
                            <asp:TextBox ID="txtDiasLicComun" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">1/2 Día</label>
                        <div>
                            <asp:CheckBox ID="chkMedio" runat="server" Checked="false" />
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Termino</label>
                        <div>
                            <asp:TextBox ID="txtfechat" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Emisión</label>
                        <div>
                            <asp:TextBox ID="dtFecEmisionLic" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Tramitación</label>
                        <div>
                            <asp:TextBox ID="dtFecTramiLic" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Presentación</label>
                        <div>
                            <asp:TextBox ID="dtFechaPreseLic" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>

                        </div>
                    </div>
                </div>


                <div class="row justify-content-center table-responsive">
                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Especialidad Médica</label>
                        <div>
                            <asp:TextBox ID="txtEspecialidad" runat="server" placeholder="Especialidad Médica..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre Médico</label>
                        <div>
                            <asp:TextBox ID="txtNomMedico" runat="server" placeholder="Nombre Médico..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Archivo</label>
                        <div>
                            <asp:FileUpload ID="uplArchivoComun" CssClass="form-control  text-uppercase" runat="server" Enabled="true" />
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center table-responsive">
                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardarComun" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCancelarEtiqueta" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportar" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>


                </div>
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-12">

                        <asp:GridView ID="grdLicenciaComun" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                            <Columns>
                                <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                </asp:CommandField>
                                <asp:TemplateField HeaderText="Rut">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRutComun" runat="server" Text='<%# Bind("rut_personal")%>' />
                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_licencia_comun")%>' />
                                        <asp:HiddenField ID="hdnurlComun" runat="server" Value='<%# Bind("url")%>' />
                                        <asp:HiddenField ID="hdnExtensionComun" runat="server" Value='<%# Bind("ext")%>' />
                                        <asp:HiddenField ID="hdnMedio" runat="server" Value='<%# Bind("act_mediodia")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombreComun" runat="server" Text='<%# Bind("nombrepersonal")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="350px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpresaComun" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="180px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Area/Faena">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("area")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Termino">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecTermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dias">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiasLicencias" runat="server" Text='<%# Bind("dias_total")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comprobante">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNumComprobante" runat="server" Text='<%# Bind("num_comprobante")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Emision">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecEmisionComun" runat="server" Text='<%# Bind("fec_emision_lic", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Presentación">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecPresentacionComun" runat="server" Text='<%# Bind("fec_presentacion_lic", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tramitación">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecTramiteComun" runat="server" Text='<%# Bind("fec_tramitacion_lic", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Especialidad">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEspecialidadComun" runat="server" Text='<%# Bind("nom_especialidad")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Medico">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMedicoComun" runat="server" Text='<%# Bind("nom_medico")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="250px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDescargarComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" ToolTip="Descargar" Width="18px"><i class="fa fa-fw fa-download"></i></asp:LinkButton>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDeleteComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>
                    </div>
                </div>
            </section>
        </asp:View>

        <asp:View ID="VwAccidente" runat="server">
            <section class="content">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">RUT</label>
                        <div>
                            <asp:TextBox ID="txtRutAcci" runat="server" placeholder="Rut Trabajador..." MaxLength="12" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Nombre</label>
                        <div>
                            <asp:TextBox ID="txtBuscarnombreAcci" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:TextBox ID="lblEmpAcci" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">N° Comprobante</label>
                        <div>
                            <asp:TextBox ID="txtNumComprobanteAcc" runat="server" placeholder="N° Comprobante..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Inicio</label>
                        <div>
                            <asp:TextBox ID="txtFecnaIniAcc" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Dias</label>
                        <div>
                            <asp:TextBox ID="txtDiasLicAcci" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">1/2 Día</label>
                        <div>
                            <asp:CheckBox ID="ChkMedioAcc" runat="server" Checked="false" />
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Termino</label>
                        <div>
                            <asp:TextBox ID="txtFechaTerAcc" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Accidente</label>
                        <div>
                            <asp:TextBox ID="dtFecAccidente" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Alta Laboral</label>
                        <div>
                            <asp:TextBox ID="dtAlta" runat="server" TextMode="Date" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-2 form-group">
                        <label for="fname" class="control-label col-form-label">Dias Perdidos</label>
                        <div>
                            <asp:DropDownList ID="cboDiasPerdidos" runat="server" CssClass="form-control text-uppercase">
                                <asp:ListItem Value="9">Seleccione</asp:ListItem>
                                <asp:ListItem Value="1">Si</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <%--<div class="col-md-4 form-group">
                        <label for="fname" class="control-label col-form-label">Movimiento</label>
                        <div>
                            <asp:DropDownList ID="ComboMovmiento" runat="server" AutoPostBack="true" CssClass="form-control text-uppercase"></asp:DropDownList>
                        </div>
                    </div>--%>

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Archivo</label>
                        <div>
                            <asp:FileUpload ID="uplArchivoAcci" CssClass="form-control  text-uppercase" runat="server" Enabled="true" />
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-8 form-group ">
                        <div>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnGuardarAcci" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnLimpiarAcci" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnExportarAcc" CssClass="btn bg-green btn-block" runat="server"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                        </div>
                    </div>

                </div>

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-12">

                        <asp:GridView ID="grdLicenciaAccidente" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                            <Columns>
                                <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                </asp:CommandField>
                                <asp:TemplateField HeaderText="Rut">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRutAcci" runat="server" Text='<%# Bind("rut_personal")%>' />
                                        <asp:HiddenField ID="hdn_idAcci" runat="server" Value='<%# Bind("id_licencia_accidente")%>' />
                                        <asp:HiddenField ID="hdn_dias" runat="server" Value='<%# Bind("dias_perdidos")%>' />
                                        <asp:HiddenField ID="hdnurlAcc" runat="server" Value='<%# Bind("url")%>' />
                                        <asp:HiddenField ID="hdnExtensionAcc" runat="server" Value='<%# Bind("ext")%>' />
                                        <asp:HiddenField ID="hdnMedio" runat="server" Value='<%# Bind("act_mediodia")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="90px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nombre">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNombreAcci" runat="server" Text='<%# Bind("nombrepersonal")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="350px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpresaAcci" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="180px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Area/Faena">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("area")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Inicio">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecInicioAcci" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Termino">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecTerminoAcci" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dias Licencia">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiasLicencias" runat="server" Text='<%# Bind("dias_total")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comprobante">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNumComprobanteAcci" runat="server" Text='<%# Bind("num_comprobante")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Accidente">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecAccidente" runat="server" Text='<%# Bind("fec_accidente", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Alta">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFecAlta" runat="server" Text='<%# Bind("fec_alta", "{0:d}")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dias Perdidos">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiasPerdidos" runat="server" Text='<%# Bind("dias")%>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDescargarAcci" Width="18px" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="1" runat="server" ToolTip="Descargar"> <i class="fa fa-fw fa-download"></i></asp:LinkButton>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEliminarAcci" Width="18px" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" runat="server" ToolTip="Eliminar" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"> <i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>
            </section>
        </asp:View>

    </asp:MultiView>

    <asp:HiddenField ID="hdnEmpresaComun" runat="server" />
    <asp:HiddenField ID="hdnEmpresaAccidente" runat="server" />
    <asp:HiddenField ID="hdnId" runat="server" />
    <asp:HiddenField ID="hdnIdAcci" runat="server" />
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnActivoAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEstVacaciones" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEstVacacionesAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnNumExist" runat="server" Value="0" />
    <asp:HiddenField ID="hdnNumExistAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdAreaComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdAreaAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCodFaena" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMailSupervisor" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPersonalComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPersonalAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdMovimiento" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRutGrdAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnComprobanteGrdAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUrlActAccie" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUrlActComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRutGrdComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnComprobanteGrdComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnFiltroSelAcci" runat="server" Value="0" />
    <asp:HiddenField ID="hdnFiltroSelComun" runat="server" Value="0" />
    <asp:HiddenField ID="hdnExpotar" runat="server" Value="0" />
</asp:Content>
