﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspLicencias
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvLicencias.ActiveViewIndex = 0
                lbltitulo.Text = "Licencias Médicas"
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try

    End Sub
    Private Sub CargarGrillaComunRut(ByVal rut As String)
        Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
        Dim strSQL As String = "Exec proc_licencias_medicas 'LCR','" & rut & "'"
        Try
            grdLicenciaComun.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicenciaComun.DataBind()
            If ContarRegistros(strSQL) = True Then
                hdnExpotar.Value = 1
            Else
                hdnExpotar.Value = 0
            End If

        Catch ex As Exception
            MessageBoxError("CargarGrillaComunRut", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaComunComp(ByVal comprobante As String)
        Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
        Dim strSQL As String = "Exec proc_licencias_medicas 'LCC','" & comprobante & "'"
        Try
            grdLicenciaComun.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicenciaComun.DataBind()
            If ContarRegistros(strSQL) = True Then
                hdnExpotar.Value = 2
            Else
                hdnExpotar.Value = 0
            End If
        Catch ex As Exception
            MessageBoxError("CargarGrillaComunComp", Err, Page, Master)
        End Try
    End Sub
    Private Sub DevolverMailSupervisorFaena()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_licencias_medicas 'SMS1', '" & hdnCodFaena.Value & "','" & hdnEmpresaComun.Value & "','" & hdnPersonalComun.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisorFaena", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub DevolverMailSupervisor()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_licencias_medicas 'SMS1', '" & hdnIdAreaComun.Value & "','" & hdnEmpresaComun.Value & "','" & hdnPersonalComun.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub
    Private Sub comprobar_comprobante()
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim strSQL As String = "exec proc_validar_feriado 'COMEX', '" & txtComprobante.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnNumExist.Value = rdoReader(0).ToString
                Else
                    hdnNumExist.Value = ""
                End If
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("comprobar_comprobante", Err, Page, Master)
            End Try
        End Using
    End Sub


    Private Sub VacacionesActivas()
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim strSQL As String = "exec proc_validar_feriado 'VV','" & Trim(txtrut.Text) & "' , '" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnEstVacaciones.Value = 1
                Else
                    hdnEstVacaciones.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("VacacionesActivas", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Function VerificarFechas() As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim verificacion As Integer = 0

        comando.CommandText = "exec proc_validar_feriado 'VV','" & txtrut.Text & "' ,'" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "' ,'" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "'"
        comando.Connection = conx
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            verificacion = 1
        End If

        rdoReader.Close()
        conx.Close()

        Return verificacion

    End Function

    Private Sub ConfirmarProgramacion(ByVal numref As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            Dim DiasCorridos As Integer = 0
            Dim DiasLaboralesD As Double = 0
            Dim fechat As DateTime = txtfechat.Text
            Dim fechai As DateTime = txtfechai.Text

            If Month(CDate(txtfechat.Text)) = Month(CDate(txtfechai.Text)) Then
                DiasCorridos = Val(CDate(txtfechat.Text)) - Val(CDate(txtfechai.Text)) + 1
            ElseIf fechat > fechai Then
                If Month(CDate(txtfechai.Text)) = 2 Then
                    DiasCorridos = (28 - Day(CDate(txtfechai.Text))) + (Day(CDate(txtfechat.Text)) + 1)
                ElseIf Month(CDate(txtfechai.Text)) = 1 Or Month(CDate(txtfechai.Text)) = 3 Or Month(CDate(txtfechai.Text)) = 5 Or Month(CDate(txtfechai.Text)) = 7 Or Month(CDate(txtfechai.Text)) = 8 Or Month(CDate(txtfechai.Text)) = 10 Or Month(CDate(txtfechai.Text)) = 12 Then
                    DiasCorridos = (31 - Day(CDate(txtfechai.Text))) + (Day(CDate(txtfechat.Text)) + 1)
                ElseIf Month(CDate(txtfechai.Text)) = 4 Or Month(CDate(txtfechai.Text)) = 6 Or Month(CDate(txtfechai.Text)) = 9 Or Month(CDate(txtfechai.Text)) = 11 Then
                    DiasCorridos = (30 - Day(CDate(txtfechai.Text))) + (Day(CDate(txtfechat.Text)) + 1)
                End If
            End If

            DiasLaboralesD = DiasLaboraes(DiasCorridos)

            If CDate(txtfechai.Text) <= CDate(txtfechat.Text) Then
                Call TrapasarComun(Replace(DiasLaboralesD, ",", "."), DiasCorridos)

                'comando = New SqlCommand
                'comando.CommandType = CommandType.Text
                'comando.CommandText = "Insert Into JQRSGT..REMMov_movimiento_personal (num_comprobante, rut_personal, fec_ingreso, fec_inicio, fec_termino, dias_habiles, dias_corridos, cod_tipo_movimiento, nom_nota, cod_empresa, dias_progresivos, fec_inicio_periodo, fec_termino_periodo) " &
                '    "Values('" & txtComprobante.Text & "', '" & Trim(txtrut.Text) & "',GetDate(),'" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "', '" & CDate(txtfechat.Text).ToString("dd-MM-yyyy") & "','" & Replace(DiasLaboralesD, ",", ".") & "', " & DiasCorridos & ", " & 3 & ", '" & "" & "', " & hdnEmpresaComun.Value & ",'" & "" & "', '" & "" & "', '" & "" & "')"
                'comando.Connection = conx
                'conx.Open()
                'comando.ExecuteNonQuery()
                'conx.Close()
            Else
                MessageBox("Guardar", "La Fecha de Ingreso no puede ser Mayor a la Fecha de Termino", Page, Master, "W")
            End If

        Catch ex As Exception
            MessageBoxError("ConfirmarProgramacion", Err, Page, Master)
        End Try
    End Sub
    Private Sub TrapasarComun(ByVal diasLaboralas As Integer, ByVal diasCorridos As Integer)
        Try
            Dim check As Integer = 0
            If chkMedio.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            If MtvLicencias.ActiveViewIndex = 1 Then
                If ChkMedioAcc.Checked = True Then
                    check = 1
                Else
                    check = 0
                End If
            End If


            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IMP"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnEmpresaComun.Value
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtfechai.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtfechat.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = Trim(txtComprobante.Text)
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@diasLaborales", SqlDbType.NVarChar)
            comando.Parameters("@diasLaborales").Value = diasLaboralas
            comando.Parameters.Add("@diasCorridos", SqlDbType.NVarChar)
            comando.Parameters("@diasCorridos").Value = diasCorridos
            comando.Parameters.Add("@movimiento", SqlDbType.NVarChar)
            comando.Parameters("@movimiento").Value = 3
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("guardar_licencias_accidentes", Err, Page, Master)
        End Try
    End Sub

    Private Function DiasLaboraes(ByVal diasC As Integer) As Integer
        Dim fecha_inicial As String = txtfechai.Text
        Dim fecha_final As String = txtfechat.Text
        Dim dias As Integer = 0
        Dim DiaSemana As Integer = 0
        Dim x As Integer
        For x = 0 To (diasC - 1)
            DiaSemana = Weekday(System.DateTime.FromOADate(CDate(fecha_inicial).ToOADate + x), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
            If DiaSemana < 6 Then
                dias = dias + 1
            End If
        Next x
        Return dias
    End Function
    Public Sub SubirFTPDctoComun(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Try
            Dim clsRequest As FtpWebRequest = DirectCast(System.Net.WebRequest.Create("ftp://192.168.10.28/Seguros/DoctosPersonal/" & ruta & "/" & Replace(Path.GetFileName(uplArchivoComun.FileName), " ", "_")), System.Net.FtpWebRequest)
            clsRequest.Credentials = New System.Net.NetworkCredential(usuario, pass)
            clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            Dim bFile As Byte() = uplArchivoComun.FileBytes
            Dim clsStream As System.IO.Stream = clsRequest.GetRequestStream()
            clsStream.Write(bFile, 0, bFile.Length)
            clsStream.Close()
            clsStream.Dispose()
        Catch ex As Exception
            MessageBoxError("SubirFTPDctoComun", Err, Page, Master)
        End Try
    End Sub

    Public Sub GrabarDctoComun()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "INSERT INTO REMMov_documento_personal_licencias (rut_personal, nom_url, des_docto, ext_archivo, fec_add, usr_add, id_tipo_docto, num_comprobante)"
        strSQL = strSQL + " Values('" & Trim(txtrut.Text).ToString & "', '" & Replace(Path.GetFileName(uplArchivoComun.FileName), " ", "_") & "','Licencia Comun', '" & Path.GetExtension(uplArchivoComun.FileName) & "', getdate(),'" & Session("cod_usu_tc").ToString & "', 2,'" & Trim(txtComprobante.Text) & "')"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()

            Catch ex As Exception
                MessageBoxError("GrabarDctoComun", Err, Page, Master)
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Function ExisteCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String) As Boolean
        Dim Request As FtpWebRequest = CType(WebRequest.Create(New Uri("ftp://192.168.10.28/Seguros/DoctosPersonal/" & ruta)), FtpWebRequest)
        Request.Credentials = New NetworkCredential(usuario, pass)
        Request.Method = WebRequestMethods.Ftp.GetDateTimestamp
        Request.UsePassive = False
        Try
            Dim respuesta As FtpWebResponse
            respuesta = CType(Request.GetResponse(), FtpWebResponse)
            Return True
        Catch ex As WebException
            Return False
        End Try
    End Function

    Public Sub CrearCarpeta(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Dim crearDirectorio As FtpWebRequest = DirectCast(System.Net.FtpWebRequest.Create("ftp://192.168.10.28/Seguros/DoctosPersonal/" & ruta), System.Net.FtpWebRequest)
        crearDirectorio.Credentials = New NetworkCredential(usuario, pass)

        crearDirectorio.Method = WebRequestMethods.Ftp.MakeDirectory
        Dim respuesta As FtpWebResponse = crearDirectorio.GetResponse()
    End Sub

    Private Sub AdjuntarDoctoComun()
        If ExisteCarpeta("Doctos_" & Trim(txtrut.Text).ToString & "_" & txtComprobante.Text, "usuarioftp", "Ftp2018.") = False Then
            Call CrearCarpeta("Doctos_" & Trim(txtrut.Text).ToString & "_" & txtComprobante.Text, "usuarioftp", "Ftp2018.")
        End If
        If uplArchivoComun.HasFile = True Then
            If Path.GetExtension(uplArchivoComun.FileName) = ".pdf" Or Path.GetExtension(uplArchivoComun.FileName) = ".doc" Or Path.GetExtension(uplArchivoComun.FileName) = ".docx" Or Path.GetExtension(uplArchivoComun.FileName) = ".xls" Or Path.GetExtension(uplArchivoComun.FileName) = ".xlsx" Then
                Call SubirFTPDctoComun("Doctos_" & Trim(txtrut.Text).ToString & "_" & txtComprobante.Text, "usuarioftp", "Ftp2018.")
                Call GrabarDctoComun()
            Else
                MessageBox("Adjuntar", "Los Documento deben ser .pdf .doc o .docx", Page, Master, "W")
            End If
        End If
    End Sub


    Private Sub EnviarCorreoLicenciaComun()
        If hdnIdAreaComun.Value = 30 Or hdnIdAreaComun.Value = 31 Then
            Call BuscarFaena()
            Call DevolverMailSupervisorFaena()
        Else
            Call DevolverMailSupervisor()
        End If
        Try
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("" & hdnMailSupervisor.Value & "")
            correo.Subject = "Licencia Medica Comun"
            correo.Body = "[LM] Estimado (a), se ha ingresado una nueva licencia medica común de Don (a) " & Trim(txtNombreLicComun.Text) & ", con fecha de inicio: " & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & " hasta " & CDate(txtfechat.Text).ToString("dd-MM-yyyy") &
                ". La informacion registra puede visualizarla en la plataforma Vitrack."
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            'correo.Attachments.Add(New Attachment(Server.MapPath(archivo)))
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("Supervisor no posee correo electronico valido", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarComun()
        txtrut.Text = ""
        txtNombreLicComun.Text = ""
        dtFecEmisionLic.Text = ""
        dtFechaPreseLic.Text = ""
        dtFecTramiLic.Text = ""
        txtDiasLicComun.Text = ""
        txtComprobante.Text = ""
        txtfechai.Text = ""
        txtfechat.Text = ""
        txtComprobante.Text = ""
        txtEspecialidad.Text = ""
        txtNomMedico.Text = ""
        txtDiasLicComun.Text = ""
        hdnEmpresaComun.Value = 0
        hdnActivo.Value = 0
        lblEmpLicComun.Text = "-"
        hdnComprobanteGrdComun.Value = ""
        hdnRutGrdComun.Value = ""
        hdnFiltroSelComun.Value = 0
        Call CargarGrillaComunRut("")
        hdnExpotar.Value = 0
        ChkMedioAcc.Checked = False
    End Sub

    Function ValidarMovimiento()
        Dim Ok As Boolean = False
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "select num_comprobante from REMMov_movimiento_personal Where num_comprobante='" & txtComprobante.Text & "' and rut_personal='" & Trim(txtrut.Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Ok = True
                Else
                    Ok = False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("grdLicenciaComun_RowDataBound", Err, Page, Master)
        End Try
        Return Ok
    End Function

    Private Sub guardar_licencias_comunes()
        Try
            Dim check As Integer = 0
            If chkMedio.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IC"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnEmpresaComun.Value
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtfechai.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtfechat.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = txtComprobante.Text
            comando.Parameters.Add("@fec_emision_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_emision_lic").Value = CDate(dtFecEmisionLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_presentacion_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_presentacion_lic").Value = CDate(dtFechaPreseLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_tramitacion_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_tramitacion_lic").Value = CDate(dtFecTramiLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@nom_especialidad", SqlDbType.NVarChar)
            comando.Parameters("@nom_especialidad").Value = txtEspecialidad.Text
            comando.Parameters.Add("@nom_medico", SqlDbType.NVarChar)
            comando.Parameters("@nom_medico").Value = txtNomMedico.Text
            comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
            comando.Parameters("@fec_registro").Value = Date.Now.ToString
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("guardar_licencias_comunes", Err, Page, Master)
        End Try
    End Sub

    Private Sub actualizar_licencias_comunes()
        Try
            Dim check As Integer = 0
            If chkMedio.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UC"
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtfechai.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtfechat.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = txtComprobante.Text
            comando.Parameters.Add("@fec_emision_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_emision_lic").Value = CDate(dtFecEmisionLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_presentacion_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_presentacion_lic").Value = CDate(dtFechaPreseLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_tramitacion_lic", SqlDbType.NVarChar)
            comando.Parameters("@fec_tramitacion_lic").Value = CDate(dtFecTramiLic.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@nom_especialidad", SqlDbType.NVarChar)
            comando.Parameters("@nom_especialidad").Value = txtEspecialidad.Text
            comando.Parameters.Add("@nom_medico", SqlDbType.NVarChar)
            comando.Parameters("@nom_medico").Value = txtNomMedico.Text
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("actualizar_licencias_comunes", Err, Page, Master)
        End Try
    End Sub

    Private Sub EliminarDoctosAntiguos(ByVal codigo As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "delete FROM REMMov_documento_personal_licencias WHERE num_comprobante = '" & codigo & "' "
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()

            Catch ex As Exception
                MessageBoxError("EliminarDoctosAntiguos", Err, Page, Master)
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub BorrarFicheroFTP(ByVal codigo As String, ByVal ruta As String)
        Dim DireccionyFichero As String
        DireccionyFichero = "ftp://192.168.10.28/Seguros/DoctosPersonal/Doctos_" + codigo + "/" + ruta
        Dim peticionFTP As FtpWebRequest
        peticionFTP = CType(WebRequest.Create(New Uri(DireccionyFichero)), FtpWebRequest)
        peticionFTP.Credentials = New NetworkCredential("usuarioftp", "Ftp2018.")
        peticionFTP.Method = WebRequestMethods.Ftp.DeleteFile
        peticionFTP.UsePassive = False
        Try
            Dim respuestaFTP As FtpWebResponse
            respuestaFTP = CType(peticionFTP.GetResponse(), FtpWebResponse)
            respuestaFTP.Close()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnGuardarComun_Click(sender As Object, e As EventArgs) Handles btnGuardarComun.Click
        If txtrut.Text = "" Or txtNombreLicComun.Text = "" Or dtFecEmisionLic.Text = "" Or dtFechaPreseLic.Text = "" Or dtFecTramiLic.Text = "" Or txtEspecialidad.Text = "" Or txtNomMedico.Text = "" Or txtComprobante.Text = "" Or txtfechai.Text = "" Or txtfechat.Text = "" Then
            MessageBox("Guardar", "Faltan datos por ingresar", Page, Master, "W")
        Else
            System.Threading.Thread.Sleep(5000)
            Dim check As Integer = 0
            If chkMedio.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            If hdnPersonalComun.Value = 1 Then
                If hdnIdAreaComun.Value = 30 Or hdnIdAreaComun.Value = 31 Then
                    Call BuscarFaena()
                    Call DevolverMailSupervisorFaena()
                Else
                    Call DevolverMailSupervisor()
                End If

                If hdnMailSupervisor.Value = "" Then
                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor asignar validador", Page, Master, "W")
                Else
                    If hdnActivo.Value = 0 Then
                        Call comprobar_comprobante()
                        If hdnNumExist.Value = "" Then
                            Call VacacionesActivas()
                            If hdnEstVacaciones.Value = 1 Then
                                MessageBox("Guardar", "La fecha de Ingreso de licencia medica no es posible ya que esta persona se encuetra actualmente en periodo de vacaciones.", Page, Master, "W")
                            Else
                                If VerificarFechas() = 0 Then
                                    Call guardar_licencias_comunes()
                                    Call ConfirmarProgramacion(txtComprobante.Text)
                                    Call AdjuntarDoctoComun()
                                    'Call DesactivarConductor()
                                    Call EnviarCorreoLicenciaComun()
                                    MessageBox("Guardar", "Registro Exitoso", Page, Master, "S")
                                    Call LimpiarComun()
                                ElseIf VerificarFechas() = 1 Then
                                    MessageBox("Guardar", "La fecha de Ingreso de licencia medica se encuentra entre un rango de Fechas de un Movimiento ya almacenada.", Page, Master, "W")
                                End If
                            End If
                        Else
                            MessageBox("Guardar", "Numero de comprobante ya existe", Page, Master, "W")
                        End If
                    Else

                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechat As DateTime = txtfechat.Text
                        Dim fechai As DateTime = txtfechai.Text
                        If Month(CDate(txtfechat.Text)) = Month(CDate(txtfechai.Text)) Then
                            DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                        ElseIf fechat > fechai Then
                            If Month(CDate(txtfechai.Text)) = 2 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            ElseIf Month(CDate(txtfechai.Text)) = 1 Or Month(CDate(txtfechai.Text)) = 3 Or Month(CDate(txtfechai.Text)) = 5 Or Month(CDate(txtfechai.Text)) = 7 Or Month(CDate(txtfechai.Text)) = 8 Or Month(CDate(txtfechai.Text)) = 10 Or Month(CDate(txtfechai.Text)) = 12 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            ElseIf Month(CDate(txtfechai.Text)) = 4 Or Month(CDate(txtfechai.Text)) = 6 Or Month(CDate(txtfechai.Text)) = 9 Or Month(CDate(txtfechai.Text)) = 11 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            End If
                        End If

                        DiasLaboralesD = DiasLaboraes(DiasCorridos)

                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand

                        If ValidarMovimiento() = True Then
                            Dim sql As String = "Update JQRSGT..REMMov_movimiento_personal Set fec_inicio='" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "', fec_termino='" & CDate(txtfechat.Text).ToString("dd-MM-yyyy") & "', " &
                                "dias_habiles='" & Replace(DiasLaboralesD, ",", ".") & "', act_mediodia=" & check & ", dias_corridos='" & DiasCorridos & "'" &
                                 "Where num_comprobante='" & txtComprobante.Text & "' and rut_personal='" & Trim(txtrut.Text) & "'"
                            comando = New SqlCommand
                            comando.CommandType = CommandType.Text
                            comando.CommandText = Sql
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                        Else
                            Dim sql As String = "Insert Into JQRSGT..REMMov_movimiento_personal (num_comprobante, rut_personal, fec_ingreso, fec_inicio, fec_termino, dias_habiles, dias_corridos, cod_tipo_movimiento, nom_nota, cod_empresa, dias_progresivos, fec_inicio_periodo, fec_termino_periodo,act_mediodia) " &
                                "Values('" & txtComprobante.Text & "', '" & Trim(txtrut.Text) & "',GetDate(),'" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "', '" & CDate(txtfechat.Text).ToString("dd-MM-yyyy") & "','" & Replace(DiasLaboralesD, ",", ".") & "', " & DiasCorridos & ", " & 3 & ", '" & "" & "', " & hdnEmpresaComun.Value & ",'" & "" & "', '" & "" & "', '" & "" & "'," & check & ")"

                            comando = New SqlCommand
                            comando.CommandType = CommandType.Text
                            comando.CommandText = sql
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                        End If


                        If uplArchivoComun.HasFile = False Then
                            Call actualizar_licencias_comunes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarComun()
                        Else
                            Call EliminarDoctosAntiguos(Trim(txtComprobante.Text))
                            Call BorrarFicheroFTP(Trim(txtrut.Text).ToString & "_" & Trim(txtComprobante.Text), hdnUrlActComun.Value)
                            Call AdjuntarDoctoComun()
                            Call actualizar_licencias_comunes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarComun()
                        End If
                    End If
                End If

            ElseIf hdnPersonalComun.Value = 2 Then

                If hdnIdAreaComun.Value = 30 Or hdnIdAreaComun.Value = 31 Then
                    Call BuscarFaena()
                    Call DevolverMailSupervisorFaena()
                Else
                    Call DevolverMailSupervisor()
                End If

                If hdnMailSupervisor.Value = "" Then
                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor asignar validador", Page, Master, "W")
                Else
                    If hdnActivo.Value = 0 Then
                        Call comprobar_comprobante()
                        If hdnNumExist.Value = "" Then
                            Call VacacionesActivas()
                            If hdnEstVacaciones.Value = 1 Then
                                MessageBox("Guardar", "La fecha de Ingreso de licencia medica no es posible ya que esta persona se encuetra actualmente en periodo de vacaciones.", Page, Master, "W")
                            Else
                                If VerificarFechas() = 0 Then
                                    Call AdjuntarDoctoComun()
                                    Call guardar_licencias_comunes()
                                    Call ConfirmarProgramacion(txtComprobante.Text)
                                    ' Call DesactivarConductor() ' dejar comentado
                                    Call EnviarCorreoLicenciaComun()
                                    MessageBox("Guardar", "Registro Exitoso", Page, Master, "S")
                                    Call LimpiarComun()
                                ElseIf VerificarFechas() = 1 Then
                                    MessageBox("Guardar", "La fecha de Ingreso de licencia medica se encuentra entre un rango de Fechas de un Movimiento ya almacenada.", Page, Master, "S")
                                End If
                            End If
                        Else
                            MessageBox("Guardar", "Numero de comprobante ya existe", Page, Master, "W")
                        End If

                    Else

                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechat As DateTime = txtfechat.Text
                        Dim fechai As DateTime = txtfechai.Text
                        If Month(CDate(txtfechat.Text)) = Month(CDate(txtfechai.Text)) Then
                            DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                        ElseIf fechat > fechai Then
                            If Month(CDate(txtfechai.Text)) = 2 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            ElseIf Month(CDate(txtfechai.Text)) = 1 Or Month(CDate(txtfechai.Text)) = 3 Or Month(CDate(txtfechai.Text)) = 5 Or Month(CDate(txtfechai.Text)) = 7 Or Month(CDate(txtfechai.Text)) = 8 Or Month(CDate(txtfechai.Text)) = 10 Or Month(CDate(txtfechai.Text)) = 12 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            ElseIf Month(CDate(txtfechai.Text)) = 4 Or Month(CDate(txtfechai.Text)) = 6 Or Month(CDate(txtfechai.Text)) = 9 Or Month(CDate(txtfechai.Text)) = 11 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtfechai.Text)), (CDate(txtfechat.Text))) + 1
                            End If
                        End If
                        DiasLaboralesD = DiasLaboraes(DiasCorridos)
                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand

                        If ValidarMovimiento() = True Then
                            Dim stringSql As String = "Update JQRSGT..REMMov_movimiento_personal Set fec_inicio='" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "', fec_termino='" & CDate(txtfechat.Text).ToString("dd-MM-yyyy") & "', " &
                                "dias_habiles='" & Replace(DiasLaboralesD, ",", ".") & "',  act_mediodia=" & check & ",dias_corridos='" & DiasCorridos & "'" &
                                 "Where num_comprobante='" & txtComprobante.Text & "' and rut_personal='" & Trim(txtrut.Text) & "'"
                            comando = New SqlCommand
                            comando.CommandType = CommandType.Text
                            comando.CommandText = stringSql
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                        Else
                            Dim sql As String = "Insert Into JQRSGT..REMMov_movimiento_personal (num_comprobante, rut_personal, fec_ingreso, fec_inicio, fec_termino, dias_habiles, dias_corridos, cod_tipo_movimiento, nom_nota, cod_empresa, dias_progresivos, fec_inicio_periodo, fec_termino_periodo,act_mediodia) " &
                                "Values('" & txtComprobante.Text & "', '" & Trim(txtrut.Text) & "',GetDate(),'" & CDate(txtfechai.Text).ToString("dd-MM-yyyy") & "', '" & CDate(txtfechat.Text).ToString("dd-MM-yyyy") & "','" & Replace(DiasLaboralesD, ",", ".") & "', " & DiasCorridos & ", " & 3 & ", '" & "" & "', " & hdnEmpresaComun.Value & ",'" & "" & "', '" & "" & "', '" & "" & "'," & check & ")"
                            comando = New SqlCommand
                            comando.CommandType = CommandType.Text
                            comando.CommandText = sql
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                        End If

                        If uplArchivoComun.HasFile = False Then
                            Call actualizar_licencias_comunes()
                            Call ConfirmarProgramacion(txtComprobante.Text)
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarComun()
                        Else
                            Call EliminarDoctosAntiguos(Trim(txtComprobante.Text))
                            Call BorrarFicheroFTP(Trim(txtrut.Text).ToString & "_" & Trim(txtComprobante.Text), hdnUrlActComun.Value)
                            Call AdjuntarDoctoComun()
                            Call actualizar_licencias_comunes()
                            Call ConfirmarProgramacion(txtComprobante.Text)
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarComun()
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub txtrut_TextChanged(sender As Object, e As EventArgs) Handles txtrut.TextChanged
        txtrut.Text = Replace(txtrut.Text, ".", "")
        txtrut.Text = Replace(txtrut.Text, "-", "")
        If Len(txtrut.Text) = 8 Then
            txtrut.Text = Left(txtrut.Text, 7) + "-" + Right(txtrut.Text, 1)
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = Left(txtrut.Text, 8) + "-" + Right(txtrut.Text, 1)
        End If

        If Len(txtrut.Text) = 10 Then
            txtrut.Text = txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call BuscarPersonal()
                Call CargarGrillaComunRut(txtrut.Text)
                btnGuardarComun.Enabled = True
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnGuardarComun.Enabled = False
            End If
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = "0" + txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call BuscarPersonal()
                Call CargarGrillaComunRut(txtrut.Text)
                btnGuardarComun.Enabled = True
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnGuardarComun.Enabled = False
            End If
        End If
    End Sub

    Private Sub BuscarPersonal()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec proc_licencias_medicas 'DS', '" & Trim(txtrut.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                lblEmpLicComun.Text = rdoReader(0).ToString
                txtNombreLicComun.Text = rdoReader(1).ToString
                txtrut.Text = Trim(rdoReader(2).ToString)
                hdnEmpresaComun.Value = rdoReader(3).ToString
                hdnIdAreaComun.Value = rdoReader(4).ToString
                If hdnIdAreaComun.Value = 30 Or hdnIdAreaComun.Value = 31 Then
                    Call BuscarFaena()
                    hdnPersonalComun.Value = 1
                Else
                    hdnPersonalComun.Value = 2
                End If

            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Private Sub BuscarFaena()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec proc_licencias_medicas 'RF', '" & Trim(txtrut.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnCodFaena.Value = rdoReader(0).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarFaena", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtDiasLicComun_TextChanged(sender As Object, e As EventArgs) Handles txtDiasLicComun.TextChanged
        If txtfechai.Text <> "" Then
            Dim hoy As DateTime = txtfechai.Text
            Dim diaFinal As DateTime = hoy.AddDays(txtDiasLicComun.Text - 1)
            txtfechat.Text = CDate(diaFinal).ToString("yyyy-MM-dd")
        End If

    End Sub

    Protected Sub txtfechai_TextChanged(sender As Object, e As EventArgs) Handles txtfechai.TextChanged
        If txtDiasLicComun.Text <> "" Then
            Dim hoy As DateTime = txtfechai.Text
            Dim diaFinal As DateTime = hoy.AddDays(txtDiasLicComun.Text - 1)
            txtfechat.Text = CDate(diaFinal).ToString("yyyy-MM-dd")
        End If
    End Sub

    Protected Sub txtComprobante_TextChanged(sender As Object, e As EventArgs) Handles txtComprobante.TextChanged
        Call CargarGrillaComunComp(txtComprobante.Text)
    End Sub

    Private Sub DescargarFTPComun(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usuarioftp:Ftp2018.@192.168.10.28/Seguros/DoctosPersonal/Doctos_" & hdnRutGrdComun.Value & "_" & hdnComprobanteGrdComun.Value & "/" & nombre, Server.MapPath(archivo), "usuarioftp", "Ftp2018.")
        Catch ex As Exception
            MessageBoxError("DescargarFTPComun", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdLicenciaComun_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLicenciaComun.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdLicenciaComun.Rows(intRow)
        If Trim(LCase(e.CommandName)) = "1" Then

            hdnRutGrdComun.Value = CType(Row.FindControl("lblRutComun"), Label).Text
            hdnComprobanteGrdComun.Value = CType(Row.FindControl("lblNumComprobante"), Label).Text

            If CType(Row.FindControl("hdnExtensionComun"), HiddenField).Value = ".pdf" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value

                Call DescargarFTPComun(archivo, CType(Row.FindControl("hdnurlComun"), HiddenField).Value)

                Response.ContentType = "application/pdf"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            ElseIf CType(Row.FindControl("hdnExtensionComun"), HiddenField).Value = ".doc" Or CType(Row.FindControl("hdnExtensionComun"), HiddenField).Value = ".docx" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value

                Call DescargarFTPComun(archivo, CType(Row.FindControl("hdnurlComun"), HiddenField).Value)

                Response.ContentType = "application/ms-word"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            ElseIf CType(Row.FindControl("hdnExtensionComun"), HiddenField).Value = ".xlsx" Or CType(Row.FindControl("hdnExtensionComun"), HiddenField).Value = ".xls" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value

                Call DescargarFTPComun(archivo, CType(Row.FindControl("hdnurlComun"), HiddenField).Value)

                Response.ContentType = "application/xls"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlComun"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            End If
        ElseIf Trim(LCase(e.CommandName)) = "2" Then

            Call Rescatar_ID_Mov(CType(row.FindControl("lblRutComun"), Label).Text, CType(row.FindControl("lblNumComprobante"), Label).Text)

            hdnId.Value = CType(row.FindControl("hdn_id"), HiddenField).Value
            Call eliminar_movimiento_personal()
            Call eliminar_licencias_comunes()

            Call EliminarDoctosAntiguos(Trim(txtComprobante.Text))
            Call BorrarFicheroFTP(Trim(txtrut.Text).ToString & "_" & Trim(txtComprobante.Text), hdnUrlActComun.Value)

            MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
            Call LimpiarComun()
        End If
    End Sub

    Private Sub eliminar_licencias_comunes()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EC"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("eliminar_licencias_comunes", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdLicenciaComun_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdLicenciaComun.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim rdoReader As SqlDataReader
                Dim cmdTemporal As SqlCommand
                Dim strSQL As String = "Exec proc_licencias_medicas 'SD', '" & HttpUtility.HtmlDecode(CType(row.FindControl("lblNumComprobante"), Label).Text) & "'"
                Using cnxBaseDatos As New SqlConnection(strCnx)
                    cnxBaseDatos.Open()
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)

                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        CType(row.FindControl("btnDescargarComun"), LinkButton).Visible = True
                    Else
                        CType(row.FindControl("btnDescargarComun"), LinkButton).Visible = False
                    End If
                End Using
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
            End If
        Catch ex As Exception
            MessageBoxError("grdLicenciaComun_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdLicenciaComun_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdLicenciaComun.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdLicenciaComun.SelectedRow
            Dim obj_hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_urlActual As HiddenField = CType(row.FindControl("hdnurlComun"), HiddenField)

            Dim rut_obj As Label = CType(row.FindControl("lblRutComun"), Label)
            Dim nombre_obj As Label = CType(row.FindControl("lblNombreComun"), Label)
            Dim empresa_obj As Label = CType(row.FindControl("lblEmpresaComun"), Label)
            Dim fechaini_obj As Label = CType(row.FindControl("lblFecInicio"), Label)
            Dim fechater_obj As Label = CType(row.FindControl("lblFecTermino"), Label)
            Dim comprobante_obj As Label = CType(row.FindControl("lblNumComprobante"), Label)
            Dim fechaemision_obj As Label = CType(row.FindControl("lblFecEmisionComun"), Label)
            Dim fechapresentacion_obj As Label = CType(row.FindControl("lblFecPresentacionComun"), Label)
            Dim fechatramite_obj As Label = CType(row.FindControl("lblFecTramiteComun"), Label)
            Dim especialidad_obj As Label = CType(row.FindControl("lblEspecialidadComun"), Label)
            Dim medico_obj As Label = CType(row.FindControl("lblMedicoComun"), Label)


            hdnId.Value = obj_hdnid.Value
            hdnUrlActComun.Value = obj_urlActual.Value
            txtrut.Text = rut_obj.Text
            Call BuscarPersonal()
            txtComprobante.Text = comprobante_obj.Text
            txtfechai.Text = CDate(fechaini_obj.Text).ToString("yyyy-MM-dd")
            Call Rescatar_ID_Mov(Trim(txtrut.Text), txtComprobante.Text)
            txtDiasLicComun.Text = CType(row.FindControl("lblDiasLicencias"), Label).Text
            txtfechat.Text = CDate(fechater_obj.Text).ToString("yyyy-MM-dd")
            dtFecEmisionLic.Text = CDate(fechaemision_obj.Text).ToString("yyyy-MM-dd")
            dtFecTramiLic.Text = CDate(fechatramite_obj.Text).ToString("yyyy-MM-dd")
            dtFechaPreseLic.Text = CDate(fechapresentacion_obj.Text).ToString("yyyy-MM-dd")
            txtEspecialidad.Text = especialidad_obj.Text
            txtNomMedico.Text = medico_obj.Text
            Dim medio_obj As HiddenField = CType(row.FindControl("hdnMedio"), HiddenField)
            chkMedio.Checked = medio_obj.Value
            'btnEliminar.Enabled = True
            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdLicenciaComun_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Rescatar_ID_Mov(ByVal rut As String, ByVal comprobante As String)
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec proc_licencias_medicas 'RID', '" & rut & "', '" & comprobante & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnIdMovimiento.Value = rdoReader(0).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("escatar_ID_Mov", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnLComun_Click(sender As Object, e As EventArgs) Handles btnLComun.Click
        MtvLicencias.ActiveViewIndex = 0
        lbltitulo.Text = "Licencias Médica"
    End Sub

    Protected Sub btnLAccidente_Click(sender As Object, e As EventArgs) Handles btnLAccidente.Click
        MtvLicencias.ActiveViewIndex = 1
        lbltitulo.Text = "Licencias Laboral"
    End Sub

    Private Sub BuscarPersonalAccidente()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec proc_licencias_medicas 'DS', '" & Trim(txtRutAcci.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                lblEmpAcci.Text = rdoReader(0).ToString
                txtBuscarnombreAcci.Text = rdoReader(1).ToString
                txtRutAcci.Text = Trim(rdoReader(2).ToString)
                hdnEmpresaAccidente.Value = rdoReader(3).ToString
                hdnIdAreaAcci.Value = rdoReader(4).ToString

                If hdnIdAreaAcci.Value = 30 Or hdnIdAreaAcci.Value = 31 Then
                    Call BuscarFaena()
                    hdnPersonalAcci.Value = 1
                Else
                    hdnPersonalAcci.Value = 2
                End If

            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonalAccidente", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtRutAcci_TextChanged(sender As Object, e As EventArgs) Handles txtRutAcci.TextChanged
        If txtRutAcci.Text <> "" Then
            txtRutAcci.Text = Replace(txtRutAcci.Text, ".", "")
            txtRutAcci.Text = Replace(txtRutAcci.Text, "-", "")
            If Len(txtRutAcci.Text) = 8 Then
                txtRutAcci.Text = Left(txtRutAcci.Text, 7) + "-" + Right(txtRutAcci.Text, 1)
            ElseIf Len(txtRutAcci.Text) = 9 Then
                txtRutAcci.Text = Left(txtRutAcci.Text, 8) + "-" + Right(txtRutAcci.Text, 1)
            End If

            If Len(txtRutAcci.Text) = 10 Then
                txtRutAcci.Text = txtRutAcci.Text
                If ValidarRut(txtRutAcci) = True Then
                    Call BuscarPersonalAccidente()
                    Call CargarGrillaAccidenteRut(txtRutAcci.Text)
                    btnGuardarAcci.Enabled = True
                Else
                    MessageBox("Rut", "RUT Incorrecto", Page, Master, "E")
                    btnGuardarAcci.Enabled = False
                End If
            ElseIf Len(txtRutAcci.Text) = 9 Then
                txtRutAcci.Text = "0" + txtRutAcci.Text
                If ValidarRut(txtRutAcci) = True Then
                    Call BuscarPersonalAccidente()
                    Call CargarGrillaAccidenteRut(txtRutAcci.Text)
                    btnGuardarAcci.Enabled = True
                Else
                    MessageBox("Rut", "RUT Incorrecto", Page, Master, "E")
                    btnGuardarAcci.Enabled = False
                End If
            End If
        Else
            MessageBox("Rut", "Deb Ingresar RUT", Page, Master, "E")
        End If

    End Sub
    Private Sub BuscarFaenaAcci()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec proc_licencias_medicas 'RF', '" & Trim(txtRutAcci.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                hdnCodFaena.Value = rdoReader(0).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarFaenaAcci", Err, Page, Master)
        End Try
    End Sub

    Private Sub DevolverMailSupervisorFaenaAcci()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_licencias_medicas 'SMS1', '" & hdnCodFaena.Value & "','" & hdnEmpresaAccidente.Value & "','" & hdnPersonalAcci.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisorFaenaAcci", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub DevolverMailSupervisorAcci()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_licencias_medicas 'SMS1', '" & hdnIdAreaAcci.Value & "','" & hdnEmpresaAccidente.Value & "','" & hdnPersonalAcci.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisorAcci", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub comprobar_comprobante_accidente()
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim strSQL As String = "exec proc_validar_feriado 'COMEX', '" & txtNumComprobanteAcc.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnNumExistAcci.Value = rdoReader(0).ToString
                Else
                    hdnNumExistAcci.Value = ""
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
            Catch ex As Exception
                MessageBoxError("comprobar_comprobante_accidente", Err, Page, Master)
            End Try
        End Using
        rdoReader = Nothing
        cmdTemporal = Nothing
    End Sub


    Private Sub VacacionesActivasAcci()
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim strSQL As String = "select id_movimiento from REMMov_movimiento_personal where rut_personal = '" & Trim(txtRutAcci.Text) & "' and '" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "' between fec_inicio and fec_termino and  cod_tipo_movimiento = 11"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnEstVacacionesAcci.Value = 1
                Else
                    hdnEstVacacionesAcci.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
            Catch ex As Exception
                MessageBoxError("VacacionesActivasAcci", Err, Page, Master)
            End Try
        End Using
        rdoReader = Nothing
        cmdTemporal = Nothing
    End Sub

    Private Function VerificarFechasAccidente() As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim verificacion As Integer = 0

        comando.CommandText = "Select M.num_comprobante From JQRSGT..REMMov_movimiento_personal M where M.rut_personal='" & txtRutAcci.Text & "' and M.fec_inicio >= '" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "' and M.fec_termino <= '" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "' Order by id_movimiento desc"
        comando.Connection = conx
        conx.Open()

        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            verificacion = 1
        End If

        rdoReader.Close()
        conx.Close()

        Return verificacion
    End Function
    Public Sub SubirFTPDcto(ByRef ruta As String, ByRef usuario As String, ByRef pass As String)
        Try
            Dim clsRequest As FtpWebRequest = DirectCast(System.Net.WebRequest.Create("ftp://192.168.10.28/Seguros/DoctosPersonal/" & ruta & "/" & Replace(Path.GetFileName(uplArchivoAcci.FileName), " ", "_")), System.Net.FtpWebRequest)
            clsRequest.Credentials = New System.Net.NetworkCredential(usuario, pass)
            clsRequest.Method = System.Net.WebRequestMethods.Ftp.UploadFile
            Dim bFile As Byte() = uplArchivoAcci.FileBytes
            Dim clsStream As System.IO.Stream = clsRequest.GetRequestStream()
            clsStream.Write(bFile, 0, bFile.Length)
            clsStream.Close()
            clsStream.Dispose()
        Catch ex As Exception
            MessageBoxError("SubirFTPDcto", Err, Page, Master)
        End Try
    End Sub

    Public Sub GrabarDctoAcci()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "INSERT INTO REMMov_documento_personal_licencias (rut_personal, nom_url, des_docto, ext_archivo, fec_add, usr_add, id_tipo_docto, num_comprobante) " &
                    "Values('" & Trim(txtRutAcci.Text).ToString & "', '" & Replace(Path.GetFileName(uplArchivoAcci.FileName), " ", "_") & "','" & "Licencia Accidente Laboral" & "', '" & Path.GetExtension(uplArchivoAcci.FileName) & "', getdate(),'" & Session("cod_usu_tc").ToString & "', 2,'" & Trim(txtNumComprobanteAcc.Text) & "')"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()

            Catch ex As Exception
                MessageBoxError("GrabarDctoComun", Err, Page, Master)
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub AdjuntarDoctoAccidente()
        If ExisteCarpeta("Doctos_" & Trim(txtRutAcci.Text).ToString & "_" & txtNumComprobanteAcc.Text, "usuarioftp", "Ftp2018.") = False Then
            Call CrearCarpeta("Doctos_" & Trim(txtRutAcci.Text).ToString & "_" & txtNumComprobanteAcc.Text, "usuarioftp", "Ftp2018.")
        End If
        If uplArchivoAcci.HasFile = True Then
            If Path.GetExtension(uplArchivoAcci.FileName) = ".pdf" Or Path.GetExtension(uplArchivoAcci.FileName) = ".doc" Or Path.GetExtension(uplArchivoAcci.FileName) = ".docx" Or Path.GetExtension(uplArchivoAcci.FileName) = ".xls" Or Path.GetExtension(uplArchivoAcci.FileName) = ".xlsx" Then
                Call SubirFTPDcto("Doctos_" & Trim(txtRutAcci.Text).ToString & "_" & txtNumComprobanteAcc.Text, "usuarioftp", "Ftp2018.")
                Call GrabarDctoAcci()
            Else
                MessageBox("Adjuntar", "Los Documento deben ser .pdf .doc o .docx", Page, Master, "W0")
            End If
        End If
    End Sub
    Private Sub guardar_licencias_accidentes()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IA"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRutAcci.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnEmpresaAccidente.Value
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = Trim(txtNumComprobanteAcc.Text)
            comando.Parameters.Add("@fec_accidente", SqlDbType.NVarChar)
            comando.Parameters("@fec_accidente").Value = CDate(dtFecAccidente.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_alta", SqlDbType.NVarChar)
            comando.Parameters("@fec_alta").Value = CDate(dtAlta.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@dias_perdidos", SqlDbType.NVarChar)
            comando.Parameters("@dias_perdidos").Value = cboDiasPerdidos.SelectedValue
            comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
            comando.Parameters("@fec_registro").Value = Date.Now.ToString
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu_tc")
            Dim check As Integer = 0
            If ChkMedioAcc.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("guardar_licencias_accidentes", Err, Page, Master)
        End Try
    End Sub

    Private Sub TrapasarAccidente(ByVal diasLaboralas As Integer, ByVal diasCorridos As Integer)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IMP"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRutAcci.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnEmpresaAccidente.Value
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = Trim(txtNumComprobanteAcc.Text)
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("cod_usu_tc")
            comando.Parameters.Add("@diasLaborales", SqlDbType.NVarChar)
            comando.Parameters("@diasLaborales").Value = diasLaboralas
            comando.Parameters.Add("@diasCorridos", SqlDbType.NVarChar)
            comando.Parameters("@diasCorridos").Value = diasCorridos
            comando.Parameters.Add("@movimiento", SqlDbType.NVarChar)
            comando.Parameters("@movimiento").Value = 8
            Dim check As Integer = 0
            If ChkMedioAcc.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("guardar_licencias_accidentes", Err, Page, Master)
        End Try
    End Sub

    Private Sub ConfirmarProgramacionAccidente(ByVal numref As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            Dim DiasCorridos As Integer = 0
            Dim DiasLaboralesD As Double = 0
            Dim fechat As DateTime = txtFechaTerAcc.Text
            Dim fechai As DateTime = txtFecnaIniAcc.Text

            If Month(CDate(txtFechaTerAcc.Text)) = Month(CDate(txtFecnaIniAcc.Text)) Then
                DiasCorridos = Val(CDate(txtFechaTerAcc.Text)) - Val(CDate(txtFecnaIniAcc.Text)) + 1
            ElseIf fechat > fechai Then
                If Month(CDate(txtFecnaIniAcc.Text)) = 2 Then
                    DiasCorridos = (28 - Day(CDate(txtFecnaIniAcc.Text))) + (Day(CDate(txtFechaTerAcc.Text)) + 1)
                ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 1 Or Month(CDate(txtFecnaIniAcc.Text)) = 3 Or Month(CDate(txtFecnaIniAcc.Text)) = 5 Or Month(CDate(txtFecnaIniAcc.Text)) = 7 Or Month(CDate(txtFecnaIniAcc.Text)) = 8 Or Month(CDate(txtFecnaIniAcc.Text)) = 10 Or Month(CDate(txtFecnaIniAcc.Text)) = 12 Then
                    DiasCorridos = (31 - Day(CDate(txtFecnaIniAcc.Text))) + (Day(CDate(txtFechaTerAcc.Text)) + 1)
                ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 4 Or Month(CDate(txtFecnaIniAcc.Text)) = 6 Or Month(CDate(txtFecnaIniAcc.Text)) = 9 Or Month(CDate(txtFecnaIniAcc.Text)) = 11 Then
                    DiasCorridos = (30 - Day(CDate(txtFecnaIniAcc.Text))) + (Day(CDate(txtFechaTerAcc.Text)) + 1)
                End If
            End If

            DiasLaboralesD = DiasLaboraesAcci(DiasCorridos)

            If CDate(txtFecnaIniAcc.Text) <= CDate(txtFechaTerAcc.Text) Then

                Call TrapasarAccidente(Replace(DiasLaboralesD, ",", "."), DiasCorridos)
                'comando = New SqlCommand
                'comando.CommandType = CommandType.Text
                'comando.CommandText = "Insert Into JQRSGT..REMMov_movimiento_personal (num_comprobante, rut_personal, fec_ingreso, fec_inicio, fec_termino, dias_habiles, dias_corridos, cod_tipo_movimiento, nom_nota, cod_empresa, dias_progresivos, fec_inicio_periodo, fec_termino_periodo) " &
                '    "Values('" & txtNumComprobanteAcc.Text & "', '" & Trim(txtRutAcci.Text) & "',GetDate(),'" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "', '" & CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy") & "','" & Replace(DiasLaboralesD, ",", ".") & "', " & DiasCorridos & ", " & ComboMovmiento.SelectedValue & ", '" & "" & "', " & hdnEmpresaAccidente.Value & ",'" & "" & "','" & "" & "', '" & "" & "')"
                'comando.Connection = conx
                'conx.Open()
                'comando.ExecuteNonQuery()
                'conx.Close()

            Else
                MessageBox("Confirmar", "La Fecha de Ingreso no puede ser Mayor a la Fecha de Termino", Master, Page, "W")
            End If

        Catch ex As Exception
            MessageBoxError("ConfirmarProgramacionAccidente", Err, Page, Master)
        End Try
    End Sub

    Private Sub EnviarCorreoLicenciaAcci()
        Try

            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("" & hdnMailSupervisor.Value & "")
            correo.Subject = "Licencia Medica Accidente"
            correo.Body = "[LM] Estimado (a), se ha ingresado una nueva licencia medica por accidente de Don (a) " & Trim(txtBuscarnombreAcci.Text) & ", con fecha de inicio: " & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & " hasta " & CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy") &
             ". La informacion registra puede visualizarla en la plataforma Vitrack."
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("Supervisor no posee correo electronico valido", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaAccidenteRut(ByVal rut As String)
        Dim strNomTabla As String = "REMMov_licencias_medicas_accidentes"
        Dim strSQL As String = "Exec proc_licencias_medicas 'LAR','" & rut & "'"
        Try
            grdLicenciaAccidente.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicenciaAccidente.DataBind()
            If ContarRegistros(strSQL) = True Then
                hdnExpotar.Value = 3
            Else
                hdnExpotar.Value = 0
            End If

        Catch ex As Exception
            MessageBoxError("CargarGrillaAccidente", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaAccidenteComprobante(ByVal Comprobante As String)
        Dim strNomTabla As String = "REMMov_licencias_medicas_accidentes"
        Dim strSQL As String = "Exec proc_licencias_medicas 'LAC','" & Comprobante & "'"
        Try
            grdLicenciaAccidente.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicenciaAccidente.DataBind()
            If ContarRegistros(strSQL) = True Then
                hdnExpotar.Value = 4
            Else
                hdnExpotar.Value = 0
            End If

        Catch ex As Exception
            MessageBoxError("CargarGrillaAccidente", Err, Page, Master)
        End Try

    End Sub
    Private Sub LimpiarAccidente()
        txtRutAcci.Text = ""
        txtBuscarnombreAcci.Text = ""
        dtFecAccidente.Text = ""
        dtAlta.Text = ""
        txtNumComprobanteAcc.Text = ""
        txtFecnaIniAcc.Text = ""
        txtFechaTerAcc.Text = ""
        txtDiasLicAcci.Text = ""
        cboDiasPerdidos.ClearSelection()
        hdnEmpresaAccidente.Value = 0
        hdnActivoAcci.Value = 0
        lblEmpAcci.Text = "-"
        hdnComprobanteGrdAcci.Value = ""
        hdnRutGrdAcci.Value = ""
        hdnFiltroSelAcci.Value = 0
        Call CargarGrillaAccidenteRut("")
        hdnExpotar.Value = 0
        ChkMedioAcc.Checked = False
    End Sub

    Private Function DiasLaboraesAcci(ByVal diasC As Integer) As Integer
        Dim fecha_inicial As String = txtFecnaIniAcc.Text
        Dim fecha_final As String = txtFechaTerAcc.Text
        Dim dias As Integer = 0
        Dim DiaSemana As Integer = 0
        Dim x As Integer
        For x = 0 To (diasC - 1)
            DiaSemana = Weekday(System.DateTime.FromOADate(CDate(fecha_inicial).ToOADate + x), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
            If DiaSemana < 6 Then
                dias = dias + 1
            End If
        Next x
        Return dias
    End Function

    Private Sub actualizar_licencias_accidentes()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UA"
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = Trim(txtNumComprobanteAcc.Text)
            comando.Parameters.Add("@fec_accidente", SqlDbType.NVarChar)
            comando.Parameters("@fec_accidente").Value = CDate(dtFecAccidente.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_alta", SqlDbType.NVarChar)
            comando.Parameters("@fec_alta").Value = CDate(dtAlta.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@dias_perdidos", SqlDbType.NVarChar)
            comando.Parameters("@dias_perdidos").Value = cboDiasPerdidos.SelectedValue
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdAcci.Value
            Dim check As Integer = 0
            If ChkMedioAcc.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            comando.Parameters.Add("@act_mediodia", SqlDbType.NVarChar)
            comando.Parameters("@act_mediodia").Value = check
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("actualizar_licencias_accidentes", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarAcci_Click(sender As Object, e As EventArgs) Handles btnGuardarAcci.Click

        If txtRutAcci.Text = "" Or txtBuscarnombreAcci.Text = "" Or txtFecnaIniAcc.Text = "" Or txtFechaTerAcc.Text = "" Or txtNumComprobanteAcc.Text = "" Or dtFecAccidente.Text = "" Or dtAlta.Text = "" Or cboDiasPerdidos.SelectedValue = 9 Then
            MessageBox("Guardar", "Faltan datos por ingresar", Page, Master, "W")
        Else
            Dim check As Integer = 0
            If ChkMedioAcc.Checked = True Then
                check = 1
            Else
                check = 0
            End If
            If hdnPersonalAcci.Value = 1 Then
                If hdnIdAreaAcci.Value = 30 Or hdnIdAreaAcci.Value = 31 Then
                    Call BuscarFaenaAcci()
                    Call DevolverMailSupervisorFaenaAcci()
                Else
                    Call DevolverMailSupervisorAcci()
                End If

                If hdnMailSupervisor.Value = "" Then
                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor asignar validador", Page, Master, "W")
                Else
                    If hdnActivoAcci.Value = 0 Then
                        Call comprobar_comprobante_accidente()
                        If hdnNumExistAcci.Value = "" Then
                            Call VacacionesActivasAcci()
                            If hdnEstVacacionesAcci.Value = 1 Then
                                MessageBox("Guardar", "La fecha de Ingreso de licencia medica no es posible, ya existe un movimento entre estas fechas", Page, Master, "W")
                            Else
                                If VerificarFechasAccidente() = 0 Then
                                    Call AdjuntarDoctoAccidente()
                                    Call guardar_licencias_accidentes()
                                    Call ConfirmarProgramacionAccidente(txtNumComprobanteAcc.Text)
                                    'Call DesactivarConductorAccidente()
                                    Call EnviarCorreoLicenciaAcci()
                                    MessageBox("Guardar", "Registro Exitoso", Page, Master, "S")
                                    Call LimpiarAccidente()
                                ElseIf VerificarFechasAccidente() = 1 Then
                                    MessageBox("Guardar", "La fecha de Ingreso de licencia medica se encuentra entre un rango de Fechas de un Movimiento ya almacenada. Favor revisar los movimientos anteriores en esta misma ventana.", Page, Master, "W")
                                End If
                            End If
                        Else
                            MessageBox("Guardar", "Numero de comprobante ya existe", Page, Master, "W")
                        End If
                    Else

                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechat As DateTime = txtFechaTerAcc.Text
                        Dim fechai As DateTime = txtFecnaIniAcc.Text
                        If Month(CDate(txtFechaTerAcc.Text)) = Month(CDate(txtFecnaIniAcc.Text)) Then
                            DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                        ElseIf fechat > fechai Then
                            If Month(CDate(txtFecnaIniAcc.Text)) = 2 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 1 Or Month(CDate(txtFecnaIniAcc.Text)) = 3 Or Month(CDate(txtFecnaIniAcc.Text)) = 5 Or Month(CDate(txtFecnaIniAcc.Text)) = 7 Or Month(CDate(txtFecnaIniAcc.Text)) = 8 Or Month(CDate(txtFecnaIniAcc.Text)) = 10 Or Month(CDate(txtFecnaIniAcc.Text)) = 12 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 4 Or Month(CDate(txtFecnaIniAcc.Text)) = 6 Or Month(CDate(txtFecnaIniAcc.Text)) = 9 Or Month(CDate(txtFecnaIniAcc.Text)) = 11 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            End If
                        End If
                        DiasLaboralesD = DiasLaboraesAcci(DiasCorridos)
                        Call ConfirmarProgramacionAccidente(txtNumComprobanteAcc.Text)
                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand
                        comando = New SqlCommand
                        comando.CommandType = CommandType.Text
                        comando.CommandText = "Update JQRSGT..REMMov_movimiento_personal Set fec_inicio='" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "', fec_termino='" & CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy") & "', " &
                            "dias_habiles='" & Replace(DiasLaboralesD, ",", ".") & "', act_mediodia=" & check & ", dias_corridos='" & DiasCorridos & "'" &
                             "Where num_comprobante='" & txtNumComprobanteAcc.Text & "' and rut_personal='" & Trim(txtRutAcci.Text) & "'"
                        comando.Connection = conx
                        conx.Open()
                        comando.ExecuteNonQuery()
                        conx.Close()

                        If uplArchivoAcci.HasFile = False Then
                            Call actualizar_licencias_accidentes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarAccidente()
                        Else
                            Call EliminarDoctosAntiguos(Trim(txtNumComprobanteAcc.Text))
                            Call BorrarFicheroFTP(Trim(txtRutAcci.Text).ToString & "_" & Trim(txtNumComprobanteAcc.Text), hdnUrlActAccie.Value)
                            Call AdjuntarDoctoAccidente()
                            Call actualizar_licencias_accidentes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarAccidente()
                        End If
                    End If
                End If

            ElseIf hdnPersonalAcci.Value = 2 Then
                If hdnIdAreaAcci.Value = 30 Or hdnIdAreaAcci.Value = 31 Then
                    Call BuscarFaenaAcci()
                    Call DevolverMailSupervisorFaenaAcci()
                Else
                    Call DevolverMailSupervisorAcci()
                End If

                If hdnMailSupervisor.Value = "" Then
                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor asignar validador", Page, Master, "W")
                Else
                    If hdnActivoAcci.Value = 0 Then
                        Call comprobar_comprobante_accidente()
                        If hdnNumExistAcci.Value = "" Then
                            Call VacacionesActivasAcci()
                            If hdnEstVacacionesAcci.Value = 1 Then
                                MessageBox("Guardar", "La fecha de Ingreso de licencia medica no es posible ya que esta persona se encuetra actualmente en periodo de vacaciones.", Page, Master, "W")
                            Else
                                If VerificarFechasAccidente() = 0 Then
                                    Call AdjuntarDoctoAccidente()
                                    Call guardar_licencias_accidentes()
                                    Call ConfirmarProgramacionAccidente(txtNumComprobanteAcc.Text)
                                    'Call DesactivarConductorAccidente()
                                    Call EnviarCorreoLicenciaAcci()
                                    MessageBox("Guardar", "Registro Exitoso", Page, Master, "S")
                                    Call LimpiarAccidente()
                                ElseIf VerificarFechasAccidente() = 1 Then
                                    MessageBox("Guardar", "La fecha de Ingreso de licencia medica se encuentra entre un rango de Fechas de un Movimiento ya almacenada. Favor revisar los movimientos anteriores en esta misma ventana.", Page, Master, "W")
                                End If
                            End If
                        Else
                            MessageBox("Guardar", "Numero de comprobante ya existe", Page, Master, "W")
                        End If
                    Else

                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechat As DateTime = txtFechaTerAcc.Text
                        Dim fechai As DateTime = txtFecnaIniAcc.Text
                        If Month(CDate(txtFechaTerAcc.Text)) = Month(CDate(txtFecnaIniAcc.Text)) Then
                            DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                        ElseIf fechat > fechai Then
                            If Month(CDate(txtFecnaIniAcc.Text)) = 2 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 1 Or Month(CDate(txtFecnaIniAcc.Text)) = 3 Or Month(CDate(txtFecnaIniAcc.Text)) = 5 Or Month(CDate(txtFecnaIniAcc.Text)) = 7 Or Month(CDate(txtFecnaIniAcc.Text)) = 8 Or Month(CDate(txtFecnaIniAcc.Text)) = 10 Or Month(CDate(txtFecnaIniAcc.Text)) = 12 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            ElseIf Month(CDate(txtFecnaIniAcc.Text)) = 4 Or Month(CDate(txtFecnaIniAcc.Text)) = 6 Or Month(CDate(txtFecnaIniAcc.Text)) = 9 Or Month(CDate(txtFecnaIniAcc.Text)) = 11 Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(txtFecnaIniAcc.Text)), (CDate(txtFechaTerAcc.Text))) + 1
                            End If
                        End If
                        DiasLaboralesD = DiasLaboraesAcci(DiasCorridos)
                        Call ConfirmarProgramacionAccidente(txtNumComprobanteAcc.Text)

                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand
                        comando = New SqlCommand
                        comando.CommandType = CommandType.Text
                        comando.CommandText = "Update JQRSGT..REMMov_movimiento_personal Set fec_inicio='" & CDate(txtFecnaIniAcc.Text).ToString("dd-MM-yyyy") & "', fec_termino='" & CDate(txtFechaTerAcc.Text).ToString("dd-MM-yyyy") & "', " &
                            "dias_habiles='" & Replace(DiasLaboralesD, ",", ".") & "',  act_mediodia=" & check & ", dias_corridos='" & DiasCorridos & "'" &
                             "Where num_comprobante='" & txtNumComprobanteAcc.Text & "' and rut_personal='" & Trim(txtRutAcci.Text) & "'"
                        comando.Connection = conx
                        conx.Open()
                        comando.ExecuteNonQuery()
                        conx.Close()

                        If uplArchivoAcci.HasFile = False Then
                            Call actualizar_licencias_accidentes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarAccidente()
                        Else
                            Call EliminarDoctosAntiguos(Trim(txtNumComprobanteAcc.Text))
                            Call BorrarFicheroFTP(Trim(txtRutAcci.Text).ToString & "_" & Trim(txtNumComprobanteAcc.Text), hdnUrlActAccie.Value)
                            Call AdjuntarDoctoAccidente()
                            Call actualizar_licencias_accidentes()
                            MessageBox("Guardar", "Registro Actualizado", Page, Master, "S")
                            Call LimpiarAccidente()
                        End If
                    End If
                End If
            End If
        End If
    End Sub



    Protected Sub grdLicenciaAccidente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdLicenciaAccidente.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdLicenciaAccidente.SelectedRow
            Dim obj_hdnid As HiddenField = CType(row.FindControl("hdn_idAcci"), HiddenField)
            Dim obj_diasPerdidos As HiddenField = CType(row.FindControl("hdn_dias"), HiddenField)
            Dim obj_urlActual As HiddenField = CType(row.FindControl("hdnurlAcc"), HiddenField)

            Dim rut_obj As Label = CType(row.FindControl("lblRutAcci"), Label)
            Dim nombre_obj As Label = CType(row.FindControl("lblNombreAcci"), Label)
            Dim empresa_obj As Label = CType(row.FindControl("lblEmpresaAcci"), Label)
            Dim fechaini_obj As Label = CType(row.FindControl("lblFecInicioAcci"), Label)
            Dim fechater_obj As Label = CType(row.FindControl("lblFecTerminoAcci"), Label)
            Dim comprobante_obj As Label = CType(row.FindControl("lblNumComprobanteAcci"), Label)
            Dim fechaAccidente_obj As Label = CType(row.FindControl("lblFecAccidente"), Label)
            Dim fechaAlta_obj As Label = CType(row.FindControl("lblFecAlta"), Label)
            Dim DiasPerdidos_obj As Label = CType(row.FindControl("lblDiasLicencias"), Label)



            hdnIdAcci.Value = obj_hdnid.Value
            hdnUrlActAccie.Value = obj_urlActual.Value

            txtRutAcci.Text = Trim(rut_obj.Text)
            Call BuscarPersonalAccidente()
            txtNumComprobanteAcc.Text = comprobante_obj.Text
            Call Rescatar_ID_Mov(Trim(txtRutAcci.Text), txtNumComprobanteAcc.Text)
            txtFecnaIniAcc.Text = CDate(fechaini_obj.Text).ToString("yyyy-MM-dd")
            txtFechaTerAcc.Text = CDate(fechater_obj.Text).ToString("yyyy-MM-dd")
            dtFecAccidente.Text = CDate(fechaAccidente_obj.Text).ToString("yyyy-MM-dd")
            dtAlta.Text = CDate(fechaAlta_obj.Text).ToString("yyyy-MM-dd")
            txtDiasLicAcci.Text = DiasPerdidos_obj.Text
            Dim medio_obj As HiddenField = CType(row.FindControl("hdnMedio"), HiddenField)
            ChkMedioAcc.Checked = medio_obj.Value
            If obj_diasPerdidos.Value = True Then
                cboDiasPerdidos.SelectedValue = 1
            Else
                cboDiasPerdidos.SelectedValue = 0
            End If

            hdnActivoAcci.Value = 1

        Catch ex As Exception
            MessageBoxError("grdLicenciaAccidente_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdLicenciaAccidente_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdLicenciaAccidente.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
                e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"
            End If

            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim rdoReader As SqlDataReader
                Dim cmdTemporal As SqlCommand
                Dim strSQL As String = "Exec proc_licencias_medicas 'SD', '" & HttpUtility.HtmlDecode(CType(row.FindControl("lblNumComprobanteAcci"), Label).Text) & "'"
                Using cnxBaseDatos As New SqlConnection(strCnx)
                    cnxBaseDatos.Open()
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        Dim obj_btnDescargar As LinkButton = CType(row.FindControl("btnDescargarAcci"), LinkButton)
                        obj_btnDescargar.Visible = True
                    Else
                        Dim obj_btnDescargar As LinkButton = CType(row.FindControl("btnDescargarAcci"), LinkButton)
                        obj_btnDescargar.Visible = False
                    End If
                End Using
                rdoReader.Close()
                rdoReader = Nothing
                cmdTemporal.Dispose()
                cmdTemporal = Nothing
            End If
        Catch ex As Exception
            MessageBoxError("grdLicenciaAccidente_RowDataBound", Err, Page, Master)
        End Try

    End Sub
    Private Sub grdLicenciaAccidente_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLicenciaAccidente.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdLicenciaAccidente.Rows(intRow)
        If Trim(LCase(e.CommandName)) = "1" Then

            hdnRutGrdAcci.Value = CType(Row.FindControl("lblRutAcci"), Label).Text
            hdnComprobanteGrdAcci.Value = CType(Row.FindControl("lblNumComprobanteAcci"), Label).Text

            If CType(Row.FindControl("hdnExtensionAcc"), HiddenField).Value = ".pdf" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value

                Call DescargarFTP(archivo, CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)

                Response.ContentType = "application/pdf"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            ElseIf CType(Row.FindControl("hdnExtensionAcc"), HiddenField).Value = ".doc" Or CType(Row.FindControl("hdnExtensionAcc"), HiddenField).Value = ".docx" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value

                Call DescargarFTP(archivo, CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)

                Response.ContentType = "application/ms-word"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            ElseIf CType(Row.FindControl("hdnExtensionAcc"), HiddenField).Value = ".xlsx" Or CType(Row.FindControl("hdnExtensionAcc"), HiddenField).Value = ".xls" Then
                Dim archivo As String = "~/Temp/" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value

                Call DescargarFTP(archivo, CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)

                Response.ContentType = "application/xls"
                Response.AppendHeader("Content-Disposition", "attachment;filename=" & CType(Row.FindControl("hdnurlAcc"), HiddenField).Value)
                Response.TransmitFile(Server.MapPath(archivo))
                Response.End()
            End If
        ElseIf Trim(LCase(e.CommandName)) = "2" Then

            Call Rescatar_ID_Mov(CType(row.FindControl("lblRutAcci"), Label).Text, CType(row.FindControl("lblNumComprobanteAcci"), Label).Text)

            hdnIdAcci.Value = CType(row.FindControl("hdn_idAcci"), HiddenField).Value
            Call eliminar_movimiento_personal()
            Call eliminar_licencias_accidentes()

            Call EliminarDoctosAntiguos(Trim(txtNumComprobanteAcc.Text))
            Call BorrarFicheroFTP(Trim(txtRutAcci.Text).ToString & "_" & Trim(txtNumComprobanteAcc.Text), hdnUrlActAccie.Value)

            MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
            Call LimpiarAccidente()
        End If
    End Sub


    Private Sub eliminar_licencias_accidentes()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EA"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdAcci.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("eliminar_licencias_accidentes", Err, Page, Master)
        End Try
    End Sub
    Private Sub eliminar_movimiento_personal()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "JQRSGT.dbo.proc_licencias_medicas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EMOV"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdMovimiento.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("eliminar_movimiento_personal", Err, Page, Master)
        End Try
    End Sub

    Private Sub DescargarFTP(ByRef archivo As String, ByRef nombre As String)
        Try
            If System.IO.File.Exists(Server.MapPath(archivo)) = True Then
                System.IO.File.Delete(Server.MapPath(archivo))
            End If

            My.Computer.Network.DownloadFile("ftp://usuarioftp:Ftp2018.@192.168.10.28/Seguros/DoctosPersonal/Doctos_" & hdnRutGrdAcci.Value & "_" & hdnComprobanteGrdAcci.Value & "/" & nombre, Server.MapPath(archivo), "usuarioftp", "Ftp2018.")
        Catch ex As Exception
            MessageBoxError("DescargarFTP", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtNumComprobanteAcc_TextChanged(sender As Object, e As EventArgs) Handles txtNumComprobanteAcc.TextChanged
        Call CargarGrillaAccidenteComprobante(txtNumComprobanteAcc.Text)
    End Sub

    Protected Sub btnCancelarEtiqueta_Click(sender As Object, e As EventArgs) Handles btnCancelarEtiqueta.Click
        Call LimpiarComun()
    End Sub

    Protected Sub btnLimpiarAcci_Click(sender As Object, e As EventArgs) Handles btnLimpiarAcci.Click
        Call LimpiarAccidente()
    End Sub

    Protected Sub btnExportarAcc_Click(sender As Object, e As EventArgs) Handles btnExportarAcc.Click
        Call Exportar()
    End Sub

    Protected Sub txtDiasLicAcci_TextChanged(sender As Object, e As EventArgs) Handles txtDiasLicAcci.TextChanged
        If txtDiasLicAcci.Text <> "" Then
            Dim hoy As DateTime = txtFecnaIniAcc.Text
            Dim diaFinal As DateTime = hoy.AddDays(txtDiasLicAcci.Text - 1)
            txtFechaTerAcc.Text = CDate(diaFinal).ToString("yyyy-MM-dd")
        End If
    End Sub

    Protected Sub txtFecnaIniAcc_TextChanged(sender As Object, e As EventArgs) Handles txtFecnaIniAcc.TextChanged
        If txtDiasLicAcci.Text <> "" Then
            Dim hoy As DateTime = txtFecnaIniAcc.Text
            Dim diaFinal As DateTime = hoy.AddDays(txtDiasLicAcci.Text - 1)
            txtFechaTerAcc.Text = CDate(diaFinal).ToString("yyyy-MM-dd")
        End If
    End Sub

    Function ContarRegistros(ByVal sql As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        Dim verificacion As Integer = 0
        Dim ok As Boolean = False
        comando.CommandText = sql
        comando.Connection = conx
        conx.Open()

        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            ok = True
        Else
            ok = False
        End If

        rdoReader.Close()
        conx.Close()
        Return ok
    End Function


    Private Sub Exportar()
        Try
            If hdnExpotar.Value <> 0 Then
                Dim grdResumen As New DataGrid
                grdResumen.CellPadding = 0
                grdResumen.CellSpacing = 0
                grdResumen.BorderStyle = BorderStyle.None
                grdResumen.Font.Size = 10
                Dim strNomTabla As String = "Licencias"
                Dim strSQL As String = ""
                Dim nombre As String = ""

                If hdnExpotar.Value = 1 Then
                    strSQL = "Exec proc_licencias_medicas 'LCR','" & txtrut.Text & "'"
                    nombre = "Licencia Medica_" & txtrut.Text
                End If
                If hdnExpotar.Value = 2 Then
                    strSQL = "Exec proc_licencias_medicas 'LCR','" & txtComprobante.Text & "'"
                    nombre = "Licencia Medica_" & txtComprobante.Text
                End If
                If hdnExpotar.Value = 3 Then
                    strSQL = "Exec proc_licencias_medicas 'LAR','" & txtRutAcci.Text & "'"
                    nombre = "Licencia Laboral_" & txtRutAcci.Text
                End If
                If hdnExpotar.Value = 4 Then
                    strSQL = "Exec proc_licencias_medicas 'LAR','" & txtNumComprobanteAcc.Text & "'"
                    nombre = "Licencia Laboral_" & txtNumComprobanteAcc.Text
                End If

                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdResumen.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grdResumen.RenderControl(hw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename= " & nombre & ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default
                Response.Write(tw.ToString())
                Response.End()
            End If


        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Call Exportar()
    End Sub
End Class