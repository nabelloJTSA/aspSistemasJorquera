﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class aspLicencias
    
    '''<summary>
    '''Control btnLComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLComun As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnLAccidente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLAccidente As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lbltitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltitulo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control MtvLicencias.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents MtvLicencias As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Control VwLComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents VwLComun As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control txtrut.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtrut As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNombreLicComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNombreLicComun As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lblEmpLicComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEmpLicComun As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtComprobante.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtComprobante As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtfechai.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtfechai As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtDiasLicComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDiasLicComun As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control chkMedio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents chkMedio As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control txtfechat.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtfechat As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control dtFecEmisionLic.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dtFecEmisionLic As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control dtFecTramiLic.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dtFecTramiLic As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control dtFechaPreseLic.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dtFechaPreseLic As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtEspecialidad.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtEspecialidad As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNomMedico.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNomMedico As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control uplArchivoComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents uplArchivoComun As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''Control btnGuardarComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardarComun As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnCancelarEtiqueta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCancelarEtiqueta As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnExportar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportar As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control grdLicenciaComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdLicenciaComun As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control VwAccidente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents VwAccidente As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control txtRutAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRutAcci As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtBuscarnombreAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtBuscarnombreAcci As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control lblEmpAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lblEmpAcci As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNumComprobanteAcc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNumComprobanteAcc As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtFecnaIniAcc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFecnaIniAcc As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtDiasLicAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtDiasLicAcci As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control ChkMedioAcc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ChkMedioAcc As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''Control txtFechaTerAcc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFechaTerAcc As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control dtFecAccidente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dtFecAccidente As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control dtAlta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents dtAlta As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cboDiasPerdidos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboDiasPerdidos As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control uplArchivoAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents uplArchivoAcci As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''Control btnGuardarAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardarAcci As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnLimpiarAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnLimpiarAcci As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnExportarAcc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExportarAcc As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control grdLicenciaAccidente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdLicenciaAccidente As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnEmpresaComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEmpresaComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEmpresaAccidente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEmpresaAccidente As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnId.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnId As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnActivo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnActivo As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnActivoAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnActivoAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEstVacaciones.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEstVacaciones As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEstVacacionesAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEstVacacionesAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumExist.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumExist As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumExistAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumExistAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdAreaComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdAreaComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdAreaAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdAreaAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodFaena.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodFaena As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnMailSupervisor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnMailSupervisor As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPersonalComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPersonalComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnPersonalAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnPersonalAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdMovimiento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdMovimiento As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutGrdAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutGrdAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnComprobanteGrdAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnComprobanteGrdAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnUrlActAccie.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnUrlActAccie As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnUrlActComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnUrlActComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutGrdComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutGrdComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnComprobanteGrdComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnComprobanteGrdComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFiltroSelAcci.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFiltroSelAcci As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFiltroSelComun.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFiltroSelComun As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnExpotar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnExpotar As Global.System.Web.UI.WebControls.HiddenField
End Class
