﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspJornadaLaboral
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltituloP.Text = "Jornada Laboral"
                Call CargarEmpresa()
                Call CargarGrilla()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_departamento"
            Dim strSQLR As String = "Exec mant_RRHH_departamento 'CCS'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_jornada"
            Dim strSQL As String = "Exec mant_RRHH_jornada 'G','" & cboEmpresa.SelectedValue & "'"
            grdJornadas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdJornadas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_jornada"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_jornada", SqlDbType.NVarChar)
            comando.Parameters("@nom_jornada").Value = Trim(txtNomJornada.Text)
            comando.Parameters.Add("@des_jornada", SqlDbType.NVarChar)
            comando.Parameters("@des_jornada").Value = Trim(txtDesJornada.Text)
            'comando.Parameters.Add("@hor_semana", SqlDbType.NVarChar)
            'comando.Parameters("@hor_semana").Value = Trim(txtHrsSem.Text)
            'comando.Parameters.Add("@hor_mes", SqlDbType.NVarChar)
            'comando.Parameters("@hor_mes").Value = Trim(txtHrsMen.Text)
            'comando.Parameters.Add("@hor_dia", SqlDbType.NVarChar)
            'comando.Parameters("@hor_dia").Value = Trim(txtHrsDia.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@est_cuarto_rol", SqlDbType.Bit)
            comando.Parameters("@est_cuarto_rol").Value = chkRol.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_jornada"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_jornada", SqlDbType.NVarChar)
            comando.Parameters("@nom_jornada").Value = Trim(txtNomJornada.Text)
            comando.Parameters.Add("@des_jornada", SqlDbType.NVarChar)
            comando.Parameters("@des_jornada").Value = Trim(txtDesJornada.Text)
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            'comando.Parameters.Add("@hor_semana", SqlDbType.NVarChar)
            'comando.Parameters("@hor_semana").Value = Trim(txtHrsSem.Text)
            'comando.Parameters.Add("@hor_mes", SqlDbType.NVarChar)
            'comando.Parameters("@hor_mes").Value = Trim(txtHrsMen.Text)
            'comando.Parameters.Add("@hor_dia", SqlDbType.NVarChar)
            'comando.Parameters("@hor_dia").Value = Trim(txtHrsDia.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_jornada", SqlDbType.NVarChar)
            comando.Parameters("@id_jornada").Value = hdnId.Value
            comando.Parameters.Add("@est_cuarto_rol", SqlDbType.Bit)
            comando.Parameters("@est_cuarto_rol").Value = chkRol.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdJornadas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdJornadas.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdJornadas.SelectedRow
            Dim obj_Id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_rol As HiddenField = CType(row.FindControl("hdn4Rol"), HiddenField)

            Dim obj_Nombre As Label = CType(row.FindControl("lblNomJor"), Label)
            Dim obj_Des As Label = CType(row.FindControl("lblDes"), Label)

            hdnId.Value = obj_Id.Value
            txtNomJornada.Text = obj_Nombre.Text
            txtDesJornada.Text = obj_Des.Text

            If obj_rol.Value = True Then
                chkRol.Checked = True
            Else
                chkRol.Checked = False
            End If

            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdJornadas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtNomJornada.Text = ""
        txtDesJornada.Text = ""
        txtHrsMen.Text = ""
        txtHrsSem.Text = ""
        txtHrsDia.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdJornadas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdJornadas.PageIndexChanging
        Try
            grdJornadas.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdJornadas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla()
    End Sub
End Class