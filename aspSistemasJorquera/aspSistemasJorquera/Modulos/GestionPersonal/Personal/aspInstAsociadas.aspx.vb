﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspInstAsociadas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lblRut.Text = Session("rut_persona")
                lblNombre.Text = Session("nom_persona")
                Call CargaAfp()
                Call CargaSalud()
                Call CargaCotizacion()
                Call CargaInstituciones()
                Call CargarGrillaAsigFamiliar()
                Call CargaPensionado()
                Call BuscarDatos()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaAfp()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_institucion 'CCA'"
            cboAfp.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAfp.DataTextField = "nom_afp"
            cboAfp.DataValueField = "cod_afp"
            cboAfp.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaAfp", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaSalud()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_institucion 'CCS'"
            cboSalud.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSalud.DataTextField = "nom_salud"
            cboSalud.DataValueField = "cod_salud"
            cboSalud.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaSalud", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaCotizacion()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_institucion 'CCT'"
            chkCotizaciones.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkCotizaciones.DataTextField = "des_cotizacion"
            chkCotizaciones.DataValueField = "id_cotizacion"
            chkCotizaciones.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaCotizacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaInstituciones()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_institucion 'CCI'"
            cboInstitucion.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInstitucion.DataTextField = "des_glosa"
            cboInstitucion.DataValueField = "cod_apv"
            cboInstitucion.DataBind()

            cboInstApv.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInstApv.DataTextField = "des_glosa"
            cboInstApv.DataValueField = "cod_apv"
            cboInstApv.DataBind()

            cboInsAhorro.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboInsAhorro.DataTextField = "des_glosa"
            cboInsAhorro.DataValueField = "cod_apv"
            cboInsAhorro.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaInstituciones", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaPensionado()
        Try
            Dim strNomTablaR As String = "RRHHMae_genero"
            Dim strSQLR As String = "Exec mant_RRHH_ficha_institucion 'CCP'"
            cboPensionado.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPensionado.DataTextField = "nom_pensionado"
            cboPensionado.DataValueField = "id_pensionado"
            cboPensionado.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaPensionado", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaAsigFamiliar()
        Try
            Dim strNomTabla As String = "ADMMae_licencias_conducir"
            Dim strSQL As String = "Exec mant_RRHH_ficha_institucion 'CGT'"
            grdAsigFamiliar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdAsigFamiliar.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAsigFamiliar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdAsigFamiliar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdAsigFamiliar.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdAsigFamiliar.SelectedRow
            Dim obj_Id As HiddenField = CType(row.FindControl("hdnId"), HiddenField)
            Dim obj_NomTramo As Label = CType(row.FindControl("lblDesTramo"), Label)
            txtAsigFamiliar.Text = obj_NomTramo.Text
            hdnIdTramo.Value = obj_Id.Value

        Catch ex As Exception
            MessageBoxError("grdAsigFamiliar_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarAsigFamiliar()
        Try
            For intCheck = 0 To chkCotizaciones.Items.Count - 1
                If chkCotizaciones.Items(intCheck).Selected = True Then
                    Dim conx As New SqlConnection(strCnx)
                    Dim comando As New SqlCommand
                    comando.CommandType = CommandType.StoredProcedure
                    comando.CommandText = "mant_RRHH_ficha_institucion"
                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                    comando.Parameters("@tipo").Value = "ICP"
                    comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                    comando.Parameters("@rut_personal").Value = Trim(lblRut.Text)
                    comando.Parameters.Add("@id_cotizacion", SqlDbType.NVarChar)
                    comando.Parameters("@id_cotizacion").Value = chkCotizaciones.Items(intCheck).Value
                    comando.Connection = conx
                    conx.Open()
                    comando.ExecuteNonQuery()
                    conx.Close()
                End If
            Next
        Catch ex As Exception
            MessageBoxError("GuardarAsigFamiliar", Err, Page, Master)
        End Try
    End Sub

    Private Sub EliminarNubs()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_ficha_institucion"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "DCP"
        comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
        comando.Parameters("@rut_personal").Value = Trim(lblRut.Text)
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_institucion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(lblRut.Text)
            comando.Parameters.Add("@id_afp", SqlDbType.NVarChar)
            comando.Parameters("@id_afp").Value = cboAfp.SelectedValue
            If txtFecInc.Text = "" Then
                txtFecInc.Text = "01/01/1900"
            End If
            comando.Parameters.Add("@fec_afp", SqlDbType.NVarChar)

            comando.Parameters("@fec_afp").Value = CDate(Trim(txtFecInc.Text)).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@id_pensionado", SqlDbType.NVarChar)
            comando.Parameters("@id_pensionado").Value = cboPensionado.SelectedValue
            comando.Parameters.Add("@est_apv", SqlDbType.NVarChar)
            comando.Parameters("@est_apv").Value = cboApv.SelectedValue
            comando.Parameters.Add("@cod_apv", SqlDbType.NVarChar)
            comando.Parameters("@cod_apv").Value = cboInstApv.SelectedValue
            comando.Parameters.Add("@reg_apv", SqlDbType.NVarChar)
            comando.Parameters("@reg_apv").Value = cboTipoApv.SelectedValue
            comando.Parameters.Add("@num_monto_apv", SqlDbType.Float)
            comando.Parameters("@num_monto_apv").Value = Replace(Trim(txtMontoApv.Text), ".", ",")
            comando.Parameters.Add("@est_ahorro_afp", SqlDbType.NVarChar)
            comando.Parameters("@est_ahorro_afp").Value = cboAhorroAfp.SelectedValue
            comando.Parameters.Add("@cod_ahorro_afp", SqlDbType.NVarChar)
            comando.Parameters("@cod_ahorro_afp").Value = cboAhorroAfp.SelectedValue
            comando.Parameters.Add("@num_monto_ahorro", SqlDbType.Float)
            comando.Parameters("@num_monto_ahorro").Value = Replace(Trim(txtMontoAhorro.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_externo", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_externo").Value = cboSegExternos.SelectedValue
            comando.Parameters.Add("@id_salud", SqlDbType.NVarChar)
            comando.Parameters("@id_salud").Value = cboSalud.SelectedValue
            comando.Parameters.Add("@nro_fun", SqlDbType.Float)
            comando.Parameters("@nro_fun").Value = Replace(Trim(txtNumFun.Text), ".", ",")
            comando.Parameters.Add("@per_inicio_dscto", SqlDbType.NVarChar)
            comando.Parameters("@per_inicio_dscto").Value = Trim(txtPerDcto.Text)
            comando.Parameters.Add("@est_seguro_cesantia", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_cesantia").Value = cboSegCesantia.SelectedValue
            comando.Parameters.Add("@fec_seguro", SqlDbType.NVarChar)
            comando.Parameters("@fec_seguro").Value = Trim(CDate(txtFecIncSegCesantia.Text)).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@id_tramo", SqlDbType.NVarChar)
            comando.Parameters("@id_tramo").Value = hdnIdTramo.Value
            comando.Parameters.Add("@num_cargas", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas").Value = Trim(txtNumCargasAsigFamiliar.Text)
            comando.Parameters.Add("@por_trabajo_pesado", SqlDbType.Float)
            comando.Parameters("@por_trabajo_pesado").Value = Replace(Trim(txtTrabajoPesado.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_empresa", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_empresa").Value = cboSegVoluntario.SelectedValue
            comando.Parameters.Add("@cod_seguro_externo", SqlDbType.NVarChar)
            comando.Parameters("@cod_seguro_externo").Value = cboInstitucion.SelectedValue
            comando.Parameters.Add("@mont_seguro_externo", SqlDbType.Float)
            comando.Parameters("@mont_seguro_externo").Value = Replace(Trim(txtMontoAhorro.Text), ".", ",")
            comando.Parameters.Add("@num_monto_seguro", SqlDbType.Float)
            comando.Parameters("@num_monto_seguro").Value = Replace(Trim(txtMontoSegVoluntario.Text), ".", ",")
            comando.Parameters.Add("@num_costo_se", SqlDbType.Float)
            comando.Parameters("@num_costo_se").Value = Replace(Trim(txtCostoPlanSegVoluntario.Text), ".", ",")
            comando.Parameters.Add("@nom_beneficiarios", SqlDbType.NVarChar)
            comando.Parameters("@nom_beneficiarios").Value = Trim(txtBeneficiarios.Text)
            comando.Parameters.Add("@num_telefono", SqlDbType.NVarChar)
            comando.Parameters("@num_telefono").Value = Trim(txtFono.Text)
            comando.Parameters.Add("@est_vida_camara", SqlDbType.NVarChar)
            comando.Parameters("@est_vida_camara").Value = cboVidaCamara.SelectedValue
            comando.Parameters.Add("@num_cargas_vc", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas_vc").Value = Trim(txtNumCargasVidaCamara.Text)
            comando.Parameters.Add("@num_costo_vc", SqlDbType.Float)
            comando.Parameters("@num_costo_vc").Value = Replace(Trim(txtCostoPlanVidaCamara.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_oncologico", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_oncologico").Value = cboSegOncologico.SelectedValue
            comando.Parameters.Add("@num_cargas_so", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas_so").Value = Trim(txtNumCargasOnco.Text)
            comando.Parameters.Add("@num_costo_so", SqlDbType.Float)
            comando.Parameters("@num_costo_so").Value = Replace(Trim(txtCostoPlanOnco.Text), ".", ",")
            comando.Parameters.Add("@num_monto_salud", SqlDbType.Float)
            comando.Parameters("@num_monto_salud").Value = Replace(Trim(txtMonSalud.Text), ".", ",")
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_rem")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_ficha_institucion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(lblRut.Text)
            comando.Parameters.Add("@id_afp", SqlDbType.NVarChar)
            comando.Parameters("@id_afp").Value = cboAfp.SelectedValue
            If txtFecInc.Text = "" Then
                txtFecInc.Text = "01-01-1900"
            End If
            comando.Parameters.Add("@fec_afp", SqlDbType.NVarChar)
            comando.Parameters("@fec_afp").Value = CDate(Trim(txtFecInc.Text)).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@id_pensionado", SqlDbType.NVarChar)
            comando.Parameters("@id_pensionado").Value = cboPensionado.SelectedValue
            comando.Parameters.Add("@est_apv", SqlDbType.NVarChar)
            comando.Parameters("@est_apv").Value = cboApv.SelectedValue
            comando.Parameters.Add("@cod_apv", SqlDbType.NVarChar)
            comando.Parameters("@cod_apv").Value = cboInstApv.SelectedValue
            comando.Parameters.Add("@reg_apv", SqlDbType.NVarChar)
            comando.Parameters("@reg_apv").Value = cboTipoApv.SelectedValue
            comando.Parameters.Add("@num_monto_apv", SqlDbType.Float)
            comando.Parameters("@num_monto_apv").Value = Replace(Trim(txtMontoApv.Text), ".", ",")
            comando.Parameters.Add("@est_ahorro_afp", SqlDbType.NVarChar)
            comando.Parameters("@est_ahorro_afp").Value = cboAhorroAfp.SelectedValue
            comando.Parameters.Add("@cod_ahorro_afp", SqlDbType.NVarChar)
            comando.Parameters("@cod_ahorro_afp").Value = cboInsAhorro.SelectedValue
            comando.Parameters.Add("@num_monto_ahorro", SqlDbType.Float)
            comando.Parameters("@num_monto_ahorro").Value = Replace(Trim(txtMontoAhorro.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_externo", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_externo").Value = cboSegExternos.SelectedValue
            comando.Parameters.Add("@id_salud", SqlDbType.NVarChar)
            comando.Parameters("@id_salud").Value = cboSalud.SelectedValue
            comando.Parameters.Add("@nro_fun", SqlDbType.Float)
            comando.Parameters("@nro_fun").Value = Replace(Trim(txtNumFun.Text), ".", ",")
            comando.Parameters.Add("@per_inicio_dscto", SqlDbType.NVarChar)
            comando.Parameters("@per_inicio_dscto").Value = Trim(txtPerDcto.Text)
            comando.Parameters.Add("@est_seguro_cesantia", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_cesantia").Value = cboSegCesantia.SelectedValue
            comando.Parameters.Add("@fec_seguro", SqlDbType.NVarChar)
            comando.Parameters("@fec_seguro").Value = Trim(CDate(txtFecIncSegCesantia.Text).ToString("dd-MM-yyyy"))
            comando.Parameters.Add("@id_tramo", SqlDbType.NVarChar)
            comando.Parameters("@id_tramo").Value = hdnIdTramo.Value
            comando.Parameters.Add("@num_cargas", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas").Value = Trim(txtNumCargasAsigFamiliar.Text)
            comando.Parameters.Add("@por_trabajo_pesado", SqlDbType.Float)
            comando.Parameters("@por_trabajo_pesado").Value = Replace(Trim(txtTrabajoPesado.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_empresa", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_empresa").Value = cboSegVoluntario.SelectedValue
            comando.Parameters.Add("@cod_seguro_externo", SqlDbType.NVarChar)
            comando.Parameters("@cod_seguro_externo").Value = cboInstitucion.SelectedValue
            comando.Parameters.Add("@mont_seguro_externo", SqlDbType.Float)
            comando.Parameters("@mont_seguro_externo").Value = Replace(Trim(txtMontoAhorro.Text), ".", ",")
            comando.Parameters.Add("@num_monto_seguro", SqlDbType.Float)
            comando.Parameters("@num_monto_seguro").Value = Replace(Trim(txtMontoSegVoluntario.Text), ".", ",")
            comando.Parameters.Add("@num_costo_se", SqlDbType.Float)
            comando.Parameters("@num_costo_se").Value = Replace(Trim(txtCostoPlanSegVoluntario.Text), ".", ",")
            comando.Parameters.Add("@nom_beneficiarios", SqlDbType.NVarChar)
            comando.Parameters("@nom_beneficiarios").Value = Trim(txtBeneficiarios.Text)
            comando.Parameters.Add("@num_telefono", SqlDbType.NVarChar)
            comando.Parameters("@num_telefono").Value = Trim(txtFono.Text)
            comando.Parameters.Add("@est_vida_camara", SqlDbType.NVarChar)
            comando.Parameters("@est_vida_camara").Value = cboVidaCamara.SelectedValue
            comando.Parameters.Add("@num_cargas_vc", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas_vc").Value = Trim(txtNumCargasVidaCamara.Text)
            comando.Parameters.Add("@num_costo_vc", SqlDbType.Float)
            comando.Parameters("@num_costo_vc").Value = Replace(Trim(txtCostoPlanVidaCamara.Text), ".", ",")
            comando.Parameters.Add("@est_seguro_oncologico", SqlDbType.NVarChar)
            comando.Parameters("@est_seguro_oncologico").Value = cboSegOncologico.SelectedValue
            comando.Parameters.Add("@num_cargas_so", SqlDbType.NVarChar)
            comando.Parameters("@num_cargas_so").Value = Trim(txtNumCargasOnco.Text)
            comando.Parameters.Add("@num_costo_so", SqlDbType.Float)
            comando.Parameters("@num_costo_so").Value = Replace(Trim(txtCostoPlanOnco.Text), ".", ",")
            comando.Parameters.Add("@num_monto_salud", SqlDbType.Float)
            comando.Parameters("@num_monto_salud").Value = Replace(Trim(txtMonSalud.Text), ".", ",")
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_rem")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub


    Private Sub BuscarDatos()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec mant_RRHH_ficha_institucion 'G', '" & Trim(lblRut.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                cboAfp.SelectedValue = rdoReader(2).ToString
                txtFecInc.Text = CDate(rdoReader(4).ToString).ToString("yyyy-MM-dd")

                cboPensionado.SelectedValue = rdoReader(5).ToString

                If rdoReader(7).ToString = True Then
                    cboApv.SelectedValue = 1
                Else
                    cboApv.SelectedValue = 0
                    cboTipoApv.Enabled = False
                    txtMontoApv.Enabled = False
                    cboInstApv.Enabled = False
                End If

                cboInstApv.SelectedValue = rdoReader(8).ToString
                cboTipoApv.Text = rdoReader(10).ToString
                txtMontoApv.Text = rdoReader(11).ToString
                If rdoReader(12).ToString = True Then
                    cboAhorroAfp.SelectedValue = 1
                Else
                    cboAhorroAfp.SelectedValue = 0
                    txtMontoAhorro.Enabled = False
                    cboInsAhorro.Enabled = False
                End If
                cboInsAhorro.SelectedValue = rdoReader(13).ToString
                txtMontoAhorro.Text = rdoReader(15).ToString
                cboSalud.SelectedValue = rdoReader(17).ToString
                txtNumFun.Text = rdoReader(19).ToString
                txtPerDcto.Text = rdoReader(20).ToString
                Call RescatarCotizaciones()
                If rdoReader(21).ToString = True Then
                    cboSegCesantia.SelectedValue = 1
                Else
                    cboSegCesantia.SelectedValue = 0
                    txtFecIncSegCesantia.Enabled = False
                    'txtNumCargasAsigFamiliar.Enabled = False
                    txtTrabajoPesado.Enabled = False
                End If
                txtFecIncSegCesantia.Text = CDate(rdoReader(22).ToString).ToString("yyyy-MM-dd")
                txtNumCargasAsigFamiliar.Text = rdoReader(24).ToString
                txtTrabajoPesado.Text = rdoReader(25).ToString
                txtAsigFamiliar.Text = rdoReader(44).ToString
                If rdoReader(26).ToString = True Then
                    cboSegVoluntario.SelectedValue = 1
                Else
                    cboSegVoluntario.SelectedValue = 0
                    txtMontoSegVoluntario.Enabled = False
                    txtCostoPlanSegVoluntario.Enabled = False
                    txtBeneficiarios.Enabled = False
                    txtFono.Enabled = False
                End If
                txtMontoSegVoluntario.Text = rdoReader(30).ToString
                txtCostoPlanSegVoluntario.Text = rdoReader(31).ToString
                txtBeneficiarios.Text = rdoReader(32).ToString
                txtFono.Text = rdoReader(33).ToString
                If rdoReader(16).ToString = True Then
                    cboSegExternos.SelectedValue = 1
                Else
                    cboSegExternos.SelectedValue = 0
                    txtMontoSegExternos.Enabled = False
                    cboInstitucion.Enabled = False
                End If
                txtMontoSegExternos.Text = rdoReader(29).ToString
                cboInstitucion.SelectedValue = rdoReader(27).ToString
                If rdoReader(34).ToString = True Then
                    cboVidaCamara.SelectedValue = 1
                Else
                    cboVidaCamara.SelectedValue = 0
                    txtNumCargasVidaCamara.Enabled = False
                    txtCostoPlanVidaCamara.Enabled = False
                End If
                txtNumCargasVidaCamara.Text = rdoReader(35).ToString
                txtCostoPlanVidaCamara.Text = rdoReader(36).ToString
                If rdoReader(37).ToString = True Then
                    cboSegOncologico.SelectedValue = 1
                Else
                    cboSegOncologico.SelectedValue = 0
                    txtNumCargasOnco.Enabled = False
                    txtCostoPlanOnco.Enabled = False
                End If
                txtNumCargasOnco.Text = rdoReader(38).ToString
                txtCostoPlanOnco.Text = rdoReader(39).ToString

                txtMonSalud.Text = rdoReader(45).ToString

                hdnActivo.Value = 1
            Else
                hdnActivo.Value = 0
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarDatos", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        Try
            cboAfp.ClearSelection()
            txtFecInc.Text = ""
            cboPensionado.ClearSelection()
            cboApv.ClearSelection()
            cboInstApv.ClearSelection()
            cboTipoApv.ClearSelection()
            txtMontoApv.Text = ""
            cboAhorroAfp.ClearSelection()
            cboInsAhorro.ClearSelection()
            txtMontoAhorro.Text = ""
            cboSalud.ClearSelection()
            txtNumFun.Text = ""
            txtPerDcto.Text = ""
            chkCotizaciones.ClearSelection()
            cboSegCesantia.ClearSelection()
            txtFecIncSegCesantia.Text = ""
            txtNumCargasAsigFamiliar.Text = ""
            txtTrabajoPesado.Text = ""
            txtAsigFamiliar.Text = ""
            cboSegVoluntario.ClearSelection()
            txtMontoSegVoluntario.Text = ""
            txtCostoPlanSegVoluntario.Text = ""
            txtBeneficiarios.Text = ""
            txtFono.Text = ""
            cboSegExternos.ClearSelection()
            txtMontoSegExternos.Text = ""
            cboInstitucion.ClearSelection()
            cboVidaCamara.ClearSelection()
            txtNumCargasVidaCamara.Text = ""
            txtCostoPlanVidaCamara.Text = ""
            cboSegOncologico.ClearSelection()
            txtNumCargasOnco.Text = ""
            txtCostoPlanOnco.Text = ""
            hdnActivo.Value = 0
            hdnIdTramo.Value = 0
        Catch ex As Exception
            MessageBoxError("Limpiar", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarCotizaciones()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            For intCheck = 0 To chkCotizaciones.Items.Count - 1
                Dim cmdTemporal As SqlCommand
                Dim rdoReader As SqlDataReader
                Dim strSQL As String = "select id_cotizacion " &
                                    "From RRHHNub_ficha_cotizacion where rut_personal='" & Trim(lblRut.Text) & "' and id_cotizacion ='" &
                chkCotizaciones.Items(intCheck).Value & "'"
                Using cnxAcceso As New SqlConnection(strCnx)
                    cnxAcceso.Open()
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        If chkCotizaciones.Items(intCheck).Value = rdoReader(0).ToString Then
                            chkCotizaciones.Items(intCheck).Selected = True
                        Else
                            chkCotizaciones.Items(intCheck).Selected = False
                        End If
                    Else
                        chkCotizaciones.Items(intCheck).Selected = False
                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                    rdoReader = Nothing
                    cmdTemporal = Nothing
                End Using
            Next
        Catch ex As Exception
            MessageBoxError("RescatarItemsGrilla", Err, Page, Master)
        End Try
    End Sub

    Function ValidarGuardado() As Boolean
        Dim guardar As Boolean = True
        Dim cmdTemporal As SqlCommand, blnError As Boolean = False
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec mant_RRHH_ficha_institucion 'CFC','" & Session("rut_persona") & "'"
        Try
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0) >= 1 Then
                        guardar = False
                    Else
                        guardar = True
                    End If
                Else
                    guardar = True
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ValidarGuardado", Err, Page, Me)
        End Try
        Return guardar
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If ValidarGuardado() = True Then
            If hdnActivo.Value = 0 Then
                Call Guardar()
                Call GuardarAsigFamiliar()
                MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
                Call BuscarDatos()
            Else
                Call Actualizar()
                Call EliminarNubs()
                Call GuardarAsigFamiliar()
                MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
                Call BuscarDatos()
            End If
        Else
            MessageBox("Guardar", "No hay registros de persona", Page, Master, "W")
        End If
    End Sub

    Protected Sub cboApv_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboApv.SelectedIndexChanged
        If cboApv.SelectedValue = 0 Then
            cboTipoApv.ClearSelection()
            cboTipoApv.Enabled = False
            txtMontoApv.Text = 0
            txtMontoApv.Enabled = False
            cboInstApv.ClearSelection()
            cboInstApv.Enabled = False
        Else
            cboTipoApv.Enabled = True
            txtMontoApv.Text = ""
            txtMontoApv.Enabled = True
            cboInstApv.ClearSelection()
            cboInstApv.Enabled = True
        End If
    End Sub

    Protected Sub cboAhorroAfp_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAhorroAfp.SelectedIndexChanged
        If cboAhorroAfp.SelectedValue = 0 Then
            txtMontoAhorro.Text = 0
            txtMontoAhorro.Enabled = False
            cboInsAhorro.ClearSelection()
            cboInsAhorro.Enabled = False
        Else
            txtMontoAhorro.Text = ""
            txtMontoAhorro.Enabled = True
            cboInsAhorro.ClearSelection()
            cboInsAhorro.Enabled = True
        End If
    End Sub

    Protected Sub cboSegVoluntario_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSegVoluntario.SelectedIndexChanged
        If cboSegVoluntario.SelectedValue = 0 Then
            txtMontoSegVoluntario.Text = 0
            txtMontoSegVoluntario.Enabled = False
            txtCostoPlanSegVoluntario.Text = 0
            txtCostoPlanSegVoluntario.Enabled = False
            txtBeneficiarios.Text = 0
            txtBeneficiarios.Enabled = False
            txtFono.Text = 0
            txtFono.Enabled = False
        Else
            txtMontoSegVoluntario.Text = ""
            txtMontoSegVoluntario.Enabled = True
            txtCostoPlanSegVoluntario.Text = ""
            txtCostoPlanSegVoluntario.Enabled = True
            txtBeneficiarios.Text = ""
            txtBeneficiarios.Enabled = True
            txtFono.Text = ""
            txtFono.Enabled = True
        End If
    End Sub

    Protected Sub cboSegExternos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSegExternos.SelectedIndexChanged
        If cboSegExternos.SelectedValue = 0 Then
            txtMontoSegExternos.Text = 0
            txtMontoSegExternos.Enabled = False
            cboInstitucion.ClearSelection()
            cboInstitucion.Enabled = False
        Else
            txtMontoSegExternos.Text = ""
            txtMontoSegExternos.Enabled = True
            cboInstitucion.ClearSelection()
            cboInstitucion.Enabled = True
        End If
    End Sub

    Protected Sub cboVidaCamara_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboVidaCamara.SelectedIndexChanged
        If cboVidaCamara.SelectedValue = 0 Then
            txtNumCargasVidaCamara.Text = 0
            txtNumCargasVidaCamara.Enabled = False
            txtCostoPlanVidaCamara.Text = 0
            txtCostoPlanVidaCamara.Enabled = False
        Else
            txtNumCargasVidaCamara.Text = ""
            txtNumCargasVidaCamara.Enabled = True
            txtCostoPlanVidaCamara.Text = ""
            txtCostoPlanVidaCamara.Enabled = True
        End If
    End Sub

    Protected Sub cboSegOncologico_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSegOncologico.SelectedIndexChanged
        If cboSegOncologico.SelectedValue = 0 Then
            txtNumCargasOnco.Text = ""
            txtNumCargasOnco.Enabled = False
            txtCostoPlanOnco.Text = ""
            txtCostoPlanOnco.Enabled = False
        Else
            txtNumCargasOnco.Text = ""
            txtNumCargasOnco.Enabled = True
            txtCostoPlanOnco.Text = ""
            txtCostoPlanOnco.Enabled = True
        End If
    End Sub

    Protected Sub cboSegCesantia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSegCesantia.SelectedIndexChanged
        If cboSegCesantia.SelectedValue = 0 Then
            txtFecIncSegCesantia.Text = "01-01-1900"
            txtFecIncSegCesantia.Enabled = False
            ' txtNumCargasAsigFamiliar.Text = ""
            ' txtNumCargasAsigFamiliar.Enabled = False
            'txtTrabajoPesado.Text = ""
            'txtTrabajoPesado.Enabled = False
        Else
            txtFecIncSegCesantia.Text = ""
            txtFecIncSegCesantia.Enabled = True
            'txtNumCargasAsigFamiliar.Text = ""
            'txtNumCargasAsigFamiliar.Enabled = True
            ' txtTrabajoPesado.Text = ""
            'txtTrabajoPesado.Enabled = True
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub


End Class