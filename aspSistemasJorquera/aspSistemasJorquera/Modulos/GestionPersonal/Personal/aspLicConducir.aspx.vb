﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspLicConducir
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
                Call CargarGrilla()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "ADMMae_tipo_licencias"
            Dim strSQL As String = "Exec mant_RRHH_licencias 'G'"
            grdLicencias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLicencias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_licencias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nom_tipo ", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo ").Value = Trim(txtTipo.Text)
            comando.Parameters.Add("@nom_descripcion ", SqlDbType.NVarChar)
            comando.Parameters("@nom_descripcion ").Value = Trim(txtDescripcion.Text)
            comando.Parameters.Add("@nom_observacion ", SqlDbType.NVarChar)
            comando.Parameters("@nom_observacion ").Value = Trim(txtObs.Text)
            comando.Parameters.Add("@tip_documento ", SqlDbType.NVarChar)
            comando.Parameters("@tip_documento ").Value = cboTipoDocto.SelectedValue
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_licencias"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nom_tipo ", SqlDbType.NVarChar)
            comando.Parameters("@nom_tipo ").Value = Trim(txtTipo.Text)
            comando.Parameters.Add("@nom_descripcion ", SqlDbType.NVarChar)
            comando.Parameters("@nom_descripcion ").Value = Trim(txtDescripcion.Text)
            comando.Parameters.Add("@nom_observacion ", SqlDbType.NVarChar)
            comando.Parameters("@nom_observacion ").Value = Trim(txtObs.Text)
            comando.Parameters.Add("@tip_documento ", SqlDbType.NVarChar)
            comando.Parameters("@tip_documento ").Value = cboTipoDocto.SelectedValue
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_licencia ", SqlDbType.NVarChar)
            comando.Parameters("@id_licencia ").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdLicencias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdLicencias.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdLicencias.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim hdn_CodDoc As HiddenField = CType(row.FindControl("hdnCodDoc"), HiddenField)

            Dim obj_Tipo As Label = CType(row.FindControl("lblNomTipo"), Label)
            Dim obj_Des As Label = CType(row.FindControl("lblDes"), Label)
            Dim obj_Obs As Label = CType(row.FindControl("lblObs"), Label)

            hdnId.Value = hdnid_obj.Value
            txtTipo.Text = obj_Tipo.Text
            txtDescripcion.Text = obj_Des.Text
            txtObs.Text = obj_Obs.Text
            cboTipoDocto.SelectedValue = hdn_CodDoc.Value
            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdLicencias_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtTipo.Text = ""
        txtDescripcion.Text = ""
        txtObs.Text = ""
        cboTipoDocto.ClearSelection()
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdLicencias_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdLicencias.PageIndexChanging
        Try
            grdLicencias.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdLicencias_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub


End Class