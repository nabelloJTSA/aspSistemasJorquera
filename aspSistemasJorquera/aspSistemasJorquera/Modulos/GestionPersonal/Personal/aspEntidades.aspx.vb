﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspEntidades
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvAreas.ActiveViewIndex = 0
                lbltituloP.Text = "Mutualidades"

                Call CargarGrilla()

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_CCAF"
            Dim strSQL As String = "Exec mant_RRHH_mutualidad 'G' , ''"
            grdMutualidades.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdMutualidades.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_mutualidad"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_mutualidad", SqlDbType.NVarChar)
            comando.Parameters("@cod_mutualidad").Value = Trim(txtCodigo.Text)
            comando.Parameters.Add("@des_glosa", SqlDbType.NVarChar)
            comando.Parameters("@des_glosa").Value = Trim(txtNombre.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_mutualidad"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@cod_mutualidad", SqlDbType.NVarChar)
            comando.Parameters("@cod_mutualidad").Value = Trim(txtCodigo.Text)
            comando.Parameters.Add("@des_glosa", SqlDbType.NVarChar)
            comando.Parameters("@des_glosa").Value = Trim(txtNombre.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_mutualidad ", SqlDbType.NVarChar)
            comando.Parameters("@id_mutualidad ").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdMutualidades_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdMutualidades.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdMutualidades.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

            Dim obj_Codigo As Label = CType(row.FindControl("lblCodigo"), Label)
            Dim obj_Nombre As Label = CType(row.FindControl("lblNombre"), Label)

            hdnId.Value = hdnid_obj.Value
            txtCodigo.Text = obj_Codigo.Text
            txtNombre.Text = obj_Nombre.Text
            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdCCAF_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtCodigo.Text = ""
        txtNombre.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdMutualidades_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdMutualidades.PageIndexChanging
        Try
            grdMutualidades.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdMutualidades_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub


    '**********************
    Private Sub CargarGrillaCCAF()
        Try
            Dim strNomTabla As String = "RRHHMae_CCAF"
            Dim strSQL As String = "Exec mant_RRHH_CCAF 'G' , ''"
            grdCCAF.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCCAF.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarCCAF()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_CCAF"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_ccaf", SqlDbType.NVarChar)
            comando.Parameters("@cod_ccaf").Value = Trim(txtCodigoCCAF.Text)
            comando.Parameters.Add("@des_glosa", SqlDbType.NVarChar)
            comando.Parameters("@des_glosa").Value = Trim(txtNombreCCAF.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrillaCCAF()
            Call LimpiarCCAF()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarCCAF()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_CCAF"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@cod_ccaf", SqlDbType.NVarChar)
            comando.Parameters("@cod_ccaf").Value = Trim(txtCodigoCCAF.Text)
            comando.Parameters.Add("@des_glosa", SqlDbType.NVarChar)
            comando.Parameters("@des_glosa").Value = Trim(txtNombreCCAF.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_ccaf", SqlDbType.NVarChar)
            comando.Parameters("@id_ccaf").Value = hdnIdCCAF.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrillaCCAF()
            Call LimpiarCCAF()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdCCAF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCCAF.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdCCAF.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_Codigo As Label = CType(row.FindControl("lblCodigo"), Label)
            Dim obj_Nombre As Label = CType(row.FindControl("lblNombre"), Label)
            hdnIdCCAF.Value = hdnid_obj.Value
            txtCodigoCCAF.Text = obj_Codigo.Text
            txtNombreCCAF.Text = obj_Nombre.Text
            hdnActivoCCAF.Value = 1
        Catch ex As Exception
            MessageBoxError("grdCCAF_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub LimpiarCCAF()
        txtCodigoCCAF.Text = ""
        txtNombreCCAF.Text = ""
        hdnActivoCCAF.Value = 0
        Call CargarGrillaCCAF()
    End Sub

    Private Sub grdCCAF_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdCCAF.PageIndexChanging
        Try
            grdCCAF.PageIndex = e.NewPageIndex
            Call CargarGrillaCCAF()
        Catch ex As Exception
            MessageBoxError("grdCCAF_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarCCAF_Click(sender As Object, e As EventArgs) Handles btnGuardarCCAF.Click
        If hdnActivoCCAF.Value = 0 Then
            Call GuardarCCAF()
        Else
            Call ActualizarCCAF()
        End If
    End Sub

    Protected Sub btnCancelarCCAF_Click(sender As Object, e As EventArgs) Handles btnCancelarCCAF.Click
        Call LimpiarCCAF()
    End Sub

    Protected Sub btnMutualidad_Click(sender As Object, e As EventArgs) Handles btnMutualidad.Click
        MtvAreas.ActiveViewIndex = 0
        lbltituloP.Text = "Mutualidades"
    End Sub

    Protected Sub btnCcaf_Click(sender As Object, e As EventArgs) Handles btnCcaf.Click
        MtvAreas.ActiveViewIndex = 1
        lbltituloP.Text = "CCAF"
        Call CargarGrillaCCAF()
    End Sub
End Class
