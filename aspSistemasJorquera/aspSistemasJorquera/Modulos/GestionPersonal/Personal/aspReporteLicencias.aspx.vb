﻿
Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspReporteLicencias
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Reportes Licencias"
                Call carga_combo_empresa_comun()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_combo_empresa_comun()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> (5) order by cod_empresa"
            cboFiltroEmpComun.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFiltroEmpComun.DataTextField = "nom_empresa"
            cboFiltroEmpComun.DataValueField = "cod_empresa"
            cboFiltroEmpComun.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa_comun", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarKP1()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim tipoSeguro As String = "%"
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If

        Dim sqlString As String = "exec rpt_PER_licencias 'KP1','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        comando.CommandTimeout = 180000
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            lblK1.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub



    Private Sub CargarDetalleKP1()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_licencias 'DKP1','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=LicenciasPrologadas.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub



    Private Sub CargarKP2()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim sqlString As String = "exec rpt_PER_licencias 'KP2','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        comando.CommandTimeout = 180000
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            lblK2.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub

    Private Sub CargarDetalleKP2()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_licencias 'DKP2','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=TotalLicenciasMedicas.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub

    Private Sub CargarKP3()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim sqlString As String = "exec rpt_PER_licencias 'KP3','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        comando.CommandTimeout = 180000
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            lblK3.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub

    Private Sub CargarDetalleKP3()

        Dim grdcargar As New DataGrid
        grdcargar.ShowHeader = True
        grdcargar.CellPadding = 0
        grdcargar.CellSpacing = 0
        grdcargar.BorderStyle = BorderStyle.Double
        grdcargar.Font.Size = 11
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim strSQL As String = "exec rpt_PER_licencias 'DKP3','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        grdcargar.DataBind()


        Me.EnableViewState = False
        Dim tw As New System.IO.StringWriter
        Dim hw As New System.Web.UI.HtmlTextWriter(tw)
        grdcargar.RenderControl(hw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"

        Response.AddHeader("Content-Disposition", "attachment;filename=Personal_Total.xls")

        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(tw.ToString())
        Response.End()
    End Sub

    Private Sub CargarKP4()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim leer As SqlDataReader
        Dim empresa As String = "%"
        If cboFiltroEmpComun.SelectedValue <> 0 Then
            empresa = cboFiltroEmpComun.SelectedValue
        End If
        Dim fechadesde As String = ""
        Dim fechahasta As String = ""
        If dtDesdeComun.Text = "" Then
            fechadesde = dtDesdeComun.Text
        Else
            fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
        End If

        If dtHastaComun.Text = "" Then
            fechahasta = dtHastaComun.Text
        Else
            fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
        End If
        Dim strNomTabla As String = "PERMov_personal_vida_camara"
        Dim sqlString As String = "exec rpt_PER_licencias 'KP4','" & txtBuscarComun.Text & "','" & fechadesde & "','" & fechahasta & "','" & empresa & "','" & txtrut.Text & "' "
        comando.CommandTimeout = 180000
        comando.CommandText = sqlString
        comando.Connection = conx
        conx.Open()

        leer = comando.ExecuteReader

        If leer.Read Then
            lblK4.Text = leer(0)
        End If
        leer.Close()
        conx.Close()
    End Sub

    Protected Sub btnExportarComun_Click(sender As Object, e As EventArgs) Handles btnExportarComun.Click
        Try
            Dim fechadesde As String = ""
            Dim fechahasta As String = ""
            If dtDesdeComun.Text = "" Then
                fechadesde = dtDesdeComun.Text
            Else
                fechadesde = CDate(dtDesdeComun.Text).ToString("dd/MM/yyy")
            End If

            If dtHastaComun.Text = "" Then
                fechahasta = dtHastaComun.Text
            Else
                fechahasta = CDate(dtHastaComun.Text).ToString("dd/MM/yyy")
            End If
            If cboTipo.SelectedValue = 1 Then
                Dim grdcargar As New DataGrid
                grdcargar.ShowHeader = True
                grdcargar.CellPadding = 0
                grdcargar.CellSpacing = 0
                grdcargar.BorderStyle = BorderStyle.Groove
                grdcargar.Font.Size = 11

                If cboFiltroEmpComun.SelectedValue = 0 Then
                    If dtHastaComun.Text = "" Then
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_EX', '', '', '', '', '', '" & txtrut.Text & "' "
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_EX', '" & txtBuscarComun.Text & "', '', '', '', '', '" & txtrut.Text & "' "
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_F_EX', '', '" & fechadesde & "', '" & fechahasta & "','', '', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_F_EX', '" & txtBuscarComun.Text & "','" & fechadesde & "', '" & fechahasta & "','', '', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If
                Else
                    If dtHastaComun.Text = "" Then
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_E_EX', '', '" & cboFiltroEmpComun.SelectedValue & "','', '', '', '" & txtrut.Text & "' "
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_E_EX','" & txtBuscarComun.Text & "','" & cboFiltroEmpComun.SelectedValue & "','', '', '', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_F_E_EX', '', '" & fechadesde & "', '" & fechahasta & "', '" & cboFiltroEmpComun.SelectedValue & "','','" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec proc_licencias_medicas 'GLC_F_E_EX', '" & txtBuscarComun.Text & "', '" & fechadesde & "', '" & fechahasta & "', '" & cboFiltroEmpComun.SelectedValue & "', '', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If
                End If


                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grdcargar.RenderControl(hw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=Licencias_Medicas.xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()
            Else
                Dim grdcargar As New DataGrid
                grdcargar.ShowHeader = True
                grdcargar.CellPadding = 0
                grdcargar.CellSpacing = 0
                grdcargar.BorderStyle = BorderStyle.Groove
                grdcargar.Font.Size = 11

                If cboFiltroEmpComun.SelectedValue = 0 Then
                    If dtHastaComun.Text = "" Then
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_EX', '' ,'', '' ,'', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_EX', '" & txtBuscarComun.Text & "' ,'', '' ,'', '" & txtrut.Text & "' "
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_F_EX', '', '" & fechadesde & "', '" & fechahasta & "','', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_F_EX', '" & txtBuscarComun.Text & "','" & fechadesde & "', '" & fechahasta & "','', '" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If
                Else
                    If dtHastaComun.Text = "" Then
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_E_EX', '', '','','" & cboFiltroEmpComun.SelectedValue & "','" & txtrut.Text & "' "
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_E_EX','" & txtBuscarComun.Text & "','','','" & cboFiltroEmpComun.SelectedValue & "','" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    Else
                        If txtBuscarComun.Text = "" Then
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_F_E_EX', '','" & fechadesde & "', '" & fechahasta & "', '" & cboFiltroEmpComun.SelectedValue & "','" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        Else
                            Dim strNomTabla As String = "REMMov_licencias_medicas_comunes"
                            Dim strSQL As String = "Exec rpt_PER_licencias 'GLC_F_E_EX', '" & txtBuscarComun.Text & "','" & fechadesde & "', '" & fechahasta & "','" & cboFiltroEmpComun.SelectedValue & "','" & txtrut.Text & "'"
                            grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                            grdcargar.DataBind()
                        End If
                    End If
                End If


                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grdcargar.RenderControl(hw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=Licencias_Laborales.xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()
            End If

        Catch ex As Exception
            MessageBoxError("btnExportarComun_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtBuscarComun_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarComun.TextChanged
        Call CargarKP1()
        Call CargarKP2()
        Call CargarKP3()
        Call CargarKP4()
    End Sub

    Protected Sub cboFiltroEmpComun_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFiltroEmpComun.SelectedIndexChanged
        Call CargarKP1()
        Call CargarKP2()
        Call CargarKP3()
        Call CargarKP4()
    End Sub

    Protected Sub txtrut_TextChanged(sender As Object, e As EventArgs) Handles txtrut.TextChanged
        txtrut.Text = Replace(txtrut.Text, ".", "")
        txtrut.Text = Replace(txtrut.Text, "-", "")
        If Len(txtrut.Text) = 8 Then
            txtrut.Text = Left(txtrut.Text, 7) + "-" + Right(txtrut.Text, 1)
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = Left(txtrut.Text, 8) + "-" + Right(txtrut.Text, 1)
        End If

        If Len(txtrut.Text) = 10 Then
            txtrut.Text = txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call CargarKP1()
                Call CargarKP2()
                Call CargarKP3()
                Call CargarKP4()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = "0" + txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call CargarKP1()
                Call CargarKP2()
                Call CargarKP3()
                Call CargarKP4()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        End If

        If txtrut.Text = "" Then
            Call CargarKP1()
            Call CargarKP2()
            Call CargarKP3()
            Call CargarKP4()
        End If
    End Sub

    Protected Sub btnKp1_Click(sender As Object, e As EventArgs) Handles btnKp1.Click
        Call CargarDetalleKP1()
    End Sub


    Protected Sub btnKp2_Click(sender As Object, e As EventArgs) Handles btnKp2.Click
        Call CargarDetalleKP2()
    End Sub
    Protected Sub btnKp3_Click(sender As Object, e As EventArgs) Handles btnKp3.Click
        Call CargarDetalleKP3()
    End Sub

    Protected Sub dtDesdeComun_TextChanged(sender As Object, e As EventArgs) Handles dtDesdeComun.TextChanged
        If dtHastaComun.Text <> "" And dtDesdeComun.Text <> "" Then
            Call CargarKP1()
            Call CargarKP2()
            Call CargarKP3()
            Call CargarKP4()
        End If

    End Sub

    Protected Sub dtHastaComun_TextChanged(sender As Object, e As EventArgs) Handles dtHastaComun.TextChanged
        If dtHastaComun.Text <> "" And dtDesdeComun.Text <> "" Then
            Call CargarKP1()
            Call CargarKP2()
            Call CargarKP3()
            Call CargarKP4()
        End If
    End Sub
End Class