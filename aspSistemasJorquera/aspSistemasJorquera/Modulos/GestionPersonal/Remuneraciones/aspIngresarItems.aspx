﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspIngresarItems.aspx.vb" Inherits="aspSistemasJorquera.aspIngresarItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Ingresar Ítems"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">

        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Concepto</label>
                <div>
                    <asp:TextBox ID="txtConcepto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
          
        </div>

        <div class="row justify-content-center table-responsive">

            <div class="col-md-4 form-group ">
                <div>
                </div>
            </div>


            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-1 form-group ">
                <div>
                    <asp:TextBox ID="txtbNombre" runat="server" placeHolder="Buscar área..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-5">

                <asp:GridView ID="grdItemPay" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                    <Columns>
                        <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                            <ItemStyle HorizontalAlign="Center" Width="20px" />
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="Nombre Item">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_item_variables")%>' />
                                <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_item_variables")%>'></asp:HiddenField>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="250px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Item Payroll">
                            <ItemTemplate>
                                <asp:Label ID="lblNombrePay" runat="server" Text='<%# Bind("nom_item_payroll")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Formula">
                            <ItemTemplate>
                                <asp:Label ID="lblFormula" runat="server" Text='<%# Bind("nom_formula_payroll")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="60px" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeleteComun" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>

            </div>

        </div>
    </section>
    <asp:HiddenField ID="hdnIdItem" runat="server" />
    <asp:HiddenField ID="hdnActivo" runat="server" Value="0" />
</asp:Content>
