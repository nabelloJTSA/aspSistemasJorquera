﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspControlPeriodo
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarEmpresas()
                Call CargarAnio()
                Call CargarPeriodo()
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresas()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "REMMae_estado_civil"
            Dim strSQL As String = "Exec mant_RRHH_periodo 'G'," & cboEmpresa.SelectedValue & ""
            grdPeriodos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdPeriodos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_periodo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@est_periodo", SqlDbType.NVarChar)
            comando.Parameters("@est_periodo").Value = chkActivo.Checked
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_periodo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@est_periodo", SqlDbType.NVarChar)
            comando.Parameters("@est_periodo").Value = chkActivo.Checked
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@id_periodo", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdEstado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdPeriodos.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdPeriodos.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_Nombre As Label = CType(row.FindControl("lblNomEstado"), Label)
            hdnId.Value = hdnid_obj.Value
            Call CargarEmpresas()
            cboEmpresa.SelectedValue = CType(row.FindControl("hdn_emp"), HiddenField).Value
            Call CargarAnio()
            cboanio.SelectedValue = CType(row.FindControl("hdn_anio"), HiddenField).Value
            Call CargarPeriodo()
            cboPeriodo.SelectedValue = CType(row.FindControl("lblPeriodo"), Label).Text
            chkActivo.Checked = CType(row.FindControl("hdn_estado"), HiddenField).Value
            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdEstado_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        cboEmpresa.ClearSelection()
        chkActivo.Checked = False
        cboanio.ClearSelection()
        cboPeriodo.ClearSelection()
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub grdEstado_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdPeriodos.PageIndexChanging
        Try
            grdPeriodos.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdEstado_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnActivo.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla()
    End Sub
End Class