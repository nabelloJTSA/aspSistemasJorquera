﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspPlantillaEspecial
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
    End Sub

    Private Sub DatosPersona()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'CPE', '" & txtRut.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(1).ToString
                    hdnCodEmp.Value = rdoReader(2).ToString
                    lblEmpresa.Text = rdoReader(3).ToString
                    lblSucursal.Text = rdoReader(7).ToString
                    hdnCodCargo.Value = rdoReader(4).ToString
                    lblInstrumento.Text = rdoReader(8).ToString
                    hdnCodInstrumento.Value = rdoReader(9).ToString
                    lblCargo.Text = rdoReader(5).ToString
                    hdnCodSucursal.Value = rdoReader(6).ToString
                    hdnSueldoBase.Value = rdoReader(10).ToString

                    lblSueldoBase.Text = rdoReader(10).ToString
                    lblAncipo.Text = rdoReader(11).ToString
                    chkActiva.Checked = rdoReader(12)
                    chkActiva.Visible = True
                      lblAct.Visible =True
                    If chkActiva.Checked = False Then
                        grdFijos.Enabled = False
                        grdVariables.Enabled = False
                    Else
                        grdFijos.Enabled = True
                        grdVariables.Enabled = True
                    End If
                    lblCar.Visible = True
                    lblNom.Visible = True
                    lblSuc.Visible = True
                    lblEmp.Visible = True
                    lblIC.Visible = True
                    lblSB.Visible = True
                    lblMA.Visible = True

                    Call VerificarPlantilla()

                    If hdnTipoPlantilla.Value = 0 Then
                        Call RescatarPlantilla()
                    Else
                        Call RescatarPlantillaEspecial()
                    End If


                    Call CargarGrillaFijos()
                    Call CargarGrillaVariables()

                    pnlDet.Visible = True

                Else
                    lblNombre.Text = ""
                    lblEmpresa.Text = ""
                    lblSucursal.Text = ""
                    lblCargo.Text = ""
                    lblInstrumento.Text = ""
                    lblSueldoBase.Text = ""
                    lblAncipo.Text = ""
                    lblIC.Visible = False
                    lblCar.Visible = False
                    lblNom.Visible = False
                    lblSuc.Visible = False
                    lblEmp.Visible = False
                    pnlDet.Visible = False
                    lblSB.Visible = False
                    chkActiva.Visible = False
                    lblAct.Visible = False
                    lblMA.Visible = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("DatosPersona", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub Limpiar()
        lblNombre.Text = ""
        lblEmpresa.Text = ""
        lblSucursal.Text = ""
        lblCargo.Text = ""
        lblCar.Visible = False
        lblNom.Visible = False
        lblSuc.Visible = False
        lblEmp.Visible = False
        pnlDet.Visible = False
    End Sub


    Private Sub VerificarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'VTP' , '" & Trim(txtRut.Text) & "', '" & hdnCodEmp.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnTipoPlantilla.Value = 1
                Else
                    hdnTipoPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarPlantillaEspecial()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'RPE' , '" & Trim(txtRut.Text) & "', '" & hdnCodEmp.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdPlantilla.Value = rdoReader(0).ToString
                Else
                    hdnIdPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'RDP' , '" & hdnCodEmp.Value & "', '" & hdnCodCargo.Value & "', '" & hdnCodSucursal.Value & "', '" & hdnCodInstrumento.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdPlantilla.Value = rdoReader(0).ToString
                Else
                    hdnIdPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub CargarGrillaFijos()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'GDF' , '" & hdnCodEmp.Value & "', '" & txtRut.Text & "'"
            grdFijos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdFijos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFijos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaVariables()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'GDV' , '" & hdnCodEmp.Value & "', '" & txtRut.Text & "'"
            grdVariables.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdVariables.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaVariables", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarDetalle(ByVal id_det_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'RDI' , '" & id_det_item & "', '" & hdnIdPlantilla.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnExisteDet.Value = 1
                    hdnEstValor.Value = rdoReader(7).ToString
                    hdnNumValor.Value = rdoReader(8).ToString
                    hdnEstFormula.Value = rdoReader(9).ToString
                    hdnEstSistema.Value = rdoReader(10).ToString
                    hdnEstIncluir.Value = rdoReader(11).ToString
                Else
                    hdnExisteDet.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarDetalle", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescatarDetalleEspeciales(ByVal id_det_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'RDE' , '" & id_det_item & "', '" & hdnIdPlantilla.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnExisteDet.Value = 1
                    hdnEstIncluir.Value = rdoReader(7).ToString
                Else
                    hdnExisteDet.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarDetalleEspeciales", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub grdFijos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdFijos.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
            Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

            If hdnIdPlantilla.Value = 0 Then
                obj_chkIncluir.Checked = False
            Else
                If hdnTipoPlantilla.Value = 0 Then
                    Call RescatarDetalle(obj_IdItem.Value)
                Else
                    Call RescatarDetalleEspeciales(obj_IdItem.Value)
                End If

                If hdnExisteDet.Value = 1 Then
                    If hdnEstIncluir.Value = True Then
                        obj_chkIncluir.Checked = True
                    Else
                        obj_chkIncluir.Checked = False
                    End If
                Else
                    obj_chkIncluir.Checked = False
                End If
            End If
        End If
    End Sub

    Private Sub grdVariables_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdVariables.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
            Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

            If hdnIdPlantilla.Value = 0 Then
                obj_chkIncluir.Checked = False
            Else
                If hdnTipoPlantilla.Value = 0 Then
                    Call RescatarDetalle(obj_IdItem.Value)
                Else
                    Call RescatarDetalleEspeciales(obj_IdItem.Value)
                End If

                If hdnExisteDet.Value = 1 Then

                    If hdnEstIncluir.Value = True Then
                        obj_chkIncluir.Checked = True
                    Else
                        obj_chkIncluir.Checked = False
                    End If
                Else
                    obj_chkIncluir.Checked = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call DatosPersona()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call DatosPersona()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        End If
    End Sub


    Private Sub GuardarDetPlantilla(ByVal id_plantilla As String, ByVal id_det_item As String, ByVal est_incluir As String, ByVal num_valor As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla_especial"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ID"
            comando.Parameters.Add("@id_plantilla_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla_esp").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Parameters.Add("@est_incluir", SqlDbType.Bit)
            comando.Parameters("@est_incluir").Value = est_incluir
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            If num_valor.Contains(".") Then
                num_valor = Replace(num_valor, ".", "")
            End If
            If num_valor.Contains(",") Then
                num_valor = Replace(num_valor, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = num_valor
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarDetPlantilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarDetPlantilla(ByVal id_plantilla As String, ByVal id_det_item As String, ByVal est_incluir As String, ByVal num_valor As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla_especial"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UD"
            comando.Parameters.Add("@id_plantilla_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla_esp").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Parameters.Add("@est_incluir", SqlDbType.Bit)
            comando.Parameters("@est_incluir").Value = est_incluir
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            If num_valor.Contains(".") Then
                num_valor = Replace(num_valor, ".", "")
            End If
            If num_valor.Contains(",") Then
                num_valor = Replace(num_valor, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = num_valor


            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDetPlantilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub EliminarDet(ByVal id_plantilla As String, ByVal id_det_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla_especial"
            comando.CommandTimeout = 900000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ED"
            comando.Parameters.Add("@id_plantilla_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla_esp").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarDet", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla_especial"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = hdnCodEmp.Value
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@est_activo", SqlDbType.NVarChar)
            comando.Parameters("@est_activo").Value = chkActiva.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarPlantilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub ActualizarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla_especial"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@est_activo", SqlDbType.NVarChar)
            comando.Parameters("@est_activo").Value = chkActiva.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarPlantilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub UltimoIdPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'MID', '" & Trim(txtRut.Text) & "', '" & hdnCodEmp.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdPlantilla.Value = rdoReader(0).ToString
                Else
                    hdnIdPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("UltimoIdPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub


    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            Call VerificarPlantilla()
            Call UltimoIdPlantilla()

            If hdnIdPlantilla.Value <> 0 Then
                For Each row As GridViewRow In grdFijos.Rows
                    Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                    Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                    Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                    If obj_chkIncluir.Checked = True Then

                        Call RescatarDetalleEspeciales(obj_IdItem.Value)

                        If hdnExisteDet.Value = 0 Then
                            Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        Else
                            Call ActualizarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        End If
                    Else
                        Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                    End If
                Next

                For Each row As GridViewRow In grdVariables.Rows
                    Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                    Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                    Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                    If obj_chkIncluir.Checked = True Then

                        Call RescatarDetalleEspeciales(obj_IdItem.Value)

                        If hdnExisteDet.Value = 0 Then
                            Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        Else
                            Call ActualizarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        End If
                    Else
                        Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                    End If
                Next

            Else
                Call GuardarPlantilla()
                Call UltimoIdPlantilla()

                For Each row As GridViewRow In grdFijos.Rows
                    Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                    Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                    Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                    If obj_chkIncluir.Checked = True Then

                        Call RescatarDetalleEspeciales(obj_IdItem.Value)

                        If hdnExisteDet.Value = 0 Then
                            Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        End If
                    Else
                        Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                    End If
                Next

                For Each row As GridViewRow In grdVariables.Rows
                    Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                    Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                    Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                    If obj_chkIncluir.Checked = True Then
                        Call RescatarDetalleEspeciales(obj_IdItem.Value)

                        If hdnExisteDet.Value = 0 Then
                            Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked, obj_numvalor.Text)
                        End If
                    Else
                        Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                    End If
                Next
            End If

            Call VerificarPlantilla()
            If hdnTipoPlantilla.Value = 0 Then
                Call RescatarPlantilla()
            Else
                Call RescatarPlantillaEspecial()
            End If
            Call CargarGrillaFijos()
            Call CargarGrillaVariables()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")

        Catch ex As Exception
            MessageBoxError("btnGuardar_Click", Err, Page, Master)
        End Try
    End Sub

    'Protected Sub SeparadorMiles(sender As Object, e As EventArgs)
    '    Dim txt As TextBox = CType(sender, TextBox)
    '    Dim row As GridViewRow = CType(txt.NamingContainer, GridViewRow)
    '    If CType(row.FindControl("txtMonto"), TextBox).Text = "" Then
    '        CType(row.FindControl("txtMonto"), TextBox).Text = ""
    '    Else
    '        CType(row.FindControl("txtMonto"), TextBox).Text = Format(CDec(CType(row.FindControl("txtMonto"), TextBox).Text), "#,##")
    '    End If

    'End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then

                btnBuscar.Enabled = True
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnBuscar.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then

                btnBuscar.Enabled = True
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnBuscar.Enabled = False
            End If
        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        chkActiva.Visible = False
        lblAct.Visible = False
        txtRut.Text = ""
        Call DatosPersona()
    End Sub

    Protected Sub chkActiva_CheckedChanged(sender As Object, e As EventArgs) Handles chkActiva.CheckedChanged
        Call ActualizarPlantilla()
        If chkActiva.Checked = False Then
            grdFijos.Enabled = False
            grdVariables.Enabled = False
        Else
            grdFijos.Enabled = True
            grdVariables.Enabled = True
        End If
    End Sub

    Protected Sub grdFijos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdFijos.SelectedIndexChanged

    End Sub
End Class

