﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspPlantilla
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarEmpresa()
                Call CargarCargo()
                Call CargarSucursal()
                Call CargarDepto()
                Call CargarInstrumentos()
                Call CargarGrillaFijos()
                Call CargarGrillaVariables()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSucursal()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCS', '" & cboEmpresa.SelectedValue & "'"
            cboSucursal.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSucursal.DataTextField = "nom_sucursal"
            cboSucursal.DataValueField = "id_sucursal"
            cboSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSucursal", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarCargo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCC', '" & cboEmpresa.SelectedValue & "'"
            cboCargo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCargo.DataTextField = "nom_cargo"
            cboCargo.DataValueField = "cod_cargo"
            cboCargo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCargo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepto()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCD', '" & cboEmpresa.SelectedValue & "'"
            cboDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboDepto.DataTextField = "nom_departamento"
            cboDepto.DataValueField = "cod_departamento"
            cboDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarCargo()
        Call CargarSucursal()
        Call CargarInstrumentos()
        If cboEmpresa.SelectedValue = 1 Then
            Call CargarDepto()
            lblDepto.Visible = True
            cboDepto.Visible = True
        Else
            lblDepto.Visible = False
            cboDepto.Visible = False
        End If
        Call RescatarPlantilla()
        Call CargarGrillaFijos()
        Call CargarGrillaVariables()


    End Sub

    Protected Sub cboCargo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCargo.SelectedIndexChanged
        If cboEmpresa.SelectedValue = 1 Then
            lblDepto.Visible = True
            cboDepto.Visible = True
        Else
            lblDepto.Visible = False
            cboDepto.Visible = False
        End If
        Call RescatarPlantilla()
        Call CargarGrillaFijos()
        Call CargarGrillaVariables()


    End Sub

    Protected Sub cboSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSucursal.SelectedIndexChanged
        'If cboEmpresa.SelectedValue = 1 Then
        '    lblDepto.Visible = True
        '    cboDepto.Visible = True
        'Else

        '    lblDepto.Visible = False
        '    cboDepto.Visible = False
        'End If
        Call RescatarPlantilla()
        'Call CargarGrillaFijos()
        'Call CargarGrillaVariables()



    End Sub

    Protected Sub cboIC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboIC.SelectedIndexChanged
        If cboEmpresa.SelectedValue = 1 Then
            lblDepto.Visible = True
            cboDepto.Visible = True
        Else
            lblDepto.Visible = False
            cboDepto.Visible = False
        End If
        Call RescatarPlantilla()
        Call CargarGrillaFijos()
        Call CargarGrillaVariables()

    End Sub


    Private Sub CargarGrillaFijos()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "Exec mant_RRHHMae_plantilla 'GDF' , '" & cboEmpresa.SelectedValue & "'"
            grdFijos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdFijos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFijos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarInstrumentos()
        Try
            Dim strNomTablaR As String = "RRHHMae_sucursal"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CCI', '" & cboEmpresa.SelectedValue & "'"
            cboIC.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboIC.DataTextField = "nom_instrumento"
            cboIC.DataValueField = "cod_instrumento"
            cboIC.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarInstrumentos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaVariables()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "Exec mant_RRHHMae_plantilla 'GDV' , '" & cboEmpresa.SelectedValue & "'"
            grdVariables.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdVariables.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaVariables", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim tipo As String
        If cboEmpresa.SelectedValue = 1 Then
            tipo = "RPJ"
        Else
            tipo = "RDP"
        End If
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla '" & tipo & "' , '" & cboEmpresa.SelectedValue & "', '" & cboCargo.SelectedValue & "', '" & cboSucursal.SelectedValue & "', '" & cboIC.SelectedValue & "', '" & cboDepto.SelectedValue & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdPlantilla.Value = rdoReader(0).ToString
                Else
                    hdnIdPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub


    Private Sub UltimoIdPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim tipo As String
        If cboEmpresa.SelectedValue = 1 Then
            tipo = "MIJ"
        Else
            tipo = "MID"
        End If
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla '" & tipo & "', '" & cboEmpresa.SelectedValue & "', '" & cboCargo.SelectedValue & "', '" & cboSucursal.SelectedValue & "', '" & cboIC.SelectedValue & "', '" & cboDepto.SelectedValue & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdPlantilla.Value = rdoReader(0).ToString
                Else
                    hdnIdPlantilla.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("UltimoIdPlantilla", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub GuardarPlantilla()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim tipo As String
        Try

            If cboEmpresa.SelectedValue = 1 Then
                tipo = "IJT"
            Else
                tipo = "I"
            End If
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = tipo
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@cod_cargo", SqlDbType.NVarChar)
            comando.Parameters("@cod_cargo").Value = cboCargo.SelectedValue
            comando.Parameters.Add("@id_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@id_sucursal").Value = cboSucursal.SelectedValue
            comando.Parameters.Add("@cod_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@cod_instrumento").Value = cboIC.SelectedValue
            If cboEmpresa.SelectedValue = 1 Then
                comando.Parameters.Add("@cod_depto", SqlDbType.NVarChar)
                comando.Parameters("@cod_depto").Value = cboDepto.SelectedValue
            End If
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarPlantilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarDetPlantilla(ByVal id_plantilla As String, ByVal id_det_item As String, ByVal est_incluir As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ID"
            comando.Parameters.Add("@id_plantilla", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Parameters.Add("@est_incluir", SqlDbType.Bit)
            comando.Parameters("@est_incluir").Value = est_incluir
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarPlantilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarDetPlantilla(ByVal id_plantilla As String, ByVal id_det_item As String, ByVal est_incluir As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UD"
            comando.Parameters.Add("@id_plantilla", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Parameters.Add("@est_incluir", SqlDbType.Bit)
            comando.Parameters("@est_incluir").Value = est_incluir
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDetPlantilla", Err, Page, Master)
        End Try
    End Sub


    Private Sub EliminarDet(ByVal id_plantilla As String, ByVal id_det_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_plantilla"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ED"
            comando.Parameters.Add("@id_plantilla", SqlDbType.NVarChar)
            comando.Parameters("@id_plantilla").Value = id_plantilla
            comando.Parameters.Add("@id_det_item", SqlDbType.NVarChar)
            comando.Parameters("@id_det_item").Value = id_det_item
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarDet", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarDetalle(ByVal id_det_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla 'RDI' , '" & id_det_item & "', '" & hdnIdPlantilla.Value & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnExisteDet.Value = 1
                    hdnEstIncluir.Value = rdoReader(11).ToString
                Else
                    hdnExisteDet.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarDetalle", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub grdFijos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdFijos.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
            Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

            ' Call UltimoIdPlantilla()

            If hdnIdPlantilla.Value = 0 Then
                obj_chkIncluir.Checked = False

            Else
                Call RescatarDetalle(obj_IdItem.Value)

                If hdnExisteDet.Value = 1 Then
                    If hdnEstIncluir.Value = True Then
                        obj_chkIncluir.Checked = True
                    Else
                        obj_chkIncluir.Checked = False
                    End If
                Else
                    obj_chkIncluir.Checked = False

                End If
            End If
        End If
    End Sub

    Private Sub grdVariables_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdVariables.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
            Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

            ' Call UltimoIdPlantilla()
            If hdnIdPlantilla.Value = 0 Then
                obj_chkIncluir.Checked = False

            Else
                Call RescatarDetalle(obj_IdItem.Value)

                If hdnExisteDet.Value = 1 Then

                    If hdnEstIncluir.Value = True Then
                        obj_chkIncluir.Checked = True
                    Else
                        obj_chkIncluir.Checked = False
                    End If
                Else
                    obj_chkIncluir.Checked = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try
            If cboEmpresa.SelectedValue = 0 Or cboIC.SelectedValue = 0 Then
                MessageBox("Guardar", "Debe seleccionar la Empresa y el IC", Page, Master, "W")
            Else
                If hdnIdPlantilla.Value <> 0 Then
                    For Each row As GridViewRow In grdFijos.Rows
                        Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                        Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

                        If obj_chkIncluir.Checked = True Then
                            Call RescatarDetalle(obj_IdItem.Value)
                            If hdnExisteDet.Value = 0 Then
                                Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            Else
                                Call ActualizarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            End If
                        Else
                            Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                        End If
                    Next

                    For Each row As GridViewRow In grdVariables.Rows
                        Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                        Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

                        If obj_chkIncluir.Checked = True Then
                            Call RescatarDetalle(obj_IdItem.Value)
                            If hdnExisteDet.Value = 0 Then
                                Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            Else
                                Call ActualizarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            End If
                        Else
                            Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                        End If
                    Next

                Else
                    Call GuardarPlantilla()
                    Call UltimoIdPlantilla()

                    For Each row As GridViewRow In grdFijos.Rows
                        Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                        Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

                        If obj_chkIncluir.Checked = True Then
                            Call RescatarDetalle(obj_IdItem.Value)
                            If hdnExisteDet.Value = 0 Then
                                Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            End If
                        Else
                            Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                        End If
                    Next

                    For Each row As GridViewRow In grdVariables.Rows
                        Dim obj_chkIncluir As CheckBox = CType(row.FindControl("chkIncluir"), CheckBox)
                        Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)

                        If obj_chkIncluir.Checked = True Then
                            Call RescatarDetalle(obj_IdItem.Value)
                            If hdnExisteDet.Value = 0 Then
                                Call GuardarDetPlantilla(hdnIdPlantilla.Value, obj_IdItem.Value, obj_chkIncluir.Checked)
                            End If
                        Else
                            Call EliminarDet(hdnIdPlantilla.Value, obj_IdItem.Value)
                        End If
                    Next
                End If

                Call CargarGrillaFijos()
                Call CargarGrillaVariables()
                Call RescatarPlantilla()


                MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            End If
        Catch ex As Exception
            MessageBoxError("btnGuardar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDepto.SelectedIndexChanged

        If cboEmpresa.SelectedValue = 1 Then
            lblDepto.Visible = True
            cboDepto.Visible = True
        Else
            lblDepto.Visible = False
            cboDepto.Visible = False
        End If
        Call RescatarPlantilla()
        Call CargarGrillaFijos()
        Call CargarGrillaVariables()

    End Sub
End Class