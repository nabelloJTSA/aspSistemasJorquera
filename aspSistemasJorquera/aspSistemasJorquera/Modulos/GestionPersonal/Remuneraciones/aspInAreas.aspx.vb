﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspInAreas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla("")
                Call carga_combo_empresa()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> 5 order by cod_empresa"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla(ByVal Nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_area"
            Dim strSQL As String = "Exec proc_registro_areas 'G' , '" & Nombre & "', '" & cboEmpresa.SelectedValue & "'"
            grdArea.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdArea.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        Call CargarGrilla(txtbNombre.Text)
    End Sub
    Private Sub Limpiar()
        txtArea.Text = ""
        cboEmpresa.ClearSelection()
        hdnActivo.Value = 0
        Call CargarGrilla("")
    End Sub

    Private Sub guardar_area()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_areas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nombre_area", SqlDbType.NVarChar)
            comando.Parameters("@nombre_area").Value = txtArea.Text
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("guardar_area", Err, Page, Master)
        End Try
    End Sub

    Private Sub actualizar_area()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_areas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nombre_area", SqlDbType.NVarChar)
            comando.Parameters("@nombre_area").Value = txtArea.Text
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdItem.Value
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Actualizado Exitosamente", Page, Master, "S")
            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("actualizar_area", Err, Page, Master)
        End Try
    End Sub

    Private Sub eliminar_area()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_areas"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdItem.Value

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("EL AREA NO SE PUEDE ELIMINAR PORQUE EXISTEN ENCARGADOS ASOCIADOS A ESTA", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Private Sub grdArea_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdArea.PageIndexChanging
        Try
            grdArea.PageIndex = e.NewPageIndex
            Call CargarGrilla("")
        Catch ex As Exception
            MessageBoxError("grdArea_PageIndexChanging", Err, Page, Master)

        End Try
    End Sub

    Protected Sub grdArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdArea.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdArea.SelectedRow
            Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim nombre_concepto_obj As Label = CType(row.FindControl("lblNombre"), Label)
            Dim hdnidDepto As HiddenField = CType(row.FindControl("hdnDepto"), HiddenField)
            Dim hdniEmpresa As HiddenField = CType(row.FindControl("hdnEmpresa"), HiddenField)
            hdnIdItem.Value = hdnid.Value

            cboEmpresa.SelectedValue = hdniEmpresa.Value
            txtArea.Text = nombre_concepto_obj.Text

            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdArea_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtArea.Text = "" Or cboEmpresa.SelectedValue = 0 Then
            MessageBox("Guardar", "Debe ingresar datos", Page, Master, "W")
        Else
            If hdnActivo.Value = 0 Then
                Call guardar_area()
            Else
                Call actualizar_area()
            End If
        End If
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla("")
    End Sub

    Private Sub grdArea_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdArea.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdArea.Rows(intRow)
        If Trim(LCase(e.CommandName)) = "2" Then

            Call eliminar_area()
        End If
    End Sub
End Class