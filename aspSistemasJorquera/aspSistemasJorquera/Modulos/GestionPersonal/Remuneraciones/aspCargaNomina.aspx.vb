﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Web.UI.HtmlControls
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Public Class aspCargaNomina
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call carga_combo_empresa()
                Call CargaPeriodo()
                mtvNomina.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargaPeriodo()
        Try
            Dim strNomTablaR As String = "REMMae_periodo"
            Dim strSQLR As String = "Exec proc_doctos_payroll 'CCP'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "num_periodo"
            cboPeriodo.DataValueField = "num_periodo"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> 5 order by cod_empresa"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnAnt_Click(sender As Object, e As EventArgs) Handles btnAnt.Click
        mtvNomina.ActiveViewIndex = 0
        btnAnt.CssClass = btnAnt.CssClass.Replace("btn-outline-info", "btn-info active")
        btnSue.CssClass = btnSue.CssClass.Replace("btn-info active", "btn-outline-info")
    End Sub

    Protected Sub btnSue_Click(sender As Object, e As EventArgs) Handles btnSue.Click
        mtvNomina.ActiveViewIndex = 1

        btnSue.CssClass = btnSue.CssClass.Replace("btn-outline-info", " btn-info active")
        btnAnt.CssClass = btnAnt.CssClass.Replace("btn-info active", " btn-outline-info")
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        If cboEmpresa.SelectedValue <> 0 Then
            pnlDatos.Visible = True
            ' Call Grillas()
        Else
            pnlDatos.Visible = False
        End If
    End Sub

    Private Sub Rescatar_cabecera()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_doctos_payroll 'RID'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdCab.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("Rescatar_cabecera", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub Rescatar_datos_cabecera(ByVal tipo As Integer)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec proc_doctos_payroll 'RDC','" & hdnIdCab.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    Call IngresoConvenio()

                    If tipo = 1 Then
                        txtRutEmpAnt.Text = Trim(rdoReader(2).ToString)
                        txtNominaAnt.Text = Trim(rdoReader(3).ToString)
                        txtNomEmpAnt.Text = Trim(rdoReader(4).ToString)
                        ' txtConvenioAnt.Text = Trim(rdoReader(7).ToString)
                        txtConvenioAnt.Text = hdnNumConvenio.Value
                        txtTotalAnt.Text = Trim(rdoReader(8).ToString)
                        txtFecPagoAnt.Text = Trim(rdoReader(12).ToString)
                    End If
                    If tipo = 2 Then
                        txtRutSueldos.Text = Trim(rdoReader(2).ToString)
                        txtNominaSueldo.Text = Trim(rdoReader(3).ToString)
                        txtEmpresaSueldo.Text = Trim(rdoReader(4).ToString)
                        'txtConvenioSueldo.Text = Trim(rdoReader(7).ToString)
                        txtConvenioSueldo.Text = hdnNumConvenio.Value
                        txtTotalSueldo.Text = Trim(rdoReader(8).ToString)
                        txtfechaSueldo.Text = Trim(rdoReader(12).ToString)
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("Rescatar_datos_cabecera", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarComboMEdioPago(ByRef objCombo As DropDownList)
        Dim strSQL As String = "select num_codigo, nom_mediopago from mae_medio_pago order by num_codigo"
        Dim strNomTabla As String = "mae_medio_pago"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBaseDatos").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboBancos(ByRef objCombo As DropDownList)
        Dim strSQL As String = "select num_codigo, nom_banco from mae_banco order by num_codigo"
        Dim strNomTabla As String = "mae_banco"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBaseDatos").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarCombos", Err, Page, Master)
        End Try
    End Sub

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".txt"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\NominaPayroll\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Private Sub IngresoConvenio()
        If cboEmpresa.SelectedValue = 22 Or cboEmpresa.SelectedValue = 4 Then
            hdnNumConvenio.Value = "815"
        Else
            hdnNumConvenio.Value = "813"
        End If
    End Sub


    Private Sub cargarDoctoTXT(ByVal instancia As HttpRequest, ByVal Tipo As Integer)
        Dim _carpeta As String = "C:\NominaPayroll\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath
        Dim archivo As FileUpload

        If Tipo = 1 Then
            archivo = fuplCargarAnt
        End If
        If Tipo = 2 Then
            archivo = fuplSueldo
        End If
        Try
            If archivo.HasFile = True Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    '----------------------Eliminar Archivos Existentes------------------------------
                    EliminarArchivos()

                    '-----------------------Guardar nuevo archivo------------------------------
                    archivo.SaveAs(_directorioParaGuardar)

                    pnlDatos.Visible = True
                    hdnIdCab.Value = 0

                    Dim objReader As New StreamReader(_directorioParaGuardar)

                    Dim sLine As String = ""
                    Dim arrText As New ArrayList()
                    Dim i As Integer = 0

                    Do
                        sLine = objReader.ReadLine()
                        If Not sLine Is Nothing Then
                            arrText.Add(sLine)
                        End If
                    Loop Until sLine Is Nothing
                    objReader.Close()



                    Dim empresa As Integer

                    empresa = cboEmpresa.SelectedValue

                    For Each sLine In arrText


                        If i = 0 Then
                            'txtcab.Text = (sLine.Substring(0, 3) & "" & String.Format("{0,-1}", sLine.Substring(3, 9)) & "" & sLine.Substring(12, 8) & "" & sLine.Substring(20, 25) & "" & sLine.Substring(45, 2) & "" & sLine.Substring(47, 8) & "" & sLine.Substring(55, 3) & "" & sLine.Substring(58, 10) & "" & String.Format("{0,-1}", sLine.Substring(68, 332)))

                            i = i + 1

                            '"-" + sLine.Substring(12, 3) +
                            ' "-" + sLine.Substring(55, 3) +

                            'TextBox1.Text = sLine.Substring(0, 3) +
                            '    "-" + String.Format("{0,-1}", sLine.Substring(3, 9)) +
                            '    "-" + sLine.Substring(15, 5) +
                            '    "-" + sLine.Substring(20, 25) +
                            '    "-" + sLine.Substring(45, 2) +
                            '    "-" + sLine.Substring(47, 8) +
                            '    "-" + sLine.Substring(12, 3) +
                            '    "-" + sLine.Substring(56, 10) +
                            '    "-" + String.Format("{0,-1}", sLine.Substring(66, 334))


                            Dim conx As New SqlConnection(strCnx)
                            Dim comando As New SqlCommand
                            comando = New SqlCommand
                            comando.CommandTimeout = 90000000
                            comando.CommandType = CommandType.Text
                            comando.CommandText = "INSERT INTO REMMae_cab_sueldos (cod_cab, rut_empresa, num_nomina, nom_empresa, tip_cabecera, fec_nomina, convenio, val_total, tip_pago, cod_empresa, cod_tipo_mov) Values ('" & sLine.Substring(0, 3) & "', '" & String.Format("{0,-1}", sLine.Substring(3, 9)) & "', '" & sLine.Substring(15, 5) & "', '" & sLine.Substring(20, 25) & "', '" & sLine.Substring(45, 2) & "', '" & sLine.Substring(47, 8) & "', '" & sLine.Substring(55, 3) & "', '" & sLine.Substring(56, 10) & "', '" & String.Format("{0,-1}", sLine.Substring(66, 334)) & "', '" & empresa & "', '" & Tipo & "')"
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()

                        ElseIf i > 0 Then

                            Call Rescatar_cabecera()

                            '  txtdet.Text = txtdet.Text & vbCrLf & (sLine.Substring(0, 3) & "" & String.Format("{0,-1}", sLine.Substring(3, 14)) & "" & sLine.Substring(17, 5) & "" & sLine.Substring(22, 3) & "" & sLine.Substring(25, 9) & "" & String.Format("{0,-1}", sLine.Substring(34, 60)) & "" & String.Format("{0,-1}", sLine.Substring(94, 73)) & "" & sLine.Substring(167, 2) & "" & String.Format("{0,-1}", sLine.Substring(169, 3)) & "" & String.Format("{0,-1}", sLine.Substring(172, 22)) & "" & sLine.Substring(194, 3) & "" & sLine.Substring(197, 11) & "" & sLine.Substring(208, 2) & "" & sLine.Substring(210, 190))

                            'TextBox2.Text = TextBox2.Text + "" + vbCrLf + "" + sLine.Substring(0, 3) & "" & String.Format("{0,-1}", sLine.Substring(3, 14)) & "" & sLine.Substring(17, 5) & "" & sLine.Substring(22, 3) & "" & sLine.Substring(25, 9) & "" & String.Format("{0,-1}", sLine.Substring(34, 60)) & "" & String.Format("{0,-1}", sLine.Substring(94, 73)) & "" & sLine.Substring(167, 2) & "" & String.Format("{0,-1}", sLine.Substring(169, 3)) & "" & String.Format("{0,-1}", sLine.Substring(172, 22)) & "" & sLine.Substring(194, 3) & "" & sLine.Substring(197, 11) & "" & sLine.Substring(208, 2) & "" & sLine.Substring(210, 145)


                            '+
                            '    "-" + String.Format("{0,-1}", sLine.Substring(3, 9)) +
                            '    "-" + sLine.Substring(15, 5) +
                            '    "-" + sLine.Substring(20, 25) +
                            '    "-" + sLine.Substring(45, 2) +
                            '    "-" + sLine.Substring(47, 8) +
                            '    "-" + sLine.Substring(12, 3) +
                            '    "-" + sLine.Substring(56, 10) +
                            '    "-" + String.Format("{0,-1}", sLine.Substring(66, 334))

                            Dim conx As New SqlConnection(strCnx)
                            Dim comando As New SqlCommand
                            comando = New SqlCommand
                            comando.CommandTimeout = 90000000
                            comando.CommandType = CommandType.Text
                            comando.CommandText = "INSERT INTO REMMae_det_sueldos (id_cab, cod_det, rut_empresa, num_nomina, medio_pago, rut_persona, nom_persona, num1, banco, cod_banco, num_cuenta, num2, val_total, num3, glosa) Values ('" & hdnIdCab.Value & "','" & sLine.Substring(0, 3) & "', '" & String.Format("{0,-1}", sLine.Substring(3, 14)) & "', '" & sLine.Substring(17, 5) & "', '" & sLine.Substring(22, 3) & "', '" & sLine.Substring(25, 9) & "', '" & String.Format("{0,-1}", sLine.Substring(34, 60)) & "', '" & String.Format("{0,-1}", sLine.Substring(94, 73)) & "', '" & sLine.Substring(167, 2) & "', '" & String.Format("{0,-1}", sLine.Substring(169, 3)) & "', '" & String.Format("{0,-1}", sLine.Substring(172, 22)) & "', '" & sLine.Substring(194, 3) & "', '" & sLine.Substring(197, 11) & "', '" & sLine.Substring(208, 2) & "', '" & sLine.Substring(210, 145) & "')"
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()

                            i = i + 1
                            Call Rescatar_datos_cabecera(Tipo)
                        End If

                    Next

                    Call CargarGrillaDetalles(Tipo)
                    If Tipo = "1" Then
                        btnGuardarAnticipo.Enabled = True
                    End If
                    If Tipo = "2" Then
                        btnGuardarSueldos.Enabled = True
                    End If

                    MessageBox("Cargar Planilla", "Datos Cargados, proceda a verificar y a Guardar", Page, Master, "S")
                    btnSueldoFin700.Enabled = False
                    btnPagosWebSueldos.Enabled = False
                    pnlCabSue.Enabled = True
                    pnlDetSue.Enabled = True

                Else
                    MessageBox("cargarDoctoTXT", "Debe seleccionar un archivo válido (.txt)", Page, Master, "I")
                    If Tipo = "1" Then
                        btnGuardarAnticipo.Enabled = False
                    End If
                    If Tipo = "2" Then
                        btnGuardarSueldos.Enabled = False
                    End If
                End If
            Else
                MessageBox("cargarDoctoTXT", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "I")
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try

    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargarAnticipos.Click
        Call cargarDoctoTXT(Me.Request, 1)
    End Sub

    Private Sub CargarGrillaDetalles(ByVal tipo As Integer)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_det_sueldos"
            Dim strSQL As String = "Exec proc_doctos_payroll 'RDET_A' , '" & hdnIdCab.Value & "'"
            If tipo = 1 Then
                grdDetAnt.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdDetAnt.DataBind()
            End If
            If tipo = 2 Then
                grdSueldos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdSueldos.DataBind()
            End If
        Catch ex As Exception
            MessageBoxError("CargarGrillaDetalles", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkactualizarA(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)
        Dim largo As Integer = 0
        Dim i As Integer = 0
        If CType(row.FindControl("chkSeleccionar"), CheckBox).Checked = True Then
            txtTotalAnt.Text = CInt(txtTotalAnt.Text) + CInt(CType(row.FindControl("txtMontoPagoAnt"), TextBox).Text)
        Else
            txtTotalAnt.Text = CInt(txtTotalAnt.Text) - CInt(CType(row.FindControl("txtMontoPagoAnt"), TextBox).Text)
        End If
        largo = Len(txtTotalAnt.Text)
        i = largo - 10
        If largo < 11 Then
            For i = largo To 9 Step 1
                txtTotalAnt.Text = "0" + txtTotalAnt.Text
            Next
        End If
    End Sub

    Protected Sub chkactualizar(sender As Object, e As EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(chk.NamingContainer, GridViewRow)
        Dim largo As Integer = 0
        Dim i As Integer = 0
        If CType(row.FindControl("chkSeleccionar"), CheckBox).Checked = True Then
            txtTotalSueldo.Text = CInt(txtTotalSueldo.Text) + CInt(CType(row.FindControl("txtMontoPagoAnt"), TextBox).Text)
        Else
            txtTotalSueldo.Text = CInt(txtTotalSueldo.Text) - CInt(CType(row.FindControl("txtMontoPagoAnt"), TextBox).Text)
        End If
        largo = Len(txtTotalSueldo.Text)
        i = largo - 10
        If largo < 11 Then
            For i = largo To 9 Step 1
                txtTotalSueldo.Text = "0" + txtTotalSueldo.Text
            Next
        End If
    End Sub

    Private Sub Actualizar_Cab_Anticipos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_doctos_payroll"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UCAB_A"
            comando.Parameters.Add("@num_nomina", SqlDbType.VarChar)
            comando.Parameters("@num_nomina").Value = String.Format("{0,8}", (String.Format("{0:D5}", Convert.ToInt32(txtNominaAnt.Text))))
            comando.Parameters.Add("@fec_nomina", SqlDbType.VarChar)
            comando.Parameters("@fec_nomina").Value = Right(txtFecPagoAnt.Text, 4) + txtFecPagoAnt.Text.Substring(3, 2) + Left(txtFecPagoAnt.Text, 2)
            comando.Parameters.Add("@convenio", SqlDbType.VarChar)
            comando.Parameters("@convenio").Value = String.Format("{0:D3}", Convert.ToInt32(txtConvenioAnt.Text))
            comando.Parameters.Add("@val_total", SqlDbType.VarChar)
            comando.Parameters("@val_total").Value = String.Format("{0:D11}", Convert.ToInt32(txtTotalAnt.Text))
            comando.Parameters.Add("@id_cab", SqlDbType.VarChar)
            comando.Parameters("@id_cab").Value = hdnIdCab.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar_Cab_Anticipos", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar_Cab_Sueldos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_doctos_payroll"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UCAB_A"
            comando.Parameters.Add("@num_nomina", SqlDbType.VarChar)
            comando.Parameters("@num_nomina").Value = String.Format("{0,8}", (String.Format("{0:D5}", Convert.ToInt32(txtNominaSueldo.Text))))
            comando.Parameters.Add("@fec_nomina", SqlDbType.VarChar)
            comando.Parameters("@fec_nomina").Value = Right(txtfechaSueldo.Text, 4) + txtfechaSueldo.Text.Substring(3, 2) + Left(txtfechaSueldo.Text, 2)
            comando.Parameters.Add("@convenio", SqlDbType.VarChar)
            comando.Parameters("@convenio").Value = String.Format("{0:D3}", Convert.ToInt32(txtConvenioSueldo.Text))
            comando.Parameters.Add("@val_total", SqlDbType.VarChar)
            comando.Parameters("@val_total").Value = String.Format("{0:D11}", Convert.ToInt32(txtTotalSueldo.Text))
            comando.Parameters.Add("@id_cab", SqlDbType.VarChar)
            comando.Parameters("@id_cab").Value = hdnIdCab.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar_Cab_Sueldos", Err, Page, Master)
        End Try
    End Sub


    Private Sub Actualizar_Det_Anticipos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_doctos_payroll"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UDET_A"
            comando.Parameters.Add("@num_nomina", SqlDbType.VarChar)
            comando.Parameters("@num_nomina").Value = hdnNumNominaAnt.Value
            comando.Parameters.Add("@medio_pago", SqlDbType.VarChar)
            comando.Parameters("@medio_pago").Value = hdnMedioPagoAnt.Value
            comando.Parameters.Add("@cod_banco", SqlDbType.VarChar)
            comando.Parameters("@cod_banco").Value = hdnCodBancoAnt.Value
            comando.Parameters.Add("@num_cuenta", SqlDbType.VarChar)
            comando.Parameters("@num_cuenta").Value = hdnNumCuentaAnt.Value
            comando.Parameters.Add("@val_total", SqlDbType.VarChar)
            comando.Parameters("@val_total").Value = String.Format("{0:D11}", Convert.ToInt32(hdnValTotalAnt.Value))
            comando.Parameters.Add("@glosa", SqlDbType.VarChar)
            comando.Parameters("@glosa").Value = hdnGlosaAnt.Value
            comando.Parameters.Add("@id_det", SqlDbType.VarChar)
            comando.Parameters("@id_det").Value = hdnIdDetAnt.Value
            'comando.Parameters.Add("@busqueda2", SqlDbType.VarChar)
            'comando.Parameters("@busqueda2").Value = txtFecPagoPer.Text
            comando.Parameters.Add("@est_traspaso", SqlDbType.Int)
            comando.Parameters("@est_traspaso").Value = hdnEstTraspaso.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar_Det_Anticipos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarAnticipo_Click(sender As Object, e As EventArgs) Handles btnGuardarAnticipo.Click
        Try
            If txtConvenioAnt.Text = "" Or txtNominaAnt.Text = "" Then
                MessageBox("Guardar Anticipo", "Ingrese Datos Obligatorios (*)", Page, Master, "I")
            Else
                txtFecPagoAnt.Text = CDate(txtFecPagoAnt.Text)
                Call Actualizar_Cab_Anticipos()

                For Each row As GridViewRow In grdDetAnt.Rows

                    Dim obj_id_det As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                    Dim objBanco As DropDownList = CType(row.FindControl("cboBancoAnt"), DropDownList)
                    Dim objMedioPago As DropDownList = CType(row.FindControl("cboMedioPagoAnt"), DropDownList)

                    Dim obj_monto_pago As TextBox = CType(row.FindControl("txtMontoPagoAnt"), TextBox)
                    Dim obj_num_cuenta As TextBox = CType(row.FindControl("txtNumCuentaAnt"), TextBox)
                    Dim obj_glosa As TextBox = CType(row.FindControl("txtDesAnt"), TextBox)

                    Dim chkSeleccionar As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)

                    hdnIdDetAnt.Value = obj_id_det.Value
                    hdnNumNominaAnt.Value = txtNominaAnt.Text
                    hdnMedioPagoAnt.Value = objMedioPago.SelectedValue
                    hdnCodBancoAnt.Value = objBanco.SelectedValue
                    hdnNumCuentaAnt.Value = obj_num_cuenta.Text
                    hdnValTotalAnt.Value = obj_monto_pago.Text
                    hdnGlosaAnt.Value = obj_glosa.Text

                    If chkSeleccionar.Checked = True Then
                        hdnEstTraspaso.Value = 1
                    Else
                        hdnEstTraspaso.Value = 0
                    End If

                    Call Actualizar_Det_Anticipos()
                Next

                Call Rescatar_datos_cabecera(1)
                Call CargarGrillaDetalles(1)

                MessageBox("Guardar Anticipo", "Registros Actualizados", Page, Master, "S")

                btnExcel.Enabled = True
                btnTxt.Enabled = False
                btnGuardarAnticipo.Enabled = True
                pnlCabAnt.Enabled = True
                pnlDetAnt.Enabled = True
            End If

        Catch ex As Exception
            MessageBoxError("btnGuardarAnticipo_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdDetAnt_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDetAnt.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then

                Dim objBanco As DropDownList = CType(row.FindControl("cboBancoAnt"), DropDownList)
                Dim objMedioPago As DropDownList = CType(row.FindControl("cboMedioPagoAnt"), DropDownList)

                Dim chk_traspaso As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)

                Dim objcodBanco As HiddenField = CType(row.FindControl("hdnCodBanco"), HiddenField)
                Dim objCodMedioPago As HiddenField = CType(row.FindControl("hdnCodMedioPago"), HiddenField)
                Dim objTraspaso As HiddenField = CType(row.FindControl("hdnEstT"), HiddenField)

                Call CargarComboBancos(objBanco)
                objBanco.SelectedValue = objcodBanco.Value

                Call CargarComboMEdioPago(objMedioPago)
                objMedioPago.SelectedValue = objCodMedioPago.Value

                Dim obj_rut As Label = CType(row.FindControl("lblRutAnt"), Label)
                hdnRutGrilla.Value = obj_rut.Text

                Call RecatarFiniquitados()

                If objTraspaso.Value = True Then
                    chk_traspaso.Checked = True
                Else
                    chk_traspaso.Checked = False
                End If

                If hdnFiniquitado.Value = 0 Then
                    e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='SandyBrown'; this.style.cursor='default';"
                    e.Row.Attributes("onmouseout") = "this.style.backgroundColor='Tomato'"
                    row.BackColor = Drawing.Color.Tomato
                    chk_traspaso.Enabled = True
                Else
                    e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
                    e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"
                    row.BackColor = Drawing.Color.White
                    chk_traspaso.Enabled = False
                End If
            End If

        Catch ex As Exception
            MessageBoxError("grdHistorial_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCargarAnticipos0_Click(sender As Object, e As EventArgs) Handles btnTxt.Click
        Call ArchivoPagoWEB("ANT")
    End Sub
    Private Function ArchivoPagoWEB(ByVal tipo As String) As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                strNomArchivo = "Nomina_PW_" & tipo & "_" & hdnIdCab.Value & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If

                    File.AppendAllText(strPathFile, Trim(strNomArchivo))

                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

                    strSQL = "Exec proc_doctos_payroll 'SAT', '" & hdnIdCab.Value & "'"
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))

                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()

                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + toDownOpen.Name)
                        Response.AddHeader("Content-Length", toDownOpen.Length.ToString())
                        Response.ContentType = "text/text"
                        Response.WriteFile(strPathFile)
                    End If
                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPagoWEB", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Call TraspasoFIN700Ant()
        btnExcel.Enabled = False
        btnTxt.Enabled = True
        btnGuardarAnticipo.Enabled = False
        pnlCabAnt.Enabled = False
        pnlDetAnt.Enabled = False
    End Sub


    Private Function TraspasoFIN700Ant() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandTimeout = 90000000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_traspasoFin700_SueloAnticipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ITA"
            comando.Parameters.Add("@id_cabecera", SqlDbType.NVarChar)
            comando.Parameters("@id_cabecera").Value = hdnIdCab.Value
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = txtFecPagoAnt.Text
            comando.Parameters.Add("@nom_usuario", SqlDbType.NVarChar)
            comando.Parameters("@nom_usuario").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Anticipos", "Registros Traspasados", Page, Master, "S")
            Return True
        Catch ex As Exception
            Return False
            MessageBox("Anticipos", "TraspasoFIN700Ant()", Page, Master, "E")
        End Try

    End Function


    Private Function TraspasoFIN700SUELDO() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandTimeout = 90000000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_traspasoFin700_SueloAnticipo"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ITS"
            comando.Parameters.Add("@id_cabecera", SqlDbType.NVarChar)
            comando.Parameters("@id_cabecera").Value = hdnIdCab.Value

            'comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            'comando.Parameters("@busqueda3").Value = txtFecPagoPer.Text

            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = cboPeriodo.SelectedValue

            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = txtfechaSueldo.Text
            comando.Parameters.Add("@nom_usuario", SqlDbType.NVarChar)
            comando.Parameters("@nom_usuario").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Sueldos", "Registros Traspasados", Page, Master, "S")
            Return True
        Catch ex As Exception
            Return False
            MessageBox("Sueldos", "TraspasoFIN700SUELDO()", Page, Master, "E")
        End Try

    End Function


    Private Function ArchivoPlanoFIN700() As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                strNomArchivo = "Nomina_F7_ANT_" & hdnIdCab.Value & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If

                    File.AppendAllText(strPathFile, Trim(strNomArchivo))

                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

                    strSQL = "Exec proc_doctos_payroll 'SAE', '" & hdnIdCab.Value & "'"
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))

                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()

                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + toDownOpen.Name)
                        Response.AddHeader("Content-Length", toDownOpen.Length.ToString())
                        Response.ContentType = "application/octet-stream"
                        Response.WriteFile(strPathFile)
                    End If
                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPlanoFIN700", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function

    'Private Function ArchivoPlanoFIN700SUELDO() As Boolean
    '    Dim strPathFile As String
    '    Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
    '    Dim strSQL As String = "", strNomArchivo As String = ""
    '    Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
    '    Using cnxBaseDatos As New SqlConnection(strCnx)
    '        cnxBaseDatos.Open()
    '        Try
    '            strNomArchivo = "Nomina_F7_SUEL_" & hdnIdCab.Value & ".txt"
    '            strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
    '            If (String.IsNullOrEmpty(strPathFile) = False) Then
    '                Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
    '                If (toDownExiFile.Exists) Then
    '                    Dim FileToDelete As String = strPathFile
    '                    If File.Exists(FileToDelete) = True Then
    '                        File.Delete(FileToDelete)
    '                    End If
    '                End If

    '                File.AppendAllText(strPathFile, Trim(strNomArchivo))

    '                Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

    '                strSQL = "Exec proc_doctos_payroll 'SSE', '" & hdnIdCab.Value & "', '" & Format(CDate(txtFecPagoPer.Text), "yyyyMMdd") & "', '" & Format(CDate(txtfechaSueldo.Text), "yyyyMMdd") & "'"
    '                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
    '                rdoReader = cmdTemporal.ExecuteReader()
    '                Do While rdoReader.Read
    '                    strTextoDetalle1 = ""
    '                    strTextoDetalle1 = Trim(rdoReader(0))

    '                    z_varocioStreamWriter.WriteLine(strTextoDetalle1)
    '                Loop
    '                rdoReader.Close()
    '                rdoReader = Nothing
    '                cmdTemporal.Dispose()
    '                cmdTemporal = Nothing
    '                z_varocioStreamWriter.Close()

    '                Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
    '                If (toDownOpen.Exists) Then
    '                    Response.Clear()
    '                    Response.AddHeader("Content-Disposition", "attachment; filename = " + toDownOpen.Name)
    '                    Response.AddHeader("Content-Length", toDownOpen.Length.ToString())
    '                    Response.ContentType = "application/octet-stream"
    '                    Response.WriteFile(strPathFile)
    '                End If
    '                blnError = True
    '            End If
    '        Catch ex As Exception
    '            blnError = False
    '            MessageBoxError("ArchivoPlanoFIN700SUELDO", Err, Page, Master)
    '        Finally
    '            cnxBaseDatos.Close()
    '            rdoReader = Nothing
    '            cmdTemporal = Nothing
    '        End Try
    '    End Using
    '    Return blnError
    'End Function

    Protected Sub btnCargaSueldos_Click(sender As Object, e As EventArgs) Handles btnCargaSueldos.Click
        Call cargarDoctoTXT(Me.Request, 2)
        txtFecPagoPer.Text = txtfechaSueldo.Text
    End Sub

    Protected Sub btnGuardarSueldos_Click(sender As Object, e As EventArgs) Handles btnGuardarSueldos.Click
        Try
            If txtConvenioSueldo.Text = "" Or txtNominaSueldo.Text = "" Then
                MessageBox("Guardar sueldos", "Ingrese Datos Obligatorios (*)", Page, Master, "I")
            Else
                txtfechaSueldo.Text = CDate(txtfechaSueldo.Text)
                Call Actualizar_Cab_Sueldos()

                For Each row As GridViewRow In grdSueldos.Rows

                    Dim obj_id_det As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                    Dim objBanco As DropDownList = CType(row.FindControl("cboBancoAnt"), DropDownList)
                    Dim objMedioPago As DropDownList = CType(row.FindControl("cboMedioPagoAnt"), DropDownList)

                    Dim obj_monto_pago As TextBox = CType(row.FindControl("txtMontoPagoAnt"), TextBox)
                    Dim obj_num_cuenta As TextBox = CType(row.FindControl("txtNumCuentaAnt"), TextBox)
                    Dim obj_glosa As TextBox = CType(row.FindControl("txtDesAnt"), TextBox)

                    hdnIdDetAnt.Value = obj_id_det.Value
                    hdnNumNominaAnt.Value = txtNominaSueldo.Text
                    hdnMedioPagoAnt.Value = objMedioPago.SelectedValue
                    hdnCodBancoAnt.Value = objBanco.SelectedValue
                    hdnNumCuentaAnt.Value = obj_num_cuenta.Text
                    hdnValTotalAnt.Value = obj_monto_pago.Text
                    hdnGlosaAnt.Value = obj_glosa.Text

                    Dim chkSeleccionar As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)
                    If chkSeleccionar.Checked = True Then
                        hdnEstTraspaso.Value = 1
                    Else
                        hdnEstTraspaso.Value = 0
                    End If

                    Call Actualizar_Det_Anticipos()
                Next

                Call Rescatar_datos_cabecera(2)
                Call CargarGrillaDetalles(2)
                MessageBox("Guardar Sueldos", "Registros Actualizados", Page, Master, "S")

                btnSueldoFin700.Enabled = True
                btnPagosWebSueldos.Enabled = False
                btnGuardarSueldos.Enabled = True
                pnlCabSue.Enabled = True
                pnlDetSue.Enabled = True
            End If
        Catch ex As Exception
            MessageBoxError("btnGuardarAnticipo_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub RecatarFiniquitados()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "select est_vigencia from REMMae_ficha_personal where Replace(rut_personal, '-', '') = '" & hdnRutGrilla.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read = True Then
                    If rdoReader(0).ToString = True Then
                        hdnFiniquitado.Value = 1
                    Else
                        hdnFiniquitado.Value = 0
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("RecatarFiniquitados", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub grdSueldos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdSueldos.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then

                Dim objBanco As DropDownList = CType(row.FindControl("cboBancoAnt"), DropDownList)
                Dim objMedioPago As DropDownList = CType(row.FindControl("cboMedioPagoAnt"), DropDownList)
                Dim objcodBanco As HiddenField = CType(row.FindControl("hdnCodBanco"), HiddenField)
                Dim objCodMedioPago As HiddenField = CType(row.FindControl("hdnCodMedioPago"), HiddenField)
                Dim chk_traspaso As CheckBox = CType(row.FindControl("chkSeleccionar"), CheckBox)
                Dim objTraspaso As HiddenField = CType(row.FindControl("hdnEstT"), HiddenField)
                Call CargarComboBancos(objBanco)
                objBanco.SelectedValue = objcodBanco.Value

                Call CargarComboMEdioPago(objMedioPago)
                objMedioPago.SelectedValue = objCodMedioPago.Value

                Dim obj_rut As Label = CType(row.FindControl("lblRutAnt"), Label)

                hdnRutGrilla.Value = obj_rut.Text

                Call RecatarFiniquitados()

                If objTraspaso.Value = True Then
                    chk_traspaso.Checked = True
                Else
                    chk_traspaso.Checked = False
                End If

                If hdnFiniquitado.Value = 0 Then
                    e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='SandyBrown'; this.style.cursor='default';"
                    e.Row.Attributes("onmouseout") = "this.style.backgroundColor='Tomato'"
                    row.BackColor = Drawing.Color.Tomato
                    chk_traspaso.Enabled = True
                Else
                    e.Row.Attributes("onmouseover") = "this.style.cursor='hand';this.style.backgroundColor='AliceBlue'; this.style.cursor='default';"
                    e.Row.Attributes("onmouseout") = "this.style.backgroundColor='White'"
                    row.BackColor = Drawing.Color.White
                    chk_traspaso.Enabled = False
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdSueldos_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnPagosWebSueldos_Click(sender As Object, e As EventArgs) Handles btnPagosWebSueldos.Click
        Call ArchivoPagoWEB("SUEL")
    End Sub

    Protected Sub btnSueldoFin700_Click(sender As Object, e As EventArgs) Handles btnSueldoFin700.Click
        Call TraspasoFIN700SUELDO()
        btnSueldoFin700.Enabled = False
        btnPagosWebSueldos.Enabled = True
        btnGuardarSueldos.Enabled = False
        pnlCabSue.Enabled = False
        pnlDetSue.Enabled = False
    End Sub


End Class