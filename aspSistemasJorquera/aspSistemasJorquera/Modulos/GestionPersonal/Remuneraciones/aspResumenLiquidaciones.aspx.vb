﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb
Public Class aspResumenLiquidaciones
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                Call CargarAnio()
                Call CargarPeriodo()
                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

            cboAnio1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio1.DataTextField = "anio"
            cboAnio1.DataValueField = "id"
            cboAnio1.DataBind()

            cboAnio2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio2.DataTextField = "anio"
            cboAnio2.DataValueField = "id"
            cboAnio2.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub DatosPersona(ByVal rut As String)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_plantilla_especial 'CPE', '" & rut & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(1).ToString
                    lblNombreRango.Text = rdoReader(1).ToString
                    hdnCodEmp.Value = rdoReader(2).ToString
                    lblEmpresa.Text = rdoReader(3).ToString
                    lblEmpresaRango.Text = rdoReader(3).ToString
                    hdnCodCargo.Value = rdoReader(4).ToString
                    hdnCodInstrumento.Value = rdoReader(9).ToString
                    hdnCodSucursal.Value = rdoReader(6).ToString
                    lblNom.Visible = True
                    lblEmp.Visible = True
                    Label3.Visible = True
                    Label1.Visible = True
                Else
                    lblNombre.Text = ""
                    lblEmpresa.Text = ""
                    lblNom.Visible = False
                    lblEmp.Visible = False
                    lblNombreRango.Text = ""
                    lblEmpresaRango.Text = ""
                    Label1.Visible = False
                    Label3.Visible = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("DatosPersona", Err, Page, Master)
            End Try
        End Using
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo1()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnio1.SelectedValue & "'"

            cboPeriodo1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo1.DataTextField = "periodo"
            cboPeriodo1.DataValueField = "id"
            cboPeriodo1.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo2()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnio2.SelectedValue & "'"

            cboPeriodo2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo2.DataTextField = "periodo"
            cboPeriodo2.DataValueField = "id"
            cboPeriodo2.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaResumen()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "rpt_RRHH_resument_liq  'FHL','" & txtRut.Text & "', '" & cboPeriodo.SelectedValue & "'"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaResumen", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaRango()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "rpt_RRHH_resument_liq  'FRL','" & txtRutRango.Text & "', '" & cboPeriodo1.SelectedValue & "', '" & cboPeriodo2.SelectedValue & "'"
            grdRango.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdRango.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaResumen", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            If txtRut.Text = "" Then
                MessageBox("Generar", "debe ingresar RUT", Page, Master, "W")
            Else
                Call CargarGrillaResumen()
            End If

        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdResumen_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdResumen.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdResumen.Rows(intRow)

                Dim obj_Periodo As HiddenField = CType(row.FindControl("hdnPeriodo"), HiddenField)
                Call ExportaPDF(hdnCodEmp.Value, obj_Periodo.Value, txtRut.Text, hdnCodSucursal.Value, 4, 0, "")
            End If

        Catch ex As Exception
            MessageBoxError("grdResumen_RowCommand", Err, Page, Master)
        End Try
    End Sub
    Private Sub grdRango_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdRango.PageIndexChanging
        Try
            grdRango.PageIndex = e.NewPageIndex

            Call CargarGrillaRango()

        Catch ex As Exception
            MessageBoxError("grdResumen_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdRango_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdRango.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdRango.Rows(intRow)

                Dim obj_Periodo As HiddenField = CType(row.FindControl("hdnPeriodo"), HiddenField)
                Call ExportaPDF(hdnCodEmp.Value, obj_Periodo.Value, txtRut.Text, hdnCodSucursal.Value, 4, 0, "")
            End If

        Catch ex As Exception
            MessageBoxError("grdResumen_RowCommand", Err, Page, Master)
        End Try
    End Sub
    Private Sub grdResumen_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdResumen.PageIndexChanging
        Try
            grdResumen.PageIndex = e.NewPageIndex

            Call CargarGrillaResumen()

        Catch ex As Exception
            MessageBoxError("grdResumen_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub ExportaPDF(ByVal empresa As Integer, ByVal periodo As Integer, ByVal rut As String, ByVal sucursal As String, ByVal tipobusqueda As Integer, ByVal tipotrab As Integer, ByVal depto As String)
        Dim paramList As New Generic.List(Of ReportParameter)

        Dim nomArchivo As String = "Liquidacion_" & lblNombre.Text & "_" & periodo

        If hdnCodEmp.Value = 3 Then ' STARTEC

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldo"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 32 Then 'Full Renta

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoFR"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 26 Then ' Puerto Seco

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoPS"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If


        If hdnCodEmp.Value = 12 Then ' ESEX

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoSX"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 13 Then ' DITRANS

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoDT"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 14 Then ' AGRICOLA

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoAGR"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 22 Then ' LOGTEC

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoLOGTEC"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If

        If hdnCodEmp.Value = 1 Then ' JTSA

            rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoJTSA"
            paramList.Add(New ReportParameter("Empresa", empresa, False))
            paramList.Add(New ReportParameter("periodo", periodo, False))
            paramList.Add(New ReportParameter("Rut", rut, False))
            paramList.Add(New ReportParameter("Sucursal", sucursal, False))
            paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
            paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
            paramList.Add(New ReportParameter("departamento", depto, False))
            paramList.Add(New ReportParameter("busqueda", Session("cod_usu_tc").ToString, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        End If


        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty

        Dim bytes As Byte() = rptVisor.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=" & nomArchivo & "." & extension)
        Response.BinaryWrite(bytes)
        Response.Flush()

    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub TxtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnExportar.Enabled = True
                Call ValidarPermisosDeptos(txtRut.Text)
                Call DatosPersona(txtRut.Text)
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnExportar.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnExportar.Enabled = True
                Call ValidarPermisosDeptos(txtRut.Text)
                Call DatosPersona(txtRut.Text)
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnExportar.Enabled = False
            End If
        End If
    End Sub

    Public Sub ValidarPermisosDeptos(ByVal Rut As String)

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & Rut & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnExportar.Enabled = True
                Else
                    MessageBox("Permisos", "No puede editar liquidacion de esta persona", Page, Master, "W")
                    btnExportar.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Protected Sub btnFiltroGeneral_Click(sender As Object, e As EventArgs) Handles btnFiltroGeneral.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnFiltroNomina_Click(sender As Object, e As EventArgs) Handles btnFiltroNomina.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub txtRutRango_TextChanged(sender As Object, e As EventArgs) Handles txtRutRango.TextChanged
        txtRutRango.Text = Replace(txtRutRango.Text, ".", "")
        txtRutRango.Text = Replace(txtRutRango.Text, "-", "")
        If Len(txtRutRango.Text) = 8 Then
            txtRutRango.Text = Left(txtRutRango.Text, 7) + "-" + Right(txtRutRango.Text, 1)
        ElseIf Len(txtRutRango.Text) = 9 Then
            txtRutRango.Text = Left(txtRutRango.Text, 8) + "-" + Right(txtRutRango.Text, 1)
        End If

        If Len(txtRutRango.Text) = 10 Then
            txtRutRango.Text = txtRutRango.Text
            If ValidarRut(txtRutRango) = True Then
                btnExportar.Enabled = True
                Call ValidarPermisosDeptos(txtRutRango.Text)
                Call DatosPersona(txtRutRango.Text)
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnExportar.Enabled = False
            End If
        ElseIf Len(txtRutRango.Text) = 9 Then
            txtRutRango.Text = "0" + txtRutRango.Text
            If ValidarRut(txtRutRango) = True Then
                btnExportar.Enabled = True
                Call ValidarPermisosDeptos(txtRutRango.Text)
                Call DatosPersona(txtRutRango.Text)
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnExportar.Enabled = False
            End If
        End If
    End Sub

    Protected Sub cboAnio1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio1.SelectedIndexChanged
        Call CargarPeriodo1()
    End Sub

    Protected Sub cboPeriodo2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo2.SelectedIndexChanged

    End Sub

    Protected Sub btnExportarRango_Click(sender As Object, e As EventArgs) Handles btnExportarRango.Click
        'Dim i As Integer = 0
        'Dim j As Integer = 0
        'Dim periodoPaso As String = ""
        'Dim mes As String = ""
        'j = cboPeriodo2.SelectedValue - cboPeriodo1.SelectedValue

        'For i = 0 To j

        '    If Right(cboPeriodo1.SelectedValue, 2) < 10 Then
        '        mes = Right(cboPeriodo1.SelectedValue, 2).ToString + i
        '        mes = "0" + mes.ToString
        '    Else
        '        mes = Right(cboPeriodo1.SelectedValue, 2).ToString + i
        '    End If
        '    'Else
        '    '    If Len(Right(cboPeriodo1.SelectedValue, 2)) < 2 Then
        '    '        mes = "0" + Right(cboPeriodo1.SelectedValue, 2) + i
        '    '    Else
        '    '        mes = Right(cboPeriodo1.SelectedValue, 2).ToString + i
        '    '    End If
        '    'End If

        '    periodoPaso = Left(cboPeriodo1.SelectedValue, 4) + mes.ToString
        '    Call ExportaPDF(hdnCodEmp.Value, periodoPaso, txtRutRango.Text, hdnCodSucursal.Value, 4, 0, "")
        '    i = i + 1
        'Next
        Try
            If txtRutRango.Text = "" Or cboPeriodo1.SelectedValue = 0 Or cboPeriodo2.SelectedValue = 0 Then
                MessageBox("Generar", "debe ingresar todos los datos para la busqueda", Page, Master, "W")
            Else
                Call CargarGrillaRango()
            End If

        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try


    End Sub



    Protected Sub cboAnio2_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles cboAnio2.SelectedIndexChanged
        Call CargarPeriodo2()
    End Sub
End Class