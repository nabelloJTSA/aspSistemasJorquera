﻿
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspCargarAsignacionVacaciones
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then

                lbltituloP.Text = "Cargar Asignación Vacaciones"
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub
    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                                New DataColumn("nombre", GetType(String)),
                                New DataColumn("num_periodo", GetType(String)),
                                New DataColumn("num_valor", GetType(String))
                                     })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut, nombre, num_periodo,num_valor FROM [Hoja1$] order by nombre", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFilaN.Item(2) = dtTemporal(i).Item(2)
                dtFilaN.Item(3) = dtTemporal(i).Item(3)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdDescuentos)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub
    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdDescuentos.Rows.Count - 1

                Dim objRut As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblRut"), Label)
                Dim objNombre As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblNombre"), Label)
                Dim objPeriodo As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblPeriodo"), Label)
                Dim objCuota As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblCuota"), Label)
                Dim objUf As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblUfDesc"), Label)
                Dim objDescuento As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblDescuento"), Label)
                Dim objValor As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblValor"), Label)

                Call GuardarAsignacion(objRut.Text, objNombre.Text, objPeriodo.Text, objValor.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Private Sub GuardarAsignacion(ByVal rut As String, ByVal nombre As String, ByVal periodo As String, ByVal valor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHTemp_asignacion_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(rut)

            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = Trim(periodo)
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = Trim(Replace(valor, ",", "."))
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarSocios", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdDescuentos.DataSource = MostrarExcel(_directorioParaGuardar)
            grdDescuentos.DataBind()
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub
End Class