﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPagoWEb.aspx.vb" Inherits="aspSistemasJorquera.aspPagoWEb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnRef" runat="server">Generar Nomina</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnGenerarRut" runat="server">Nominas por Rut</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnPWEspecial" runat="server">PW Especial</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnEERRGeneral" runat="server">Historial</asp:LinkButton>
        </li>
    </ul>
        <asp:HiddenField ID="hdnNomina" runat="server" />
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Pago Web"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Sueldo</label>
                            <div>
                                <asp:RadioButton ID="rdbSld" runat="server" GroupName="1" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Anticipo</label>
                            <div>
                                <asp:RadioButton ID="rdbAnt" runat="server" GroupName="1" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Nomina</label>
                            <div>
                                <asp:Label ID="lblNomina" runat="server"></asp:Label>
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Sucursal</label>
                            <div>
                                <asp:DropDownList ID="cboSucursal" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                       <%-- <div class="col-md-1 form-group">
                            <asp:Label ID="lblDepto" runat="server" Text="Depto" Visible="False"></asp:Label>
                            <div style="height: 5px" class="container"></div>
                            <div>
                                <asp:DropDownList ID="cboDepto" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="False"></asp:DropDownList>
                            </div>
                        </div>--%>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnProcesar" CssClass="btn bg-orange btn-block" runat="server">Generar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnProcesar" runat="server" />
                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresaHistorial" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdHistorial" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                        <ItemStyle HorizontalAlign="Center" Width="20px" />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Nomina">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomina" runat="server" Text='<%# Bind("id_nomina")%>' /><asp:HiddenField ID="hdn_tipo" runat="server" Value='<%# Bind("tip_nomina")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("num_periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sucursal">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSucursal" runat="server" Text='<%# Bind("Tipo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tipo Nomina">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tipo_nomina")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="70px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Eliminar">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEliminar" CommandName="1" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'><i class="far fa-trash-alt"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descargar">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDescargar" CommandName="2" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'><i class="fas fa-download"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:Panel ID="Panel2" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Sueldo</label>
                            <div>
                                <asp:RadioButton ID="rdbSldR" runat="server" GroupName="1" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Anticipo</label>
                            <div>
                                <asp:RadioButton ID="rdbAntr" runat="server" GroupName="1" />
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboanioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodoRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdRuts" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="HiddenField1" runat="server" />
            </asp:View>
            <asp:View ID="View4" runat="server">
                <asp:Panel ID="Panel3" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnio3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuEspecial" runat="server" CssClass="form-control text-uppercase" />
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargarEspecial" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGenerarEspecial" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdEspecial" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="valor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValor" runat="server" Text='<%# Bind("valor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                </asp:Panel>
            </asp:View>
        </asp:MultiView>
    </section>
</asp:Content>
