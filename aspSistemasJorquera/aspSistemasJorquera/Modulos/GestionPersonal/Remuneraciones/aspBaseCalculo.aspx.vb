﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspBaseCalculo
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call carga_combo_anio()
                Call carga_combo_periodo()
                Call RescatarUTM()
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarUTM()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_impuesto_unico 'RUTM', '" & Val(cboPeriodo.SelectedValue) & "' "
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblUTM.Text = rdoReader(0).ToString
                Else
                    lblUTM.Text = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescatarUTM", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
    End Sub

    Private Sub carga_combo_anio()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec proc_valores_mensuales 'CA'"
            cboAnio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio.DataTextField = "num_anio"
            cboAnio.DataValueField = "num_anio"
            cboAnio.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_anio", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_periodo()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec proc_valores_mensuales 'CP', '" & cboAnio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "num_periodo"
            cboPeriodo.DataValueField = "num_periodo"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_periodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "REMMae_impuesto_unico"
            Dim strSQL As String = "Exec pro_impuesto_unico 'G' , '" & cboPeriodo.SelectedValue & "'"
            grdImpuesto.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdImpuesto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub


    Private Sub actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_impuesto_unico"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@val_desde", SqlDbType.Float)
            comando.Parameters("@val_desde").Value = hdn_desde.Value
            comando.Parameters.Add("@val_hasta", SqlDbType.Float)
            comando.Parameters("@val_hasta").Value = hdn_hasta.Value
            comando.Parameters.Add("@num_factor", SqlDbType.Float)
            comando.Parameters("@num_factor").Value = hdn_factor.Value
            comando.Parameters.Add("@num_descuento", SqlDbType.Float)
            comando.Parameters("@num_descuento").Value = Replace(hdn_dcto.Value, ".", ",")
            comando.Parameters.Add("@id", SqlDbType.Int)
            comando.Parameters("@id").Value = hdn_id_imp.Value
            comando.Parameters.Add("@base_tope", SqlDbType.Float)
            comando.Parameters("@base_tope").Value = Replace(hdnBaseTope.Value, ".", ",")
            comando.Parameters.Add("@base_tasa", SqlDbType.Float)
            comando.Parameters("@base_tasa").Value = Replace(hdnBaseTasa.Value, ".", ",")
            comando.Parameters.Add("@base_descuento", SqlDbType.Float)
            comando.Parameters("@base_descuento").Value = Replace(hdnBaseDescuento.Value, ".", ",")
            comando.Parameters.Add("@tope_afp_salud", SqlDbType.Float)
            comando.Parameters("@tope_afp_salud").Value = Replace(hdnTopeAfpSalud.Value, ".", ",")
            comando.Parameters.Add("@tope_apf_antiguo", SqlDbType.Float)
            comando.Parameters("@tope_apf_antiguo").Value = Replace(hdnTopeAfpAnt.Value, ".", ",")
            comando.Parameters.Add("@tope_afc", SqlDbType.Float)
            comando.Parameters("@tope_afc").Value = Replace(hdnTopeAFC.Value, ".", ",")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()

            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")

            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("actualizar", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdImpuesto_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdImpuesto.PageIndexChanging
        Try
            grdImpuesto.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdMovTct_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdImpuesto_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdImpuesto.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdImpuesto.Rows(index)

                Dim hdnID_obj As HiddenField = CType(row.FindControl("hdnId"), HiddenField)
                Dim obj_desde As TextBox = CType(row.FindControl("txtDesd"), TextBox)
                Dim obj_hasta As TextBox = CType(row.FindControl("txtHast"), TextBox)
                Dim obj_factor As TextBox = CType(row.FindControl("txtFac"), TextBox)
                Dim obj_dcto As TextBox = CType(row.FindControl("txtDescu"), TextBox)

                Dim obj_BaseTope As TextBox = CType(row.FindControl("txtBaseTope"), TextBox)
                Dim obj_BaseTasa As TextBox = CType(row.FindControl("txtBaseTasa"), TextBox)
                Dim obj_BaseDcto As TextBox = CType(row.FindControl("txtBaseDesto"), TextBox)


                Dim obj_TopeAfpSalud As TextBox = CType(row.FindControl("txtTopeAFPSALUD"), TextBox)
                Dim obj_TopeAfpAnt As TextBox = CType(row.FindControl("txtTopeAFPAnt"), TextBox)
                Dim obj_TopeAFC As TextBox = CType(row.FindControl("txtTopeAFC"), TextBox)


                hdn_id_imp.Value = hdnID_obj.Value
                hdn_desde.Value = obj_desde.Text
                hdn_hasta.Value = obj_hasta.Text
                hdn_factor.Value = obj_factor.Text
                hdn_dcto.Value = obj_dcto.Text
                hdnBaseTope.Value = obj_BaseTope.Text
                hdnBaseTasa.Value = obj_BaseTasa.Text
                hdnBaseDescuento.Value = obj_BaseDcto.Text

                hdnTopeAfpSalud.Value = obj_TopeAfpSalud.Text
                hdnTopeAfpAnt.Value = obj_TopeAfpAnt.Text
                hdnTopeAFC.Value = obj_TopeAFC.Text


                Call actualizar()
                Call CargarGrilla()

            End If
        Catch ex As Exception
            MessageBoxError("grdImpuesto_RowCommand", Err, Page, Master)
        End Try
    End Sub


    Protected Sub btnCalcular_Click(sender As Object, e As EventArgs) Handles btnCalcular.Click

        If lblUTM.Text = "0" Then
            MessageBox("Calcular", "No existe valor UTM, favor ingrese valores en ventana Valores mensuales", Page, Master, "E")
        Else
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            Try
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_impuesto_unico"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "CAL"
                comando.Parameters.Add("@utm", SqlDbType.NVarChar)
                comando.Parameters("@utm").Value = Replace(lblUTM.Text, ".", ",")
                comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
                comando.Parameters("@num_periodo").Value = cboPeriodo.SelectedValue
                comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
                comando.Parameters("@user_add").Value = Session("id_usu_tc")
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()

                MessageBox("Calcular", "Calculo Realizado", Page, Master, "S")

                Call CargarGrilla()
                If grdImpuesto.Rows.Count <= 0 Then
                    grdImpuesto.Visible = False
                Else
                    grdImpuesto.Visible = True
                End If
            Catch ex As Exception
                MessageBoxError("btnCalcular_Click", Err, Page, Master)
            End Try
        End If
    End Sub

    Protected Sub cboAnio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedIndexChanged
        Call carga_combo_periodo()
        Call RescatarUTM()
        Call CargarGrilla()

        If grdImpuesto.Rows.Count <= 0 Then
            grdImpuesto.Visible = False
        Else
            grdImpuesto.Visible = True
        End If
    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged
        Call RescatarUTM()
        Call CargarGrilla()

        If grdImpuesto.Rows.Count <= 0 Then
            grdImpuesto.Visible = False
        Else
            grdImpuesto.Visible = True
        End If
    End Sub

    Protected Sub grdImpuesto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdImpuesto.SelectedIndexChanged

    End Sub
End Class