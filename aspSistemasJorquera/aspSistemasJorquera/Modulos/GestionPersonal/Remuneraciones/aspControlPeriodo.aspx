﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspControlPeriodo.aspx.vb" Inherits="aspSistemasJorquera.aspControlPeriodo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Abrir/Cerrar Periodo Rem"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">

        <asp:Panel ID="pnlDatos" runat="server">
            <div class="row justify-content-center table-responsive">

                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Empresa</label>
                    <div>
                        <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Año</label>
                    <div>
                        <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">Periodo</label>
                    <div>
                        <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-5 form-group">
                    <asp:Label ID="lblDepto" runat="server" Text="Abrir/Cerrar" Font-Bold="True"></asp:Label>
                           <div style="height: 5px; width:5px" class="container"></div>
                    <div>
                        <asp:CheckBox ID="chkActivo" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row justify-content-center table-responsive">

                <div class="col-md-5 form-group">
                    <div>
                    </div>
                </div>
                <div class="col-md-1 form-group">
                    <label for="fname" class="control-label col-form-label">&nbsp</label>
                    <div>
                        <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center table-responsive">

                <div class="col-md-4">

                    <asp:GridView ID="grdPeriodos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                        <Columns>
                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                            </asp:CommandField>

                            <asp:TemplateField HeaderText="Empresa">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmp" runat="server" Text='<%# Bind("empresa")%>' />
                                    <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_periodo")%>' />
                                    <asp:HiddenField ID="hdn_anio" runat="server" Value='<%# Bind("anio")%>' />
                                    <asp:HiddenField ID="hdn_emp" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                    <asp:HiddenField ID="hdn_estado" runat="server" Value='<%# Bind("est_periodo")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Periodo">
                                <ItemTemplate>
                                    <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("num_periodo")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("estado")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                    </asp:GridView>

                </div>

            </div>
        </asp:Panel>
        <asp:HiddenField ID="hdnActivo" runat="server" />
        <asp:HiddenField ID="hdnId" runat="server" />
    </section>
</asp:Content>
