﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb
Public Class aspConsolidadoProduccion
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
                GenerarReporte()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Protected Sub GenerarReporte()
        Dim paramList As New Generic.List(Of ReportParameter)
        Try
            rptVisor.Visible = True
            rptVisor.ServerReport.ReportPath = "/SGT/rptProduccionMsoft"
            rptVisor.ServerReport.Refresh()

        Catch ex As Exception
            MessageBox("GenerarReporte", "GenerarReporte", Page, Master, "E")
        End Try
    End Sub
End Class