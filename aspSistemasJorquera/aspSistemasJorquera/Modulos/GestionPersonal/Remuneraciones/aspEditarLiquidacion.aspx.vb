﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspEditarLiquidacion
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "Procesar"
                Call CargarAnio()
                Call CargarPeriodo()
                MultiView1.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub DatosPersona()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_editar_liquidacion 'CPE', '" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "' "
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(1).ToString
                    hdnCodEmp.Value = rdoReader(2).ToString
                    lblEmpresa.Text = rdoReader(3).ToString
                    lblSucursal.Text = rdoReader(7).ToString
                    hdnCodCargo.Value = rdoReader(4).ToString
                    lblInstrumento.Text = rdoReader(8).ToString
                    hdnCodInstrumento.Value = rdoReader(9).ToString
                    lblCargo.Text = rdoReader(5).ToString
                    hdnCodSucursal.Value = rdoReader(6).ToString
                    hdnSueldoBase.Value = rdoReader(10).ToString

                    lblSueldoBase.Text = rdoReader(10).ToString
                    lblAncipo.Text = rdoReader(11).ToString
                    lblCar.Visible = True
                    lblNom.Visible = True
                    lblSuc.Visible = True
                    lblEmp.Visible = True
                    lblIC.Visible = True
                    lblSB.Visible = True
                    lblMA.Visible = True
                    Call CargarGrilla()
                    Call CargarGrillaDescuentos()
                    Call CargarGrillaDescuentosLegales()
                    pnlDet.Visible = True

                Else
                    MessageBox("Rut", "No existen liquidaciones en el periodo indicado", Page, Master, "W")
                    lblNombre.Text = ""
                    lblEmpresa.Text = ""
                    lblSucursal.Text = ""
                    lblCargo.Text = ""
                    lblInstrumento.Text = ""
                    lblSueldoBase.Text = ""
                    lblAncipo.Text = ""
                    lblIC.Visible = False
                    lblCar.Visible = False
                    lblNom.Visible = False
                    lblSuc.Visible = False
                    lblEmp.Visible = False
                    pnlDet.Visible = False
                    lblSB.Visible = False
                    lblMA.Visible = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("DatosPersona", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub Limpiar()
        lblNombre.Text = ""
        lblEmpresa.Text = ""
        lblSucursal.Text = ""
        lblCargo.Text = ""
        lblCar.Visible = False
        lblNom.Visible = False
        lblSuc.Visible = False
        lblEmp.Visible = False
        pnlDet.Visible = False
    End Sub
    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHcab_liquidacion"
            Dim strSQL As String = "Exec mant_RRHH_editar_liquidacion 'CHL', '" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "' "
            grdHaberes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHaberes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaDescuentos()
        Try
            Dim strNomTabla As String = "RRHHcab_liquidacion"
            Dim strSQL As String = "Exec mant_RRHH_editar_liquidacion 'COD', '" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "' "
            grdDescuentos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdDescuentos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrillaDescuentosLegales()
        Try
            Dim strNomTabla As String = "RRHHcab_liquidacion"
            Dim strSQL As String = "Exec mant_RRHH_editar_liquidacion 'CDL', '" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "' "
            grdLegales.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdLegales.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call DatosPersona()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call DatosPersona()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call Limpiar()
            End If
        End If
    End Sub

    Private Sub ActualizarLiquidacion(ByVal id_det_item As String, ByVal num_valor As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_editar_liquidacion"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id_det_item
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("id_usu_tc")

            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            If num_valor.Contains(".") Then
                num_valor = Replace(num_valor, ".", "")
            End If
            If num_valor.Contains(",") Then
                num_valor = Replace(num_valor, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = num_valor
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarLiquidacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarOtrosDescuentos(ByVal id_det_item As String, ByVal num_valor As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_editar_liquidacion"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UOD"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id_det_item
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("id_usu_tc")

            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            If num_valor.Contains(".") Then
                num_valor = Replace(num_valor, ".", "")
            End If
            If num_valor.Contains(",") Then
                num_valor = Replace(num_valor, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = num_valor
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarOtrosDescuentos", Err, Page, Master)
        End Try
    End Sub


    Private Sub ActualizarDescuentosLegales()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_editar_liquidacion"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ADL"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = txtRut.Text
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = cboPeriodo.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDescuentosLegales", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

            For Each row As GridViewRow In grdHaberes.Rows
                Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                Call ActualizarLiquidacion(obj_IdItem.Value, obj_numvalor.Text)

            Next
            Call ActualizarDescuentosLegales()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")

        Catch ex As Exception
            MessageBoxError("btnGuardar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
        Call DatosPersona()
    End Sub

    Protected Sub btnRef_Click(sender As Object, e As EventArgs) Handles btnRef.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnEERRGeneral_Click(sender As Object, e As EventArgs) Handles btnEERRGeneral.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnGuardarOtro_Click(sender As Object, e As EventArgs) Handles btnGuardarOtro.Click
        Try

            For Each row As GridViewRow In grdDescuentos.Rows
                Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)

                Call ActualizarOtrosDescuentos(obj_IdItem.Value, obj_numvalor.Text)

            Next

            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")

        Catch ex As Exception
            MessageBoxError("btnGuardarOtro_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCancelarOtro_Click(sender As Object, e As EventArgs) Handles btnCancelarOtro.Click
        Call Limpiar()
        Call DatosPersona()
    End Sub

    Private Sub EliminarDescuento()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_editar_liquidacion"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ADL"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = txtRut.Text
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = cboPeriodo.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDescuentosLegales", Err, Page, Master)
        End Try
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs)

    End Sub


    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnBuscar.Enabled = True
                Else
                    MessageBox("Permisos", "No puede editar liquidacion de esta persona", Page, Master, "W")
                    btnBuscar.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnBuscar.Enabled = True
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnBuscar.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnBuscar.Enabled = True
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnBuscar.Enabled = False
            End If
        End If
    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub LinkButton1_Click1(sender As Object, e As EventArgs) Handles LinkButton1.Click
        MultiView1.ActiveViewIndex = 2
    End Sub
    Private Sub ActualizarDescuentoLegales(ByVal id_det_item As String, ByVal num_valor As String, ByVal por As String, ByVal des As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_editar_liquidacion"
            comando.CommandTimeout = 90000
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UDL"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = id_det_item
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("id_usu_tc")
            comando.Parameters.Add("@des_descuento", SqlDbType.NVarChar)
            comando.Parameters("@des_descuento").Value = des
            comando.Parameters.Add("@des_por", SqlDbType.NVarChar)
            If por.Contains(".") Then
                por = Replace(por, ".", "")
            End If
            If por.Contains(",") Then
                por = Replace(por, ",", ".")
            End If
            comando.Parameters("@des_por").Value = por
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            If num_valor.Contains(".") Then
                num_valor = Replace(num_valor, ".", "")
            End If
            If num_valor.Contains(",") Then
                num_valor = Replace(num_valor, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = num_valor
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarDescuentoLegales", Err, Page, Master)
        End Try
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try

            For Each row As GridViewRow In grdLegales.Rows
                Dim obj_IdItem As HiddenField = CType(row.FindControl("hdnIdItem"), HiddenField)
                Dim obj_numvalor As TextBox = CType(row.FindControl("txtMonto"), TextBox)
                Dim obj_des As TextBox = CType(row.FindControl("lblNomItem"), TextBox)
                Dim obj_por As TextBox = CType(row.FindControl("lblPorDesc"), TextBox)

                Call ActualizarDescuentoLegales(obj_IdItem.Value, obj_numvalor.Text, obj_por.Text, obj_des.Text)
                Call Limpiar()
                Call DatosPersona()
            Next

            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")

        Catch ex As Exception
            MessageBoxError("btnGuardarOtro_Click", Err, Page, Master)
        End Try
    End Sub
End Class