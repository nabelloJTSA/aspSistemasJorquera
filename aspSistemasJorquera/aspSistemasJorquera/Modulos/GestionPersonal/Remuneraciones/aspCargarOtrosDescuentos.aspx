﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCargarOtrosDescuentos.aspx.vb" Inherits="aspSistemasJorquera.aspCargarOtrosDescuentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnCargarOtro" runat="server">Cargar Desc.</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnEliminar" runat="server">Eliminar Desc.</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnENomina" runat="server">Eliminar Nomina</asp:LinkButton>
        </li>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase"/>
                            </div>
                        </div>

                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnGuardar" CssClass="btn bg-red btn-block" runat="server">Guardar</asp:LinkButton>
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdDescuentos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("num_periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cuota">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCuota" runat="server" Text='<%# Bind("des_cuota")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UF. Desc">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUfDesc" runat="server" Text='<%# Bind("des_uf")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descuento">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescuento" runat="server" Text='<%# Bind("des_descuento")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Valor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValor" runat="server" Text='<%# Bind("num_valor")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeridoRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:Label ID="lblNombre" runat="server" Font-Bold="True"></asp:Label>
                            </div>
                        </div>                    
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3">

                            <asp:GridView ID="grdEliminarDescuentos" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("des_descuento")%>' />
                                            <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_descuento")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Monto">
                                        <ItemTemplate>
                                            <asp:Label ID="txtMonto" runat="server" Text='<%# Bind("num_valor", "{0:####,###,###,###,##.##}")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Eliminar">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnEliminar" CommandName="btneliminar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:Panel ID="Panel2" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Descuento</label>
                            <div>
                                <asp:DropDownList ID="cboDescuentos" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Archivo</label>
                            <div>
                                <asp:FileUpload ID="fuNomina" runat="server"  CssClass="form-control text-uppercase"/>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnCargarNomina" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="col-md-1 form-group ">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnEliminarNomina" CssClass="btn bg-red btn-block" runat="server">Eliminar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5">

                            <asp:GridView ID="grdNominaDesc" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                  <%--  <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Periodo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("periodo")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
        </asp:MultiView>
    </section>
    <asp:HiddenField ID="hdnProcesar" runat="server" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="HiddenField2" runat="server" />
</asp:Content>
