﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspDiasAdicionales
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarEmpresa()
                Call CargarSucursal()
                Call CargarAnio()
                Call CargarPeriodo()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSucursal()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCS', '" & cboEmpresa.SelectedValue & "'"
            cboSucursal.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSucursal.DataTextField = "nom_sucursal"
            cboSucursal.DataValueField = "id_sucursal"
            cboSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSucursal", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_jornada"
            Dim strSQL As String = "Exec mant_RRHH_dias_adicionales 'G','" & cboEmpresa.SelectedValue & "', '" & cboSucursal.SelectedValue & "', '" & cboPeriodo.SelectedValue & "'"
            grdProduccion.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdProduccion.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_dias_adicionales"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@cod_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@cod_sucursal").Value = cboSucursal.SelectedValue
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@num_valor", SqlDbType.Float)
            Dim monto As Double = txtMonto.Text
            If txtMonto.Text.Contains(".") Then
                monto = Replace(txtMonto.Text, ".", "")
            End If
            If txtMonto.Text.Contains(",") Then
                monto = Replace(txtMonto.Text, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = monto
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = txtRut.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtRut.Text = ""
        cboSucursal.ClearSelection()
        cboPeriodo.ClearSelection()
        cboEmpresa.ClearSelection()
        txtMonto.Text = ""
        Call CargarGrilla()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If hdnactivo.Value = 0 Then
            If cboEmpresa.SelectedValue = 0 Or cboPeriodo.SelectedValue = 0 Or cboSucursal.SelectedValue = 0 _
                Or txtRut.Text = "" Or txtMonto.Text = "" Then
                MessageBox("Guardar", "Debe completar todos los campos", Page, Master, "W")
            Else
                Call Guardar()
            End If
        Else
            Call Actualizar()
        End If
    End Sub
    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_dias_adicionales"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@id_dias", SqlDbType.NVarChar)
            comando.Parameters("@id_dias").Value = hdnID.Value
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@cod_sucursal", SqlDbType.NVarChar)
            comando.Parameters("@cod_sucursal").Value = cboSucursal.SelectedValue
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@num_valor", SqlDbType.Float)
            Dim monto As Double = txtMonto.Text
            If txtMonto.Text.Contains(".") Then
                monto = Replace(txtMonto.Text, ".", "")
            End If
            If txtMonto.Text.Contains(",") Then
                monto = Replace(txtMonto.Text, ",", ".")
            End If
            comando.Parameters("@num_valor").Value = monto
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = txtRut.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Modificado", Page, Master, "S")
            Call CargarGrilla()
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarSucursal()
        CargarGrilla()
    End Sub

    Protected Sub cboSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSucursal.SelectedIndexChanged
        CargarGrilla()
    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged
        CargarGrilla()
    End Sub

    Protected Sub grdProduccion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProduccion.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdProduccion.SelectedRow

            hdnID.Value = CType(row.FindControl("hdn_id"), HiddenField).Value
            cboEmpresa.SelectedValue = CType(row.FindControl("hdnempresa"), HiddenField).Value
            cboSucursal.SelectedValue = CType(row.FindControl("hdnsucursal"), HiddenField).Value
            cboPeriodo.SelectedValue = CType(row.FindControl("lblPeriodo"), Label).Text
            txtRut.Text = CType(row.FindControl("hdntipo"), HiddenField).Value
            txtMonto.Text = CType(row.FindControl("lblTipo"), Label).Text

            hdnactivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdProduccion_PageIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdProduccion_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdProduccion.PageIndexChanging
        Try
            grdProduccion.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdProduccion_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
        End If
    End Sub
    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub
End Class