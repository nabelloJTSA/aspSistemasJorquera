﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspPagoLeyesSociales.aspx.vb" Inherits="aspSistemasJorquera.aspPagoLeyesSociales" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Pago Leyes Sociales"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlCabSue" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>                       
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnExportar" CssClass="btn bg-red btn-block" runat="server">Generar Reporte</asp:LinkButton>

                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </asp:View>
        </asp:MultiView>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-10">

                <asp:Panel ID="pnlReporte" runat="server" Style="overflow: auto;" Height="650px" Width="100%" Visible="true">
                    <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana" Visible="true"
                        waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                    </rsreport:ReportViewer>
                </asp:Panel>
            </div>
        </div>
    </section>
</asp:Content>
