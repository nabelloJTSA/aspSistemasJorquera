﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspCentSueldos.aspx.vb" Inherits="aspSistemasJorquera.aspCentSueldos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnCarga" runat="server">Cargar Archivo</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnTraspaso" runat="server">Traspasar</asp:LinkButton>
        </li>
    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="mtvCentralizacion" runat="server">
            <asp:View ID="vwIngresar" runat="server">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">

                        <asp:Label ID="lblPersonal" runat="server" Text="Personal" Visible="false" Font-Bold="true" CssClass="control-label col-form-label"></asp:Label>
                        <div style="height: 5px" class="container"></div>
                        <div>
                            <asp:DropDownList ID="cboPersonal" runat="server" CssClass="form-control text-uppercase" Visible="false" AutoCompleteType="Disabled" AutoPostBack="True">
                                <asp:ListItem Value="C">Conductor</asp:ListItem>
                                <asp:ListItem Value="NC">NO Conductor</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Archivo</label>
                        <div>
                            <asp:FileUpload ID="uplArchivo" runat="server" CssClass="form-control text-uppercase" />
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <asp:Label ID="lblNom" runat="server" Text="Nombre:" Visible="False" Font-Bold="true">Actualizar</asp:Label>
                         <div>
                         </div>
                        <div>
                             <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <asp:CheckBox ID="chkActualizacion" runat="server" CssClass="custom-checkbox btn-sm" ToolTip="Permite actualizar los registros por el documento cargado" Enabled="False" AutoPostBack="True" />
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-4 form-group ">
                        <div>
                        </div>
                    </div>


                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnSubir" CssClass="btn bg-orange btn-block" runat="server">Subir Dcto.</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnValidar" CssClass="btn bg-orange btn-block" runat="server">Validar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Limpiar</asp:LinkButton>
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grdCarga" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Cod Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCodEmpresa" runat="server" Text='<%# Bind("cod_empresa")%>' Width="80px" />
                                        <asp:HiddenField ID="hdnEstado" runat="server" Value='<%# Bind("estado")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nomEmpresa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="450px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Periodo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPeriodo" runat="server" Text='<%# Bind("periodo")%>' Width="80px" />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Carga">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaProceso" runat="server" Text='<%# Bind("fechaProceso")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Validación">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaValidacion" runat="server" Text='<%# Bind("fechaValidacion")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnAdd" runat="server" CommandName="btnSArchivo" ImageUrl="~/imagen/imgAdd.png" Enabled="false" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                        </ContentTemplate>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="20" Wrap="False" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>
            </asp:View>
            <asp:View ID="vwEventosMasivos" runat="server">
                <div class="row justify-content-center table-responsive">
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaDesc" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <asp:Label ID="lblPersonalDesc" runat="server" Text="Personal" Visible="false" Font-Bold="true"></asp:Label>
                          <div style="height: 5px" class="container"></div>
                        <div>
                            <asp:DropDownList ID="cboPersonalDesc" runat="server" CssClass="form-control text-uppercase" Visible="false" AutoCompleteType="Disabled" AutoPostBack="True">
                                <asp:ListItem Value="C">Conductor</asp:ListItem>
                                <asp:ListItem Value="NC">NO Conductor</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoDesc" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-4 form-group ">
                        <div>
                        </div>
                    </div>


                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="btnTraspasar" CssClass="btn bg-orange btn-block" runat="server">Generar Traspaso</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group ">
                        <div>
                            <asp:LinkButton ID="Button3" CssClass="btn bg-red btn-block" runat="server">Limpiar</asp:LinkButton>
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grDdescarga" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="15" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Cod Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCodEmpresaDesc" runat="server" Text='<%# Bind("cod_empresa")%>' Width="80px" />
                                        <asp:HiddenField ID="hdnEstadoDesc" runat="server" Value='<%# Bind("estado")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Empresa">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpresaDesc" runat="server" Text='<%# Bind("nomEmpresa")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="450px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Periodo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPeriodoDesc" runat="server" Text='<%# Bind("periodo")%>' Width="80px" />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Carga">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaProcesoDesc" runat="server" Text='<%# Bind("fechaProceso")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Fecha Validación">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFechaValidacionDesc" runat="server" Text='<%# Bind("fechaValidacion")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Validar" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnAddDesc" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="btnSArchivo" ImageUrl="~/imagen/imgAdd.png" />
                                        </ContentTemplate>
                                                       
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" VerticalAlign="Middle" Width="50" Wrap="False" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>

                    </div>

                </div>
            </asp:View>
        </asp:MultiView>
    </section>
    <asp:HiddenField ID="hdnEstado" runat="server" />
</asp:Content>
