﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspValoresMensuales
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call carga_combo_tipo()
                Call carga_combo_anio()
                cboAnio.SelectedValue = Year(Date.Now.Date)
                Call carga_combo_periodo()
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try

    End Sub

    Private Sub carga_combo_tipo()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec proc_valores_mensuales 'CT'"
            cboTipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipo.DataTextField = "nombre_valor"
            cboTipo.DataValueField = "id_valor"
            cboTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_tipo", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_periodo()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec proc_valores_mensuales 'CP', '" & cboAnio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "num_periodo"
            cboPeriodo.DataValueField = "num_periodo"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_periodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_anio()
        Try
            Dim strNomTablaR As String = "REMMae_valores"
            Dim strSQLR As String = "Exec proc_valores_mensuales 'CA'"
            cboAnio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio.DataTextField = "num_anio"
            cboAnio.DataValueField = "num_anio"
            cboAnio.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_anio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_valores_mensuales"
            Dim strSQL As String = "Exec proc_valores_mensuales 'GA', '" & cboTipo.SelectedValue & "','" & cboAnio.SelectedValue & "'"
            grdValores.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdValores.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        txtValor.Text = ""
        cboTipo.ClearSelection()
        cboPeriodo.ClearSelection()
        hdnActivo.Value = 0
        Call CargarGrilla()
    End Sub

    Private Sub guardar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_valores_mensuales"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@id_valor", SqlDbType.NVarChar)
            comando.Parameters("@id_valor").Value = cboTipo.SelectedValue
            comando.Parameters.Add("@cod_periodo", SqlDbType.NVarChar)
            comando.Parameters("@cod_periodo").Value = cboPeriodo.SelectedItem.ToString
            comando.Parameters.Add("@valor_mensual", SqlDbType.NVarChar)
            comando.Parameters("@valor_mensual").Value = Replace(txtValor.Text, ",", ".")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            Call Limpiar()

            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("guardar_area", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdValores_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdValores.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdValores.Rows(intRow)

        If Trim(LCase(e.CommandName)) = "1" Then
            hdnId.Value = CType(row.FindControl("hdn_id"), HiddenField).Value
            Call eliminar()
            MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
            Call CargarGrilla()
        End If
    End Sub

    Private Sub actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_valores_mensuales"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@valor_mensual", SqlDbType.NVarChar)
            comando.Parameters("@valor_mensual").Value = Replace(txtValor.Text, ",", ".")
            comando.Parameters.Add("@cod_periodo", SqlDbType.NVarChar)
            comando.Parameters("@cod_periodo").Value = cboPeriodo.SelectedItem.ToString
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            MessageBox("Actualizar", "Registro Actualizado Exitosamente", Page, Master, "S")

            Call Limpiar()
            Call CargarGrilla()

        Catch ex As Exception
            MessageBoxError("actualizar_area", Err, Page, Master)
        End Try
    End Sub

    Private Sub eliminar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_valores_mensuales"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnId.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
            Call Limpiar()
            Call CargarGrilla()

        Catch ex As Exception
            MessageBoxError("eliminar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Private Sub grdValores_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdValores.PageIndexChanging
        Try
            grdValores.PageIndex = e.NewPageIndex
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("grdValores_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdValore_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdValores.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdValores.SelectedRow
            Dim hdnid_obj As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim hdnidTipo_obj As HiddenField = CType(row.FindControl("hdnIdTipo"), HiddenField)
            Dim hdnCodPer_obj As HiddenField = CType(row.FindControl("hdnCodPeriodo"), HiddenField)

            Dim valor_obj As Label = CType(row.FindControl("lblValor"), Label)

            hdnId.Value = hdnid_obj.Value
            cboTipo.SelectedValue = hdnidTipo_obj.Value

            cboPeriodo.SelectedValue = hdnCodPer_obj.Value

            txtValor.Text = Replace(valor_obj.Text, ",", ".")

            hdnActivo.Value = 1

        Catch ex As Exception
            MessageBoxError("grdValore_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtValor.Text = "" Or cboTipo.SelectedValue = 0 Or cboPeriodo.SelectedValue = 0 Then
            MessageBox("Guardar", "Debe ingresar datos", Page, Me, "W")
        Else
            If hdnActivo.Value = 0 Then
                Call guardar()
            Else
                Call actualizar()
            End If
        End If
    End Sub



    Protected Sub cboAnio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedIndexChanged
        Call carga_combo_periodo()
        Call CargarGrilla()
    End Sub

    Protected Sub cboTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipo.SelectedIndexChanged
        Call CargarGrilla()
    End Sub

End Class