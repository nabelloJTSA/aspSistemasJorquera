﻿Imports System
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Owin
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Web.UI.HtmlControls
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.IO
Imports System.Data.OleDb
Public Class aspCentSueldos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then
                mtvCentralizacion.ActiveViewIndex = 0
                Call CargarEmpresa()
                Call CargarPeriodos()
                Call CargarEmpresaDesc()
                Call CargarPeriodosDesc()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarEstadoTraspaso()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "pro_CentraSueldos 'REV','','', '" & cboEmpresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = True Then
                        chkActualizacion.Enabled = False
                    Else
                        chkActualizacion.Enabled = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("RescatarEstadoTraspaso", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub RescatarEstadoTraspasoJTSA()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "pro_CentraSueldos 'REVJ','','', '" & cboEmpresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "', '' , '" & cboPersonal.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = True Then
                        chkActualizacion.Enabled = False
                    Else
                        chkActualizacion.Enabled = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("RescatarEstadoTraspasoJTSA", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub RescatarEstadoTraspasoDesc()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "pro_CentraSueldos 'REV','','', '" & cboEmpresaDesc.SelectedValue & "', '" & cboPeriodoDesc.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = True Then
                        btnTraspasar.Enabled = False
                    Else
                        btnTraspasar.Enabled = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("RescatarEstadoTraspaso", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub RescatarEstadoTraspasoJTSADesc()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "pro_CentraSueldos 'REVJ','','', '" & cboEmpresaDesc.SelectedValue & "', '" & cboPeriodoDesc.SelectedValue & "', '' , '" & cboPersonalDesc.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = True Then
                        btnTraspasar.Enabled = False
                    Else
                        btnTraspasar.Enabled = True
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("RescatarEstadoTraspasoJTSA", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub


    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "cont_periodos"
            Dim strSQLR As String = "pro_CentraSueldos 'SE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodos()
        Try
            Dim strNomTablaR As String = "cont_periodos"
            Dim strSQLR As String = "pro_CentraSueldos 'SP'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "num_periodo"
            cboPeriodo.DataValueField = "num_periodo"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnSubir_Click(sender As Object, e As EventArgs) Handles btnSubir.Click
        'If GuardarValidacion() = True Then

        'End If
        If chkActualizacion.Checked = False Then
            If cboEmpresa.SelectedValue <> 0 Then
                If uplArchivo.FileName <> "" Then
                    If CargarArchivo(uplArchivo, Me.Request) = True Then
                        If GuardarValidacion() = True Then
                            MessageBox("Subir Docto.", "Carga Exitosa, favor Validar Archivo", Page, Master, "S")
                            chkActualizacion.Checked = False
                            If cboPeriodo.SelectedValue <> "Seleccione" Then
                                If cboEmpresa.SelectedValue = 1 Then
                                    Call RescatarEstadoTraspasoJTSA()
                                Else
                                    Call RescatarEstadoTraspaso()
                                End If

                            End If
                            Call CargarGrilla()
                        End If
                    End If
                Else
                    MessageBox("Subir Docto.", "Debe Seleccionar Archivo", Page, Master, "W")
                End If
            Else
                MessageBox("Subir Docto.", "Seleccione Empresa", Page, Master, "W")
            End If
        Else
            If EliminarDatos() = True Then
                If cboEmpresa.SelectedValue <> 0 Then
                    If uplArchivo.FileName <> "" Then
                        If CargarArchivo(uplArchivo, Me.Request) = True Then
                            If GuardarValidacion() = True Then
                                Call CargarGrilla()
                                MessageBox("Subir Docto.", "Carga Exitosa, favor Validar Archivo", Page, Master, "S")
                                chkActualizacion.Checked = False
                                If cboPeriodo.SelectedValue <> "Seleccione" Then
                                    If cboEmpresa.SelectedValue = 1 Then
                                        Call RescatarEstadoTraspasoJTSA()
                                    Else
                                        Call RescatarEstadoTraspaso()
                                    End If

                                End If
                            End If
                        End If
                    Else
                        MessageBox("Subir Docto.", "Debe Seleccionar Archivo", Page, Master, "W")
                    End If
                Else
                    MessageBox("Subir Docto.", "Seleccione Empresa", Page, Master, "W")
                End If
            End If
        End If
    End Sub

    Private Function CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest) As Boolean
        Dim _carpeta As String = "C:\centralizacion\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath
        'Try
        If archivo.HasFile Then
            Dim _extension As String = Path.GetExtension(archivo.FileName)
            If ValidarExtension(_extension) Then
                Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                '----------------------Eliminar Archivos Existentes------------------------------
                Call EliminarArchivos()
                '-----------------------Guardar nuevo archivo------------------------------
                archivo.SaveAs(_directorioParaGuardar)
                If Cargar(_directorioParaGuardar, "centralizacion") = True Then
                    Return True
                Else
                    Return False
                End If
            Else
                MessageBox("Cargar Archivo", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
            End If
        End If
        'Catch ex As Exception
        '    MessageBoxError("cargar", Err, Page, Master)
        '    Return False
        'End Try
    End Function


    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\centralizacion\"
        Dim _directorioGral As String = _carpeta
        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub
    Function GuardarValidacion() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand

        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "INSV"

            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresa.SelectedValue

            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue

            comando.Parameters.Add("@Usuario", SqlDbType.NVarChar)
            comando.Parameters("@Usuario").Value = Session("id_usu_tc")

            comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
            comando.Parameters("@Dato").Value = cboPersonal.SelectedValue

            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("GuardarValidacion", Err, Page, Master)
            Return False
        End Try
    End Function

    Public Sub CargarGrilla()
        Dim valor As String
        If cboPeriodo.SelectedValue = "Seleccione" Then
            valor = "0"
        Else
            valor = cboPeriodo.SelectedValue
        End If

        Try
            Dim strNomTabla As String = "ContMov_centFullRenta"
            Dim strSQL As String = "Exec pro_CentraSueldos 'SCFUll','','','" & cboEmpresa.SelectedValue & "','" & valor & "','','" & cboPersonal.SelectedValue & "'"
            grdCarga.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCarga.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla ", Err, Page, Master)
        End Try
    End Sub

    Function EliminarDatos() As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand

        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ECENT"
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
            comando.Parameters("@Dato").Value = cboPersonal.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EliminarDatos", Err, Page, Master)
            Return False
        End Try
    End Function


    Private Function Cargar(ByVal ruta As String, ByVal sHoja As String) As Boolean
        Dim retorno As Boolean = True
        Dim cs As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)

        Try
            Dim dAdapter As New OleDbDataAdapter("Select * From [" & sHoja & "$]", cs)
            Dim datos As New DataSet
            dAdapter.Fill(datos)
            'Selecciona Columnas
            For Each cont As DataColumn In datos.Tables(0).Columns
                Call CrearColumnas("ICFR", Convert.ToString(cont), cboEmpresa.SelectedValue)
            Next
            'Guardar Filas
            For contFila = 0 To datos.Tables(0).Rows.Count - 1
                Call GuardarDatos("IDFR", Convert.ToString(datos.Tables(0).Rows(contFila)(0)), cboEmpresa.SelectedValue)
            Next
            'Carga Archivo Excel a tabla temporal
            Dim conx As New SqlConnection(strCnx)
            Dim sqlBc As SqlBulkCopy
            conx.Open()
            sqlBc = New SqlBulkCopy(conx)
            sqlBc.DestinationTableName = "Cont_temp"
            sqlBc.WriteToServer(datos.Tables(0))
            conx.Close()
            Call CrearColumnaPeriodo(cboPeriodo.SelectedValue)
            Return retorno
        Catch ex As Exception
            MessageBoxError("Cargar", Err, Page, Master)
            Return False
        End Try
    End Function

    Private Sub CrearColumnas(ByVal Tipo As String, ByVal Tabla As String, ByVal empresa As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        'Try
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "dbo.pro_CentraSueldos"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = Tipo
        comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
        comando.Parameters("@codEmpresa").Value = empresa
        comando.Parameters.Add("@columNueva", SqlDbType.NVarChar)
        comando.Parameters("@columNueva").Value = Tabla.Replace("&", "").Replace("/", "").Replace(" ", "").Replace("\", "").Replace(".", "").Replace(",", "").Replace("%", "").Replace("(", "").Replace(")", "").Replace("!", "").Replace("¿", "").Replace("'", "").Replace("""", "").Replace("#", "").Replace("@", "").Replace("*", "").Replace("!", "")
        comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
        comando.Parameters("@Dato").Value = cboPersonal.SelectedValue
        comando.CommandTimeout = 900000000
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
        'Catch ex As Exception
        '    MessageBoxError("CrearColumnas", Err, Page, Master)
        'End Try
    End Sub


    Private Sub GuardarDatos(ByVal Tipo As String, ByVal RutPersona As String, ByVal Empresa As String)

        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = Tipo
            comando.Parameters.Add("@rutPersona", SqlDbType.NVarChar)
            comando.Parameters("@rutPersona").Value = RutPersona
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = Empresa
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
            comando.Parameters("@Dato").Value = cboPersonal.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarDatos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CrearColumnaPeriodo(ByVal Periodo As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IPE"
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = Periodo
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
            comando.Parameters("@Dato").Value = cboPersonal.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("CrearColumnaPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged

        If cboEmpresa.SelectedValue = 0 Then
            grdCarga.Visible = False
        Else
            grdCarga.Visible = True
            If cboPeriodo.SelectedItem.ToString = "Seleccione" Then
            Else
                If cboEmpresa.SelectedValue = 1 Then
                    Call RescatarEstadoTraspasoJTSA()
                Else
                    Call RescatarEstadoTraspaso()
                End If
            End If

            If cboEmpresa.SelectedValue = 1 Then
                lblPersonal.Visible = True
                cboPersonal.Visible = True
                'grdCarga.DataBind()
                Call CargarGrilla()
            Else
                lblPersonal.Visible = False
                cboPersonal.Visible = False
                Call CargarGrilla()
            End If
        End If


        If grdCarga.Rows.Count = 0 Then
            btnSubir.Enabled = True
            btnValidar.Enabled = False
        Else
            btnSubir.Enabled = False
        End If

    End Sub

    Protected Sub cboPersonal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPersonal.SelectedIndexChanged

        If cboPeriodo.SelectedValue <> "Seleccione" Then
            If cboEmpresa.SelectedValue = 1 Then
                Call RescatarEstadoTraspasoJTSA()
            Else
                Call RescatarEstadoTraspaso()
            End If

        End If
        Call CargarGrilla()
        If grdCarga.Rows.Count = 0 Then
            btnSubir.Enabled = True
            btnValidar.Enabled = False
        Else
            btnSubir.Enabled = False
        End If
    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged
        Try
            If cboPeriodo.SelectedValue <> "Seleccione" Then
                If cboEmpresa.SelectedValue = 1 Then
                    Call RescatarEstadoTraspasoJTSA()
                Else
                    Call RescatarEstadoTraspaso()
                End If

            End If


            Call CargarGrilla()
            If grdCarga.Rows.Count = 0 Then
                btnSubir.Enabled = True
                btnValidar.Enabled = False
            Else
                btnSubir.Enabled = False
            End If
        Catch ex As Exception
            MessageBoxError("cboPeriodo_SelectedIndexChanged ", Err, Page, Master)
        End Try
    End Sub

    Protected Sub chkActualizacion_CheckedChanged(sender As Object, e As EventArgs) Handles chkActualizacion.CheckedChanged
        If chkActualizacion.Checked = True Then
            btnSubir.Enabled = True
        Else
            btnSubir.Enabled = False
        End If
    End Sub

    Protected Sub btnValidar_Click(sender As Object, e As EventArgs) Handles btnValidar.Click
        Try
            If EstadoValidacion() = True Then
                Call CargarGrilla()
                MessageBox("Validar", "Validacion Correcta, Favor Comunicar a Depto. de Contabilidad", Page, Master, "S")
            End If
        Catch ex As Exception
            MessageBoxError("btnValidar_Click", Err, Page, Master)
        End Try
    End Sub

    Function EstadoValidacion() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand

        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UPDE"
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue
            comando.Connection = conx
            comando.CommandTimeout = 900000000
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("EstadoValidacion", Err, Page, Master)
            Return False
        End Try
    End Function

    Private Sub Limpiar()
        btnValidar.Enabled = False
        chkActualizacion.Checked = False
        cboPersonal.ClearSelection()
        cboEmpresa.ClearSelection()
        grdCarga.DataBind()
        chkActualizacion.Checked = False
        chkActualizacion.Enabled = False
        lblPersonal.Visible = False
        cboPersonal.Visible = False
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub grdCarga_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCarga.SelectedIndexChanged

    End Sub

    Private Sub grdCarga_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCarga.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim img As ImageButton = CType(row.FindControl("btnAdd"), ImageButton)
            Dim estado As HiddenField = CType(row.FindControl("hdnEstado"), HiddenField)
            If estado.Value = False Then
                img.Visible = False
                btnSubir.Enabled = False
                btnValidar.Enabled = True
            End If
            If estado.Value = True Then
                img.Visible = True
                btnSubir.Enabled = False
                btnValidar.Enabled = False
            End If
        End If
    End Sub

    ' TRASPASO --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ' TRASPASO --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    Private Sub CargarEmpresaDesc()
        Try
            Dim strNomTablaR As String = "cont_periodos"
            Dim strSQLR As String = "pro_CentraSueldos 'SE'"
            cboEmpresaDesc.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaDesc.DataTextField = "nom_empresa"
            cboEmpresaDesc.DataValueField = "cod_empresa"
            cboEmpresaDesc.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresaDesc", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodosDesc()
        Try
            Dim strNomTablaR As String = "cont_periodos"
            Dim strSQLR As String = "pro_CentraSueldos 'SP'"
            cboPeriodoDesc.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoDesc.DataTextField = "num_periodo"
            cboPeriodoDesc.DataValueField = "num_periodo"
            cboPeriodoDesc.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodosDesc", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresaDesc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaDesc.SelectedIndexChanged
        Try
            If cboEmpresaDesc.SelectedValue = 0 Then
                cboPersonal.Visible = False
            Else

                If cboPeriodoDesc.SelectedValue <> "Seleccione" Then
                    If cboEmpresaDesc.SelectedValue = 1 Then
                        Call RescatarEstadoTraspasoJTSADesc()
                    Else
                        Call RescatarEstadoTraspasoDesc()
                    End If
                End If

                If cboEmpresaDesc.SelectedValue = 1 Then
                    cboPersonalDesc.Visible = True
                    lblPersonalDesc.Visible = True
                    grDdescarga.DataBind()
                Else
                    cboPersonalDesc.Visible = False
                    lblPersonalDesc.Visible = False
                    cboPersonal.Visible = False
                    Call CargarGillaDescarga()
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cboEmpresaDesc_SelectedIndexChanged ", Err, Page, Master)
        End Try
    End Sub


    Public Sub CargarGillaDescarga()
        Dim valor As String
        If cboPeriodoDesc.SelectedValue = "Seleccione" Then
            valor = "0"
        Else
            valor = cboPeriodoDesc.SelectedValue
        End If
        Try
            Dim strNomTabla As String = "ContMov_centFullRenta"
            Dim strSQL As String = "Exec [pro_CentraSueldos] 'SCFUll','','','" & cboEmpresaDesc.SelectedValue & "','" & valor & "','','" & cboPersonalDesc.SelectedValue & "'"
            grDdescarga.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grDdescarga.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGillaDescarga ", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboPeriodoDesc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoDesc.SelectedIndexChanged
        If cboPeriodoDesc.SelectedValue <> "Seleccione" Then
            If cboEmpresaDesc.SelectedValue = 1 Then
                Call RescatarEstadoTraspasoJTSADesc()
            Else
                Call RescatarEstadoTraspasoDesc()
            End If
        End If
        Call CargarGillaDescarga()
    End Sub

    Protected Sub cboPersonalDesc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPersonalDesc.SelectedIndexChanged
        If cboPeriodoDesc.SelectedValue <> "Seleccione" Then
            If cboEmpresaDesc.SelectedValue = 1 Then
                Call RescatarEstadoTraspasoJTSADesc()
            Else
                Call RescatarEstadoTraspasoDesc()
            End If
        End If
        Call CargarGillaDescarga()
    End Sub

    Protected Sub btnTraspasar_Click(sender As Object, e As EventArgs) Handles btnTraspasar.Click
        Try
            If cboEmpresaDesc.SelectedValue <> 0 Then
                If cboEmpresaDesc.SelectedValue <> 0 Then
                    If cboPeriodoDesc.SelectedItem.ToString <> "Seleccione" Then
                        If hdnEstado.Value = True Then
                            If TraspasoFin700() = True Then
                                If cboEmpresaDesc.SelectedValue = 1 Then
                                    Call ActualizarEstTraspasoJTSA()
                                Else
                                    Call ActualizarEstTraspaso()
                                End If

                                If cboPeriodoDesc.SelectedValue <> "Seleccione" Then
                                    If cboEmpresaDesc.SelectedValue = 1 Then
                                        Call RescatarEstadoTraspasoJTSADesc()
                                    Else
                                        Call RescatarEstadoTraspasoDesc()
                                    End If
                                End If
                                MessageBox("Traspasar", "Traspaso Correcto,Favor Comunicar a Depto. de Contabilidad", Page, Master, "S")
                            End If
                        Else
                            MessageBox("Traspasar", "Falta Ingreso o Validación de archivo", Page, Master, "W")
                        End If
                    Else
                        MessageBox("Traspasar", "Seleccione Periodo", Page, Master, "W")
                    End If
                Else
                    MessageBox("Traspasar", "Seleccione Areas", Page, Master, "W")
                End If
            Else
                MessageBox("Traspasar", "Seleccione Empresa", Page, Master, "W")
            End If
        Catch ex As Exception
            MessageBoxError("btntraspasar_Click", Err, Page, Master)
        End Try
    End Sub


    Private Sub grDdescarga_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grDdescarga.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim img As ImageButton = CType(row.FindControl("btnAddDesc"), ImageButton)
                Dim estado As HiddenField = CType(row.FindControl("hdnEstadoDesc"), HiddenField)
                If estado.Value = False Then
                    img.Visible = False
                    hdnEstado.Value = estado.Value
                End If
                If estado.Value = True Then
                    img.Visible = True
                    hdnEstado.Value = estado.Value
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grDdescarga_RowDataBound ", Err, Page, Master)
        End Try
    End Sub


    Private Sub ActualizarEstTraspaso()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UET"
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresaDesc.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoDesc.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarEstTraspaso", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarEstTraspasoJTSA()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_CentraSueldos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UETJ"
            comando.Parameters.Add("@codEmpresa", SqlDbType.NVarChar)
            comando.Parameters("@codEmpresa").Value = cboEmpresaDesc.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoDesc.SelectedValue
            comando.Parameters.Add("@Dato", SqlDbType.NVarChar)
            comando.Parameters("@Dato").Value = cboPersonalDesc.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

        Catch ex As Exception
            MessageBoxError("ActualizarEstTraspasoJTSA", Err, Page, Master)
        End Try
    End Sub


    Function TraspasoFin700() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand

        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "dbo.pro_DescargaCentraSueldos"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaDesc.SelectedValue
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoDesc.SelectedValue
            comando.Parameters.Add("@dato", SqlDbType.NVarChar)
            comando.Parameters("@dato").Value = cboPersonalDesc.SelectedValue
            comando.CommandTimeout = 900000000
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            Return True
        Catch ex As Exception
            MessageBoxError("TraspasoFin700", Err, Page, Master)
            Return False
        End Try
    End Function

    Protected Sub btnCarga_Click(sender As Object, e As EventArgs) Handles btnCarga.Click
        mtvCentralizacion.ActiveViewIndex = 0

        If cboPeriodo.SelectedValue <> "Seleccione" Then
            If cboEmpresa.SelectedValue = 1 Then
                Call RescatarEstadoTraspasoJTSA()
            Else
                Call RescatarEstadoTraspaso()
            End If

        End If
    End Sub

    Protected Sub btnTraspaso_Click(sender As Object, e As EventArgs) Handles btnTraspaso.Click
        mtvCentralizacion.ActiveViewIndex = 1
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        cboEmpresaDesc.ClearSelection()
        lblPersonalDesc.Visible = False
        cboPersonalDesc.Visible = False
        cboPeriodoDesc.ClearSelection()
    End Sub
End Class