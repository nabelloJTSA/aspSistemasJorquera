﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb
Public Class aspPrevired
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                Call CargarAnio()
                lbltitulo.Text = "Previred"
                Call CargarAnioCor()
                Call CargarEmpresa()
                Call CargarPeriodo()
                Call CargarPeriodoCor()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
            cboEmpresaCor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaCor.DataTextField = "nom_empresa"
            cboEmpresaCor.DataValueField = "cod_empresa"
            cboEmpresaCor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarAnio2()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut2.DataTextField = "anio"
            cboAnioRut2.DataValueField = "id"
            cboAnioRut2.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnioCor()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioCor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioCor.DataTextField = "anio"
            cboAnioCor.DataValueField = "id"
            cboAnioCor.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoRut()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut2.SelectedValue & "'"
            cboPeriodoRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRut.DataTextField = "periodo"
            cboPeriodoRut.DataValueField = "id"
            cboPeriodoRut.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoCor()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioCor.SelectedValue & "'"
            cboPeriodoCor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoCor.DataTextField = "periodo"
            cboPeriodoCor.DataValueField = "id"
            cboPeriodoCor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        If ValidarPrevired(cboEmpresa.SelectedValue, cboPeriodo.SelectedValue) = True Then
            chkRepro.Checked = False
        Else
            chkRepro.Checked = True
        End If

        If chkRepro.Checked = True Then
            Dim grdResumen As New DataGrid
            Try
                grdResumen.CellPadding = 0
                grdResumen.CellSpacing = 0
                grdResumen.BorderStyle = BorderStyle.None

                grdResumen.Font.Size = 10

                Dim strNomTabla As String = "RRHHMov_previred"

                Dim strSQL As String = "Exec pro_RRHH_GenerarPrevired 'G','" & cboEmpresa.SelectedValue & "','" & cboPeriodo.SelectedValue & "'"

                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdResumen.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

                grdResumen.RenderControl(hw)

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=Previred-" & cboEmpresa.SelectedValue & "-" & cboPeriodo.SelectedValue & ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()

            Catch ex As Exception
                MessageBox("Generar Previred", "btnProcesar", Page, Master, "W")
            End Try
        Else
            Dim grdResumen As New DataGrid
            Try
                grdResumen.CellPadding = 0
                grdResumen.CellSpacing = 0
                grdResumen.BorderStyle = BorderStyle.None

                grdResumen.Font.Size = 10

                Dim strNomTabla As String = "RRHHMov_previred"

                Dim strSQL As String = "Exec sel_RRHH_previred '" & cboEmpresa.SelectedValue & "','" & cboPeriodo.SelectedValue & "','" & Session("cod_usu_tc") & "'"

                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdResumen.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

                grdResumen.RenderControl(hw)

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=Previred-" & cboEmpresa.SelectedValue & "-" & cboPeriodo.SelectedValue & ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()

            Catch ex As Exception
                MessageBox("Generar Previred", "btnProcesar", Page, Master, "W")
            End Try
        End If

    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub


    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C: \CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Protected Sub btnPrevired_Click(sender As Object, e As EventArgs) Handles btnPrevired.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "Previred"
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "Actualizar Previred por Rut"
        Call CargarAnio2()
        Call CargarPeriodoRut()
    End Sub

    Protected Sub btnProcesarRut_Click(sender As Object, e As EventArgs) Handles btnProcesarRut.Click
        Dim grdResumen As New DataGrid
        Try
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None

            grdResumen.Font.Size = 10

            Dim strNomTabla As String = "RRHHMov_previred"

            Dim strSQL As String = "Exec rpt_RRHH_previredRut '" & cboPeriodoRut.SelectedValue & "','" & txtRut.Text & "'"

            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)

            grdResumen.RenderControl(hw)

            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Previred-" & cboEmpresa.SelectedValue & "-" & cboPeriodo.SelectedValue & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default

            Response.Write(tw.ToString())
            Response.End()

        Catch ex As Exception
            MessageBox("Generar Previred", "btnProcesar", Page, Master, "W")
        End Try
    End Sub
    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnProcesarRut.Enabled = True
                Else
                    MessageBox("Permisos", "No puede procesar esta persona", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub


    Private Sub ObtenerNombre()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRut.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(0)
                    btnProcesarRut.Enabled = True
                Else
                    lblNombre.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub


    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        End If
    End Sub

    Protected Sub cboAnioRut2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut2.SelectedIndexChanged
        Call CargarPeriodoRut()
    End Sub


    Private Sub btnMPrevired_Click(sender As Object, e As EventArgs) Handles btnMPrevired.Click
        MultiView1.ActiveViewIndex = 2
    End Sub

    Private Sub cboAnioCor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioCor.SelectedIndexChanged
        Call CargarPeriodoCor()
    End Sub

    Protected Sub Upload()
        Dim excelPath As String = Server.MapPath("~/Temp/") + Path.GetFileName(fuplCargar.PostedFile.FileName)
        fuplCargar.SaveAs(excelPath)

        Dim connString As String = String.Empty
        Dim extension As String = Path.GetExtension(fuplCargar.PostedFile.FileName)
        Select Case extension
            Case ".xls"
                connString = ConfigurationManager.ConnectionStrings("Excel03ConString").ConnectionString
                Exit Select
            Case ".xlsx"
                connString = ConfigurationManager.ConnectionStrings("Excel07+ConString").ConnectionString
                Exit Select

        End Select
        connString = String.Format(connString, excelPath)
        Using excel_con As New OleDbConnection(connString)
            excel_con.Open()
            Dim sheet1 As String = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing).Rows(0)("TABLE_NAME").ToString()
            Dim dtExcelData As New DataTable()
            Dim sqlExcelString As String = (Convert.ToString("SELECT " & NominaPrevired() & " as id_previred," & cboPeriodoCor.SelectedValue & " as num_periodo," & cboEmpresaCor.SelectedValue & " as cod_empresa,1 as linea ,* FROM [") & sheet1) + "]"
            Using oda As New OleDbDataAdapter(sqlExcelString, excel_con)
                oda.Fill(dtExcelData)
            End Using
            excel_con.Close()


            Using con As New SqlConnection(strCnx)
                Using sqlBulkCopy As New SqlBulkCopy(con)
                    sqlBulkCopy.DestinationTableName = "dbo.RRHHMov_previred"
                    con.Open()
                    sqlBulkCopy.WriteToServer(dtExcelData)
                    con.Close()
                End Using
            End Using
            MessageBox("Cargar", "Previred Actualizado", Page, Master, "S")
        End Using
    End Sub
    Function NominaPrevired() As Integer
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec mant_RRHH_tempRutNomina 'np'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    NominaPrevired = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("NominaPrevired", Err, Page, Master)
            Finally
                cnxAcceso.Close()
            End Try
        End Using
        Return NominaPrevired
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If fuplCargar.HasFile = True Then
            'validar seleccion de cbo y agregar elimar previred anterior
            Call Upload()
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If

    End Sub

    Function ValidarPrevired(empresa, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_GenerarPrevired 'VP' , '" & empresa & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function
End Class