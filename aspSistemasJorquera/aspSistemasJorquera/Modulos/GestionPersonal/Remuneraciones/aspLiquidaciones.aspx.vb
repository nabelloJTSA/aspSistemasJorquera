﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb

Public Class aspLiquidaciones
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                Call CargarEmpresa()
                Call CargarEmpresaNomina()
                Call CargarTipoTrabajador()
                Call CargarDepto()
                Call CargarAnio()
                Call CargarAnioNomina()
                Call CargarPeriodoNomina()
                Call CargarPeriodo()
                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipoTrabajador()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CTT', '" & cboEmpresa.SelectedValue & "'"
            cboTipoTrabajador.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajador.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajador.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajador.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkDepto.SelectedIndexChanged
        If chkDepto.Items(0).Selected = True Then
            hdnTodos.Value = 1
            For intCheck = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck).Selected = True
                chkDepto.Items(intCheck).Enabled = False
            Next
        End If
        If chkDepto.Items(0).Selected = False And hdnTodos.Value = 1 Then
            For intCheck2 = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck2).Selected = False
                chkDepto.Items(intCheck2).Enabled = True
            Next
            hdnTodos.Value = 0
        End If
    End Sub


    Protected Sub chkSucursal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkSucursal.SelectedIndexChanged
        If chkSucursal.Items(0).Selected = True Then
            hdnTodosSuc.Value = 1
            For intCheck = 1 To chkSucursal.Items.Count - 1
                chkSucursal.Items(intCheck).Selected = True
                chkSucursal.Items(intCheck).Enabled = False
            Next
        End If
        If chkSucursal.Items(0).Selected = False And hdnTodos.Value = 1 Then
            For intCheck2 = 1 To chkSucursal.Items.Count - 1
                chkSucursal.Items(intCheck2).Selected = False
                chkSucursal.Items(intCheck2).Enabled = True
            Next
            hdnTodosSuc.Value = 0
        End If
    End Sub
    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarPeriodoNomina()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioNomina.SelectedValue & "'"
            cboPeriodoNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoNomina.DataTextField = "periodo"
            cboPeriodoNomina.DataValueField = "id"
            cboPeriodoNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnioNomina()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioNomina.DataTextField = "anio"
            cboAnioNomina.DataValueField = "id"
            cboAnioNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepto()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLD', '" & cboEmpresa.SelectedValue & "', '" & Session("cod_usu_tc") & "'"
            chkDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkDepto.DataTextField = "nom_departamento"
            chkDepto.DataValueField = "cod_departamento"
            chkDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarSucursal()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLS', '" & cboEmpresa.SelectedValue & "'"
            chkSucursal.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkSucursal.DataTextField = "nom_sucursal"
            chkSucursal.DataValueField = "id_sucursal"
            chkSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresaNomina()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresaNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaNomina.DataTextField = "nom_empresa"
            cboEmpresaNomina.DataValueField = "cod_empresa"
            cboEmpresaNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub
    Public Sub GenerarListaDeptos()
        hdnDptos.Value = ""
        Dim i As Integer = 1
        For intCheck = 1 To chkDepto.Items.Count - 1
            If chkDepto.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnDptos.Value = chkDepto.Items(intCheck).Value
                Else
                    hdnDptos.Value = hdnDptos.Value + "," + chkDepto.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub

    Public Sub GenerarListaSucursal()
        hdnSucursal.Value = ""
        Dim i As Integer = 1
        For intCheck = 1 To chkSucursal.Items.Count - 1
            If chkSucursal.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnSucursal.Value = chkSucursal.Items(intCheck).Value
                Else
                    hdnSucursal.Value = hdnSucursal.Value + "," + chkSucursal.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub
    Public Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click

        'Call CreatePDF2(cboEmpresa.SelectedValue, cboPeriodo.SelectedValue, txtRut.Text, hdnSucursal.Value, hdnTipoBusqueda.Value, cboTipoTrabajador.SelectedValue, hdnDptos.Value)
        'Try
        If cboEmpresa.SelectedValue <> 0 And rdbDeptos.Checked = False And rdbSucursales.Checked = False Then
                hdnTipoBusqueda.Value = 3
            End If
            If rdbDeptos.Checked = True Then
                hdnTipoBusqueda.Value = 1
            End If
            If rdbSucursales.Checked = True Then
                hdnTipoBusqueda.Value = 2
            End If
            Call GenerarListaSucursal()
            Call GenerarListaDeptos()
            If txtRut.Text <> "" Then
                hdnTipoBusqueda.Value = 4
                hdnDptos.Value = ""
                hdnSucursal.Value = ""
            End If
            Call GenerarReporte(cboEmpresa.SelectedValue, cboPeriodo.SelectedValue, txtRut.Text, hdnSucursal.Value, hdnTipoBusqueda.Value, cboTipoTrabajador.SelectedValue, hdnDptos.Value)
            hdnTipoBusqueda.Value = 0
            hdnDptos.Value = ""
            hdnSucursal.Value = ""
            pnlDepto.Visible = False
            pnlSucursal.Visible = False
            rdbDeptos.Checked = False
            rdbSucursales.Checked = False
        'Catch ex As Exception
        '    MessageBoxError("btnExportar_Click", Err, Page, Master)
        'End Try
    End Sub

    'Private Sub CreatePDF2(ByVal empresa As Integer, ByVal periodo As Integer, ByVal rut As String, ByVal sucursal As String, ByVal tipobusqueda As Integer, ByVal tipotrab As Integer, ByVal depto As String)
    '    rptVisor.Visible = True
    '    Dim paramList As New Generic.List(Of ReportParameter)
    '    rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldo"
    '    paramList.Add(New ReportParameter("Empresa", empresa, False))
    '    paramList.Add(New ReportParameter("periodo", periodo, False))
    '    paramList.Add(New ReportParameter("Rut", rut, False))
    '    paramList.Add(New ReportParameter("Sucursal", sucursal, False))
    '    paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
    '    paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
    '    paramList.Add(New ReportParameter("departamento", depto, False))
    '    rptVisor.ServerReport.SetParameters(paramList)
    '    rptVisor.ServerReport.Refresh()

    '    Dim warnings As Warning()
    '    Dim streamIds As String()
    '    Dim mimeType As String = String.Empty
    '    Dim encoding As String = String.Empty
    '    Dim extension As String = String.Empty
    '    Dim deviceInfo As String = String.Empty
    '    deviceInfo = "True"
    '    'Dim viewer As ReportViewer = New ReportViewer()
    '    'viewer.ProcessingMode = ProcessingMode.Local
    '    'viewer.LocalReport.ReportPath = "YourReportHere.rdlc"
    '    Dim bytes As Byte() = rptVisor.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, warnings)

    '    Dim pdfPath As String = "C:\prueba." & extension
    '    Dim pdfFile As System.IO.FileStream = New System.IO.FileStream(pdfPath, System.IO.FileMode.Create)
    '    pdfFile.Write(bytes, 0, bytes.Length)
    '    pdfFile.Close()
    '    Response.Flush()
    '    Response.[End]()

    'End Sub
    Private Sub CreatePDF2(ByVal empresa As Integer, ByVal periodo As Integer, ByVal rut As String, ByVal sucursal As String, ByVal tipobusqueda As Integer, ByVal tipotrab As Integer, ByVal depto As String)
        rptVisor.Visible = True
        Dim paramList As New Generic.List(Of ReportParameter)
        rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldo"
        paramList.Add(New ReportParameter("Empresa", empresa, False))
        paramList.Add(New ReportParameter("periodo", periodo, False))
        paramList.Add(New ReportParameter("Rut", rut, False))
        paramList.Add(New ReportParameter("Sucursal", sucursal, False))
        paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
        paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
        paramList.Add(New ReportParameter("departamento", depto, False))
        rptVisor.ServerReport.SetParameters(paramList)
        rptVisor.ServerReport.Refresh()

        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty
        Dim deviceInfo As String = String.Empty
        deviceInfo = "True"
        Dim bytes As Byte() = rptVisor.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=" & "Prueba" & "." & extension)
        Response.BinaryWrite(bytes)
        Response.Flush()

    End Sub

    Private Sub CreatePDF(ByVal fileName As String)
        Dim warnings As Warning()
        Dim streamIds As String()
        Dim mimeType As String = String.Empty
        Dim encoding As String = String.Empty
        Dim extension As String = String.Empty
        Dim viewer As ReportViewer = New ReportViewer()
        viewer.ProcessingMode = ProcessingMode.Local
        viewer.LocalReport.ReportPath = "YourReportHere.rdlc"
        Dim bytes As Byte() = viewer.LocalReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, warnings)
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=" & fileName & "." & extension)
        Response.BinaryWrite(bytes)
        Response.Flush()
    End Sub

    Public Sub GenerarReporte(ByVal empresa As Integer, ByVal periodo As Integer, ByVal rut As String, ByVal sucursal As String, ByVal tipobusqueda As Integer, ByVal tipotrab As Integer, ByVal depto As String)
        Dim paramList As New Generic.List(Of ReportParameter)
        'Try
        If cboEmpresa.SelectedValue = 3 Then ' STARTEC

                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldo"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 32 Then 'Full Renta
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoFR"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 26 Then ' Puerto Seco
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoPS"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If


            If cboEmpresa.SelectedValue = 12 Then ' ESEX
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoSX"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 13 Then ' DITRANS
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoDT"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 14 Then ' AGRICOLA
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoAGR"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 22 Then ' LOGTEC
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoLOGTEC"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresa.SelectedValue = 1 Then ' JTSA
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoJTSA"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                paramList.Add(New ReportParameter("busqueda", Session("cod_usu_tc").ToString, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 3 Then ' STARTEC
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldo"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 32 Then 'Full Renta
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoFR"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()

            End If

            If cboEmpresaNomina.SelectedValue = 26 Then ' Puerto Seco
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoPS"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If


            If cboEmpresaNomina.SelectedValue = 12 Then ' ESEX
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoSX"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 13 Then ' DITRANS
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoDT"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 14 Then ' AGRICOLA
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoAGR"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 22 Then ' LOGTEC
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoLOGTEC"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If

            If cboEmpresaNomina.SelectedValue = 1 Then ' JTSA
                rptVisor.Visible = True
                rptVisor.ServerReport.ReportPath = "/SGT/rptLiquidacionSueldoJTSA"
                paramList.Add(New ReportParameter("Empresa", empresa, False))
                paramList.Add(New ReportParameter("periodo", periodo, False))
                paramList.Add(New ReportParameter("Rut", rut, False))
                paramList.Add(New ReportParameter("Sucursal", sucursal, False))
                paramList.Add(New ReportParameter("tipoBusqueda", tipobusqueda, False))
                paramList.Add(New ReportParameter("tipo_Trabajador", tipotrab, False))
                paramList.Add(New ReportParameter("departamento", depto, False))
                paramList.Add(New ReportParameter("busqueda", Session("cod_usu_tc").ToString, False))
                rptVisor.ServerReport.SetParameters(paramList)
                rptVisor.ServerReport.Refresh()
            End If


        'Catch ex As Exception
        '    MessageBox("GenerarReporte", "btnExportar_Click", Page, Master, "E")
        'End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarTipoTrabajador()
        Call CargarDepto()
        Call CargarSucursal()

    End Sub



    Protected Sub rdbDeptos_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDeptos.CheckedChanged
        pnlDepto.Visible = True
        pnlSucursal.Visible = False
        Call CargarDepto()
    End Sub

    Protected Sub rdbSucursales_CheckedChanged(sender As Object, e As EventArgs) Handles rdbSucursales.CheckedChanged
        pnlDepto.Visible = False
        pnlSucursal.Visible = True
        Call CargarSucursal()
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnExportar.Enabled = True
                Else
                    MessageBox("Permisos", "No puede editar liquidacion de esta persona", Page, Master, "W")
                    btnExportar.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Session("rut_persona") = Trim(txtRut.Text)
                Call ValidarPermisosDeptos()
                hdnTipoBusqueda.Value = 4
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                hdnTipoBusqueda.Value = 0
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Session("rut_persona") = Trim(txtRut.Text)
                Call ValidarPermisosDeptos()
                hdnTipoBusqueda.Value = 4
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                hdnTipoBusqueda.Value = 0

            End If
            If txtRut.Text = "" Then
                btnExportar.Enabled = True
            End If
        End If
    End Sub

    Protected Sub btnFiltroNomina_Click(sender As Object, e As EventArgs) Handles btnFiltroNomina.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub btnFiltroGeneral_Click(sender As Object, e As EventArgs) Handles btnFiltroGeneral.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String))
                                 })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdRuts)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub

    Public Sub EliminarRuts()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "D"
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Public Sub GuardarRuts(ByVal rut As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(rut)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Trim(cboPeriodoNomina.SelectedValue)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Call EliminarRuts()
        Try
            For intRow = 0 To grdRuts.Rows.Count - 1

                Dim objRut As Label = CType(grdRuts.Rows(intRow).FindControl("lblRut"), Label)

                Call GuardarRuts(objRut.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdRuts.DataSource = MostrarExcel(_directorioParaGuardar)
            grdRuts.DataBind()
            hdnTipoBusqueda.Value = 5
        End If
        Call GenerarReporte(cboEmpresaNomina.SelectedValue, cboPeriodoNomina.SelectedValue, "", "", hdnTipoBusqueda.Value, 0, "")

        hdnTipoBusqueda.Value = 0


    End Sub

    Protected Sub cboAnioNomina_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioNomina.SelectedIndexChanged
        Call CargarPeriodoNomina()
    End Sub

    Protected Sub cboEmpresaNomina_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaNomina.SelectedIndexChanged

    End Sub
End Class