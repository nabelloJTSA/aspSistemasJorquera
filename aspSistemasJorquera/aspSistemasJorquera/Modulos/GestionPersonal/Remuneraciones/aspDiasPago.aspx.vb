﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb
Public Class aspDiasPago
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarEmpresa()
                Call CargarAnio()
                Call CargarPeriodo()
                MultiView1.ActiveViewIndex = 0
                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            If cboEmpresa.SelectedValue = 0 Or cboPeriodo.SelectedValue = 0 Then
                MessageBox("Exportar", "Debe seleccionar un archivo empresa y periodo", Page, Master, "W")
            Else
                Call GenerarReporte(cboPeriodo.SelectedValue, cboEmpresa.SelectedValue)
            End If

        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub GenerarReporte(ByVal Periodo As Integer, Empresa As Integer)
        Dim paramList As New Generic.List(Of ReportParameter)
        Try
            rptVisor.Visible = True
            rptVisor.ServerReport.ReportPath = "/SGT/rptDiasPago"
            paramList.Add(New ReportParameter("cod_empresa", Empresa, False))
            paramList.Add(New ReportParameter("periodo", Periodo, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        Catch ex As Exception
            MessageBox("GenerarReporte", "btnExportar", Page, Master, "E")
        End Try
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub
End Class