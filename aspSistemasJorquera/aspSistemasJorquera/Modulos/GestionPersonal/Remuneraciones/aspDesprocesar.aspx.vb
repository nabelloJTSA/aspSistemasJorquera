﻿Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspDesprocesar
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "DesProcesar"
                Call CargarEmpresa()
                Call CargarDepto()
                Call CargarTipoTrabajador()
                Call CargarAnio()
                Call CargarPeriodo()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoRut()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodoRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRut.DataTextField = "periodo"
            cboPeriodoRut.DataValueField = "id"
            cboPeriodoRut.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepto()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLD', '" & cboEmpresa.SelectedValue & "' , '" & Session("cod_usu_tc") & "'"
            chkDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkDepto.DataTextField = "nom_departamento"
            chkDepto.DataValueField = "cod_departamento"
            chkDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarTipoTrabajador()
        Call CargarDepto()
    End Sub

    Private Sub DesProcesar()
        'Try
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandTimeout = 3000
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "mant_RRHH_desProcesar"

        comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
        comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue
        comando.Parameters.Add("@usuario", SqlDbType.NVarChar)
        comando.Parameters("@usuario").Value =  Session("id_usu_tc")
        comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
        comando.Parameters("@cod_departamento").Value = hdnDptos.Value
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
        comando.Parameters.Add("@tipo_Trabajador", SqlDbType.NVarChar)
        comando.Parameters("@tipo_Trabajador").Value = cboTipoTrabajador.SelectedValue
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
        MessageBox("DesProcesado", "DesProcesado", Page, Master, "S")
        'Catch ex As Exception
        '    MessageBoxError("btnProcesar_Click", Err, Page, Master)
        'End Try
    End Sub

    Public Sub GenerarListaDeptos()
        Dim i As Integer = 1
        For intCheck = 1 To chkDepto.Items.Count - 1
            If chkDepto.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnDptos.Value = chkDepto.Items(intCheck).Value
                Else
                    hdnDptos.Value = hdnDptos.Value + "," + chkDepto.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub
    Function ValidarPeriodo(empresa, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VP' , '" & empresa & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function
    Protected Sub btnDesProcesar_Click(sender As Object, e As EventArgs) Handles btnDesProcesar.Click
        If ValidarPeriodo(cboEmpresa.SelectedValue, cboPeriodo.SelectedValue) = True Then
            Call GenerarListaDeptos()
            Call DesProcesar()
            hdnDptos.Value = ""
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If
    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged

    End Sub

    Protected Sub btnRef_Click(sender As Object, e As EventArgs) Handles btnRef.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "DesProcesar"
    End Sub

    Protected Sub btnEERRGeneral_Click(sender As Object, e As EventArgs) Handles btnEERRGeneral.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "DesProcesar por Rut"
    End Sub

    Function ValidarSeleccionRut() As Boolean
        Dim VL As Boolean = True
        If txtRut.Text = "" Then
            MessageBox("Procesar", "Ingrese Rut", Page, Master, "W")
            VL = False
        End If
        If cboPeriodoRut.SelectedValue = 0 Then
            MessageBox("Procesar", "Seleccione SUCURSAL", Page, Master, "W")
            VL = False
        End If
        Return VL
    End Function

    Function RevisarProceso() As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'RR' , '', '', '" & cboPeriodoRut.SelectedValue & "','','" & txtRut.Text & "'"
        Dim estado As Integer = 0
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    estado = 1
                Else
                    estado = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RevisarExisteProceso", Err, Page, Master)
            End Try
        End Using
        Return estado
    End Function


    Private Sub ObtenerNombre()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRut.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(0)
                    btnProcesarRut.Enabled = True
                Else
                    lblNombre.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub DesProcesarRut()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 3000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_RRHH_liquidacion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "DPR"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = txtRut.Text
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoRut.SelectedValue
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value =  Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("DesProcesar", "DesProcesado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("DesProcesarRut", Err, Page, Master)
        End Try
    End Sub
    Function ValidarPeriodoRut(rut, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VPR' , '" & rut & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function
    Protected Sub btnProcesarRut_Click(sender As Object, e As EventArgs) Handles btnProcesarRut.Click
        If ValidarPeriodorut(txtRut.Text, cboPeriodoRut.SelectedValue) = True Then
            If ValidarSeleccionRut() = True Then
                If RevisarProceso() = 1 Then
                    Call DesProcesarRut()
                Else
                    MessageBox("Procesar", "Rut no se encuentra Procesado", Page, Master, "W")
                End If

            End If
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If

    End Sub
    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnProcesarRut.Enabled = True
                Else
                    MessageBox("Permisos", "No puede procesar esta persona", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub
    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        End If
    End Sub
    Private Sub CargarTipoTrabajador()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CTT', '" & cboEmpresa.SelectedValue & "'"
            cboTipoTrabajador.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajador.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajador.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajador.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkDepto.SelectedIndexChanged
        If chkDepto.Items(0).Selected = True Then
            hdnTodos.Value = 1
            For intCheck = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck).Selected = True
                chkDepto.Items(intCheck).Enabled = False
            Next
        End If
        If chkDepto.Items(0).Selected = False And hdnTodos.Value = 1 Then
            For intCheck2 = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck2).Selected = False
                chkDepto.Items(intCheck2).Enabled = True
            Next
            hdnTodos.Value = 0
        End If
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodoRut()
    End Sub
End Class