﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb

Public Class aspCentralizar
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then
                Call CargarAnio()
                lbltitulo.Text = "Generar Centralización"
                Call CargarEmpresa()
                Call CargarPeriodo()

            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
            'cboEmpresaCor.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            'cboEmpresaCor.DataTextField = "nom_empresa"
            'cboEmpresaCor.DataValueField = "cod_empresa"
            'cboEmpresaCor.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        If chkRepro.Checked = True Then
            Dim grdResumen As New DataGrid
            Try
                grdResumen.CellPadding = 0
                grdResumen.CellSpacing = 0
                grdResumen.BorderStyle = BorderStyle.None

                grdResumen.Font.Size = 10

                Dim strNomTabla As String = "RRHHMov_centralizacion"

                Dim strSQL As String = "Exec pro_RRHH_centralizar '" & cboEmpresa.SelectedValue & "','" & cboPeriodo.SelectedValue & "'"

                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdResumen.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

                grdResumen.RenderControl(hw)

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=centralizacion-" & cboEmpresa.SelectedValue & "-" & cboPeriodo.SelectedValue & ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()

            Catch ex As Exception
                MessageBox("Generar Previred", "btnProcesar", Page, Master, "W")
            End Try
        Else
            Dim grdResumen As New DataGrid
            Try
                grdResumen.CellPadding = 0
                grdResumen.CellSpacing = 0
                grdResumen.BorderStyle = BorderStyle.None

                grdResumen.Font.Size = 10

                Dim strNomTabla As String = "RRHHMov_previred"

                Dim strSQL As String = "Exec sel_RRHH_centralizar '" & cboEmpresa.SelectedValue & "','" & cboPeriodo.SelectedValue & "'"

                grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdResumen.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

                grdResumen.RenderControl(hw)

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=centralizacion-" & cboEmpresa.SelectedValue & "-" & cboPeriodo.SelectedValue & ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()

            Catch ex As Exception
                MessageBox("Generar Previred", "btnProcesar", Page, Master, "W")
            End Try
        End If
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged

    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub
End Class