﻿
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspCargarOtrosDescuentos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                Call CargarAnio()
                Call CargarPeriodo()
                lbltitulo.Text = "Cargar Descuentos"
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDescuentos()
        Try
            Dim strNomTablaR As String = "REMMae_item_variables"
            Dim strSQLR As String = "Exec mant_RRHHMov_otros_descuentos 'CD'"
            cboDescuentos.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboDescuentos.DataTextField = "des_descuento"
            cboDescuentos.DataValueField = "cod"
            cboDescuentos.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarDescuentos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeridoRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeridoRut.DataTextField = "periodo"
            cboPeridoRut.DataValueField = "id"
            cboPeridoRut.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCargarAnticipos_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdDescuentos)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub
    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub
    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarArchivoNomina(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)

        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcelNomina(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub
    Function MostrarExcelNomina(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                                New DataColumn("periodo", GetType(String))
                                     })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut, periodo FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                                New DataColumn("nombre", GetType(String)),
                                New DataColumn("num_periodo", GetType(String)),
                                New DataColumn("des_cuota", GetType(String)),
                                New DataColumn("des_uf", GetType(String)),
                                New DataColumn("des_descuento", GetType(String)),
                                New DataColumn("num_valor", GetType(String))
                                     })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut, nombre, num_periodo, des_cuota, des_uf,des_descuento,num_valor FROM [Hoja1$] order by nombre", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFilaN.Item(2) = dtTemporal(i).Item(2)
                dtFilaN.Item(3) = dtTemporal(i).Item(3)
                dtFilaN.Item(4) = dtTemporal(i).Item(4)
                dtFilaN.Item(5) = dtTemporal(i).Item(5)
                dtFilaN.Item(6) = dtTemporal(i).Item(6)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function
    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdDescuentos.Rows.Count - 1

                Dim objRut As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblRut"), Label)
                Dim objNombre As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblNombre"), Label)
                Dim objPeriodo As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblPeriodo"), Label)
                Dim objCuota As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblCuota"), Label)
                Dim objUf As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblUfDesc"), Label)
                Dim objDescuento As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblDescuento"), Label)
                Dim objValor As Label = CType(grdDescuentos.Rows(intRow).FindControl("lblValor"), Label)

                Call GuardarDescuentos(objRut.Text, objNombre.Text, objPeriodo.Text, objCuota.Text, objUf.Text, objDescuento.Text, objValor.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Private Sub GuardarDescuentos(ByVal rut As String, ByVal nombre As String, ByVal periodo As String, ByVal cuota As String,
                              ByVal uf As String, ByVal descuento As String, ByVal valor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMov_otros_descuentos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(rut)
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = Trim(periodo)
            comando.Parameters.Add("@des_cuota", SqlDbType.NVarChar)
            comando.Parameters("@des_cuota").Value = Trim(cuota)
            comando.Parameters.Add("@uf_descuento", SqlDbType.NVarChar)
            comando.Parameters("@uf_descuento").Value = Trim(uf)
            comando.Parameters.Add("@des_descuento", SqlDbType.NVarChar)
            comando.Parameters("@des_descuento").Value = Trim(descuento)
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = Trim(Replace(valor, ",", "."))

            If (Trim(descuento)) = "Retención Judicial" Then
                comando.Parameters.Add("@num_orden", SqlDbType.NVarChar)
                comando.Parameters("@num_orden").Value = Trim(1000)
            Else
                comando.Parameters.Add("@num_orden", SqlDbType.NVarChar)
                comando.Parameters("@num_orden").Value = Trim(1001)
            End If

            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarSocios", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdDescuentos.DataSource = MostrarExcel(_directorioParaGuardar)
            grdDescuentos.DataBind()
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub

    Protected Sub btnCargarOtro_Click(sender As Object, e As EventArgs) Handles btnCargarOtro.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "Cargar Descuentos"
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "Eliminar Descuentos"
    End Sub

    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    Call CargarGrillaDescuentos()
                Else
                    MessageBox("Permisos", "No puede modificar descuentos de esta persona", Page, Master, "W")

                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")

            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")

            End If
        End If
    End Sub
    Private Sub ObtenerNombre()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRut.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(0)
                    Call CargarGrillaDescuentos()
                Else
                    lblNombre.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub CargarGrillaDescuentos()
        Try
            Dim strNomTabla As String = "RRHHcab_liquidacion"
            Dim strSQL As String = "Exec mant_RRHH_editar_liquidacion 'COD', '" & txtRut.Text & "','" & cboPeridoRut.SelectedValue & "' "
            grdEliminarDescuentos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdEliminarDescuentos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdEliminarDescuentos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdEliminarDescuentos.SelectedIndexChanged

    End Sub

    Private Sub grdEliminarDescuentos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdEliminarDescuentos.RowCommand
        If Trim(LCase(e.CommandName)) = "btneliminar" Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = grdEliminarDescuentos.Rows(index)
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMov_otros_descuentos"
            comando.Parameters.Add("@busqueda", SqlDbType.Int)
            comando.Parameters("@busqueda").Value = CType(row.FindControl("hdnIdItem"), HiddenField).Value
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "D"
            comando.Connection = conx
            comando.CommandTimeout = 5000
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call CargarGrillaDescuentos()
            'If cboEmpresas.SelectedValue = 1 Then
            '    Call CalcularMontosJ()
            '    Call CargarIngresos("CGIJ")
            'Else
            '    Call CalcularMontos()
            '    Call CargarIngresos("CGI")
            'End If
        End If
    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub BbtnCargarNomina_Click(sender As Object, e As EventArgs) Handles btnCargarNomina.Click
        If cboDescuentos.SelectedValue = "0" Then
            MessageBox("Cargar", "Debe Seleccionar Descuento", Page, Master, "W")
        Else
            If fuNomina.HasFile = True Then
                Call CargarArchivoNomina(fuNomina, Me.Request, grdNominaDesc)
                btnEliminarNomina.Enabled = True
            Else
                MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
            End If
        End If
    End Sub

    Protected Sub btnENomina_Click(sender As Object, e As EventArgs) Handles btnENomina.Click
        MultiView1.ActiveViewIndex = 2
        lbltitulo.Text = "Eliminar por Nomina"
        Call CargarDescuentos()
    End Sub

    Protected Sub btnEliminarNomina_Click(sender As Object, e As EventArgs) Handles btnEliminarNomina.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If ElimnarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdNominaDesc.DataSource = MostrarExcel(_directorioParaGuardar)
            grdNominaDesc.DataBind()
        Else
            MessageBox("Eliminar", "Ocurrió un error al Eliminar los datos", Page, Master, "W")
        End If
    End Sub

    Function ElimnarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdNominaDesc.Rows.Count - 1

                Dim objRut As Label = CType(grdNominaDesc.Rows(intRow).FindControl("lblRut"), Label)
                Dim objNombre As Label = CType(grdNominaDesc.Rows(intRow).FindControl("lblNombre"), Label)
                Dim objPeriodo As Label = CType(grdNominaDesc.Rows(intRow).FindControl("lblPeriodo"), Label)

                Call EliminarDescuentos(objRut.Text, objNombre.Text, objPeriodo.Text)
            Next
            MessageBox("Guardar", "Registros Elimiandos Exitosamente", Page, Master, "S")
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Private Sub EliminarDescuentos(ByVal rut As String, ByVal nombre As String, ByVal periodo As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMov_otros_descuentos"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "EN"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(rut)
            comando.Parameters.Add("@des_descuento", SqlDbType.NVarChar)
            comando.Parameters("@des_descuento").Value = cboDescuentos.SelectedValue
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = Trim(periodo)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarDescuentos", Err, Page, Master)
        End Try
    End Sub
End Class