﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb

Public Class aspItemInformados
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarItems()
                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "Ingreso Items"
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarItems()
        Try
            Dim strNomTablaR As String = "REMMae_item_variables"
            Dim strSQLR As String = "Exec mant_RRHHmae_items_supervisor 'CI'"
            cboItem.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboItem.DataTextField = "nom_item_variables"
            cboItem.DataValueField = "id_item_variables"
            cboItem.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarItems", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub
    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                                New DataColumn("nombre", GetType(String)),
                                New DataColumn("num_periodo", GetType(String)),
                                New DataColumn("num_valor", GetType(String))
                                     })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut, nombre, num_periodo, num_valor FROM [Hoja1$] order by nombre", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFilaN.Item(2) = dtTemporal(i).Item(2)
                dtFilaN.Item(3) = dtTemporal(i).Item(3)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function
    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If cboItem.SelectedValue = 0 Then
            MessageBox("Cargar", "Debe Seleccionar el item", Page, Master, "W")
        Else
            If fuplCargar.HasFile = True Then
                Call CargarArchivo(fuplCargar, Me.Request, grdItems)
                btnGuardar.Enabled = True
            Else
                MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
            End If
        End If

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdItems.DataSource = MostrarExcel(_directorioParaGuardar)
            grdItems.DataBind()
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub


    Private Sub GuardarDescuentos(ByVal rut As String, ByVal nombre As String, ByVal periodo As String, ByVal valor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHmae_items_supervisor"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(rut)
            comando.Parameters.Add("@id_item", SqlDbType.NVarChar)
            comando.Parameters("@id_item").Value = cboItem.SelectedValue
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = Trim(periodo)
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = Trim(Replace(valor, ",", "."))
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarDescuentos", Err, Page, Master)
        End Try
    End Sub

    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdItems.Rows.Count - 1

                Dim objRut As Label = CType(grdItems.Rows(intRow).FindControl("lblRut"), Label)
                Dim objNombre As Label = CType(grdItems.Rows(intRow).FindControl("lblNombre"), Label)
                Dim objPeriodo As Label = CType(grdItems.Rows(intRow).FindControl("lblPeriodo"), Label)
                Dim objValor As Label = CType(grdItems.Rows(intRow).FindControl("lblValor"), Label)

                Call GuardarDescuentos(objRut.Text, objNombre.Text, objPeriodo.Text, objValor.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")
            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Protected Sub btnRef_Click(sender As Object, e As EventArgs) Handles btnRef.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "Ingreso Items"
    End Sub
    '********************FALLAS
    Protected Sub btnEERRGeneral_Click(sender As Object, e As EventArgs) Handles btnEERRGeneral.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "Ingreso Fallas"
    End Sub


    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call CargarGrilla()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnGuardarF.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call CargarGrilla()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnGuardarF.Enabled = False
            End If
        End If
    End Sub

    Private Sub ObtenerNombre()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRut.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(0)
                    btnGuardarF.Enabled = True
                Else
                    lblNombre.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                    btnGuardarF.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub GuardarFallas(ByVal rut As String, ByVal valor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHmae_items_supervisor"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IF"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(rut)
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = Trim(Replace(valor, ",", "."))
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(txtFechaT.Text)
            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(txtfechai.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Falla Guardad", Page, Master, "S")
            Call CargarGrilla()
        Catch ex As Exception
            MessageBoxError("GuardarDescuentos", Err, Page, Master)
        End Try
    End Sub

    Private Sub EliminarFallas(ByVal id As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHmae_items_supervisor"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "D"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(id)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("EliminarFallas", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnProcesarRut_Click(sender As Object, e As EventArgs) Handles btnGuardarF.Click
        If txtRut.Text = "" Or txtfechai.Text = "" Or txtFechaT.Text = "" Or txtDias.Text = "" Then
            MessageBox("Guardar", "Faltan datos por ingresar", Page, Master, "W")
        Else
            GuardarFallas(txtRut.Text, txtDias.Text)
        End If
    End Sub

    Protected Sub grdFallas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdFallas.SelectedIndexChanged

    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_Area"
            Dim strSQL As String = "Exec mant_RRHHmae_items_supervisor 'G','" & txtRut.Text & "'"
            grdFallas.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdFallas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdFallas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdFallas.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdFallas.Rows(intRow)
        If Trim(LCase(e.CommandName)) = "2" Then

            Call EliminarFallas(CType(row.FindControl("hdn_id"), HiddenField).Value)
            MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
            Call CargarGrilla()
        End If
    End Sub

    Protected Sub txtfechai_TextChanged(sender As Object, e As EventArgs) Handles txtfechai.TextChanged
        If txtFechaT.Text <> "" Then
            Dim DiferenciaDias As Integer = DateDiff(DateInterval.Day, CDate(txtfechai.Text), CDate(txtFechaT.Text)) + 1
            txtDias.Text = DiferenciaDias
        End If

    End Sub

    Protected Sub txtFechaT_TextChanged(sender As Object, e As EventArgs) Handles txtFechaT.TextChanged
        If txtfechai.Text <> "" Then
            Dim DiferenciaDias As Integer = DateDiff(DateInterval.Day, CDate(txtfechai.Text), CDate(txtFechaT.Text)) + 1
            txtDias.Text = DiferenciaDias
        End If
    End Sub
End Class