﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb

Public Class aspLibroRemuneraciones
    Inherits System.Web.UI.Page
    Private ConfiguracionAppSettings As New System.Configuration.AppSettingsReader
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarEmpresa()
                Call CargarEmpresaNomina()
                Call CargarTipoTrabajador()
                Call CargarDepto()
                Call CargarAnio()
                Call CargarPeriodo()
                Call CargarAnioNomina()
                Call CargarPeriodoNomina()
                MultiView1.ActiveViewIndex = 0
                Dim strURLReports As String = DecryptTripleDES(CType(ConfiguracionAppSettings.GetValue("urlReports", GetType(System.String)), String))
                rptVisor.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                rptVisor.ServerReport.ReportServerUrl = New Uri(strURLReports)
                rptVisor.ShowPrintButton = True
                rptVisor.ServerReport.Refresh()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarPeriodoNomina()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioNomina.SelectedValue & "'"
            cboPeriodoNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoNomina.DataTextField = "periodo"
            cboPeriodoNomina.DataValueField = "id"
            cboPeriodoNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEmpresaNomina()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresaNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaNomina.DataTextField = "nom_empresa"
            cboEmpresaNomina.DataValueField = "cod_empresa"
            cboEmpresaNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnioNomina()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioNomina.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioNomina.DataTextField = "anio"
            cboAnioNomina.DataValueField = "id"
            cboAnioNomina.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipoTrabajador()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CTT', '" & cboEmpresa.SelectedValue & "'"
            cboTipoTrabajador.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajador.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajador.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajador.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkDepto.SelectedIndexChanged
        If chkDepto.Items(0).Selected = True Then
            hdnTodos.Value = 1
            For intCheck = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck).Selected = True
                chkDepto.Items(intCheck).Enabled = False
            Next
        End If
        If chkDepto.Items(0).Selected = False And hdnTodos.Value = 1 Then
            For intCheck2 = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck2).Selected = False
                chkDepto.Items(intCheck2).Enabled = True
            Next
            hdnTodos.Value = 0
        End If
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepto()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLD', '" & cboEmpresa.SelectedValue & "' , '" & Session("cod_usu_tc") & "'"
            chkDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkDepto.DataTextField = "nom_departamento"
            chkDepto.DataValueField = "cod_departamento"
            chkDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub
    Public Sub GenerarListaDeptos()
        hdnDptos.Value = ""
        Dim i As Integer = 1
        For intCheck = 1 To chkDepto.Items.Count - 1
            If chkDepto.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnDptos.Value = chkDepto.Items(intCheck).Value
                Else
                    hdnDptos.Value = hdnDptos.Value + "," + chkDepto.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub


    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Call GenerarListaDeptos()
            Call GenerarReporte(cboPeriodo.SelectedValue, cboEmpresa.SelectedValue, cboTipoTrabajador.SelectedValue, hdnDptos.Value)
            pnlDepto.Visible = False
            hdnDptos.Value = ""
            btnVer.Visible = True
        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub GenerarReporte(ByVal Periodo As Integer, Empresa As Integer, ByVal tipo_trabajador As Integer, ByVal deptos As String)
        Dim paramList As New Generic.List(Of ReportParameter)
        Try
            rptVisor.Visible = True
            rptVisor.ServerReport.ReportPath = "/SGT/LibroRemConsolidado"
            paramList.Add(New ReportParameter("empresa", Empresa, False))
            paramList.Add(New ReportParameter("tipo_trabajador", tipo_trabajador, False))
            paramList.Add(New ReportParameter("departamento", deptos, False))
            paramList.Add(New ReportParameter("Periodo", Periodo, False))
            paramList.Add(New ReportParameter("nomina", hdnNomina.Value, False))
            paramList.Add(New ReportParameter("busqueda", Session("cod_usu_tc").ToString, False))
            rptVisor.ServerReport.SetParameters(paramList)
            rptVisor.ServerReport.Refresh()
        Catch ex As Exception
            MessageBox("GenerarReporte", "btnExportar", Page, Master, "E")
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarTipoTrabajador()
        Call CargarDepto()
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub btnVer_Click(sender As Object, e As EventArgs) Handles btnVer.Click
        pnlDepto.Visible = True
    End Sub

    Protected Sub cboEmpresaNomina_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaNomina.SelectedIndexChanged

    End Sub

    Protected Sub cboAnioNomina_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioNomina.SelectedIndexChanged
        Call CargarPeriodoNomina()
    End Sub

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String))
                                 })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdRuts)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub


    Public Sub EliminarRuts()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "D"
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Public Sub GuardarRuts(ByVal rut As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(rut)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Trim(cboPeriodoNomina.SelectedValue)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub


    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Call EliminarRuts()
        Try
            For intRow = 0 To grdRuts.Rows.Count - 1

                Dim objRut As Label = CType(grdRuts.Rows(intRow).FindControl("lblRut"), Label)

                Call GuardarRuts(objRut.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdRuts.DataSource = MostrarExcel(_directorioParaGuardar)
            grdRuts.DataBind()
            hdnNomina.Value = 1
        End If
        Call GenerarReporte(cboPeriodoNomina.SelectedValue, cboEmpresaNomina.SelectedValue, 0, "")
        hdnNomina.Value = 0

    End Sub

    Protected Sub btnFiltroGeneral_Click(sender As Object, e As EventArgs) Handles btnFiltroGeneral.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnFiltroNomina_Click(sender As Object, e As EventArgs) Handles btnFiltroNomina.Click
        MultiView1.ActiveViewIndex = 1
    End Sub
End Class
