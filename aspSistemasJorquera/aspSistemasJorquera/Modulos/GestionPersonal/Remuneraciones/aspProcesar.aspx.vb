﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspProcesar
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then
                MultiView1.ActiveViewIndex = 0
                lbltitulo.Text = "Procesar"
                Call CargarEmpresa()
                Call CargarDepto()
                Call CargarTipoTrabajador()
                Call CargarAnio()
                Call CargarPeriodo()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Function ValidarPeriodo(empresa, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VP' , '" & empresa & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function

    Function ValidarPeriodoRut(rut, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VPR' , '" & rut & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoRut()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodoRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRut.DataTextField = "periodo"
            cboPeriodoRut.DataValueField = "id"
            cboPeriodoRut.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboanio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanio.DataTextField = "anio"
            cboanio.DataValueField = "id"
            cboanio.DataBind()
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDepto()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLD', '" & cboEmpresa.SelectedValue & "' , '" & Session("cod_usu_tc") & "'"
            chkDepto.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkDepto.DataTextField = "nom_departamento"
            chkDepto.DataValueField = "cod_departamento"
            chkDepto.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarTipoTrabajador()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CTT', '" & cboEmpresa.SelectedValue & "'"
            cboTipoTrabajador.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajador.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajador.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajador.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarTipoTrabajador()
        Call CargarDepto()

    End Sub

    Private Sub Procesar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 3000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_RRHH_liquidacion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "P"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@tipo_trabajador", SqlDbType.NVarChar)
            comando.Parameters("@tipo_trabajador").Value = cboTipoTrabajador.SelectedValue
            comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
            comando.Parameters("@cod_departamento").Value = hdnDptos.Value
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("id_usu_tc")

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Procesar", "Procesado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("btnProcesar_Click", Err, Page, Master)
        End Try
    End Sub

    Private Sub ProcesarRut()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 3000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_RRHH_liquidacion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "PR"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = txtRut.Text
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoRut.SelectedValue
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Procesar", "Procesado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("ProcesarRut", Err, Page, Master)
        End Try
    End Sub

    Private Sub ObtenerNombre()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRut.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombre.Text = rdoReader(0)
                    btnProcesarRut.Enabled = True
                Else
                    lblNombre.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub

    Function ValidarSeleccion() As Boolean
        Dim VL As Boolean = True
        If cboEmpresa.SelectedValue = 0 Then
            MessageBox("Procesar", "Seleccione EMPRESA", Page, Master, "W")
            VL = False
        End If
        If cboPeriodo.SelectedValue = 0 Then
            MessageBox("Procesar", "Seleccione PERIODO", Page, Master, "W")
            VL = False
        End If
        Return VL
    End Function


    Function ValidarSeleccionrUT() As Boolean
        Dim VL As Boolean = True
        If txtRut.Text = "" Then
            MessageBox("Procesar", "Ingrese Rut", Page, Master, "W")
            VL = False
        End If
        If cboPeriodoRut.SelectedValue = 0 Then
            MessageBox("Procesar", "Seleccione SUCURSAL", Page, Master, "W")
            VL = False
        End If
        Return VL
    End Function

    Protected Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        If ValidarPeriodo(cboEmpresa.SelectedValue, cboPeriodo.SelectedValue) = True Then
            Call GenerarListaDeptos()
            Call Procesar()
            hdnDptos.Value = ""
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If

    End Sub

    Protected Sub btnRef_Click(sender As Object, e As EventArgs) Handles btnRef.Click
        MultiView1.ActiveViewIndex = 0
        lbltitulo.Text = "Procesar"
    End Sub

    Protected Sub btnEERRGeneral_Click(sender As Object, e As EventArgs) Handles btnEERRGeneral.Click
        MultiView1.ActiveViewIndex = 1
        lbltitulo.Text = "Procesar por Rut"
        Call CargarAnio()
        Call CargarPeriodoRut()
    End Sub

    Function RevisarProceso() As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'RR' , '', '', '" & cboPeriodoRut.SelectedValue & "','','" & txtRut.Text & "'"
        Dim estado As Integer = 0
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    estado = 1
                Else
                    estado = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RevisarExisteProceso", Err, Page, Master)
            End Try
        End Using
        Return estado
    End Function

    Protected Sub btnProcesarRut_Click(sender As Object, e As EventArgs) Handles btnProcesarRut.Click
        If ValidarPeriodoRut(txtRut.Text, cboPeriodoRut.SelectedValue) = True Then
            If ValidarSeleccionrUT() = True Then
                If RevisarProceso() = 0 Then
                    Call ProcesarRut()
                Else
                    MessageBox("Procesar", "Rut ya se encuentra Procesado", Page, Master, "W")
                End If

            End If
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If


    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRut.Enabled = False
            End If
        End If
    End Sub

    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnProcesarRut.Enabled = True
                Else
                    MessageBox("Permisos", "No puede procesar esta persona", Page, Master, "W")
                    btnProcesarRut.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub

    Public Sub GenerarListaDeptos()
        Dim i As Integer = 1
        For intCheck = 1 To chkDepto.Items.Count - 1
            If chkDepto.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnDptos.Value = chkDepto.Items(intCheck).Value
                Else
                    hdnDptos.Value = hdnDptos.Value + "," + chkDepto.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub


    Protected Sub chkDepto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkDepto.SelectedIndexChanged
        If chkDepto.Items(0).Selected = True Then
            hdnTodos.Value = 1
            For intCheck = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck).Selected = True
                chkDepto.Items(intCheck).Enabled = False
            Next
        End If
        If chkDepto.Items(0).Selected = False And hdnTodos.Value = 1 Then
            For intCheck2 = 1 To chkDepto.Items.Count - 1
                chkDepto.Items(intCheck2).Selected = False
                chkDepto.Items(intCheck2).Enabled = True
            Next
            hdnTodos.Value = 0
        End If
    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodoRut()
    End Sub

    Protected Sub cboanio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    '*************************Reversar

    Private Sub CargarPeriodoRe()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRe.SelectedValue & "'"
            cboPeriodoRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRe.DataTextField = "periodo"
            cboPeriodoRe.DataValueField = "id"
            cboPeriodoRe.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoRutRe()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRutRe.SelectedValue & "'"
            cboPeriodoRutRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRutRe.DataTextField = "periodo"
            cboPeriodoRutRe.DataValueField = "id"
            cboPeriodoRutRe.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnioRe()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRe.DataTextField = "anio"
            cboAnioRe.DataValueField = "id"
            cboAnioRe.DataBind()
            cboAnioRutRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRutRe.DataTextField = "anio"
            cboAnioRutRe.DataValueField = "id"
            cboAnioRutRe.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarDeptoRe()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec mant_RRHHMae_plantilla 'CLD', '" & cboEmpresaRe.SelectedValue & "' , '" & Session("cod_usu_tc") & "'"
            chkDeptosRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkDeptosRe.DataTextField = "nom_departamento"
            chkDeptosRe.DataValueField = "cod_departamento"
            chkDeptosRe.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarDepto", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarEmpresaRe()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresaRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaRe.DataTextField = "nom_empresa"
            cboEmpresaRe.DataValueField = "cod_empresa"
            cboEmpresaRe.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub


    Protected Sub cboEmpresaRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaRe.SelectedIndexChanged
        Call CargarTipoTrabajadorRe()
        Call CargarDeptoRe()
    End Sub

    Private Sub DesProcesarRe()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 3000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_desProcesar"

            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoRe.SelectedValue
            comando.Parameters.Add("@usuario", SqlDbType.NVarChar)
            comando.Parameters("@usuario").Value = Session("id_usu_tc")
            comando.Parameters.Add("@cod_departamento", SqlDbType.NVarChar)
            comando.Parameters("@cod_departamento").Value = hdnDeptosRe.Value
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresaRe.SelectedValue
            comando.Parameters.Add("@tipo_Trabajador", SqlDbType.NVarChar)
            comando.Parameters("@tipo_Trabajador").Value = cboTipoTrabajadorRe.SelectedValue
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("DesProcesado", "DesProcesado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("btnProcesar_Click", Err, Page, Master)
        End Try
    End Sub

    Public Sub GenerarListaDeptosRe()
        Dim i As Integer = 1
        For intCheck = 1 To chkDeptosRe.Items.Count - 1
            If chkDeptosRe.Items(intCheck).Selected = True Then
                If i = 1 Then
                    hdnDeptosRe.Value = chkDeptosRe.Items(intCheck).Value
                Else
                    hdnDeptosRe.Value = hdnDeptosRe.Value + "," + chkDeptosRe.Items(intCheck).Value
                End If
                i = i + 1
            End If
        Next
    End Sub
    Function ValidarPeriodoRe(empresa, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VP' , '" & empresa & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function
    Protected Sub btnDesProcesarRe_Click(sender As Object, e As EventArgs) Handles btnDesProcesar.Click
        If ValidarPeriodo(cboEmpresaRe.SelectedValue, cboPeriodoRe.SelectedValue) = True Then
            Call GenerarListaDeptosRe()
            Call DesProcesarRe()
            hdnDeptosRe.Value = ""
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If
    End Sub

    Function ValidarSeleccionRutRe() As Boolean
        Dim VL As Boolean = True
        If txtRutRe.Text = "" Then
            MessageBox("Procesar", "Ingrese Rut", Page, Master, "W")
            VL = False
        End If
        If cboPeriodoRutRe.SelectedValue = 0 Then
            MessageBox("Procesar", "Seleccione SUCURSAL", Page, Master, "W")
            VL = False
        End If
        Return VL
    End Function

    Function RevisarProcesoRe() As Integer
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'RR' , '', '', '" & cboPeriodoRutRe.SelectedValue & "','','" & txtRutRe.Text & "'"
        Dim estado As Integer = 0
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    estado = 1
                Else
                    estado = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RevisarExisteProceso", Err, Page, Master)
            End Try
        End Using
        Return estado
    End Function


    Private Sub ObtenerNombreRe()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion 'SN' , '" & txtRutRe.Text & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblNombreRe.Text = rdoReader(0)
                    btnProcesarRutRe.Enabled = True
                Else
                    lblNombreRe.Text = ""
                    MessageBox("Nombre", "Rut no existe en Ficha", Page, Master, "W")
                    btnProcesarRutRe.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ObtenerNombre", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub DesProcesarRutRe()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandTimeout = 3000
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_RRHH_liquidacion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "DPR"
            comando.Parameters.Add("@rut", SqlDbType.NVarChar)
            comando.Parameters("@rut").Value = txtRutRe.Text
            comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
            comando.Parameters("@periodo").Value = cboPeriodoRutRe.SelectedValue
            comando.Parameters.Add("@user_add", SqlDbType.NVarChar)
            comando.Parameters("@user_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("DesProcesar", "DesProcesado", Page, Master, "S")
        Catch ex As Exception
            MessageBoxError("DesProcesarRut", Err, Page, Master)
        End Try
    End Sub
    Function ValidarPeriodoRutRe(rut, periodo) As Boolean

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_periodo 'VPR' , '" & rut & "', '" & periodo & "'"
        Dim ok As Boolean = False
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPeriodo", Err, Page, Master)
            End Try
            Return ok
        End Using
    End Function
    Protected Sub btnProcesarRutRe_Click(sender As Object, e As EventArgs) Handles btnProcesarRutRe.Click
        If ValidarPeriodoRut(txtRutRe.Text, cboPeriodoRutRe.SelectedValue) = True Then
            If ValidarSeleccionrUTRe() = True Then
                If RevisarProcesoRe() = 1 Then
                    Call DesProcesarRutRe()
                Else
                    MessageBox("Procesar", "Rut no se encuentra Procesado", Page, Master, "W")
                End If

            End If
        Else
            MessageBox("Periodo", "Este perido no se encuentra abierto", Page, Master, "E")
        End If

    End Sub
    Public Sub ValidarPermisosDeptosRe()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRutRe.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnProcesarRutRe.Enabled = True
                Else
                    MessageBox("Permisos", "No puede procesar esta persona", Page, Master, "W")
                    btnProcesarRutRe.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub
    Protected Sub txtRutRe_TextChanged(sender As Object, e As EventArgs) Handles txtRutRe.TextChanged
        txtRutRe.Text = Replace(txtRutRe.Text, ".", "")
        txtRutRe.Text = Replace(txtRutRe.Text, "-", "")
        If Len(txtRutRe.Text) = 8 Then
            txtRutRe.Text = Left(txtRutRe.Text, 7) + "-" + Right(txtRutRe.Text, 1)
        ElseIf Len(txtRutRe.Text) = 9 Then
            txtRutRe.Text = Left(txtRutRe.Text, 8) + "-" + Right(txtRutRe.Text, 1)
        End If

        If Len(txtRutRe.Text) = 10 Then
            txtRutRe.Text = txtRutRe.Text
            If ValidarRut(txtRutRe) = True Then
                Call ObtenerNombreRe()
                Call ValidarPermisosDeptosRe()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRutRe.Enabled = False
            End If
        ElseIf Len(txtRutRe.Text) = 9 Then
            txtRutRe.Text = "0" + txtRutRe.Text
            If ValidarRut(txtRutRe) = True Then
                Call ObtenerNombre()
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnProcesarRutRe.Enabled = False
            End If
        End If
    End Sub
    Private Sub CargarTipoTrabajadorRe()
        Try
            Dim strNomTablaR As String = "RRHHMae_Departamento"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CTT', '" & cboEmpresaRe.SelectedValue & "'"
            cboTipoTrabajadorRe.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipoTrabajadorRe.DataTextField = "nom_tipo_trabajador"
            cboTipoTrabajadorRe.DataValueField = "id_tipo_trabajador"
            cboTipoTrabajadorRe.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarTipoTrabajador", Err, Page, Master)
        End Try
    End Sub
    Protected Sub chkDeptosRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkDeptosRe.SelectedIndexChanged
        If chkDeptosRe.Items(0).Selected = True Then
            hdnTodosRe.Value = 1
            For intCheck = 1 To chkDeptosRe.Items.Count - 1
                chkDeptosRe.Items(intCheck).Selected = True
                chkDeptosRe.Items(intCheck).Enabled = False
            Next
        End If
        If chkDeptosRe.Items(0).Selected = False And hdnTodosRe.Value = 1 Then
            For intCheck2 = 1 To chkDeptosRe.Items.Count - 1
                chkDeptosRe.Items(intCheck2).Selected = False
                chkDeptosRe.Items(intCheck2).Enabled = True
            Next
            hdnTodosRe.Value = 0
        End If
    End Sub

    Protected Sub cboanioRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRe.SelectedIndexChanged
        Call CargarPeriodoRe()
    End Sub

    Protected Sub cboAnioRutRe_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRutRe.SelectedIndexChanged
        Call CargarPeriodoRutRe()
    End Sub

    Protected Sub btnReversarG_Click(sender As Object, e As EventArgs) Handles btnReversarG.Click

        MultiView1.ActiveViewIndex = 2
        lbltitulo.Text = "Reversar"
        Call CargarEmpresaRe()
        Call CargarDeptoRe()
        Call CargarTipoTrabajadorRe()
        Call CargarAnioRe()
        Call CargarPeriodoRe()
    End Sub

    Protected Sub btnReversarR_Click(sender As Object, e As EventArgs) Handles btnReversarR.Click
        MultiView1.ActiveViewIndex = 3
        lbltitulo.Text = "Reversar por Rut"
        Call CargarEmpresaRe()
        Call CargarDeptoRe()
        Call CargarTipoTrabajadorRe()
        Call CargarAnioRe()
        Call CargarPeriodoRe()
    End Sub
End Class