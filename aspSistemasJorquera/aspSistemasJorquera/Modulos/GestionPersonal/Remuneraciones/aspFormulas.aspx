﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspFormulas.aspx.vb" Inherits="aspSistemasJorquera.aspFormulas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Fórmulas"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:MultiView ID="mvFormulas" runat="server">
                    <asp:View ID="vwCrear" runat="server">
                        <asp:Panel ID="pnlDatos" runat="server">
                            <asp:TextBox ID="txtFormula" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" TextMode="MultiLine" Visible="false"></asp:TextBox>

                            <div class="row justify-content-center table-responsive">

                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">Empresa</label>
                                    <div>
                                        <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">ITEM</label>
                                    <div>
                                        <asp:DropDownList ID="cboItem" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label for="fname" class="control-label col-form-label">Descripción</label>
                                    <div>
                                        <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlOpcion1" runat="server">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Valor</label>
                                        <div>
                                            <asp:RadioButton ID="rdbValorBase1" runat="server" AutoPostBack="True" GroupName="1" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Movimiento</label>
                                        <div>
                                            <asp:RadioButton ID="rdbMovimiento1" runat="server" AutoPostBack="True" GroupName="1" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Formulas</label>
                                        <div>
                                            <asp:RadioButton ID="rdbFormula1" runat="server" AutoPostBack="True" GroupName="1" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Informados</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemsInfo1" runat="server" AutoPostBack="True" GroupName="1" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Fijos</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemFijo1" runat="server" AutoPostBack="True" GroupName="1" />
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlValor1" runat="server" Visible="false">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-3 form-group ">                                       
                                        <div>
                                            <asp:TextBox ID="txtValorBase1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="cboMovimiento1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboFormulas1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboCantidad1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboMontos1" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group ">
                                    <label for="fname" class="control-label col-form-label">Operador</label>
                                    <div>
                                        <asp:DropDownList ID="cboOperador" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="False">
                                            <asp:ListItem Value="0">Seleccionar</asp:ListItem>
                                            <asp:ListItem>-</asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>*</asp:ListItem>
                                            <asp:ListItem>/</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlOpcion2" runat="server" Visible="false">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Valor</label>
                                        <div>
                                            <asp:RadioButton ID="rdbValorBase2" runat="server" AutoPostBack="True" GroupName="2" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Movimiento</label>
                                        <div>
                                            <asp:RadioButton ID="rdbMovimiento2" runat="server" AutoPostBack="True" GroupName="2" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Formulas</label>
                                        <div>
                                            <asp:RadioButton ID="rdbFormula2" runat="server" AutoPostBack="True" GroupName="2" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Informados</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemsInfo2" runat="server" AutoPostBack="True" GroupName="2" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Fijos</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemFijo2" runat="server" AutoPostBack="True" GroupName="2" />
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlValor2" runat="server" Visible="false">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-3 form-group ">                                      
                                        <div>
                                            <asp:TextBox ID="txtValorBase2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="cboMovimiento2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboFormulas2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboCantidad2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboMontos2" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>

                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-3 form-group ">
                                    <label for="fname" class="control-label col-form-label">Expresion</label>
                                    <div>
                                        <asp:TextBox ID="txtExpresion" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="False" TextMode="MultiLine" Height="70px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group ">
                                    <label for="fname" class="control-label col-form-label">Tope</label>
                                    <div>
                                        <asp:CheckBox ID="chkTope" runat="server" AutoPostBack="True" />
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlTope1" runat="server" Visible="false">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Valor</label>
                                        <div>
                                            <asp:RadioButton ID="rdbValorBase3" runat="server" AutoPostBack="True" GroupName="3" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Movimiento</label>
                                        <div>
                                            <asp:RadioButton ID="rdbMovimiento3" runat="server" AutoPostBack="True" GroupName="3" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Formulas</label>
                                        <div>
                                            <asp:RadioButton ID="rdbFormula3" runat="server" AutoPostBack="True" GroupName="3" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Informados</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemsInfo3" runat="server" AutoPostBack="True" GroupName="3" />
                                        </div>
                                    </div>

                                    <div class="col-md-1 form-group ">
                                        <label for="fname" class="control-label col-form-label">Items Fijos</label>
                                        <div>
                                            <asp:RadioButton ID="rdbItemFijo3" runat="server" AutoPostBack="True" GroupName="3" />
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlTope" runat="server" Visible="false">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-3 form-group ">                                      
                                        <div>
                                            <asp:TextBox ID="txtValorBase3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:TextBox>
                                            <asp:DropDownList ID="cboMovimientos3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboFormulas3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboMontos3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList ID="cboCantidad3" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                            </asp:Panel>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-6 form-group ">
                                    <div>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group ">
                                    <div>
                                        <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                                    </div>
                                </div>

                                <div class="col-md-1 form-group ">
                                    <div>
                                        <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlDetAnt" runat="server" Enabled="true">
                                <div class="row justify-content-center table-responsive">

                                    <div class="col-md-8">

                                        <asp:GridView ID="grdFormulas" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:CommandField>

                                                <asp:TemplateField HeaderText="Empresa">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("empresa")%>' />
                                                        <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_formula")%>' />
                                                        <asp:HiddenField ID="hdn_item" runat="server" Value='<%# Bind("id_item")%>' />
                                                        <asp:HiddenField ID="hdn_empresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="250px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItem" runat="server" Text='<%# Bind("Item")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Des. Formula" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDesFormula" runat="server" Text='<%# Bind("des_formula")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="300px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Editar">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEditar" runat="server" CommandName="btnEditar" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>'><i class="fas fa-edit"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="40px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>
                                    <div class="col-md-1">
                                    </div>

                                    <div class="col-md-4">

                                        <asp:GridView ID="grdVariables" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Descripción">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nom_item")%>' />
                                                        <asp:HiddenField ID="hdnIdItem" runat="server" Value='<%# Bind("id_item")%>' />
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Incluir">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkIncluir" runat="server" AutoPostBack="true" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="50px" CssClass="EstFilasGrilla" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:View>
                </asp:MultiView>
                <asp:HiddenField ID="hdnTipo1" runat="server" Value="0" />
                <asp:HiddenField ID="hdnTipo2" runat="server" Value="0" />
                <asp:HiddenField ID="hdnTipo3" runat="server" Value="0" />
                <asp:HiddenField ID="hdnContar" runat="server" />
                <asp:HiddenField ID="hdnActualizar" runat="server" Value="0" />
                <asp:HiddenField ID="hdnFormula" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdResolucion" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                        <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>

</asp:Content>
