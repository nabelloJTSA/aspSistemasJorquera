﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.OleDb

Public Class aspPagoWEb
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                Call CargarEmpresa()
                Call CargarSucursal()
                Call CargarAnio()
                Call CargarPeriodo()
                Call CargarPeriodoRut()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
            cboEmpresaHistorial.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaHistorial.DataTextField = "nom_empresa"
            cboEmpresaHistorial.DataValueField = "cod_empresa"
            cboEmpresaHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarSucursal()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCS', '" & cboEmpresa.SelectedValue & "'"
            cboSucursal.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboSucursal.DataTextField = "nom_sucursal"
            cboSucursal.DataValueField = "id_sucursal"
            cboSucursal.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarSucursal", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnio.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio.DataTextField = "anio"
            cboAnio.DataValueField = "id"
            cboAnio.DataBind()
            cboanioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboanioRut.DataTextField = "anio"
            cboanioRut.DataValueField = "id"
            cboanioRut.DataBind()
            cboAnio3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnio3.DataTextField = "anio"
            cboAnio3.DataValueField = "id"
            cboAnio3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorial()
        Try
            Dim strNomTabla As String = "RRHHcab_liquidacion"
            Dim strSQL As String = "Exec pro_RRHH_pagoWeb 'BNP', '" & cboEmpresaHistorial.SelectedValue & "' "
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoRut()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboanioRut.SelectedValue & "'"
            cboPeriodoRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodoRut.DataTextField = "periodo"
            cboPeriodoRut.DataValueField = "id"
            cboPeriodoRut.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnio.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodoESP()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnio3.SelectedValue & "'"
            cboPeriodo3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo3.DataTextField = "periodo"
            cboPeriodo3.DataValueField = "id"
            cboPeriodo3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnProcesar_Click(sender As Object, e As EventArgs) Handles btnProcesar.Click
        Call ObtenerNomina()

    End Sub
    Private Sub ObtenerNomina()
        'Dim cmdTemporal As SqlCommand
        'Dim rdoReader As SqlDataReader

        Dim tipo As String
        If rdbAnt.Checked Then
            tipo = "ANT"
        End If
        If rdbSld.Checked Then
            tipo = "SLD"
        End If

        'Dim strSQL As String = "exec pro_RRHH_pagoWeb 'NNA', " & tipo & ", " & cboEmpresa.SelectedValue & " ," & cboPeriodo.SelectedValue & ""
        'Using cnxAcceso As New SqlConnection(strCnx)
        '    cnxAcceso.Open()
        '    Try
        '        cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
        '        rdoReader = cmdTemporal.ExecuteReader()
        '        If rdoReader.Read Then
        '            hdnNomina.Value = rdoReader(0).ToString
        '            lblNomina.Text = "Nomina Nº:" + hdnNomina.Value
        'Call ArchivoPagoWEB()
        '            lblNomina.Text = ""
        '            'MessageBox("Generar Nomina", "Ya Se genero esta nomina", Page, Master, "W")
        '        Else
        Call ObtenerNominaMAXIMA()
        Call ArchivoPagoWEB()
        lblNomina.Text = ""
        '        End If
        '    Catch ex As Exception
        '        MessageBoxError("GenerarNomina", Err, Page, Master)
        '        Exit Sub
        '    Finally
        '        cnxAcceso.Close()
        '    End Try
        'End Using
    End Sub
    Private Sub ObtenerNominaMAXIMA()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader

        Dim strSQL As String = "exec pro_RRHH_pagoWeb 'NNM'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnNomina.Value = rdoReader(0).ToString
                Else
                    hdnNomina.Value = ""
                End If
            Catch ex As Exception
                MessageBoxError("ObtenerNominaMAXIMA", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Public Function ArchivoPagoWEBRut() As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                Dim tipo As String
                If rdbAntr.Checked Then
                    tipo = "ATR"
                End If
                If rdbSldR.Checked Then
                    tipo = "SDR"
                End If
                strNomArchivo = "Nomina_Rut" & tipo & "_" & hdnNomina.Value & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If
                    File.AppendAllText(strPathFile, Trim(strNomArchivo))
                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

                    strSQL = "exec pro_RRHH_pagoWeb '" & tipo & "','','' ," & cboPeriodoRut.SelectedValue & " ,'','','" & Session("id_usu_tc") & "'"
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))
                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()

                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + strNomArchivo)
                        Response.ContentType = "application/xml"
                        Response.WriteFile(strPathFile)
                        Response.Flush()
                        Response.End()
                    End If
                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPagoWEB", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function

    Public Function ArchivoPagoWEB() As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                Dim tipo As String
                If rdbAnt.Checked Then
                    tipo = "ANT"
                End If
                If rdbSld.Checked Then
                    tipo = "SLD"
                End If
                strNomArchivo = "Nomina_" & tipo & "_" & cboEmpresa.SelectedItem.Text & "_" & hdnNomina.Value & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If
                    File.AppendAllText(strPathFile, Trim(strNomArchivo))
                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

                    strSQL = "exec pro_RRHH_pagoWeb '" & tipo & "'," & Session("cod_usu_tc") & "," & cboEmpresa.SelectedValue & " ," & cboPeriodo.SelectedValue & " ," & cboSucursal.SelectedValue & " ,'','" & Session("id_usu_tc") & "'"
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))
                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()

                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + strNomArchivo)
                        Response.ContentType = "application/xml"
                        Response.WriteFile(strPathFile)
                        Response.Flush()
                        Response.End()
                    End If
                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPagoWEB", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function


    Public Function ArchivoPagoWEBDescarga(ByVal id_nomina As String, ByVal tip_nomina As String) As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                strNomArchivo = "Nomina_" & tip_nomina & "_" & cboEmpresa.SelectedItem.Text & "_" & id_nomina & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If
                    File.AppendAllText(strPathFile, Trim(strNomArchivo))
                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)
                    strSQL = "exec pro_RRHH_pagoWeb 'DNP'," & tip_nomina & " ," & id_nomina & ""
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))
                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()


                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + strNomArchivo)
                        Response.ContentType = "application/xml"
                        Response.WriteFile(strPathFile)
                        Response.Flush()
                        Response.End()
                    End If


                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPagoWEB", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarSucursal()
        'If cboEmpresa.SelectedValue = 1 Then
        '    Call CargarDepto()
        '    lblDepto.Visible = True
        '    cboDepto.Visible = True
        'Else
        '    lblDepto.Visible = False
        '    cboDepto.Visible = False
        'End If
    End Sub




    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged

    End Sub

    Protected Sub btnEERRGeneral_Click(sender As Object, e As EventArgs) Handles btnEERRGeneral.Click
        MultiView1.ActiveViewIndex = 1
    End Sub


    Private Sub grdEmpresas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHistorial.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdHistorial.Rows(index)
        If Trim(LCase(e.CommandName)) = "1" Then
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand

            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_RRHH_pagoWeb"
            comando.Parameters.Add("@busqueda", SqlDbType.Int)
            comando.Parameters("@busqueda").Value = CType(row.FindControl("lblNomina"), Label).Text
            If CType(row.FindControl("hdn_tipo"), HiddenField).Value = "SLD" Then
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "BNS"
            End If
            If CType(row.FindControl("hdn_tipo"), HiddenField).Value = "ANT" Then
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "BNA"
            End If
            comando.Connection = conx
            comando.CommandTimeout = 5000
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call CargarGrillaHistorial()

        ElseIf Trim(LCase(e.CommandName)) = "2" Then

            Call ArchivoPagoWEBDescarga(CType(row.FindControl("lblNomina"), Label).Text, CType(row.FindControl("hdn_tipo"), HiddenField).Value)


        End If
    End Sub

    Protected Sub cboEmpresaHistorial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaHistorial.SelectedIndexChanged
        CargarGrillaHistorial()
    End Sub

    Protected Sub btnRef_Click(sender As Object, e As EventArgs) Handles btnRef.Click
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub btnGenerarRut_Click(sender As Object, e As EventArgs) Handles btnGenerarRut.Click
        MultiView1.ActiveViewIndex = 2
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdRuts)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub
    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function


    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub
    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String))
                                 })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdRuts.DataSource = MostrarExcel(_directorioParaGuardar)
            grdRuts.DataBind()
            Call ObtenerNominaMAXIMA()
            Call ArchivoPagoWEBRut()
            Call EliminarRuts()
            lblNomina.Text = ""
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub

    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Call EliminarRuts()
        Try
            For intRow = 0 To grdRuts.Rows.Count - 1

                Dim objRut As Label = CType(grdRuts.Rows(intRow).FindControl("lblRut"), Label)

                Call GuardarRuts(objRut.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Public Sub GuardarRuts(ByVal rut As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(rut)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Trim(cboPeriodoRut.SelectedValue)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Public Sub EliminarRuts()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "D"
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboAnio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub

    Protected Sub cboanioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboanioRut.SelectedIndexChanged
        Call CargarPeriodoRut()
    End Sub

    Protected Sub cboPeriodoRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoRut.SelectedIndexChanged

    End Sub

    Function MostrarExcelEspecial(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                New DataColumn("valor", GetType(String))
                                 })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut,valor FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)

                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcelEspecial", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function
    Private Sub CargarArchivoEspecial(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcelEspecial(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub
    Protected Sub btnCargarEspecial_Click(sender As Object, e As EventArgs) Handles btnCargarEspecial.Click
        If fuEspecial.HasFile = True Then
            Call CargarArchivoEspecial(fuEspecial, Me.Request, grdEspecial)
            btnGenerarEspecial.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub
    Function GrabarDatosEspecial() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Call EliminarRuts()
        Try
            For intRow = 0 To grdEspecial.Rows.Count - 1

                Dim objRut As Label = CType(grdEspecial.Rows(intRow).FindControl("lblRut"), Label)
                Dim objValor As Label = CType(grdEspecial.Rows(intRow).FindControl("lblValor"), Label)

                Call GuardarRutsEspecial(objRut.Text, objValor.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function
    Public Sub GuardarRutsEspecial(ByVal rut As String, ByVal valor As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IE"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(rut)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Trim(cboPeriodo3.SelectedValue)
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = valor
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub

    Public Function ArchivoPagoWEBEspecial() As Boolean
        Dim strPathFile As String
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSQL As String = "", strNomArchivo As String = ""
        Dim blnError As Boolean = False, strTextoDetalle1 As String = ""
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                strNomArchivo = "Nomina_Rut_" & hdnNomina.Value & ".txt"
                strPathFile = Server.MapPath("~\temp") + "\\" + Trim(strNomArchivo)
                If (String.IsNullOrEmpty(strPathFile) = False) Then
                    Dim toDownExiFile As FileInfo = New FileInfo(strPathFile)
                    If (toDownExiFile.Exists) Then
                        Dim FileToDelete As String = strPathFile
                        If File.Exists(FileToDelete) = True Then
                            File.Delete(FileToDelete)
                        End If
                    End If
                    File.AppendAllText(strPathFile, Trim(strNomArchivo))
                    Dim z_varocioStreamWriter As StreamWriter = New StreamWriter(strPathFile, False, System.Text.Encoding.Default)

                    strSQL = "exec pro_RRHH_pagoWeb 'PWE'"
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    Do While rdoReader.Read
                        strTextoDetalle1 = ""
                        strTextoDetalle1 = Trim(rdoReader(0))
                        z_varocioStreamWriter.WriteLine(strTextoDetalle1)
                    Loop
                    rdoReader.Close()
                    rdoReader = Nothing
                    cmdTemporal.Dispose()
                    cmdTemporal = Nothing
                    z_varocioStreamWriter.Close()

                    Dim toDownOpen As FileInfo = New FileInfo(strPathFile)
                    If (toDownOpen.Exists) Then
                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename = " + strNomArchivo)
                        Response.ContentType = "application/xml"
                        Response.WriteFile(strPathFile)
                        Response.Flush()
                        Response.End()
                    End If
                    blnError = True
                End If
            Catch ex As Exception
                blnError = False
                MessageBoxError("ArchivoPagoWEB", Err, Page, Master)
            Finally
                cnxBaseDatos.Close()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Try
        End Using
        Return blnError
    End Function
    Protected Sub btnGenerarEspecial_Click(sender As Object, e As EventArgs) Handles btnGenerarEspecial.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatosEspecial() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdEspecial.DataSource = MostrarExcel(_directorioParaGuardar)
            grdEspecial.DataBind()
            Call ArchivoPagoWEBEspecial()
            Call EliminarRuts()
            lblNomina.Text = ""
        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub

    Protected Sub btnPWEspecial_Click(sender As Object, e As EventArgs) Handles btnPWEspecial.Click
        MultiView1.ActiveViewIndex = 3
        Call CargarAnio()
    End Sub

    Protected Sub cboAnio3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio3.SelectedIndexChanged
        Call CargarPeriodoESP()
    End Sub
End Class
