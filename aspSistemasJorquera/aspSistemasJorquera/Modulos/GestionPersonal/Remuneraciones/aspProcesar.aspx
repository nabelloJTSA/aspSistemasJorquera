﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspProcesar.aspx.vb" Inherits="aspSistemasJorquera.aspProcesar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnRef" runat="server">Procesar por Depto.</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnEERRGeneral" runat="server">Procesar por Rut</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnReversarG" runat="server">Reversar por Depto.</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnReversarR" runat="server">Reversar por Rut</asp:LinkButton>
        </li>

    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlDatos" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Tipo Trabajador</label>
                            <div>
                                <asp:DropDownList ID="cboTipoTrabajador" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-8 form-group">
                            <asp:Label ID="lblDepto" runat="server" Text="Departamentos:" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:CheckBoxList ID="chkDepto" runat="server" AutoPostBack="True" RepeatColumns="5"></asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5 form-group">
                            <div>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnProcesar" CssClass="btn bg-red btn-block" runat="server">Procesar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnProcesar" runat="server" />
                <asp:HiddenField ID="hdnTodos" runat="server" Value="0" />
                <asp:HiddenField ID="hdnDptos" runat="server" Value="&quot;&quot;" />
            </asp:View>
            <asp:View ID="View2" runat="server">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodoRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 form-group ">
                            <asp:Label ID="lblNombre" runat="server" Font-Bold="True">&nbsp</asp:Label>
                            <div style="height: 5px; width: 5px" class="container"></div>
                            <div>
                                <asp:LinkButton ID="btnProcesarRut" CssClass="btn bg-red btn-block" runat="server">Generar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View3" runat="server">
                <asp:Panel ID="Panel2" runat="server">
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresaRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Tipo Trabajador</label>
                            <div>
                                <asp:DropDownList ID="cboTipoTrabajadorRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodoRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-8 form-group">
                            <asp:Label ID="Label1" runat="server" Text="Departamentos:" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:CheckBoxList ID="chkDeptosRe" runat="server" AutoPostBack="True" RepeatColumns="5"></asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5 form-group">
                            <div>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnDesProcesar" CssClass="btn bg-red btn-block" runat="server">Reversar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnProcesarRe" runat="server" />
                <asp:HiddenField ID="hdnTodosRe" runat="server" Value="0" />
                <asp:HiddenField ID="hdnDeptosRe" runat="server" Value="&quot;&quot;" />
            </asp:View>
            <asp:View ID="View4" runat="server">
                <asp:Panel ID="Panel3" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboAnioRutRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodoRutRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRutRe" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2 form-group ">
                            <asp:Label ID="lblNombreRe" runat="server" Font-Bold="True">&nbsp</asp:Label>
                                <div style="height: 5px; width: 5px" class="container"></div>
                            <div>
                                <asp:LinkButton ID="btnProcesarRutRe" CssClass="btn bg-red btn-block" runat="server">Reversar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
        </asp:MultiView>
    </section>
    <asp:HiddenField ID="HiddenField1" runat="server" />
</asp:Content>
