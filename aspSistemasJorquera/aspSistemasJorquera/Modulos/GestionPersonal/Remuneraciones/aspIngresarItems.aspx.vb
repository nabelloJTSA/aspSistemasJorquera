﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspIngresarItems
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarGrilla("")
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrilla(ByVal Nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_item_variables"
            Dim strSQL As String = "Exec proc_registro_items 'G' , '" & Nombre & "'"
            grdItemPay.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdItemPay.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub


    Private Sub Limpiar()
        txtbNombre.Text = ""
        txtConcepto.Text = ""
        hdnActivo.Value = 0
        Call CargarGrilla("")
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Private Sub guardar_item()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_items"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@nombre_item", SqlDbType.NVarChar)
            comando.Parameters("@nombre_item").Value = txtConcepto.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("guardar_item", Err, Page, Master)
        End Try
    End Sub

    Private Sub actualizar_item()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_items"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@nombre_item", SqlDbType.NVarChar)
            comando.Parameters("@nombre_item").Value = txtConcepto.Text
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdItem.Value

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Actualizado Exitosamente", Page, Master, "S")

            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("actualizar_item", Err, Page, Master)
        End Try
    End Sub

    Private Sub eliminar_item()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_registro_items"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "E"
            comando.Parameters.Add("@id", SqlDbType.NVarChar)
            comando.Parameters("@id").Value = hdnIdItem.Value

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call Limpiar()
            Call CargarGrilla("")

        Catch ex As Exception
            MessageBoxError("eliminar_item", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdItemPay_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdItemPay.PageIndexChanging
        Try
            grdItemPay.PageIndex = e.NewPageIndex
            Call CargarGrilla("")
        Catch ex As Exception
            MessageBoxError("grdItemPay_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdItemPay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdItemPay.SelectedIndexChanged
        Dim row As GridViewRow = grdItemPay.SelectedRow

        Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
        Dim nombre_concepto_obj As Label = CType(row.FindControl("lblNombre"), Label)
        Dim nombre_pay_obj As Label = CType(row.FindControl("lblNombrePay"), Label)
        Dim formula_obj As Label = CType(row.FindControl("lblFormula"), Label)

        hdnIdItem.Value = hdnid.Value

        txtConcepto.Text = nombre_concepto_obj.Text

        hdnActivo.Value = 1
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtConcepto.Text = "" Then
            MessageBox("Guardar", "Debe ingresar datos", Page, Master, "W")
        Else
            If hdnActivo.Value = 0 Then
                Call guardar_item()
            Else
                Call actualizar_item()
            End If
        End If
    End Sub



    Private Sub grdArea_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdItemPay.RowCommand
        Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grdItemPay.Rows(intRow)
        If Trim(LCase(e.CommandName)) = "2" Then
            Call eliminar_item()
        End If
    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        Call CargarGrilla(txtbNombre.Text)
    End Sub
End Class