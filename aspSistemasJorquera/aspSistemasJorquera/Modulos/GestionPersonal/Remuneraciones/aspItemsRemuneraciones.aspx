﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspItemsRemuneraciones.aspx.vb" Inherits="aspSistemasJorquera.aspItemsRemuneraciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Ingresar Ítems"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:MultiView ID="mvItems" runat="server">
                    <asp:View ID="vwIngresar" runat="server">
                        <asp:Panel ID="Panel1" runat="server" Enabled="true">
                            <div class="row justify-content-center table-responsive">

                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">Empresa</label>
                                    <div>
                                        <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Año</label>
                                    <div>
                                        <asp:DropDownList ID="cboAnio" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Periodo</label>
                                    <div>
                                        <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label for="fname" class="control-label col-form-label">Areas</label>
                                    <div>
                                        <asp:DropDownList ID="cboAreas" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center table-responsive">

                                <div class="col-md-2 form-group">
                                    <label for="fname" class="control-label col-form-label">Personal</label>
                                    <div>
                                        <asp:TextBox ID="txtNombreHorasExtra" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                        <asp:DropDownList ID="cboBuscarNombre" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True" Visible="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                    <div>
                                        <asp:LinkButton ID="bntBuscarNombreHorasExtra" runat="server" CssClass="btn" ><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Rut</label>
                                    <div>
                                        <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <label for="fname" class="control-label col-form-label">Fecha</label>
                                    <div>
                                        <asp:TextBox ID="dtFechaIni" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Valor</label>
                                    <div>
                                        <asp:TextBox ID="txtValor" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Detalle</label>
                                    <div>
                                        <asp:TextBox ID="txtDetalle" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-6 form-group ">
                                    <div>
                                    </div>
                                </div>

                                <div class="col-md-1 form-group ">
                                    <div>
                                        <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server" Enabled="False">Cargar</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Seleccione Item:</label>
                                    <div>
                                        <asp:DropDownList ID="cboItems" runat="server" CssClass="form-control text-uppercase" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">Mostrar Todos</label>
                                    <div>
                                        <asp:CheckBox ID="chkTI" runat="server" AutoPostBack="True" /></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-3 form-group">
                                    <label for="fname" class="control-label col-form-label">Rut</label>
                                    <div>
                                        <asp:TextBox ID="txtBuscarNom" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-1 form-group">
                                    <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                    <div>
                                        <asp:LinkButton ID="BtnNBuscar" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                                    </div>
                                </div>

                                <div class="col-md-1 form-group ">
                                      <label for="fname" class="control-label col-form-label">&nbsp;</label>
                                    <div>
                                        <asp:LinkButton ID="bntIngresados" CssClass="btn bg-green btn-block" runat="server" ToolTip="Exportar Reumen Total"> <i class="fa fa-file-excel-o"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-1 form-group ">
                                    <div>
                                        <asp:Label ID="lblFielTotal" runat="server" Text="Item"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center table-responsive">
                                <div class="col-md-12">

                                    <asp:GridView ID="grdHorasExtra" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Rut">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' />
                                                    <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_item_personal")%>' />

                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Nombre">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombrepersonal")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="250px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Area">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nombrearea")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="200px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNomItem" runat="server" Text='<%# Bind("nombreitem")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valor">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtValorGRilla" runat="server" Width="100px" Text='<%# Bind("num_valor")%>' AutoPostBack="true" OnTextChanged="txtValorActualizar"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="90px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Detalle">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtDetalleGRilla" runat="server" Width="200px" Text='<%# Bind("obs_detalle")%>' AutoPostBack="true" OnTextChanged="txtDetalleActualizar"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fecha">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTermino" runat="server" Text='<%# Bind("fec_ingreso", "{0:d}")%>' />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                                    </asp:GridView>
                                </div>
                            </div>

                        </asp:Panel>
                    </asp:View>
                </asp:MultiView>
                <asp:HiddenField ID="hdnCantItems" runat="server" Value="0" />
                <asp:HiddenField ID="hdnIdItem" runat="server" />
                <asp:HiddenField ID="hdnDia" runat="server" />
                <asp:HiddenField ID="hdnHora" runat="server" />
                <asp:HiddenField ID="hdnPeriodoActual" runat="server" />
                <asp:HiddenField ID="hdnPeriodoCbo" runat="server" />
                <asp:HiddenField ID="hdnPeriodoCaducado" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdateProgress ID="updProgress2" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 98; top: 0px; left: 0px; right: 0px; bottom: 0px; background-color: #aaa; filter: alpha(opacity=80); opacity: 0.8;">

                    <div style="z-index: 99; margin: 250px auto; width: 250px; height: 40px;">
                        <asp:Label ID="Label1" runat="server" Text="Cargando..." Font-Bold="True" Font-Italic="True" Font-Names="Arial" ForeColor="#003399"></asp:Label>
                        <asp:Image ID="Image1" ImageUrl="~/Imagen/gif/cargando3.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </section>

</asp:Content>
