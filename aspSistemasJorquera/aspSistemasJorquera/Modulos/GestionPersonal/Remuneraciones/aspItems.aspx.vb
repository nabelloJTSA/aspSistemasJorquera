﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspItems
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarEmpresa()
                mtvItems.ActiveViewIndex = 0
                Call CargarGrilla(cboEmpresa.SelectedValue)
                Session("dtItems") = CrearTablaDetalle()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Function CrearTablaDetalle() As DataTable
        Dim dtTemporal As New DataTable
        Try
            dtTemporal.Columns.Add("nom_sucursal", Type.GetType("System.String"))
            dtTemporal.Columns.Add("nom_instrumento", Type.GetType("System.String"))
            dtTemporal.Columns.Add("id_item", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_sucursal", Type.GetType("System.String"))
            dtTemporal.Columns.Add("cod_instrumento", Type.GetType("System.String"))
            dtTemporal.Columns.Add("des_detalle", Type.GetType("System.String"))
            dtTemporal.Columns.Add("des_criterio", Type.GetType("System.String"))
            dtTemporal.Columns.Add("est_pago_diferido", Type.GetType("System.String"))
            dtTemporal.Columns.Add("num_meses", Type.GetType("System.String"))
            dtTemporal.Columns.Add("num_valor", Type.GetType("System.String"))
            dtTemporal.Columns.Add("usr_add", Type.GetType("System.String"))
        Catch ex As Exception
            MessageBoxError("CrearTablaDetalle", Err, Page, Master)
        End Try
        Return dtTemporal
    End Function

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec mant_RRHHMae_item_remuneracion 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla(ByVal empresa As String)
        Try
            Dim strNomTabla As String = "RRHHMae_item_remuneracion"
            Dim strSQL As String = "Exec mant_RRHHMae_item_remuneracion 'G', '" & empresa & "'"
            grdItems.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdItems.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub Guardar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_item_remuneracion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@nom_item", SqlDbType.NVarChar)
            comando.Parameters("@nom_item").Value = Trim(txtNombreItem.Text)
            comando.Parameters.Add("@des_item", SqlDbType.NVarChar)
            comando.Parameters("@des_item").Value = Trim(txtDesItem.Text)
            comando.Parameters.Add("@item_imponible", SqlDbType.NVarChar)
            comando.Parameters("@item_imponible").Value = cboImponible.SelectedValue
            comando.Parameters.Add("@est_tributable", SqlDbType.Bit)
            comando.Parameters("@est_tributable").Value = chkTributable.Checked
            comando.Parameters.Add("@item_tipo", SqlDbType.NVarChar)
            comando.Parameters("@item_tipo").Value = cboTipo.SelectedValue
            comando.Parameters.Add("@est_reajuste", SqlDbType.Bit)
            comando.Parameters("@est_reajuste").Value = chkReajuste.Checked
            comando.Parameters.Add("@est_hhee", SqlDbType.Bit)
            comando.Parameters("@est_hhee").Value = chkHHEE.Checked
            comando.Parameters.Add("@est_asig_vac", SqlDbType.Bit)
            comando.Parameters("@est_asig_vac").Value = chkAsigVac.Checked
            comando.Parameters.Add("@est_ias", SqlDbType.Bit)
            comando.Parameters("@est_ias").Value = chkIas.Checked
            comando.Parameters.Add("@est_vac_pro", SqlDbType.Bit)
            comando.Parameters("@est_vac_pro").Value = chkVacPro.Checked
            comando.Parameters.Add("@ref_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@ref_instrumento").Value = Trim(txtRefInstrumento.Text)
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Parameters.Add("@num_orden", SqlDbType.NVarChar)
            comando.Parameters("@num_orden").Value = txtOrden.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Guardar", "Registro Guardado", Page, Master, "S")
            Call CargarGrilla(cboEmpresa.SelectedValue)
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHHMae_item_remuneracion"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@nom_item", SqlDbType.NVarChar)
            comando.Parameters("@nom_item").Value = Trim(txtNombreItem.Text)
            comando.Parameters.Add("@des_item", SqlDbType.NVarChar)
            comando.Parameters("@des_item").Value = Trim(txtDesItem.Text)
            comando.Parameters.Add("@item_imponible", SqlDbType.NVarChar)
            comando.Parameters("@item_imponible").Value = cboImponible.SelectedValue
            comando.Parameters.Add("@est_tributable", SqlDbType.Bit)
            comando.Parameters("@est_tributable").Value = chkTributable.Checked
            comando.Parameters.Add("@item_tipo", SqlDbType.NVarChar)
            comando.Parameters("@item_tipo").Value = cboTipo.SelectedValue
            comando.Parameters.Add("@est_reajuste", SqlDbType.Bit)
            comando.Parameters("@est_reajuste").Value = chkReajuste.Checked
            comando.Parameters.Add("@est_hhee", SqlDbType.Bit)
            comando.Parameters("@est_hhee").Value = chkHHEE.Checked
            comando.Parameters.Add("@est_asig_vac", SqlDbType.Bit)
            comando.Parameters("@est_asig_vac").Value = chkAsigVac.Checked
            comando.Parameters.Add("@est_ias", SqlDbType.Bit)
            comando.Parameters("@est_ias").Value = chkIas.Checked
            comando.Parameters.Add("@est_vac_pro", SqlDbType.Bit)
            comando.Parameters("@est_vac_pro").Value = chkVacPro.Checked
            comando.Parameters.Add("@ref_instrumento", SqlDbType.NVarChar)
            comando.Parameters("@ref_instrumento").Value = Trim(txtRefInstrumento.Text)
            comando.Parameters.Add("@usr_edit", SqlDbType.NVarChar)
            comando.Parameters("@usr_edit").Value = Session("id_usu_tc")
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = hdnId.Value
            comando.Parameters.Add("@num_orden", SqlDbType.NVarChar)
            comando.Parameters("@num_orden").Value = txtOrden.Text
            comando.Parameters.Add("@busqueda2 ", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2 ").Value = hdnExisteDetalle.Value
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
            Call CargarGrilla(cboEmpresa.SelectedValue)
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla(cboEmpresa.SelectedValue)
        Call Limpiar()
    End Sub

    Private Sub Limpiar()
        txtNombreItem.Text = ""
        txtDesItem.Text = ""
        cboImponible.ClearSelection()
        cboTipo.ClearSelection()
        txtRefInstrumento.Text = ""
        chkTributable.Checked = False
        chkReajuste.Checked = False
        chkHHEE.Checked = False
        chkAsigVac.Checked = False
        chkIas.Checked = False
        chkVacPro.Checked = False
        hdnActivo.Value = 0
        Call CargarGrilla(cboEmpresa.SelectedValue)
        hdnExisteDetalle.Value = 0
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Call Limpiar()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If cboEmpresa.SelectedValue = 0 Then
            MessageBox("Guardar", "Seleccione Empresa y Cargo", Page, Master, "W")
        Else
            If hdnActivo.Value = 1 Then
                Call Actualizar()
            Else
                Call Guardar()
            End If
        End If
    End Sub

    Protected Sub grdItems_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdItems.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdItems.SelectedRow
            Dim obj_Id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
            Dim obj_hdnCodEmp As HiddenField = CType(row.FindControl("hdnCodEmp"), HiddenField)
            Dim obj_hdnImponible As HiddenField = CType(row.FindControl("hdnImponible"), HiddenField)
            Dim obj_hdnTributable As HiddenField = CType(row.FindControl("hdnTributable"), HiddenField)
            Dim obj_hdnTipo As HiddenField = CType(row.FindControl("hdnTipo"), HiddenField)
            Dim obj_hdnReajuste As HiddenField = CType(row.FindControl("hdnReajuste"), HiddenField)
            Dim obj_hdnHHEE As HiddenField = CType(row.FindControl("hdnHHEE"), HiddenField)
            Dim obj_hdnAsigVac As HiddenField = CType(row.FindControl("hdnAsigVac"), HiddenField)
            Dim obj_hdnIas As HiddenField = CType(row.FindControl("hdnIas"), HiddenField)
            Dim obj_hdnVacPro As HiddenField = CType(row.FindControl("hdnVacPro"), HiddenField)
            Dim obj_hdnRefIns As HiddenField = CType(row.FindControl("hdnRefIns"), HiddenField)
            Dim obj_numOrden As Label = CType(row.FindControl("lblOrden"), Label)
            Dim obj_NomItem As Label = CType(row.FindControl("lblNomItem"), Label)
            Dim obj_DesItem As Label = CType(row.FindControl("lblDesItem"), Label)

            hdnId.Value = obj_Id.Value
            cboEmpresa.SelectedValue = obj_hdnCodEmp.Value
            txtNombreItem.Text = obj_NomItem.Text
            txtDesItem.Text = obj_DesItem.Text
            cboImponible.SelectedValue = obj_hdnImponible.Value
            cboTipo.SelectedValue = obj_hdnTipo.Value
            txtRefInstrumento.Text = obj_hdnRefIns.Value
            txtOrden.Text = obj_numOrden.Text
            Call VerificarDetalleItem(hdnId.Value)

            If obj_hdnTributable.Value = True Then
                chkTributable.Checked = True
            Else
                chkTributable.Checked = False
            End If

            If obj_hdnReajuste.Value = True Then
                chkReajuste.Checked = True
            Else
                chkReajuste.Checked = False
            End If

            If obj_hdnHHEE.Value = True Then
                chkHHEE.Checked = True
            Else
                chkHHEE.Checked = False
            End If

            If obj_hdnAsigVac.Value = True Then
                chkAsigVac.Checked = True
            Else
                chkAsigVac.Checked = False
            End If

            If obj_hdnIas.Value = True Then
                chkIas.Checked = True
            Else
                chkIas.Checked = False
            End If

            If obj_hdnVacPro.Value = True Then
                chkVacPro.Checked = True
            Else
                chkVacPro.Checked = False
            End If

            hdnActivo.Value = 1
        Catch ex As Exception
            MessageBoxError("grdCausales_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub VerificarDetalleItem(ByVal id_item As String)
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHHMae_item_remuneracion 'RDE', '" & id_item & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnExisteDetalle.Value = 1
                Else
                    hdnExisteDetalle.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("VerificarDetalleItem", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub grdItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdItems.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdItems.Rows(intRow)
                Dim obj_Emp As HiddenField = CType(row.FindControl("hdnCodEmp"), HiddenField)
                Dim obj_IdItem As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                hdnIdItem.Value = obj_IdItem.Value
                mtvItems.ActiveViewIndex = 1
            End If
        Catch ex As Exception
            MessageBoxError("grdIngPrestamos_RowCommand", Err, Page, Master)
        End Try
    End Sub
End Class
