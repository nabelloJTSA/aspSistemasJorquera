﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspLiquidaciones.aspx.vb" Inherits="aspSistemasJorquera.aspLiquidaciones" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnFiltroGeneral" runat="server">Filtro General</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnFiltroNomina" runat="server">Filtro Nomina</asp:LinkButton>
        </li>

    </ul>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltitulo" runat="server" Text="Liquidaciones"></asp:Label>
                </h2>
            </div>
        </div>
    </section>
    <section class="content">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <asp:Panel ID="pnlCabSue" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Rut</label>
                            <div>
                                <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Empresa</label>
                            <div>
                                <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Tipo Trabajador</label>
                            <div>
                                <asp:DropDownList ID="cboTipoTrabajador" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Año</label>
                            <div>
                                <asp:DropDownList ID="cboanio" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">Periodo</label>
                            <div>
                                <asp:DropDownList ID="cboPeriodo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <asp:Label ID="Label2" runat="server" Text="Departamentos:" Font-Bold="True"></asp:Label>
                            <div>
                                <asp:RadioButton ID="rdbDeptos" runat="server" AutoPostBack="true" GroupName="1" Text="Deptos" />
                                <asp:RadioButton ID="rdbSucursales" runat="server" AutoPostBack="true" GroupName="1" Text="Sucursales" />

                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlDepto" runat="server" Visible="false">
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-5 form-group">
                                <asp:CheckBoxList ID="chkDepto" runat="server" AutoPostBack="True" RepeatColumns="5"></asp:CheckBoxList>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlSucursal" runat="server" Visible="false">
                        <div class="row justify-content-center table-responsive">
                            <div class="col-md-5 form-group">
                                <asp:CheckBoxList ID="chkSucursal" runat="server" AutoPostBack="True" RepeatColumns="5"></asp:CheckBoxList>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-5 form-group">
                            <div>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp</label>
                            <div>
                                <asp:LinkButton ID="btnExportar" CssClass="btn bg-red btn-block" runat="server">Generar Reporte</asp:LinkButton>
                                <asp:HiddenField ID="hdnTodos" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnDptos" runat="server" Value="&quot;&quot;" />
                                <asp:HiddenField ID="hdnTodosSuc" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnTipoBusqueda" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnSucursal" runat="server" Value="&quot;&quot;" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Empresa</label>
                        <div>
                            <asp:DropDownList ID="cboEmpresaNomina" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Año</label>
                        <div>
                            <asp:DropDownList ID="cboAnioNomina" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">Periodo</label>
                        <div>
                            <asp:DropDownList ID="cboPeriodoNomina" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Archivo</label>
                        <div>
                            <asp:FileUpload ID="fuplCargar" runat="server" CssClass="form-control text-uppercase" />
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp</label>
                        <div>
                            <asp:LinkButton ID="btnCargar" CssClass="btn bg-orange btn-block" runat="server">Cargar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp</label>
                        <div>
                            <asp:LinkButton ID="btnGuardar" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center table-responsive">

                    <div class="col-md-5">

                        <asp:GridView ID="grdRuts" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" Visible="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Rut">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut")%>' />
                                    </ItemTemplate>
                                    <ItemStyle CssClass="EstFilasGrilla" Width="100px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
        <div class="row justify-content-center table-responsive">

            <div class="col-md-10">

                <asp:Panel ID="pnlDetSue" runat="server" Enabled="true">


                    <rsreport:ReportViewer ID="rptVisor" runat="server" Font-Names="Arial" Font-Size="8pt" waitmessagefont-names="Verdana"
                        waitmessagefont-size="14pt" Height="100%" PromptAreaCollapsed="true" AsyncRendering="false" SizeToReportContent="true">
                    </rsreport:ReportViewer>

                </asp:Panel>
            </div>
        </div>
    </section>
</asp:Content>
