﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspVistaPrevia
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
            If Not Page.IsPostBack Then

                Call CargarAnio()
                Call CargarPeriodo()
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnio()
        Try
            Dim strNomTablaR As String = "remmae_periodo"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CCA'"
            cboAnioRut.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboAnioRut.DataTextField = "anio"
            cboAnioRut.DataValueField = "id"
            cboAnioRut.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarAnio", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarPeriodo()
        Try
            Dim strNomTablaR As String = "RRHHMae_Area"
            Dim strSQLR As String = "Exec pro_RRHHM_procesar_sueldos 'CP','" & cboAnioRut.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarPeriodo", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "RRHHMae_plantilla"
            Dim strSQL As String = "Exec pro_RRHH_Preliquidacion 'CPV','" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "' "
            grdVistaPrevia.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdVistaPrevia.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnVistaPrevia_Click(sender As Object, e As EventArgs) Handles btnVistaPrevia.Click
        If txtRut.Text = "" Then
            MessageBox("Vista Previa", "Debe Ingresar Rut", Page, Master, "W")
        Else
            Call DatosPersona()
            If ValidarIngresos() = True Then
                Call CargarGrilla()
            End If
        End If
    End Sub
    Function RevisarIngresos(ByVal tipo As String) As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_liquidacion '" & tipo & "' , '" & hdnEmpresa.Value & "', '" & hdnSucursal.Value & "', '" & cboPeriodo.SelectedValue & "'"
        Dim estado As Boolean = 0
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    estado = 1
                Else
                    estado = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RevisarExisteProceso", Err, Page, Master)
            End Try
        End Using
        Return estado
    End Function

    Function ValidarIngresos() As Boolean
        Dim VL As Boolean = True
        If RevisarIngresos("VIP") = 0 Then
            MessageBox("Vista Previa", "No se ha ingresado PRODUCCION", Page, Master, "W")
            VL = False
        End If
        If RevisarIngresos("VIO") = 0 Then
            MessageBox("Vista Previa", "No se ha ingresado OTROS DESC.", Page, Master, "W")
            VL = False
        End If
        If RevisarIngresos("VIA") = 0 Then
            MessageBox("Vista Previa", "No se ha ingresado ASIG. VACACIONES", Page, Master, "W")
            VL = False
        End If
        Return VL
    End Function
    Private Sub DatosPersona()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec pro_RRHH_Preliquidacion 'CDI', '" & txtRut.Text & "','" & cboPeriodo.SelectedValue & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    lblnombre.Text = rdoReader(0).ToString
                    lblEmpresa.Text = rdoReader(1).ToString
                    lblSucursal.Text = rdoReader(2).ToString
                    lblCargo.Text = rdoReader(3).ToString
                    lblBeneficio.Text = rdoReader(4).ToString
                    lblPlantilla.Text = rdoReader(5).ToString
                    hdnEmpresa.Value = rdoReader(6).ToString
                    hdnSucursal.Value = rdoReader(7).ToString
                    hdnSucursal.Value = rdoReader(8).ToString
                    lblDepto.Text = rdoReader(9).ToString
                    lblDias.Text = rdoReader(10).ToString


                    pnlDet.Visible = True

                Else
                    lblnombre.Text = ""
                    lblEmpresa.Text = ""
                    lblSucursal.Text = ""
                    lblCargo.Text = ""
                    hdnEmpresa.Value = 0
                    hdnSucursal.Value = 0
                    pnlDet.Visible = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("DatosPersona", Err, Page, Master)
            End Try
        End Using
    End Sub

    Public Sub ValidarPermisosDeptos()

        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_ficha_personal 'VPD' , '" & txtRut.Text & "', '" & Session("cod_usu_tc") & "'"

        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    btnVistaPrevia.Enabled = True
                Else
                    MessageBox("Permisos", "No puede generar vista previa de esta persona", Page, Master, "W")
                    btnVistaPrevia.Enabled = False
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("ValidarPermisosDeptos", Err, Page, Master)
            End Try
        End Using
    End Sub


    Protected Sub TxtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnVistaPrevia.Enabled = True
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnVistaPrevia.Enabled = False
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                btnVistaPrevia.Enabled = True
                Call ValidarPermisosDeptos()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                btnVistaPrevia.Enabled = False
            End If
        End If
    End Sub

    Protected Sub cboAnioRut_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnioRut.SelectedIndexChanged
        Call CargarPeriodo()
    End Sub
End Class