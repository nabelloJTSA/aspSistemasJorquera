﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspFormulas
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call CargarEmpresa()
                Call CargarGridFormulas()
                Call CargarItem()
                mvFormulas.ActiveViewIndex = 0
            End If
        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarEmpresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarFormulas()
        Try
            Dim strNomTablaR As String = "RRHHCab_formula"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CCF' ,'" & cboEmpresa.SelectedValue & "'"
            cboFormulas1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFormulas1.DataTextField = "des_formula"
            cboFormulas1.DataValueField = "id_formula"
            cboFormulas1.DataBind()

            cboFormulas2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFormulas2.DataTextField = "des_formula"
            cboFormulas2.DataValueField = "id_formula"
            cboFormulas2.DataBind()

            cboFormulas3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFormulas3.DataTextField = "des_formula"
            cboFormulas3.DataValueField = "id_formula"
            cboFormulas3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarEmpresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGridFormulas()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'G','" & cboEmpresa.SelectedValue & "'"
            grdFormulas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            grdFormulas.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGridFormulas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarMovimientos()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CCM'"
            cboMovimiento1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMovimiento1.DataTextField = "nom_tipo_movimiento"
            cboMovimiento1.DataValueField = "cod_tipo_movimiento"
            cboMovimiento1.DataBind()

            cboMovimiento2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMovimiento2.DataTextField = "nom_tipo_movimiento"
            cboMovimiento2.DataValueField = "cod_tipo_movimiento"
            cboMovimiento2.DataBind()

            cboMovimientos3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMovimientos3.DataTextField = "nom_tipo_movimiento"
            cboMovimientos3.DataValueField = "cod_tipo_movimiento"
            cboMovimientos3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarMovimientos", Err, Page, Master)
        End Try
    End Sub
    Private Sub CargarItem()
        Try
            Dim strNomTablaR As String = "RRHHMae_item_remuneracion"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CCI','" & cboEmpresa.SelectedValue & "'"
            cboItem.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboItem.DataTextField = "nom_item"
            cboItem.DataValueField = "id_item"
            cboItem.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarItem", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarItemFijo()
        Try
            Dim strNomTablaR As String = "RRHHMae_item_remuneracion"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CCI','" & cboEmpresa.SelectedValue & "'"
            cboMontos1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMontos1.DataTextField = "nom_item"
            cboMontos1.DataValueField = "id_item"
            cboMontos1.DataBind()

            cboMontos2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMontos2.DataTextField = "nom_item"
            cboMontos2.DataValueField = "id_item"
            cboMontos2.DataBind()

            cboMontos3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboMontos3.DataTextField = "nom_item"
            cboMontos3.DataValueField = "id_item"
            cboMontos3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarItemFijo", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarItemVariable()
        Try
            Dim strNomTablaR As String = "REMMae_item_variables"
            Dim strSQLR As String = "Exec mant_RRHH_formula 'CIV'"
            cboCantidad1.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCantidad1.DataTextField = "nom_item_variables"
            cboCantidad1.DataValueField = "id_item_variables"
            cboCantidad1.DataBind()

            cboCantidad2.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCantidad2.DataTextField = "nom_item_variables"
            cboCantidad2.DataValueField = "id_item_variables"
            cboCantidad2.DataBind()

            cboCantidad3.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboCantidad3.DataTextField = "nom_item_variables"
            cboCantidad3.DataValueField = "id_item_variables"
            cboCantidad3.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarItemVariable", Err, Page, Master)
        End Try
    End Sub

    Protected Sub rdbValorBase1_CheckedChanged(sender As Object, e As EventArgs) Handles rdbValorBase1.CheckedChanged
        txtValorBase1.Visible = True
        cboCantidad1.Visible = False
        cboFormulas1.Visible = False
        cboMontos1.Visible = False
        cboMovimiento1.Visible = False
        hdnTipo1.Value = 1
        pnlValor1.Visible = True
    End Sub

    Protected Sub rdbMovimiento1_CheckedChanged(sender As Object, e As EventArgs) Handles rdbMovimiento1.CheckedChanged
        txtValorBase1.Visible = False
        cboCantidad1.Visible = False
        cboFormulas1.Visible = False
        cboMontos1.Visible = False
        cboMovimiento1.Visible = True
        hdnTipo1.Value = 2
        pnlValor1.Visible = True
    End Sub

    Protected Sub rdbFormula1_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFormula1.CheckedChanged
        txtValorBase1.Visible = False
        cboCantidad1.Visible = False
        cboFormulas1.Visible = True
        cboMontos1.Visible = False
        cboMovimiento1.Visible = False
        hdnTipo1.Value = 3
        pnlValor1.Visible = True
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarItem()
        Call CargarMovimientos()
        Call CargarFormulas()
        Call CargarItemFijo()
        Call CargarItemVariable()
        Call CargarGridFormulas()
    End Sub

    Protected Sub txtValorBase1_TextChanged(sender As Object, e As EventArgs) Handles txtValorBase1.TextChanged
        txtExpresion.Text = txtValorBase1.Text
        cboOperador.Enabled = True
    End Sub

    Protected Sub cboOperador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOperador.SelectedIndexChanged
        txtExpresion.Text = txtExpresion.Text + " " + cboOperador.SelectedItem.Text
        Call Activar2doPaso()
    End Sub

    Protected Sub rdbMovimiento2_CheckedChanged(sender As Object, e As EventArgs) Handles rdbMovimiento2.CheckedChanged
        txtValorBase2.Visible = False
        cboCantidad2.Visible = False
        cboFormulas2.Visible = False
        cboMontos2.Visible = False
        cboMovimiento2.Visible = True
        hdnTipo2.Value = 2
    End Sub

    Protected Sub txtValorBase2_TextChanged(sender As Object, e As EventArgs) Handles txtValorBase2.TextChanged
        txtExpresion.Text = txtExpresion.Text + " " + txtValorBase2.Text
    End Sub

    Protected Sub cboMovimiento1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMovimiento1.SelectedIndexChanged
        txtExpresion.Text = cboMovimiento1.SelectedItem.Text
        cboOperador.Enabled = True
    End Sub

    Protected Sub cboMovimiento2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMovimiento2.SelectedIndexChanged
        txtExpresion.Text = txtExpresion.Text + " " + cboMovimiento2.SelectedItem.Text
    End Sub

    Protected Sub rdbValorBase2_CheckedChanged(sender As Object, e As EventArgs) Handles rdbValorBase2.CheckedChanged
        txtValorBase2.Visible = True
        cboCantidad2.Visible = False
        cboFormulas2.Visible = False
        cboMontos2.Visible = False
        cboMovimiento2.Visible = False
        hdnTipo2.Value = 1
    End Sub

    Protected Sub rdbFormula2_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFormula2.CheckedChanged
        txtValorBase2.Visible = False
        cboCantidad2.Visible = False
        cboFormulas2.Visible = True
        cboMontos2.Visible = False
        cboMovimiento2.Visible = False
        hdnTipo2.Value = 3
    End Sub

    Private Sub GuardarDet()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_formula"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "ID"
            comando.Parameters.Add("@id_formula", SqlDbType.NVarChar)
            comando.Parameters("@id_formula").Value = txtFormula.Text
            comando.Parameters.Add("@tip_valor1", SqlDbType.NVarChar)
            comando.Parameters("@tip_valor1").Value = hdnTipo1.Value
            If hdnTipo1.Value = 1 Then 'base
                comando.Parameters.Add("@num_valor1", SqlDbType.NVarChar)
                comando.Parameters("@num_valor1").Value = txtValorBase1.Text
            End If
            If hdnTipo1.Value = 2 Then 'movimiento
                comando.Parameters.Add("@num_valor1", SqlDbType.NVarChar)
                comando.Parameters("@num_valor1").Value = cboMovimiento1.SelectedValue
            End If
            If hdnTipo1.Value = 3 Then 'formula
                comando.Parameters.Add("@num_valor1", SqlDbType.NVarChar)
                comando.Parameters("@num_valor1").Value = cboFormulas1.SelectedValue
            End If
            If hdnTipo1.Value = 4 Then ' items
                comando.Parameters.Add("@num_valor1", SqlDbType.NVarChar)
                comando.Parameters("@num_valor1").Value = cboCantidad1.SelectedValue
            End If
            If hdnTipo1.Value = 5 Then ' beneficios
                comando.Parameters.Add("@num_valor1", SqlDbType.NVarChar)
                comando.Parameters("@num_valor1").Value = cboMontos1.SelectedValue
            End If
            If chkTope.Checked = False Then
                comando.Parameters.Add("@tip_operador", SqlDbType.NVarChar)
                comando.Parameters("@tip_operador").Value = cboOperador.SelectedValue
                comando.Parameters.Add("@tip_valor2", SqlDbType.NVarChar)
                comando.Parameters("@tip_valor2").Value = hdnTipo2.Value
                If hdnTipo2.Value = 1 Then 'base
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = txtValorBase2.Text
                End If
                If hdnTipo2.Value = 2 Then 'movimiento
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboMovimiento2.Text
                End If
                If hdnTipo2.Value = 3 Then 'formula
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboFormulas2.Text
                End If
                If hdnTipo2.Value = 4 Then ' informado
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboCantidad2.SelectedValue
                End If
                If hdnTipo2.Value = 5 Then ' fijo
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboMontos2.SelectedValue
                End If
            Else
                comando.Parameters.Add("@tip_operador", SqlDbType.NVarChar)
                comando.Parameters("@tip_operador").Value = "="
                comando.Parameters.Add("@tip_valor2", SqlDbType.NVarChar)
                comando.Parameters("@tip_valor2").Value = hdnTipo3.Value
                If hdnTipo3.Value = 1 Then 'base
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = txtValorBase3.Text
                End If
                If hdnTipo3.Value = 2 Then 'movimiento
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboMovimientos3.Text
                End If
                If hdnTipo3.Value = 3 Then 'formula
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboFormulas3.Text
                End If
                If hdnTipo3.Value = 4 Then ' informado
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboCantidad3.SelectedValue
                End If
                If hdnTipo3.Value = 5 Then ' fijo
                    comando.Parameters.Add("@num_valor2", SqlDbType.NVarChar)
                    comando.Parameters("@num_valor2").Value = cboMontos3.SelectedValue
                End If
            End If

            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("id_usu_tc")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarDet", Err, Page, Master)
        End Try
    End Sub
    Private Sub GuardarCab()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_formula"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IC"
            comando.Parameters.Add("@id_item", SqlDbType.NVarChar)
            comando.Parameters("@id_item").Value = cboItem.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@des_formula", SqlDbType.NVarChar)
            comando.Parameters("@des_formula").Value = txtDescripcion.Text
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarCab", Err, Page, Master)
            txtFormula.Text = ""
        End Try
    End Sub

    Private Sub RescartarFormula()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_formula 'RUF'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    txtFormula.Text = rdoReader(0).ToString
                Else
                    txtFormula.Text = ""
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescartarFormula", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub RescataPasoAnterior()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_formula 'RPA','" & txtFormula.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    txtExpresion.Text = rdoReader(0).ToString
                Else
                    txtExpresion.Text = ""
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescataPasoAnterior", Err, Page, Master)
            End Try
        End Using
    End Sub

    Function ExisteFormula()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_formula 'EF','" & cboItem.SelectedValue & "'"
        Dim estado As Integer
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    estado = 1
                Else
                    estado = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescataPasoAnterior", Err, Page, Master)
            End Try
        End Using
        Return estado
    End Function

    Private Sub ContarDetalle()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Exec mant_RRHH_formula 'CDF','" & txtFormula.Text & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnContar.Value = rdoReader(0)
                Else
                    hdnContar.Value = 0
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("RescataPasoAnterior", Err, Page, Master)
            End Try
        End Using
    End Sub
    Protected Sub cboMontos1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMontos1.SelectedIndexChanged
        txtExpresion.Text = cboMontos1.SelectedItem.Text
        cboOperador.Enabled = True
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If txtFormula.Text = "" Then
            If rdbItemsInfo1.Checked = False And rdbFormula1.Checked = False And rdbItemFijo1.Checked = False And
                rdbMovimiento1.Checked = False And rdbValorBase1.Checked = False Then
                MessageBox("Formulas", "Debe Seleccionar un tipo de Dato 1", Page, Master, "W")
            Else
                If txtValorBase1.Text = "" And cboCantidad1.SelectedValue = 0 And cboMontos1.SelectedValue = 0 And
                        cboFormulas1.SelectedValue = 0 And cboMovimiento1.SelectedValue = 0 Then
                    MessageBox("Formulas", "Debe Ingresar o Seleccionar el valor 1", Page, Master, "W")
                Else
                    If cboOperador.SelectedValue = "0" Then
                        MessageBox("Formulas", "Debe Seleccionar un operador", Page, Master, "W")
                    Else
                        If rdbItemsInfo2.Checked = False And rdbFormula2.Checked = False And rdbItemFijo2.Checked = False And
                             rdbMovimiento2.Checked = False And rdbValorBase2.Checked = False Then
                            MessageBox("Formulas", "Debe Seleccionar un tipo de Dato 2", Page, Master, "W")
                        Else
                            If txtValorBase2.Text = "" And cboCantidad2.SelectedValue = 0 And cboMontos2.SelectedValue = 0 And
                                    cboFormulas2.SelectedValue = 0 And cboMovimiento2.SelectedValue = 0 Then
                                MessageBox("Formulas", "Debe Ingresar o Seleccionar el valor 2", Page, Master, "W")
                            Else
                                If ExisteFormula() = 0 Then
                                    Call GuardarCab()
                                    Call RescartarFormula()
                                    Call GuardarDet()
                                    Call LimpiarGuardar()
                                    Call RescataPasoAnterior()
                                    Call ContarDetalle()
                                    If hdnContar.Value >= 1 Then
                                        pnlOpcion1.Visible = False
                                        pnlValor1.Visible = False
                                        Call Bloquear2doPaso()
                                    Else
                                        pnlOpcion1.Visible = True
                                        pnlValor1.Visible = True
                                    End If
                                    Call CargarGridFormulas()
                                    MessageBox("Formulas", "Registro Guardado", Page, Master, "S")
                                Else
                                    MessageBox("Formulas", "Formula Ya existe", Page, Master, "W")
                                End If

                            End If
                        End If
                    End If
                End If
            End If
        Else
            If chkTope.Checked = False Then
                If cboOperador.SelectedValue = "0" Then
                    MessageBox("Formulas", "Debe Seleccionar un operador", Page, Master, "W")
                Else
                    If rdbItemFijo2.Checked = False And rdbFormula2.Checked = False And rdbItemsInfo2.Checked = False And
                                 rdbMovimiento2.Checked = False And rdbValorBase2.Checked = False Then
                        MessageBox("Formulas", "Debe Seleccionar un tipo de Dato 2", Page, Master, "W")
                    Else
                        If txtValorBase2.Text = "" And cboCantidad2.SelectedValue = 0 And cboMontos2.SelectedValue = 0 And
                                        cboFormulas2.SelectedValue = 0 And cboMovimiento2.SelectedValue = 0 Then
                            MessageBox("Formulas", "Debe Ingresar o Seleccionar el valor 2", Page, Master, "W")
                        Else
                            Call GuardarDet()
                            Call RescataPasoAnterior()
                            Call Bloquear2doPaso()
                            MessageBox("Formulas", "Registro Guardado", Page, Master, "S")
                        End If
                    End If
                End If
            Else
                Call GuardarDet()
                Call RescataPasoAnterior()
                Call Bloquear2doPaso()
                MessageBox("Formulas", "Registro Guardado", Page, Master, "S")
            End If

        End If
    End Sub

    Protected Sub grdFormulas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdFormulas.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdFormulas.SelectedRow
            cboEmpresa.SelectedValue = CType(row.FindControl("hdn_empresa"), HiddenField).Value
            cboItem.SelectedValue = CType(row.FindControl("hdn_item"), HiddenField).Value
            txtDescripcion.Text = CType(row.FindControl("lblDesFormula"), Label).Text
            txtFormula.Text = CType(row.FindControl("hdn_id"), HiddenField).Value
            Call RescataPasoAnterior()
            Call ContarDetalle()
            If hdnContar.Value >= 1 Then
                pnlOpcion1.Visible = False
                pnlValor1.Visible = False
                Call Bloquear2doPaso()
            Else
                pnlOpcion1.Visible = True
                pnlValor1.Visible = True
            End If

        Catch ex As Exception
            MessageBoxError("grdFormulas_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Bloquear2doPaso()
        cboOperador.ClearSelection()
        cboOperador.Enabled = True
        pnlOpcion2.Enabled = False
        pnlValor2.Enabled = False
        pnlValor2.Visible = False
        pnlOpcion2.Visible = False
    End Sub

    Private Sub Activar2doPaso()
        pnlOpcion2.Enabled = True
        pnlValor2.Enabled = True
        pnlValor2.Visible = True
        pnlOpcion2.Visible = True
    End Sub
    Protected Sub LimpiarGuardar()
        hdnTipo1.Value = 0
        txtExpresion.Text = ""
        Call CargarGridFormulas()
        cboCantidad1.ClearSelection()
        cboFormulas1.ClearSelection()
        cboMontos1.ClearSelection()
        cboOperador.ClearSelection()
        txtValorBase1.Text = ""
        cboMovimiento1.ClearSelection()
    End Sub
    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        cboEmpresa.ClearSelection()
        cboItem.ClearSelection()
        txtFormula.Text = ""
        txtDescripcion.Text = ""
        txtExpresion.Text = ""
        Call CargarGridFormulas()
        rdbFormula1.Checked = False
        rdbFormula2.Checked = False
        rdbItemFijo2.Checked = False
        rdbItemFijo1.Checked = False
        rdbItemsInfo1.Checked = False
        rdbItemsInfo2.Checked = False
        rdbValorBase1.Checked = False
        rdbValorBase2.Checked = False
        rdbMovimiento1.Checked = False
        rdbMovimiento2.Checked = False
        cboCantidad1.ClearSelection()
        cboCantidad2.ClearSelection()
        cboFormulas1.ClearSelection()
        cboFormulas2.ClearSelection()
        cboMontos1.ClearSelection()
        cboMontos2.ClearSelection()
        cboOperador.ClearSelection()
        txtValorBase1.Text = ""
        txtValorBase2.Text = ""
        cboMovimiento1.ClearSelection()
        cboMovimiento2.ClearSelection()
        pnlOpcion1.Visible = True
        pnlValor1.Visible = False
        cboOperador.Enabled = False
        pnlOpcion2.Visible = False
        pnlValor2.Visible = False
    End Sub

    Protected Sub cboFormulas1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFormulas1.SelectedIndexChanged
        txtExpresion.Text = cboFormulas1.SelectedItem.Text
        cboOperador.Enabled = True
    End Sub

    Protected Sub cboCantidad1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCantidad1.SelectedIndexChanged
        txtExpresion.Text = cboCantidad1.SelectedItem.Text
        cboOperador.Enabled = True
    End Sub

    Protected Sub rdbItemsInfo1_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemsInfo1.CheckedChanged
        txtValorBase1.Visible = False
        cboCantidad1.Visible = True
        cboFormulas1.Visible = False
        cboMontos1.Visible = False
        cboMovimiento1.Visible = False
        hdnTipo1.Value = 4
        pnlValor1.Visible = True
    End Sub

    Protected Sub rdbItemsInfo2_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemsInfo2.CheckedChanged
        txtValorBase2.Visible = False
        cboCantidad2.Visible = True
        cboFormulas2.Visible = False
        cboMontos2.Visible = False
        cboMovimiento2.Visible = False
        hdnTipo2.Value = 4
    End Sub

    Protected Sub rdbItemFijo1_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemFijo1.CheckedChanged
        txtValorBase1.Visible = False
        cboCantidad1.Visible = False
        cboFormulas1.Visible = False
        cboMontos1.Visible = True
        cboMovimiento1.Visible = False
        hdnTipo1.Value = 5
        pnlValor1.Visible = True
    End Sub

    Protected Sub rdbItemFijo2_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemFijo2.CheckedChanged
        txtValorBase2.Visible = False
        cboCantidad2.Visible = False
        cboFormulas2.Visible = False
        cboMontos2.Visible = True
        cboMovimiento2.Visible = False
        hdnTipo2.Value = 5
    End Sub

    Protected Sub cboCantidad2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCantidad2.SelectedIndexChanged
        txtExpresion.Text = txtExpresion.Text + " " + cboCantidad2.SelectedItem.Text
    End Sub

    Protected Sub cboMontos2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMontos2.SelectedIndexChanged
        txtExpresion.Text = txtExpresion.Text + " " + cboMontos2.SelectedItem.Text
    End Sub

    Protected Sub cboFormulas2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFormulas2.SelectedIndexChanged
        txtExpresion.Text = txtExpresion.Text + " " + cboFormulas2.SelectedItem.Text
    End Sub

    Protected Sub chkTope_CheckedChanged(sender As Object, e As EventArgs) Handles chkTope.CheckedChanged
        If chkTope.Checked = True Then
            pnlTope.Visible = True
            pnlTope1.Visible = True
        Else
            pnlTope.Visible = False
            pnlTope1.Visible = False
        End If
    End Sub

    Protected Sub rdbValorBase3_CheckedChanged(sender As Object, e As EventArgs) Handles rdbValorBase3.CheckedChanged
        txtValorBase3.Visible = True
        cboCantidad3.Visible = False
        cboFormulas3.Visible = False
        cboMontos3.Visible = False
        cboMovimientos3.Visible = False
        hdnTipo3.Value = 1
    End Sub

    Protected Sub rdbMovimiento3_CheckedChanged(sender As Object, e As EventArgs) Handles rdbMovimiento3.CheckedChanged
        txtValorBase3.Visible = False
        cboCantidad3.Visible = False
        cboFormulas3.Visible = False
        cboMontos3.Visible = False
        cboMovimientos3.Visible = True
        hdnTipo3.Value = 2
    End Sub

    Protected Sub rdbFormula3_CheckedChanged(sender As Object, e As EventArgs) Handles rdbFormula3.CheckedChanged
        txtValorBase3.Visible = False
        cboCantidad3.Visible = False
        cboFormulas3.Visible = True
        cboMontos3.Visible = False
        cboMovimientos3.Visible = False
        hdnTipo3.Value = 3
    End Sub

    Protected Sub rdbItemsInfo3_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemsInfo3.CheckedChanged
        txtValorBase3.Visible = False
        cboCantidad3.Visible = True
        cboFormulas3.Visible = False
        cboMontos3.Visible = False
        cboMovimientos3.Visible = False
        hdnTipo3.Value = 4
    End Sub

    Protected Sub rdbItemFijo3_CheckedChanged(sender As Object, e As EventArgs) Handles rdbItemFijo3.CheckedChanged
        txtValorBase3.Visible = False
        cboCantidad3.Visible = False
        cboFormulas3.Visible = False
        cboMontos3.Visible = True
        cboMovimientos3.Visible = False
        hdnTipo3.Value = 5
    End Sub

    Protected Sub cboItem_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboItem.SelectedIndexChanged
        txtDescripcion.Text = cboItem.SelectedItem.Text
        txtFormula.Text = ""
        txtExpresion.Text = ""
        Call CargarGridFormulas()
        rdbFormula1.Checked = False
        rdbFormula2.Checked = False
        rdbItemFijo2.Checked = False
        rdbItemFijo1.Checked = False
        rdbItemsInfo1.Checked = False
        rdbItemsInfo2.Checked = False
        rdbValorBase1.Checked = False
        rdbValorBase2.Checked = False
        rdbMovimiento1.Checked = False
        rdbMovimiento2.Checked = False
        cboCantidad1.ClearSelection()
        cboCantidad2.ClearSelection()
        cboFormulas1.ClearSelection()
        cboFormulas2.ClearSelection()
        cboMontos1.ClearSelection()
        cboMontos2.ClearSelection()
        cboOperador.ClearSelection()
        txtValorBase1.Text = ""
        txtValorBase2.Text = ""
        cboMovimiento1.ClearSelection()
        cboMovimiento2.ClearSelection()
        pnlOpcion1.Visible = True
        pnlValor1.Visible = False
        cboOperador.Enabled = False
        pnlOpcion2.Visible = False
        pnlValor2.Visible = False
    End Sub

    Private Sub grdFormulas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdFormulas.RowCommand
        If Trim(LCase(e.CommandName)) = LCase("btnEditar") Then
            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim row As GridViewRow = grdFormulas.Rows(index)
            hdnFormula.Value = CType(row.FindControl("hdn_id"), HiddenField).Value
            mvFormulas.ActiveViewIndex = 1
        End If
    End Sub
End Class