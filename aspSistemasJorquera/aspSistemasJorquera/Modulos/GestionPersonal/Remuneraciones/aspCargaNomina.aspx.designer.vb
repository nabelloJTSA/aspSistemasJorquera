﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class aspCargaNomina
    
    '''<summary>
    '''Control btnAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnAnt As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnSue.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnSue As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control lbltitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents lbltitulo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Control cboEmpresa.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboEmpresa As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control pnlDatos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDatos As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control mtvNomina.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents mtvNomina As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Control vwANT.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents vwANT As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control fuplCargarAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents fuplCargarAnt As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''Control btnCargarAnticipos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCargarAnticipos As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnGuardarAnticipo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardarAnticipo As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnExcel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnExcel As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnTxt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnTxt As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control pnlCabAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCabAnt As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control txtRutEmpAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRutEmpAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNomEmpAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNomEmpAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtConvenioAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtConvenioAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNominaAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNominaAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtFecPagoAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFecPagoAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtTotalAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTotalAnt As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control DropDownList5.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DropDownList5 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control DropDownList3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DropDownList3 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control DropDownList4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DropDownList4 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control pnlDetAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDetAnt As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control grdDetAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdDetAnt As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control vwSUE.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents vwSUE As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Control fuplSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents fuplSueldo As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''Control btnCargaSueldos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnCargaSueldos As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnGuardarSueldos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnGuardarSueldos As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnSueldoFin700.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnSueldoFin700 As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control btnPagosWebSueldos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents btnPagosWebSueldos As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Control pnlCabSue.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlCabSue As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control txtRutSueldos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtRutSueldos As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtEmpresaSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtEmpresaSueldo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtConvenioSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtConvenioSueldo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtNominaSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtNominaSueldo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtfechaSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtfechaSueldo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control txtFecPagoPer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtFecPagoPer As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cboPeriodo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cboPeriodo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control txtTotalSueldo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents txtTotalSueldo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Control cbo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents cbo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control DropDownList2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DropDownList2 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control DropDownList6.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DropDownList6 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Control pnlDetSue.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pnlDetSue As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Control grdSueldos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents grdSueldos As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''Control hdnIdCab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdCab As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnIdDetAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnIdDetAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumNominaAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumNominaAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnMedioPagoAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnMedioPagoAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnCodBancoAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnCodBancoAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumCuentaAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumCuentaAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnValTotalAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnValTotalAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnGlosaAnt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnGlosaAnt As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnRutGrilla.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnRutGrilla As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnFiniquitado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnFiniquitado As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnEstTraspaso.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnEstTraspaso As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Control hdnNumConvenio.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hdnNumConvenio As Global.System.Web.UI.WebControls.HiddenField
End Class
