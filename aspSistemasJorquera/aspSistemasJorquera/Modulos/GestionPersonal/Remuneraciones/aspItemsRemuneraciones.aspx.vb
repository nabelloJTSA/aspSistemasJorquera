﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspItemsRemuneraciones
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                mvItems.ActiveViewIndex = 0
                Call carga_combo_empresa()
                Call carga_anio()
                cboAnio.SelectedValue = Year(Date.Now.Date)
                Call cargar_periodo()
                Call carga_areas()
                Call ContarItems()
                Call BuscarPeriodo()
                Call BuscarPeriodoCboPeriodo()
                Call carga_cboitems()
                hdnIdItem.Value = 0
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try

    End Sub

    Private Sub BuscarPeriodo()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Select num_periodo From dbo.REMMae_periodo Where bit_activo=1"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    cboPeriodo.SelectedValue = rdoReader(0).ToString
                    hdnPeriodoActual.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("BuscarPeriodo", Err, Page, Master)
            End Try
        End Using
    End Sub

    Private Sub BuscarPeriodoCboPeriodo()
        Dim conx As New SqlConnection(strCnx)
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand, comando As New SqlCommand
        Dim strSQL As String = "Select bit_activo From dbo.REMMae_periodo Where num_periodo= '" & cboPeriodo.SelectedValue & "'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                comando = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = comando.ExecuteReader()
                If rdoReader.Read Then
                    hdnPeriodoCbo.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                rdoReader = Nothing
                comando.Dispose()
                cmdTemporal = Nothing
            Catch ex As Exception
                MessageBoxError("BuscarPeriodoCboPeriodo", Err, Page, Master)
            End Try
        End Using
    End Sub



    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa in (select cod_empresa from REMMae_personal_ingreso where cod_usuario = '" & Session("cod_usu_tc_via") & "')"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_anio()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "select distinct num_anio from REMMae_periodo order by num_anio DESC"
        cboAnio.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboAnio.DataTextField = "num_anio"
        cboAnio.DataValueField = "num_anio"
        cboAnio.DataBind()
    End Sub


    Protected Sub cboAnio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedIndexChanged

        Call cargar_periodo()

        If hdnPeriodoCbo.Value = 1 Then
            btnCargar.Enabled = True
        Else
            btnCargar.Enabled = False
        End If

    End Sub

    Private Sub cargar_periodo()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "Select num_periodo from REMMae_periodo where num_anio = '" & cboAnio.SelectedValue & "' order by num_periodo"
        cboPeriodo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboPeriodo.DataTextField = "num_periodo"
        cboPeriodo.DataValueField = "num_periodo"
        cboPeriodo.DataBind()
    End Sub

    Private Sub carga_areas()
        Dim strNomTablaC As String = "REMMae_area"
        Dim strSQLP As String = "Select id_area=0, nom_area='Ninguna' UNION select id_area, nom_area from REMMae_area where id_area in (select id_area from REMMae_personal_ingreso where cod_usuario = '" & Session("cod_usu_tc_via") & "') order by id_area"
        cboAreas.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboAreas.DataTextField = "nom_area"
        cboAreas.DataValueField = "id_area"
        cboAreas.DataBind()
    End Sub

    Private Sub carga_cboitems()
        Dim strNomTablaC As String = "REMMae_item_variables"
        Dim strSQLP As String = "Select id_item_variables=0, nom_item_variables='Ninguno' UNION select id_item_variables, nom_item_variables from REMMae_item_variables where id_item_variables in (select id_item from REMMae_personal_ingreso_items where cod_usuario = '" & Session("cod_usu_tc_via") & "') order by id_item_variables"
        cboItems.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboItems.DataTextField = "nom_item_variables"
        cboItems.DataValueField = "id_item_variables"
        cboItems.DataBind()
    End Sub


    Private Sub CargarComboNombreHorasExtras(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Select '0' as rut_personal, '<Seleccionar>' as nom_personal Union Select rut_personal, nom_personal From REMMae_ficha_personal Where cod_empresa = '" & cboEmpresa.SelectedValue & "' and nom_personal Like '%" & strBuscar & "%' and id_area = '" & cboAreas.SelectedValue & "'  and est_vigencia = 1 order by nom_personal "
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cboBuscarNombre.DataTextField = "nom_personal"
            cboBuscarNombre.DataValueField = "rut_personal"
            cboBuscarNombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cboBuscarNombre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombreHorasExtras", Err, Page, Master)
        End Try
    End Sub



    Private Sub limpiarHorasExtra()
        cboBuscarNombre.ClearSelection()
        cboBuscarNombre.Visible = False
        txtNombreHorasExtra.Text = ""
        txtNombreHorasExtra.Visible = True
    End Sub


    Protected Sub cboBuscarNombreHorasExtra_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBuscarNombre.SelectedIndexChanged

        Call BuscarPeriodoCboPeriodo()
        If hdnPeriodoCaducado.Value = cboPeriodo.SelectedValue Then
            btnCargar.Enabled = False
        Else
            If hdnPeriodoCbo.Value = 1 Then
                btnCargar.Enabled = True
            Else
                btnCargar.Enabled = False
            End If
            Call CalcularFechaSinMsj()
            txtRut.Text = cboBuscarNombre.SelectedValue

            If hdnPeriodoCbo.Value = 1 Then
                btnCargar.Enabled = True
            Else
                btnCargar.Enabled = False
            End If
        End If



    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call limpiarHorasExtra()
        cboAreas.SelectedValue = 0
        If cboEmpresa.SelectedValue <> 0 Then
            Call CalcularFecha()
        End If

        If Month(Date.Now.Date) <> 12 Then
            If hdnPeriodoCbo.Value = 1 Then
                btnCargar.Enabled = True
            Else
                btnCargar.Enabled = False
            End If
        End If


    End Sub

    Protected Sub cboAreas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAreas.SelectedIndexChanged
        Call limpiarHorasExtra()
        Call CalcularFechaSinMsj()
        Call BuscarPeriodoCboPeriodo()

        If hdnPeriodoCaducado.Value = cboPeriodo.SelectedValue Then
            btnCargar.Enabled = False
        Else
            If hdnPeriodoActual.Value = cboPeriodo.SelectedValue Then
                btnCargar.Enabled = True
            Else
                btnCargar.Enabled = False
            End If


            If chkTI.Checked = True Then
                If cboAreas.SelectedValue <> "0" Then
                    Call CargarGrillaHorasExtraTotal("")
                End If
            Else
                If cboAreas.SelectedValue <> "0" Then
                    Call CargarGrillaHorasExtra("")
                End If
            End If

            If Month(Date.Now.Date) <> 12 Then
                If hdnPeriodoCbo.Value = 1 Then
                    btnCargar.Enabled = True
                Else
                    btnCargar.Enabled = False
                End If
            End If

        End If

    End Sub

    Private Sub ContarItems()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "select Count(id_item_variables) + 1 From REMMae_item_variables"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnCantItems.Value = rdoReader(0).ToString
                Else
                    hdnCantItems.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("ContarItems", Err, Page, Master)
        End Try
    End Sub

    Private Sub guardar_item()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_items_remuneraciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@id_item", SqlDbType.NVarChar)
            comando.Parameters("@id_item").Value = hdnIdItem.Value
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = txtRut.Text
            comando.Parameters.Add("@num_periodo", SqlDbType.NVarChar)
            comando.Parameters("@num_periodo").Value = cboPeriodo.SelectedValue
            comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
            comando.Parameters("@id_area").Value = cboAreas.SelectedValue
            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@fec_ingreso").Value = dtFechaIni.Text
            comando.Parameters.Add("@num_valor", SqlDbType.NVarChar)
            comando.Parameters("@num_valor").Value = Replace(txtValor.Text, ",", ".")
            comando.Parameters.Add("@obs_detalle", SqlDbType.NVarChar)
            comando.Parameters("@obs_detalle").Value = txtDetalle.Text
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@user_reg", SqlDbType.NVarChar)
            comando.Parameters("@user_reg").Value = Session("cod_usu_tc_via")
            comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
            comando.Parameters("@fec_registro").Value = Date.Now.ToString
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("", "Registro Guardado Exitosamente", Page, Master, "I")

        Catch ex As Exception
            MessageBoxError("guardar_item", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHorasExtra(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_area"
            Dim strSQL As String = "Exec proc_items_remuneraciones 'G' ,'" & nombre & "','" & cboEmpresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "' , '" & hdnIdItem.Value & "', '" & cboAreas.SelectedValue & "', '" & Session("cod_usu_tc_via") & "'"
            grdHorasExtra.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHorasExtra.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHorasExtra", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHorasExtraTotal(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_area"
            Dim strSQL As String = "Exec proc_items_remuneraciones 'GTI' ,'" & nombre & "','" & cboEmpresa.SelectedValue & "', '" & cboPeriodo.SelectedValue & "' , '', '" & cboAreas.SelectedValue & "', '" & Session("cod_usu_tc_via") & "'"
            grdHorasExtra.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHorasExtra.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHorasExtraTotal", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdHorasExtra_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdHorasExtra.PageIndexChanging
        Try
            grdHorasExtra.PageIndex = e.NewPageIndex
            If txtBuscarNom.Text = "" Then
                If chkTI.Checked = True Then
                    Call CargarGrillaHorasExtraTotal("")
                Else
                    Call CargarGrillaHorasExtra("")
                End If
            Else
                If chkTI.Checked = True Then
                    Call CargarGrillaHorasExtraTotal(txtBuscarNom.Text)
                Else
                    Call CargarGrillaHorasExtra(txtBuscarNom.Text)
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdHorasExtra_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdHorasExtra_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHorasExtra.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "btnEliminar" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdHorasExtra.Rows(intRow)
                Dim objID As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "proc_items_remuneraciones"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "E"
                comando.Parameters.Add("@id", SqlDbType.NVarChar)
                comando.Parameters("@id").Value = objID.Value
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                Call CargarGrillaHorasExtra("")
            End If
        Catch ex As Exception
            MessageBoxError("grdHorasExtra_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Protected Sub BtnNBuscar_Click(sender As Object, e As EventArgs) Handles BtnNBuscar.Click
        If chkTI.Checked = True Then
            Call CargarGrillaHorasExtraTotal(txtBuscarNom.Text)
        Else
            Call CargarGrillaHorasExtra(txtBuscarNom.Text)
        End If

        Call CalcularFechaSinMsj()

        If hdnPeriodoActual.Value = cboPeriodo.SelectedValue Then
            btnCargar.Enabled = True
        Else
            btnCargar.Enabled = False
        End If

    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged
        Call BuscarPeriodoCboPeriodo()


        If hdnPeriodoCaducado.Value = cboPeriodo.SelectedValue Then
            btnCargar.Enabled = False
        Else
            If Month(Date.Now.Date) <> 12 Then
                If hdnPeriodoCbo.Value = 1 Then
                    btnCargar.Enabled = True
                Else
                    btnCargar.Enabled = False
                End If
            End If

            Call limpiarHorasExtra()

            If cboAreas.SelectedValue <> "0" Then
                Call CargarGrillaHorasExtra("")
            End If
        End If

    End Sub

    Private Sub RescatarDia()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "select * from REMMae_dia_cierre where cod_empresa='" & cboEmpresa.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnDia.Value = rdoReader(1).ToString
                    hdnHora.Value = rdoReader(2).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch
            MessageBoxError("RescatarDia", Err, Page, Master)
        End Try

    End Sub

    Private Sub Activar()
        cboEmpresa.Enabled = True
        cboAnio.Enabled = True
        cboPeriodo.Enabled = True
        cboAreas.Enabled = True
        txtNombreHorasExtra.Enabled = True
        bntBuscarNombreHorasExtra.Enabled = True
        dtFechaIni.Enabled = True
        txtValor.Enabled = True
        txtDetalle.Enabled = True
        btnCargar.Enabled = True
        Panel1.Enabled = True
    End Sub

    Private Sub CalcularFechaSinMsj()
        Call Activar()
        Call RescatarDia()
        Dim HoraActual As String = Date.Now.ToLongTimeString
        Dim DiaActual As String = Date.Now.Day
        Dim total As Integer = Val(hdnDia.Value) - DiaActual

        If DiaActual = 10 Or DiaActual = 11 Or DiaActual = 12 Or DiaActual = 13 Or DiaActual = 14 Or DiaActual = 15 Or DiaActual = 16 Then
            cboEmpresa.Enabled = True
            Panel1.Enabled = True
            cboAnio.Enabled = True
            cboPeriodo.Enabled = True
            cboAreas.Enabled = True
            txtNombreHorasExtra.Enabled = True
            bntBuscarNombreHorasExtra.Enabled = True
            dtFechaIni.Enabled = True
            txtValor.Enabled = True
            txtDetalle.Enabled = True
            btnCargar.Enabled = True
            cboPeriodo.SelectedValue = hdnPeriodoActual.Value
        Else
            If total = 0 Then
                If hdnHora.Value > DateTime.Now.ToString("HH:mm") Then
                    cboPeriodo.SelectedValue = hdnPeriodoActual.Value
                    MessageBox("Fecha", "Hoy a las " & hdnHora.Value & " Hrs. vence el periodo para ingresar informacion  ", Page, Master, "I")
                End If
            End If
        End If

    End Sub

    Private Sub CalcularFecha()

        Call Activar()
        Call RescatarDia()
        Dim HoraActual As String = Date.Now.ToLongTimeString
        Dim DiaActual As String = Date.Now.Day
        Dim total As Integer = Val(hdnDia.Value) - DiaActual

        If DiaActual = 11 Or DiaActual = 11 Or DiaActual = 12 Or DiaActual = 13 Or DiaActual = 14 Or DiaActual = 15 Or DiaActual = 16 Then
            cboEmpresa.Enabled = True
            Panel1.Enabled = True
            cboAnio.Enabled = True
            cboPeriodo.Enabled = True
            cboAreas.Enabled = True
            txtNombreHorasExtra.Enabled = True
            bntBuscarNombreHorasExtra.Enabled = True
            dtFechaIni.Enabled = True
            txtValor.Enabled = True
            txtDetalle.Enabled = True
            btnCargar.Enabled = True
            cboPeriodo.SelectedValue = hdnPeriodoActual.Value
        Else

            If total < 0 Then
                If hdnDia.Value = 1 Or hdnDia.Value = 2 Or hdnDia.Value = 3 Or hdnDia.Value = 4 Or hdnDia.Value = 9 Or hdnDia.Value = 10 Then

                    If total = 0 Then
                        If hdnHora.Value < DateTime.Now.ToString("HH:mm") Then

                            If Month(Date.Now.Date) = 12 Then
                                MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, favor cambiar año", Page, Master, "I")
                                btnCargar.Enabled = False
                            Else
                                MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, periodo siguiente activo", Page, Master, "I")
                                cboPeriodo.SelectedValue = cboPeriodo.SelectedValue + 1
                            End If

                        Else
                            cboPeriodo.SelectedValue = hdnPeriodoActual.Value
                            MessageBox("Fecha", "Hoy a las " & hdnHora.Value & " Hrs. vence el periodo para ingresar informacion  ", Page, Master, "I")
                        End If
                    End If

                    If total <= 5 And total <> 0 And total > 0 Then
                        cboPeriodo.SelectedValue = hdnPeriodoActual.Value
                        MessageBox("Fecha", "Faltan " & total & " dias para desactivar el el periodo " & cboPeriodo.SelectedItem.ToString & "  ", Page, Master, "I")
                    End If
                Else

                    If Month(Date.Now.Date) = 12 Then
                        MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, favor cambiar año", Page, Master, "I")
                        btnCargar.Enabled = False
                    Else
                        MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, periodo siguiente activo", Page, Master, "I")
                        hdnPeriodoCaducado.Value = cboPeriodo.SelectedValue
                        cboPeriodo.SelectedValue = cboPeriodo.SelectedValue + 1
                    End If

                End If
            End If

            If total = 0 Then
                If hdnHora.Value < DateTime.Now.ToString("HH:mm") Then
                    If Month(Date.Now.Date) = 12 Then
                        MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, favor cambiar año", Page, Master, "I")
                        btnCargar.Enabled = False
                    Else
                        MessageBox("Fecha", "Periodo " & cboPeriodo.SelectedItem.ToString & " caducado, periodo siguiente activo", Page, Master, "I")
                        hdnPeriodoCaducado.Value = cboPeriodo.SelectedValue
                        cboPeriodo.SelectedValue = cboPeriodo.SelectedValue + 1
                    End If


                Else
                    cboPeriodo.SelectedValue = hdnPeriodoActual.Value
                    MessageBox("Fecha", "Hoy a las " & hdnHora.Value & " Hrs. vence el periodo para ingresar informacion  ", Page, Master, "I")
                End If
            End If

            If total <= 5 And total <> 0 And total > 0 Then
                cboPeriodo.SelectedValue = hdnPeriodoActual.Value
                MessageBox("Fecha", "Faltan " & total & " dias para desactivar el el periodo " & cboPeriodo.SelectedItem.ToString & "  ", Page, Master, "I")
            End If

        End If

    End Sub



    Private Sub grdHorasExtra_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHorasExtra.RowDataBound

        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim rdoReader As SqlDataReader, trsBaseDatos As SqlTransaction
            Dim cmdTemporal As SqlCommand, intRow As Integer
            Dim strSQL As String = "Exec proc_items_remuneraciones 'GD', '" & HttpUtility.HtmlDecode(CType(row.FindControl("hdn_id"), HiddenField).Value) & "', '" & cboPeriodo.SelectedValue & "'"
            Using cnxBaseDatos As New SqlConnection(strCnx)
                cnxBaseDatos.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    cmdTemporal.Transaction = trsBaseDatos
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        If rdoReader(13).ToString = True Then
                            Dim objBtn As ImageButton = CType(row.FindControl("btnEliminar"), ImageButton)
                            objBtn.Visible = False
                        ElseIf rdoReader(13).ToString = False Then
                            Dim objBtn As ImageButton = CType(row.FindControl("btnEliminar"), ImageButton)
                            objBtn.Visible = True
                        End If
                    End If
                Catch ex As Exception
                    MessageBoxError("grdHorasExtra_RowDataBound", Err, Page, Master)
                End Try

            End Using
            rdoReader.Close()
            rdoReader = Nothing
            cmdTemporal.Dispose()
            cmdTemporal = Nothing
        End If
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If cboBuscarNombre.SelectedValue = "0" Or dtFechaIni.Text = "" Or txtValor.Text = "" Then
            MessageBox("Cargar", "Debe ingresar datos", Page, Master, "E")
        ElseIf hdnIdItem.Value > 0 Then
            If chkTI.Checked = True Then
                Call guardar_item()
                Call CargarGrillaHorasExtraTotal("")
            Else
                Call guardar_item()
                Call CargarGrillaHorasExtra("")
            End If
        ElseIf hdnIdItem.Value = 0 Then
            MessageBox("Cargar", "Seleccione item a registrar", Page, Master, "E")
        End If
    End Sub

    Protected Sub cboItems_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboItems.SelectedIndexChanged
        If chkTI.Checked = True Then
            hdnIdItem.Value = cboItems.SelectedValue
            Call CargarGrillaHorasExtraTotal("")
            lblFielTotal.Text = "TOTAL ITEMS"
        Else
            hdnIdItem.Value = cboItems.SelectedValue
            Call CargarGrillaHorasExtra("")
            lblFielTotal.Text = cboItems.SelectedItem.ToString
        End If

    End Sub

    Protected Sub chkTI_CheckedChanged(sender As Object, e As EventArgs) Handles chkTI.CheckedChanged
        If chkTI.Checked = True Then
            hdnIdItem.Value = cboItems.SelectedValue
            Call CargarGrillaHorasExtraTotal("")
            lblFielTotal.Text = "TOTAL ITEMS"
        Else
            hdnIdItem.Value = cboItems.SelectedValue
            Call CargarGrillaHorasExtra("")
            lblFielTotal.Text = cboItems.SelectedItem.ToString
        End If
    End Sub

    Protected Sub txtValorActualizar(sender As Object, e As EventArgs)
        Dim Txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(Txt.NamingContainer, GridViewRow)
        Call ActualizarValor(CType(row.FindControl("txtValorGRilla"), TextBox).Text, (CType(row.FindControl("hdn_id"), HiddenField).Value))
        Call CargarGrillaHorasExtraTotal("")
    End Sub

    Protected Sub txtDetalleActualizar(sender As Object, e As EventArgs)
        Dim Txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(Txt.NamingContainer, GridViewRow)
        Call Actualizar(CType(row.FindControl("txtDetalleGRilla"), TextBox).Text, (CType(row.FindControl("hdn_id"), HiddenField).Value))
        Call CargarGrillaHorasExtraTotal("")
    End Sub

    Private Sub ActualizarValor(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_items_remuneraciones"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "U_VAL"
            comando.Parameters.Add("@actualizar", SqlDbType.VarChar)
            comando.Parameters("@actualizar").Value = Valor
            comando.Parameters.Add("@user_edit", SqlDbType.VarChar)
            comando.Parameters("@user_edit").Value = Session("cod_usu_tc_via")
            comando.Parameters.Add("@fec_edit", SqlDbType.VarChar)
            comando.Parameters("@fec_edit").Value = Date.Now.ToString
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarValor", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "proc_items_remuneraciones"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "U_G"
            comando.Parameters.Add("@actualizar", SqlDbType.VarChar)
            comando.Parameters("@actualizar").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub dtFechaIni_TextChanged(sender As Object, e As EventArgs) Handles dtFechaIni.TextChanged
        Call BuscarPeriodoCboPeriodo()
        If CDate(dtFechaIni.Text).DayOfWeek = 0 Then
            dtFechaIni.BackColor = Drawing.Color.Red
        Else
            dtFechaIni.BackColor = Drawing.Color.White
        End If

        If hdnPeriodoCbo.Value = 1 Then
            btnCargar.Enabled = True
        Else
            btnCargar.Enabled = False
        End If
    End Sub

    Protected Sub bntBuscarNombreHorasExtra_Click(sender As Object, e As EventArgs) Handles bntBuscarNombreHorasExtra.Click
        If cboEmpresa.SelectedValue = 0 Then
            MessageBox("Buscar", "Seleccione Empresa", Page, Master, "E")
        Else
            Dim blnVisible As Boolean = cboBuscarNombre.Visible, intRegistros As Integer = 0
            Try
                cboBuscarNombre.Visible = Not blnVisible
                txtNombreHorasExtra.Visible = blnVisible
                Call CargarComboNombreHorasExtras(Trim(txtNombreHorasExtra.Text), intRegistros)
                If intRegistros = 2 And cboBuscarNombre.Visible = True Then
                    cboBuscarNombre.SelectedIndex = 1
                    Dim varValores() As String = Split(cboBuscarNombre.SelectedValue, ";")
                End If
                cboBuscarNombre.ClearSelection()
            Catch ex As Exception
                MessageBoxError("bntBuscarNombreHorasExtra_Click", Err, Page, Master)
            End Try
        End If
    End Sub

    Protected Sub bntIngresados_Click(sender As Object, e As EventArgs) Handles bntIngresados.Click
        Try
            If cboEmpresa.SelectedValue <> 0 And cboPeriodo.SelectedValue <> 0 And cboAreas.SelectedValue <> 0 Then
                Dim grdcargar As New DataGrid
                grdcargar.ShowHeader = True
                grdcargar.CellPadding = 0
                grdcargar.CellSpacing = 0
                grdcargar.BorderStyle = BorderStyle.Groove
                grdcargar.Font.Size = 11

                Dim strNomTabla As String = "REMMov_item_personal"
                Dim strSQL As String = "Exec proc_items_remuneraciones 'G_R', '" & cboPeriodo.SelectedValue & "','" & cboAreas.SelectedValue & "', '" & cboEmpresa.SelectedValue & "'"
                grdcargar.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
                grdcargar.DataBind()

                Me.EnableViewState = False
                Dim tw As New System.IO.StringWriter
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)

                grdcargar.RenderControl(hw)

                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=RegistrosIngresados.xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default

                Response.Write(tw.ToString())
                Response.End()
            Else
                MessageBox("Ingresados", "Debe seleccionar empresa, periodo y area", Page, Master, "E")
            End If
        Catch ex As Exception
            MessageBoxError("bntIngresados_Click", Err, Page, Master)
        End Try

        If hdnPeriodoActual.Value = cboPeriodo.SelectedValue Then
            btnCargar.Enabled = True
        Else
            btnCargar.Enabled = False
        End If

    End Sub
End Class

