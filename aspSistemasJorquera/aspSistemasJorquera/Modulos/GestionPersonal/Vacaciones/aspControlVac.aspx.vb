﻿
Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspControlVac
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call carga_combo_empresa()
                Call carga_especiales()
                Call carga_periodo_año()
                Call CargarGrilla("")
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSql As String = "Exec pro_control_vacaciones 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSql, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub carga_especiales()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_control_vacaciones 'CDE'"
            cboTipoDias.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboTipoDias.DataTextField = "nom_dia"
            cboTipoDias.DataValueField = "id_dia_esp"
            cboTipoDias.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_especiales", Err, Page, Master)
        End Try
    End Sub


    Private Sub carga_periodo_año()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_control_vacaciones 'CPE'"
            cboPeriodoOtros.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPeriodoOtros.DataTextField = "año_correspondiente"
            cboPeriodoOtros.DataValueField = "id_periodo_vac"
            cboPeriodoOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_anio", Err, Page, Master)
        End Try
    End Sub

    Protected Sub bntBuscarNombreHorasExtra_Click(sender As Object, e As EventArgs) Handles bntBuscarNombreHorasExtra.Click
        Dim blnVisible As Boolean = cboBuscarNombre.Visible, intRegistros As Integer = 0
        Try
            cboBuscarNombre.Visible = Not blnVisible
            txtnombre.Visible = blnVisible
            Call CargarComboNombre(Trim(txtnombre.Text), intRegistros)
            If intRegistros = 2 And cboBuscarNombre.Visible = True Then
                cboBuscarNombre.SelectedIndex = 1
                Dim varValores() As String = Split(cboBuscarNombre.SelectedValue, ";")
            End If
            cboBuscarNombre.ClearSelection()
        Catch ex As Exception
            MessageBoxError("bntBuscarNombreClick", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboNombre(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Select rut_personal= '0', nom_personal= '<Seleccionar>' UNION Select rut_personal, nom_personal From REMMae_ficha_personal Where nom_personal Like '%" & strBuscar & "%' and cod_empresa = '" & cboEmpresa.SelectedValue & "' and est_vigencia = 1 order by nom_personal"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cboBuscarNombre.DataTextField = "nom_personal"
            cboBuscarNombre.DataValueField = "rut_personal"
            cboBuscarNombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cboBuscarNombre.DataBind()

        Catch ex As Exception
            MessageBoxError("CargarComboNombre", Err, Page, Master)
        End Try
    End Sub

    Private Sub BuscarPersonal()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec pro_control_vacaciones 'BP', '" & Trim(txtRut.Text) & "', '" & cboEmpresa.SelectedValue & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                txtRut.Text = rdoReader(0).ToString
                txtnombre.Text = rdoReader(1).ToString
                txtFecIngreso.Text = rdoReader(2).ToString
                Call RescatarDias()
            Else
                MessageBox("Buscar", "Personal NO pertenece a empresa seleccionada", Page, Master, "E")

            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboBuscarNombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBuscarNombre.SelectedIndexChanged
        Dim blnVisible As Boolean = cboBuscarNombre.Visible
        Try
            cboBuscarNombre.Visible = Not blnVisible
            txtnombre.Visible = blnVisible
            If cboBuscarNombre.SelectedValue <> "0" Then
                Dim varValores() As String = Split(cboBuscarNombre.SelectedValue, ";")
                txtRut.Text = varValores(0).ToString
                Call BuscarPersonal()

                ' Call CargarGrillaSaldos()
            End If
        Catch ex As Exception
            MessageBoxError("cboBuscar_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla(ByVal nombre As String)
        Try

            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_control_vacaciones 'GDI' , '" & Trim(nombre) & "', '" & cboEmpresa.SelectedValue & "'"
            grdDias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdDias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla("")
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Trim(txtRut.Text)
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        End If
    End Sub

    Private Sub grdDias_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdDias.PageIndexChanging
        Try
            grdDias.PageIndex = e.NewPageIndex
            If txtBuscar.Text = "" Then
                Call CargarGrilla("")
            Else
                Call CargarGrilla(txtBuscar.Text)
            End If
        Catch ex As Exception
            MessageBoxError("grdDias_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGuardarEsp_Click(sender As Object, e As EventArgs) Handles btnGuardarEsp.Click
        If cboTipoDias.SelectedValue = 4 Then
            Call Actualizar4Rol()
        End If

        If hdnExisteDias.Value = 0 Then
            Call Guardar()
        Else
            Call Actualizar()
        End If

    End Sub

    Private Sub Limpiar()
        txtnombre.Text = ""
        cboBuscarNombre.Visible = False
        txtRut.Text = ""
        txtFecIngreso.Text = ""
        cboTipoDias.ClearSelection()
        cboPeriodoOtros.ClearSelection()
        txtDiasEsp.Text = ""
        pnlDR.Visible = False
        hdnExisteDias.Value = 0
        Call CargarGrilla("")
        txtBuscar.Text = ""
    End Sub

    Private Sub Guardar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@id_periodo_vac", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo_vac").Value = cboPeriodoOtros.SelectedValue
            comando.Parameters.Add("@id_dia_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_dia_esp").Value = cboTipoDias.SelectedValue
            comando.Parameters.Add("@num_saldo", SqlDbType.NVarChar)
            comando.Parameters("@num_saldo").Value = Trim(txtDiasEsp.Text)
            comando.Parameters.Add("@usr", SqlDbType.NVarChar)
            comando.Parameters("@usr").Value = Session("id_usu_per")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Registro Almacenado", Page, Master, "S")
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Private Sub Actualizar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@id_periodo_vac", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo_vac").Value = cboPeriodoOtros.SelectedValue
            comando.Parameters.Add("@id_dia_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_dia_esp").Value = cboTipoDias.SelectedValue
            comando.Parameters.Add("@num_saldo", SqlDbType.NVarChar)
            comando.Parameters("@num_saldo").Value = Val(txtDiasEsp.Text) - Val(txtDiasPago.Text)
            comando.Parameters.Add("@usr", SqlDbType.NVarChar)
            comando.Parameters("@usr").Value = Session("id_usu_per")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
            Call Limpiar()
        Catch ex As Exception
            MessageBoxError("Actualizar", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarDias()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_control_vacaciones 'RDI', '" & Trim(txtRut.Text) & "', '" & cboPeriodoOtros.SelectedValue & "', '" & cboTipoDias.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtDiasEsp.Text = rdoReader(0).ToString

                    If rdoReader(1).ToString = "0" Then
                        hdnExisteDias.Value = 0
                    Else
                        hdnExisteDias.Value = 1
                    End If

                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarDias", Err, Page, Master)
        End Try
    End Sub

    Private Sub Recatar4Rol()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_control_vacaciones 'R4R', '" & Trim(txtRut.Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then

                    If rdoReader(4).ToString = True Then
                        chkVigencia.Checked = True
                    Else
                        chkVigencia.Checked = False
                    End If

                    txtFecIngreso4rol.Text = CDate(rdoReader(0).ToString)

                    If rdoReader(0).ToString = True Then
                        chkPagados.Checked = True
                        txtFecPago.Text = CDate(rdoReader(2).ToString)
                        txtDiasPago.Text = Trim(rdoReader(3).ToString)
                    Else
                        chkPagados.Checked = False
                        txtFecPago.Text = ""
                        txtDiasPago.Text = ""
                    End If



                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("Recatar4Rol", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        If Trim(txtBuscar.Text) = "" Then
            Call CargarGrilla("")
        Else
            Call CargarGrilla(txtBuscar.Text)
        End If
    End Sub



    Protected Sub cboTipoDias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoDias.SelectedIndexChanged
        Call RescatarDias()

        If cboTipoDias.SelectedValue = 4 Then
            pnlDR.Visible = True
            Call Recatar4Rol()
        Else
            txtFecPago.Text = ""
            txtFecIngreso4rol.Text = ""
            chkPagados.Checked = False
            txtDiasPago.Text = ""
            pnlDR.Visible = False
        End If
    End Sub

    Protected Sub cboPeriodoOtros_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoOtros.SelectedIndexChanged
        Call RescatarDias()
    End Sub

    Protected Sub grdDias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdDias.SelectedIndexChanged
        Dim row As GridViewRow = grdDias.SelectedRow
        Dim rut As Label = CType(row.FindControl("lblRut"), Label)
        Dim obj_saldo As Label = CType(row.FindControl("lblSaldo"), Label)
        Dim obj_PeriodoVac As HiddenField = CType(row.FindControl("hdnIdPeriodo"), HiddenField)
        Dim obj_DiaEsp As HiddenField = CType(row.FindControl("hdnDiaEsp"), HiddenField)


        txtRut.Text = Trim(rut.Text)
        cboPeriodoOtros.SelectedValue = obj_PeriodoVac.Value
        cboTipoDias.SelectedValue = obj_DiaEsp.Value
        txtDiasEsp.Text = obj_saldo.Text

        Call RescatarDias()

        If cboTipoDias.SelectedValue = 4 Then
            pnlDR.Visible = True
            Call Recatar4Rol()
        Else
            txtFecPago.Text = ""
            txtFecIngreso4rol.Text = ""
            chkPagados.Checked = False
            txtDiasPago.Text = ""
            pnlDR.Visible = False
        End If

        Call BuscarPersonal()

    End Sub

    Private Sub grdDias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDias.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "2" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdDias.Rows(intRow)
                Dim objID As HiddenField = CType(row.FindControl("hdnId"), HiddenField)

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_control_vacaciones"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "EPE"
                comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
                comando.Parameters("@busqueda").Value = objID.Value
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                If Trim(txtBuscar.Text) = "" Then
                    Call CargarGrilla("")
                Else
                    Call CargarGrilla(Trim(txtBuscar.Text))
                End If

                MessageBox("Eliminar", "Permiso Eliminado", Page, Master, "S")

            End If

        Catch ex As Exception
            MessageBoxError("grdHistorial_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Protected Sub chkPagados_CheckedChanged(sender As Object, e As EventArgs) Handles chkPagados.CheckedChanged
        If chkPagados.Checked = True Then
            txtDiasPago.Enabled = True
            txtFecPago.Enabled = True
            txtFecPago.Text = Date.Now.Date
        Else
            txtDiasPago.Enabled = False
            txtFecPago.Enabled = False
        End If
    End Sub

    Private Sub Actualizar4Rol()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "UPE"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@id_periodo_vac", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo_vac").Value = cboPeriodoOtros.SelectedValue
            comando.Parameters.Add("@est_pago", SqlDbType.Bit)
            comando.Parameters("@est_pago").Value = chkPagados.Checked
            comando.Parameters.Add("@fec_pago", SqlDbType.NVarChar)
            comando.Parameters("@fec_pago").Value = Trim(txtFecPago.Text)
            comando.Parameters.Add("@dias_pago", SqlDbType.NVarChar)
            comando.Parameters("@dias_pago").Value = Trim(txtDiasEsp.Text)
            comando.Parameters.Add("@id_dia_esp", SqlDbType.NVarChar)
            comando.Parameters("@id_dia_esp").Value = cboTipoDias.SelectedValue
            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@fec_ingreso").Value = CDate(txtFecIngreso4rol.Text)
            comando.Parameters.Add("@user_edit", SqlDbType.NVarChar)
            comando.Parameters("@user_edit").Value = Session("id_usu_per")
            comando.Parameters.Add("@usr", SqlDbType.NVarChar)
            comando.Parameters("@usr").Value = Session("id_usu_per")
            comando.Parameters.Add("@est_vigencia", SqlDbType.Bit)
            comando.Parameters("@est_vigencia").Value = chkVigencia.Checked
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("Actualizar4Rol", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtDiasPago_TextChanged(sender As Object, e As EventArgs) Handles txtDiasPago.TextChanged
        If Val(txtDiasPago.Text) > Val(txtDiasEsp.Text) Then
            MessageBox("Pago", "Exede los dias de saldo", Page, Master, "W")
            txtDiasPago.Text = 0
        End If

    End Sub
End Class