﻿Imports System.IO
Imports System.Data.OleDb
Imports aspSistemasJorquera.vbClasePrincipal
Imports System.Data.SqlClient

Public Class aspNominaProgresivos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then


            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
        If fuplCargar.HasFile = True Then
            Call CargarArchivo(fuplCargar, Me.Request, grdRuts)
            btnGuardar.Enabled = True
        Else
            MessageBox("Cargar", "Debe Seleccionar un Archivo antes de continuar", Page, Master, "W")
        End If
    End Sub

    Private Sub CargarArchivo(ByVal archivo As FileUpload, ByVal instancia As HttpRequest, ByVal tabla As GridView)
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = instancia.PhysicalApplicationPath

        Try
            If archivo.HasFile Then
                Dim _extension As String = Path.GetExtension(archivo.FileName)
                If ValidarExtension(_extension) Then
                    Dim _directorioParaGuardar As String = _carpeta + archivo.FileName
                    Call EliminarArchivos()
                    archivo.SaveAs(_directorioParaGuardar)
                    tabla.DataSource = MostrarExcel(_directorioParaGuardar)
                    tabla.DataBind()

                Else
                    MessageBox("Cargar", "Debe seleccionar un archivo válido (.xls, .xlsx)", Page, Master, "W")
                End If
            End If
        Catch ex As Exception
            MessageBoxError("cargar", Err, Page, Master)
        End Try
    End Sub

    Public Shared Function ValidarExtension(ByVal extension As String) As Boolean
        Select Case extension.ToLower()
            Case ".xlsx"
                Return True
            Case ".xls"
                Return True
            Case Else
                Return False
        End Select
    End Function


    Public Shared Sub EliminarArchivos()
        Dim _carpeta As String = "C:\CargarDescuentos\"
        Dim _directorioGral As String = _carpeta

        Dim directorio As New DirectoryInfo(_directorioGral)
        Dim archivos As FileInfo() = directorio.GetFiles()
        Dim archivo As FileInfo
        For Each archivo In archivos
            archivo.Delete()
        Next
    End Sub

    Function MostrarExcel(ByVal ruta As String) As DataTable
        Dim strTmpCnx As String = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0", ruta)
        Dim cnxBaseDatos As New OleDbConnection(strTmpCnx)
        Dim dtTemporal As New DataTable, dtFinal As New DataTable
        Dim dtFilaN As DataRow

        Try
            dtFinal.Columns.AddRange(New DataColumn() {New DataColumn("rut", GetType(String)),
                New DataColumn("dias", GetType(String))
                                 })

            cnxBaseDatos.Open()
            Dim cmdTemporal As OleDbCommand = New OleDbCommand("SELECT rut,dias FROM [Hoja1$]", cnxBaseDatos)
            Dim _oleda As OleDbDataAdapter = New OleDbDataAdapter()
            _oleda.SelectCommand = cmdTemporal
            _oleda.Fill(dtTemporal)
            _oleda.Dispose()
            cmdTemporal.Dispose()

            Dim i As Integer
            For i = 0 To dtTemporal.Rows.Count - 1
                dtFilaN = dtFinal.NewRow()
                dtFilaN.Item(0) = dtTemporal(i).Item(0)
                dtFilaN.Item(1) = dtTemporal(i).Item(1)
                dtFinal.Rows.Add(dtFilaN)
            Next

        Catch ex As Exception
            MessageBoxError("MostrarExcel", Err, Page, Master)
        Finally
            cnxBaseDatos.Close()
            cnxBaseDatos.Dispose()
        End Try
        Return dtFinal
    End Function



    Function GrabarDatos() As Boolean
        Dim intRow As Integer = 0, blnError As Boolean = True
        Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
        Dim esOK As Boolean = True
        cnxBaseDatos.Open()
        Try
            For intRow = 0 To grdRuts.Rows.Count - 1

                Dim objRut As Label = CType(grdRuts.Rows(intRow).FindControl("lblRut"), Label)
                Dim objDias As Label = CType(grdRuts.Rows(intRow).FindControl("lblDias"), Label)

                Call GuardarRuts(objRut.Text, objDias.Text)
            Next
            MessageBox("Guardar", "Registro Guardado Exitosamente", Page, Master, "S")

            cnxBaseDatos.Close()
        Catch ex As Exception
            MessageBoxError("GrabarDatos", Err, Page, Master)
        End Try
        Return esOK
    End Function

    Public Sub GuardarRuts(ByVal rut As String, ByVal dias As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "mant_RRHH_tempRutNomina"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "IP"
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = Trim(rut)
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Trim(dias)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("GuardarRuts", Err, Page, Master)
        End Try
    End Sub



    Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim _directorioParaGuardar As String = "C:\CargarDescuentos\"
        If GrabarDatos() = True Then
            MessageBox("Cargar", "Informacion almacenada", Page, Master, "S")
            grdRuts.DataSource = MostrarExcel(_directorioParaGuardar)
            grdRuts.DataBind()

            'Call EliminarRuts()

        Else
            MessageBox("Guardar", "Ocurrió un error al grabar los datos, Pudo no haberse guardado toda la información" & "\n" & "Intentar cargar  y guardar el archivo nuevamente", Page, Master, "W")
        End If
    End Sub
End Class