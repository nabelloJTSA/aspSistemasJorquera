﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspPermisosLicencias
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call carga_empresas()
                dtFechaIni.Text = Date.Now
                mtvReporte.ActiveViewIndex = 0
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_empresas()
        Dim strNomTabla As String = "REMMae_empresa"
        Dim strSQL As String = "Exec rpt_permisos_licencias 'CCE'"
        cboEmpresa.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        cboEmpresa.DataTextField = "nom_empresa"
        cboEmpresa.DataValueField = "cod_empresa"
        cboEmpresa.DataBind()


        cboEmpresaAnulados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        cboEmpresaAnulados.DataTextField = "nom_empresa"
        cboEmpresaAnulados.DataValueField = "cod_empresa"
        cboEmpresaAnulados.DataBind()
    End Sub

    Private Sub CargarGrilla(ByVal nombre As String, ByVal rut As String)
        Try
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            Dim strSQL As String = "Exec rpt_permisos_licencias 'G', '" & Trim(nombre) & "',  '" & Trim(rut) & "', '" & cboEmpresa.SelectedValue & "', '" & cboTipo.SelectedValue & "', '" & CDate(dtFechaIni.Text) & "'"
            grdPermisos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdPermisos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaAnular()
        Try
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            Dim strSQL As String = "Exec rpt_permisos_licencias 'GPA', '" & TxtFecAnulados.Text & "',  '" & cboEmpresaAnulados.SelectedValue & "', '" & txtRutAnulado.Text & "'"
            grdAnulados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdAnulados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaAnular", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorico()
        Try
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            Dim strSQL As String = "Exec rpt_permisos_licencias 'GHI', '" & Trim(txtRutHis.Text) & "', '" & cboTipoHis.SelectedValue & "'"
            grdHistorico.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorico.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorico", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Call FiltrosGrilla()
        If grdPermisos.Rows.Count > 0 Then
            pnlPermisos.Visible = True
        Else
            pnlPermisos.Visible = False
        End If
    End Sub

    Protected Sub txtrut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        If Trim(txtRut.Text) = "" Then
            Call FiltrosGrilla()
        Else
            txtRut.Text = Replace(txtRut.Text, ".", "")
            txtRut.Text = Replace(txtRut.Text, "-", "")
            If Len(txtRut.Text) = 8 Then
                txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
            ElseIf Len(txtRut.Text) = 9 Then
                txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
            End If

            If Len(txtRut.Text) = 10 Then
                txtRut.Text = txtRut.Text
                If ValidarRut(txtRut) = True Then
                    Call FiltrosGrilla()
                Else
                    MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                End If
            ElseIf Len(txtRut.Text) = 9 Then
                txtRut.Text = "0" + txtRut.Text
                If ValidarRut(txtRut) = True Then
                    Call FiltrosGrilla()
                Else
                    MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                End If
            End If
        End If

    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        Call FiltrosGrilla()
    End Sub


    Private Sub FiltrosGrilla()
        If dtFechaIni.Text = "" Then
            dtFechaIni.Text = "01/01/1900"
        End If
        If Trim(txtbNombre.Text) = "" Then
            If Trim(txtRut.Text) = "" Then
                Call CargarGrilla("", "")
            Else
                Call CargarGrilla("", Trim(txtRut.Text))
            End If
        Else
            If Trim(txtRut.Text) = "" Then
                Call CargarGrilla(Trim(txtbNombre.Text), "")
            Else
                Call CargarGrilla(Trim(txtbNombre.Text), Trim(txtRut.Text))
            End If
        End If
    End Sub

    Protected Sub btnBuscar0_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String

            If Trim(txtbNombre.Text) = "" Then
                If Trim(txtRut.Text) = "" Then
                    strSQL = "Exec rpt_permisos_licencias 'EXP', '',  '', '" & cboEmpresa.SelectedValue & "', '" & cboTipo.SelectedValue & "', '" & CDate(dtFechaIni.Text) & "'"
                Else
                    strSQL = "Exec rpt_permisos_licencias 'EXP', '',  '" & Trim(txtRut.Text) & "', '" & cboEmpresa.SelectedValue & "', '" & cboTipo.SelectedValue & "', '" & CDate(dtFechaIni.Text) & "'"
                End If
            Else
                If Trim(txtRut.Text) = "" Then
                    strSQL = "Exec rpt_permisos_licencias 'EXP', '" & Trim(txtbNombre.Text) & "',  '', '" & cboEmpresa.SelectedValue & "', '" & cboTipo.SelectedValue & "', '" & CDate(dtFechaIni.Text) & "'"
                Else
                    strSQL = "Exec rpt_permisos_licencias 'EXP', '" & Trim(txtbNombre.Text) & "',  '" & Trim(txtRut.Text) & "', '" & cboEmpresa.SelectedValue & "', '" & cboTipo.SelectedValue & "', '" & CDate(dtFechaIni.Text) & "'"
                End If
            End If

            Dim strNomTabla As String = "REMMov_movimiento_personal"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Permisos_" & cboEmpresa.SelectedItem.ToString & "_" & cboTipo.SelectedItem.ToString & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGral_Click(sender As Object, e As EventArgs) Handles btnGral.Click
        mtvReporte.ActiveViewIndex = 0
    End Sub

    Protected Sub btnHistorico_Click(sender As Object, e As EventArgs) Handles btnHistorico.Click
        mtvReporte.ActiveViewIndex = 1

    End Sub

    Protected Sub btnBuscarHistorico_Click(sender As Object, e As EventArgs) Handles btnBuscarHistorico.Click
        If Trim(txtRutHis.Text) = "" Then
            MessageBox("Rut", "Ingrese RUT a buscar", Page, Master, "W")
            pnlHis.Visible = False
        Else
            Call CargarGrillaHistorico()
            pnlHis.Visible = True
        End If
    End Sub

    Protected Sub btnExportarHistorico_Click(sender As Object, e As EventArgs) Handles btnExportarHistorico.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String = "Exec rpt_permisos_licencias 'GHI', '" & Trim(txtRutHis.Text) & "'"
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Permisos_Historicos_" & txtRutHis.Text & ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarHistorico_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtRutHis_TextChanged(sender As Object, e As EventArgs) Handles txtRutHis.TextChanged

        txtRutHis.Text = Replace(txtRutHis.Text, ".", "")
        txtRutHis.Text = Replace(txtRutHis.Text, "-", "")
        If Len(txtRutHis.Text) = 8 Then
            txtRutHis.Text = Left(txtRutHis.Text, 7) + "-" + Right(txtRutHis.Text, 1)
        ElseIf Len(txtRutHis.Text) = 9 Then
            txtRutHis.Text = Left(txtRutHis.Text, 8) + "-" + Right(txtRutHis.Text, 1)
        End If

        If Len(txtRutHis.Text) = 10 Then
            txtRutHis.Text = txtRutHis.Text
            If ValidarRut(txtRutHis) = True Then
                btnBuscarHistorico.Enabled = True
                btnExportarHistorico.Enabled = True
            Else
                btnBuscarHistorico.Enabled = False
                btnExportarHistorico.Enabled = False
            End If
        ElseIf Len(txtRutHis.Text) = 9 Then
            txtRutHis.Text = "0" + txtRutHis.Text
            If ValidarRut(txtRutHis) = True Then
                btnBuscarHistorico.Enabled = True
                btnExportarHistorico.Enabled = True
            Else
                btnBuscarHistorico.Enabled = False
                btnExportarHistorico.Enabled = False
            End If
        End If

    End Sub

    Protected Sub btAnulados_Click(sender As Object, e As EventArgs) Handles btAnulados.Click
        mtvReporte.ActiveViewIndex = 2
    End Sub

    Protected Sub btnBuscarAnulado_Click(sender As Object, e As EventArgs) Handles btnBuscarAnulado.Click
        Call CargarGrillaAnular()
    End Sub

    Protected Sub btnExportarAnulado_Click(sender As Object, e As EventArgs) Handles btnExportarAnulado.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String = "Exec rpt_permisos_licencias 'GPA', '" & TxtFecAnulados.Text & "',  '" & cboEmpresaAnulados.SelectedValue & "', '" & txtRutAnulado.Text & "'"
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=Permisos_Anulados.xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportarHistorico_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdAnulados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdAnulados.SelectedIndexChanged

    End Sub

    Private Sub grdAnulados_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdAnulados.PageIndexChanging
        Try
            grdAnulados.PageIndex = e.NewPageIndex

            Call CargarGrillaAnular()
        Catch ex As Exception
            MessageBoxError("grdCartas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdPermisos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdPermisos.SelectedIndexChanged

    End Sub

    Private Sub grdPermisos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdPermisos.PageIndexChanging
        Try
            grdPermisos.PageIndex = e.NewPageIndex

            Call FiltrosGrilla()
        Catch ex As Exception
            MessageBoxError("grdCartas_PageIndexChanging", Err, Page, Master)
        End Try


    End Sub

    Protected Sub grdHistorico_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdHistorico.SelectedIndexChanged

    End Sub

    Private Sub grdHistorico_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdHistorico.PageIndexChanging
        Try
            grdHistorico.PageIndex = e.NewPageIndex

            Call CargarGrillaHistorico()
        Catch ex As Exception
            MessageBoxError("grdCartas_PageIndexChanging", Err, Page, Master)
        End Try


    End Sub
End Class