﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal
Public Class aspAdelantarVacaciones
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Adelantar Días"
                Call carga_combo_empresa()
                Call carga_especiales()
                Call carga_periodo_año()
                mtvVac.ActiveViewIndex = 0
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSql As String = "Exec pro_adelantar_vacaciones 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSql, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()

            cboEmpresaS.DataSource = dtsTablas(strSql, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresaS.DataTextField = "nom_empresa"
            cboEmpresaS.DataValueField = "cod_empresa"
            cboEmpresaS.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub


    Private Sub carga_especiales()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_adelantar_vacaciones 'CDE'"
            cboTipoDias.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboTipoDias.DataTextField = "nom_dia"
            cboTipoDias.DataValueField = "cod_tipo_movimiento"
            cboTipoDias.DataBind()

            cboTipoDiasS.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboTipoDiasS.DataTextField = "nom_dia"
            cboTipoDiasS.DataValueField = "cod_tipo_movimiento"
            cboTipoDiasS.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_especiales", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_periodo_año()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_adelantar_vacaciones 'CPE'"
            cboPeriodoOtros.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPeriodoOtros.DataTextField = "nom_periodo"
            cboPeriodoOtros.DataValueField = "id_periodo_vac"
            cboPeriodoOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_anio", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarComboNombre(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Select rut_personal= '0', nom_personal= '<Seleccionar>' UNION Select rut_personal, nom_personal From REMMae_ficha_personal Where nom_personal Like '%" & strBuscar & "%' and cod_empresa = '" & cboEmpresa.SelectedValue & "' and est_vigencia = 1 order by nom_personal"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cbobuscarnombre.DataTextField = "nom_personal"
            cbobuscarnombre.DataValueField = "rut_personal"
            cbobuscarnombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cbobuscarnombre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombre", Err, Page, Master)
        End Try
    End Sub


    Private Sub BuscarPersonal()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec pro_adelantar_vacaciones 'BP', '" & Trim(txtRut.Text) & "', '" & cboEmpresa.SelectedValue & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                txtRut.Text = rdoReader(0).ToString
                txtnombre.Text = rdoReader(1).ToString
                txtFecIngreso.Text = rdoReader(2).ToString
                Call RescatarDias()
                Call CargarGrilla()
                pnlBusqueda.Visible = True
            Else
                pnlBusqueda.Visible = False
                MessageBox("Buscar", "Personal NO pertenece a empresa seleccionada", Page, Master, "E")
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarDias()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_adelantar_vacaciones 'RDI', '" & Trim(txtRut.Text) & "', '" & cboPeriodoOtros.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblSaldoAct.Visible = True
                    lblSaldoAct.Text = "Saldo Actual: " & rdoReader(0).ToString & " dias  "
                Else
                    lblSaldoAct.Visible = True
                    lblSaldoAct.Text = "Saldo Actual: 0 dias  "
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarDias", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboBuscarNombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbobuscarnombre.SelectedIndexChanged
        Dim blnVisible As Boolean = cbobuscarnombre.Visible
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtnombre.Visible = blnVisible
            If cbobuscarnombre.SelectedValue <> "0" Then
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
                txtRut.Text = varValores(0).ToString
                Call BuscarPersonal()

            End If
        Catch ex As Exception
            MessageBoxError("cboBuscar_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrilla()
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_adelantar_vacaciones 'GDI', '" & Trim(txtRut.Text) & "'"
            grdDias.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdDias.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call Limpiar()

        If cboEmpresa.SelectedValue = 0 Then
            pnlDatos.Visible = False
            pnlBusqueda.Visible = False
        Else
            pnlDatos.Visible = True
            pnlBusqueda.Visible = False
        End If
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        txtRut.Text = Trim(txtRut.Text)
        txtRut.Text = Replace(txtRut.Text, ".", "")
        txtRut.Text = Replace(txtRut.Text, "-", "")
        If Len(txtRut.Text) = 8 Then
            txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
        End If

        If Len(txtRut.Text) = 10 Then
            txtRut.Text = txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        ElseIf Len(txtRut.Text) = 9 Then
            txtRut.Text = "0" + txtRut.Text
            If ValidarRut(txtRut) = True Then
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        End If
    End Sub

    Protected Sub btnGuardarEsp_Click(sender As Object, e As EventArgs) Handles btnGuardarEsp.Click
        If Trim(txtMotivo.Text) = "" Then
            MessageBox("Guardar", "Debe ingresar un motivo", Page, Master, "W")
        Else
            Call Guardar()
        End If

    End Sub

    Private Sub Limpiar()
        txtnombre.Text = ""
        cbobuscarnombre.Visible = False
        txtRut.Text = ""
        txtFecIngreso.Text = ""
        cboTipoDias.ClearSelection()
        cboPeriodoOtros.ClearSelection()
        txtDiasAdd.Text = ""
        txtMotivo.Text = ""
        hdnExisteDias.Value = 0
        Call CargarGrilla()
        pnlBusqueda.Visible = False
    End Sub

    Private Sub Guardar()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_adelantar_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRut.Text)
            comando.Parameters.Add("@id_periodo_vac", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo_vac").Value = cboPeriodoOtros.SelectedValue
            comando.Parameters.Add("@num_dias", SqlDbType.Float)
            comando.Parameters("@num_dias").Value = Trim(Replace(txtDiasAdd.Text, ".", ","))
            comando.Parameters.Add("@motivo", SqlDbType.NVarChar)
            comando.Parameters("@motivo").Value = Trim(txtMotivo.Text)
            comando.Parameters.Add("@usr", SqlDbType.NVarChar)
            comando.Parameters("@usr").Value = Session("id_usu_per")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Guardar", "Registro Exitoso", Page, Master, "S")
            Call CargarGrilla()
            txtDiasAdd.Text = ""
            txtMotivo.Text = ""
        Catch ex As Exception
            MessageBoxError("Guardar", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboTipoDias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipoDias.SelectedIndexChanged
        Call RescatarDias()
    End Sub

    Protected Sub cboPeriodoOtros_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoOtros.SelectedIndexChanged
        Call RescatarDias()
    End Sub

    Private Sub grdDias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDias.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdDias.Rows(intRow)
                Dim objID As HiddenField = CType(row.FindControl("hdnId"), HiddenField)

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_adelantar_vacaciones"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "E"
                comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
                comando.Parameters("@busqueda").Value = objID.Value
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                MessageBox("Eliminar", "Registro Eliminado", Page, Master, "S")
                Call CargarGrilla()
            End If

        Catch ex As Exception
            MessageBoxError("grdHistorial_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdDias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDias.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objBtnE As ImageButton = CType(row.FindControl("btnEliminar"), ImageButton)
                Dim objEstado As HiddenField = CType(row.FindControl("hdnEstado"), HiddenField)
                Dim objIdSolicitud As HiddenField = CType(row.FindControl("hdnIdSolicitud"), HiddenField)

                If objIdSolicitud.Value = "0" Then
                    objBtnE.Visible = True
                Else
                    objBtnE.Visible = False
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdDias_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnAdelantar_Click(sender As Object, e As EventArgs) Handles btnAdelantar.Click
        btnAdelantar.CssClass = btnAdelantar.CssClass.Replace("btn-outline-info", "btn-info active")
        btnSAldos.CssClass = btnSAldos.CssClass.Replace("btn-info active", "btn-outline-info")
        mtvVac.ActiveViewIndex = 0
        lbltitulo.Text = "Adelantar Días"
    End Sub

    Protected Sub btnSAldos_Click(sender As Object, e As EventArgs) Handles btnSAldos.Click
        btnSAldos.CssClass = btnSAldos.CssClass.Replace("btn-outline-info", "btn-info active")
        btnAdelantar.CssClass = btnAdelantar.CssClass.Replace("btn-info active", "btn-outline-info")
        mtvVac.ActiveViewIndex = 1
        lbltitulo.Text = "Regularizar Saldos"
    End Sub


    Private Sub CargaPeriodoSaldos()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_saldos_vacaciones 'CPE', '" & Trim(txtRutS.Text) & "'"
            cboPeriodoOtrosS.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPeriodoOtrosS.DataTextField = "nom_periodo"
            cboPeriodoOtrosS.DataValueField = "id_periodo_vac"
            cboPeriodoOtrosS.DataBind()
        Catch ex As Exception
            MessageBoxError("CargaPeriodoSaldos", Err, Page, Master)
        End Try
    End Sub



    Private Sub CargarComboNombreS(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Select rut_personal= '0', nom_personal= '<Seleccionar>' UNION Select rut_personal, nom_personal From REMMae_ficha_personal Where nom_personal Like '%" & strBuscar & "%' and cod_empresa = '" & cboEmpresaS.SelectedValue & "' and est_vigencia = 1 order by nom_personal"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cboBuscarNombreS.DataTextField = "nom_personal"
            cboBuscarNombreS.DataValueField = "rut_personal"
            cboBuscarNombreS.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cboBuscarNombreS.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombreS", Err, Page, Master)
        End Try
    End Sub


    Private Sub BuscarPersonalS()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec pro_saldos_vacaciones 'BP', '" & Trim(txtRutS.Text) & "', '" & cboEmpresaS.SelectedValue & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                Call LimpiarSaldos()
                txtRutS.Text = rdoReader(0).ToString
                txtnombreS.Text = rdoReader(1).ToString
                txtFecIngresoS.Text = rdoReader(2).ToString
                Call CargaPeriodoSaldos()
                Call RescatarDiasSaldos()
                Call CargarGrillaSaldos()
                pnlBusquedaS.Visible = True
            Else
                pnlBusquedaS.Visible = False
                MessageBox("Buscar", "Personal NO pertenece a empresa seleccionada", Page, Master, "E")
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarDiasSaldos()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "Exec pro_saldos_vacaciones 'RDI', '" & Trim(txtRutS.Text) & "', '" & cboPeriodoOtrosS.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblSaldoActS.Visible = True
                    lblSaldoActS.Text = "Saldo Actual: " & rdoReader(0).ToString & " dias  "
                Else
                    lblSaldoActS.Visible = False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarDiasSaldos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboBuscarNombreS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBuscarNombreS.SelectedIndexChanged
        Dim blnVisible As Boolean = cboBuscarNombreS.Visible
        Try
            cboBuscarNombreS.Visible = Not blnVisible
            txtnombreS.Visible = blnVisible
            If cboBuscarNombreS.SelectedValue <> "0" Then
                Dim varValores() As String = Split(cboBuscarNombreS.SelectedValue, ";")
                txtRutS.Text = varValores(0).ToString
                Call BuscarPersonalS()

            End If
        Catch ex As Exception
            MessageBoxError("cboBuscarNombreS_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaSaldos()
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_saldos_vacaciones 'G', '" & Trim(txtRutS.Text) & "'"
            grdSaldos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdSaldos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaSaldos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresaS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresaS.SelectedIndexChanged
        Call LimpiarSaldos()

        If cboEmpresaS.SelectedValue = 0 Then
            pnlDatosS.Visible = False
            pnlBusquedaS.Visible = False
        Else
            pnlDatosS.Visible = True
            pnlBusquedaS.Visible = False
        End If
    End Sub

    Protected Sub txtRutS_TextChanged(sender As Object, e As EventArgs) Handles txtRutS.TextChanged
        txtRutS.Text = Trim(txtRutS.Text)
        txtRutS.Text = Replace(txtRutS.Text, ".", "")
        txtRutS.Text = Replace(txtRutS.Text, "-", "")
        If Len(txtRutS.Text) = 8 Then
            txtRutS.Text = Left(txtRutS.Text, 7) + "-" + Right(txtRutS.Text, 1)
        ElseIf Len(txtRutS.Text) = 9 Then
            txtRutS.Text = Left(txtRutS.Text, 8) + "-" + Right(txtRutS.Text, 1)
        End If

        If Len(txtRutS.Text) = 10 Then
            txtRutS.Text = txtRutS.Text
            If ValidarRut(txtRutS) = True Then
                Call BuscarPersonalS()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        ElseIf Len(txtRutS.Text) = 9 Then
            txtRutS.Text = "0" + txtRutS.Text
            If ValidarRut(txtRutS) = True Then
                Call BuscarPersonalS()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
            End If
        End If
    End Sub

    Protected Sub btnGuardarS_Click(sender As Object, e As EventArgs) Handles btnGuardarS.Click
        If Trim(txtMotivoS.Text) = "" Then
            MessageBox("Saldos", "Debe ingresar un motivo", Page, Master, "W")
        Else
            Call ActualizarSaldos()
        End If

    End Sub

    Private Sub LimpiarSaldos()
        txtnombreS.Text = ""
        cboBuscarNombreS.Visible = False
        txtRutS.Text = ""
        txtFecIngresoS.Text = ""
        cboTipoDiasS.ClearSelection()
        cboPeriodoOtrosS.ClearSelection()
        txtSaldoNuevo.Text = ""
        txtMotivoS.Text = ""
        Call CargarGrillaSaldos()
        pnlBusquedaS.Visible = False
    End Sub

    Private Sub ActualizarSaldos()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_saldos_vacaciones"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "U"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtRutS.Text)
            comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
            comando.Parameters("@busqueda").Value = cboPeriodoOtrosS.SelectedValue
            comando.Parameters.Add("@num_dias", SqlDbType.Float)
            comando.Parameters("@num_dias").Value = Trim(Replace(txtSaldoNuevo.Text, ".", ","))
            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
            comando.Parameters("@busqueda2").Value = Session("id_usu_per")
            comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
            comando.Parameters("@busqueda3").Value = Trim(txtMotivoS.Text)
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
            MessageBox("Actualizar", "Registro Actualizado", Page, Master, "S")
            Call CargarGrillaSaldos()
            txtSaldoNuevo.Text = ""
            txtMotivoS.Text = ""
        Catch ex As Exception
            MessageBoxError("ActualizarSaldos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboPeriodoOtrosS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodoOtrosS.SelectedIndexChanged
        Call RescatarDiasSaldos()
    End Sub

    Protected Sub grdSaldos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdSaldos.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdSaldos.SelectedRow

            Dim hdnid As HiddenField = CType(row.FindControl("hdnId"), HiddenField)
            Dim hdn_IdPeriodo As HiddenField = CType(row.FindControl("hdnIdPeriodo"), HiddenField)


            cboPeriodoOtrosS.SelectedValue = hdn_IdPeriodo.Value

            lblSaldoActS.Visible = True
            lblSaldoActS.Text = "Saldo Actual: " & CType(row.FindControl("lblSaldo"), Label).Text & " dias  "


        Catch ex As Exception
            MessageBoxError("grdSaldos_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub



    Private Sub bntBuscarNombreHorasExtra_Click(sender As Object, e As EventArgs) Handles bntBuscarNombreHorasExtra.Click
        Dim blnVisible As Boolean = cbobuscarnombre.Visible, intRegistros As Integer = 0
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtnombre.Visible = blnVisible
            Call CargarComboNombre(Trim(txtnombre.Text), intRegistros)
            If intRegistros = 2 And cbobuscarnombre.Visible = True Then
                cbobuscarnombre.SelectedIndex = 1
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
            End If
            cbobuscarnombre.ClearSelection()
        Catch ex As Exception
            MessageBoxError("bntBuscarNombreClick", Err, Page, Master)
        End Try
    End Sub

    Private Sub bntBuscarNombreS_Click(sender As Object, e As EventArgs) Handles bntBuscarNombreS.Click
        Dim blnVisible As Boolean = cboBuscarNombreS.Visible, intRegistros As Integer = 0
        Try
            cboBuscarNombreS.Visible = Not blnVisible
            txtnombreS.Visible = blnVisible
            Call CargarComboNombreS(Trim(txtnombreS.Text), intRegistros)
            If intRegistros = 2 And cboBuscarNombreS.Visible = True Then
                cboBuscarNombreS.SelectedIndex = 1
                Dim varValores() As String = Split(cboBuscarNombreS.SelectedValue, ";")
            End If
            cboBuscarNombreS.ClearSelection()
        Catch ex As Exception
            MessageBoxError("bntBuscarNombreS_Click", Err, Page, Master)
        End Try
    End Sub
End Class