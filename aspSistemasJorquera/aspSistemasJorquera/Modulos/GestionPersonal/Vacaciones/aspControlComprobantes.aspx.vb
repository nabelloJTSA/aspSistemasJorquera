﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspControlComprobantes
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltituloP.Text = "Comprobantes Pendientes"

                MtvSolicitudPersonal.ActiveViewIndex = 0

                Dim rtn As New Date
                rtn = Date.Now
                rtn = rtn.AddDays(-rtn.Day + 1)

                dtDesde.Text = rtn
                dtHasta.Text = rtn.AddDays(-rtn.Day + 1).AddMonths(1).AddDays(-1)
                Call carga_combo_empresa()
                Call CargarGrillaFiltroFechas("")

            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa order by cod_empresa"
            cboFiltroEmp.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboFiltroEmp.DataTextField = "nom_empresa"
            cboFiltroEmp.DataValueField = "cod_empresa"
            cboFiltroEmp.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaComprobantes(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'G', '" & nombre & "'"
            grdComprobantes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdComprobantes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaComprobantes", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroEmpresa(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GE', '" & nombre & "', '" & cboFiltroEmp.SelectedValue & "'"
            grdComprobantes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdComprobantes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroEmpresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroFechas(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GF', '" & nombre & "', '" & CDate(dtDesde.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtHasta.Text).ToString("dd-MM-yyyy") & "'"
            grdComprobantes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdComprobantes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroFechas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroEmpresaFechas(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GEF', '" & nombre & "', '" & cboFiltroEmp.SelectedValue & "','" & CDate(dtDesde.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtHasta.Text).ToString("dd-MM-yyyy") & "'"
            grdComprobantes.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdComprobantes.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroEmpresaFechas", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        If MtvSolicitudPersonal.ActiveViewIndex = 0 Then
            If cboFiltroEmp.SelectedValue = 0 Then
                If dtHasta.Text = "" Then
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaComprobantes("")
                    Else
                        Call CargarGrillaComprobantes(txtBuscar.Text)
                    End If
                Else
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroFechas("")
                    Else
                        Call CargarGrillaFiltroFechas(txtBuscar.Text)
                    End If
                End If
            Else
                If dtHasta.Text = "" Then
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroEmpresa("")
                    Else
                        Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                    End If
                Else
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroEmpresaFechas("")
                    Else
                        Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                    End If
                End If
            End If
        Else
            If cboFiltroEmp.SelectedValue = 0 Then
                If dtHasta.Text = "" Then
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaComprobantes_revisados("")
                    Else
                        Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                    End If
                Else
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroFechas_revisados("")
                    Else
                        Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                    End If
                End If
            Else
                If dtHasta.Text = "" Then
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroEmpresa_revisados("")
                    Else
                        Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                    End If
                Else
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroEmpresaFechas_revisados("")
                    Else
                        Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub grdCartas_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdComprobantes.PageIndexChanging
        Try
            grdComprobantes.PageIndex = e.NewPageIndex
            If cboFiltroEmp.SelectedValue = 0 Then
                If dtHasta.Text = "" Then
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaComprobantes("")
                    Else
                        Call CargarGrillaComprobantes(txtBuscar.Text)
                    End If
                Else
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroFechas("")
                    Else
                        Call CargarGrillaFiltroFechas(txtBuscar.Text)
                    End If
                End If
            Else
                If dtHasta.Text = "" Then
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroEmpresa("")
                    Else
                        Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                    End If
                Else
                    If txtBuscar.Text = "" Then
                        Call CargarGrillaFiltroEmpresaFechas("")
                    Else
                        Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdCartas_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdCartas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdComprobantes.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdComprobantes.Rows(intRow)
                ' Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                Dim obj_rut As String = CType(row.FindControl("lblRut"), Label).Text
                Dim obj_nombre As String = CType(row.FindControl("lblNombre"), Label).Text
                Dim obj_empresa As String = CType(row.FindControl("lblEmpresa"), Label).Text
                ' Dim obj_fec_sol As String = CType(row.FindControl("lblFecSol"), Label).Text
                Dim obj_num_comprobante As String = CType(row.FindControl("lblNumComprobante"), Label).Text

                Dim obj_user_add As String = CType(row.FindControl("hdnUserAdd"), HiddenField).Value
                Dim obj_fec_add As String = CType(row.FindControl("hdnFedAdd"), HiddenField).Value

                Dim obj_id As String = CType(row.FindControl("hdn_id"), HiddenField).Value

                hdnId.Value = obj_id
                hdnUserAdd.Value = obj_user_add
                hdnRut.Value = obj_rut
                hdnNombre.Value = obj_nombre
                hdnEmpresa.Value = obj_empresa
                hdnFecSol.Value = obj_fec_add
                hdnNumComprobante.Value = obj_num_comprobante

                Call DevolverMail()


                Try
                    Dim correo As New MailMessage
                    correo.From = New MailAddress("sistemas@viatrack.cl")
                    correo.To.Add("" & hdnMail.Value & "")
                    correo.Subject = "[CV]Comprobantes Pendientes"
                    correo.Body = "Estimado (a), se informa que tiene los siguientes comprobantes de feriados pendientes correspondientes a: " & hdnNombre.Value & ", RUT: " & hdnRut.Value & ", Nº Comprobante: " & hdnNumComprobante.Value & ", solicitada en la fecha: " & hdnFecSol.Value & ""
                    correo.IsBodyHtml = False
                    correo.Priority = MailPriority.Normal
                    Dim smtp As New SmtpClient
                    smtp.Host = "mail.viatrack.cl"
                    smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
                    smtp.Send(correo)
                    correo.Attachments.Clear()
                    correo.Dispose()
                Catch ex As Exception
                    MessageBoxError("btnEnviarRecordar_Click", Err, Page, Master)
                End Try

                Call ActualizarEstRecordad(1, hdnId.Value)

                If cboFiltroEmp.SelectedValue = 0 Then
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaComprobantes("")
                        Else
                            Call CargarGrillaComprobantes(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroFechas("")
                        Else
                            Call CargarGrillaFiltroFechas(txtBuscar.Text)
                        End If
                    End If
                Else
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresa("")
                        Else
                            Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresaFechas("")
                        Else
                            Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                        End If
                    End If
                End If

                MessageBox("Enviar", "Recordatorio enviados a validadores correspondientes", Page, Master, "S")

            ElseIf Trim(LCase(e.CommandName)) = "2" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdComprobantes.Rows(intRow)

                Dim obj_id As String = CType(row.FindControl("hdn_id"), HiddenField).Value
                Dim obj_rut As String = CType(row.FindControl("lblRut"), Label).Text
                Dim obj_nombre As String = CType(row.FindControl("lblNombre"), Label).Text
                Dim obj_empresa As String = CType(row.FindControl("lblEmpresa"), Label).Text
                Dim obj_num_comprobante As String = CType(row.FindControl("lblNumComprobante"), Label).Text
                Dim obj_user_add As String = CType(row.FindControl("hdnUserAdd"), HiddenField).Value
                Dim obj_fec_add As String = CType(row.FindControl("hdnFedAdd"), HiddenField).Value


                hdnId.Value = obj_id

                Call ActualizarEstReversado(1, hdnId.Value, obj_num_comprobante, obj_rut)

                If cboFiltroEmp.SelectedValue = 0 Then
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaComprobantes("")
                        Else
                            Call CargarGrillaComprobantes(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroFechas("")
                        Else
                            Call CargarGrillaFiltroFechas(txtBuscar.Text)
                        End If
                    End If
                Else
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresa("")
                        Else
                            Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresaFechas("")
                        Else
                            Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                        End If
                    End If
                End If

                MessageBox("Reversar", "Movimiento Reversado", Page, Master, "S")


            End If
        Catch ex As Exception
            MessageBoxError("grdCartas_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboEstado(ByRef objCombo As DropDownList)
        Dim strSQL As String = "Select id_estado = 0, nom_estado = 'Ninguno' UNION select id_estado, nom_estado from REMMae_estados_comprobantes order by id_estado"
        Dim strNomTabla As String = "REMMae_estados_comprobantes"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBaseDatos").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboEstado", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboEstado_revisados(ByRef objCombo As DropDownList)
        Dim strSQL As String = "Select id_estado = 0, nom_estado = 'Ninguno' UNION select id_estado, nom_estado from REMMae_estados_comprobantes order by id_estado"
        Dim strNomTabla As String = "REMMae_estados_comprobantes"
        Try
            objCombo.DataSource = dtsTablas(strSQL, strNomTabla, "cnxBaseDatos").Tables(strNomTabla).DefaultView
            objCombo.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboEstado_revisados", Err, Page, Master)
        End Try
    End Sub

    Private Sub Accion()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_control_comprobantes 'EST_R' , " & hdnId.Value & ""
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnAccion.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("Accion", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub grdCartas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdComprobantes.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim objEstado As DropDownList = CType(row.FindControl("cboEstado"), DropDownList)
                Dim objIdEstado As HiddenField = CType(row.FindControl("hdnEstCom"), HiddenField)
                Call CargarComboEstado(objEstado)
                objEstado.SelectedValue = objIdEstado.Value

                ' Dim obj_num As Label = CType(row.FindControl("lblNum"), Label)
                Dim obj_rut As Label = CType(row.FindControl("lblRut"), Label)
                Dim obj_nombre As Label = CType(row.FindControl("lblNombre"), Label)
                Dim obj_empresa As Label = CType(row.FindControl("lblEmpresa"), Label)
                ' Dim obj_fec_sol As Label = CType(row.FindControl("lblFecSol"), Label)
                Dim obj_num_comprobante As Label = CType(row.FindControl("lblNumComprobante"), Label)
                Dim obj_fec_inicio As Label = CType(row.FindControl("lblFecIni"), Label)
                Dim obj_fec_termino As Label = CType(row.FindControl("lblFecTermino"), Label)
                Dim obj_tipo_permiso As Label = CType(row.FindControl("lblTipo"), Label)

                Dim obj_id As Integer = CType(row.FindControl("hdn_id"), HiddenField).Value

                hdnId.Value = obj_id

                Call Accion()

                If hdnAccion.Value = 0 Then
                    row.BackColor = Drawing.Color.White
                    '  obj_num.ForeColor = Drawing.Color.Black
                    obj_rut.ForeColor = Drawing.Color.Black
                    obj_nombre.ForeColor = Drawing.Color.Black
                    obj_empresa.ForeColor = Drawing.Color.Black
                    '  obj_fec_sol.ForeColor = Drawing.Color.Black
                    obj_num_comprobante.ForeColor = Drawing.Color.Black
                    obj_fec_inicio.ForeColor = Drawing.Color.Black
                    obj_fec_termino.ForeColor = Drawing.Color.Black
                    obj_tipo_permiso.ForeColor = Drawing.Color.Black
                ElseIf hdnAccion.Value = 1 Then
                    row.BackColor = Drawing.Color.LightGreen
                    ' obj_num.ForeColor = Drawing.Color.Black
                    obj_rut.ForeColor = Drawing.Color.Black
                    obj_nombre.ForeColor = Drawing.Color.Black
                    obj_empresa.ForeColor = Drawing.Color.Black
                    ' obj_fec_sol.ForeColor = Drawing.Color.Black
                    obj_num_comprobante.ForeColor = Drawing.Color.Black
                    obj_fec_inicio.ForeColor = Drawing.Color.Black
                    obj_fec_termino.ForeColor = Drawing.Color.Black
                    obj_tipo_permiso.ForeColor = Drawing.Color.Black
                End If


            End If
        Catch ex As Exception
            MessageBoxError("grdCartas_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarEstado(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_comprobantes"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UEST"
            comando.Parameters.Add("@est_solicitud", SqlDbType.VarChar)
            comando.Parameters("@est_solicitud").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Parameters.Add("@user_edit", SqlDbType.NVarChar)
            comando.Parameters("@user_edit").Value = Session("id_usu_per")
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarEstado", Err, Page, Master)
        End Try
    End Sub

    Protected Sub CboActualizarEstado(sender As Object, e As EventArgs)
        Dim Cbo As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(Cbo.NamingContainer, GridViewRow)
        Dim objTxt As TextBox = CType(row.FindControl("txtObs"), TextBox)
        Call ActualizarEstado(CType(row.FindControl("cboEstado"), DropDownList).SelectedValue, (CType(row.FindControl("hdn_id"), HiddenField).Value))

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaComprobantes("")
                Else
                    Call CargarGrillaComprobantes(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroFechas("")
                Else
                    Call CargarGrillaFiltroFechas(txtBuscar.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresa("")
                Else
                    Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                End If
            End If
        End If
    End Sub

    Private Sub ActualizarObs(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_comprobantes"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UOBS"
            comando.Parameters.Add("@obs_detalle", SqlDbType.VarChar)
            comando.Parameters("@obs_detalle").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarObs", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtObsActualizar(sender As Object, e As EventArgs)
        Dim Txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(Txt.NamingContainer, GridViewRow)
        Call ActualizarObs(CType(row.FindControl("txtObs"), TextBox).Text, (CType(row.FindControl("hdn_id"), HiddenField).Value))

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaComprobantes("")
                Else
                    Call CargarGrillaComprobantes(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroFechas("")
                Else
                    Call CargarGrillaFiltroFechas(txtBuscar.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresa("")
                Else
                    Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                End If
            End If
        End If
    End Sub

    Protected Sub CboActualizarEstado_revisado(sender As Object, e As EventArgs)
        Dim Cbo As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(Cbo.NamingContainer, GridViewRow)
        Dim objTxt As TextBox = CType(row.FindControl("txtObs"), TextBox)
        Call ActualizarEstado(CType(row.FindControl("cboEstado"), DropDownList).SelectedValue, (CType(row.FindControl("hdn_id"), HiddenField).Value))

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaComprobantes_revisados("")
                Else
                    Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroFechas_revisados("")
                Else
                    Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresa_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        End If

    End Sub

    Protected Sub txtObsActualizar_revisado(sender As Object, e As EventArgs)
        Dim Txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(Txt.NamingContainer, GridViewRow)
        Call ActualizarObs(CType(row.FindControl("txtObs"), TextBox).Text, (CType(row.FindControl("hdn_id"), HiddenField).Value))

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaComprobantes_revisados("")
                Else
                    Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroFechas_revisados("")
                Else
                    Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresa_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        End If
    End Sub

    Protected Sub txtBuscar_TextChanged(sender As Object, e As EventArgs) Handles txtBuscar.TextChanged
        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaComprobantes("")
                Else
                    Call CargarGrillaComprobantes(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroFechas("")
                Else
                    Call CargarGrillaFiltroFechas(txtBuscar.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresa("")
                Else
                    Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                End If
            End If
        End If
    End Sub

    Private Sub DevolverMail()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_control_comprobantes 'SMS', '" & hdnUserAdd.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMail.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMail", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnEnviarRecordar_Click(sender As Object, e As EventArgs) Handles btnEnviarRecordar.Click
        Try
            If grdComprobantes.Rows.Count = 0 Then
                MessageBox("Enviar", "No existen datos para enviar", Page, Master, "I")
            Else
                Dim fechaActual As DateTime = DateTime.Now.Date

                For Each row As GridViewRow In grdComprobantes.Rows
                    Dim obj_rut As String = CType(row.FindControl("lblRut"), Label).Text
                    Dim obj_nombre As String = CType(row.FindControl("lblNombre"), Label).Text
                    Dim obj_empresa As String = CType(row.FindControl("lblEmpresa"), Label).Text


                    Dim obj_num_comprobante As String = CType(row.FindControl("lblNumComprobante"), Label).Text

                    Dim obj_user_add As Integer = CType(row.FindControl("hdnUserAdd"), HiddenField).Value
                    Dim obj_fec_add As String = CType(row.FindControl("hdnFedAdd"), HiddenField).Value
                    Dim obj_id As Integer = CType(row.FindControl("hdn_id"), HiddenField).Value

                    hdnId.Value = obj_id
                    hdnUserAdd.Value = obj_user_add
                    hdnRut.Value = obj_rut
                    hdnNombre.Value = obj_nombre
                    hdnEmpresa.Value = obj_empresa
                    hdnFecSol.Value = obj_fec_add
                    hdnNumComprobante.Value = obj_num_comprobante

                    Call DevolverMail()


                    Try
                        Dim correo As New MailMessage
                        correo.From = New MailAddress("sistemas@viatrack.cl")
                        correo.To.Add("" & hdnMail.Value & "")
                        correo.Subject = "[CV] Comprobantes Pendientes"
                        correo.Body = "Estimado (a), se informa que tiene los siguientes comprobantes de feriados pendientes correspondientes a: " & hdnNombre.Value & ", RUT: " & hdnRut.Value & ", Nº Comprobante: " & hdnNumComprobante.Value & ", solicitada en la fecha: " & hdnFecSol.Value & ""
                        correo.IsBodyHtml = False
                        correo.Priority = MailPriority.Normal
                        Dim smtp As New SmtpClient
                        smtp.Host = "mail.viatrack.cl"
                        smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
                        smtp.Send(correo)
                        correo.Attachments.Clear()
                        correo.Dispose()
                    Catch ex As Exception
                        MessageBoxError("btnEnviarRecordar_Click", Err, Page, Master)
                    End Try

                    Call ActualizarEstRecordad(1, hdnId.Value)

                Next

                MessageBox("Enviar", "Recordatorios enviados a validadores correspondientes", Page, Master, "S")

                If cboFiltroEmp.SelectedValue = 0 Then
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaComprobantes("")
                        Else
                            Call CargarGrillaComprobantes(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroFechas("")
                        Else
                            Call CargarGrillaFiltroFechas(txtBuscar.Text)
                        End If
                    End If
                Else
                    If dtHasta.Text = "" Then
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresa("")
                        Else
                            Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                        End If
                    Else
                        If txtBuscar.Text = "" Then
                            Call CargarGrillaFiltroEmpresaFechas("")
                        Else
                            Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBoxError("btnEnviarRecordar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnPendientes_Click(sender As Object, e As EventArgs) Handles btnPendientes.Click
        MtvSolicitudPersonal.ActiveViewIndex = 0

        lbltituloP.Text = "Comprobantes Pendientes"

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaComprobantes("")
                Else
                    Call CargarGrillaComprobantes(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroFechas("")
                Else
                    Call CargarGrillaFiltroFechas(txtBuscar.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresa("")
                Else
                    Call CargarGrillaFiltroEmpresa(txtBuscar.Text)
                End If
            Else
                If txtBuscar.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas(txtBuscar.Text)
                End If
            End If
        End If
    End Sub

    Protected Sub btnOtros_Click(sender As Object, e As EventArgs) Handles btnOtros.Click
        MtvSolicitudPersonal.ActiveViewIndex = 1

        lbltituloP.Text = "Comprobantes Revisados"

        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaComprobantes_revisados("")
                Else
                    Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroFechas_revisados("")
                Else
                    Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresa_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        End If
    End Sub

    Private Sub CargarGrillaComprobantes_revisados(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'G_R', '" & nombre & "'"
            grdOtros.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaComprobantes_revisados", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroEmpresa_revisados(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GE_R', '" & nombre & "', '" & cboFiltroEmp.SelectedValue & "'"
            grdOtros.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroEmpresa_revisados", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroFechas_revisados(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GF_R', '" & nombre & "', '" & CDate(dtDesde.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtHasta.Text).ToString("dd-MM-yyyy") & "'"
            grdOtros.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroFechas_revisados", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaFiltroEmpresaFechas_revisados(ByVal nombre As String)
        Try
            Dim strNomTabla As String = "REMMov_estado_comprobantes"
            Dim strSQL As String = "Exec pro_control_comprobantes 'GEF_R', '" & nombre & "', '" & cboFiltroEmp.SelectedValue & "','" & CDate(dtDesde.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtHasta.Text).ToString("dd-MM-yyyy") & "'"
            grdOtros.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdOtros.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaFiltroEmpresaFechas_revisados", Err, Page, Master)
        End Try
    End Sub


    Private Sub grdOtros_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdOtros.PageIndexChanging
        Try
            grdOtros.PageIndex = e.NewPageIndex

            If cboFiltroEmp.SelectedValue = 0 Then
                If dtHasta.Text = "" Then
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaComprobantes_revisados("")
                    Else
                        Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                    End If
                Else
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroFechas_revisados("")
                    Else
                        Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                    End If
                End If
            Else
                If dtHasta.Text = "" Then
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroEmpresa_revisados("")
                    Else
                        Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                    End If
                Else
                    If txtBuscarRevisado.Text = "" Then
                        Call CargarGrillaFiltroEmpresaFechas_revisados("")
                    Else
                        Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBoxError("grdOtros_PageIndexChanging", Err, Page, Master)
        End Try

    End Sub

    Protected Sub txtBuscarRevisado_TextChanged(sender As Object, e As EventArgs) Handles txtBuscarRevisado.TextChanged
        If cboFiltroEmp.SelectedValue = 0 Then
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaComprobantes_revisados("")
                Else
                    Call CargarGrillaComprobantes_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroFechas_revisados("")
                Else
                    Call CargarGrillaFiltroFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        Else
            If dtHasta.Text = "" Then
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresa_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresa_revisados(txtBuscarRevisado.Text)
                End If
            Else
                If txtBuscarRevisado.Text = "" Then
                    Call CargarGrillaFiltroEmpresaFechas_revisados("")
                Else
                    Call CargarGrillaFiltroEmpresaFechas_revisados(txtBuscarRevisado.Text)
                End If
            End If
        End If
    End Sub

    Private Sub grdOtros_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdOtros.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim objEstado As DropDownList = CType(row.FindControl("cboEstado"), DropDownList)
                Dim objIdEstado As HiddenField = CType(row.FindControl("hdnEstCom"), HiddenField)
                Call CargarComboEstado_revisados(objEstado)
                objEstado.SelectedValue = objIdEstado.Value
            End If
        Catch ex As Exception
            MessageBoxError("grdOtros_RowDataBound", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarEstRecordad(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_comprobantes"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "U_ER"
            comando.Parameters.Add("@est_recordado", SqlDbType.VarChar)
            comando.Parameters("@est_recordado").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarEstRecordad", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarEstReversado(ByVal Valor As String, ByVal ID As String, ByVal num_comprobante As String, ByVal rut_personal As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_control_comprobantes"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "RVA"
            comando.Parameters.Add("@est_reversado", SqlDbType.NVarChar)
            comando.Parameters("@est_reversado").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Parameters.Add("@user_edit", SqlDbType.NVarChar)
            comando.Parameters("@user_edit").Value = Session("id_usu_per")
            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
            comando.Parameters("@num_comprobante").Value = num_comprobante
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = rut_personal
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarEstReversado", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdOtros_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdOtros.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "3" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdOtros.Rows(intRow)

                Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                Dim hdnCodEmpresa_obj As HiddenField = CType(row.FindControl("hdnCodEmp"), HiddenField)
                Dim hdn_tipopermiso_obj As HiddenField = CType(row.FindControl("hdnTipoPer"), HiddenField)

                Dim numobj As HiddenField = CType(row.FindControl("hdnIdSolicitud"), HiddenField)

                Dim obj_rut As Label = CType(row.FindControl("lblRut"), Label)
                Dim obj_num_comprobante As Label = CType(row.FindControl("lblNumComprobante"), Label)

                hdnRutComprobante.Value = obj_rut.Text
                hdnNumComprobante.Value = obj_num_comprobante.Text

                Dim inicio As Label = CType(row.FindControl("lblFecIni"), Label)
                Dim termino As Label = CType(row.FindControl("lblFecTermino"), Label)

                hdnInicio.Value = inicio.Text
                hdnTermino.Value = termino.Text

                lblNumFormulario.Text = obj_num_comprobante.Text

                Call CargarComprobante(hdnCodEmpresa_obj.Value)

                btnImp.Visible = True
                btnImp.Enabled = True
                lblNumFormulario.Visible = True

            End If
        Catch ex As Exception
            MessageBoxError("grdOtros_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComprobante(ByVal empresa As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader

        comando.CommandText = "Exec pro_validar_feriado 'RCV', '" & hdnRutComprobante.Value & "', '" & hdnNumComprobante.Value & "'"
        comando.Connection = conx
        conx.Open()

        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            If rdoReader(14).ToString = 1 Or rdoReader(14).ToString = 1 Then
                lblTituloComprobante.Text = "COMPROBANTE DE VACACIONES"
            Else
                lblTituloComprobante.Text = "COMPROBANTE DE PERMISOS"
            End If

            lblncomprobante.Text = rdoReader(0).ToString
            lblrut.Text = rdoReader(1).ToString
            lblfecha.Text = Mid(rdoReader(2).ToString, 1, 10)
            lblfechai.Text = Mid(rdoReader(3).ToString, 1, 10)
            lblfechat.Text = Mid(rdoReader(4).ToString, 1, 10)
            lbldiashabiles.Text = rdoReader(5).ToString
            lblnota.Text = rdoReader(17).ToString
            lblnombre.Text = rdoReader(10).ToString

            If rdoReader(14).ToString = 4 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 9 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 11 Or rdoReader(14).ToString = 1 Then
                lbldiasprogresivosn.Text = "Dias progresivos:"
                lbldiasprogresivos.Text = rdoReader(13).ToString
                lbltituloperiodo.Text = "Correspondiente al período:"
                lblal.Text = "al:"
                lblperiodoi.Text = Mid(rdoReader(11).ToString, 1, 10)
                lblperiodot.Text = Mid(rdoReader(12).ToString, 1, 10)
            End If
        End If


        If empresa = 1 Then
            imglogo.ImageUrl = "../../../../imagen/logo.png"
        ElseIf empresa = 2 Then
            imglogo.ImageUrl = "../../../../imagen/logovmj.png"
        ElseIf empresa = 3 Then
            imglogo.ImageUrl = "../../../../imagen/logostartec.png"
        ElseIf empresa = 12 Then
            imglogo.ImageUrl = "../../../../imagen/logoesex.png"
        ElseIf empresa = 13 Then
            imglogo.ImageUrl = "../../../../imagen/logoditrans.png"
        ElseIf empresa = 14 Or empresa = 26 Or empresa = 27 Then
            imglogo.ImageUrl = ""
        ElseIf empresa = 22 Then
            imglogo.ImageUrl = "../../../../imagen/logologtec.png"
        End If

        rdoReader.Close()
        conx.Close()
    End Sub

    Protected Sub grdOtros_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdOtros.SelectedIndexChanged

    End Sub

    Protected Sub grdComprobantes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdComprobantes.SelectedIndexChanged

    End Sub
End Class