﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal


Public Class aspUsuariosVacaciones
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                lbltitulo.Text = "Usuarios Vacaciones"
                Call carga_combo_empresa()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarComboNombre(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        'Dim strSQL As String = "select cod_usuario, (select nom_personal + ' ' + ape_paterno + ' ' + ape_materno from RRHHMae_ficha_personal where rut_personal = U.rut_usuario) as nom_usuario " &
        ' "from TDIMae_usuarios U where (select nom_personal from RRHHMae_ficha_personal where rut_personal = U.rut_usuario) Is Not null And (select nom_personal + ' ' + ape_paterno + ' ' + ape_materno from RRHHMae_ficha_personal where rut_personal = U.rut_usuario) Like '%" & strBuscar & "%'  and (select cod_empresa from RRHHMae_ficha_contrato where rut_personal = U.rut_usuario) = '" & cboEmpresa.SelectedValue & "'"

        Dim strSQL As String = "select (select cod_usuario from mae_usuarios where id_usuario = U.id_usuario) as cod_usuario, (select nom_usuario from mae_usuarios where id_usuario = U.id_usuario) as nom_usuario" &
            " from TDIMae_usuarios U where (select nom_usuario from mae_usuarios where id_usuario = U.id_usuario) Like '%" & strBuscar & "%'"

        Dim strNomTabla As String = "mae_usuarios"
        Try
            cbobuscarnombre.DataTextField = "nom_usuario"
            cbobuscarnombre.DataValueField = "cod_usuario"
            cbobuscarnombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cbobuscarnombre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombre", Err, Page, Master)
        End Try
    End Sub



    Private Sub Chk_areasNoConductores()
        Try
            Dim strNomTablaR As String = "REMMae_area"
            Dim strSQLR As String = "Select id_area, nom_area from REMMae_area where cod_empresa = '" & cboEmpresa.SelectedValue & "'"
            chkAreas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkAreas.DataTextField = "nom_area"
            chkAreas.DataValueField = "id_area"
            chkAreas.DataBind()
        Catch ex As Exception
            MessageBoxError("Chk_areasNoConductores", Err, Page, Master)
        End Try
    End Sub

    Private Sub Chk_areasConductores()
        Try
            Dim strNomTablaR As String = "mae_faenas"
            Dim strSQLR As String = "select cod_faena, nom_faena from mae_faenas order by nom_faena"
            chkAreas.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            chkAreas.DataTextField = "nom_faena"
            chkAreas.DataValueField = "cod_faena"
            chkAreas.DataBind()
        Catch ex As Exception
            MessageBoxError("Chk_areasConductores", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Select cod_empresa=0, nom_empresa='Ninguna' UNION select cod_empresa, nom_empresa from REMMae_empresa where cod_empresa <> (5) order by cod_empresa"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call CargarGrilla("")
        If cboEmpresa.SelectedValue <> 1 Then
            rblTipoPersonal.ClearSelection()
            rblTipoPersonal.Visible = False
            Call Chk_areasNoConductores()
        Else
            rblTipoPersonal.Visible = True
            rblTipoPersonal.SelectedValue = 1
            Call Chk_areasConductores()
        End If
    End Sub

    Private Sub CargarGrilla(ByVal Nombre As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMae_usuarios_feriado_val"
            Dim strSQL As String = "Exec proc_usuarios_feriados 'G' , '" & Nombre & "', '" & cboEmpresa.SelectedValue & "'"
            grdEncargados.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdEncargados.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrilla", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnGrabarMov_Click(sender As Object, e As EventArgs) Handles btnGrabarMov.Click
        If cbobuscarnombre.SelectedValue = "" Or cbobuscarnombre.SelectedValue = 0 Or txtMail.Text = "" Then
            MessageBox("Guardar", "Ingrese todos los registro", Page, Master, "I")
        Else

            If chkAreas.Items.Count = 0 Then
                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                Dim cmdTemporal As SqlCommand
                Dim rdoReader As SqlDataReader
                Dim strSQL As String = "select cod_usuario From REMMae_usuarios_feriado_val where cod_usuario='" & cbobuscarnombre.SelectedValue & "' and cod_empresa = '" & cboEmpresa.SelectedValue & "'"
                Using cnxAcceso As New SqlConnection(strCnx)
                    cnxAcceso.Open()
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        MessageBox("Guardar", "Usuario ya asignado", Page, Master, "I")
                    Else
                        comando.CommandType = CommandType.StoredProcedure
                        comando.CommandText = "proc_usuarios_feriados"
                        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                        comando.Parameters("@tipo").Value = "I"
                        comando.Parameters.Add("@cod_usuario", SqlDbType.NVarChar)
                        comando.Parameters("@cod_usuario").Value = cbobuscarnombre.SelectedValue
                        comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                        comando.Parameters("@id_area").Value = 0
                        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
                        comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
                        comando.Parameters.Add("@mail", SqlDbType.NVarChar)
                        comando.Parameters("@mail").Value = txtMail.Text
                        comando.Parameters.Add("@cod_tipo", SqlDbType.NVarChar)
                        comando.Parameters("@cod_tipo").Value = 2
                        comando.Connection = conx
                        conx.Open()
                        comando.ExecuteNonQuery()

                        MessageBox("Guardar", "Registros Almacenados Correctamente", Page, Master, "S")
                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                    rdoReader = Nothing
                    cmdTemporal = Nothing
                End Using
            Else

                For intCheck = 0 To chkAreas.Items.Count - 1
                    Dim conx As New SqlConnection(strCnx)
                    Dim comando As New SqlCommand
                    Dim cmdTemporal As SqlCommand
                    Dim rdoReader As SqlDataReader
                    Dim strSQL As String = "select cod_usuario, id_area " &
                                     "From REMMae_usuarios_feriado_val where cod_usuario='" & cbobuscarnombre.SelectedValue & "' and id_area ='" &
                                     chkAreas.Items(intCheck).Value & "' and cod_empresa = '" & cboEmpresa.SelectedValue & "' and cod_tipo =  '" & rblTipoPersonal.SelectedValue & "'"

                    Using cnxAcceso As New SqlConnection(strCnx)
                        cnxAcceso.Open()
                        cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                        rdoReader = cmdTemporal.ExecuteReader()
                        If rdoReader.Read Then
                            If chkAreas.Items(intCheck).Selected = False Then
                                comando.CommandType = CommandType.StoredProcedure
                                comando.CommandText = "proc_usuarios_feriados"
                                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                                comando.Parameters("@tipo").Value = "E"
                                comando.Parameters.Add("@id", SqlDbType.NVarChar)
                                comando.Parameters("@id").Value = rdoReader(0).ToString
                                comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                                comando.Parameters("@id_area").Value = rdoReader(1).ToString
                                comando.Connection = conx
                                conx.Open()
                                comando.ExecuteNonQuery()
                            End If
                        Else
                            If cboEmpresa.SelectedValue = 1 Then
                                If chkAreas.Items(intCheck).Selected = True Then
                                    comando.CommandType = CommandType.StoredProcedure
                                    comando.CommandText = "proc_usuarios_feriados"
                                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                                    comando.Parameters("@tipo").Value = "I"
                                    comando.Parameters.Add("@cod_usuario", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_usuario").Value = cbobuscarnombre.SelectedValue
                                    comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                                    comando.Parameters("@id_area").Value = chkAreas.Items(intCheck).Value
                                    comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
                                    comando.Parameters.Add("@mail", SqlDbType.NVarChar)
                                    comando.Parameters("@mail").Value = txtMail.Text
                                    comando.Parameters.Add("@cod_tipo", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_tipo").Value = rblTipoPersonal.SelectedValue
                                    comando.Connection = conx
                                    conx.Open()
                                    comando.ExecuteNonQuery()
                                End If
                            Else
                                If chkAreas.Items(intCheck).Selected = True Then
                                    comando.CommandType = CommandType.StoredProcedure
                                    comando.CommandText = "proc_usuarios_feriados"
                                    comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                                    comando.Parameters("@tipo").Value = "I"
                                    comando.Parameters.Add("@cod_usuario", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_usuario").Value = cbobuscarnombre.SelectedValue
                                    comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                                    comando.Parameters("@id_area").Value = chkAreas.Items(intCheck).Value
                                    comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
                                    comando.Parameters.Add("@mail", SqlDbType.NVarChar)
                                    comando.Parameters("@mail").Value = txtMail.Text
                                    comando.Parameters.Add("@cod_tipo", SqlDbType.NVarChar)
                                    comando.Parameters("@cod_tipo").Value = 2
                                    comando.Connection = conx
                                    conx.Open()
                                    comando.ExecuteNonQuery()
                                End If
                            End If

                        End If
                        rdoReader.Close()
                        cmdTemporal.Dispose()
                        rdoReader = Nothing
                        cmdTemporal = Nothing
                    End Using
                Next
                MessageBox("Guardar", "Registros Almacenados Correctamente", Page, Master, "S")
            End If
            cbobuscarnombre.ClearSelection()
            cbobuscarnombre.Visible = False
            txtnombre.Text = ""
            txtMail.Text = ""
            txtnombre.Visible = True
            chkAreas.ClearSelection()
            Call CargarGrilla("")
        End If
    End Sub

    Private Sub RescatarItemsGrilla()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            For intCheck = 0 To chkAreas.Items.Count - 1
                Dim cmdTemporal As SqlCommand
                Dim rdoReader As SqlDataReader
                Dim strSQL As String = "select id_area " &
                                    "From REMMae_usuarios_feriado_val where cod_usuario='" & hdnCodUsu.Value & "' and id_area ='" &
                chkAreas.Items(intCheck).Value & "' and cod_empresa = '" & cboEmpresa.SelectedValue & "' and cod_tipo = '" & hdnTipo.Value & "'"
                Using cnxAcceso As New SqlConnection(strCnx)
                    cnxAcceso.Open()
                    cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        If chkAreas.Items(intCheck).Value = rdoReader(0).ToString Then
                            chkAreas.Items(intCheck).Selected = True
                        Else
                            chkAreas.Items(intCheck).Selected = False
                        End If
                    Else
                        chkAreas.Items(intCheck).Selected = False
                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                    rdoReader = Nothing
                    cmdTemporal = Nothing
                End Using
            Next
        Catch ex As Exception
            MessageBoxError("RescatarItemsGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarMail()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSQL As String = "select mail From REMMae_usuarios_feriado_val where cod_usuario='" & hdnCodUsu.Value & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    txtMail.Text = rdoReader(0).ToString
                Else
                    txtMail.Text = ""
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using
        Catch ex As Exception
            MessageBoxError("RescatarItemsGrilla", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdEncargados_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdEncargados.PageIndexChanging
        Try
            grdEncargados.PageIndex = e.NewPageIndex
            If txtbNombre.Text = "" Then
                Call CargarGrilla("")
            Else
                Call CargarGrilla(txtbNombre.Text)
            End If
        Catch ex As Exception
            MessageBoxError("grdEncargados_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdEncargados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdEncargados.SelectedIndexChanged
        Try
            Dim row As GridViewRow = grdEncargados.SelectedRow
            Dim hdnid As HiddenField = CType(row.FindControl("hdn_codusuario"), HiddenField)
            Dim hdn_Tipo As HiddenField = CType(row.FindControl("hdn_tipo"), HiddenField)
            Dim nombre_obj As Label = CType(row.FindControl("lblNombre"), Label)

            hdnCodUsu.Value = hdnid.Value
            hdnTipo.Value = hdn_Tipo.Value

            If hdnTipo.Value = 1 Then
                rblTipoPersonal.SelectedValue = 1
                Call Chk_areasConductores()
            Else
                rblTipoPersonal.SelectedValue = 2
                Call Chk_areasNoConductores()
            End If

            Call RescatarMail()
            Call RescatarItemsGrilla()
            cbobuscarnombre.Visible = True
            txtnombre.Visible = False
            Call CargarComboNombre(Trim(nombre_obj.Text))

        Catch ex As Exception
            MessageBoxError("grdEncargados_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub Limpiar()
        cbobuscarnombre.ClearSelection()
        cbobuscarnombre.Visible = False
        Call carga_combo_empresa()
        txtbNombre.Text = ""
        txtnombre.Visible = True
        rblTipoPersonal.ClearSelection()
        txtMail.Text = ""
        chkAreas.ClearSelection()
        Call CargarGrilla("")
    End Sub

    Protected Sub btnLimpiarMov_Click(sender As Object, e As EventArgs) Handles btnLimpiarMov.Click
        Call Limpiar()
    End Sub

    Protected Sub rblTipoPersonal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblTipoPersonal.SelectedIndexChanged
        chkAreas.Visible = True
        If rblTipoPersonal.SelectedValue = 1 Then
            If cboEmpresa.SelectedValue = 1 Then
                Call Chk_areasConductores()
            Else
                Call Chk_areasNoConductores()
            End If
        Else
            Call Chk_areasNoConductores()
        End If
    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        If Trim(txtbNombre.Text) = "" Then
            Call CargarGrilla("")
        Else
            Call CargarGrilla(txtbNombre.Text)
        End If
    End Sub

    Protected Sub txtnombre_TextChanged(sender As Object, e As EventArgs) Handles txtnombre.TextChanged

    End Sub

    Protected Sub cbobuscarnombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbobuscarnombre.SelectedIndexChanged

    End Sub

    Private Sub bntBuscarNombre_Click(sender As Object, e As EventArgs) Handles bntBuscarNombre.Click
        Dim blnVisible As Boolean = cbobuscarnombre.Visible, intRegistros As Integer = 0
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtnombre.Visible = blnVisible
            Call CargarComboNombre(Trim(txtnombre.Text), intRegistros)
            If intRegistros = 2 And cbobuscarnombre.Visible = True Then
                cbobuscarnombre.SelectedIndex = 1
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
            End If
            cbobuscarnombre.ClearSelection()
        Catch ex As Exception
            MessageBoxError("bntBuscarNombre_Click", Err, Page, Master)
        End Try
    End Sub
End Class