﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspControlComprobantes.aspx.vb" Inherits="aspSistemasJorquera.aspControlComprobantes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script lang="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();       
          
           
        }
    </script>


    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnPendientes" runat="server">Comprobantes Pendientes</asp:LinkButton></li>
        <li>
            <asp:LinkButton ID="btnOtros" runat="server">Comprobantes Revisados</asp:LinkButton></li>

    </ul>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboFiltroEmp" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>

       
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Desde</label>
                <div>
                    <asp:TextBox ID="dtDesde" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Hasta</label>
                <div>
                    <asp:TextBox ID="dtHasta" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                </div>
            </div>
        </div>
     

    <asp:MultiView ID="MtvSolicitudPersonal" runat="server">
        <asp:View ID="ViewMovimiento" runat="server">
            <!-- Main content -->
          
                <asp:Panel ID="pnlSeguimiento" runat="server" Visible="true">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Personal</label>
                            <div>
                                <asp:TextBox ID="txtBuscar" placeholder="Buscar Nombre..." runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>                     
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-12">
                            <asp:GridView ID="grdComprobantes" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="25" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' />
                                            <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_estado_com")%>' />
                                            <asp:HiddenField ID="hdnEstCom" runat="server" Value='<%# Bind("est_comprobante")%>' />
                                            <asp:HiddenField ID="hdnUserAdd" runat="server" Value='<%# Bind("user_add")%>' />
                                            <asp:HiddenField ID="hdnFedAdd" runat="server" Value='<%# Bind("fec_add")%>' />
                                            <asp:HiddenField ID="hdnIdSolicitud" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre_personal")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Empresa">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="180px" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Nº Comprobante">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNumComprobante" runat="server" Text='<%# Bind("num_comprobante")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Inicio">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecIni" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Termino">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecTermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tipo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tipo_solicitud")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Generador">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGenerador" runat="server" Text='<%# Bind("nom_user")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cboEstado" runat="server" DataTextField="nom_estado" CssClass="Border border rounded border-info text-info"
                                                DataValueField="id_estado" Height="20px" Width="130px" AutoPostBack="true" OnTextChanged="CboActualizarEstado" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="140px" Wrap="False" CssClass="EstFilasGrilla"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OBS Estado">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObs" runat="server" Width="150px" Text='<%# Bind("obs_estado")%>' AutoPostBack="true" OnTextChanged="txtObsActualizar" CssClass="Border border rounded border-info text-info"> </asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="160px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnTraspasar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="1" CssClass="btn btn-success btn-sm" ToolTip="Enviar Mail Recordatorio a Validador"><i class="fa fa-fw fa-envelope"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="40px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnReversar" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="2" CssClass="btn btn-danger btn-sm" OnClientClick="return confirm('¿Esta seguro que desea REVERSAR este permiso?');" ToolTip="Reversar"><i class="fa fa-fw fa-history"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="40px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                            <div>
                                <asp:LinkButton ID="btnEnviarRecordar" runat="server" CssClass="btn bg-info">Enviar Recordatorio</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

         
        </asp:View>

        <asp:View ID="ViewHistorial" runat="server">
         
                <asp:Panel ID="Panel2" runat="server" Visible="true">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Personal</label>
                            <div>
                                <asp:TextBox ID="txtBuscarRevisado" runat="server" placeholder="Buscar Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>

                     
                        <div class="col-md-2 form-group">
                            <label for="fname" class="control-label col-form-label">Nº Comprobante:</label>
                            <div>
                                <asp:TextBox ID="lblNumFormulario" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Enabled="false"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-1 form-group">
                            <label for="fname" class="control-label col-form-label">&nbsp;</label>
                            <div>
                                <asp:LinkButton ID="btnImp" CssClass="btn btn-warning btn-block" runat="server" OnClientClick="javascript:CallPrint('Salida');"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                            </div>
                        </div>

                    </div>


                    <div class="row justify-content-center table-responsive">

                        <div class="col-md-12">

                            <asp:GridView ID="grdOtros" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Rut">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' />
                                            <asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_estado_com")%>' />
                                            <asp:HiddenField ID="hdnEstCom" runat="server" Value='<%# Bind("est_comprobante")%>' />
                                            <asp:HiddenField ID="hdnUserAdd" runat="server" Value='<%# Bind("user_add")%>' />
                                            <asp:HiddenField ID="hdnFedAdd" runat="server" Value='<%# Bind("fec_add")%>' />
                                            <asp:HiddenField ID="hdnIdSolicitud" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                            <asp:HiddenField ID="hdnCodEmp" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                            <asp:HiddenField ID="hdnTipoPer" runat="server" Value='<%# Bind("id_tipo_permiso")%>' />

                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nombre">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombre_personal")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Empresa">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("nom_empresa")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="180px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fec. Solicitud">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecSol" runat="server" Text='<%# Bind("fec_add", "{0:d}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="90px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Nº Comprobante">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNumComprobante" runat="server" Text='<%# Bind("num_comprobante")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="90px" />
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Inicio">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecIni" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Termino">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFecTermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tipo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("tipo_solicitud")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Generador">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGenerador" runat="server" Text='<%# Bind("nom_user")%>' />
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Estado">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cboEstado" runat="server" DataTextField="nom_estado" CssClass="Border border rounded border-info text-info"
                                                DataValueField="id_estado" Height="20px" Width="130px" AutoPostBack="true" OnTextChanged="CboActualizarEstado_revisado" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" Width="140px" Wrap="False" CssClass="EstFilasGrilla"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OBS Estado">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObs" runat="server" Width="150px" Text='<%# Bind("obs_estado")%>' AutoPostBack="true" CssClass="Border border rounded border-info text-info" OnTextChanged="txtObsActualizar_revisado"> </asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="160px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnImprimir" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CommandName="3" CssClass="btn btn-warning btn-sm" ToolTip="Imprimir"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                            </asp:GridView>

                        </div>

                    </div>
                    </asp:Panel>
          
        </asp:View>

    </asp:MultiView>
    <asp:HiddenField ID="hdnId" runat="server" />
    <asp:HiddenField ID="hdnRut" runat="server" />
    <asp:HiddenField ID="hdnNombre" runat="server" />
    <asp:HiddenField ID="hdnEmpresa" runat="server" />
    <asp:HiddenField ID="hdnFecSol" runat="server" />
    <asp:HiddenField ID="hdnNumComprobante" runat="server" />
    <asp:HiddenField ID="hdnUserAdd" runat="server" />
    <asp:HiddenField ID="hdnMail" runat="server" />
    <asp:HiddenField ID="hdnAccion" runat="server" />
    <asp:HiddenField ID="hdnRutComprobante" runat="server" Value="0" />
    <asp:HiddenField ID="hdnInicio" runat="server" />
    <asp:HiddenField ID="hdnTermino" runat="server" />

     </section>
    <asp:Panel ID="pnlComprobante" runat="server" Style="display: none" CssClass="EstPanel" Width="750px" Height="600px" Visible="True">
        <div id="Salida">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td rowspan="2" width="33%">
                                    <asp:ImageButton ID="imglogo" runat="server" ImageUrl="" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="33%" style="text-align: center;"><b><font style="font-size: 16px; font-family: Arial;"><asp:label runat="server" ID="lblTituloComprobante"></asp:label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 20px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 20px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Fecha</font></b></td>
                                <td style="padding: 20px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfecha" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 5px 0px 5px 4px"><b><font style="font-size: 11px; font-family: Arial;">N° Comprobante</font></b></td>
                                <td style="padding: 5px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblncomprobante" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial;">En cumplimiento de las disposiciones legales vigentes se deja constancia que don (ña):</font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px; width: 180px; height: 10px;"><b><font style="font-size: 11px; font-family: Arial;">Nombre del trabajador:</font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 400px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnombre" Text=""></asp:Label></font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial;">RUT:</font></b></td>
                                <td style="padding: 5px 20px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblrut" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Hará uso de su feriado desde el:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechai" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Hasta el:</font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechat" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Días Hábiles:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiashabiles" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbldiasprogresivosn" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiasprogresivos" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbltituloperiodo" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodoi" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lblal" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodot" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 40px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnota" Visible="true"></asp:Label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 40px 00px 0px 50px; text-align: center;">
                                    <hr />
                                </td>
                                <td width="70%"></td>
                                <td style="padding: 40px 50px 0px 0px; text-align: center;">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 00px 30px 50px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">EMPLEADOR</font></b></td>
                                <td width="70%" style="text-align: center;"></td>
                                <td style="padding: 0px 50px 30px 0px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">TRABAJADOR</font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
