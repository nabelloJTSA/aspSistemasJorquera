﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspValFeriado.aspx.vb" Inherits="aspSistemasJorquera.aspValFeriado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>

    <script lang="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
        }
    </script>
    <ul class="nav nav-tabs bg-info ">
        <li>
            <asp:LinkButton ID="btnValidar" runat="server">Solicitudes Actuales</asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="btnHis" runat="server">Historial</asp:LinkButton>
        </li>
    </ul>

    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text=""></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">

            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <%--<div class="col-md-2 form-group">--%>
                <%--<label for="fname" class="control-label col-form-label">Area</label>--%>
              <%--  <div>--%>
                    <asp:DropDownList ID="cboAreas" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible ="false"></asp:DropDownList>
                <%--</div>--%>
            <%--</div>--%>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Tipo</label>
                <div>

                    <asp:DropDownList ID="cboTipo" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="btnBuscarTodo" runat="server" CssClass="btn"><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">Mostrar Todo</label>
                <div>
                    <asp:CheckBox ID="chkTodos" runat="server" AutoPostBack="True" />
                </div>
            </div>

        </div>


        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="vwProcesar" runat="server">
                <asp:Panel ID="pnlBuscar" runat="server">
                    <div class="row justify-content-center table-responsive">
                        <div class="col-md-3 form-group">
                            <label for="fname" class="control-label col-form-label">Personal</label>
                            <div>
                                <asp:TextBox ID="txtbNombre" placeholder="Buscar Nombre..." runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="grdCrearMov" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="25" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="Rut">
                                <ItemTemplate>
                                    <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' /><asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                    <asp:HiddenField ID="hdn_detalles" runat="server" Value='<%# Bind("obs_detalle")%>' />
                                    <asp:HiddenField ID="hdn_tipopermiso" runat="server" Value='<%# Bind("id_tipo_permiso")%>' />
                                    <asp:HiddenField ID="hdn_cargo" runat="server" Value='<%# Bind("id_cargo")%>' />
                                    <asp:HiddenField ID="hdn_area" runat="server" Value='<%# Bind("id_area")%>' />
                                    <asp:HiddenField ID="hdn_empresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                    <asp:HiddenField ID="hdnNom_empresa" runat="server" Value='<%# Bind("nom_empresa")%>' />
                                    <asp:HiddenField ID="hdn_IdPeriodo" runat="server" Value='<%# Bind("id_periodo")%>' />
                                    <asp:HiddenField ID="hdn_CodSucursal" runat="server" Value='<%# Bind("cod_sucursal")%>' />
                                    <asp:HiddenField ID="hdnFecingresoEmp" runat="server" Value='<%# Bind("fec_ingreso_emp")%>' />
                                    <asp:HiddenField ID="hdnAnioCorresponde" runat="server" Value='<%# Bind("anio_corresponde")%>' />
                                    <asp:HiddenField ID="hdnNomDiaEspecial" runat="server" Value='<%# Bind("nom_dia_especial")%>' />
                                    <asp:HiddenField ID="hdnEstMedioDia" runat="server" Value='<%# Bind("est_medio_dia")%>' />
                                    <asp:HiddenField ID="hdnIdDiaEsp" runat="server" Value='<%# Bind("id_dia_esp")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Nombre">
                                <ItemTemplate>
                                    <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombrepersonal")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Area">
                                <ItemTemplate>
                                    <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nombrearea")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tipo">
                                <ItemTemplate>
                                    <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("permiso")%>' />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inicio">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <%--<asp:TextBox ID="dtFechaEntrega" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' Width="80px" CssClass="Border border rounded border-info text-info" AutoPostBack="true" OnTextChanged="txtInicioActualizar" />
                                                                     <ajaxToolkit:CalendarExtender ID="dtFechaEntrega_CalendarExtender" runat="server" BehaviorID="dtFechaEntrega_CalendarExtender" TargetControlID="dtFechaEntrega" />--%>
                                                <asp:Label ID="lblFecIni" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                                            </td>

                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="110px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Termino">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <%-- <asp:TextBox ID="dtFechaDevolucion" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' Width="80px" CssClass="Border border rounded border-info text-info" AutoPostBack="true" OnTextChanged="txtTerminoActualizar" />
                                                                     <ajaxToolkit:CalendarExtender ID="dtFechaDevolucion_CalendarExtender" runat="server" BehaviorID="dtFechaDevolucion_CalendarExtender" TargetControlID="dtFechaDevolucion" />--%>
                                                <asp:Label ID="lblFecTer" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="110px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Observación">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOBS" runat="server" Width="150px" Text='<%# Bind("obs_detalle")%>' CssClass="Border border rounded border-info text-info" AutoPostBack="true" OnTextChanged="txtObsActualizar"></asp:TextBox>
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" Width="150px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Prestamo">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrestamo" runat="server" />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="90px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="1/2 Dia">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkMedioDia" runat="server" Enabled="false" />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="90px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnTraspasar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' OnClientClick="return confirm('¿Esta seguro que desea validar este permiso?');" ToolTip="Aprobar" CommandName="1" ImageUrl="~/imagen/imgEnabled.png" />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="30px" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnRechazar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Rechazar" OnClientClick="return confirm('¿Esta seguro que desea RECHAZAR este permiso?');" CommandName="2" ImageUrl="~/imagen/imgDisabled.png" />
                                </ItemTemplate>
                                <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Wrap="False" Width="30px" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                    </asp:GridView>
                </asp:Panel>
            </asp:View>
            <asp:View ID="vwHistorial" runat="server">

                <div class="row justify-content-center table-responsive">

                    <div class="col-md-3 form-group">
                        <label for="fname" class="control-label col-form-label">Personal</label>
                        <div>
                            <asp:TextBox ID="txtbNombreHis" placeholder="Buscar Nombre..." runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2 form-group">
                        <label for="fname" id="lbltitulo" class="control-label col-form-label">Nº Solicitud</label>
                        <div>
                            <asp:TextBox ID="lblNumFormulario" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnImpFormulario" CssClass="btn btn-warning btn-block" runat="server" OnClientClick="javascript:CallPrint('Salida');"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-1 form-group">
                        <label for="fname" class="control-label col-form-label">&nbsp;</label>
                        <div>
                            <asp:LinkButton ID="btnImpAnticipo" CssClass="btn btn-warning btn-block" runat="server" OnClientClick="javascript:CallPrint('Anticipos');" Visible="False"><i class="fa fa-fw fa-print"></i></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <asp:GridView ID="grdHistorial" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="25" AllowPaging="True">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="btnSeleccionar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="Volver" CommandName="3" ImageUrl="~/imagen/seleccionar.png" />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="N°">
                            <ItemTemplate>
                                <asp:Label ID="lblNumFor" runat="server" Text='<%# Bind("id_solicitud")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="40px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rut">
                            <ItemTemplate>
                                <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' /><asp:HiddenField ID="hdn_id" runat="server" Value='<%# Bind("id_solicitud")%>' />
                                <asp:HiddenField ID="hdn_detalles" runat="server" Value='<%# Bind("obs_detalle")%>' />
                                <asp:HiddenField ID="hdn_tipopermiso" runat="server" Value='<%# Bind("id_tipo_permiso")%>' />
                                <asp:HiddenField ID="hdn_cargo" runat="server" Value='<%# Bind("id_cargo")%>' />
                                <asp:HiddenField ID="hdn_area" runat="server" Value='<%# Bind("id_area")%>' />
                                <asp:HiddenField ID="hdn_empresa" runat="server" Value='<%# Bind("cod_empresa")%>' />
                                <asp:HiddenField ID="hdnNumComprobante" runat="server" Value='<%# Bind("num_comprobante")%>' />
                                <asp:HiddenField ID="hdnEstEvaluado" runat="server" Value='<%# Bind("est_evaluado")%>' />
                                <asp:HiddenField ID="hdnIdDiaEsp" runat="server" Value='<%# Bind("id_dia_esp")%>' />

                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="80px" HorizontalAlign="center" Height="25px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nombrepersonal")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="300px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Area">
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%# Bind("nombrearea")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("permiso")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inicio">
                            <ItemTemplate>
                                <asp:Label ID="lblInicio" runat="server" Text='<%# Bind("fec_inicio", "{0:d}")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Termino">
                            <ItemTemplate>
                                <asp:Label ID="lblTermino" runat="server" Text='<%# Bind("fec_termino", "{0:d}")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Observacion">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnObs" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" CssClass="btn btn-light btn-sm far fa-comment-dots" Enabled="false" ToolTip='<%#Bind("obs_detalle")%>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="50px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Estado">
                            <ItemTemplate>
                                <asp:Label ID="lblestado1" runat="server" Text='<%# Bind("est_generado")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="center" Width="80px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </asp:View>

        </asp:MultiView>
    </section>

    <asp:HiddenField ID="hdnCodigo" runat="server" />
    <asp:HiddenField ID="hdnId" runat="server" />
    <asp:HiddenField ID="hdnRut" runat="server" />
    <asp:HiddenField ID="hdnFecIni" runat="server" />
    <asp:HiddenField ID="hdnFecTer" runat="server" />
    <asp:HiddenField ID="hdnObs" runat="server" />
    <asp:HiddenField ID="hdnNombre" runat="server" />
    <asp:HiddenField ID="hdnEmpresa" runat="server" />
    <asp:HiddenField ID="hdnTipoPer" runat="server" />
    <asp:HiddenField ID="hdnRutDet" runat="server" />
    <asp:HiddenField ID="hdnIdSolicitud" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMail" runat="server" />
    <asp:HiddenField ID="hdnTipoPersona" runat="server" />
    <asp:HiddenField ID="hdnMailSolicitante" runat="server" />
    <asp:HiddenField ID="hdnInicio" runat="server" />
    <asp:HiddenField ID="hdnTipoPermiso" runat="server" />
    <asp:HiddenField ID="hdnTermino" runat="server" />
    <asp:HiddenField ID="hdn_nom_empresa" runat="server" />
    <asp:HiddenField ID="hdnDiasFeriados" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRutComprobante" runat="server" Value="0" />
    <asp:HiddenField ID="hdnNumComprobante" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdPeriodo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPrestamo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSaldoDiasPermiso" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdAnticipo" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCodSucursal" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCantTipos" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDiaIngreso" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMesIngreso" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAdelanto" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIdAdelanto" runat="server" Value="0" />

    <asp:Panel ID="pnlComprobante" runat="server" Style="display: none" CssClass="EstPanel" Width="750px" Height="600px" Visible="True">
        <div id="Salida">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td rowspan="2" width="33%">
                                    <asp:ImageButton ID="imglogo" runat="server" ImageUrl="" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="33%" style="text-align: center;"><b><font style="font-size: 16px; font-family: Arial;"><asp:label runat="server" ID="lblTituloComprobante"></asp:label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 20px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 20px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Fecha</font></b></td>
                                <td style="padding: 20px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfecha" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;" colspan="2"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td style="padding: 5px 0px 5px 4px"><b><font style="font-size: 11px; font-family: Arial;">N° Comprobante</font></b></td>
                                <td style="padding: 5px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblncomprobante" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial;">En cumplimiento de las disposiciones legales vigentes se deja constancia que don (ña):</font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px; width: 180px; height: 10px;"><b><font style="font-size: 11px; font-family: Arial;">Nombre del trabajador:</font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 400px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnombre" Text=""></asp:Label></font></b></td>
                                <td style="padding: 5px 0px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial;">RUT:</font></b></td>
                                <td style="padding: 5px 20px 5px 4px; width: 120px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblrut" Text=""></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Hará uso de su feriado desde el:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechai" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Hasta el:</font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblfechat" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Días Hábiles:</font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiashabiles" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbldiasprogresivosn" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lbldiasprogresivos" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr>
                                            <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lbltituloperiodo" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodoi" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;"><asp:Label runat="server" id="lblal" Text=""></asp:Label></font></b></td>
                                            <td style="padding: 5px 20px 5px 4px; width: 180px"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblperiodot" Text=""></asp:Label></font></b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 5px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px 40px 20px;" colspan="4"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblnota" Visible="true"></asp:Label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 40px 00px 0px 50px; text-align: center;">
                                    <hr />
                                </td>
                                <td width="70%"></td>
                                <td style="padding: 40px 50px 0px 0px; text-align: center;">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 00px 30px 50px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">EMPLEADOR</font></b></td>
                                <td width="70%" style="text-align: center;"></td>
                                <td style="padding: 0px 50px 30px 0px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">TRABAJADOR</font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="Panel1" runat="server" Style="display: none" CssClass="EstPanel" Width="750px" Height="600px" Visible="True">
        <div id="Anticipos">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td rowspan="2" width="33%">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td width="33%" style="text-align: center;"><b><font style="font-size: 16px; font-family: Arial;"><asp:label runat="server" ID="Label2" Text="ANTICIPO"></asp:label></font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 20px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">
                                                <asp:ImageButton ID="imglogoAnticipo" runat="server" ImageUrl="" />
                                                </font></b></td>
                                <td align="right" style="padding: 20px 0px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial;">Nº:</font></b></td>
                                <td style="padding: 20px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblNumAnt"></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;"></font></b></td>
                                <td align="right" style="padding: 5px 0px 5px 4px"><b><font style="font-size: 11px; font-family: Arial;">FECHA:</font></b></td>
                                <td style="padding: 5px 20px 5px 4px;"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label runat="server" id="lblFecAnt"></asp:Label></font></b></td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;">&nbsp;</td>
                                <td align="right" style="padding: 5px 0px 5px 4px">&nbsp;</td>
                                <td style="padding: 5px 20px 5px 4px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 0px 5px 20px;" class="auto-style1" colspan="3"><b><font style="font-size: 11px; font-family: Arial;">Yo, <font style="font-size:11px; font-family:Arial; font-weight:bold;">
                                                <asp:Label ID="lblNomTrabAnt" runat="server"></asp:Label>
                                                </font> , RUT:</font><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><asp:Label ID="lblRutTrabAnt" runat="server"></asp:Label>
                                                <font style="font-size:11px; font-family:Arial;">, autorizo se descuente de mis futuras liquidaciones el siguiente </font>
                                                </font></b></td>
                            </tr>
                            <tr>
                                <td class="auto-style1" style="padding: 5px 0px 5px 20px;" colspan="3"><b><font style="font-size: 11px; font-family: Arial; font-weight: bold;"><font style="font-size:11px; font-family:Arial;"> monto: </font>
                                                <asp:Label ID="lblMontoAnt" runat="server"></asp:Label>
                                                </font></b></td>
                            </tr>

                            <tr>
                                <td class="auto-style1" colspan="3" style="padding: 5px 0px 5px 20px;"><b><font style="font-size: 11px; font-family: Arial;">Por Concepto de: <font style="font-size:11px; font-family:Arial; font-weight:bold;">
                                                <asp:Label ID="lblConceptoAnt" runat="server"></asp:Label>
                                                </font> </font></b></td>
                            </tr>

                            <tr>
                                <td class="auto-style1" colspan="3" style="padding: 5px 0px 5px 20px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style1" colspan="3" style="padding: 5px 0px 5px 20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #000; padding: 0; margin: auto; height: 25px;" cellpadding="0px" cellspacing="0px">
                            <tr>
                                <td style="padding: 40px 00px 0px 50px; text-align: center;">
                                    <hr />
                                </td>
                                <td width="10%"></td>
                                <td style="padding: 40px 50px 0px 0px; text-align: center;">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 00px 30px 50px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">Firma Trabajador</font></b></td>
                                <td width="10%" style="text-align: center;"></td>
                                <td style="padding: 0px 50px 30px 0px; text-align: center;"><b><font style="font-size: 10px; font-family: Arial;">VºBº Autorización</font></b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>




    <%--   </ContentTemplate>

        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grdHistorial" EventName="RowCommand" />
        </Triggers>


    </asp:UpdatePanel>--%>
</asp:Content>
