﻿
Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspSaldos
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                Call carga_empresas()
                Call cargar_areas()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub
    Private Sub carga_empresas()
        Dim strNomTabla As String = "REMMae_empresa"
        Dim strSQL As String = "Exec rpt_saldos_permisos 'CCE'"
        cboEmpresa.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        cboEmpresa.DataTextField = "nom_empresa"
        cboEmpresa.DataValueField = "cod_empresa"
        cboEmpresa.DataBind()
    End Sub

    Private Sub cargar_areas()
        Dim strNomTabla As String = "REMMae_empresa"
        Dim strSQL As String = "Exec rpt_saldos_permisos 'CCA', " & cboEmpresa.SelectedValue & ""
        cboAreaFaena.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
        cboAreaFaena.DataTextField = "nom_area"
        cboAreaFaena.DataValueField = "id_area"
        cboAreaFaena.DataBind()
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged
        Call cargar_areas()
    End Sub

    Private Sub CargarGrillaSaldos(ByVal nombre As String, ByVal rut As String)
        Try
            Dim strNomTabla As String = "REMMov_movimiento_personal"
            Dim strSQL As String = "Exec rpt_saldos_permisos 'CGS', '" & nombre & "', '" & rut & "', '" & cboAreaFaena.SelectedValue & "'"
            grdSaldos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdSaldos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaSaldos", Err, Page, Master)
        End Try
    End Sub

    Private Sub Grillas()
        If Trim(txtbNombre.Text) = "" Then
            If Trim(txtRut.Text) = "" Then
                Call CargarGrillaSaldos("", "")
            Else
                Call CargarGrillaSaldos("", Trim(txtRut.Text))
            End If
        Else
            If Trim(txtRut.Text) = "" Then
                Call CargarGrillaSaldos(Trim(txtbNombre.Text), "")
            Else
                Call CargarGrillaSaldos(Trim(txtbNombre.Text), Trim(txtRut.Text))
            End If
        End If

        If grdSaldos.Rows.Count > 0 Then
            pnlSaldos.Visible = True
        Else
            pnlSaldos.Visible = False
        End If
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Call Grillas()
    End Sub

    Protected Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        Try
            Dim grdResumen As New DataGrid
            grdResumen.CellPadding = 0
            grdResumen.CellSpacing = 0
            grdResumen.BorderStyle = BorderStyle.None
            grdResumen.Font.Size = 10

            Dim strSQL As String

            If Trim(txtbNombre.Text) = "" Then
                If Trim(txtRut.Text) = "" Then
                    strSQL = "Exec rpt_saldos_permisos 'GEX', '',  '', '" & cboAreaFaena.SelectedValue & "'"
                Else
                    strSQL = "Exec rpt_saldos_permisos 'GEX', '',  '" & Trim(txtRut.Text) & "', '" & cboAreaFaena.SelectedValue & "'"
                End If
            Else
                If Trim(txtRut.Text) = "" Then
                    strSQL = "Exec rpt_saldos_permisos 'GEX', '" & Trim(txtbNombre.Text) & "',  '', '" & cboAreaFaena.SelectedValue & "'"
                Else
                    strSQL = "Exec rpt_saldos_permisos 'GEX', '" & Trim(txtbNombre.Text) & "',  '" & Trim(txtRut.Text) & "', '" & cboAreaFaena.SelectedValue & "'"
                End If
            End If

            Dim strNomTabla As String = "REMMov_movimiento_personal"
            grdResumen.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdResumen.DataBind()

            Me.EnableViewState = False
            Dim tw As New System.IO.StringWriter
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            grdResumen.RenderControl(hw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"

            If cboAreaFaena.SelectedValue = 0 Then
                Response.AddHeader("Content-Disposition", "attachment;filename=Saldos_" & cboEmpresa.SelectedItem.ToString & ".xls")
            Else
                Response.AddHeader("Content-Disposition", "attachment;filename=Saldos_" & cboEmpresa.SelectedItem.ToString & "_" & cboAreaFaena.SelectedItem.ToString & ".xls")
            End If

            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(tw.ToString())
            Response.End()
        Catch ex As Exception
            MessageBoxError("btnExportar_Click", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        Call Grillas()
    End Sub

    Protected Sub txtRut_TextChanged(sender As Object, e As EventArgs) Handles txtRut.TextChanged
        If Trim(txtRut.Text) = "" Then
            Call Grillas()
        Else
            txtRut.Text = Replace(txtRut.Text, ".", "")
            txtRut.Text = Replace(txtRut.Text, "-", "")
            If Len(txtRut.Text) = 8 Then
                txtRut.Text = Left(txtRut.Text, 7) + "-" + Right(txtRut.Text, 1)
            ElseIf Len(txtRut.Text) = 9 Then
                txtRut.Text = Left(txtRut.Text, 8) + "-" + Right(txtRut.Text, 1)
            End If

            If Len(txtRut.Text) = 10 Then
                txtRut.Text = txtRut.Text
                If ValidarRut(txtRut) = True Then
                    Call Grillas()
                Else
                    MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                End If
            ElseIf Len(txtRut.Text) = 9 Then
                txtRut.Text = "0" + txtRut.Text
                If ValidarRut(txtRut) = True Then
                    Call Grillas()
                Else
                    MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                End If
            End If
        End If
    End Sub


    Private Sub grdSaldos_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdSaldos.PageIndexChanging
        Try
            grdSaldos.PageIndex = e.NewPageIndex
            Call Grillas()
        Catch ex As Exception
            MessageBoxError("grdSaldos_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub
End Class