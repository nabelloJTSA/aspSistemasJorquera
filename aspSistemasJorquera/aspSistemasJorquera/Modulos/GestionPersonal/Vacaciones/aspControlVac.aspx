﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspControlVac.aspx.vb" Inherits="aspSistemasJorquera.aspControlVac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <div class="row btn-sm">
            <div class="col-md-4 form-group">
                <h2>
                    <asp:Label ID="lbltituloP" runat="server" Text="Control Vacaciones"></asp:Label>
                </h2>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Empresa</label>
                <div>
                    <asp:DropDownList ID="cboEmpresa" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Personal</label>
                <div>
                    <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                    <asp:DropDownList ID="cboBuscarNombre" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True" Visible="false"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-1 form-group">
                <label for="fname" class="control-label col-form-label">&nbsp;</label>
                <div>
                    <asp:LinkButton ID="bntBuscarNombreHorasExtra" runat="server" CssClass="btn "><i class="fa fa-fw fa-search"></i></asp:LinkButton>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">RUT</label>
                <div>
                    <asp:TextBox ID="txtRut" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-2 form-group">
                <label for="fname" class="control-label col-form-label">Fecha Ingreso</label>
                <div>
                    <asp:TextBox ID="txtFecIngreso" Enabled="False" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Tipo</label>
                <div>
                    <asp:DropDownList ID="cboTipoDias" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Periodo</label>
                <div>
                    <asp:DropDownList ID="cboPeriodoOtros" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Dias</label>
                <div>
                    <asp:TextBox ID="txtDiasEsp" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
        </div>

        <asp:Panel ID="pnlDR" runat="server" Visible="false">
            <div class="row justify-content-center table-responsive">
                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Fec. Ingreso 4º Rol</label>
                    <div>
                        <asp:TextBox ID="txtFecIngreso4rol" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Pagar Dias</label>
                    <div>
                        <asp:CheckBox ID="chkPagados" runat="server" AutoPostBack="True" />
                    </div>
                </div>
                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Dias a Pagar</label>
                    <div>
                        <asp:TextBox ID="txtDiasPago" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Fec. Pago</label>
                    <div>
                        <asp:TextBox ID="txtFecPago" TextMode="Date" runat="server" CssClass="form-control text-uppercase" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 form-group">
                    <label for="fname" class="control-label col-form-label">Vigente</label>
                    <div>
                        <asp:CheckBox ID="chkVigencia" runat="server" />
                    </div>
                </div>
            </div>
        </asp:Panel>


        <div class="row justify-content-center table-responsive">
            <div class="col-md-3 form-group">
                <label for="fname" class="control-label col-form-label">Personal</label>
                <div>
                    <asp:TextBox ID="txtBuscar" runat="server" placeholder="Buscar Nombre..." CssClass="form-control text-uppercase" AutoCompleteType="Disabled" AutoPostBack="True"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row justify-content-center table-responsive">
            <div class="col-md-8 form-group ">
                <div>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnGuardarEsp" CssClass="btn bg-orange btn-block" runat="server">Guardar</asp:LinkButton>
                </div>
            </div>

            <div class="col-md-1 form-group ">
                <div>
                    <asp:LinkButton ID="btnCancelar" CssClass="btn bg-red btn-block" runat="server">Cancelar</asp:LinkButton>
                </div>
            </div>

        </div>
        <div class="row justify-content-center table-responsive">
            <div class="col-md-12">
                <asp:GridView ID="grdDias" runat="server" CssClass="table table-bordered table-striped with-border bg-warning" AutoGenerateColumns="false" PageSize="20" AllowPaging="true">
                    <Columns>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="EstFilasGrilla" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px" SelectImageUrl="~/imagen/select.png" SelectText="Selec" ShowSelectButton="True">
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="Center" Width="20px" />
                        </asp:CommandField>

                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("nom_personal")%>' />

                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Bind("id")%>' />
                                <asp:HiddenField ID="hdnIdPeriodo" runat="server" Value='<%# Bind("id_periodo_vac")%>' />
                                <asp:HiddenField ID="hdnDiaEsp" runat="server" Value='<%# Bind("id_dia_esp")%>' />

                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="300px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rut">
                            <ItemTemplate>
                                <asp:Label ID="lblRut" runat="server" Text='<%# Bind("rut_personal")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="90px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Año">
                            <ItemTemplate>
                                <asp:Label ID="lblAnio" runat="server" Text='<%# Bind("año")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="100px" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tipo">
                            <ItemTemplate>
                                <asp:Label ID="lblTipo" runat="server" Text='<%# Bind("nom_dia_esp")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="400px" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Saldo">
                            <ItemTemplate>
                                <asp:Label ID="lblSaldo" runat="server" Text='<%# Bind("num_saldo")%>' />
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" Width="70px" HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' CommandName="2" ToolTip="Eliminar" Width="18px" OnClientClick="return confirm('Esta seguro (a) que desea Eliminar este Registro');"><i class="fa fa-fw fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle CssClass="EstFilasGrilla" HorizontalAlign="left" VerticalAlign="Top" Width="27px" Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="center" BackColor="#c55a11" />
                </asp:GridView>
            </div>
        </div>
    </section>

    <asp:HiddenField ID="hdnExisteDias" runat="server" />
</asp:Content>
