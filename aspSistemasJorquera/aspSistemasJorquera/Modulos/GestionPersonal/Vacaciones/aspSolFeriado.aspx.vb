﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal


Public Class aspSolFeriado
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then
                MtvSolicitudFeriado.ActiveViewIndex = 0
                lbltituloP.Text = "Solicitud Feriado o Permiso"
                Call carga_combo_empresa()
                Call carga_areas()
                Call carga_cargo()
                Call carga_combo_tipo_movimiento()
                Call carga_periodo_vac()
                dtFechaIniMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
                dtFecTerminoMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
                lblDH.Visible = True
                lblDiasHabiles1.Visible = True
                Call CalcularDias()
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQLR As String = "Exec pro_solicitud_feriado 'CCE'"
            cboEmpresa.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_combo_tipo_movimiento()
        Try
            Dim strNomTablaR As String = "REMMae_tipo_movimiento"
            Dim strSQLR As String = "Exec pro_solicitud_feriado 'CTP'"
            cboTipo.DataSource = dtsTablas(strSQLR, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboTipo.DataTextField = "nom_tipo_movimiento"
            cboTipo.DataValueField = "cod_tipo_movimiento"
            cboTipo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_tipo_movimiento", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_areas()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "Exec pro_solicitud_feriado 'CCA'"
        cboArea.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboArea.DataTextField = "nom_area"
        cboArea.DataValueField = "id_area"
        cboArea.DataBind()
    End Sub


    Private Sub carga_cargo()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "Exec pro_solicitud_feriado 'CCC'"
        cboCargo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboCargo.DataTextField = "nom_cargo"
        cboCargo.DataValueField = "cod_cargo"
        cboCargo.DataBind()
    End Sub

    Private Sub carga_periodo_vac()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_solicitud_feriado 'CPV' ,'" & Trim(txtrut.Text) & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id_periodo_vac"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_periodo", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_periodo_otros()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_solicitud_feriado 'CPO' ,'" & Trim(txtrut.Text) & "' ,'" & cboPermisos.SelectedValue & "'"
            cboPeriodo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPeriodo.DataTextField = "periodo"
            cboPeriodo.DataValueField = "id_periodo_vac"
            cboPeriodo.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_periodo", Err, Page, Master)
        End Try
    End Sub


    Private Sub carga_permisos()
        Try
            Dim strNomTablaC As String = "REMmae_periodo_vacaciones"
            Dim strSQLP As String = "Exec pro_solicitud_feriado 'CPE' ,'" & Trim(txtrut.Text) & "','" & cboTipo.SelectedValue & "'"
            cboPermisos.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
            cboPermisos.DataTextField = "nom_dia"
            cboPermisos.DataValueField = "id_dia_esp"
            cboPermisos.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_permisos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTipo.SelectedIndexChanged

        lblSaldoTotal.Visible = False
        lbltextosaldo.Visible = False
        lblDias.Visible = False
        Call CalcularDias()

        If cboTipo.SelectedValue = 0 Then
            lblPermisos.Visible = False
            cboPermisos.Visible = False
            Call carga_periodo_vac()
        Else

            Call carga_permisos()

            If cboTipo.SelectedValue = 11 Then
                lblPermisos.Visible = True
                cboPermisos.Visible = True
                chkMedioDia.Visible = False
                lblMedioDia.Visible = False
                Call carga_periodo_vac()

            ElseIf cboTipo.SelectedValue = 4 Then
                lblPermisos.Visible = True
                cboPermisos.Visible = True
                chkMedioDia.Visible = False
                lblMedioDia.Visible = False
                Call carga_periodo_otros()

            ElseIf cboTipo.SelectedValue = 9 Then
                lblPermisos.Visible = True
                cboPermisos.Visible = True
                chkMedioDia.Visible = True
                lblMedioDia.Visible = True
                Call carga_periodo_otros()
            End If
        End If
    End Sub


    Protected Sub txtrut_TextChanged(sender As Object, e As EventArgs) Handles txtrut.TextChanged
        txtrut.Text = Replace(txtrut.Text, ".", "")
        txtrut.Text = Replace(txtrut.Text, "-", "")
        If Len(txtrut.Text) = 8 Then
            txtrut.Text = Left(txtrut.Text, 7) + "-" + Right(txtrut.Text, 1)
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = Left(txtrut.Text, 8) + "-" + Right(txtrut.Text, 1)
        End If

        If Len(txtrut.Text) = 10 Then
            txtrut.Text = txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call limpiar()
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call limpiar()
            End If
        ElseIf Len(txtrut.Text) = 9 Then
            txtrut.Text = "0" + txtrut.Text
            If ValidarRut(txtrut) = True Then
                Call limpiar()
                Call BuscarPersonal()
            Else
                MessageBox("Rut", "Rut Incorrecto", Page, Master, "E")
                Call limpiar()
            End If
        End If
    End Sub

    Private Sub BuscarPersonal()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec pro_solicitud_feriado 'DS', '" & Trim(txtrut.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                cboEmpresa.SelectedValue = rdoReader(0).ToString
                cboArea.SelectedValue = rdoReader(1).ToString
                cboCargo.SelectedValue = rdoReader(2).ToString
                txtNombreMov.Text = rdoReader(3).ToString
                txtFecIng.Text = CDate(rdoReader(4).ToString).ToShortDateString
                hdnRut.Value = rdoReader(5).ToString

                If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                    lblNomFaenaAct.Visible = True
                    lblFaenaAct.Visible = True
                    Call BuscarFaena()
                    hdnPersonal.Value = 1
                Else
                    lblNomFaenaAct.Visible = False
                    lblFaenaAct.Visible = False
                    hdnPersonal.Value = 2
                End If

                cboTipo.ClearSelection()
                cboPeriodo.ClearSelection()
                lblPermisos.Visible = False
                cboPermisos.Visible = False

                dtFechaIniMov.Text = CDate(Date.Now.Date).ToString("yyyy-MM-dd")
                dtFecTerminoMov.Text = CDate(Date.Now.Date).ToString("yyyy-MM-dd")

            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarPersonal", Err, Page, Master)
        End Try
    End Sub

    Private Sub BuscarFaena()
        Dim rdoReader As SqlDataReader, cmdTemporal As SqlCommand
        Dim strSql As String = "Exec pro_solicitud_feriado 'RF', '" & Trim(txtrut.Text) & "'"
        Try
            Dim cnxBaseDatos As SqlConnection = New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            cmdTemporal = New SqlCommand(strSql, cnxBaseDatos)
            rdoReader = cmdTemporal.ExecuteReader()
            If rdoReader.Read Then
                Session("CodFaena") = rdoReader(0).ToString
                lblNomFaenaAct.Text = rdoReader(1).ToString
            End If
            rdoReader.Close()
            cmdTemporal.Dispose()
            cnxBaseDatos.Close()
            rdoReader = Nothing
            cmdTemporal = Nothing
            cnxBaseDatos = Nothing
        Catch ex As Exception
            MessageBoxError("BuscarFaena", Err, Page, Master)
        End Try
    End Sub

    Private Sub limpiar()
        cboTipo.ClearSelection()
        txtOBSMov.Text = ""
        cboPeriodo.ClearSelection()
        lblSaldoTotal.Visible = False
        lbltextosaldo.Visible = False
        lblDias.Visible = False
        chkPrestamo.Checked = False
        chkPrestamo.Visible = False
        lblSolPre.Visible = False
        lblPermisos.Visible = False
        cboPermisos.Visible = False
        If dtFechaIniMov.Text = "" Then
            dtFechaIniMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If
        If dtFecTerminoMov.Text = "" Then
            dtFecTerminoMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If
        Call carga_periodo_vac()
        Call MostrarFeriados()
        chkMedioDia.Checked = False
        lblDH.Visible = True
        lblDH.Text = "1"
        lblDiasHabiles1.Visible = True

    End Sub

    Protected Sub txtNombreMov_TextChanged(sender As Object, e As EventArgs) Handles txtNombreMov.TextChanged
        Dim blnVisible As Boolean = cbobuscarnombre.Visible, intRegistros As Integer = 0
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtNombreMov.Visible = blnVisible
            Call CargarComboNombre(Trim(txtNombreMov.Text), intRegistros)
            If intRegistros = 2 And cbobuscarnombre.Visible = True Then
                cbobuscarnombre.SelectedIndex = 1
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
            End If
            cbobuscarnombre.ClearSelection()
        Catch ex As Exception
            MessageBoxError("txtNombreMov_TextChanged", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComboNombre(Optional ByVal strBuscar As String = "", Optional ByRef intTotalRegistros As Integer = 0)
        Dim strSQL As String = "Select '0' as rut_personal, '<Seleccionar>' as nom_personal Union Select rut_personal, nom_personal From REMMae_ficha_personal Where nom_personal Like '%" & strBuscar & "%'"
        Dim strNomTabla As String = "REMMae_ficha_personal"
        Try
            cbobuscarnombre.DataTextField = "nom_personal"
            cbobuscarnombre.DataValueField = "rut_personal"
            cbobuscarnombre.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos", intTotalRegistros).Tables(Trim(strNomTabla)).DefaultView
            cbobuscarnombre.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarComboNombre", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cbobuscarnombre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbobuscarnombre.SelectedIndexChanged
        Dim blnVisible As Boolean = cbobuscarnombre.Visible
        Try
            cbobuscarnombre.Visible = Not blnVisible
            txtNombreMov.Visible = blnVisible
            If cbobuscarnombre.SelectedValue <> "0" Then
                Dim varValores() As String = Split(cbobuscarnombre.SelectedValue, ";")
                txtrut.Text = varValores(0).ToString
                Call limpiar()
                Call BuscarPersonal()
            End If

            If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                lblNomFaenaAct.Visible = True
                lblFaenaAct.Visible = True
                Call limpiar()
                Call BuscarFaena()
                hdnPersonal.Value = 1
            Else
                lblNomFaenaAct.Visible = False
                lblFaenaAct.Visible = False
                hdnPersonal.Value = 2
            End If

            Call carga_periodo_vac()

        Catch ex As Exception
            MessageBoxError("cboBuscar_SelectedIndexChanged", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboPermisos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPermisos.SelectedIndexChanged
        lblSaldoTotal.Visible = False
        lbltextosaldo.Visible = False
        lblDias.Visible = False

        If cboPermisos.SelectedValue = 21 Then
            Call carga_periodo_vac()
        Else
            Call carga_periodo_otros()
        End If
        If cboPermisos.SelectedValue = 4 Or cboPermisos.SelectedValue = 5 Then
            CHKPAGADO.Visible = True
        Else
            CHKPAGADO.Visible = False
        End If
    End Sub

    Protected Sub cboPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriodo.SelectedIndexChanged
        If cboPeriodo.SelectedValue = 0 Then
            lblSaldoTotal.Visible = False
            lbltextosaldo.Visible = False
            lblDias.Visible = False
        Else
            If cboTipo.SelectedValue = 11 Then
                lblSaldoTotal.Visible = True
                lbltextosaldo.Visible = True
                lblDias.Visible = True

                If cboPermisos.SelectedValue = 21 Then
                    Call CalcularDiasVac()
                Else
                    Call CalcularDiasPermisos()
                End If

            ElseIf cboTipo.SelectedValue = 9 Then
                lblSaldoTotal.Visible = True
                lbltextosaldo.Visible = True
                lblDias.Visible = True
                Call CalcularDiasPermisos()

            ElseIf cboTipo.SelectedValue = 4 Then
                lblSaldoTotal.Visible = False
                lbltextosaldo.Visible = False
                lblDias.Visible = False
            End If
        End If
    End Sub

    Private Sub CalcularDiasVac()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'CDV', '" & cboPeriodo.SelectedValue & "', '" & Trim(txtrut.Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblSaldoTotal.Text = rdoReader(0).ToString
                    hdnVerificar10Dias.Value = rdoReader(0).ToString
                    hdnAdelantado.Value = rdoReader(2).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularDiasVac", Err, Page, Master)
        End Try
    End Sub


    Private Sub CalcularDiasPermisos()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'CDP', '" & cboPeriodo.SelectedValue & "','" & Trim(txtrut.Text) & "','" & cboPermisos.SelectedValue & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    lblSaldoTotal.Text = rdoReader(0).ToString
                Else
                    lblSaldoTotal.Text = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularDiasPermisos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub dtFecTerminoMov_TextChanged(sender As Object, e As EventArgs) Handles dtFecTerminoMov.TextChanged
        lblDiasHabiles1.Visible = True
        lblDH.Visible = True
        chkMedioDia.Checked = False
        Call MostrarFeriados()


        If cboPermisos.SelectedValue = "" Then
            Call CalcularDias()
        Else
            If cboPermisos.SelectedValue = 4 Then
                Call Verificar4Rol()

                If hdnExiste4Rol.Value = 1 Then
                    If cboArea.SelectedValue = 23 Or cboArea.SelectedValue = 24 Then
                        lblDH.Text = DateDiff(DateInterval.Day, CDate(dtFechaIniMov.Text), CDate(dtFecTerminoMov.Text)) + 1
                    Else
                        Call CalcularDias()
                    End If
                Else
                    Call CalcularDias()
                End If
            Else
                Call CalcularDias()
            End If
        End If


        chkMedioDia.Checked = False
        lblDiasHabiles1.Visible = True
        lblDH.Visible = True

        If Val(lblDH.Text) >= 10 Then
            chkPrestamo.Checked = False
            chkPrestamo.Visible = True
            lblSolPre.Visible = True
        Else
            chkPrestamo.Checked = False
            chkPrestamo.Visible = False
            lblSolPre.Visible = False
        End If

    End Sub

    Function ComprobarFeriado() As Boolean
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim ok As Boolean = False
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'VDF', '" & CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy") & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0) = True Then
                        ok = True
                    Else
                        ok = False
                    End If
                Else
                    ok = False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("ComprobarFeriado", Err, Page, Master)
        End Try
        Return ok
    End Function

    Private Sub CalcularDiasFeriados()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'CDF', '" & CDate(dtFechaIniMov.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy") & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnDiasFeriados.Value = rdoReader(0).ToString
                Else
                    hdnDiasFeriados.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularDiasFeriados", Err, Page, Master)
        End Try
    End Sub

    Private Sub MostrarFeriados()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        If dtFechaIniMov.Text = "" Then
            dtFechaIniMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If
        If dtFecTerminoMov.Text = "" Then
            dtFecTerminoMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'RDF', '" & CDate(dtFechaIniMov.Text).ToString("dd-MM-yyyy") & "', '" & CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy") & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = "0" Then
                        lblnomFeriados.Visible = False
                        txtFeriados.Visible = False
                    Else
                        lblnomFeriados.Visible = True
                        txtFeriados.Visible = True
                        txtFeriados.Text = rdoReader(0).ToString
                    End If
                Else
                    lblnomFeriados.Visible = False
                    txtFeriados.Visible = False
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("MostrarFeriados", Err, Page, Master)
        End Try
    End Sub

    Private Function DiasLaboraes(ByVal diasC As Integer) As Integer
        Dim fecha_inicial As String = dtFechaIniMov.Text
        Dim fecha_final As String = dtFecTerminoMov.Text
        Dim dias As Integer = 0
        Dim DiaSemana As Integer = 0
        Dim x As Integer
        For x = 0 To (diasC - 1)
            DiaSemana = Weekday(System.DateTime.FromOADate(CDate(fecha_inicial).ToOADate + x), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
            If DiaSemana < 6 Then
                dias = dias + 1
            End If
        Next x
        Return dias
    End Function

    Private Sub CalcularDias()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim DiasCorridos As Integer = 0
        Dim DiasLaboralesD As Double = 0
        If dtFechaIniMov.Text = "" Then
            dtFechaIniMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If
        If dtFecTerminoMov.Text = "" Then
            dtFecTerminoMov.Text = Date.Now.Date.ToString("yyyy-MM-dd")
        End If

        Dim fechat As DateTime = CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy")
        Dim fechai As DateTime = CDate(dtFechaIniMov.Text).ToString("dd-MM-yyyy")

        Call CalcularDiasFeriados()

        If Year(CDate(dtFecTerminoMov.Text)) = Year(CDate(dtFechaIniMov.Text)) Then

            If Month(CDate(dtFecTerminoMov.Text)) = Month(CDate(dtFechaIniMov.Text)) Then
                DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
            ElseIf fechat > fechai Then
                If Month(CDate(dtFechaIniMov.Text)) = 2 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                ElseIf Month(CDate(dtFechaIniMov.Text)) = 1 Or Month(CDate(dtFechaIniMov.Text)) = 3 Or Month(CDate(dtFechaIniMov.Text)) = 5 Or Month(CDate(dtFechaIniMov.Text)) = 7 Or Month(CDate(dtFechaIniMov.Text)) = 8 Or Month(CDate(dtFechaIniMov.Text)) = 10 Or Month(CDate(dtFechaIniMov.Text)) = 12 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                ElseIf Month(CDate(dtFechaIniMov.Text)) = 4 Or Month(CDate(dtFechaIniMov.Text)) = 6 Or Month(CDate(dtFechaIniMov.Text)) = 9 Or Month(CDate(dtFechaIniMov.Text)) = 11 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                End If
            End If
            DiasLaboralesD = (DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value))
            hdndiasHabiles.Value = DiasLaboralesD
            lblDH.Text = DiasLaboralesD

        ElseIf Year(CDate(dtFecTerminoMov.Text)) > Year(CDate(dtFechaIniMov.Text)) Then

            If Month(CDate(dtFecTerminoMov.Text)) = Month(CDate(dtFechaIniMov.Text)) Then
                DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
            ElseIf fechat > fechai Then
                If Month(CDate(dtFechaIniMov.Text)) = 2 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                ElseIf Month(CDate(dtFechaIniMov.Text)) = 1 Or Month(CDate(dtFechaIniMov.Text)) = 3 Or Month(CDate(dtFechaIniMov.Text)) = 5 Or Month(CDate(dtFechaIniMov.Text)) = 7 Or Month(CDate(dtFechaIniMov.Text)) = 8 Or Month(CDate(dtFechaIniMov.Text)) = 10 Or Month(CDate(dtFechaIniMov.Text)) = 12 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                ElseIf Month(CDate(dtFechaIniMov.Text)) = 4 Or Month(CDate(dtFechaIniMov.Text)) = 6 Or Month(CDate(dtFechaIniMov.Text)) = 9 Or Month(CDate(dtFechaIniMov.Text)) = 11 Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(dtFechaIniMov.Text)), (CDate(dtFecTerminoMov.Text))) + 1
                End If
            End If
            DiasLaboralesD = (DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value))
            hdndiasHabiles.Value = DiasLaboralesD
            lblDH.Text = DiasLaboralesD
        End If
    End Sub
    Private Sub Verificar4Rol()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'R4R', '" & Trim(txtrut.Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnExiste4Rol.Value = 1
                Else
                    hdnExiste4Rol.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("Veriicar4Rol", Err, Page, Master)
        End Try
    End Sub

    Protected Sub dtFechaIniMov_TextChanged(sender As Object, e As EventArgs) Handles dtFechaIniMov.TextChanged
        lblDiasHabiles1.Visible = True
        lblDH.Visible = True
        chkMedioDia.Checked = False
        Call MostrarFeriados()


        If cboPermisos.SelectedValue = "" Then
            Call CalcularDias()
        Else
            If cboPermisos.SelectedValue = 4 Then
                Call Verificar4Rol()

                If hdnExiste4Rol.Value = 1 Then
                    If cboArea.SelectedValue = 23 Or cboArea.SelectedValue = 24 Then
                        lblDH.Text = DateDiff(DateInterval.Day, CDate(dtFechaIniMov.Text), CDate(dtFecTerminoMov.Text)) + 1
                    Else
                        Call CalcularDias()
                    End If
                Else
                    Call CalcularDias()
                End If
            Else
                Call CalcularDias()
            End If
        End If


        chkMedioDia.Checked = False
        lblDiasHabiles1.Visible = True
        lblDH.Visible = True

        If Val(lblDH.Text) >= 10 Then
            chkPrestamo.Checked = False
            chkPrestamo.Visible = True
            lblSolPre.Visible = True
        Else
            chkPrestamo.Checked = False
            chkPrestamo.Visible = False
            lblSolPre.Visible = False
        End If

    End Sub

    Private Sub RescatarSaldoAnterior()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_solicitud_feriado 'RSA', '" & cboPeriodo.SelectedValue & "', '" & Trim(txtrut.Text) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnSaldoAntiguo.Value = 1
                Else
                    hdnSaldoAntiguo.Value = 0
                End If
            Catch ex As Exception
                MessageBoxError("RescatarSaldoAnterior", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub
    Function RescatarSaldoProgresivo() As Boolean
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim ok As Boolean = False
        Dim strSQL As String = "exec pro_solicitud_feriado 'RSP', '" & cboPeriodo.SelectedValue & "', '" & Trim(txtrut.Text) & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    ok = True
                Else
                    ok = False
                End If
            Catch ex As Exception
                MessageBoxError("RescatarSaldoAnterior", Err, Page, Master)
                ok = False
            Finally
                cnxAcceso.Close()
            End Try
        End Using
        Return ok
    End Function

    Private Sub SolicitudesPendientes()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'RDS', '" & Trim(txtrut.Text) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnDiferenciaDias.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("SolicitudesPendientes", Err, Page, Master)
        End Try
    End Sub

    Private Sub DevolverMailSupervisorFaena()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_solicitud_feriado 'SMS1', '" & Session("CodFaena") & "','" & cboEmpresa.SelectedValue & "','" & hdnPersonal.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisorFaena", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub DevolverMailSupervisor()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_solicitud_feriado 'SMS1', '" & cboArea.SelectedValue & "','" & cboEmpresa.SelectedValue & "','" & hdnPersonal.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSupervisor.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSupervisor", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub EnviarCorreoSolicitud()
        If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
            Call BuscarFaena()
            Call DevolverMailSupervisorFaena()
        Else
            Call DevolverMailSupervisor()
        End If

        Try
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("" & hdnMailSupervisor.Value & "")
            correo.Subject = "[SV] Solicitud Vacaciones"
            correo.Body = "Estimado (a), La solicitud " & hdnTipoPermiso.Value & " de " & txtNombreMov.Text & ", para los periodos entre: " & dtFechaIniMov.Text & " y " & dtFecTerminoMov.Text &
                " ha sido ingresada para su revisión con fecha: " & Date.Now.ToString & "."
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()

        Catch ex As Exception
            MessageBoxError("Supervisor no posee correo electronico valido", Err, Page, Master)
        End Try
    End Sub

    Private Sub RescatarProporcionales()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_solicitud_feriado 'RPS','" & Trim(txtrut.Text) & "','" & cboPeriodo.SelectedValue & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnProporcionales.Value = rdoReader(0).ToString
                    hdnSaldoReal.Value = rdoReader(1).ToString
                End If
            Catch ex As Exception
                MessageBoxError("RescatarProporcionales", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Protected Sub btnGrabarMov_Click(sender As Object, e As EventArgs) Handles btnGrabarMov.Click
        Try
            If Trim(txtrut.Text) = "" Then
                MessageBox("Guardar", "Ingrese Rut", Page, Master, "I")
            Else
                If Trim(txtMail.Text) = "" Or dtFechaIniMov.Text = "" Or dtFecTerminoMov.Text = "" Or cboTipo.SelectedValue = 0 Or txtNombreMov.Text = "" Or cboPeriodo.SelectedValue = 0 Then
                    MessageBox("Guardar", "Ingrese Datos Obligatorios (*)", Page, Master, "W")
                Else
                    If ComprobarFeriado() = True Then
                        MessageBox("Guardar", "Fecha Termino no puede ser día inhábil", Page, Master, "W")
                    Else

                        If cboTipo.SelectedValue = 11 And cboPermisos.SelectedValue = 21 Then
                            If txtMail.Text = "" Or dtFechaIniMov.Text = "" Or dtFecTerminoMov.Text = "" Or cboTipo.SelectedValue = 0 Or txtNombreMov.Text = "" Or cboPeriodo.SelectedValue = 0 Then
                                MessageBox("Guardar", "Ingrese Datos Obligatorios (*)", Page, Master, "I")
                            Else
                                System.Threading.Thread.Sleep(0)
                                Call RescatarSaldoAnterior()
                                Call CalcularDiasVac()
                                Call SolicitudesPendientes()
                                Call RescatarProporcionales()
                                If RescatarSaldoProgresivo() = True Then
                                    MessageBox("Guardar", "Posee saldo del periodo anterior, Vacaciones Progresivas", Page, Master, "W")
                                Else

                                    If hdnSaldoAntiguo.Value = 1 Then
                                        MessageBox("Guardar", "Posee saldo del periodo anterior, favor solicitar primero", Page, Master, "I")
                                    Else
                                        If Convert.ToDecimal(lblDH.Text) > Convert.ToDecimal(lblSaldoTotal.Text) Then
                                            MessageBox("Guardar", "No posee saldo suficiente", Page, Master, "I")
                                        Else
                                            If hdnDiferenciaDias.Value > 0 Then
                                                MessageBox("Guardar", "Posee una solictud pendiente favor regularizar", Page, Master, "I")
                                                Exit Sub
                                            Else

                                                If hdnProporcionales.Value = 15 Then
                                                    If hdnVerificar10Dias.Value > 10 Then
                                                        If (Val(hdnVerificar10Dias.Value) - Convert.ToDecimal(lblDH.Text)) >= 10 Then
                                                            If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                hdnPersonal.Value = 1
                                                                Call DevolverMailSupervisorFaena()
                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 1
                                                                    Call GuardarSolFaena()
                                                                End If
                                                            Else
                                                                hdnPersonal.Value = 2
                                                                Call DevolverMailSupervisor()

                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call GuardarSolicitud()


                                                                End If
                                                            End If
                                                            Call limpiar()
                                                        Else
                                                            If Convert.ToDecimal(lblDH.Text) >= 10 Then
                                                                If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                    hdnPersonal.Value = 1
                                                                    Call DevolverMailSupervisorFaena()
                                                                    If hdnMailSupervisor.Value = "" Then
                                                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                    Else
                                                                        hdnPersonal.Value = 1
                                                                        Call GuardarSolFaena()
                                                                    End If
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call DevolverMailSupervisor()

                                                                    If hdnMailSupervisor.Value = "" Then
                                                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                    Else
                                                                        hdnPersonal.Value = 2
                                                                        Call GuardarSolicitud()
                                                                    End If
                                                                End If
                                                                Call limpiar()
                                                            Else
                                                                MessageBox("Guardar", "Dias solicitados no validos, debe tomarse 10 dias de corridos en este periodo.", Page, Master, "I")
                                                            End If
                                                        End If

                                                    ElseIf hdnVerificar10Dias.Value = 10 Then

                                                        If (Convert.ToDecimal(lblSaldoTotal.Text) - Convert.ToDecimal(lblDH.Text)) = 0 Then
                                                            If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                hdnPersonal.Value = 1
                                                                Call DevolverMailSupervisorFaena()
                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 1
                                                                    Call GuardarSolFaena()
                                                                End If
                                                            Else
                                                                hdnPersonal.Value = 2
                                                                Call DevolverMailSupervisor()

                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call GuardarSolicitud()
                                                                End If
                                                            End If
                                                            Call limpiar()
                                                        Else
                                                            MessageBox("Guardar", "Debe seleccionar 10 dias habiles", Page, Master, "I")
                                                            Exit Sub
                                                        End If

                                                    ElseIf hdnVerificar10Dias.Value < 10 Then
                                                        If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                            hdnPersonal.Value = 1
                                                            Call DevolverMailSupervisorFaena()
                                                            If hdnMailSupervisor.Value = "" Then
                                                                MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                            Else
                                                                hdnPersonal.Value = 1
                                                                Call GuardarSolFaena()
                                                            End If
                                                        Else
                                                            hdnPersonal.Value = 2
                                                            Call DevolverMailSupervisor()

                                                            If hdnMailSupervisor.Value = "" Then
                                                                MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                            Else
                                                                hdnPersonal.Value = 2
                                                                Call GuardarSolicitud()
                                                            End If
                                                        End If
                                                        Call limpiar()
                                                    End If
                                                Else
                                                    ' si proporcionales sistema NO es igual a 15 -- COMENTARRR en caso de que no funcione

                                                    Dim diferencia As Decimal = 15 - hdnProporcionales.Value
                                                    Dim total As Decimal = diferencia + hdnSaldoReal.Value

                                                    If total >= 10 Then

                                                        If Val(lblDH.Text) >= 10 Then
                                                            If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                hdnPersonal.Value = 1
                                                                Call DevolverMailSupervisorFaena()
                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 1
                                                                    Call GuardarSolFaena()
                                                                End If
                                                            Else
                                                                hdnPersonal.Value = 2
                                                                Call DevolverMailSupervisor()

                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call GuardarSolicitud()
                                                                End If
                                                            End If
                                                            Call limpiar()


                                                        Else
                                                            If (total - Val(lblDH.Text)) < 10 Then
                                                                MessageBox("Guardar", "Debe haber un comprobante de 10 dias para este periodo.", Page, Master, "E")
                                                            Else
                                                                If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                    hdnPersonal.Value = 1
                                                                    Call DevolverMailSupervisorFaena()
                                                                    If hdnMailSupervisor.Value = "" Then
                                                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                    Else
                                                                        hdnPersonal.Value = 1
                                                                        Call GuardarSolFaena()
                                                                    End If
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call DevolverMailSupervisor()

                                                                    If hdnMailSupervisor.Value = "" Then
                                                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                    Else
                                                                        hdnPersonal.Value = 2
                                                                        Call GuardarSolicitud()
                                                                    End If
                                                                End If
                                                                Call limpiar()
                                                            End If
                                                        End If
                                                    Else
                                                        If (total - Val(lblDH.Text)) > 5 Then
                                                            MessageBox("Guardar", "Debe seleccionar entre 0 y 5 Dias", Page, Master, "E")
                                                        Else
                                                            If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                                                hdnPersonal.Value = 1
                                                                Call DevolverMailSupervisorFaena()
                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 1
                                                                    Call GuardarSolFaena()
                                                                End If
                                                            Else
                                                                hdnPersonal.Value = 2
                                                                Call DevolverMailSupervisor()

                                                                If hdnMailSupervisor.Value = "" Then
                                                                    MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                                                Else
                                                                    hdnPersonal.Value = 2
                                                                    Call GuardarSolicitud()
                                                                End If
                                                            End If
                                                            Call limpiar()
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If

                        ElseIf cboTipo.SelectedValue = 11 And cboPermisos.SelectedValue = 4 Or cboTipo.SelectedValue = 11 And cboPermisos.SelectedValue = 5 Or cboTipo.SelectedValue = 9 Then
                            If txtMail.Text = "" Or dtFechaIniMov.Text = "" Or dtFecTerminoMov.Text = "" Or cboTipo.SelectedValue = 0 Or txtNombreMov.Text = "" Or cboPeriodo.SelectedValue = 0 Then
                                MessageBox("Guardar", "Ingrese Datos Obligatorios (*)", Page, Master, "I")
                            Else
                                System.Threading.Thread.Sleep(0)

                                If Convert.ToDecimal(lblDH.Text) > Convert.ToDecimal(lblSaldoTotal.Text) Then
                                    MessageBox("Guardar", "No posee saldo suficiente", Page, Master, "I")
                                Else
                                    If Convert.ToDecimal(lblSaldoTotal.Text) <= 0 Then
                                        MessageBox("Guardar", "Saldo de vacaciones insuficiente", Page, Master, "I")
                                    Else
                                        If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                            hdnPersonal.Value = 1
                                            Call DevolverMailSupervisorFaena()
                                            If hdnMailSupervisor.Value = "" Then
                                                MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                            Else
                                                hdnPersonal.Value = 1
                                                Call GuardarSolFaena()
                                            End If

                                        Else
                                            hdnPersonal.Value = 2
                                            Call DevolverMailSupervisor()

                                            If hdnMailSupervisor.Value = "" Then
                                                MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                            Else
                                                hdnPersonal.Value = 2
                                                Call GuardarSolicitud()

                                            End If
                                        End If
                                    End If
                                    Call limpiar()
                                End If
                            End If

                        ElseIf cboTipo.SelectedValue = 4 Then

                            If Session("cod_usu_tc").ToString = 240 Or Session("cod_usu_tc").ToString = 183 Or Session("cod_usu_tc").ToString = 539 Then
                                If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                    hdnPersonal.Value = 1
                                    Call DevolverMailSupervisorFaena()
                                    If hdnMailSupervisor.Value = "" Then
                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                    Else
                                        hdnPersonal.Value = 1
                                        Call GuardarSolFaena()
                                    End If

                                Else
                                    hdnPersonal.Value = 2
                                    Call DevolverMailSupervisor()

                                    If hdnMailSupervisor.Value = "" Then
                                        MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                    Else
                                        hdnPersonal.Value = 2
                                        Call GuardarSolicitud()

                                    End If
                                End If
                                Call limpiar()
                            Else

                                If DateDiff(DateInterval.Month, (CDate(Date.Now.Date)), (CDate(dtFechaIniMov.Text))) <= -1 Then
                                    MessageBox("Guardar", "No puese solicitar permisos atrasados mas de un mes", Page, Master, "I")
                                Else
                                    If cboArea.SelectedValue = 30 Or cboArea.SelectedValue = 31 Then
                                        hdnPersonal.Value = 1
                                        Call DevolverMailSupervisorFaena()
                                        If hdnMailSupervisor.Value = "" Then
                                            MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                        Else
                                            hdnPersonal.Value = 1
                                            Call GuardarSolFaena()
                                        End If
                                    Else
                                        hdnPersonal.Value = 2
                                        Call DevolverMailSupervisor()

                                        If hdnMailSupervisor.Value = "" Then
                                            MessageBox("Guardar", "No existe correo valido para validador de esta area, favor comunicarse con RRHH e indicar que falta asignar validador", Page, Master, "I")
                                        Else
                                            hdnPersonal.Value = 2
                                            Call GuardarSolicitud()
                                        End If
                                    End If
                                    Call limpiar()
                                End If
                            End If
                        End If
                    End If
                    End If
                    End If
        Catch ex As Exception
            MessageBoxError("GuardarSolFaena", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarSolFaena()
        Try
            Call BuscarFaena()

            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_solicitud_feriado"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@id_tipo_permiso", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_permiso").Value = cboTipo.SelectedValue

            If cboArea.SelectedIndex = 0 Then
                comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                comando.Parameters("@id_area").Value = 0
            Else
                comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                comando.Parameters("@id_area").Value = Session("CodFaena")
            End If

            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(dtFechaIniMov.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@obs_detalle", SqlDbType.NVarChar)
            comando.Parameters("@obs_detalle").Value = txtOBSMov.Text
            comando.Parameters.Add("@usr_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@usr_ingreso").Value = Session("cod_usu_tc").ToString
            comando.Parameters.Add("@id_cargo", SqlDbType.NVarChar)
            comando.Parameters("@id_cargo").Value = cboCargo.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@est_rechazado", SqlDbType.NVarChar)
            comando.Parameters("@est_rechazado").Value = 0
            comando.Parameters.Add("@mail", SqlDbType.NVarChar)
            comando.Parameters("@mail").Value = txtMail.Text
            comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
            comando.Parameters("@fec_registro").Value = DateTime.Now.ToString
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc").tostring
            comando.Parameters.Add("@id_periodo", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo").Value = cboPeriodo.SelectedValue

            If chkPrestamo.Checked = True Then
                comando.Parameters.Add("@sol_prestamo", SqlDbType.NVarChar)
                comando.Parameters("@sol_prestamo").Value = 1
            Else
                comando.Parameters.Add("@sol_prestamo", SqlDbType.NVarChar)
                comando.Parameters("@sol_prestamo").Value = 0
            End If

            If cboPermisos.Visible = True Then
                comando.Parameters.Add("@id_dia_esp", SqlDbType.NVarChar)
                comando.Parameters("@id_dia_esp").Value = cboPermisos.SelectedValue
            End If


            comando.Parameters.Add("@est_pagado", SqlDbType.Bit)
            comando.Parameters("@est_pagado").Value = CHKPAGADO.Checked
            comando.Parameters.Add("@est_medio_dia", SqlDbType.Bit)
            comando.Parameters("@est_medio_dia").Value = chkMedioDia.Checked

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call EnviarCorreoSolicitud()
            MessageBox("Guardar", "Solicitud Enviada", Page, Master, "S")
            Call limpiar()

        Catch ex As Exception
            MessageBoxError("GuardarSolFaena", Err, Page, Master)
        End Try
    End Sub

    Private Sub GuardarSolicitud()
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_solicitud_feriado"
            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
            comando.Parameters("@tipo").Value = "I"
            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
            comando.Parameters("@rut_personal").Value = Trim(txtrut.Text)
            comando.Parameters.Add("@id_tipo_permiso", SqlDbType.NVarChar)
            comando.Parameters("@id_tipo_permiso").Value = cboTipo.SelectedValue

            If cboArea.SelectedIndex = 0 Then
                comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                comando.Parameters("@id_area").Value = 0
            Else
                comando.Parameters.Add("@id_area", SqlDbType.NVarChar)
                comando.Parameters("@id_area").Value = cboArea.SelectedValue
            End If

            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
            comando.Parameters("@fec_inicio").Value = CDate(dtFechaIniMov.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
            comando.Parameters("@fec_termino").Value = CDate(dtFecTerminoMov.Text).ToString("dd-MM-yyyy")
            comando.Parameters.Add("@obs_detalle", SqlDbType.NVarChar)
            comando.Parameters("@obs_detalle").Value = txtOBSMov.Text
            comando.Parameters.Add("@usr_ingreso", SqlDbType.NVarChar)
            comando.Parameters("@usr_ingreso").Value = Session("cod_usu_tc").ToString
            comando.Parameters.Add("@id_cargo", SqlDbType.NVarChar)
            comando.Parameters("@id_cargo").Value = cboCargo.SelectedValue
            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
            comando.Parameters("@cod_empresa").Value = cboEmpresa.SelectedValue
            comando.Parameters.Add("@est_rechazado", SqlDbType.NVarChar)
            comando.Parameters("@est_rechazado").Value = 0
            comando.Parameters.Add("@mail", SqlDbType.NVarChar)
            comando.Parameters("@mail").Value = txtMail.Text
            comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
            comando.Parameters("@fec_registro").Value = DateTime.Now.ToString
            comando.Parameters.Add("@usr_add", SqlDbType.NVarChar)
            comando.Parameters("@usr_add").Value = Session("cod_usu_tc").tostring
            comando.Parameters.Add("@id_periodo", SqlDbType.NVarChar)
            comando.Parameters("@id_periodo").Value = cboPeriodo.SelectedValue

            If chkPrestamo.Checked = True Then
                comando.Parameters.Add("@sol_prestamo", SqlDbType.NVarChar)
                comando.Parameters("@sol_prestamo").Value = 1
            Else
                comando.Parameters.Add("@sol_prestamo", SqlDbType.NVarChar)
                comando.Parameters("@sol_prestamo").Value = 0
            End If

            If cboPermisos.Visible = True Then
                comando.Parameters.Add("@id_dia_esp", SqlDbType.NVarChar)
                comando.Parameters("@id_dia_esp").Value = cboPermisos.SelectedValue
            End If

            comando.Parameters.Add("@est_pagado", SqlDbType.Bit)
            comando.Parameters("@est_pagado").Value = CHKPAGADO.Checked
            comando.Parameters.Add("@est_medio_dia", SqlDbType.Bit)
            comando.Parameters("@est_medio_dia").Value = chkMedioDia.Checked

            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()

            Call EnviarCorreoSolicitud()
            MessageBox("Guardar", "Solicitud Enviada", Page, Master, "S")
            Call limpiar()
        Catch ex As Exception
            MessageBoxError("GuardarSolicitud", Err, Page, Master)
        End Try
    End Sub

    Protected Sub chkMedioDia_CheckedChanged(sender As Object, e As EventArgs) Handles chkMedioDia.CheckedChanged
        If chkMedioDia.Checked = True Then
            lblDH.Text = Val(lblDH.Text) - 0.5
        Else
            Call CalcularDias()
        End If
    End Sub

    Protected Sub btnHistorial_Click(sender As Object, e As EventArgs) Handles btnHistorial.Click
        MtvSolicitudFeriado.ActiveViewIndex = 1
        lbltituloP.Text = "Historial Permisos"
        Call CargarGrillaTodos()

    End Sub

    Protected Sub btnIngresoFeriado_Click(sender As Object, e As EventArgs) Handles btnIngresoFeriado.Click
        MtvSolicitudFeriado.ActiveViewIndex = 0

        lbltituloP.Text = "Solicitud Feriado o Permiso"
    End Sub

    Protected Sub btnSaldos_Click(sender As Object, e As EventArgs) Handles btnSaldos.Click
        MtvSolicitudFeriado.ActiveViewIndex = 2
        lbltituloP.Text = "Ver Saldos"

        If txtrut.Text = "" Then
            MessageBox("Saldos", "Ingrese RUT valido", Page, Master, "I")
        Else
            Call CargarGrillaDetalleSaldoSolicitud()
        End If
    End Sub

    Private Sub CargarGrillaTodos()
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_solicitud_feriado 'GT' ,'" & Trim(txtrut.Text) & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaDetalleSaldoSolicitud()
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_solicitud_feriado 'CGS' , '" & Trim(txtrut.Text) & "'"
            grdSaldos.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdSaldos.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaDetalleSaldoSolicitud", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdHistorial_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHistorial.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim rdoReader As SqlDataReader
            Dim cmdTemporal As SqlCommand
            Dim strSQL As String = "Exec pro_solicitud_feriado 'SPE', '" & HttpUtility.HtmlDecode(CType(row.FindControl("hdn_id"), HiddenField).Value) & "'"
            Using cnxBaseDatos As New SqlConnection(strCnx)
                cnxBaseDatos.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        If rdoReader(0).ToString = 2 Or rdoReader(0).ToString = 1 Then
                            Dim objBtn As LinkButton = CType(row.FindControl("btnEliminar"), LinkButton)
                            objBtn.Visible = False
                        Else
                            Dim objBtn As LinkButton = CType(row.FindControl("btnEliminar"), LinkButton)
                            objBtn.Visible = True
                        End If

                        If rdoReader(0).ToString = 2 Then
                            Dim objBtn_Imp As LinkButton = CType(row.FindControl("btnImprimir"), LinkButton)
                            objBtn_Imp.Visible = True
                        Else
                            Dim objBtn_Imp As LinkButton = CType(row.FindControl("btnImprimir"), LinkButton)
                            objBtn_Imp.Visible = False
                        End If

                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                Catch
                    MessageBoxError("grdHistorial_RowDataBound", Err, Page, Master)
                End Try

            End Using
            rdoReader = Nothing
            cmdTemporal = Nothing
        End If
    End Sub

    Private Sub grdHistorial_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdHistorial.PageIndexChanging
        Try
            grdHistorial.PageIndex = e.NewPageIndex
            Call CargarGrillaTodos()
        Catch ex As Exception
            MessageBoxError("grdHistorial_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Private Sub grdHistorial_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHistorial.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdHistorial.Rows(intRow)
                Dim objID As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                comando.CommandType = CommandType.StoredProcedure
                comando.CommandText = "pro_solicitud_feriado"
                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                comando.Parameters("@tipo").Value = "E"
                comando.Parameters.Add("@id", SqlDbType.NVarChar)
                comando.Parameters("@id").Value = objID.Value
                comando.Connection = conx
                conx.Open()
                comando.ExecuteNonQuery()
                conx.Close()

                Call CargarGrillaTodos()

                MessageBox("Eliminar", "Solicitud Eliminada", Page, Master, "S")


            ElseIf Trim(LCase(e.CommandName)) = "3" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdHistorial.Rows(intRow)

                Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim hdnDiaEsp As HiddenField = CType(row.FindControl("hdnIdDiaEsp"), HiddenField)



                Dim obj_num_comprobante As Label = CType(row.FindControl("lblNumComprobante"), Label)

                hdnRutComprobante.Value = Trim(txtrut.Text)
                hdnNumComprobante.Value = obj_num_comprobante.Text

                Dim inicio As Label = CType(row.FindControl("lblInicio"), Label)
                Dim termino As Label = CType(row.FindControl("lblTermino"), Label)


                lblNumFormulario.Text = obj_num_comprobante.Text

                Call CargarComprobante(cboEmpresa.SelectedValue, hdnDiaEsp.Value)

                btnImp.Visible = True
                btnImp.Enabled = True
                lbltitulo.Visible = True
                lblNumFormulario.Visible = True

            End If

        Catch ex As Exception
            MessageBoxError("grdHistorial_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarComprobante(ByVal empresa As String, ByVal dia_esp As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader

        comando.CommandText = "Exec pro_validar_feriado 'RCV', '" & hdnRutComprobante.Value & "', '" & hdnNumComprobante.Value & "'"
        comando.Connection = conx
        conx.Open()

        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            If rdoReader(14).ToString = 1 Or rdoReader(14).ToString = 1 Then
                lblTituloComprobante.Text = "COMPROBANTE DE VACACIONES"
            Else
                lblTituloComprobante.Text = "COMPROBANTE DE PERMISOS"
            End If

            lblncomprobante.Text = rdoReader(0).ToString
            lblrut.Text = rdoReader(1).ToString
            lblfecha.Text = Mid(rdoReader(2).ToString, 1, 10)
            lblfechai.Text = Mid(rdoReader(3).ToString, 1, 10)
            lblfechat.Text = Mid(rdoReader(4).ToString, 1, 10)
            lbldiashabiles.Text = rdoReader(5).ToString
            lblnota.Text = rdoReader(17).ToString
            lblnombre.Text = rdoReader(10).ToString

            If rdoReader(14).ToString = 4 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 9 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 11 Or rdoReader(14).ToString = 1 Then
                If dia_esp = 5 Then
                    lbldiasprogresivosn.Text = "Dias progresivos:"
                    lbldiasprogresivos.Text = rdoReader(5).ToString
                    lbldiashabiles.Text = "-"
                ElseIf dia_esp = 4 Then
                    lbldiashabiles.Text = rdoReader(5).ToString

                    If rdoReader(18).ToString = 23 Or rdoReader(18).ToString = 24 Then
                        lbldiasprogresivosn.Text = "Dias Corridos:"
                        lbldiasprogresivos.Text = rdoReader(6).ToString
                    Else
                        lbldiasprogresivosn.Text = ""
                        lbldiasprogresivos.Text = ""
                    End If
                Else
                    lbldiasprogresivosn.Text = ""
                    lbldiasprogresivos.Text = ""
                    lbldiashabiles.Text = rdoReader(5).ToString
                End If

                lbltituloperiodo.Text = "Correspondiente al período:"
                lblal.Text = "al:"
                lblperiodoi.Text = Mid(rdoReader(11).ToString, 1, 10)
                lblperiodot.Text = Mid(rdoReader(12).ToString, 1, 10)
            End If
        End If


        If empresa = 1 Then
            imglogo.ImageUrl = "../../../../imagen/logo.png"
        ElseIf empresa = 2 Then
            imglogo.ImageUrl = "../../../../imagen/logovmj.png"
        ElseIf empresa = 3 Then
            imglogo.ImageUrl = "../../../../imagen/logostartec.png"
        ElseIf empresa = 12 Then
            imglogo.ImageUrl = "../../../../imagen/logoesex.png"
        ElseIf empresa = 13 Then
            imglogo.ImageUrl = "../../../../imagen/logoditrans.png"
        ElseIf empresa = 14 Or empresa = 26 Or empresa = 27 Then
            imglogo.ImageUrl = ""
        ElseIf empresa = 22 Then
            imglogo.ImageUrl = "../../../../imagen/logologtec.png"
        End If

        rdoReader.Close()
        conx.Close()
    End Sub


    Protected Sub btnLimpiarMov_Click(sender As Object, e As EventArgs) Handles btnLimpiarMov.Click
        Call limpiar()
    End Sub

    Protected Sub grdHistorial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdHistorial.SelectedIndexChanged

    End Sub


End Class