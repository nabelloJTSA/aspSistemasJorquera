﻿Imports System.IO
Imports System.Data
Imports System
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.Configuration
Imports aspSistemasJorquera.vbClasePrincipal

Public Class aspValFeriado
    Inherits System.Web.UI.Page
    Private strCnx As String = DecryptTripleDES(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("cnxBaseDatos").ConnectionString())
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call PanelesInicio(Master, Session("id_perfil_tc"), Session("nom_usu_tc"), Session("cod_usu_tc"))
        Try
            If Not Page.IsPostBack Then

                MultiView1.ActiveViewIndex = 0
                lbltituloP.Text = "Solicitudes Actuales"
                Call CantTipos()
                Call VerificarUsuario()
                Call carga_combo_empresa()
                Call carga_tipo_permiso()

                If hdnCantTipos.Value = 1 Then
                    cboAreas.Visible = True
                    cboEmpresa.Visible = True
                    If hdnTipoPersona.Value = 1 Then
                        Call carga_faenas()
                    Else
                        Call carga_areas()
                    End If
                Else
                    cboEmpresa.Visible = True
                End If
            End If

        Catch ex As Exception
            MessageBoxError("Page_Load", Err, Page, Master)
        End Try
    End Sub


    Private Sub CantTipos()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_validar_feriado 'CCT', '" & Session("cod_usu_tc_via").ToString & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = 1 Then
                        hdnCantTipos.Value = 1
                    Else
                        hdnCantTipos.Value = 2
                    End If
                End If
            Catch ex As Exception
                MessageBoxError("CantTipos", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub VerificarUsuario()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_validar_feriado 'VUS', '" & Session("cod_usu_tc_via").ToString & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnEmpresa.Value = rdoReader(3).ToString
                    hdnMail.Value = rdoReader(4).ToString
                    hdnTipoPersona.Value = rdoReader(5).ToString
                End If
            Catch ex As Exception
                MessageBoxError("VerificarUsuario", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub carga_combo_empresa()
        Try
            Dim strNomTablaR As String = "REMMae_empresa"
            Dim strSQL As String = "Exec pro_validar_feriado 'CCE', '" & Session("cod_usu_tc_via").ToString & "'"
            cboEmpresa.DataSource = dtsTablas(strSQL, Trim(strNomTablaR), "cnxBaseDatos").Tables(Trim(strNomTablaR)).DefaultView
            cboEmpresa.DataTextField = "nom_empresa"
            cboEmpresa.DataValueField = "cod_empresa"
            cboEmpresa.DataBind()
        Catch ex As Exception
            MessageBoxError("carga_combo_empresa", Err, Page, Master)
        End Try
    End Sub

    Private Sub carga_faenas()
        Dim strNomTablaC As String = "REMMae_area"
        Dim strSQL As String = "Exec pro_validar_feriado 'CFA', '" & Session("cod_usu_tc_via").ToString & "'"
        cboAreas.DataSource = dtsTablas(strSQL, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboAreas.DataTextField = "nom_faena"
        cboAreas.DataValueField = "cod_faena"
        cboAreas.DataBind()
    End Sub

    Private Sub carga_areas()
        Dim strNomTablaC As String = "REMMae_area"
        Dim strSQL As String = "Exec pro_validar_feriado 'CAR', '" & Session("cod_usu_tc_via").ToString & "'"
        cboAreas.DataSource = dtsTablas(strSQL, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboAreas.DataTextField = "nom_area"
        cboAreas.DataValueField = "id_area"
        cboAreas.DataBind()
    End Sub

    Private Sub carga_tipo_permiso()
        Dim strNomTablaC As String = "mae_periodo"
        Dim strSQLP As String = "Exec pro_validar_feriado 'CCM'"
        cboTipo.DataSource = dtsTablas(strSQLP, Trim(strNomTablaC), "cnxBaseDatos").Tables(Trim(strNomTablaC)).DefaultView
        cboTipo.DataTextField = "nom_tipo_movimiento"
        cboTipo.DataValueField = "cod_tipo_movimiento"
        cboTipo.DataBind()
    End Sub

    Private Sub Grillas()
        If MultiView1.ActiveViewIndex = 0 Then
            If hdnCantTipos.Value = 1 Then
                If hdnTipoPersona.Value = 1 Then
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        If txtbNombre.Text = "" Then
                            Call CargarGrillaTodosCheckFaenas("")
                        Else
                            Call CargarGrillaTodosCheckFaenas(Trim(txtbNombre.Text))
                        End If
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        If txtbNombre.Text = "" Then
                            Call CargarGrillaTodosFaenas("")
                        Else
                            Call CargarGrillaTodosFaenas(Trim(txtbNombre.Text))
                        End If
                    End If
                Else
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        If txtbNombre.Text = "" Then
                            Call CargarGrillaTodosCheck("")
                        Else
                            Call CargarGrillaTodosCheck(Trim(txtbNombre.Text))
                        End If
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True

                        If txtbNombre.Text = "" Then
                            Call CargarGrillaTodos("")
                        Else
                            Call CargarGrillaTodos(Trim(txtbNombre.Text))
                        End If

                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = True
                    cboTipo.Enabled = False
                    cboEmpresa.Enabled = False
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos2Tipos("")
                    Else
                        Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                    End If
                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos2TiposFiltroEmp("")
                    Else
                        Call CargarGrillaTodos2TiposFiltroEmp(txtbNombre.Text)
                    End If
                End If
            End If
        Else
            If hdnCantTipos.Value = 1 Then
                If hdnTipoPersona.Value = 1 Then
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboEmpresa.Enabled = False
                        cboTipo.Enabled = False

                        If txtbNombreHis.Text = "" Then
                            Call CargarGrillaHistorialCheckFaenas("")
                        Else
                            Call CargarGrillaHistorialCheckFaenas(txtbNombreHis.Text)
                        End If
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        If txtbNombreHis.Text = "" Then
                            Call CargarGrillaHistorialFaenas("")
                        Else
                            Call CargarGrillaHistorialFaenas(txtbNombreHis.Text)
                        End If
                    End If
                Else
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        If txtbNombreHis.Text = "" Then
                            Call CargarGrillaHistorialCheck("")
                        Else
                            Call CargarGrillaHistorialCheck(txtbNombreHis.Text)
                        End If
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True

                        If txtbNombreHis.Text = "" Then
                            Call CargarGrillaHistorial("")
                        Else
                            Call CargarGrillaHistorial(txtbNombreHis.Text)
                        End If
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = False
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCantTipos("")
                    Else
                        Call CargarGrillaHistorialCantTipos(txtbNombreHis.Text)
                    End If
                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCantTiposFiltroEmp("")
                    Else
                        Call CargarGrillaHistorialCantTiposFiltroEmp(txtbNombreHis.Text)
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub btnBuscarTodo_Click(sender As Object, e As EventArgs) Handles btnBuscarTodo.Click
        If cboEmpresa.SelectedValue = 0 Then
            MessageBox("Buscar", "Seleccione empresa a visualizar", Page, Master, "I")
        Else
            Call Grillas()
        End If
        btnImpFormulario.Enabled = False
    End Sub

    Private Sub CargarGrillaTodosCheckFaenas(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTVF' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodosCheckFaenas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaTodosFaenas(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTF' , '" & NombreTodo & "', '" & cboEmpresa.SelectedValue & "', '" & cboAreas.SelectedValue & "', '" & cboTipo.SelectedValue & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodosFaenas", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaTodosCheck(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTV' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodosCheck", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaTodos(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GT' , '" & NombreTodo & "', '" & cboEmpresa.SelectedValue & "', '" & cboAreas.SelectedValue & "', '" & cboTipo.SelectedValue & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaTodos2Tipos(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'G2T' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodos2Tipos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaTodos2TiposFiltroEmp(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'G2TF' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "', '" & cboEmpresa.SelectedValue & "'"
            grdCrearMov.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdCrearMov.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaTodos2Tipos", Err, Page, Master)
        End Try
    End Sub

    Protected Sub chkTodos_CheckedChanged(sender As Object, e As EventArgs) Handles chkTodos.CheckedChanged
        If MultiView1.ActiveViewIndex = 0 Then
            If hdnCantTipos.Value = 1 Then
                If hdnTipoPersona.Value = 1 Then
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        Call CargarGrillaTodosCheckFaenas("")
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        Call CargarGrillaTodosFaenas("")
                    End If
                Else
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        Call CargarGrillaTodosCheck("")
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        Call CargarGrillaTodos("")
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = True
                    cboTipo.Enabled = False
                    cboEmpresa.Enabled = False
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos2Tipos("")
                    Else
                        Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                    End If
                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos2TiposFiltroEmp("")
                    Else
                        Call CargarGrillaTodos2TiposFiltroEmp(txtbNombre.Text)
                    End If
                End If
            End If
        Else
            If hdnCantTipos.Value = 1 Then
                If hdnTipoPersona.Value = 1 Then
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboEmpresa.Enabled = False
                        cboTipo.Enabled = False
                        Call CargarGrillaHistorialCheckFaenas("")
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        Call CargarGrillaHistorialFaenas("")
                    End If
                Else
                    If chkTodos.Checked = True Then
                        cboEmpresa.Enabled = False
                        cboAreas.Enabled = False
                        cboTipo.Enabled = False
                        cboEmpresa.Enabled = False
                        Call CargarGrillaHistorialCheck("")
                    Else
                        cboEmpresa.Enabled = True
                        cboAreas.Enabled = True
                        cboTipo.Enabled = True
                        cboEmpresa.Enabled = True
                        Call CargarGrillaHistorial("")
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = False
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCantTipos("")
                    Else
                        Call CargarGrillaHistorialCantTipos(txtbNombreHis.Text)
                    End If
                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCantTiposFiltroEmp("")
                    Else
                        Call CargarGrillaHistorialCantTiposFiltroEmp(txtbNombreHis.Text)
                    End If
                End If
            End If
        End If

        btnImpFormulario.Enabled = False
    End Sub


    Private Sub CargarGrillaHistorialCheckFaenas(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTHCF' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorial", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorialFaenas(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTHF' , '" & NombreTodo & "', '" & cboEmpresa.SelectedValue & "', '" & cboAreas.SelectedValue & "', '" & cboTipo.SelectedValue & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorialFaenas", Err, Page, Master)
        End Try
    End Sub


    Private Sub CargarGrillaHistorialCheck(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTHC' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorialCheck", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorial(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'GTH' , '" & NombreTodo & "', '" & cboEmpresa.SelectedValue & "', '" & cboAreas.SelectedValue & "',  '" & cboTipo.SelectedValue & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorial", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorialCantTipos(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'G2H' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorialCantTipos", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarGrillaHistorialCantTiposFiltroEmp(ByVal NombreTodo As String)
        Try
            Dim strNomTabla As String = "JQRSGT.dbo.REMMov_item_personal"
            Dim strSQL As String = "Exec pro_validar_feriado 'G2HF' , '" & NombreTodo & "', '" & Session("cod_usu_tc_via").ToString & "', '" & cboEmpresa.SelectedValue & "'"
            grdHistorial.DataSource = dtsTablas(strSQL, Trim(strNomTabla), "cnxBaseDatos").Tables(Trim(strNomTabla)).DefaultView
            grdHistorial.DataBind()
        Catch ex As Exception
            MessageBoxError("CargarGrillaHistorialCantTiposFiltroEmp", Err, Page, Master)
        End Try
    End Sub

    Protected Sub btnValidar_Click(sender As Object, e As EventArgs) Handles btnValidar.Click
        MultiView1.ActiveViewIndex = 0
        lbltituloP.Text = "Solicitudes Actuales"
    End Sub

    Protected Sub btnHis_Click(sender As Object, e As EventArgs) Handles btnHis.Click
        MultiView1.ActiveViewIndex = 1
        lbltituloP.Text = "Historial"

        If hdnCantTipos.Value = 1 Then
            If hdnTipoPersona.Value = 1 Then
                If chkTodos.Checked = True Then
                    Call CargarGrillaHistorialCheckFaenas("")
                Else
                    If cboEmpresa.SelectedValue = 0 Or cboAreas.SelectedValue = 0 Then
                        MessageBox("Historial", "Selecciona datos a visualizar", Page, Master, "I")
                    Else
                        Call CargarGrillaHistorialFaenas("")
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    Call CargarGrillaHistorialCheck("")
                Else
                    If cboEmpresa.SelectedValue = 0 Or cboAreas.SelectedValue = 0 Then
                        MessageBox("Historial", "Selecciona datos a visualizar", Page, Master, "I")
                    Else
                        Call CargarGrillaHistorial("")
                    End If
                End If
            End If
        Else
            If chkTodos.Checked = True Then
                If txtbNombreHis.Text = "" Then
                    Call CargarGrillaHistorialCantTipos("")
                Else
                    Call CargarGrillaHistorialCantTipos(txtbNombreHis.Text)
                End If
            Else
                If txtbNombreHis.Text = "" Then
                    Call CargarGrillaHistorialCantTiposFiltroEmp("")
                Else
                    Call CargarGrillaHistorialCantTiposFiltroEmp(txtbNombreHis.Text)
                End If
            End If
        End If
    End Sub

    Protected Sub txtbNombre_TextChanged(sender As Object, e As EventArgs) Handles txtbNombre.TextChanged
        If hdnCantTipos.Value = 1 Then
            If hdnTipoPersona.Value = 1 Then
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = False
                    cboTipo.Enabled = False
                    cboEmpresa.Enabled = False
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosCheckFaenas("")
                    Else
                        Call CargarGrillaTodosCheckFaenas(txtbNombre.Text)
                    End If
                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True

                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosFaenas("")
                    Else
                        Call CargarGrillaTodosFaenas(txtbNombre.Text)
                    End If

                End If
            Else
                If chkTodos.Checked = True Then
                    cboEmpresa.Enabled = False
                    cboAreas.Enabled = False
                    cboTipo.Enabled = False
                    cboEmpresa.Enabled = False
                    Call CargarGrillaTodosCheck("")

                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosCheck("")
                    Else
                        Call CargarGrillaTodosCheck(txtbNombre.Text)
                    End If

                Else
                    cboEmpresa.Enabled = True
                    cboAreas.Enabled = True
                    cboTipo.Enabled = True
                    cboEmpresa.Enabled = True
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos("")
                    Else
                        Call CargarGrillaTodos(txtbNombre.Text)
                    End If

                End If
            End If
        Else
            If chkTodos.Checked = True Then
                cboEmpresa.Enabled = False
                cboAreas.Enabled = True
                cboTipo.Enabled = False
                cboEmpresa.Enabled = False
                If txtbNombre.Text = "" Then
                    Call CargarGrillaTodos2Tipos("")
                Else
                    Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                End If
            Else
                cboEmpresa.Enabled = True
                cboAreas.Enabled = True
                cboTipo.Enabled = True
                cboEmpresa.Enabled = True
                If txtbNombre.Text = "" Then
                    Call CargarGrillaTodos2TiposFiltroEmp("")
                Else
                    Call CargarGrillaTodos2TiposFiltroEmp(txtbNombre.Text)
                End If
            End If
        End If
    End Sub


    Private Sub grdCrearMov_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCrearMov.RowDataBound
        Try
            Dim row As GridViewRow = e.Row
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim obj_id As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim obj_MedioDia As HiddenField = CType(row.FindControl("hdnEstMedioDia"), HiddenField)
                Dim obkChek As CheckBox = CType(row.FindControl("chkPrestamo"), CheckBox)
                Dim obkChek_dia As CheckBox = CType(row.FindControl("chkMedioDia"), CheckBox)

                hdnId.Value = obj_id.Value
                Call RescatarPrestamo()

                If hdnPrestamo.Value = 1 Then
                    obkChek.Checked = True
                    obkChek.Enabled = True
                Else
                    obkChek.Checked = False
                    obkChek.Enabled = False
                End If

                If obj_MedioDia.Value = True Then
                    obkChek_dia.Checked = True
                Else
                    obkChek_dia.Checked = False
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdEventosM_RowDataBound", Err, Page, Master, Page)
        End Try
    End Sub

    Private Sub RescatarPrestamo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_validar_feriado 'RP', '" & hdnId.Value & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    If rdoReader(0).ToString = 1 Then
                        hdnPrestamo.Value = 1
                    Else
                        hdnPrestamo.Value = 0
                    End If
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("RescatarPrestamo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtObsActualizar(sender As Object, e As EventArgs)
        Dim Txt As TextBox = CType(sender, TextBox)
        Dim row As GridViewRow = CType(Txt.NamingContainer, GridViewRow)
        Call ActualizarObs(CType(row.FindControl("txtOBS"), TextBox).Text, (CType(row.FindControl("hdn_id"), HiddenField).Value))
        If hdnCantTipos.Value = 1 Then
            If hdnTipoPersona.Value = 1 Then
                If chkTodos.Checked = True Then
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosCheckFaenas("")
                    Else
                        Call CargarGrillaTodosCheckFaenas(txtbNombre.Text)
                    End If
                Else
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosFaenas("")
                    Else
                        Call CargarGrillaTodosFaenas(txtbNombre.Text)
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodosCheck("")
                    Else
                        Call CargarGrillaTodosCheck(txtbNombre.Text)
                    End If
                Else
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos("")
                    Else
                        Call CargarGrillaTodos(txtbNombre.Text)
                    End If
                End If
            End If
        Else
            If txtbNombre.Text = "" Then
                Call CargarGrillaTodos2Tipos("")
            Else
                Call CargarGrillaTodos2Tipos(txtbNombre.Text)
            End If
        End If
    End Sub

    Private Sub ActualizarObs(ByVal Valor As String, ByVal ID As String)
        Try
            Dim conx As New SqlConnection(strCnx)
            Dim comando As New SqlCommand
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandText = "pro_validar_feriado"
            comando.Parameters.Add("@tipo", SqlDbType.VarChar)
            comando.Parameters("@tipo").Value = "UOBS"
            comando.Parameters.Add("@obs_detalle", SqlDbType.VarChar)
            comando.Parameters("@obs_detalle").Value = Valor
            comando.Parameters.Add("@id", SqlDbType.VarChar)
            comando.Parameters("@id").Value = ID
            comando.Connection = conx
            conx.Open()
            comando.ExecuteNonQuery()
            conx.Close()
        Catch ex As Exception
            MessageBoxError("ActualizarObs", Err, Page, Master)
        End Try
    End Sub

    Protected Sub txtbNombreHis_TextChanged(sender As Object, e As EventArgs) Handles txtbNombreHis.TextChanged
        If hdnCantTipos.Value = 1 Then
            If hdnTipoPersona.Value = 1 Then
                If chkTodos.Checked = True Then
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCheckFaenas("")
                    Else
                        Call CargarGrillaHistorialCheckFaenas(txtbNombreHis.Text)
                    End If
                Else
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialFaenas("")
                    Else
                        Call CargarGrillaHistorialFaenas(txtbNombreHis.Text)
                    End If
                End If
            Else
                If chkTodos.Checked = True Then
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorialCheck("")
                    Else
                        Call CargarGrillaHistorialCheck(txtbNombreHis.Text)
                    End If
                Else
                    If txtbNombreHis.Text = "" Then
                        Call CargarGrillaHistorial("")
                    Else
                        Call CargarGrillaHistorial(txtbNombreHis.Text)
                    End If
                End If
            End If
        Else
            If txtbNombreHis.Text = "" Then
                Call CargarGrillaHistorialCantTipos("")
            Else
                Call CargarGrillaHistorialCantTipos(txtbNombreHis.Text)
            End If
        End If
    End Sub

    Protected Sub grdHistorial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdHistorial.SelectedIndexChanged

    End Sub

    Private Sub grdHistorial_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHistorial.RowDataBound
        Dim row As GridViewRow = e.Row
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim rdoReader As SqlDataReader
            Dim cmdTemporal As SqlCommand
            Dim strSQL As String = "Exec pro_validar_feriado 'BTNI', '" & HttpUtility.HtmlDecode(CType(row.FindControl("hdn_id"), HiddenField).Value) & "'"
            Using cnxBaseDatos As New SqlConnection(strCnx)
                cnxBaseDatos.Open()
                Try
                    cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)

                    rdoReader = cmdTemporal.ExecuteReader()
                    If rdoReader.Read Then
                        If rdoReader(0).ToString = 1 Or rdoReader(0).ToString = 0 Then
                            Dim objBtn As ImageButton = CType(row.FindControl("btnSeleccionar"), ImageButton)
                            objBtn.Visible = False
                        ElseIf rdoReader(0).ToString = 2 Then
                            Dim objBtn As ImageButton = CType(row.FindControl("btnSeleccionar"), ImageButton)
                            objBtn.Visible = True
                        End If
                    End If
                    rdoReader.Close()
                    cmdTemporal.Dispose()
                Catch ex As Exception
                    MessageBoxError("grdHistorial_RowDataBound", Err, Page, Master)
                End Try
            End Using
            rdoReader = Nothing
            cmdTemporal = Nothing
        End If
    End Sub

    Private Function DiasLaboraes2(ByVal diasC As Integer) As Integer
        Dim fecha_inicial As String = hdnInicio.Value
        Dim fecha_final As String = hdnTermino.Value
        Dim dias As Integer = 0
        Dim DiaSemana As Integer = 0
        Dim x As Integer
        For x = 0 To (diasC - 1)
            DiaSemana = Weekday(System.DateTime.FromOADate(CDate(fecha_inicial).ToOADate + x), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
            If DiaSemana < 6 Then
                dias = dias + 1
            End If
        Next x
        Return dias
    End Function

    Private Sub grdHistorial_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHistorial.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "3" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdHistorial.Rows(intRow)

                Dim hdnid As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim hdnCodEmpresa_obj As HiddenField = CType(row.FindControl("hdn_empresa"), HiddenField)
                Dim hdn_tipopermiso_obj As HiddenField = CType(row.FindControl("hdn_tipopermiso"), HiddenField)
                Dim hdn_dia_esp_obj As HiddenField = CType(row.FindControl("hdnIdDiaEsp"), HiddenField)

                Dim numobj As Label = CType(row.FindControl("lblNumFor"), Label)

                Dim obj_rut As Label = CType(row.FindControl("lblRut"), Label)
                Dim obj_num_comprobante As HiddenField = CType(row.FindControl("hdnNumComprobante"), HiddenField)
                Dim obj_est_evaluado As HiddenField = CType(row.FindControl("hdnEstEvaluado"), HiddenField)

                hdnRutComprobante.Value = obj_rut.Text
                hdnNumComprobante.Value = obj_num_comprobante.Value

                Dim inicio As Label = CType(row.FindControl("lblInicio"), Label)
                Dim termino As Label = CType(row.FindControl("lblTermino"), Label)

                hdnInicio.Value = inicio.Text
                hdnTermino.Value = termino.Text

                Dim conx As New SqlConnection(strCnx)
                Dim comando As New SqlCommand
                Dim DiasCorridos As Integer = 0
                Dim DiasLaboralesD As Double = 0
                Dim fechat As Date = CDate(CType(row.FindControl("lblTermino"), Label).Text)
                Dim fechai As Date = CDate(CType(row.FindControl("lblInicio"), Label).Text)

                If Month(CDate(fechat)) = Month(CDate(fechai)) Then
                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                ElseIf fechat > fechai Then
                    If Month(CDate(fechai)) = 2 Then
                        DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                    ElseIf Month(CDate(fechai)) = 1 Or Month(CDate(fechai)) = 3 Or Month(CDate(fechai)) = 5 Or Month(CDate(fechai)) = 7 Or Month(CDate(fechai)) = 8 Or Month(CDate(fechai)) = 10 Or Month(CDate(fechai)) = 12 Then
                        DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                    ElseIf Month(CDate(fechai)) = 4 Or Month(CDate(fechai)) = 6 Or Month(CDate(fechai)) = 9 Or Month(CDate(fechai)) = 11 Then
                        DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                    End If
                End If

                DiasLaboralesD = DiasLaboraes2(DiasCorridos)

                lblNumFormulario.Text = numobj.Text
                hdnIdSolicitud.Value = hdnid.Value

                Call CargarComprobante(hdn_dia_esp_obj.Value)

                Call CargarAnticipo(hdnCodEmpresa_obj.Value, obj_num_comprobante.Value)

                If hdn_tipopermiso_obj.Value = 11 Or hdn_tipopermiso_obj.Value = 1 Then
                    If lblperiodoi.Text = "" Or lblperiodot.Text = "" Then
                        btnImpFormulario.Visible = False
                        btnImpFormulario.Enabled = False
                    Else
                        btnImpFormulario.Visible = True
                        btnImpFormulario.Enabled = True
                    End If
                Else
                    btnImpFormulario.Visible = True
                    btnImpFormulario.Enabled = True
                End If


                If lblConceptoAnt.Text = "0" Then
                    btnImpAnticipo.Visible = False
                    btnImpAnticipo.Enabled = False
                Else
                    btnImpAnticipo.Visible = True
                    btnImpAnticipo.Enabled = True
                End If

            End If
        Catch ex As Exception
            MessageBoxError("grdHistorial_RowCommand", Err, Page, Master)
        End Try
    End Sub

    Private Sub CargarAnticipo(ByVal empresa As String, ByVal comprobante As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader
        comando.CommandText = "Exec pro_validar_feriado 'SANT', '" & comprobante & "'"
        comando.Connection = conx
        conx.Open()
        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            lblNomTrabAnt.Text = rdoReader(0).ToString
            lblRutTrabAnt.Text = rdoReader(1).ToString
            lblFecAnt.Text = Date.Now.Date
            lblNumAnt.Text = rdoReader(2).ToString
            lblMontoAnt.Text = rdoReader(3).ToString
            lblConceptoAnt.Text = rdoReader(4).ToString
        Else
            lblConceptoAnt.Text = "0"
        End If


        If empresa = 1 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logo.png"
        ElseIf empresa = 2 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logovmj.png"
        ElseIf empresa = 3 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logostartec.png"
        ElseIf empresa = 12 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logoesex.png"
        ElseIf empresa = 13 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logoditrans.png"
        ElseIf empresa = 14 Or empresa = 26 Or empresa = 27 Then
            imglogoAnticipo.ImageUrl = ""
        ElseIf empresa = 22 Then
            imglogoAnticipo.ImageUrl = "../../../../imagen/logologtec.png"
        End If

        rdoReader.Close()
        conx.Close()
    End Sub


    Private Sub CargarComprobante(ByVal dia_esp As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Dim rdoReader As SqlDataReader

        comando.CommandText = "Exec pro_validar_feriado 'RCV', '" & hdnRutComprobante.Value & "', '" & hdnNumComprobante.Value & "'"
        comando.Connection = conx
        conx.Open()

        rdoReader = comando.ExecuteReader

        If rdoReader.Read Then
            If rdoReader(14).ToString = 1 Or rdoReader(14).ToString = 1 Then
                lblTituloComprobante.Text = "COMPROBANTE DE VACACIONES"
            Else
                lblTituloComprobante.Text = "COMPROBANTE DE PERMISOS"
            End If

            lblncomprobante.Text = rdoReader(0).ToString
            lblrut.Text = rdoReader(1).ToString
            lblfecha.Text = Mid(rdoReader(2).ToString, 1, 10)
            lblfechai.Text = Mid(rdoReader(3).ToString, 1, 10)
            lblfechat.Text = Mid(rdoReader(4).ToString, 1, 10)
            lbldiashabiles.Text = rdoReader(5).ToString
            lblnota.Text = rdoReader(17).ToString
            lblnombre.Text = rdoReader(10).ToString
            cboEmpresa.SelectedValue = rdoReader(15).ToString

            If rdoReader(14).ToString = 4 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 9 Then
                lbldiasprogresivosn.Text = ""
                lbldiasprogresivos.Text = ""
                lbltituloperiodo.Text = ""
                lblal.Text = ""
                lblperiodoi.Text = ""
                lblperiodot.Text = ""
            ElseIf rdoReader(14).ToString = 11 Or rdoReader(14).ToString = 1 Then
                If dia_esp = 5 Then
                    lbldiasprogresivosn.Text = "Dias progresivos:"
                    lbldiasprogresivos.Text = rdoReader(5).ToString
                    lbldiashabiles.Text = "-"
                ElseIf dia_esp = 4 Then
                    lbldiashabiles.Text = rdoReader(5).ToString

                    If rdoReader(18).ToString = 23 Or rdoReader(18).ToString = 24 Then
                        lbldiasprogresivosn.Text = "Dias Corridos:"
                        lbldiasprogresivos.Text = rdoReader(6).ToString
                    Else
                        lbldiasprogresivosn.Text = ""
                        lbldiasprogresivos.Text = ""
                    End If
                Else
                    lbldiashabiles.Text = rdoReader(5).ToString
                    lbldiasprogresivosn.Text = ""
                    lbldiasprogresivos.Text = ""
                End If

                lbltituloperiodo.Text = "Correspondiente al período:"
                lblal.Text = "al:"
                lblperiodoi.Text = Mid(rdoReader(11).ToString, 1, 10)
                lblperiodot.Text = Mid(rdoReader(12).ToString, 1, 10)
            End If
        End If


        If cboEmpresa.SelectedValue = 1 Then
            imglogo.ImageUrl = "../../../../imagen/logo.png"
        ElseIf cboEmpresa.SelectedValue = 2 Then
            imglogo.ImageUrl = "../../../../imagen/logovmj.png"
        ElseIf cboEmpresa.SelectedValue = 3 Then
            imglogo.ImageUrl = "../../../../imagen/logostartec.png"
        ElseIf cboEmpresa.SelectedValue = 12 Then
            imglogo.ImageUrl = "../../../../imagen/logoesex.png"
        ElseIf cboEmpresa.SelectedValue = 13 Then
            imglogo.ImageUrl = "../../../../imagen/logoditrans.png"
        ElseIf cboEmpresa.SelectedValue = 14 Or cboEmpresa.SelectedValue = 26 Or cboEmpresa.SelectedValue = 27 Then
            imglogo.ImageUrl = ""
        ElseIf cboEmpresa.SelectedValue = 22 Then
            imglogo.ImageUrl = "../../../../imagen/logologtec.png"
        End If

        rdoReader.Close()
        conx.Close()
    End Sub

    Private Sub CalcularDiasFeriados()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_solicitud_feriado 'CDF', '" & CDate(hdnFecIni.Value) & "', '" & CDate(hdnFecTer.Value) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnDiasFeriados.Value = rdoReader(0).ToString
                Else
                    hdnDiasFeriados.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularDiasFeriados", Err, Page, Master)
        End Try
    End Sub

    Private Sub NuevoComprobante()
        Dim rdoReader As SqlDataReader
        Dim cmdTemporal As SqlCommand
        Dim strSQL As String = "exec pro_validar_feriado 'NC'"
        Using cnxBaseDatos As New SqlConnection(strCnx)
            cnxBaseDatos.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxBaseDatos)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnCodigo.Value = rdoReader(0).ToString + 1
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
            Catch ex As Exception
                MessageBoxError("NuevoComprobante", Err, Page, Master)
            End Try
        End Using

        rdoReader = Nothing

        cmdTemporal = Nothing
    End Sub

    Private Sub RescatarNumeroAnticipo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_prestamos_empresa 'RNC'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnIdAnticipo.Value = rdoReader(0).ToString
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("RescatarNumeroAnticipo", Err, Page, Master)
        End Try
    End Sub

    Private Sub ActualizarEst()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "pro_crear_movimiento"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "U"
        comando.Parameters.Add("@id", SqlDbType.NVarChar)
        comando.Parameters("@id").Value = hdnId.Value
        comando.Parameters.Add("@cod_usu", SqlDbType.NVarChar)
        comando.Parameters("@cod_usu").Value = Session("cod_usu_tc_via").ToString
        comando.Parameters.Add("@sol_prestamo", SqlDbType.NVarChar)
        comando.Parameters("@sol_prestamo").Value = hdnPrestamo.Value
        comando.Parameters.Add("@id_aticipo", SqlDbType.NVarChar)
        comando.Parameters("@id_aticipo").Value = hdnIdAnticipo.Value
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub GuardarControlEstado()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "pro_validar_feriado"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "IMOV"
        comando.Parameters.Add("@id_solicitud", SqlDbType.NVarChar)
        comando.Parameters("@id_solicitud").Value = hdnId.Value
        comando.Parameters.Add("@est_comprobante", SqlDbType.NVarChar)
        comando.Parameters("@est_comprobante").Value = 1
        comando.Parameters.Add("@obs_estado", SqlDbType.NVarChar)
        comando.Parameters("@obs_estado").Value = ""
        comando.Parameters.Add("@fec_edit", SqlDbType.NVarChar)
        comando.Parameters("@fec_edit").Value = Date.Now.ToString
        comando.Parameters.Add("@user_edit", SqlDbType.NVarChar)
        comando.Parameters("@user_edit").Value = Session("cod_usu_tc_via").ToString
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub EnviarCorreoAprobacion()
        Call DevolverMailSolicitante()
        Try
            Dim mensajeMail As String
            If hdnCodSucursal.Value = 1 Then
                mensajeMail = "Estimado (a), el requerimiento de " & hdnTipoPermiso.Value & " efectuado para el trabajador: " & hdnNombre.Value & ", para los periodos entre: " & hdnFecIni.Value & " y " & hdnFecTer.Value &
                " fue autorizado con  fecha: " & Date.Now.ToString & "."
            Else
                mensajeMail = "Estimado (a), el requerimiento de " & hdnTipoPermiso.Value & " efectuado para el trabajador: " & hdnNombre.Value & ", para los periodos entre: " & hdnFecIni.Value & " y " & hdnFecTer.Value &
                " fue autorizado con  fecha: " & Date.Now.ToString & ", el comprobante está disponible para impresión en Viatrack. Debe requerir la firma al validador y enviar comprobante a casa matriz a la brevedad."
            End If
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("" & hdnMailSolicitante.Value + "," + hdnMail.Value & "")
            correo.Subject = "[SV] Solicitud Feriado/Permisos Aprobado"
            correo.Body = mensajeMail
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("EnviarCorreoAprobacion", Err, Page, Master)
        End Try
    End Sub

    Private Sub AdelantoDias(ByVal rut As String, ByVal periodo As String, ByVal id_solicitud As String)
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "Exec pro_validar_feriado 'ADV', '" & rut & "', '" & periodo & "', '" & id_solicitud & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnAdelanto.Value = rdoReader(0).ToString
                    hdnIdAdelanto.Value = rdoReader(1).ToString
                Else
                    hdnAdelanto.Value = "0"
                End If
            Catch ex As Exception
                MessageBoxError("AdelantoDias", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub ActualizarEstAdelanto(ByVal id As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "pro_validar_feriado"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "UAV"
        comando.Parameters.Add("@busqueda", SqlDbType.NVarChar)
        comando.Parameters("@busqueda").Value = id
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub


    Private Sub grdCrearMov_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCrearMov.RowCommand
        Try
            If Trim(LCase(e.CommandName)) = "1" Then
                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdCrearMov.Rows(intRow)
                Dim objID As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim objEst As Label = CType(row.FindControl("lblGenerado"), Label)
                Dim objRut As Label = CType(row.FindControl("lblRut"), Label)
                Dim objFecIni As Label = CType(row.FindControl("lblFecIni"), Label)
                Dim objFecTer As Label = CType(row.FindControl("lblFecTer"), Label)

                Dim objOBS As HiddenField = CType(row.FindControl("hdn_detalles"), HiddenField)
                Dim objNombre As Label = CType(row.FindControl("lblNombre"), Label)
                Dim objEmp As HiddenField = CType(row.FindControl("hdn_empresa"), HiddenField)
                Dim objNom_Emp As HiddenField = CType(row.FindControl("hdnNom_empresa"), HiddenField)
                Dim objTipo As HiddenField = CType(row.FindControl("hdn_tipopermiso"), HiddenField)
                Dim obbIdPeriodo As HiddenField = CType(row.FindControl("hdn_IdPeriodo"), HiddenField)
                Dim obj_CodSucursal As HiddenField = CType(row.FindControl("hdn_CodSucursal"), HiddenField)
                Dim obj_FecIngEmp As HiddenField = CType(row.FindControl("hdnFecingresoEmp"), HiddenField)
                Dim obj_AnioCorresponde As HiddenField = CType(row.FindControl("hdnAnioCorresponde"), HiddenField)
                Dim obj_NomDiaEspecial As HiddenField = CType(row.FindControl("hdnNomDiaEspecial"), HiddenField)
                Dim obj_MedioDia As HiddenField = CType(row.FindControl("hdnEstMedioDia"), HiddenField)
                Dim obj_IdDiaEsp As HiddenField = CType(row.FindControl("hdnIdDiaEsp"), HiddenField)
                Dim obj_IdArea As HiddenField = CType(row.FindControl("hdn_area"), HiddenField)


                Dim obj_tipo_nom As Label = CType(row.FindControl("lblTipo"), Label)
                Dim objImprimir As ImageButton = CType(row.FindControl("btnTraspasar"), ImageButton)
                Dim objChek As CheckBox = CType(row.FindControl("chkPrestamo"), CheckBox)


                If Trim(Session("rut_usu_int")) = Trim(objRut.Text) Then
                    MessageBox("Validar", "NO PUEDE AUTOVALIDAR SOLICITUDES", Page, Master, "W")
                Else

                    Dim mes_ing As String
                    If Month(obj_FecIngEmp.Value) < 10 Then
                        mes_ing = "0" & Month(obj_FecIngEmp.Value)
                    Else
                        mes_ing = Month(obj_FecIngEmp.Value)
                    End If

                    Dim fec_ini_per As String = Day(obj_FecIngEmp.Value) & "-" & mes_ing & "-" & obj_AnioCorresponde.Value - 1
                    Dim fec_ter_per As String = Day(obj_FecIngEmp.Value) & "-" & mes_ing & "-" & obj_AnioCorresponde.Value

                    hdnId.Value = objID.Value
                    hdnRut.Value = objRut.Text
                    hdnFecIni.Value = objFecIni.Text
                    hdnFecTer.Value = objFecTer.Text
                    hdnObs.Value = objOBS.Value
                    hdnNombre.Value = objNombre.Text
                    hdnTipoPer.Value = objTipo.Value
                    hdnEmpresa.Value = objEmp.Value
                    hdn_nom_empresa.Value = objNom_Emp.Value
                    hdnIdPeriodo.Value = obbIdPeriodo.Value
                    hdnCodSucursal.Value = obj_CodSucursal.Value

                    Call CalcularDiasFeriados()
                    Call CantTipos()

                    If hdnTipoPer.Value = 11 And obj_IdDiaEsp.Value = 21 Then

                        Call AdelantoDias(objRut.Text, obbIdPeriodo.Value, objID.Value)

                        hdnTipoPermiso.Value = obj_tipo_nom.Text

                        Call NuevoComprobante()

                        If objChek.Checked = True Then
                            Call RescatarNumeroAnticipo()
                            Call GuardarPrestamoEmpresa(objID.Value)
                        Else
                            hdnIdAnticipo.Value = 0
                        End If

                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand
                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechai As Date = CDate(CType(row.FindControl("lblFecIni"), Label).Text)
                        Dim fechat As Date = CDate(CType(row.FindControl("lblFecTer"), Label).Text)

                        If Year(CDate(fechat)) = Year(CDate(fechai)) Then

                            If Month(CDate(fechat)) = Month(CDate(fechai)) Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                            ElseIf fechat > fechai Then
                                If Month(CDate(fechai)) = 2 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 1 Or Month(CDate(fechai)) = 3 Or Month(CDate(fechai)) = 5 Or Month(CDate(fechai)) = 7 Or Month(CDate(fechai)) = 8 Or Month(CDate(fechai)) = 10 Or Month(CDate(fechai)) = 12 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 4 Or Month(CDate(fechai)) = 6 Or Month(CDate(fechai)) = 9 Or Month(CDate(fechai)) = 11 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                End If
                            End If
                            DiasLaboralesD = (DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value))

                        ElseIf Year(CDate(fechat)) > Year(CDate(fechai)) Then

                            If Month(CDate(fechat)) = Month(CDate(fechai)) Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                            ElseIf fechat > fechai Then
                                If Month(CDate(fechai)) = 2 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 1 Or Month(CDate(fechai)) = 3 Or Month(CDate(fechai)) = 5 Or Month(CDate(fechai)) = 7 Or Month(CDate(fechai)) = 8 Or Month(CDate(fechai)) = 10 Or Month(CDate(fechai)) = 12 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 4 Or Month(CDate(fechai)) = 6 Or Month(CDate(fechai)) = 9 Or Month(CDate(fechai)) = 11 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                End If
                            End If
                            DiasLaboralesD = (DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value))
                        End If

                        If DiasLaboralesD = 1 Then
                            If obj_MedioDia.Value = True Then
                                DiasLaboralesD = 0.5
                            Else
                                DiasLaboralesD = DiasLaboralesD
                            End If
                        Else
                            If obj_MedioDia.Value = True Then
                                DiasLaboralesD = DiasLaboralesD + 0.5
                            Else
                                DiasLaboralesD = DiasLaboralesD
                            End If
                        End If

                        If objChek.Checked = True Then
                            hdnPrestamo.Value = 1
                            ' Dim obsPrestamo As String = "Firma de comprobante de prestamo para: " & Trim(hdnNombre.Value)
                            'Call GrabarEventos(14, 1, hdnEmpresa.Value, Trim(hdnRut.Value), CDate(hdnFecIni.Value), obsPrestamo, 0, Session("id_usu_per").ToString, Date.Now.ToString, 6, procedencia)
                        Else
                            hdnPrestamo.Value = 0
                        End If

                        If CDate(hdnFecIni.Value) <= CDate(hdnFecTer.Value) Then
                            comando = New SqlCommand
                            comando.CommandType = CommandType.StoredProcedure
                            comando.CommandText = "pro_crear_movimiento"
                            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                            comando.Parameters("@tipo").Value = "I"
                            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
                            comando.Parameters("@num_comprobante").Value = hdnCodigo.Value
                            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                            comando.Parameters("@rut_personal").Value = hdnRut.Value
                            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ingreso").Value = Date.Now.ToString
                            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
                            comando.Parameters("@fec_inicio").Value = hdnFecIni.Value
                            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
                            comando.Parameters("@fec_termino").Value = hdnFecTer.Value
                            comando.Parameters.Add("@dias_habiles", SqlDbType.NVarChar)
                            comando.Parameters("@dias_habiles").Value = Replace(DiasLaboralesD, ",", ".")
                            comando.Parameters.Add("@dias_corridos", SqlDbType.NVarChar)
                            comando.Parameters("@dias_corridos").Value = DiasCorridos
                            comando.Parameters.Add("@tipo_movimiento", SqlDbType.NVarChar)
                            comando.Parameters("@tipo_movimiento").Value = hdnTipoPer.Value
                            comando.Parameters.Add("@obs_detalle", SqlDbType.NVarChar)
                            comando.Parameters("@obs_detalle").Value = hdnObs.Value
                            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
                            comando.Parameters("@cod_empresa").Value = objEmp.Value
                            comando.Parameters.Add("@fec_ini_per", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ini_per").Value = fec_ini_per
                            comando.Parameters.Add("@fec_ter_per", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ter_per").Value = fec_ter_per
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()

                            Call CalcularSaldoVac()

                            Dim saldo As Decimal = Convert.ToDecimal(hdnSaldoDiasPermiso.Value)
                            Dim solicitud As Decimal = Convert.ToDecimal(DiasLaboralesD)
                            Dim total_actualizar As String

                            If hdnAdelanto.Value = "0" Then
                                total_actualizar = saldo - solicitud
                            Else
                                total_actualizar = -1 * (hdnAdelanto.Value)
                                Call ActualizarEstAdelanto(hdnIdAdelanto.Value)
                            End If

                            comando = New SqlCommand
                            comando.CommandType = CommandType.StoredProcedure
                            comando.CommandText = "pro_validar_feriado"
                            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                            comando.Parameters("@tipo").Value = "USV"
                            comando.Parameters.Add("@busqueda", SqlDbType.Float)
                            comando.Parameters("@busqueda").Value = Replace(total_actualizar, ".", ",")
                            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                            comando.Parameters("@rut_personal").Value = Trim(hdnRut.Value)
                            comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
                            comando.Parameters("@busqueda2").Value = obbIdPeriodo.Value
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()

                            System.Threading.Thread.Sleep(0)


                            Call ActualizarEst()
                            Call GuardarControlEstado()
                            Call EnviarCorreoAprobacion()

                            If objChek.Checked = True Then
                                Call EnviarCorreoSolicitudConPrestamo()
                            Else
                                Call EnviarCorreoSolicitudSinPrestamo()
                            End If

                            MessageBox("Validar", "Solicitud validada", Page, Master, "S")

                        Else
                            MessageBox("Validar", "Error en rango de fechas", Page, Master, "I")
                        End If

                        If hdnCantTipos.Value = 1 Then
                            If hdnTipoPersona.Value = 1 Then
                                If chkTodos.Checked = True Then
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosCheckFaenas("")
                                    Else
                                        Call CargarGrillaTodosCheckFaenas(txtbNombre.Text)
                                    End If
                                Else
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosFaenas("")
                                    Else
                                        Call CargarGrillaTodosFaenas(txtbNombre.Text)
                                    End If
                                End If
                            Else
                                If chkTodos.Checked = True Then
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosCheck("")
                                    Else
                                        Call CargarGrillaTodosCheck(txtbNombre.Text)
                                    End If
                                Else
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodos("")
                                    Else
                                        Call CargarGrillaTodos(txtbNombre.Text)
                                    End If
                                End If
                            End If
                        Else
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodos2Tipos("")
                            Else
                                Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                            End If
                        End If

                    Else
                        If obj_NomDiaEspecial.Value = "" Then
                            hdnTipoPermiso.Value = obj_tipo_nom.Text
                        Else
                            hdnTipoPermiso.Value = obj_NomDiaEspecial.Value
                        End If

                        Call NuevoComprobante()

                        Dim conx As New SqlConnection(strCnx)
                        Dim comando As New SqlCommand
                        Dim DiasCorridos As Integer = 0
                        Dim DiasLaboralesD As Double = 0
                        Dim fechai As Date = CDate(CType(row.FindControl("lblFecIni"), Label).Text)
                        Dim fechat As Date = CDate(CType(row.FindControl("lblFecTer"), Label).Text)

                        If Year(CDate(fechat)) = Year(CDate(fechai)) Then
                            If Month(CDate(fechat)) = Month(CDate(fechai)) Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                            ElseIf fechat > fechai Then
                                If Month(CDate(fechai)) = 2 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 1 Or Month(CDate(fechai)) = 3 Or Month(CDate(fechai)) = 5 Or Month(CDate(fechai)) = 7 Or Month(CDate(fechai)) = 8 Or Month(CDate(fechai)) = 10 Or Month(CDate(fechai)) = 12 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 4 Or Month(CDate(fechai)) = 6 Or Month(CDate(fechai)) = 9 Or Month(CDate(fechai)) = 11 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                End If
                            End If
                            DiasLaboralesD = DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value)

                        ElseIf Year(CDate(fechat)) > Year(CDate(fechai)) Then

                            If Month(CDate(fechat)) = Month(CDate(fechai)) Then
                                DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                            ElseIf fechat > fechai Then
                                If Month(CDate(fechai)) = 2 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 1 Or Month(CDate(fechai)) = 3 Or Month(CDate(fechai)) = 5 Or Month(CDate(fechai)) = 7 Or Month(CDate(fechai)) = 8 Or Month(CDate(fechai)) = 10 Or Month(CDate(fechai)) = 12 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                ElseIf Month(CDate(fechai)) = 4 Or Month(CDate(fechai)) = 6 Or Month(CDate(fechai)) = 9 Or Month(CDate(fechai)) = 11 Then
                                    DiasCorridos = DateDiff(DateInterval.Day, (CDate(fechai)), (CDate(fechat))) + 1
                                End If
                            End If
                            DiasLaboralesD = DiasLaboraes(DiasCorridos) - Val(hdnDiasFeriados.Value)
                        End If

                        If DiasLaboralesD = 1 Then
                            If obj_MedioDia.Value = True Then
                                DiasLaboralesD = 0.5
                            Else
                                DiasLaboralesD = DiasLaboralesD
                            End If
                        Else
                            If obj_MedioDia.Value = True Then
                                DiasLaboralesD = DiasLaboralesD + 0.5
                            Else
                                DiasLaboralesD = DiasLaboralesD
                            End If
                        End If

                        If objChek.Checked = True Then
                            hdnPrestamo.Value = 1
                            ' Dim obsPrestamo1 As String = "Firma de comprobante de prestamo para: " & Trim(hdnNombre.Value)
                            'Call GrabarEventos(14, 1, hdnEmpresa.Value, Trim(hdnRut.Value), CDate(hdnFecIni.Value), obsPrestamo1, 0, Session("id_usu_per").ToString, Date.Now.ToString, 6, procedencia)
                        Else
                            hdnPrestamo.Value = 0
                        End If


                        If CDate(hdnFecIni.Value) <= CDate(hdnFecTer.Value) Then
                            comando = New SqlCommand
                            comando.CommandType = CommandType.StoredProcedure
                            comando.CommandText = "pro_crear_movimiento"
                            comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                            comando.Parameters("@tipo").Value = "I"
                            comando.Parameters.Add("@num_comprobante", SqlDbType.NVarChar)
                            comando.Parameters("@num_comprobante").Value = hdnCodigo.Value
                            comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                            comando.Parameters("@rut_personal").Value = hdnRut.Value
                            comando.Parameters.Add("@fec_ingreso", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ingreso").Value = Date.Now.ToString
                            comando.Parameters.Add("@fec_inicio", SqlDbType.NVarChar)
                            comando.Parameters("@fec_inicio").Value = hdnFecIni.Value
                            comando.Parameters.Add("@fec_termino", SqlDbType.NVarChar)
                            comando.Parameters("@fec_termino").Value = hdnFecTer.Value
                            comando.Parameters.Add("@dias_habiles", SqlDbType.NVarChar)
                            comando.Parameters("@dias_habiles").Value = Replace(DiasLaboralesD, ",", ".")
                            comando.Parameters.Add("@dias_corridos", SqlDbType.NVarChar)
                            comando.Parameters("@dias_corridos").Value = DiasCorridos
                            comando.Parameters.Add("@tipo_movimiento", SqlDbType.NVarChar)
                            comando.Parameters("@tipo_movimiento").Value = hdnTipoPer.Value
                            comando.Parameters.Add("@obs_detalle", SqlDbType.NVarChar)
                            comando.Parameters("@obs_detalle").Value = hdnObs.Value
                            comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
                            comando.Parameters("@cod_empresa").Value = objEmp.Value
                            comando.Parameters.Add("@fec_ini_per", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ini_per").Value = fec_ini_per
                            comando.Parameters.Add("@fec_ter_per", SqlDbType.NVarChar)
                            comando.Parameters("@fec_ter_per").Value = fec_ter_per
                            comando.Connection = conx
                            conx.Open()
                            comando.ExecuteNonQuery()
                            conx.Close()
                            System.Threading.Thread.Sleep(0)

                            'Dim obs As String = "Control de Firma de comprobante de vacaciones para: " & Trim(hdnNombre.Value)
                            ' Call GrabarEventos(3, 1, hdnEmpresa.Value, Trim(hdnRut.Value), CDate(hdnFecIni.Value), obs, 0, Session("id_usu_per").ToString, Date.Now.ToString, 6, procedencia)


                            If hdnTipoPer.Value = 9 Or hdnTipoPer.Value = 11 And obj_IdDiaEsp.Value = 4 Or hdnTipoPer.Value = 11 And obj_IdDiaEsp.Value = 5 Then
                                Call CalcularSaldoPermisos(obj_IdDiaEsp.Value)
                                Dim saldo As Decimal = Convert.ToDecimal(hdnSaldoDiasPermiso.Value)

                                Dim solicitud As Decimal

                                If obj_IdArea.Value = 23 Or obj_IdArea.Value = 24 Then
                                    solicitud = Convert.ToDecimal(DiasCorridos)
                                Else
                                    solicitud = Convert.ToDecimal(DiasLaboralesD)
                                End If

                                Dim total_actualizar As String = saldo - solicitud


                                comando = New SqlCommand
                                comando.CommandType = CommandType.StoredProcedure
                                comando.CommandText = "pro_validar_feriado"
                                comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
                                comando.Parameters("@tipo").Value = "USADP"
                                comando.Parameters.Add("@busqueda", SqlDbType.Float)
                                comando.Parameters("@busqueda").Value = Replace(total_actualizar, ".", ",")
                                comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
                                comando.Parameters("@rut_personal").Value = Trim(hdnRut.Value)
                                comando.Parameters.Add("@busqueda2", SqlDbType.NVarChar)
                                comando.Parameters("@busqueda2").Value = obbIdPeriodo.Value
                                comando.Parameters.Add("@busqueda3", SqlDbType.NVarChar)
                                comando.Parameters("@busqueda3").Value = obj_IdDiaEsp.Value
                                comando.Connection = conx
                                conx.Open()
                                comando.ExecuteNonQuery()
                                conx.Close()
                            End If

                            Call ActualizarEst()
                            Call GuardarControlEstado()

                            Call EnviarCorreoAprobacion()

                            If objChek.Checked = True Then
                                Call EnviarCorreoSolicitudConPrestamo()
                            Else
                                Call EnviarCorreoSolicitudSinPrestamo()
                            End If

                            MessageBox("Validar", "Solicitud validada", Page, Master, "S")

                        Else
                            MessageBox("Validar", "Error en rango de fechas", Page, Master, "I")
                        End If

                        If hdnCantTipos.Value = 1 Then
                            If hdnTipoPersona.Value = 1 Then
                                If chkTodos.Checked = True Then
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosCheckFaenas("")
                                    Else
                                        Call CargarGrillaTodosCheckFaenas(txtbNombre.Text)
                                    End If
                                Else
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosFaenas("")
                                    Else
                                        Call CargarGrillaTodosFaenas(txtbNombre.Text)
                                    End If
                                End If
                            Else
                                If chkTodos.Checked = True Then
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodosCheck("")
                                    Else
                                        Call CargarGrillaTodosCheck(txtbNombre.Text)
                                    End If
                                Else
                                    If txtbNombre.Text = "" Then
                                        Call CargarGrillaTodos("")
                                    Else
                                        Call CargarGrillaTodos(txtbNombre.Text)
                                    End If
                                End If
                            End If
                        Else
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodos2Tipos("")
                            Else
                                Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                            End If
                        End If
                    End If
                End If

            ElseIf Trim(LCase(e.CommandName)) = "2" Then

                Dim intRow As Integer = Convert.ToInt32(e.CommandArgument)
                Dim row As GridViewRow = grdCrearMov.Rows(intRow)

                Dim objID As HiddenField = CType(row.FindControl("hdn_id"), HiddenField)
                Dim objEst As Label = CType(row.FindControl("lblGenerado"), Label)
                Dim objRut As Label = CType(row.FindControl("lblRut"), Label)
                Dim objFecIni As Label = CType(row.FindControl("lblFecIni"), Label)
                Dim objFecTer As Label = CType(row.FindControl("lblFecTer"), Label)
                Dim objOBS As HiddenField = CType(row.FindControl("hdn_detalles"), HiddenField)
                Dim objNombre As Label = CType(row.FindControl("lblNombre"), Label)
                Dim objTipo As HiddenField = CType(row.FindControl("hdn_tipopermiso"), HiddenField)
                Dim obj_tipo_nom As Label = CType(row.FindControl("lblTipo"), Label)
                Dim obj_NomDiaEspecial As HiddenField = CType(row.FindControl("hdnNomDiaEspecial"), HiddenField)
                Dim obj_MedioDia As HiddenField = CType(row.FindControl("hdnEstMedioDia"), HiddenField)

                hdnId.Value = objID.Value
                hdnRut.Value = objRut.Text
                hdnFecIni.Value = objFecIni.Text
                hdnFecTer.Value = objFecTer.Text
                hdnObs.Value = objOBS.Value
                hdnNombre.Value = objNombre.Text

                If obj_NomDiaEspecial.Value = "" Then
                    hdnTipoPermiso.Value = obj_tipo_nom.Text
                Else
                    hdnTipoPermiso.Value = obj_NomDiaEspecial.Value
                End If

                System.Threading.Thread.Sleep(0)
                Call ActualizarRechazo()
                Call EnviarCorreoRechazo()

                If hdnCantTipos.Value = 1 Then
                    If hdnTipoPersona.Value = 1 Then
                        If chkTodos.Checked = True Then
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodosCheckFaenas("")
                            Else
                                Call CargarGrillaTodosCheckFaenas(txtbNombre.Text)
                            End If
                        Else
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodosFaenas("")
                            Else
                                Call CargarGrillaTodosFaenas(txtbNombre.Text)
                            End If
                        End If
                    Else
                        If chkTodos.Checked = True Then
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodosCheck("")
                            Else
                                Call CargarGrillaTodosCheck(txtbNombre.Text)
                            End If
                        Else
                            If txtbNombre.Text = "" Then
                                Call CargarGrillaTodos("")
                            Else
                                Call CargarGrillaTodos(txtbNombre.Text)
                            End If
                        End If
                    End If
                Else
                    If txtbNombre.Text = "" Then
                        Call CargarGrillaTodos2Tipos("")
                    Else
                        Call CargarGrillaTodos2Tipos(txtbNombre.Text)
                    End If
                End If
                MessageBox("Validar", "Solicitud rechazada", Page, Master, "I")
            End If
        Catch ex As Exception
            MessageBoxError("grdCrearMov_RowCommand", Err, Page, Master)
        End Try
    End Sub


    Private Sub CalcularSaldoPermisos(ByVal dia_esp As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_validar_feriado 'RSO', '" & hdnIdPeriodo.Value & "','" & Trim(hdnRut.Value) & "','" & dia_esp & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnSaldoDiasPermiso.Value = rdoReader(0).ToString
                Else
                    hdnSaldoDiasPermiso.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularSaldoPermisos", Err, Page, Master)
        End Try
    End Sub


    Private Sub CalcularSaldoVac()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        Try
            Dim cmdTemporal As SqlCommand
            Dim rdoReader As SqlDataReader
            Dim strSql As String = "Exec pro_validar_feriado 'RSV', '" & hdnIdPeriodo.Value & "','" & Trim(hdnRut.Value) & "'"
            Using cnxAcceso As New SqlConnection(strCnx)
                cnxAcceso.Open()
                cmdTemporal = New SqlCommand(strSql, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnSaldoDiasPermiso.Value = rdoReader(0).ToString
                Else
                    hdnSaldoDiasPermiso.Value = 0
                End If
                rdoReader.Close()
                cmdTemporal.Dispose()
                rdoReader = Nothing
                cmdTemporal = Nothing
            End Using

        Catch ex As Exception
            MessageBoxError("CalcularSaldoVac", Err, Page, Master)
        End Try
    End Sub


    Private Sub EnviarCorreoSolicitudConPrestamo()
        Try
            Dim mensajeMail As String
            Dim correo As New MailMessage
            mensajeMail = "Estimada, La solicitud de permiso (" & hdnTipoPermiso.Value & "), de " & hdnNombre.Value & ", con RUT N°: " & hdnRut.Value & ", de la empresa: " & hdn_nom_empresa.Value & ", para los periodos entre: " & hdnFecIni.Value & " y " & hdnFecTer.Value &
                    " ha sido aprobada por su supervisor con fecha: " & Date.Now.ToString & " con el numero de comprobante: " & hdnCodigo.Value & ", el numero de anticipo es: " & hdnIdAnticipo.Value & ", favor ingresar los valores correspondiente a este anticipo."
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("macarena.figueroa@jtsa.cl, paula.ruiz@jtsa.cl, alexis.escobar@jtsa.cl")
            correo.Subject = "[SV] Solicitud Feriado/Permisos Aprobado"
            correo.Body = mensajeMail
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("EnviarCorreoSolicitudConPrestamo", Err, Page, Master)
        End Try
    End Sub

    Private Sub EnviarCorreoSolicitudSinPrestamo()
        Try
            Dim mensajeMail As String
            Dim correo As New MailMessage
            mensajeMail = "Estimada, La solicitud de permiso (" & hdnTipoPermiso.Value & "), de " & hdnNombre.Value & ", con RUT N°: " & hdnRut.Value & ", de la empresa: " & hdn_nom_empresa.Value & ", para los periodos entre: " & hdnFecIni.Value & " y " & hdnFecTer.Value &
                " ha sido aprobada por su supervisor con fecha: " & Date.Now.ToString & " con el numero de comprobante: " & hdnCodigo.Value & "."
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("macarena.figueroa@jtsa.cl, paula.ruiz@jtsa.cl, natalia.castillo@jtsa.cl")
            correo.Subject = "[SV] Solicitud Feriado/Permisos Aprobado"
            correo.Body = mensajeMail
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("EnviarCorreoSolicitudSinPrestamo", Err, Page, Master)
        End Try
    End Sub

    Private Function DiasLaboraes(ByVal diasC As Integer) As Integer
        Dim fecha_inicial As String = hdnFecIni.Value
        Dim fecha_final As String = hdnFecTer.Value
        Dim dias As Integer = 0
        Dim DiaSemana As Integer = 0
        Dim x As Integer
        For x = 0 To (diasC - 1)
            DiaSemana = Weekday(System.DateTime.FromOADate(CDate(fecha_inicial).ToOADate + x), Microsoft.VisualBasic.FirstDayOfWeek.Monday)
            If DiaSemana < 6 Then
                dias = dias + 1
            End If
        Next x
        Return dias
    End Function

    Private Sub GuardarPrestamoEmpresa(ByVal id_permiso As String)
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "pro_prestamos_empresa"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "I"
        comando.Parameters.Add("@rut_personal", SqlDbType.NVarChar)
        comando.Parameters("@rut_personal").Value = hdnRut.Value
        comando.Parameters.Add("@cod_empresa", SqlDbType.NVarChar)
        comando.Parameters("@cod_empresa").Value = hdnEmpresa.Value
        comando.Parameters.Add("@periodo", SqlDbType.NVarChar)
        comando.Parameters("@periodo").Value = Year(Date.Now.Date) & "0" & Month(Date.Now.Date)
        comando.Parameters.Add("@num_prestamo", SqlDbType.NVarChar)
        comando.Parameters("@num_prestamo").Value = hdnIdAnticipo.Value
        comando.Parameters.Add("@id_concepto", SqlDbType.NVarChar)
        comando.Parameters("@id_concepto").Value = 1
        comando.Parameters.Add("@val_total", SqlDbType.NVarChar)
        comando.Parameters("@val_total").Value = "200000"
        comando.Parameters.Add("@num_cuotas", SqlDbType.NVarChar)
        comando.Parameters("@num_cuotas").Value = "10"
        comando.Parameters.Add("@fec_ini_pago", SqlDbType.NVarChar)
        comando.Parameters("@fec_ini_pago").Value = Date.Now.Date
        comando.Parameters.Add("@fec_registro", SqlDbType.NVarChar)
        comando.Parameters("@fec_registro").Value = Date.Now.ToString
        comando.Parameters.Add("@obs", SqlDbType.NVarChar)
        comando.Parameters("@obs").Value = Trim(hdnObs.Value)
        comando.Parameters.Add("@id_permiso", SqlDbType.NVarChar)
        comando.Parameters("@id_permiso").Value = id_permiso
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub


    Private Sub ActualizarRechazo()
        Dim conx As New SqlConnection(strCnx)
        Dim comando As New SqlCommand
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandText = "pro_crear_movimiento"
        comando.Parameters.Add("@tipo", SqlDbType.NVarChar)
        comando.Parameters("@tipo").Value = "UR"
        comando.Parameters.Add("@id", SqlDbType.NVarChar)
        comando.Parameters("@id").Value = hdnId.Value
        comando.Parameters.Add("@cod_usu", SqlDbType.NVarChar)
        comando.Parameters("@cod_usu").Value = Session("cod_usu_tc_via").ToString
        comando.Connection = conx
        conx.Open()
        comando.ExecuteNonQuery()
        conx.Close()
    End Sub


    Private Sub DevolverMailSolicitante()
        Dim cmdTemporal As SqlCommand
        Dim rdoReader As SqlDataReader
        Dim strSQL As String = "exec pro_validar_feriado 'SMS', '" & hdnId.Value & "'"
        Using cnxAcceso As New SqlConnection(strCnx)
            cnxAcceso.Open()
            Try
                cmdTemporal = New SqlCommand(strSQL, cnxAcceso)
                rdoReader = cmdTemporal.ExecuteReader()
                If rdoReader.Read Then
                    hdnMailSolicitante.Value = rdoReader(0).ToString
                End If
            Catch ex As Exception
                MessageBoxError("DevolverMailSolicitante", Err, Page, Master)
                Exit Sub
            Finally
                cnxAcceso.Close()
            End Try
        End Using
    End Sub

    Private Sub EnviarCorreoRechazo()
        Call DevolverMailSolicitante()
        Try
            Dim correo As New MailMessage
            correo.From = New MailAddress("sistemas@viatrack.cl")
            correo.To.Add("" & hdnMailSolicitante.Value + "," + hdnMail.Value & "")
            correo.Subject = "[SV] Solicitud Feriado/Permisos Rechazado"
            correo.Body = "Estimado (a), el requerimiento de " & hdnTipoPermiso.Value & " para el trabajador: " & hdnNombre.Value & ", para los periodos entre: " & hdnFecIni.Value & " y " & hdnFecTer.Value &
                " ha sido rechazada con fecha: " & Date.Now.ToString & ", por no ajustarse a normativa legal, comuníquese con Macarena Figueroa, anexo 248."
            correo.IsBodyHtml = True
            correo.Priority = MailPriority.Normal
            'correo.Attachments.Add(New Attachment(Server.MapPath(archivo)))
            Dim smtp As New SmtpClient
            smtp.Host = "mail.viatrack.cl"
            smtp.Credentials = New System.Net.NetworkCredential("sistemas@viatrack.cl", "Sistemas.7190")
            smtp.Send(correo)
            correo.Attachments.Clear()
            correo.Dispose()
        Catch ex As Exception
            MessageBoxError("EnviarCorreoRechazo", Err, Page, Master)
        End Try
    End Sub

    Protected Sub grdCrearMov_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdCrearMov.SelectedIndexChanged

    End Sub

    Private Sub grdHistorial_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdHistorial.PageIndexChanging
        Try
            grdHistorial.PageIndex = e.NewPageIndex
            Call Grillas()
        Catch ex As Exception
            MessageBoxError("grdHistorial_PageIndexChanging", Err, Page, Master)
        End Try
    End Sub

    Protected Sub cboEmpresa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedIndexChanged

    End Sub
End Class