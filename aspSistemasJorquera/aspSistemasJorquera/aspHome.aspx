﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="aspHome.aspx.vb" Inherits="aspSistemasJorquera.aspHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlMapa" runat="server" Visible="false">

        <section class="content">

            <div class="row ">

                <div class="col-md-8 form-group">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <h3>
                                <b>
                                    <asp:Label ID="lbltitulo" CssClass="text-primary" runat="server" Text="UT EN EJECUCIÓN AL: "></asp:Label><asp:Label ID="lblFecActualHome" CssClass="text-primary" runat="server" Text="UT EN EJECUCIÓN AL: "></asp:Label></b>
                            </h3>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-md-12 form-group">
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                        Estado</label>
                                    <div>
                                        <asp:DropDownList ID="cboEstado" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="fname"
                                        class="control-label col-form-label">
                                    </label>
                                    <div>
                                        <asp:Button ID="btnLimpiarFiltros" runat="server" CssClass="btn btn-primary" Text="Limpiar Filtros" />
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="col-md-12 form-group">
                            <div style="overflow: auto; height: 800px">
                                <h4>
                                    <asp:Label ID="lblTotRegistros" runat="server" CssClass="text-primary" Font-Bold="true" Text=""></asp:Label></h4>

                                <asp:GridView ID="grdPatentes" runat="server" CssClass="table table-bordered table-striped table-responsive table-hover" AutoGenerateColumns="false">
                                    <Columns>

                                        <asp:TemplateField HeaderText="CODIGO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCodCamion" runat="server" Text='<%# Bind("cod_vehiculo")%>' />
                                                <asp:HiddenField ID="hdnIdUltimoViaje" runat="server" Value='<%# Bind("id_est_ultimo_viaje")%>' />

                                                <asp:HiddenField ID="hdnEstGPSCamion" runat="server" Value='<%# Bind("est_gps_camion")%>' />
                                                <asp:HiddenField ID="hdnEstGPSArrastre" runat="server" Value='<%# Bind("est_gps_arrastre")%>' />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CAMION ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPatenteCamion" runat="server" Text='<%# Bind("nom_patente")%>'></asp:Label>
                                                <br />
                                                <b>
                                                    <asp:Label ID="lblGPS" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSCamion" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ARASTRE ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrastre" runat="server" Text='<%# Bind("pat_arrastre")%>'></asp:Label>
                                                <br />
                                                <b>
                                                    <asp:Label ID="lblGPSArrastre" runat="server" Text="GPS" /></b>
                                                <asp:ImageButton ID="btnGPSArrastre" ImageUrl='' runat="server" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Width="15px" Height="15px" />

                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="CONDUCTOR">
                                            <ItemTemplate>
                                                <asp:Label ID="lblConductor" runat="server" Text='<%# Bind("nom_conductor")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ULTIMO VIAJE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIDUltViaje" runat="server" Text='<%# Bind("id_ultimo_viaje")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="FECHA ULT. VIAJE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFecUltViaje" runat="server" Text='<%# Bind("fec_ultimo_viaje")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ESTADO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEstado" runat="server" Text='<%# Bind("nom_estado")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="TRANSPORTISTA">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNomTrasnportista" runat="server" Text='<%# Bind("nom_transportista")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="EstFilasGrilla" />
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnVerMapa" runat="server" CommandArgument="<%# Container.DataItemIndex.ToString() %>" OnClick="VerMapa" CssClass="btn btn-primary" ToolTip="Ver Mapa">
                                                       Ver Mapa                                                  
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                    <HeaderStyle HorizontalAlign="center" BackColor="#7FBA27" ForeColor="White" />

                                </asp:GridView>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-md-4 form-group">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <asp:Label ID="Label1" runat="server" ForeColor="#cc9900" Font-Bold="true" Text="EN ORIGEN"></asp:Label>
                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;    EN RUTA"></asp:Label>
                            <asp:Label ID="Label3" runat="server" ForeColor="#3399ff" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;&nbsp;   EN DESTINO"></asp:Label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 form-group">

                            <div id="map" class="table-responsive" style="width: 100%; height: 900px;">
                            </div>
                        </div>
                    </div>

                </div>

                <script>
                    var map = L.map('map').setView([-37.448841, -72.329437], 8);
                    mapLink =
                        '<a href="http://openstreetmap.org">OpenStreetMap</a>';
                    L.tileLayer(
                        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                            attribution: '&copy; ' + mapLink + ' Contributors',
                            maxZoom: 18,
                        }).addTo(map);                       

                    var azul = new L.Icon({
                        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
                        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                        iconSize: [25, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });
            
                    var verde = new L.Icon({
                        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
                        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                        iconSize: [25, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var rojo = new L.Icon({
                        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
                        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                        iconSize: [25, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var amarillo = new L.Icon({
                        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
                        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                        iconSize: [25, 41],
                        iconAnchor: [12, 41],
                        popupAnchor: [1, -34],
                        shadowSize: [41, 41]
                    });

                    var planes = <%=CargarGrafico3()%>;

                var color;                   
                   
                for (let i = 0; i < planes.length; i++) {

                    if( planes[i][3] == 4){
                        color = rojo;
                    }else if(planes[i][3] == 12){
                        color = amarillo;
                        //}else if(planes[i][3] == 7 || planes[i][3] == 8){
                        //    color = verde;
                    }else{
                        color = azul;
                    }

                    marker = new L.marker([planes[i][1], planes[i][2]],{icon: color})
                        .bindPopup('ESTADO: ' + planes[i][10]+ '</br>Viaje: ' +planes[i][4]+ '</br> Camión: ' +planes[i][0] + '</br> Arrastre: ' +planes[i][7] + '</br> Origen: ' + planes[i][5]+ '</br> Destino: ' + planes[i][6]+ '</br> Conductor: ' + planes[i][8]+ '</br> Transportista: ' + planes[i][9])
                        .addTo(map);
                }
                </script>


            </div>

        </section>


        <asp:HiddenField ID="hdnLat" runat="server" Value="-37.448841" />
        <asp:HiddenField ID="hdnLon" runat="server" Value="-72.329437" />

        <asp:HiddenField ID="hdnPatente" runat="server" Value="0" />


    </asp:Panel>

</asp:Content>

